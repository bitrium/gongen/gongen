# SPDX-License-Identifier: BSD-3-Clause
# Copyright (c) 2024 GongEn Contributors

cmake_minimum_required(VERSION 3.18)

project(GongEn C CXX)

set(CMAKE_CXX_STANDARD 20)
add_compile_definitions(_CRT_SECURE_NO_WARNINGS IMGUI_API=GEN_API)

set(gongen_directory ${CMAKE_CURRENT_SOURCE_DIR})

include(cmake/gongen.cmake)

add_subdirectory(examples)
if(NOT GEN_NO_TEST)
  add_subdirectory(test)
endif()
