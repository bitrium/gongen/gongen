#if defined(VERTEX_SHADER)
layout (location = 0) in vec2 iPos;
layout (location = 1) in vec2 iUv;
layout (location = 2) in vec4 iColor;

layout (set = 0, binding = 0, std140) uniform UniformBuffer {
    mat4 projectionMatrix;
};

layout(location = 0) out vec2 vUv;
layout(location = 1) out vec4 vColor;

void main() {
    gl_Position = projectionMatrix * vec4(iPos.xy, 0.0f, 1.0f);
    vUv = iUv;
    vColor = iColor;
}
#endif //VERTEX_SHADER
#if defined(FRAGMENT_SHADER)
layout (location = 0) out vec4 FragColor;

layout(set = 0, binding = 1) uniform texture2D Texture;
layout(set = 0, binding = 2) uniform sampler Sampler;

layout(location = 0) in vec2 vUv;
layout(location = 1) in vec4 vColor;

void main() {
    FragColor = vColor * texture(sampler2D(Texture, Sampler), vUv.st);
}
#endif //FRAGMENT_SHADER
