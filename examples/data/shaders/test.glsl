#if defined(VERTEX_SHADER)
layout (location = 0) in vec2 iPos;

layout(location = 0) out vec2 oPos;

void main() {
    gl_Position = vec4(iPos, 0.0f, 1.0f);
    oPos = iPos;
}
#endif //VERTEX_SHADER
#if defined(FRAGMENT_SHADER)
layout (location = 0) out vec4 FragColor;

layout(location = 0) in vec2 oPos;

void main() {
    FragColor = vec4(oPos, 1.0f, 1.0f);
}
#endif //FRAGMENT_SHADER
