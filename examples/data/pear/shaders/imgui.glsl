#if defined(VERTEX_SHADER)
layout (location = 0) in vec2 iPos;
layout (location = 1) in vec2 iUv;
layout (location = 2) in vec4 iColor;

layout (binding = 0, std140) uniform UniformBuffer {
    mat4 projectionMatrix;
};

layout(location = 0) out vec2 vUv;
layout(location = 1) out vec4 vColor;

void main() {
    gl_Position = projectionMatrix * vec4(iPos.xy, 0.0f, 1.0f);
    vUv = iUv;
    vColor = iColor;
}
#endif //VERTEX_SHADER
#if defined(FRAGMENT_SHADER)
layout (location = 0) out vec4 FragColor;

layout(binding = 0) uniform sampler2D Texture;

layout(location = 0) in vec2 vUv;
layout(location = 1) in vec4 vColor;

void main() {
    FragColor = vColor * texture(Texture, vUv.st);
}
#endif //FRAGMENT_SHADER
