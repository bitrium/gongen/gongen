#if defined(VERTEX_SHADER)
layout (location = 0) in vec3 iPos;

layout(std140, binding = 0) uniform UniformBufferCamera{
    mat4 vp_matrix;
    mat4 skybox_matrix;
    vec4 camera_pos;
};

layout(location = 0) out vec3 vPos;

void main() {
    vPos = iPos;
    vec4 final_pos = skybox_matrix * vec4(iPos, 1.0);
    gl_Position = final_pos.xyww;
}
#endif //VERTEX_SHADER
#if defined(FRAGMENT_SHADER)
layout (location = 0) out vec4 FragColor;

layout(binding = 0) uniform samplerCube Texture;

layout(location = 0) in vec3 vPos;

void main() {
  FragColor = texture(Texture, vec3(-vPos.x, vPos.y, vPos.z));
}
#endif //FRAGMENT_SHADER
