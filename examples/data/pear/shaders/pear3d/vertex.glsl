#ifdef MDI_RENDERING
#extension GL_ARB_shader_draw_parameters : require
#endif

#if defined(VERTEX_SHADER)
    layout(location = 0) in vec3 iPos;
#if !defined(DEPTH_INSTANCE_DATA)
    layout(location = 1) in vec4 iNormalAndMat;
    
    layout(location = 0) out vec3 vWorldPos;
    layout(location = 1) out vec3 vNormal;
    layout(location = 2) out vec4 vColor;
    layout(location = 3) out vec2 vMaterialData;
#endif

#if defined(DEPTH_ONLY)
struct InstanceData {
  mat4 transform_matrix;
};
#else
struct InstanceData {
  mat4 transform_matrix;
  float normal_matrix[9];
  float color[4];
  uint first_material;
  float padding[2];
};
struct Material {
    vec4 color;
    float roughness;
    float metalness;
    float uv_layer;
    float padding[1];
    vec4 uv_offset;
};
#endif

layout(std430, binding = 0) readonly buffer InstanceDataBuffer {
  InstanceData instances[];
};
#if !defined(DEPTH_ONLY)
layout(std430, binding = 1) readonly buffer MaterialDataBuffer {
    Material materials[];
};
#endif

#if defined(TEXTURED)
layout(location = 2) in vec2 iUV;
layout(location = 4) out vec3 vUV;
#endif

layout(std140, binding = 0) uniform UniformBufferCamera {
    mat4 vp_matrix;
    mat4 skybox_matrix;
    vec4 camera_pos;
};

void main() {
#ifdef MDI_RENDERING
    int instance_id = gl_BaseInstanceARB + gl_InstanceID;
#else
    int instance_id = gl_InstanceID;
#endif
    InstanceData instance_data = instances[instance_id];
    vec4 world_pos = instance_data.transform_matrix * vec4(iPos, 1.0);
    gl_Position = vp_matrix * world_pos;
#if !defined(DEPTH_ONLY)
    mat3 normal_matrix = transpose(mat3(instance_data.normal_matrix[0],
        instance_data.normal_matrix[1], instance_data.normal_matrix[2],
        instance_data.normal_matrix[3], instance_data.normal_matrix[4],
        instance_data.normal_matrix[5], instance_data.normal_matrix[6],
        instance_data.normal_matrix[7], instance_data.normal_matrix[8]));

    float nx = (iNormalAndMat.x * 2.0f) - 1.0f;
    float ny = (iNormalAndMat.y * 2.0f) - 1.0f;
    float nz = (iNormalAndMat.z * 2.0f) - 1.0f;
    vec3 normal = vec3(nx, ny, nz);

    uint mat_id = instance_data.first_material + uint(iNormalAndMat.w * 255);
    Material mat = materials[mat_id];

    vWorldPos = world_pos.xyz;
    vNormal = normal_matrix * normal;
    vColor = mat.color * vec4(instance_data.color[0], instance_data.color[1], instance_data.color[2], instance_data.color[3]);
    vMaterialData = vec2(mat.roughness, mat.metalness);
#endif
#if defined(TEXTURED)
    vec2 uv2 = (iUV * mat.uv_offset.xy) + mat.uv_offset.zw;
    vUV = vec3(uv2, mat.uv_layer);
#endif
}
#endif //VERTEX_SHADER

#if defined(FRAGMENT_SHADER) && defined(DEPTH_ONLY)
void main() {}
#endif //FRAGMENT_SHADER
