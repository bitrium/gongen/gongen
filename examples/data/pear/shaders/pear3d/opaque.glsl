layout(location = 0) out vec4 FragColor;
layout(location = 1) out vec4 FragNormal;

layout(location = 0) in vec4 vWorldPos;
layout(location = 1) in vec3 vNormal;
layout(location = 2) in vec4 vColor;
layout(location = 3) in vec2 vMaterialData;

#if defined(TEXTURED)
layout(location = 4) in vec3 vUV;
#endif

layout(std140, binding = 0) uniform UniformBufferCamera {
    mat4 vp_matrix;
    mat4 skybox_matrix;
    vec4 camera_pos;
};

#if defined(TEXTURED)
layout(binding = 0) uniform sampler2DArray Texture;
#endif

struct DirLight {
    vec4 dir_and_intensity;
    vec4 color;
};
struct PointLight {
    vec4 pos_and_radius;
    vec4 color;
    vec4 properties;
};

layout(std430, binding = 2) readonly buffer LightDataBuffer {
    DirLight dir_light;
    PointLight point_lights[];
};

#define PI 3.1415926538

float DistributionGGX(vec3 N, vec3 H, float roughness) {
    float a = roughness * roughness;
    float a2 = a * a;
    float NdotH = max(dot(N, H), 0.0);
    float NdotH2 = NdotH * NdotH;

    float nom = a2;
    float denom = (NdotH2 * (a2 - 1.0) + 1.0);
    denom = PI * denom * denom;

    return nom / denom;
}
float GeometrySchlickGGX(float NdotV, float k) {
    float nom = NdotV;
    float denom = NdotV * (1.0 - k) + k;

    return nom / denom;
}
float GeometrySmith(vec3 N, vec3 V, vec3 L, float k) {
    float NdotV = max(dot(N, V), 0.0);
    float NdotL = max(dot(N, L), 0.0);
    float ggx1 = GeometrySchlickGGX(NdotV, k);
    float ggx2 = GeometrySchlickGGX(NdotL, k);

    return ggx1 * ggx2;
}
vec3 fresnelSchlick(float cosTheta, vec3 F0) {
    return F0 + (1.0 - F0) * pow(clamp(1.0 - cosTheta, 0.0, 1.0), 5.0);
}
vec3 calc_lighting(vec3 N, vec3 V, vec3 L, float roughness, float metallic, vec3 albedo) {
    vec3 H = normalize(V + L);

    vec3 F0 = vec3(0.04);

    F0 = mix(F0, albedo, metallic);
    vec3 F = fresnelSchlick(max(dot(H, V), 0.0), F0);

    float NDF = DistributionGGX(N, H, roughness);
    float G = GeometrySmith(N, V, L, roughness);
    vec3 numerator = NDF * G * F;
    float denominator = 4.0 * max(dot(N, V), 0.0) * max(dot(N, L), 0.0) + 0.0001;
    vec3 specular = numerator / denominator;

    vec3 kS = F;
    vec3 kD = vec3(1.0) - kS;
    kD *= 1.0 - metallic;

    float NdotL = max(dot(N, L), 0.0);

    return (kD * albedo / PI + specular) * NdotL;
}
float PointLightAttenuation(float dist, float radius, vec4 data) {
    float intensity = data.x;
    float falloff = data.y;

    float s = dist / radius;
    if (s >= 1.0) return 0.0;
    float s2 = s * s;
    return intensity * ((1 - s2) * (1 - s2)) / (1 + falloff * s2);
}

void main() {
    vec3 nNormal = (vNormal.xyz + vec3(1.0f, 1.0f, 1.0f)) / 2.0f;
    FragNormal = vec4(nNormal, 0);

    vec3 N = normalize(vNormal.xyz);
    vec3 V = normalize(camera_pos.xyz - vWorldPos.xyz);

    vec3 light = vec3(0.0f);
    {
        vec3 L = normalize(-dir_light.dir_and_intensity.xyz);
        light += (calc_lighting(N, V, L, vMaterialData.x, vMaterialData.y, vColor.rgb) * dir_light.dir_and_intensity.w);
    }
    for (uint i = 0; i < LIGHT_COUNT; i++) {
        PointLight point_light = point_lights[i];
        vec3 L = normalize(point_light.pos_and_radius.xyz - vWorldPos.xyz);
        float distance = length(point_light.pos_and_radius.xyz - vWorldPos.xyz);
        float attenuation = PointLightAttenuation(distance, point_light.pos_and_radius.w, point_light.properties);
        light += (calc_lighting(N, V, L, vMaterialData.x, vMaterialData.y, vColor.rgb) * attenuation);
    }

#if defined(TEXTURED)
    FragColor = vec4(texture(Texture, vUV).rgb * light, 1.0f);
#else
    FragColor = vec4(vColor.rgb * light, 1.0f);
#endif
}
