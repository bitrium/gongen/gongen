#if defined(VERTEX_SHADER)

layout(location = 0) out vec2 vUv;

void main() {
  vUv = vec2((gl_VertexID << 1) & 2, gl_VertexID & 2);
  gl_Position = vec4(vUv * 2.0f + -1.0f, 0.0f, 1.0f);
}
#endif //VERTEX_SHADER
#if defined(FRAGMENT_SHADER)
layout (location = 0) out vec4 FragColor;

layout(binding = 0) uniform sampler2D Texture;

layout(location = 0) in vec2 vUv;

layout (std140, binding = 1) uniform Hdr2LdrUniformBuffer {
    float gamma;
    float exposure;
};

void main() {
    vec3 hdrColor = texture(Texture, vUv).rgb;
    vec3 mapped = vec3(1.0) - exp(-hdrColor * exposure);
    mapped = pow(mapped, vec3(1.0 / gamma));
    FragColor = vec4(mapped, 1.0);
}
#endif //FRAGMENT_SHADER
