#include <gongen.hpp>
#include <gongen/loaders/font.hpp>

void init_gui(gen::window::Window* window, gen::gl::Device& device);
void draw_gui(gen::gl::Device& device);

int main(int argc, char** argv) {
	gen::init();

	gen::log::Thread log_thread;
	log_thread.add_handler(new gen::log::ConsoleHandler());

	gen::window::Window* window = gen::window::create("The Window::window::window", gen::vec2u(800, 600), 0);
	window->input();

	gen::gl::Device gl(window, true);
	init_gui(window, gl);

	gen::loaders::Font font;
	font.open(new gen::File("data/test.genfont"), true);
	gen::gl::Texture font_texture = font.build(&gl, 1024);

	gen::loaders::Font::Glyph* A_glyph = font.get_glyph(65);

	gen::event::Hopper* hopper = gen::event::hopper();
	while (window->is_open()) {
		window->poll_events();
		hopper->poll();

		draw_gui(gl);

		hopper->clear();
	}

	return 0;
}
