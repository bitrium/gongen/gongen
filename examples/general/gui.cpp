#include <gongen/gui.hpp>

gen::SharedPtr<gen::gui::Context> gui;

void init_gui(gen::window::Window* window, gen::gl::Device& device) {
	gui = new gen::gui::Context(window, gen::rect2d(0.0, 0.0, window->get_size().x, window->get_size().y), new gen::gui::DefaultMesher());
	gui->setup_gl(device);
}
void draw_gui(gen::gl::Device& device) {
	gui->update();
	gen::gui::Block root = gui->root();
	if (root.button("abc") == gen::gui::ButtonState::LEFT_CLICKED) {
		printf("Button clicked!\n");
	}
	gui->draw(device);
}
