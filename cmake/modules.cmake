# SPDX-License-Identifier: BSD-3-Clause
# Copyright (c) 2024 GongEn Contributors

if (${gongen_platform} STREQUAL "linux")
	set(_platform_api_source_dir "src/common/platform/unix" "src/common/platform/unix/linux")
	set(_audio_deps pipewire)
elseif (${gongen_platform} STREQUAL "windows")
	set(_platform_api_source_dir "src/common/platform/win32")
	set(_audio_deps)
endif ()

_gongen_add_module(common
	SOURCE_DIRS "src/common" "src/common/platform" "src/common/math" "src/common/containers" "src/common/random" ${_platform_api_source_dir}
	DEPS xxhash lfqueue stb miniz bearssl
)
_gongen_add_module(window
	SOURCE_DIRS "src/window"
	DEPS glfw
	REQUIRE common
)
_gongen_add_module(gsf
	SOURCE_DIRS "src/gsf"
	DEPS gsf
	REQUIRE common
)
_gongen_add_module(audio
	SOURCE_DIRS "src/audio"
	DEPS ${audio_deps}
	REQUIRE common
)
_gongen_add_module(gl
	SOURCE_DIRS "src/gl"
	DEPS glad
	REQUIRE common window
)
_gongen_add_module(gui
	SOURCE_DIRS "src/gui"
	REQUIRE gl
)
_gongen_add_module(imgui
	SOURCE_FILES "src/imgui.cpp"
	DEPS imgui
)
_gongen_add_module(loaders_core
	SOURCE_FILES "src/loaders/audio/common.cpp" "src/loaders/image/common.cpp" "src/loaders/mesh/common.cpp"
	REQUIRE common
)
_gongen_add_module(loader_stb
	SOURCE_FILES "src/loaders/image/stb.cpp"
	DEPS stb
	REQUIRE common loaders_core
)
_gongen_add_module(loader_opus
	SOURCE_FILES "src/loaders/audio/opus.cpp"
	DEPS ogg opus opusfile
	REQUIRE common loaders_core
)
_gongen_add_module(loader_ply
	SOURCE_FILES "src/loaders/mesh/ply.cpp"
	REQUIRE common loaders_core
)
_gongen_add_module(loader_gfnt
	SOURCE_FILES "src/loaders/gfnt.cpp"
	REQUIRE common loaders_core
)
_gongen_add_module(loader_gltf
	SOURCE_FILES "src/loaders/mesh/gltf.cpp"
	DEPS cjson
	REQUIRE common loaders_core
)
_gongen_add_module(ecs
	SOURCE_DIRS "src/ecs"
	REQUIRE common
)
_gongen_add_module(resources
	SOURCE_DIRS "src/resources"
	REQUIRE common
)
_gongen_add_module(sound
	SOURCE_DIRS "src/sound"
	REQUIRE common audio resources loaders_core
)
_gongen_add_module(net
	SOURCE_DIRS_REC "src/net"
	REQUIRE common
)
_gongen_add_module(match
	SOURCE_DIRS_REC "src/match"
	REQUIRE common net
)
