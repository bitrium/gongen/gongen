# Style Guidelines

## Global guidelines

Every file shall begin with the *legal boilerplate*. The boilerplate is commented according to the programming language.

For C++ files, use:
```
// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors
```

All files should use Unix newlines (LF). Every file should end with a single newline character.

## Python guidelines

Python code should adapt the [PEP 8 style guidelines](https://peps.python.org/pep-0008/).

## C++ guidelines

Tabs are used for indentation. Contents of namespaces are not indented.

Classes, structures and enumerations are written using `CamelCase` and definitions – `SCREAMING_SNAKE_CASE`. All the other identifiers use `snake_case`. Preferably, these casing styles should overwrite shortcuts.

Use guard-ifs. Else if's should not use line breaks between `}` and `{`.

Utility classes must allow uninitialized state (default constructor).

Use the `override` keyword for overriden functions.
