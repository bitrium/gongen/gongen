#!/usr/bin/env bash
python3 -m pip install --editable . --config-settings editable_mode=strict --break-system-packages
