# SPDX-License-Identifier: BSD-3-Clause
# Copyright (c) 2024 GongEn Contributors

from pathlib import Path
from typing import Callable
from shutil import copyfile
from .dependency import Dependency
from ..utils import free_path, hash_file
import platform

class DependencyManager:
    def __init__(self, directory: Path, engine_dir: Path) -> None:
        directory.mkdir(exist_ok=True)
        self._dir = directory
        self._deps: dict[str, Dependency] = {}
        self._engine_dir = engine_dir

    @property
    def list(self) -> list[Dependency]:
        return list(self._deps.values())

    # def add(self, config: DependencyConfig) -> Dependency:
    #     self._deps[config.name] = Dependency(self._dir / config.name, config)
    #     return self._deps[config.name]

    def remove(self, name: str) -> None:
        del self._deps[name]

    def delete(self, name: str, on_unknown: Callable[[Path], bool] | bool = False) -> None:
        # TODO check hash and delete sources
        self.remove(name)

    # empty the manager from any loaded dependencies
    def clear(self) -> None:
        self._deps.clear()

    def reload(self) -> None:
        self.clear()
        self.load()

    # remove all dependencies and clear the dependency directory
    def reset(self, on_unknown: Callable[[Path], bool] | bool = False) -> None:
        self.reload()
        # TODO delete all loaded deps while checking hashes
        # TODO delete remaining files
        self.clear()

    # load all dependencies from dependency declarations
    def bring(self, directory: Path | None) -> None:
        if directory is None:
            directory = self._engine_dir / 'deps'
            if not directory.exists():
                raise FileNotFoundError('Engine sources does not provide dependency files.')
        for declaration in directory.iterdir():
            if declaration.is_dir() or declaration.suffix != '.gta':
                continue
            filename = declaration.stem
            if((".linux_only" in filename) and (platform.system() != "Linux")): continue
            if((".windows_only" in filename) and (platform.system() != "Windows")): continue
            filename = filename.replace('.linux_only', '')
            filename = filename.replace('.windows_only', '')
            dep_dir = self._dir / filename
            dep_dir.mkdir(exist_ok=True)
            dep_config = dep_dir / 'config.gta'
            if dep_config.exists():
                if hash_file(declaration) == hash_file(dep_config):
                    continue
                free_path(dep_config)
            copyfile(declaration, dep_config, follow_symlinks=True)

    def load_one(self, name: str) -> Dependency:
        path = self._dir / name
        if not path.exists() or not path.is_dir():
            raise FileNotFoundError(f'Dependency "{name}" not found.')
        self._deps[name] = Dependency.load(path, self._engine_dir)
        return self._deps[name]

    # load all dependencies from the dependency directory
    def load(self) -> None:
        for path in self._dir.iterdir():
            if not path.is_dir():
                continue
            self._deps[path.name] = Dependency.load(path, self._engine_dir)

    def obtain_all(self) -> None:
        for dep in self._deps.values():
            dep.obtain()
