# SPDX-License-Identifier: BSD-3-Clause
# Copyright (c) 2024 GongEn Contributors

from __future__ import annotations
from sys import executable as python_exec
from dataclasses import dataclass
from copy import deepcopy
from pathlib import Path
from os import remove as rm_file
from shutil import rmtree
import re
import gongsf
from ..utils import (
    free_path, gsf_schema, gsf_write, gsf_read, hash_file, hash_dir,
    version_compare, version_allowed, run_command, Toolset, BuildProduct
)
from .obtaining import InstalledObtainer, LocalObtainer, Obtainer, GitObtainer, PkgConfigObtainer
from .building import Builder, CompileBuilder, CmakeBuilder, SourceInjector, LibLinker

# TODO manual obtainer choice
# TODO multiple instances of one dependency (e.g. using different obtainers)
class Dependency:
    def __init__(self, directory: Path, config: DependencyConfig, engine_dir: Path) -> None:
        self._dir = directory
        self._src_path = self._dir / 'sources'
        self._config_file = self._dir / 'config.gta'
        self._cache_file = self._dir / 'cache.gta'
        self._config = config
        self._cache: DependencyCache | None = None
        self._obtainer: Obtainer | None = None
        self._obtainer_idx = -1
        self._version: str | None = None
        self._engine_dir = engine_dir

    @property
    def directory(self):
        return self._dir

    @classmethod
    def load(cls, directory: Path, engine_dir: Path) -> Dependency:
        config_path = directory / 'config.gta'
        config = DependencyConfig.load(config_path)
        dependency = Dependency(directory, config, engine_dir)
        cache_path = directory / 'cache.gta'
        used_cache = False
        if cache_path.exists():
            cache = DependencyCache.load(cache_path)
            used_cache, keep_sources = dependency._with_cache(cache)
            if not keep_sources and dependency._src_path.exists():
                if not dependency._src_path.is_symlink():
                    rmtree(dependency._src_path)
                else:
                    dependency._src_path.unlink()
        if not used_cache:
            dependency.cleanup()
        return dependency

    @property
    def name(self) -> str:
        return self._config.name

    @property
    def version(self) -> str | None:
        return self._version

    @property
    def version_spec(self) -> str | None:
        return self._config.version

    @property
    def is_obtained(self) -> bool:
        return self._obtainer is not None

    @property
    def public(self) -> bool:
        return self._config.public

    @property
    def cache(self) -> DependencyCache | None:
        if self._cache is not None:
            return self._cache
        if not self._config_file.exists() or not self._src_path.exists():
            return None
        self._cache = DependencyCache(
            hash_dir(self._src_path, self._config.hash_ignore),
            hash_file(self._config_file), self._version, self._obtainer_idx
        )
        return self._cache

    def _update_cache(self) -> None:
        self._cache = None
        if not self.cache:
            if self._cache_file.exists():
                rm_file(self._cache_file)
            return
        DependencyCache.save(self._cache_file, self.cache)

    def cleanup(self) -> None:
        free_path(self._src_path)
        self._update_cache()

    def _with_cache(self, cache: DependencyCache) -> tuple[bool, bool]:
        if not self.cache:
            return False, False
        if self.cache.sources_hash != cache.sources_hash:
            return False, True
        if self.cache.config_hash != cache.config_hash:
            return False, False
        self._obtainer = deepcopy(self._config.obtainers[cache.obtainer])
        self._obtainer_idx = cache.obtainer
        self._version = cache.version
        self._update_cache()
        return True, True

    def obtain(self) -> None:
        if self._obtainer:
            assert self.cache is not None
            version = self._obtainer.version(self.version_spec)
            if not version or version and (
                not self.cache.version or (
                    version_allowed(self.cache.version, self.version_spec) and
                    version_compare(version, self.cache.version) <= 0
                )
            ):
                print(f'reusing sources for "{self.name}"')
                return
            rmtree(self._src_path) # safe - cache's source hash matched
        print(f'obtaining sources for "{self.name}"...')
        self._obtainer_idx = 0
        for obtainer_preset in self._config.obtainers:
            self._obtainer = deepcopy(obtainer_preset)
            try:
                self._version = self._obtainer.version(self.version_spec)
                self._obtainer.obtain(self._src_path, self._version) # may raise
                break
            except Exception as exception:
                print(f'obtainer #{self._obtainer_idx} ({type(self._obtainer).__name__}) failed: {exception}')
                self._obtainer_idx += 1
                if self._src_path.exists():
                    rmtree(self._src_path)
        else:
            self._version = None
            self._obtainer = None
            error = f'All {self._obtainer_idx} obtainers for "{self.name}" failed.'
            self._obtainer_idx = -1
            raise RuntimeError(error)
        print(f'obtainer #{self._obtainer_idx} ({type(self._obtainer).__name__}) succeeded')
        self._update_cache()

    def reuse(self, build_dir: Path, platform: str) -> BuildProduct | None:
        # obtainer preparation
        product = BuildProduct(self.name, platform)
        product.public = self._config.public
        if self._obtainer != None:
            if not self._obtainer.prepare(self._src_path, product):
                raise RuntimeError(f'Could not prepare dependency {self.name} (unsupported platform?).')
        if not product.pending_build:
            return product
        # try to use cached build
        cache_path = build_dir.with_suffix('.gta')
        if not cache_path.exists():
            return None
        cache = DependencyBuild.load(cache_path)
        binaries_hash = hash_dir(build_dir, []) if build_dir.exists() else ''
        if (
            # self.cache is not None and
            # self.cache.sources_hash == cache.sources_hash and
            binaries_hash == cache.binaries_hash
        ):
            if cache.builder == -1:
                return product
            builder = self._config.builders[cache.builder]
            try:
                builder.apply(self.name, self._src_path, build_dir, product) # may raise
                return product
            except Exception as exception:
                print(f'failed to reuse builder #{cache.builder} ({type(builder).__name__}): {exception}')

    def build(self, build_dir: Path, toolset: Toolset, platform: str) -> BuildProduct:
        product = self.reuse(build_dir, platform)
        if product:
            return product
        product = BuildProduct(self.name, platform)
        if self._obtainer is None:
            raise RuntimeError(f'Cannot build an unobtained dependency {self.name}.')
        build_dir.mkdir(parents=True, exist_ok=True)
        print(f'building binaries for "{self.name}"...')
        # custom preparation commands
        cmd_regex = re.compile(r'<([a-z]+):([a-z]+)>')
        cmd_queries = {
            'prog': {
                'python': python_exec,
                'cc': toolset.c_compiler,
                'c++': toolset.cpp_compiler,
                'ar': toolset.archiver,
            },
            'dir': {
                'src': str(self._src_path.absolute()),
                'dst': str(build_dir.absolute()),
                'engine': str(self._engine_dir.absolute()),
            },
        }
        for platforms, in_bin, cmd in self._config.prepare:
            if platforms and platform not in platforms:
                continue
            cmd = cmd.replace('\n', ' ').strip()
            while match := re.search(cmd_regex, cmd):
                query, key = match.groups()
                if query not in cmd_queries:
                    raise RuntimeError(f'Unknown query type "{query}"'
                                       ' of a preparation command.')
                results = cmd_queries[query]
                if key not in results:
                    raise RuntimeError(f'Unknown key "{key}" in "{query}"'
                                       ' of a preparation command.')
                result = results[key]
                if result is None:
                    raise RuntimeError(f'Key "{key}" in "{query}"'
                                       ' is unavailable.')
                cmd = cmd[:match.start()] + result + cmd[match.end():]
            if run_command(cmd, working_dir=build_dir if in_bin else self._src_path) is None:
                raise RuntimeError('Preparation command failed.')
        # proper building
        builder_idx = 0
        builder: Builder | None = None
        for builder in self._config.builders:
            try:
                builder.build(
                    toolset, self.name, self._src_path, build_dir, product, 
                ) # may raise
                break
            except Exception as exception:
                print(f'builder #{builder_idx} ({type(builder).__name__}) failed: {exception}')
                builder_idx += 1
                if self._src_path.exists():
                    rmtree(self._src_path)
        else:
            if builder_idx > 0:
                raise RuntimeError(f'All {builder_idx} builders for "{self.name}" failed.')
        if builder is not None:
            builder.apply(self.name, self._src_path, build_dir, product)
            print(f'builder #{builder_idx} ({type(builder).__name__}) succeeded')
        else:
            builder_idx = -1
        # delete directory if empty
        try:
            next(build_dir.iterdir())
        except StopIteration:
            build_dir.rmdir()
        # cache build
        cache_path = build_dir.with_suffix('.gta')
        if self.cache:
            binaries_hash = hash_dir(build_dir, []) if build_dir.exists() else ''
            cache = DependencyBuild(self.cache.sources_hash, binaries_hash, builder_idx)
            DependencyBuild.save(cache_path, cache)
        return product

@dataclass
class DependencyConfig:
    name: str
    version: str | None
    public: bool
    hash_ignore: list[Path]
    obtainers: list[Obtainer]
    prepare: list[tuple[set[str], bool, str]] # (platforms, in_bin, cmd)
    builders: list[Builder]

    @classmethod
    def from_gsf(cls, name: str, gsf: gongsf.Object) -> DependencyConfig:
        version: str | None = None
        if gsf['version'].type.is_string():
            version = gsf['version'].string
        public = bool(gsf['public'].u8)
        hash_ignore = [Path(pattern) for pattern in gsf['hash_ignore'].decode_list().string]
        obtainers: list[Obtainer] = []
        for obtainer_gsf in gsf['obtain'].decode_list().object:
            if obtainer_gsf.get_class().name == 'GitObtainer':
                repo = obtainer_gsf['repo'].string
                commit: str | None = None
                if obtainer_gsf['commit'].type.is_string():
                    commit = obtainer_gsf['commit'].string
                headers: list[str] = []
                headers_gsfv = obtainer_gsf['headers']
                if headers_gsfv.type.is_string():
                    headers.append(headers_gsfv.string)
                elif headers_gsfv.type.is_list():
                    headers.extend(headers_gsfv.decode_list().string)
                ver_prefix = obtainer_gsf['ver_prefix'].string
                obtainers.append(GitObtainer(repo, commit, headers, ver_prefix))
            elif obtainer_gsf.get_class().name == 'LocalObtainer':
                env = obtainer_gsf['env'].string
                headers: list[str] = []
                headers_gsfv = obtainer_gsf['headers']
                if headers_gsfv.type.is_string():
                    headers.append(headers_gsfv.string)
                elif headers_gsfv.type.is_list():
                    headers.extend(headers_gsfv.decode_list().string)
                obtainers.append(LocalObtainer(env, headers))
            elif obtainer_gsf.get_class().name == 'InstalledObtainer':
                platforms: list[str] = []
                platforms_gsfv = obtainer_gsf['platforms']
                if platforms_gsfv.type.is_list():
                    platforms.extend(platforms_gsfv.decode_list().string)
                headers_dir: str | None = None
                if obtainer_gsf['headers'].type.is_string():
                    headers_dir = obtainer_gsf['headers'].string
                library = obtainer_gsf['library'].string
                obtainers.append(InstalledObtainer(platforms, headers_dir, library))
            elif obtainer_gsf.get_class().name == 'PkgConfigObtainer':
                platforms: list[str] = []
                platforms_gsfv = obtainer_gsf['platforms']
                if platforms_gsfv.type.is_list():
                    platforms.extend(platforms_gsfv.decode_list().string)
                module_name = obtainer_gsf['name'].string
                obtainers.append(PkgConfigObtainer(platforms, module_name))
            else:
                raise RuntimeError(f'Unsupported obtainer "{obtainer_gsf.get_class().name}".')
        prepare: list[tuple[set[str], bool, str]] = []
        for command_gsfv in gsf['prepare'].decode_list():
            if command_gsfv.type.is_string():
                prepare.append((set(), True, command_gsfv.string))
            else:
                command_gsf = command_gsfv.decode_object()
                cmd_platforms = set(command_gsf['platforms'].decode_list().string)
                in_bin = bool(command_gsf['in_bin'].u8)
                command = command_gsf['sh'].string
                prepare.append((cmd_platforms, in_bin, command))
        builders: list[Builder] = []
        for builder_gsf in gsf['build'].decode_list().object:
            if builder_gsf.get_class().name == 'CompileBuilder':
                sources = list(builder_gsf['sources'].decode_list().string)
                static = bool(builder_gsf['static'].u8)
                headers: list[str] = []
                headers_gsfv = builder_gsf['headers']
                if headers_gsfv.type.is_list():
                    headers.extend(headers_gsfv.decode_list().string)
                elif headers_gsfv.type.is_string():
                    headers.append(headers_gsfv.string)
                use_c = bool(builder_gsf['use_c'].u8)
                c_flags = list(builder_gsf['c_flags'].decode_list().string)
                l_flags = list(builder_gsf['l_flags'].decode_list().string)
                builders.append(CompileBuilder(sources, static, headers, use_c, c_flags, l_flags))
            elif builder_gsf.get_class().name == 'CMakeBuilder':
                target = builder_gsf['target'].string
                sub_project: str | None = None
                if builder_gsf['sub_project'].type.is_string():
                    sub_project = builder_gsf['sub_project'].string
                headers: list[str] = []
                headers_gsfv = builder_gsf['headers']
                if headers_gsfv.type.is_list():
                    headers.extend(headers_gsfv.decode_list().string)
                elif headers_gsfv.type.is_string():
                    headers.append(headers_gsfv.string)
                arguments = list(builder_gsf['arguments'].decode_list().string)
                library: str | None = None
                product_names: list[str] | None = None
                if builder_gsf['library'].type.is_string():
                    library = builder_gsf['library'].string
                if builder_gsf['product_names'].type.is_list():
                    product_names = list(builder_gsf['product_names'].decode_list().string)
                builders.append(CmakeBuilder(target, sub_project, headers, arguments, library, product_names))
            elif builder_gsf.get_class().name == 'SourceInjector':
                sources = list(builder_gsf['sources'].decode_list().string)
                headers: list[str] = []
                headers_gsfv = builder_gsf['headers']
                if headers_gsfv.type.is_list():
                    headers.extend(headers_gsfv.decode_list().string)
                elif headers_gsfv.type.is_string():
                    headers.append(headers_gsfv.string)
                defines = dict(builder_gsf['defines'].decode_map().string)
                from_bin = bool(builder_gsf['from_bin'].u8)
                own_target = bool(builder_gsf['own_target'].u8)
                use_c = bool(builder_gsf['use_c'].u8)
                builders.append(SourceInjector(sources, headers, defines, from_bin, own_target, use_c))
            elif builder_gsf.get_class().name == 'LibLinker':
                library = builder_gsf['library'].string
                static = bool(builder_gsf['static'].u8)
                builders.append(LibLinker(library, static))
            else:
                raise RuntimeError(f'Unsupported builder "{builder_gsf.get_class().name}".')
        return DependencyConfig(name, version, public, hash_ignore, obtainers, prepare, builders)

    @classmethod
    def load(cls, file: Path) -> DependencyConfig:
        gsf = gsf_read(file, gsf_schema('dep_config')[1])
        config_gsf = gsf.root.decode_object()
        return cls.from_gsf(file.parent.stem, config_gsf)

@dataclass
class DependencyCache:
    sources_hash: str
    config_hash: str
    version: str | None
    obtainer: int

    @classmethod
    def save(cls, file: Path, cache: DependencyCache) -> None:
        gsf = gongsf.Article.new(gsf_schema('dep_cache')[1])
        cache_gsf = gsf.root.encode_object(None)
        cache_gsf['sources'].string = cache.sources_hash
        cache_gsf['config'].string = cache.config_hash
        if cache.version is not None:
            cache_gsf['version'].string = cache.version
        if cache.obtainer == -1:
            raise ValueError('Cannot save cache - obtainer not selected.')
        cache_gsf['obtainer'].u8 = cache.obtainer
        cache_gsf.apply_defaults()
        gsf_write(file, gsf)

    @classmethod
    def load(cls, file: Path) -> DependencyCache:
        gsf = gsf_read(file, gsf_schema('dep_cache')[1])
        cache_gsf = gsf.root.decode_object()
        sources_hash = cache_gsf['sources'].string
        config_hash = cache_gsf['config'].string
        version: str | None = None
        if cache_gsf['version'].type.is_string():
            version = cache_gsf['version'].string
        obtainer = cache_gsf['obtainer'].u8
        return DependencyCache(sources_hash, config_hash, version, obtainer)

@dataclass
class DependencyBuild:
    sources_hash: str
    binaries_hash: str
    builder: int

    @classmethod
    def save(cls, file: Path, cache: DependencyBuild) -> None:
        gsf = gongsf.Article.new(gsf_schema('dep_build')[1])
        cache_gsf = gsf.root.encode_object(None)
        cache_gsf['sources'].string = cache.sources_hash
        cache_gsf['binaries'].string = cache.binaries_hash
        cache_gsf['builder'].i8 = cache.builder
        cache_gsf.apply_defaults()
        gsf_write(file, gsf)

    @classmethod
    def load(cls, file: Path) -> DependencyBuild:
        gsf = gsf_read(file, gsf_schema('dep_build')[1])
        cache_gsf = gsf.root.decode_object()
        sources_hash = cache_gsf['sources'].string
        binaries_hash = cache_gsf['binaries'].string
        builder = cache_gsf['builder'].i8
        return DependencyBuild(sources_hash, binaries_hash, builder)
