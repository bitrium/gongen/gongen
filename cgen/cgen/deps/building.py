# SPDX-License-Identifier: BSD-3-Clause
# Copyright (c) 2024 GongEn Contributors

from pathlib import Path
from abc import ABC, abstractmethod
from ..utils import (
    file_pattern, Toolset, run_command, hash_source_filename,
    product_name, find_lib, BuildProduct
)
from os import getcwd, chdir
import platform

class Builder(ABC):
    @abstractmethod
    def build(
        self, toolset: Toolset, name: str, src_path: Path,
        destination: Path, product: BuildProduct
    ) -> None: ...

    @abstractmethod
    def apply(
        self, name: str, src_path: Path, bin_path: Path,
        product: BuildProduct,
    ) -> None: ...

class CompileBuilder(Builder):
    def __init__(
        self, sources: list[str], static: bool, include_dirs: list[str],
        use_c: bool, c_flags: list[str], l_flags: list[str],
    ) -> None:
        self._sources = list(sources)
        self._static = static
        self._include_dirs = list(include_dirs)
        self._use_c = use_c
        self._c_flags = set(c_flags)
        self._l_flags = set(l_flags)

    def build(
        self, toolset: Toolset, name: str, src_path: Path,
        destination: Path, product: BuildProduct,
    ) -> None:
        destination.mkdir(parents=True, exist_ok=True)
        compiler = toolset.c_compiler if self._use_c else toolset.cpp_compiler
        object_dir = destination.absolute() / 'obj'
        object_dir.mkdir(exist_ok=True)
        c_flags = set(self._c_flags)
        l_flags = set(self._l_flags)
        object_files: list[str] = []
        if not self._static:
            if product.platform == 'linux':
                c_flags.add('-fPIC')
            l_flags.add('-shared')
        for source in self._sources:
            for source_path in file_pattern(src_path.absolute() / source):
                cmd = compiler + ' '
                # TODO -L{product.include_dir}?
                cmd += ' '.join(c_flags)
                cmd += ' -c'
                cmd += f' "{source_path}"'
                object_path = object_dir / f'{hash_source_filename(source_path)}.o'
                cmd += f' -o "{object_path}"'
                if run_command(cmd, src_path, merge_out=True) is None:
                    raise RuntimeError('Failed to compile a dependency.')
                object_files.append(f'"{object_path.relative_to(object_dir)}"')
        #if platform.system() == "Windows":
        #    cmd = (toolset.archiver if self._static else compiler) + '" '
        #else:
        cmd = (toolset.archiver if self._static else compiler) + ' '
        cmd += ' '.join(l_flags)
        kind = 'lib' if self._static else 'dlib'
        path = destination.absolute() / product_name(name, kind, product.platform)
        cmd += ' rcs' if self._static else ' -o'
        cmd += f' "{path}" '
        cmd += ' '.join(object_files)
        if run_command(cmd, object_dir, merge_out=True) is None:
            raise RuntimeError('Failed to link a dependency.')

    def apply(
        self, name: str, src_path: Path, bin_path: Path,
        product: BuildProduct,
    ) -> None:
        kind = 'lib' if self._static else 'dlib'
        path = bin_path / product_name(name, kind, product.platform)
        (product.static_libs if self._static else product.shared_libs).append(path)
        for include_dir in self._include_dirs:
            product.include_dirs.append(bin_path / include_dir)
        return None

class CmakeBuilder(Builder):
    def __init__(
        self, target: str, sub_project: str | None, include_dirs: list[str],
        arguments: list[str], library: str | None, product_names: list[str] | None
    ) -> None:
        self._target = target
        self._sub_project = sub_project
        self._include_dirs = include_dirs
        self._args = list(arguments)
        self._library = library if library else target
        self._product_names = product_names

    def build(
        self, toolset: Toolset, name: str, src_path: Path,
        destination: Path, product: BuildProduct,
    ) -> None:
        destination.mkdir(parents=True, exist_ok=True)
        cmd = f'cmake -G Ninja -S {src_path} -B {destination}' \
              f' -DCMAKE_C_COMPILER={toolset.c_compiler}' \
              f' -DCMAKE_CXX_COMPILER={toolset.cpp_compiler}'
        for arg in self._args:
            cmd += ' ' + arg
        if run_command(cmd, merge_out=True) is None:
            raise RuntimeError('Failed to generate build file using CMake.')
        cmd = f'cmake --build {destination}'
        if self._target:
            cmd += f' --target {self._target}'
        if run_command(cmd, merge_out=True) is None:
            raise RuntimeError('Failed to compile with CMake-generated configs.')

    def apply(
        self, name: str, src_path: Path, bin_path: Path,
        product: BuildProduct,
    ) -> None:
        product_dir = bin_path
        if self._sub_project:
            product_dir /= self._sub_project
            
        result = None
        if self._product_names:
            for product_name in self._product_names:
                result = find_lib(product_dir, product_name)
                if result is not None:
                    break
        else:
            result = find_lib(product_dir, self._library)
        
        if not result:
            raise RuntimeError('Could not find product of CMake build.')
        product_file, static = result
        (product.static_libs if static else product.shared_libs).append(product_file)
        for include_dir in self._include_dirs:
            product.include_dirs.append(bin_path / include_dir)
        return None

class SourceInjector(Builder):
    def __init__(
        self, sources: list[str], include_dirs: list[str], defines: dict[str, str],
        from_bin: bool, own_target: bool, use_c: bool
    ) -> None:
        self._sources = list(sources)
        self._include_dirs = list(include_dirs)
        self._defines = dict(defines)
        self._from_bin = from_bin
        self._own_target = own_target
        self._use_c = use_c
        if self._use_c and not self._own_target:
            raise RuntimeError('Injected sources using C must be added as a separate target.')

    def build(
        self, toolset: Toolset, name: str, src_path: Path,
        destination: Path, product: BuildProduct,
    ) -> None:
        pass

    def apply(
        self, name: str, src_path: Path, bin_path: Path,
        product: BuildProduct,
    ) -> None:
        if self._own_target:
            # FIXME this was a child in ninja times but now it makes less sense and breaks cmake generation; reconsider
            # dep_product = product.child(name)
            dep_product = product
            dep_product.include_dirs.extend(product.include_dirs)
        else:
            dep_product = product
        for pattern in self._sources:
            dep_product.sources.extend(file_pattern((bin_path if self._from_bin else src_path) / pattern))
        for include_dir in self._include_dirs:
            dep_product.include_dirs.append(bin_path / include_dir)
        for macro_name, value in self._defines.items():
            dep_product.definitions.append((macro_name, value))
        dep_product.is_c = self._use_c

class LibLinker(Builder):
    def __init__(self, library: str, static: bool) -> None:
        self._library = library
        self._static = static

    def build(
            self, toolset: Toolset, name: str, src_path: Path,
            destination: Path, product: BuildProduct,
    ) -> None:
        pass

    def apply(
            self, name: str, src_path: Path, bin_path: Path,
            product: BuildProduct,
    ) -> None:
        result = find_lib(bin_path, self._library, None, self._static)
        if not result:
            raise RuntimeError('Could not find library requested.')
        product_file, static = result
        (product.static_libs if static else product.shared_libs).append(product_file)
