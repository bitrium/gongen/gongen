# SPDX-License-Identifier: BSD-3-Clause
# Copyright (c) 2024 GongEn Contributors

from os import environ
from platform import system
from abc import ABC, abstractmethod
from pathlib import Path
import re
from ..utils import run_command, version_select, version_allowed, BuildProduct


class Obtainer(ABC):
    @abstractmethod
    def version(self, spec: str | None) -> str | None:
        ...

    @abstractmethod
    def obtain(self, destination: Path, version: str | None) -> None:
        ...

    @abstractmethod
    def prepare(self, directory: Path, product: BuildProduct) -> bool:
        ...


GIT_SERVICES = {
    'gitlab': 'https://gitlab.com/',
    'github': 'https://github.com/',
}


def resolve_git(git: str) -> str:
    split = git.split(':')
    if len(split) == 1 or split[0] not in GIT_SERVICES:
        return git
    return GIT_SERVICES[split[0]] + ':'.join(split[1:])


class GitObtainer(Obtainer):  # TODO commit
    def __init__(
        self, repo: str, commit: str | None, include_dirs: list[str],
        version_prefix: str
    ) -> None:
        self._url = resolve_git(repo)
        self._ref = commit
        self._include_dirs = list(include_dirs)
        self._version_prefix = version_prefix

    def version(self, spec: str | None) -> str | None:
        if self._ref:
            return None
        result = run_command(
            f'git ls-remote -t {self._url}', merge_out=True
        )
        if result is None:
            raise RuntimeError(f'Could not access git remote {self._url}.')
        regex = r'refs/tags/' + self._version_prefix + r'([0-9A-Za-z.\-_]+)'
        tag_names = re.findall(regex, result)
        version = version_select(tag_names, spec)
        if not version:
            raise RuntimeError('No matching version available in the git remote.')
        return version

    def obtain(self, destination: Path, version: str | None) -> None:
        destination.mkdir(parents=True, exist_ok=True)
        if run_command(f'git init', working_dir=destination, merge_out=True) is None:
            raise RuntimeError('Could not init a git repo.')
        ref = f'refs/tags/{self._version_prefix}{version}' if not self._ref else self._ref
        cmd = f'git fetch --depth=1 {self._url} {ref}'
        if run_command(cmd, working_dir=destination, merge_out=True) is None:
            raise RuntimeError('Could not init a git repo.')
        cmd = 'git -c advice.detachedHead=false checkout FETCH_HEAD'
        if run_command(cmd, working_dir=destination, merge_out=True) is None:
            raise RuntimeError('Could not checkout the downloaded git ref.')

    def prepare(self, directory: Path, product: BuildProduct) -> bool:
        for include_dir in self._include_dirs:
            product.include_dirs.append(directory / include_dir)
        return True


class LocalObtainer(Obtainer):
    def __init__(self, env: str, include_dirs: list[str]) -> None:
        self._env = env
        self._include_dirs = list(include_dirs)

    def version(self, spec: str | None) -> str | None:
        return None

    def obtain(self, destination: Path, version: str | None) -> None:
        destination.symlink_to(environ[self._env], target_is_directory=True)
        # Path(environ[self._env]).symlink_to(destination, target_is_directory=True)

    def prepare(self, directory: Path, product: BuildProduct) -> bool:
        for include_dir in self._include_dirs:
            product.include_dirs.append(directory / include_dir)
        return True


class InstalledObtainer(Obtainer):
    def __init__(self, platforms: list[str], include_dir: str | None,
                 library: str) -> None:
        self._platforms = platforms
        self._include_dirs = include_dir
        self._library = library

    def version(self, spec: str | None) -> str | None:
        raise NotImplementedError('not implemented')

    def obtain(self, destination: Path, version: str | None) -> None:
        raise NotImplementedError('not implemented')

    def prepare(self, directory: Path, product: BuildProduct) -> bool:
        raise NotImplementedError('not implemented')


class PkgConfigObtainer(Obtainer):
    def __init__(self, platforms: list[str], name: str) -> None:
        current_platform = system()
        if not current_platform in platforms:
            self.wrong_platform = True
        else:
            self.wrong_platform = False
        self._platforms = platforms
        self._name = name

    def version(self, spec: str | None) -> str | None:
        if run_command(f'pkg-config --exists {self._name}') is None:
            raise RuntimeError('PkgConf does not contain this module.')
        result = run_command(f'pkg-config --modversion {self._name}')
        if not result:
            return None
        result = result.strip()
        if not version_allowed(result, spec):
            raise RuntimeError('PkgConf version of the module is not allowed.')
        return result

    def obtain(self, destination: Path, version: str | None) -> None:
        # FIXME this is only needed because cache usage depends on src dir existance
        destination.mkdir(parents=True, exist_ok=True)

    def prepare(self, directory: Path, product: BuildProduct) -> bool:
        if product.platform not in self._platforms:
            return False
        c_flags = run_command(f'pkg-config --cflags {self._name}')
        l_flags = run_command(f'pkg-config --libs {self._name}')
        if c_flags is None or l_flags is None:
            raise RuntimeError('Failed to obtain pkg-config info.')
        for flag in c_flags.split(' '):
            flag = flag.strip()
            if flag.startswith('-I'):
                product.include_dirs.append(Path(flag[2:]))
            else:
                product.c_flags.append(flag.strip())
        for flag in l_flags.split(' '):
            flag = flag.strip()
            if flag.startswith('-l'):
                product.shared_libs.append(Path(flag[2:]))
            else:
                product.l_flags.append(flag.strip())
        product.pending_build = False
        return True
