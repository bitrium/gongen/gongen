# SPDX-License-Identifier: BSD-3-Clause
# Copyright (c) 2024 GongEn Contributors

from __future__ import annotations
from dataclasses import dataclass
from os.path import relpath
from threading import Thread
from queue import Queue
from typing import Generator, Iterable, Any
from subprocess import Popen as subprocess, PIPE, STDOUT
from hashlib import sha1
from pathlib import Path
from importlib import resources
from string import digits as DIGITS
import gongsf as gsf
from . import schemas

@dataclass
class Toolset:
    c_compiler: str = 'clang'
    cpp_compiler: str = 'clang++'
    archiver: str = 'llvm-ar'


class BuildProduct:
    def __init__(self, name: str, platform: str) -> None:
        self.pending_build = True
        self._name = name
        self._platform = platform
        self._children: dict[str, BuildProduct] = {}
        self.c_flags: list[str] = []
        self.l_flags: list[str] = []
        self.static_libs: list[Path] = []
        self.shared_libs: list[Path] = []
        self.include_dirs: list[Path] = []
        self.definitions: list[tuple[str, str]] = []
        self.sources: list[Path] = []
        self.is_c = False
        self.public = False

    @property
    def name(self) -> str:
        return self._name

    @property
    def platform(self) -> str:
        return self._platform

    @property
    def children(self) -> dict[str, BuildProduct]:
        return dict(self._children)

    def child(self, name: str) -> BuildProduct:
        child = BuildProduct(name, self.platform)
        self._children[name] = child
        return child

    def inherit(self, dep: BuildProduct) -> None:
        self.include_dirs.extend(dep.include_dirs)


def wd_path(absolute: Path):
    # TODO if two or more '..' levels, return absolute
    try:
        return relpath(absolute)
    except:
        return absolute


def hash_data(*values: str | bytes | Any) -> str:
    result = sha1()
    for value in values:
        if not isinstance(value, bytes):
            value = repr(value)
        if isinstance(value, str):
            value = value.encode('utf-8')
        result.update(value)
    return result.hexdigest()

def hash_data_int(*values: str | bytes | Any) -> int:
    return int.from_bytes(hash_data(*values).encode('utf-8'), byteorder='big')

def hash_source_filename(file: Path) -> str:
    return (file.stem + '_' + hash_data(file))[:20]


def run_command(
    cmd: str, working_dir: Path | None = None, merge_out: bool = False,
    verbose: bool = True, timeout: int = 20
) -> str | None:
    if not working_dir:
        print(f'running "{cmd}"...')
    else:
        print(f'running "{cmd}" in "{working_dir}"...')
    error = []
    output = ''
    with subprocess(
        cmd, cwd=working_dir, bufsize=1, universal_newlines=True,
        stdout=PIPE, stderr=STDOUT if merge_out else PIPE, shell=True
    ) as process:
        assert process.stdout
        out_que = Queue()
        out_collector = Thread(
            target=lambda queue, stream: queue.put(stream.read()),
            args=(out_que, process.stdout)
        )
        if not merge_out:
            stream = process.stderr
            out_collector.start()
        else:
            stream = process.stdout
        assert stream
        last_len = 0
        for line in stream:
            line = line[:-1]
            current_len = len(line)
            if last_len > current_len:
                line += ' ' * (last_len - current_len)
            error.append(line)
            print('| ' + line, end='\r')
            last_len = current_len
        if not merge_out:
            out_collector.join()
            output = out_que.get()
    if merge_out:
        output = '\n'.join(error)
    if process.returncode != 0:
        if error:
            print(' ' * (last_len + 2), end='\r')
        for line in error:
            print('! ' + line)
        print('failed')
        return None
    if error:
        print()
    print('done')
    return output


def file_pattern(path: Path, missing_ok = False) -> Generator[Path, None, None]:
    if path.exists():
        # if path.is_dir():
        #     raise RuntimeError(f'Expected file at "{path}; got directory."')
        yield path
    else:
        pattern = path.name
        if not pattern.startswith('*.'):
            if missing_ok:
                return
            raise FileNotFoundError(f'File "{path}" does not exit.')
        path = path.parent
        pattern = pattern[1:]
        for file in path.iterdir():
            if file.is_dir() or not file.name.endswith(pattern):
                continue
            yield file


def hash_file(path: Path) -> str:
    if not path.exists:
        return ''
    file = open(path, 'rb')
    result = hash_data(file.read())
    file.close()
    return result

def hash_files(files: list[Path], hash_paths: bool = True) -> str:
    return hash_data(
        *([str(p) for p in files] if hash_paths else []),
        *[hash_file(path) for path in files]
    )

def hash_dir(directory: Path, ignore: list[Path], hash_paths: bool = True) -> str:
    hashes: list[str | Path] = []
    if hash_paths:
        hashes.append(directory)
    ignored_files: list[Path] = []
    for ignore_pattern in ignore:
        ignored_files.extend([directory / path for path in file_pattern(directory / ignore_pattern, True)])
    for child in directory.iterdir():
        if child in ignored_files:
            continue
        hashes.append(child)
        if child.is_dir():
            child_ignore: list[Path] = []
            for ignored_file in ignored_files:
                if not str(ignored_file).startswith(str(child)):
                    continue
                child_ignore.append(ignored_file.relative_to(child))
            hashes.append(hash_dir(child, child_ignore, hash_paths))
        else:
            hashes.append(hash_file(child))
    return hash_data(*hashes)


def free_path(path: Path) -> Path | None:
    if not path.exists():
        return None
    i = 0
    while (bkp_path := path.with_name(f'{path.stem}-old{i}{path.suffix}')).exists():
        i += 1
    path.rename(bkp_path)


_schemas: dict[str, tuple[gsf.Descriptor, gsf.Schema]] = {}

def gsf_schema(name: str) -> tuple[gsf.Descriptor, gsf.Schema]:
    if name in _schemas:
        return _schemas[name]
    file = (resources.files(schemas) / (name + '.gts')).open('r')
    text = file.read()
    file.close()
    result = gsf.codec.read_schema_txt(text)
    _schemas[name] = result
    return result

def gsf_write(path: Path, article: gsf.Article) -> None:
    txt = gsf.codec.write_article_txt(article)
    file = open(path, 'wt')
    file.write(txt)
    file.close()

def gsf_read(path: Path, schema: gsf.Schema) -> gsf.Article:
    file = open(path, 'rt')
    txt = file.read()
    file.close()
    return gsf.codec.read_article_txt(txt, schema)


def find_lib(directory: Path, name: str, version: str | None = None,
             static: bool | None = None) -> tuple[Path, bool] | None:
    if not directory.exists():
        return None
    for path in directory.iterdir():
        if not path.is_file() and not path.is_symlink():
            continue
        orig_path = path
        file = path.name
        if version and file.endswith(version):
            removed = len(version)
            if file.endswith('.'):
                removed += 1
            path = path.parent / file[:-removed]
            file = path.name
        is_static = False
        if static != True and (file.endswith('.so') or file.endswith('.dll')):
            file = path.stem
        if static != False and (file.endswith('.a') or file.endswith('.lib')):
            file = path.stem
            is_static = True
        if file.startswith('lib'):
            file = file[3:]
        if file == name:
            return orig_path, is_static
        if version and file.endswith(version):
            file = file[:-len(version)]
        if file == name:
            return orig_path, is_static
    return None


_EXEC_NAMES = {
    'linux': lambda name: name,
    'windows': lambda name: name + '.exe',
}

_LIB_NAMES = {
    'linux': lambda name: 'lib' + name.lower() + '.a',
    'windows': lambda name: name + '.lib',
}

_DLIB_NAMES = {
    'linux': lambda name: 'lib' + name.lower() + '.so',
    'windows': lambda name: name + '.dll',
}

_NAMES = {
    'exec': _EXEC_NAMES,
    'lib': _LIB_NAMES,
    'dlib': _DLIB_NAMES,
}


def product_name(name: str, kind: str, platform: str, version: str | None = None):
    return _NAMES[kind][platform](name) + (f'.{version}' if version else '')


def simplify_name(name: str) -> str:
    return name.replace(' ', '_').replace('-', '_').lower()

def version_allowed(version: str, spec: str | None) -> bool:
    # TODO swap '+' notation for expanded '>=' notation
    # TODO '^' notation for semver-compatible 'a.b+.c'
    if spec is None:
        return True
    spec = spec.strip()
    version = version.strip()
    if spec == '*' or version == spec:
        return True
    if spec.startswith('"') and spec.endswith('"'):
        return version == spec[1:-1]

    comparisons = {
        '<': lambda x: x < 0,
        '<=': lambda x: x <= 0,
        '=': lambda x: x == 0,
        '>=': lambda x: x >= 0,
        '>': lambda x: x > 0,
    }
    for comparison, comp_func in comparisons.items():
        if not spec.startswith(comparison):
            continue
        return comp_func(version_compare(version, spec[len(comparison):].lstrip()))

    # FIXME ugly code ahead
    comp_func = lambda version_num, spec_num: version_num == spec_num
    comp_func_times = -100
    version_split = version.split('-')
    spec_split = spec.split('-')
    for version_part, spec_part in zip(version_split, spec_split):
        version_segments = version_part.split('.')
        spec_segments = spec_part.split('.')
        for version_segment, spec_segment in zip(version_segments, spec_segments):
            if comp_func_times == 0:
                return True
            if spec_segment.rstrip().endswith('+'):
                comp_func = lambda version_num, spec_num: version_num >= spec_num
                comp_func_times = 1
                spec_segment = spec_segment[:-1]
            # elif spec_segment.rstrip().endswith('-'):
            #     if comp_func:
            #         return False
            #     comp_func = lambda version_num, spec_num: version_num <= spec_num
            #     spec_segment = spec_segment[:-1]
            version_number = version_segment[len(version_segment.rstrip(DIGITS)):]
            spec_number = spec_segment[len(spec_segment.rstrip(DIGITS)):]
            if len(version_number) == 0:
                if len(spec_number) == 0:
                    if not comp_func(len(version_segment), len(spec_segment)):
                        return False
                    comp_func_times -= 1
                    continue
                return False
            if not comp_func(int(version_number), int(spec_number)):
                return False
            if int(version_number) == int(spec_number):
                comp_func_times += 1
            comp_func_times -= 1
        if len(version_segments) != len(spec_segments):
            if not comp_func(len(version_segments), len(spec_segments)):
                return False
    if len(version_split) != len(spec_split):
        if not comp_func(len(version_split), len(spec_split)):
            return False
    return True

# semver compatible, but allows for variations
# returns zero if equal, negative if second is greater, positive if first is greater
def version_compare(first: str, second: str) -> int:
    # TODO ignore build number/metadata? (semver's and other '+' notations)
    first = first.strip()
    second = second.strip()
    if first == second:
        return 0
    first_split = first.split('-')
    second_split = second.split('-')
    for first_part, second_part in zip(first_split, second_split):
        first_segments = first_part.split('.')
        second_segments = second_part.split('.')
        for first_segment, second_segment in zip(first_segments, second_segments):
            first_numeric = first_segment.isnumeric()
            second_numeric = second_segment.isnumeric()
            if first_numeric and not second_numeric:
                return 1
            if not first_numeric and second_numeric:
                return -1
            numeric = first_numeric
            if numeric:
                if int(first_segment) != int(second_segment):
                    return int(first_segment) - int(second_segment)
            else:
                labels = ['alpha', 'beta', 'pre', 'rc']
                scores = []
                for segment in [first_segment, second_segment]:
                    for label_idx, label in enumerate(labels):
                        if segment.lower().startswith(label):
                            scores.append(label_idx + 1)
                            break
                    else:
                        scores.append(0)
                if scores[0] != scores[1]:
                    return scores[0] - scores[1]
                for i, segment in enumerate([first_segment, second_segment]):
                    suffix_number = ''
                    while segment[-1].isnumeric():
                        suffix_number += segment[-1]
                        segment = segment[:-1]
                    if not suffix_number:
                        continue
                    scores[i] += int(suffix_number)
                if scores[0] != scores[1]:
                    return scores[0] - scores[1]
        if len(first_segments) > len(second_segments):
            return 1
        if len(first_segments) < len(second_segments):
            return -1
    if len(first_split) > len(second_split):
        return 1
    if len(first_split) < len(second_split):
        return -1
    return 0

# returns greatest version supported by the specification
def version_select(versions: Iterable[str], spec: str | None) -> str | None:
    best: str | None = None
    for version in versions:
        if not version_allowed(version, spec):
            continue
        if best is None or version_compare(version, best) > 0:
            best = version
    return best
