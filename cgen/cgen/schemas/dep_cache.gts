group: cgen
name: dep-cache
version: 0.0
root: Cache
classes:
  Cache:
    sources: str / hash of the source files
    config: str / hash of the config
    version: !str|nul.nul = / sources version
    obtainer: u8 / idx of current obtainer
