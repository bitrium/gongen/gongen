group: cgen
name: dep-cache
version: 0.0
root: Build
classes:
  Build:
    sources: str / hash of the source files (from main dep's cache)
    binaries: str / hash of the binary files
    builder: i8 / idx of current builder
