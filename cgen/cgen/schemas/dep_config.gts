group: cgen
name: dep-config
version: 0.0
root: Dependency
classes:
  GitObtainer: # download sources from git
    repo: str / either git url or 'service:group/name' (e.g. gitlab:bitrium/gongen/gongen)
    commit: !str|nul.nul = / hash of the commit that should be used
    headers: !str|lst[str]|nul.str = include / relative paths of include directories
    ver_prefix: str = v / tag version prefix
  LocalObtainer: # localize sources
    env: str / environment variable containing the path to the given dependency sources
    headers: !str|lst[str]|nul.str = include / relative paths of include directories
  InstalledObtainer: # localize binary and headers
    platforms: !lst[str]|nul.nul = / supported platforms
    headers: !str|nul.nul = / path to the include directory or null if standard (e.g. '/usr/include')
    library: str / sole name of the library (without path, 'lib' or '.so')
  PkgConfigObtainer: # get pkg-config output
    platforms: !lst[str]|nul.nul = / supported platforms
    name: str / name of the pkg-config module

  Command:
    platforms: lst[str] = / list of platforms on which the commands hould be executed
    in_bin: u8 = 1 / whether to run the command in build directory
    sh: str / the executed command

  CompileBuilder: # build using a compiler directly
    sources: lst[str] / list of path patterns to source files
    static: u8 / whether to make a static library (1) or a shared one (0)
    headers: !str|lst[str]|nul.nul = / relative paths to build output include directories
    use_c: u8 = 0 / whether to use a c compiler (1) or a c++ one (0)
    c_flags: lst[str] = / compilation flags
    l_flags: lst[str] = / linking flags
  CMakeBuilder: # configure and build with cmake
    target: str / name of the cmake target to compile
    sub_project: !str|nul.nul = / a 'sub-project' added using 'add_subdirectory'
    headers: !str|lst[str]|nul.nul = / relative paths to build output include directories
    arguments: lst[str] = / cmake arguments
    library: !str|nul.nul = / library name if it differs from target name
    product_names: !lst[str]|nul.nul = / library names if it differs from target name(check one by one)
  SourceInjector: # add sources to build system instead of building
    sources: lst[str] / list of paths or path patterns to source files
    headers: !lst[str]|nul.nul = / relative paths to build output include directories
    defines: map[str] = / additional compile definitions
    from_bin: u8 = 0 / whether source paths are relative to source dir (0) or binary dir (1)
    own_target: u8 = 1 / whether it should create a separate (static lib) target
    use_c: u8 = 0 / whether to use a c compiler (1) or a c++ one (0)
  LibLinker:
    library: !str|nul.nul = / library name if it differs from target name
    static: u8 = 1 / whether the library is static (1) or shared (0)

  Dependency:
    version: !str|nul.nul = / version specification
    public: u8 = 0 / whether deps of targets using this dep should inherit it
    hash_ignore: list[str] = / patterns of source files to ignore during hashing
    obtain: lst[GitObtainer|LocalObtainer|InstalledObtainer|PkgConfigObtainer]
    prepare: lst[!str|Command] =
    build: lst[CMakeBuilder|CompileBuilder|SourceInjector|LibLinker] =
# TODO system_libs: lst[str] = # shared system libraries to add with this dep
