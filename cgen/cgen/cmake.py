# SPDX-License-Identifier: BSD-3-Clause
# Copyright (c) 2024 GongEn Contributors

from pathlib import Path
import platform
from .utils import BuildProduct

def generate_cmake(out_path: Path, products: list[BuildProduct]) -> None:
    file = open(out_path, 'wt')
    for product in products:
        line = 'gongen_add_dep(' + product.name
        if product.include_dirs:
            line += ' INCLUDE_DIRS ' + ' '.join([str(p.absolute().as_posix()) for p in product.include_dirs])
        if product.static_libs:
            line += ' STATIC_LIBS ' + ' '.join([str(p.absolute().as_posix()) for p in product.static_libs])
        if product.shared_libs:
            line += ' SHARED_LIBS ' + ' '.join([str(p.absolute().as_posix()) for p in product.shared_libs])
        if product.sources:
            line += ' INJECT_SOURCES ' + ' '.join([str(p.absolute().as_posix()) for p in product.sources])
        if product.public:
            line += ' PUBLIC'
        line += ')\n'
        if platform.system() == "Windows":
            line = line.replace(".dll", ".lib")
        file.write(line)
    file.close()
