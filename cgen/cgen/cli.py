# SPDX-License-Identifier: BSD-3-Clause
# Copyright (c) 2024 GongEn Contributors

from __future__ import annotations
from argparse import Namespace as Args
from pathlib import Path
from argparse import ArgumentParser
from platform import system as system_name
from .deps import DependencyManager
from .utils import Toolset, BuildProduct
from .cmake import generate_cmake


def parser() -> ArgumentParser:
    parser = ArgumentParser(
        description='GongEn Dependency Manager'
    )
    parser.add_argument(
        '-e', '--engine-dir', type=lambda s: Path(s), default='.',
        help='path of the engine project/repository directory'
    )
    parser.add_argument(
        '-p', '--cgen-dir', type=lambda s: Path(s), default='./.cgen',
        help='path of the cgen storage directory'
    )

    subparsers = parser.add_subparsers(
        title='subcommand', required=True, dest='dep_command',
        help='dependency manager command'
    )
    pull_parser = subparsers.add_parser(
        'pull', help='pull dependency configurations'
    )
    pull_parser.add_argument(
        'directory', type=str, nargs='?',
        help='directory containing pulled configs (empty for engine\'s dependencies)'
    )
    # TODO cgen deps pull --editable # for deps symlinks instead of copies
    obtain_parser = subparsers.add_parser(
        'obtain', help='download sources for dependencies'
    )
    obtain_parser.add_argument(
        'dependencies', nargs='*',
        help='dependencies to update; empty for all'
    )
    # TODO cgen deps obtian --only-required # for skipping optional updates
    build_parser = subparsers.add_parser(
        'build', help='build dependencies'
    )
    build_parser.add_argument(
        'dependencies', nargs='*',
        help='dependencies to build; empty for all'
    )
    build_parser.add_argument(
        '--c-compiler', type=str, nargs='?',
    )
    build_parser.add_argument(
        '--cpp-compiler', type=str, nargs='?',
    )
    build_parser.add_argument(
        '--archiver', type=str, nargs='?',
    )
    subparsers.add_parser(
        'list', help='show downloaded dependencies and their versions'
    )
    subparsers.add_parser(
        'cmake', help='generate dep-listng CMake file for GongEn\'s CMake'
    )

    return parser

def interactive_ask(question: str, default: str, return_default: bool = True) -> str:
    response = input(f'{question} [{default}]: ').strip()
    if not response and return_default:
        response = default
    return response

def interactive_bool(question: str, default: bool) -> bool:
    response = interactive_ask(question, 'yes' if default else 'no', False)
    if not response:
        return default
    return response.lower() == 'yes'


def execute_command(args: Args, platform: str) -> None:
    deps = DependencyManager(args.cgen_dir, args.engine_dir)
    if args.dep_command == 'pull':
        deps.bring(Path(args.directory) if args.directory is not None else None)
    elif args.dep_command == 'obtain':
        if args.dependencies:
            for dep in args.dependencies:
                deps.load_one(dep)
        else:
            deps.load()
        deps.obtain_all()
    elif args.dep_command == 'build':
        if args.dependencies:
            for dep in args.dependencies:
                deps.load_one(dep)
        else:
            deps.load()
        toolset = Toolset()
        if args.c_compiler:
            toolset.c_compiler = args.c_compiler
        if args.cpp_compiler:
            toolset.cpp_compiler = args.cpp_compiler
        if args.archiver:
            toolset.archiver = args.archiver
        for dep in deps.list:
            dep.build(dep.directory / 'build', toolset, platform)
    elif args.dep_command == 'list':
        deps.load()
        print(f'listing {len(deps.list)} deps:')
        for dep in deps.list:
            info = dep.version if dep.version else '(unversioned or version unknown)'
            if not dep.is_obtained:
                info = dep.version_spec if dep.version_spec else 'unversioned'
                info += ' (unobtained)'
            print(f'{dep.name}: {info}')
    elif args.dep_command == 'cmake':
        deps.load()
        products: list[BuildProduct] = []
        for dep in deps.list:
            product = dep.reuse(dep.directory / 'build', platform)
            if product:
                products.append(product)
        generate_cmake(args.cgen_dir / 'deps.cmake', products)

_PLATFORMS = {
    'Linux': 'linux',
    'Windows': 'windows',
}
def main() -> None:
    sys = system_name()
    if sys not in _PLATFORMS:
        raise EnvironmentError(f'Unsupported platform "{sys}".')
    platform = _PLATFORMS[sys]
    execute_command(parser().parse_args(), platform)
