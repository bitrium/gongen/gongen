import gdb
from contextlib import contextmanager
import re

class GenBufferPrinter:
    def __init__(self, value : gdb.Value):
        self.value = value

    def to_string(self):
        size = self.value["size_"]
        return f"size: {size}"

    def children(self):
        size = self.value["size_"]
        ptr = self.value["ptr_"]
        yield "size", size
        yield "capacity", self.value["capacity_"]
        yield "capacity_func", self.value["capacity_function_"]
        for i in range(size):
            yield str(i), ptr[i]

    def display_hint(self):
        return "buffer"

class GenDynamicArrayPrinter(GenBufferPrinter):
    def __init__(self, value : gdb.Value):
        super().__init__(value["buffer"])

class GenStringPrinter:
    def __init__(self, value : gdb.Value):
        self.value = value

    def is_sso(self):
        return self.value["size_"] | 1

    def get_str(self):
        if self.is_sso():
            return self.value["sso"].cast(gdb.lookup_type("char").pointer())
        else:
            return self.value["ptr_"]

    def to_string(self):
        return self.get_str()

    def children(self):
        if self.is_sso():
            yield "size", (self.value["sso_size"] / 2)
        else:
            yield "size", (self.value["size_"] / 2)
            yield "capacity", self.value["capacity_"]

    def display_hint(self):
        return "string"

class GenHashmapPrinter:
    def __init__(self, value : gdb.Value):
        self.value = value

    def to_string(self):
        size = self.value["size_"]
        return f"size: {size}"

    def children(self):
        size = self.value["size_"]
        yield "size", size
        yield "hash_func", self.value["hash"]
        # for i in range(size):
        #     yield str(i), ptr[i]
        bucket_count = int(re.match(r"[^0-9]*([0-9]+)>", self.value.type.name).group(1))
        buckets = self.value['nodes']
        for bucket in range(bucket_count):
            bucket_buffer = buckets[bucket]['buffer']
            buffer_size = bucket_buffer['size_']
            for i in range(buffer_size):
                element = bucket_buffer['ptr_'][i]
                if element['deleted']:
                    continue
                node = element['data'].cast(gdb.lookup_type(self.value.type.name + '::Node').pointer())
                yield str(node['hash']), node['val']

    def display_hint(self):
        return "hashmap"

class GongenPrettyPrinters(gdb.printing.PrettyPrinter):
    def __init__(self):
        super().__init__("gongen-printers")

    def __call__(self, value: gdb.Value):
        type = value.type
        if type is None:
            return None
        type_name = type.strip_typedefs().name
        if type_name is None:
            return None
        if type_name.startswith("gen::Buffer<"):
            return GenBufferPrinter(value)
        if type_name.startswith("gen::DynamicArray<"):
            return GenDynamicArrayPrinter(value)
        if type_name.startswith("gen::ByteBuffer"):
            return GenBufferPrinter(value)
        if type_name.startswith("gen::String"):
            return GenStringPrinter(value)
        if type_name.startswith("gen::HashMap") and type_name.endswith('>'):
            return GenHashmapPrinter(value)
        return None


gdb.printing.register_pretty_printer(None, GongenPrettyPrinters(), replace=True)
