group: games.bitrium.gongen
name: match-room-settings
version: 0
root: Settings
classes:
  Settings:
    max_members: u16
    max_endpoint_members: u16
    want_members: u16
