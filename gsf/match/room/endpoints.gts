group: games.bitrium.gongen
name: match-room-join
version: 0
root: Data
classes:
  Member:
    endpoint: u32
    name: str
  Endpoint:
    id: u32
    rank: u32
  Data:
    members: lst[Member]
    endpoints: lst[Endpoint]