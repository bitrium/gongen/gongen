// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/event.hpp>

namespace gen::input {

class GEN_API InputSource {
public:
	virtual ~InputSource() = default;
	// checks if other belongs to (or is) this
	virtual bool houses(const InputSource* other) const = 0;
};

class GEN_API Event : public gen::event::Event {
public:
	Event(const char* type_name, const InputSource* source)
		: gen::event::Event(type_name, (void*) source) {}
	virtual ~Event() = default;
};


enum class Key {
	NONE = 0,

	// Mouse
	MOUSE_LEFT, MOUSE_MIDDLE, MOUSE_RIGHT,
	MOUSE_BACK, MOUSE_FORWARD,

	// Keyboard
	KB_LCOMMAND, KB_LCTRL, KB_LSHIFT, KB_LALT,
	KB_RCOMMAND, KB_RCTRL, KB_RSHIFT, KB_RALT,
	KB_ESCAPE, KB_CAPS_LOCK, KB_BACKSPACE,
	KB_PRINT_SCREEN, KB_SCROLL_LOCK, KB_PAUSE,
	KB_DELETE, KB_INSERT, KB_HOME, KB_END, KB_PAGE_UP, KB_PAGE_DOWN,
	KB_UP, KB_DOWN, KB_LEFT, KB_RIGHT,
	KB_SPACE, KB_TAB, KB_ENTER,
	KB_NUM_LOCK, KB_NUM_ENTER,
	KB_NUM_ADD, KB_NUM_SUB, KB_NUM_MULT, KB_NUM_DIV,
	KB_NUM_0, KB_NUM_1, KB_NUM_2, KB_NUM_3, KB_NUM_4,
	KB_NUM_5, KB_NUM_6, KB_NUM_7, KB_NUM_8, KB_NUM_9,
	KB_F1, KB_F2, KB_F3, KB_F4, KB_F5, KB_F6, KB_F7, KB_F8,
	KB_F9, KB_F10, KB_F11, KB_F12, KB_F13, KB_F14, KB_F15, KB_F16,
	KB_F17, KB_F18, KB_F19, KB_F20, KB_F21, KB_F22, KB_F23, KB_F24,
	KB_0, KB_1, KB_2, KB_3, KB_4, KB_5, KB_6, KB_7, KB_8, KB_9,
	KB_Q, KB_W, KB_E, KB_R, KB_T, KB_Y, KB_U, KB_I, KB_O, KB_P,
	KB_A, KB_S, KB_D, KB_F, KB_G, KB_H, KB_J, KB_K, KB_L, 
	KB_Z, KB_X, KB_C, KB_V, KB_B, KB_N, KB_M,
	KB_TILDE, KB_MINUS, KB_EQUAL,
	KB_LEFT_SQUARE_BRACKET, KB_RIGHT_SQUARE_BRACKET, KB_BACKSLASH,
	KB_SEMICOLON, KB_APOSTROPHE,
	KB_COMMA, KB_PERIOD, KB_SLASH,

	// Gamepad
	GP_Y, GP_A, GP_X, GP_B,
	GP_UP, GP_DOWN, GP_LEFT, GP_RIGHT,
	GP_BACK, GP_GUIDE, GP_START,
	GP_LBUMPER, GP_RBUMPER, GP_LTRIGGER, GP_RTRIGGER,
	GP_LJOYSTICK, GP_RJOYSTICK,
};

char key_char(Key key);

enum class KeyAction {
	PRESS, HOLD, REPEAT, RELEASE,
};

class GEN_API KeyEvent : public Event {
public:
	KeyAction action;
	Key key;

	KeyEvent(const InputSource* source, KeyAction action, Key key)
		: Event("input/key", source), action(action), key(key) {}
};


enum class Offsetter {
	MOUSE_CURSOR, MOUSE_SCROLL,
	GP_LJOYSTICK, GP_RJOYSTICK,
};

class GEN_API OffsetEvent : public Event {
public:
	Offsetter controller;
	vec2d value; // for mouse, this is the new position, not offset

	OffsetEvent(const InputSource* source, Offsetter ctr, vec2d value)
		: Event("input/offset", source), controller(ctr), value(value) {}
};

class GEN_API TextEvent : public Event {
public:
	gen::String str;

	TextEvent(const InputSource* source, const gen::String& str)
		: Event("input/text", source), str(str) {}
	~TextEvent() {}
};

} // namespace gen::input
