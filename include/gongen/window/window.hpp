// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/string.hpp>
#include <gongen/common/math/vector.hpp>
#include "input.hpp"
#include <gongen/common/event.hpp>

namespace gen::window {
class GEN_API Window;

enum class WindowEventAction {
	MOVE, RESIZE,
	CLOSE, FOCUS, UNFOCUS, MAXIMIZE, UNMAXIMIZE, MINIMIZE, UNMINIMIZE,
	CURSOR_ENTER, CURSOR_EXIT,
};

class GEN_API WindowEvent : public event::Event {
public:
	Window* window;
	WindowEventAction action;
	vec2i pos;

	WindowEvent(Window* window, WindowEventAction action, vec2i pos = vec2i()) : event::Event("window") {
		this->window = window;
		this->action = action;
		this->pos = pos;
	}
};

namespace wsi {
enum WsiFunction {
	GET_NATIVE_HANDLES = 0
};

struct NativeHandles {
#ifdef GEN_PLATFORM_WIN32
	void* window;
#elif defined(GEN_PLATFORM_LINUX)
	unsigned long window;
	void* display = nullptr;
#endif
};
}

class GEN_API Window {
public:
	virtual ~Window() = default;

	virtual bool is_open() = 0;
	virtual void poll_events() = 0;
	virtual vec2d cursor_pos() = 0;
	virtual vec2u get_size() = 0;

	virtual void grab_cursor(bool grab) = 0;
	virtual bool is_cursor_grabbed() = 0;

	virtual void set_clipboard(const char* text) = 0;
	// the pointer is only temporarily valid – make a string!
	virtual const char* get_clipboard() = 0;

	// Starts emiting input events; returns null if input is unsupported
	virtual gen::input::InputSource* input() = 0;

	virtual void wsi_func(wsi::WsiFunction function, void* data) = 0;
};

enum WindowFlags {
	NO_DECORATION = 1
};

// Creates new window using supported window implementation
GEN_API Window* create(const gen::String& title, vec2u size, uint32_t flags = 0);
}
