// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors
#pragma once

#include <gongen/common/error.hpp>
#ifdef GEN_MOD_RESOURCES
#include "manager.hpp"
#endif

// this header still can be used without the 'resources' module loaded
namespace gen::res {
class Manager;
class Identifier;

template<typename T>
class Handle {
	Manager* manager = nullptr;
	union {
		Ptr<T> value;
#ifdef GEN_MOD_RESOURCES
		Flex<Identifier> id;
#endif
	};
public:
	Handle() : value() {
	}
#ifdef GEN_MOD_RESOURCES
	Handle(Manager* manager, const Identifier& id) : manager(manager), id() {
		this->id.construct(Identifier(id));
	}
	Handle(Manager* manager, const String& kind, const String& ns, const String& name) : manager(manager), id() {
		this->id.construct(Identifier { .kind = kind, .ns = ns, .name = name });
	}
	Handle(Manager* manager, const String& kind, const String& name) : manager(manager), id() {
		this->id.construct(Identifier { .kind = kind, .name = name });
	}
#endif
	explicit Handle(const Ptr<T>& value) {
		this->value = value;
	}
	Handle(T* value) {
		this->value = Ptr<T>(value, false);
	}
	~Handle() {
#ifdef GEN_MOD_RESOURCES
		if (this->manager) this->id.destroy();
#endif
	}
#ifdef GEN_MOD_RESOURCES
	void set(Manager* manager, const Identifier& id) {
		if (this->manager) this->id.destroy();
		this->manager = manager;
		this->id.construct(Identifier(id));
	}
#endif
	Handle& transfer_from(Handle<T>& other) {
		this->manager = other.manager;
		if (this->manager) {
#ifdef GEN_MOD_RESOURCES
			this->id.construct(Identifier(other.id.val()));
#endif
		} else {
			this->value = std::move(other.value.transfer());
		}
		return *this;
	}
	Handle& operator=(const Ptr<T>& value) {
#ifdef GEN_MOD_RESOURCES
		if (this->manager) {
			this->manager = nullptr;
			this->id.destroy();
		}
#endif
		this->value = value;
		return *this;
	}
	Handle& operator=(T* ptr) {
		this->operator=(Ptr<T>(ptr));
	}
	T* load() {
#ifdef GEN_MOD_RESOURCES
		if (this->manager) return this->manager->template get<T>(this->id.val());
#endif
		return this->value.ptr();
	}
	void reload(T*& value) {
#ifdef GEN_MOD_RESOURCES
		if (!this->manager || !this->manager->has(this->id.val())) return;
		value = this->manager->template get<T>(this->id.val());
#endif
	}
	T* reload() {
#ifdef GEN_MOD_RESOURCES
		if (!this->manager || !this->manager->has(this->id.val())) return nullptr;
		return this->manager->template get<T>(this->id.val());
#else
		return nullptr;
#endif
	}
	void reload_deref(T& value) {
#ifdef GEN_MOD_RESOURCES
		if (!this->manager || !this->manager->has(this->id.val())) return;
		value = *this->manager->template get<T>(this->id.val());
#endif
	}
};
}
