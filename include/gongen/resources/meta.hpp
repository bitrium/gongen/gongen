// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/path.hpp>

namespace gen::res {

struct GEN_API Identifier {
	String kind;
	String ns;
	String name;
	String ext;

	static uint64_t hash(const Identifier& id);

	static Identifier from_path(const String& path);
	static Identifier from_path(const AbstractPath& path);

	static Identifier from_str(const String& str);
	static String to_str(const Identifier& id);
	static Identifier from_str_ex(const String& str, String kind, String ns);
	static String to_str_ex(const Identifier& id, String kind, String ns);

	bool is_empty() const;
	void clear();

	/**
	 * @brief Checks if the identifier is valid.
	 * @return True if flawless; false if contains illegal characters.
	 */
	bool verification() const;
	/**
	 * @brief Checks verification and throws in case of errors.
	 * @see verification()
	 */
	void verify() const;

	/**
	 * @brief Get expanded version if provided default components are missing.
	 * @param kind Default kind, appended to the identifier if its missing.
	 * @param ns Default namespace, appended to the identifier if its missing.
	 * @return Created identifier.
	 */
	Identifier expanded(String kind, String ns = "") const;
	Identifier& expand(String kind, String ns = "");

	/**
	 * @brief Checks if the identifier features all components.
	 * @return Whether resource file identifier is complete.
	 */
	bool is_full_ext() const;
	/**
	 * @brief Checks if the identifier features 'kind', 'ns' and 'name'.
	 * @return Whether resource identifier is complete.
	 */
	bool is_full() const;
	/**
	 * @brief Throws if the identifier does not feature any of the components.
	 */
	void require_full_ext() const;
	/**
	 * @brief Throws if the identifier does not feature 'kind', 'ns' or 'name'.
	 */
	void require_full() const;

	bool operator==(const Identifier& other) const {
		return Identifier::hash(*this) == Identifier::hash(other);
	}
};

/*struct ModInfo {
	String id; // unique mod id, usually the main namespace
	String version;
	String name;
	String desc;
};*/

} // namespace gen::res
