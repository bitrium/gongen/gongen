// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/string.hpp>
#include <gongen/common/data_source.hpp>
#include "meta.hpp"

namespace gen::res {

class KindHandler;

struct KindFile {
	/**
	 * @brief Extension of the file excluding the leading dot.
	 *
	 * If multiple files of the same type exist, the beginning of the extension
	 * can be used for distinguishing. For example, a resource could use three
	 * files: 'x.txt', 'x.something.txt' and 'x.png'. In such case, this value
	 * shall be consecutively set to 'txt', 'something.txt' and 'png'.
	 *
	 * Non-arbitrary contents should rarely be placed in subdirectories,
	 * but in case this is required, the extension shall be 'd': for example
	 * 'x.txt', 'x.d/image.png' and 'x.d/description.txt' could be used by
	 * defining the extensions to 'txt', 'd/image.png' and 'd/description.txt'.
	 * For directories with arbitrary contents, any extension ending with '.d'
	 * can be used.
	 */
	String extension;

	/**
	 * @brief Whether to abort loading resource if the file is missing.
	 */
	bool is_optional : 1 = false;
	/**
	 * @brief Whether to use a VFS handle instead of pure data.
	 *
	 * If this value is set to true, the load function will be called with a VFS
	 * handle to the file (which will be required to be real and not a virtual
	 * file returned by a content script or included in any kind of a bundle).
	 *
	 * This is best used for loading directories with arbitrary contents.
	 */
	bool is_vfs_handle : 1 = false;
	/**
	 * @brief Whether this file may require any other resources.
	 *
	 * If this value is set to true, the dependencies will be requested before
	 * loading the file. If false, the hash will be generated from just the
	 * binary data.
	 */
	bool is_complex : 1 = false;
	/**
	 * @brief Whether the file is kept loaded throughout the resource existance.
	 *
	 * 
	 * If this value is set to true, the file will not be closed until the
	 * resource is destroyed. This will also disable reuse on matching hash.
	 */
	bool keep_open : 1 = false;

	/**
	 * @brief Dependencies and hash function. Only used for "complex" resources.
	 */
	union {
		uint64_t (*data)(KindHandler* handler, const Identifier& id, void** item, SeekableDataSource* data, Buffer<Identifier>& deps);
		uint64_t (*vfs)(KindHandler* handler, const Identifier& id, void** item/*, TODO VFS dir handle */, Buffer<Identifier>& deps);
	} prepare;

	/**
	 * @brief Load function called for each file load of matching resources.
	 */
	union {
		/**
		 * @brief Version of the function for data-based files.
		 * @param handler Owner kind handler.
		 * @param id Resource file identifier.
	 	 * @param item Pointer to the item object data.
		 * @param data The file data.
		 */
		void (*data)(KindHandler* handler, const Identifier& id, void** item, SeekableDataSource* data);
		// TODO doc
		void (*vfs)(KindHandler* handler, const Identifier& id, void** item/*, TODO VFS dir handle */);
	} load;
};

// TODO missing resource support (get default from kind handler)
/**
 * @brief Provides interfaces to used to process resources of a given kind.
 */
class KindHandler {
public:
	virtual ~KindHandler() = default;

	/**
	 * @brief Gets the unique ID of the kind.
	 * @return KindHandler's identifier.
	 */
	virtual String get_id() const = 0;
	/**
	 * @brief Gets files composing a resource of that kind.
	 * @return List of file description structures ordered by load priority.
	 */
	virtual Buffer<KindFile> get_files() const = 0;

	/**
	 * @brief Initializes an item. Prepares the resource for loading.
	 * By default the item is set to its identifier, so if it must be obtained
	 * for storing or another usage during initialization, cast '*item' to
	 * 'Identifier*' before changing it and store the result.
	 * If this throws, destroy will not be called.
	 * @param item Pointer to the item object data.
	 */
	virtual void create(void** item) = 0;
	/**
	 * @brief Completes an item. Prepares the resource for listing.
	 * This will only be called if all of the required files are loaded succes-
	 * fully and the optional ones either missing or also succesfully loaded.
	 * @param item Pointer to the item object data.
	 */
	virtual void finish(void** item) {}
	/**
	 * @brief Acknowledges an item. Called right before listing.
	 * Unlike 'finish', this is also called for items not loaded from files.
	 * Should not throw.
	 * @param item Pointer to the item object data.
	 */
	virtual void accept(void** item) {}
	/**
	 * @brief Acknowledges an item again, after reload with no change in hash.
	 * Should not throw.
	 * @param item Pointer to the item object data.
	 */
	virtual void reaccept(void** item) {}
	/**
	 * @brief Destroys an item. Requests deletion of an incomplete resource.
	 * Should not throw.
	 * @param item Pointer to the item object data.
	 */
	virtual void cancel(void** item) { this->destroy(item); };
	/**
	 * @brief Destroys an item. Requests deletion of a complete resource.
	 * Should not throw.
	 * @param item Pointer to the item object data.
	 */
	virtual void destroy(void** item) = 0;
	/**
	 * @brief Get pointer to the final resource object.
	 * @param item Pointer to the item object data.
	 * @return Pointer to the resource object data.
	 */
	virtual void* get_res(void** item) { return *item; }
};

} // namespace gen::content
