// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/iteration.hpp>
#include <gongen/common/platform/system.hpp>
#include <gongen/common/job.hpp>
#include "meta.hpp"

namespace gen::res {

class KindHandler;

struct LoadableFile {
	Identifier id;
	int priority = 0;
	void* user;
	SeekableDataSource* (*read)(void* user);
	void (*done)(void* user);
};
/**
 * @brief Helper class for creating LoadableFile in Object-oriented environment.
 * Destroys self when done.
 */
class GEN_API AbsLoadableFile {
	virtual ~AbsLoadableFile() = default;
	virtual SeekableDataSource* read() = 0;
	LoadableFile get(const Identifier& id, int priority = 0);
};

class GEN_API LoadConfig {
	void* impl;
	friend class Manager;
public:
	LoadConfig();
	~LoadConfig();

	/**
	 * @brief Add a file to load.
	 * The file may be dismissed due to low priority.
	 * @param file Information on file to load.
	 * @return Whether the file was added (true) or dismissed (false).
	 */
	bool add_file(LoadableFile file);

	/**
	 * @brief Manually add a resource. The resource is forced over any priority.
	 * @param id Resource identifier (without extension).
	 * @param resource The resource object. Takes ownership.
	 */
	void add_resource(Identifier id, void* resource);
};

class GEN_API Manager {
	void* impl;
public:
	Manager();
	~Manager();

	/**
	 * @brief Destroy and remove all resources and kinds.
	 */
	void cleanup();
	/**
	 * @brief Destroy and remove all resources.
	 */
	void empty();

	/**
	 * @brief Add a kind of loaded resources.
	 * @note It is illegal to call this function during resource loading.
	 * This function should be called for each kind in the best possible loading
	 * order (bad orders or more complex resource dependency trees are supported
	 * but will work slower).
	 * @param handler Handler of the kind.
	 */
	void add_kind(const SharedPtr<KindHandler>& handler);

	void set_default_ns(const String& ns);

	UniversalIterable<Identifier> list(String kind);
	UniversalIterable<Identifier> list_all();
	bool has(const Identifier& id);
	bool has(const String& kind, const String& ns, const String& name);
	bool has(const String& kind, const String& name);
	void* get(const String& kind, uint64_t id_hash);
	void* get(const Identifier& id);
	void* get(const String& kind, const String& ns, const String& name);
	void* get(const String& kind, const String& name);
	template<typename T>
	T* get(const String& kind, uint64_t id_hash) {
		return (T*) this->get(kind, id_hash);
	}
	template<typename T>
	T* get(const Identifier& id) {
		return (T*) this->get(id);
	}
	template<typename T>
	T* get(const String& kind, const String& ns, const String& name) {
		return (T*) this->get(kind, ns, name);
	}
	template<typename T>
	T* get(const String& kind, const String& name) {
		return (T*) this->get(kind, name);
	}

	bool busy() const;
	void load(const LoadConfig& config, bool clear = true);
	job::Handle load_job(job::System& jobs, const LoadConfig& config, bool clear = true);
};

} // namespace gen::content
