// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <stdint.h>

namespace gen::ecs {
class Simulation;
class Object;
#ifdef GEN_COMPONENT_CONFIG
#include GEN_COMPONENT_CONFIG
#else
typedef uint64_t CompBitfield;
typedef uint8_t CompId;
const static CompId COMPONENT_TYPE_COUNT = sizeof(CompBitfield) * 8;
#endif
} // namespace gen::ecs
