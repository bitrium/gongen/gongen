// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/event.hpp>
#include <gongen/common/platform.hpp>
#include <gongen/common/random.hpp>
#include "object.hpp"

namespace gen::ecs {

static const uint64_t INVALID_TICK = UINT64_MAX;

class Simulation;

struct GEN_API TickEvent : public event::Event {
	uint32_t tick;
	time::Time time;

	TickEvent(void* simulation) : event::Event("ecs/tick", simulation) {
	}

	Simulation* get_simulation() {
		return (Simulation*) this->source;
	}
};
struct GEN_API ObjectAddEvent : public event::Event {
	uint32_t tick;
	uint64_t object;

	ObjectAddEvent(void* simulation) : event::Event("ecs/object/add", simulation) {
	}

	Simulation* get_simulation() {
		return (Simulation*) this->source;
	}
	Object* get_object();
};
struct GEN_API ObjectRemoveEvent : public event::Event {
	uint32_t tick;
	uint64_t object;
	ByteBuffer object_copy;

	ObjectRemoveEvent(void* simulation) : event::Event("ecs/object/remove", simulation) {
	}

	Simulation* get_simulation() {
		return (Simulation*) this->source;
	}
	Object* get_copy() {
		return (Object*) this->object_copy.ptr();
	}
};

struct GEN_API SimulationState {
	uint64_t next_object_id = 0;
	ByteBuffer object_buffer;
	ByteBuffer rng_state;

	/**
	 * @brief Hashes the simulation state.
	 * @return The hash.
	 */
	uint64_t hash();

	void get_rng(rand::Random& random) const;
	void set_rng(const rand::Random& random);
};

/**
 * @brief An ECS-like Simulation of Objects.
 *
 * This is roughly a manager for Simulation States and Objects. The Simulation
 * is where the gameplay of a game usually takes place.
 *
 * Throughout the Simulation's runtime, numerous Ticks occur, each representing
 * an update of the Simulation and every Component of every Object.
 * States contain the complete information on the Simulation at the given Tick.
 *
 * At least two States are managed simultaneously: the *active* State, which is
 * completed and held for display, and the *current* State, which is incomplete,
 * and can be freely modified.
 * On every Tick, the *current* State is updated and becomes the *active* State,
 * and a new *current* State is created by copying the previous one.
 *
 * If the Simulation may be rewinded, more than two States can be kept to allow
 * reverting to one of them.
 */
class GEN_API Simulation {
public:
	using ObjectMap = HashMap<uint64_t, Object*, 1024>;
private:
	uint64_t reset_tick;
	DynamicArray<SimulationState> states;
	DynamicArray<ObjectMap> object_maps;

	uint64_t current_tick;
	uint64_t active_tick;

	double base_speed = 1.0f;
	double real_speed = 1.0f;

	void rewrite_object_map();
public:
	rand::Random random;

	/**
	 * @brief Create a new Simulation.
	 * @param state_memory_size Amount of simultaneously remembered States.
	 * @param base_speed Starting base speed. Ignore if a Ticker is used.
	 * @see set_base_speed
	 */
	Simulation(size_t state_memory_size, double base_speed = 1.0);

	/**
	 * @brief Check whether a State at the given Tick is in the memory.
	 * @param tick The Tick number.
	 * @return Whether it is valid.
	 */
	bool is_valid_tick(uint64_t tick);
	/**
	 * @brief Get the *active* State's Tick number.
	 * @return *Active* Tick number.
	 */
	size_t get_active_tick();
	/**
	 * @brief Get the *current* State's Tick number.
	 * @return *Current* Tick number.
	 */
	size_t get_current_tick();
	/**
	 * @brief Get the Tick number of oldest State still available in memory.
	 * @return Oldest Tick number.
	 */
	size_t get_oldest_tick();

	/**
	 * @brief Update the state to the next Tick.
	 * The *current* State becomes the *active* one. Every Component of every
	 * Object is updated.
	 * @param user User data to pass to the Component updates.
	 * @return The new *current* Tick number.
	 */
	uint64_t tick(void* user);
	/**
	 * @brief Tick the simulation until it reaches the given Tick number.
	 * @param tick The target Tick number.
	 * @param user User data to pass to the Component updates.
	 * @return Amount of sprinted ticks or INVALID_TICK if it's not future.
	 * @see tick
	 */
	uint64_t sprint(uint64_t tick, void* user);
	/**
	 * @brief Reset simulation to the given State.
	 * Previous *current* state does not become *active* – it is instead
	 * simply forgotten. Tick before to make it *active*.
	 * After resetting, all previous States are invalidated.
	 * @param tick The new *current* Tick number.
	 * @param state The new *current* State.
	 */
	void reset(uint64_t tick, const SimulationState& state);
	/**
	 * @brief Reset the simulation to a memorized state.
	 * Similarly to reset, does not change the active tick.
	 * @param tick The target Tick number.
	 * @return Amount of rewinded ticks or INVALID_TICK if it's too old.
	 */
	uint64_t rewind(uint64_t tick);
	/**
	 * @brief Reset simulation to an empty State at Tick 0.
	 * @see reset
	 */
	void clear();

	/**
	 * @brief Get the amount of simultaneously remembered States.
	 * @return State count.
	 */
	size_t state_count();
	/**
	 * @brief Get the State for the given Tick.
	 * @param tick The Tick number.
	 * @return The Simulation State or null pointer if Tick is invalid.
	 */
	SimulationState* get_state(uint64_t tick);
	/**
	 * @brief Get the *active* State.
	 * @return The Simulation State.
	 */
	SimulationState* get_active_state();
	/**
	 * @brief Get the *current* State.
	 * @return The Simulation State.
	 */
	SimulationState* get_current_state();

	/**
	 * @brief Adds a new Object to the Simulation (*current* State).
	 * @param components Bitfield listing Components to use.
	 * @return Created Object.
	 */
	Object* add_object(CompBitfield components);
	/**
	 * @brief Removes an Object from the Simulation (*current* State).
	 * @param id Identifier of the Object.
	 * @return Whether it existed before this call.
	 */
	bool remove_object(uint64_t id);
	/**
	 * @brief Get specific Object at the given Tick.
	 * @param tick Tick number of the queried State.
	 * @param id Identifier of the Object.
	 * @return The obtained Object or null pointer if missing.
	 */
	Object* get_object(uint64_t tick, uint64_t id);
	/**
	 * @brief Get specific Object at the *active* State.
	 * @param id Identifier of the Object.
	 * @return The obtained Object or null pointer if missing.
	 */
	Object* get_active_object(uint64_t id);
	/**
	 * @brief Get specific Object at the *current* State.
	 * @param id Identifier of the Object.
	 * @return The obtained Object or null pointer if missing.
	 */
	Object* get_current_object(uint64_t id);

	/**
	 * @brief Get map of Objects at a given State.
	 * @param tick The Tick of the State.
	 * @return The Object map.
	 */
	ObjectMap& get_object_map(uint64_t tick);
	/**
	 * @brief Get map of Objects at the *active* State.
	 * @return The Object map.
	 */
	ObjectMap& get_active_object_map();
	/**
	 * @brief Get map of Objects at the *current* State.
	 * @return The Object map.
	 */
	ObjectMap& get_current_object_map();

	/**
	 * @brief Get the current base speed value.
	 * @return Current base speed.
	 * @see set_base_speed
	 */
	double get_base_speed();
	/**
	 * @brief Set the base speed value.
	 * This value is the speed of the simulation (passed to Components during
	 * updates) when it is neither sped up nor slowed down. This is usually
	 * the amount of default ticks per second divided by the selected amount so
	 * that the base speed of 1.0 is used for the default amount.
	 * @param new_base_speed New base speed value.
	 */
	void set_base_speed(double new_base_speed);
	/**
	 * @brief Get the current speed multiplier.
	 * @return Current speed multiplier value.
	 */
	double get_speed();
	/**
	 * @brief Change the current speed multiplier (1.0 for normal speed).
	 * @param new_speed New speed multiplier value.
	 */
	void set_speed(double new_speed);
	/**
	 * @brief Get the current speed value passed to the Components on updates.
	 * @return get_base_speed() * get_speed()
	 */
	double get_actual_speed();
};

} // namespace gen::ecs
