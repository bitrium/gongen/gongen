// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include "component.hpp"
#include <gongen/common/math/color.hpp>
#include <gongen/common/math/vector.hpp>

namespace gen::ecs {
struct Name;
struct Props;
} // namespace gen::ecs

GEN_COMP(gen::ecs::Name, NAME) {
	char name[48];
};
GEN_COMP(gen::ecs::Props, PROPS) {
	gen::vec3f pos;
	float scale = 1.0f;
	gen::vec2f rotation;
	float mass = 1.0f;
	gen::Color color;
};
