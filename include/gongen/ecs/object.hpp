// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/error.hpp>
#include "component.hpp"

namespace gen::ecs {
const static uint64_t INVALID_OBJECT = UINT64_MAX;

#pragma pack(push, 8)
/**
 * @brief A Simulation Object.
 *
 * As the name suggests, this class represents Objects in the game, some of
 * which may be visible or interactible. Objects are managed by the Simulation.
 * While the Objects themselves usually contain few information about the
 * represented entity, they feature Components which define the Object's state
 * and behaviour.
 *
 * The Simulation manages Objects within a byte buffer, where each Object is
 * directly followed by its Components. Following this rule, simple methods can
 * be used to obtain Components of the Object without querying the Simulation.
 */
class GEN_API Object {
	uint64_t id;
	CompBitfield components;
	size_t component_size;

	void* get_component_internal(CompId id);
	Object(uint64_t id, CompBitfield components);
	Object() = delete;
	Object(Object&) = delete;

	friend class Simulation;
public:
	/**
	 * @brief User data. Do whatever you want with this.
	 */
	void* user = nullptr;

	bool has_components(CompBitfield components) const;
	/**
	 * @brief Checks whether the Object has the Component of the given type.
	 * @param id Identifier of the checked Component.
	 * @return Whether the components following this Object include it.
	 */
	bool has_component(CompId id) const;
	/**
	 * @brief Gets one of the components following this Object in the Simulation's buffer.
	 * @tparam T Type of the Component (it's structure type).
	 * @param id Identifier of the obtained Component.
	 * @return Obtained Component.
	 */
	template<typename T>
	T* get_component(CompId id) {
		if (sizeof(T) != internal::types[id].size) {
			throw gen::FunctionCallException("Bad component size.");
		}
		return (T*) this->get_component_internal(id);
	}

	/**
	 * @brief Gets the Object's identifier.
	 * @return The identifier of the Object or INVALID_OBJECT if deleted.
	 */
	uint64_t get_id() const { return this->id; }
	/**
	 * @brief Gets information on available Components.
	 * @return Bitfield containing bits representing available Components.
	 */
	CompBitfield get_components() const { return this->components; }
	/**
	 * @brief Gets amount of bytes the following Components take.
	 * @return Sum of sizes of used Components.
	 */
	size_t get_component_size() const { return this->component_size; }
	/**
	 * @brief Gets amount of bytes this Object and its Components take.
	 * @return Amount of space taken in the Simulation's buffer.
	 */
	size_t get_size() const { return sizeof(Object) + this->get_component_size(); }
};
#pragma pack(pop)

} // namespace gen::ecs
