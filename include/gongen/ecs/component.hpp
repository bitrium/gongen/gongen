// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <stddef.h>
#include <new>
#include <gongen/common/defines.hpp>
#include "definitions.hpp"

namespace gen::ecs {

inline namespace internal {
struct GEN_API TypeSpec {
	size_t size;
	void (*create)(void* ptr);
	void (*update)(Simulation* sim, Object* obj, void* comp, double speed, void* user);
};
GEN_API extern TypeSpec types[COMPONENT_TYPE_COUNT];
GEN_API CompId comp_register(TypeSpec spec);
GEN_API extern CompId registered_type_count;
} // inline namespace internal

/**
 * @brief Converts a component id into a bitfield value.
 * @param id The component id.
 * @return The bit representing this component.
 */
GEN_API CompBitfield comp_id_to_bit(CompId id);

/**
 * @brief Declares an ECS component.
 *
 * Must be called outside of any namespaces.
 * Should be enclosed in `#pragma pack(push, 8)` and `#pragma pack(pop)`
 * The component struct must be forward-declared using `struct (name);`.
 *
 * Starts the definition of the component's structure. Should be followed with:
 * `{`, component members, and `};`. All members should provide default values,
 * and the implicit constructor must be available.
 *
 * @param name Forward-declared name of the component struct with namespaces.
 * @param component Unique codename of the component (all caps).
 */
#define GEN_COMP(name, component)													\
	namespace gen::ecs {															\
		GEN_API extern const CompId COMP_##component##_ID;							\
		GEN_API extern const CompBitfield COMP_##component##_BIT;					\
		typedef name COMP_##component##_CLASS;										\
		void GEN_API COMP_##component##_UPDATE_FN(									\
			Simulation*, Object*, name*, double, void*								\
		);																			\
	}																				\
	struct GEN_API name

/**
 * @brief Implements an ECS component.
 *
 * Must be called outside of any namespaces and in a source file.
 * Internally registers the component type.
 *
 * Starts the definition of the component's update function and therefore must
 * be followed with a `{`, the function code, and `}`.
 *
 * @param component The component's codename.
 * @param sim_arg Name of the *simulation* argument of the update function.
 * @param obj_arg Name of the *object* argument of the update function.
 * @param comp_arg Name of the *component* argument of the update function.
 * @param speed_arg Name of the *speed* argument of the update function.
 * @param user_arg Name of the *user data* argument of the update function.
 */
#define GEN_COMP_IMPL(component, sim_arg, obj_arg, comp_arg, speed_arg, user_arg)				\
	namespace gen::ecs {																		\
		const CompId COMP_##component##_ID = internal::comp_register({							\
			.size = sizeof(COMP_##component##_CLASS),											\
			.create = [](void* ptr) {															\
				new (ptr) COMP_##component##_CLASS();											\
			},																					\
			.update = [](Simulation* sim, Object* obj, void* ptr, double speed, void* user) {	\
				auto* comp = (COMP_##component##_CLASS*) ptr;									\
				return COMP_##component##_UPDATE_FN(sim, obj, comp, speed, user);				\
			},																					\
		});																						\
		const CompBitfield COMP_##component##_BIT 												\
				= comp_id_to_bit(COMP_##component##_ID);										\
	}																							\
	void gen::ecs::COMP_##component##_UPDATE_FN(												\
		gen::ecs::Simulation* sim_arg, gen::ecs::Object* obj_arg,								\
		gen::ecs::COMP_##component##_CLASS* comp_arg, double speed_arg, void* user_arg			\
	)

#define GEN_COMP_IMPL_STATIC(component) GEN_COMP_IMPL(component, sim, obj, comp, speed, user) {}

} // namespace gen::ecs
