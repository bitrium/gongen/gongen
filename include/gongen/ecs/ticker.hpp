// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/platform/system.hpp>

namespace gen::ecs {

class Simulation;

/**
 * @brief Counts how many ticks an update should perform.
 */
class GEN_API Ticker {
	double leftover_time;
	double base_tps;
	double tps;
	double spt;
	gen::time::Time last_update;
public:
	/**
	 * @brief The simulation to tick. May be null for custom usage.
	 */
	Simulation* simulation;
	/**
	 * @brief Create a ticker.
	 * The base tick rate will be used as default for setting simulation base speed.
	 * @param simulation Simulation to manage (can be null pointer for none).
	 * @param base_tps Default and starting tick rate (in ticks per second).
	 */
	Ticker(Simulation* simulation, double base_tps);
	/**
	 * @brief Change the tick rate (and the base speed of the simulation).
	 * @param new_tps New tick rate in ticks per second.
	 */
	void set_tick_rate(double new_tps);
	/**
	 * @brief On update. Calculate and execute (if Simulation is present) ticks.
	 * @param tick_user User data to pass to the Simulation (if present).
	 * @return Calculated amount ticks.
	 */
	size_t update(void* tick_user);
	/**
	 * @brief On update, but also execute a callback on every tick.
	 * @param on_tick The callback to execute on every tick.
	 * @param user User data to pass to the callback.
	 * @param tick_user User data to pass to the Simulation (if present).
	 * @return Calculated amount ticks.
	 * @see size_t update(void*)
	 */
	size_t update(void (*on_tick)(void*), void* user, void* tick_user);
};

}
