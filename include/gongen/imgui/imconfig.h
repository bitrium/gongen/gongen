// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2023 GongEn Contributors

#pragma once

#include <gongen/common/defines.hpp>

#define IMGUI_API GEN_API
#define IMGUI_DISABLE_OBSOLETE_FUNCTIONS
#define IMGUI_DISABLE_OBSOLETE_KEYIO
#define IMGUI_USE_WCHAR32
#define ImDrawIdx uint32_t
#define ImTextureID size_t
#define IMGUI_STB_TRUETYPE_FILENAME   <stb_truetype.h>
#define IMGUI_STB_RECT_PACK_FILENAME  <stb_rect_pack.h>
#define IMGUI_DISABLE_STB_RECT_PACK_IMPLEMENTATION
#define IMGUI_DISABLE_STB_SPRINTF_IMPLEMENTATION

#include <gongen/common/math/vector.hpp>

#define IM_VEC2_CLASS_EXTRA                                                     \
        constexpr ImVec2(const gen::vec2f& f) : x(f.x), y(f.y) {}                   \
        operator gen::vec2f() const { return gen::vec2f(x,y); }

#define IM_VEC4_CLASS_EXTRA                                                     \
        constexpr ImVec4(const gen::vec4f& f) : x(f.x), y(f.y), z(f.z), w(f.w) {}   \
        operator gen::vec4f() const { return gen::vec4f(x,y,z,w); }
