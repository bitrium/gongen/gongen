// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/gl/gl.hpp>
#define IMGUI_USER_CONFIG <gongen/imgui/imconfig.h>
#define IMGUI_DEFINE_MATH_OPERATORS 1
#include <imgui.h>
#include <gongen/resources/handle.hpp>

namespace gen::imgui {
void GEN_API init(gl::Device* device, window::Window* window, res::Handle<gl::Program> imgui_program);
void GEN_API refresh_font_texture();
void GEN_API new_frame();
void GEN_API render();
void GEN_API shutdown();

//Must be called every frame in which texture is used
ImTextureID GEN_API texture_id(const gen::gl::Texture& texture, const gen::gl::Sampler& sampler);
}
namespace ImGui {
IMGUI_API bool InputText(const char* label, gen::String& str, ImGuiInputTextFlags flags = 0, ImGuiInputTextCallback callback = nullptr, void* user_data = nullptr);
IMGUI_API bool InputTextMultiline(const char* label, gen::String& str, const ImVec2& size = ImVec2(0, 0), ImGuiInputTextFlags flags = 0, ImGuiInputTextCallback callback = nullptr, void* user_data = nullptr);
IMGUI_API bool InputTextWithHint(const char* label, const char* hint, gen::String& str, ImGuiInputTextFlags flags = 0, ImGuiInputTextCallback callback = nullptr, void* user_data = nullptr);
}
