// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/buffer.hpp>
#include <gongen/net/providers.hpp>

namespace gen::match {

namespace ranks {
enum : int16_t {
	SPECTATOR = -1000,
	NORMAL = 0,
	MODERATOR = 1000,
	ADMIN = 2000,
	HOST = INT16_MAX,
};
}

struct RoomInfoVague {
	uint32_t id;
	String name;
};
// TODO Optional<Buffer<RoomInfoVague>> room_list(net::ClientProvider& client);

struct RoomInfo {
	uint32_t id;
	String name;
	String desc;
};
struct RoomSettings {
	// maximum amount of members (or 0 for none)
	uint16_t max_members = 0;
	// maximum amount of members per endpoint (or 0 for none)
	uint16_t max_endpoint_members = 0;
	// amount of members required to start (or 0 for none)
	uint16_t want_members = 0;
};
struct Permissions {
	uint32_t rank_manage_endpoints = ranks::ADMIN;
	uint32_t rank_manage_members = ranks::MODERATOR;
	uint32_t rank_change_settings = ranks::MODERATOR;
	uint32_t rank_change_info = ranks::MODERATOR;
	uint32_t rank_start = ranks::ADMIN;
	uint32_t rank_view_match = ranks::SPECTATOR;
};
struct Member {
	uint32_t endpoint_id;
	String name;
};
struct Endpoint {
	uint32_t id; // 0 for host
	uint32_t rank;
	Buffer<Member> members;
};
struct MatchInfo {
	time::Time started;
	Buffer<Member> members;
};

class Room {
	void* impl;
public:
	Room(net::ClientProvider* client);
	Room(net::ServerProvider* server);
	~Room();

	void update();
	void join(uint32_t id);
	/**
	 * For client, returns true when connected.
	 * @return Whether ready for usage.
	 */
	bool is_ready();

	RoomInfo get_info();
	RoomSettings get_settings();
	HashMap<uint32_t, Endpoint> get_endpoints();
	Permissions get_permissions();

	void set_info(const RoomInfo&);
	void set_settings(const RoomSettings&);
	void set_endpoints(HashMap<uint32_t, Endpoint>&);
	void set_permissions(const Permissions&);

	Optional<MatchInfo> match_info();
	MatchInfo match_start();
};

} // namespace gen::match
