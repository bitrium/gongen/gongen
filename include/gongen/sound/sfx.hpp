// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/string.hpp>
#include <gongen/common/containers/lockless_queue.hpp>
#include <gongen/common/containers/list.hpp>
#include <gongen/common/random.hpp>
#include <gongen/common/math/vector.hpp>
#include <gongen/resources/meta.hpp>
#include "input.hpp"

namespace gen::res {
class Manager;
class KindHandler;
}

namespace gen::sound {

// TODO SFX variants (.d folder in resources, random selection)
class Sfx {
public:
	void* impl;
	const res::Identifier id;

	Sfx(const res::Identifier& id);
	~Sfx();
};

struct SfxPlayback {
	res::Identifier id;
	gen::vec3f pos = gen::vec3f(); // TODO
	uint32_t frame = 0;
	float volume = 1.0f;
	float speed = 1.0f;
	size_t impl_id = 0; // ignore this

	bool operator==(const SfxPlayback& other) const = default;
};

// TODO track playback control: seek music, change speed
class SfxPlayer : public Input {
	DynamicArray<SfxPlayback> playback_buf;
	Buffer<size_t> to_remove_buf;
	size_t next_impl_id = 1;
public:
	const String kind_id;
	res::Manager* const resources;
	std::atomic_bool pause = false;
	std::atomic<float> origin[3]; // move this with camera/player
	std::atomic<float> rotation[3]; // change this with camera/player
	BufferList<SfxPlayback> playback;
	std::atomic_bool changing = false; // set this while changing playback

	SfxPlayer(res::Manager* resources, const String& kind = "sfx");

	/**
	 * @brief Adds a sound effect to playback.
	 * @param sfx Effect's identifier.
	 * @param pos Offset from Player's origin (used to calculate pan).
	 * @return Played sound. Pointer is only valid until sound ends.
	 */
	SfxPlayback* play(const res::Identifier& sfx, gen::vec3f pos = gen::vec3f());
	/**
	 * @brief Removes all instances of a sound effect from playback.
	 * @param sfx Effect's identifier.
	 * @return Count of stopped effects.
	 */
	size_t stop(const res::Identifier& sfx);

	void data(Buffer<float>& buf, size_t frame_count) override;
};

res::KindHandler* sfx_handler(const String& kind = "sfx");

}
