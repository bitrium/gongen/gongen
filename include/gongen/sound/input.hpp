// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <stdlib.h>

namespace gen::sound {

class Input {
public:
	virtual ~Input() = default;
	virtual void data(Buffer<float>& buf, size_t frame_count) = 0;
};

}
