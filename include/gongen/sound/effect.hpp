// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/buffer.hpp>

namespace gen::sound {

struct Effect {
	virtual ~Effect() = default;
	virtual void process(gen::Buffer<float>& samples) = 0;
};

} // namespace gen::sound
