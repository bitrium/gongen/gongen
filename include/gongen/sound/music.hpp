// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/string.hpp>
#include <gongen/common/containers/lockless_queue.hpp>
#include <gongen/common/random.hpp>
#include <gongen/common/math/vector.hpp>
#include <gongen/resources/meta.hpp>
#include "input.hpp"

namespace gen::res {
class Manager;
class KindHandler;
}

namespace gen::sound {

class Music {
public:
	void* impl;
	const res::Identifier id;

	Music(const res::Identifier& id);
	~Music();

	/**
	 * @brief Loads the entire track into memory.
	 */
	void buffer();
	/**
	 * @brief Removes buffered track from memory.
	 */
	void free();
};

// TODO track playback control: seek music, change speed
class MusicPlayer : public Input {
	gen::rand::Random rng;
	res::Identifier current;
	res::Identifier holding;
	bool paused = true;
public:
	const String kind_id;
	res::Manager* const resources;
	TypedLocklessQueue<res::Identifier> queue;
	bool loop = false; // TODO
	bool loop_queue = false; // TODO
	/**
	 * @brief Whether to select a random music track after finishing the queue.
	 */
	bool select_random = false;
	/**
	 * @brief Time range of intervals between music taacks in miliseconds.
	 * Every interval will be a random number between these two.
	 * The inverval may be artificially increased by no more than a second due
	 * to the way implementation depending on audio request sizes.
	 */
	vec2u intervals = vec2u(30000, 4 * 60000);
	/**
	 * @brief Current interval in seconds.
	 * This is the countdown to the next music selection.
	 */
	double interval = 0.0;
	double buffer_secs = 1.0;

	MusicPlayer(res::Manager* resources, const String& kind = "music", uint64_t rng_seed = UINT64_MAX);

	void reset_interval();
	bool is_playing();
	/**
	 * @brief Replaces currently played music with the given one.
	 * @param track Requested music identifier.
	 */
	void play(const res::Identifier& track);
	void skip();
	bool is_paused();
	void pause();
	void unpause();
	bool is_held();
	void hold(const res::Identifier& filler); // pause with waiting music
	void unhold();

	void data(Buffer<float>& buf, size_t frame_count) override;
};

res::KindHandler* music_handler(const String& kind = "music");

}
