// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/audio/format.hpp>

namespace gen::sound {

size_t resample_calc_dst(audio::Format from, audio::Format to, size_t src_frames);
size_t resample_calc_src(audio::Format from, audio::Format to, size_t dst_frames);

void resample(
	audio::Format from, uint8_t* src, size_t* src_frames,
	audio::Format to, uint8_t* dst, size_t* dst_frames
);

} // namespace gen::sound
