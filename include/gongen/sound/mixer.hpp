// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/list.hpp>
#include <gongen/common/containers/lockless_queue.hpp>
#include <gongen/audio/audio.hpp>
#include "input.hpp"
#include "effect.hpp"

// TODO turn generator effects into Inputs and make Effect control samples from previous Effect/Input

namespace gen::sound {

const static audio::Format MIXER_FORMAT = {
	.bit_depth = 32,
	.sample_rate = 48000,
	.channel_count = 2,
};

class Track {
	LinkedList<Effect*> effects;
	gen::SharedPtr<Input> input_;

	Effect* add_effect(Effect* effect);
	void process(gen::Buffer<float>& samples);
	friend class Mixer;
public:
	/**
	 * @brief Volume of the sound on the track.
	 */
	float volume = 1.0f;

	/**
	 * @brief Add a new effect that will be processed after the previous one.
	 * @tparam T Type of the newly added effect.
	 * @tparam Args Types of constructor arugments.
	 * @param args Constructor arguments that will be used to create the effect.
	 * @return Created and added effect.
	 */
	template<typename T, typename... Args>
	T* add_effect(Args&&... args) {
		return (T*) this->add_effect(new T(std::forward<Args>(args)...));
	}
	/**
	 * @brief Get current list of effects.
	 * @return List of effects in processing order.
	 */
	const LinkedList<Effect*> get_effects() { return this->effects; };
	/**
	 * @brief Remove all effects.
	 */
	void clear_effects();

	void input(gen::SharedPtr<Input> input) { this->input_ = input; }
};

class Mixer {
	LinkedList<uint8_t> links[0x100];
	Buffer<float> process_buffers[0x100];
public:
	/**
	 * @brief Tracks by their ids.
	 */
	Track tracks[0x100];

	/**
	 * @brief Clear unused memory.
	 * Clears memory reserved for processing sound for used tracks.
	 */
	void clear_buffers();

	/**
	 * @brief Link one or many tracks to another.
	 * The destination track will consume sound from all source tracks.
	 * If @a count is too big, wraps around the track array.
	 * @param dst The destination track.
	 * @param src The first source track.
	 * @param count Amount of consecutive source tracks to link.
	 * @return Amount of newly linked tracks.
	 */
	uint8_t link(uint8_t dst, uint8_t src, uint8_t count = 1);
	/**
	 * @brief Unlink one or many tracks from another.
	 * If @a count is too big, wraps around the track array.
	 * @param dst The link's destination track.
	 * @param src The first link's source track.
	 * @param count Amount of consecutive source tracks to unlink.
	 * @return Amount of just unlinked tracks.
	 */
	uint8_t unlink(uint8_t dst, uint8_t src, uint8_t count = 1);
	/**
	 * @brief Unlink all sources from a destination track.
	 * @param dst The track.
	 * @return Amount of just unlinked tracks.
	 */
	uint8_t unlink_all(uint8_t dst);
	/**
	 * @brief Unlink all sources from a destination track and repeat recursively.
	 * @param dst The track.
	 * @return Amount of just unlinked tracks.
	 */
	size_t unlink_recursive(uint8_t dst);
	/**
	 * @brief Remove all links.
	 * @return Amount of just unlinked tracks.
	 */
	size_t clear_links();

	/**
	 * @brief Process sound. Advances the state of used effects.
	 * @param master The final output track. Usually, the "master" track.
	 * @param frame_count Amount of samples to produce per channel.
	 * @return Processed samples.
	 */
	void process(uint8_t master, size_t frame_count);
	/**
	 * @brief Process sound in advance and queue it for playback.
	 * @param master The final output track. Usually, the "master" track.
	 */
	// void advance(uint8_t master);

	/**
	 * @brief Sets up playback from queued buffers to the Audio API.
	 * @return A callback to pass to the Audio API. Use the mixer as user data.
	 */
	audio::OutputCallback audio_output();
};

} // namespace gen::sound
