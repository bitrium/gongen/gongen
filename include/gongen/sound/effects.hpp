// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "effect.hpp"

namespace gen::sound::effects {

struct Limiter : public Effect {
	/**
	 * @brief [0; 1]; limits the sound.
	 */
	float threshold = 1.0f;

	Limiter(float threshold = 0.0f) : threshold(threshold) {}

	void process(gen::Buffer<float>& samples) override;
};

struct Panning : public Effect {
	/**
	 * @brief [-1; 1]; negative shifts to left, positive shifts to right.
	 */
	float value = 0.0f;

	Panning(float value = 0.0f) : value(value) {}

	void process(gen::Buffer<float>& samples) override;
};

/**
 * This is a "noise" generator that 
 */
struct WeirdWave : public Effect {
	/**
	 * @brief Current offset. Modified throughout the processing.
	 */
	double offset = 0;

	/**
	 * @brief Volume of input sound.
	 */
	float dry = 0.0f;
	/**
	 * @brief Volume of generated noise.
	 */
	float wet = 1.0f;

	float quality = 10.0f;
	float amplitude = 1.0f;
	float shape = 0.1f;
	float width = 0.1f;

	void process(gen::Buffer<float>& samples) override;
};

struct SinWave : public Effect {
	double time = 0;

	float frequency = 440;
	float volume = 1; // [0; 1]

	void process(gen::Buffer<float>& samples) override;
};

} // namespace gen::sound::effects
