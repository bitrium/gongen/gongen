#pragma once

#include <gongen/common/containers/string.hpp>

namespace gen::gui {

enum class WidgetType {
	TEXT,
	BUTTON,
	CHECKBOX,
};

struct GEN_API Widget {
	WidgetType type;
	uint32_t flags;
	String text;
};

}
