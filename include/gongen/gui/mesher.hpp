#pragma once

#include <gongen/common/math/rect.hpp>
#include "widgets.hpp"

namespace gen::gui {

struct GEN_API Vertex {
	gen::vec2d pos;
};

struct GEN_API Mesher {
	virtual ~Mesher() = default;
	virtual void mesh(const Widget& widget, Buffer<Vertex>& vertices, rect2d& interact_area) = 0;
};

struct GEN_API DefaultMesher : public Mesher {
	void mesh(const Widget& widget, Buffer<Vertex>& vertices, rect2d& interact_area) override;
};

}
