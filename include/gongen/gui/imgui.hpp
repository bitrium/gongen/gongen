// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/string.hpp>
#include <gongen/common/math/rect.hpp>
#include <gongen/window/window.hpp>
#include <gongen/gl/gl_defines.hpp>

namespace gen::gui {

enum class ButtonState : uint8_t {
	IDLE = 0,
	LEFT_CLICKED = 1,
	RIGHT_CLICKED = 2,
};

enum class CheckboxState : uint8_t {
	UNCHECKED = 0,
	CHECKED = 1,
};

struct Config {
};

// bool returns tell whether the state changed
class GEN_API Block {
	size_t idx = 0;
public:
	void* impl;

	explicit Block(void* impl) { this->impl = impl; }

	void push_config(Config config);
	void pop_config();

	void header(const String& text);
	void paragraph(const String& text, uint32_t flags = 0);

	bool button(ButtonState& state, const String& name, uint32_t flags = 0);
	ButtonState button(const String& name, uint32_t flags = 0);
	bool checkbox(CheckboxState& state, const String& name, uint32_t flags = 0);
	CheckboxState checkbox(const String& name, uint32_t flags = 0);

	Block box(uint32_t flags = 0);
	// adds a context menu to the last item.
	Block context(uint32_t flags = 0);
	Block spoiler(bool* collapsed);
};

struct Mesher;
struct Widget;
class GEN_API Context {
	void* impl;
public:
	Context(window::Window* window, rect2d area, Ptr<Mesher> mesher);
	~Context();

	Block root();

	void update();
	void setup_gl(gl::Device& device);
	void draw(gl::Device& device);
};

}
