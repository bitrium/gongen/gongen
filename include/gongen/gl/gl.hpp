// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "state.hpp"
#include <gongen/window/window.hpp>
#include <gongen/common/containers/dynamic_array.hpp>
#include <gongen/common/error.hpp>
#include <gongen/common/platform/system.hpp>

namespace gen::gl {
GEN_EXCEPTION_TYPE(Exception, gen::RuntimeException, "Graphics Error");
GEN_EXCEPTION_TYPE(InitException, Exception,
				   "Graphics Initialization Error");
GEN_EXCEPTION_TYPE(HardwareException, InitException,
				   "Graphics Hardware Error");
GEN_EXCEPTION_TYPE(RuntimeException, Exception,
				   "Graphics Runtime Error");

GEN_API size_t get_texture_size(Format format, vec3z size = vec3z(1), size_t layers = 1);
GEN_API size_t get_mip_levels(gen::vec3z size, bool compressed = false);

namespace internal {
const static uint32_t CACHE_OBJECT_LIFETIME = (60 * 60);

struct GEN_API FormatInfo {
	uint32_t type;
	uint32_t vertex_size;
	bool vertex_normalized;
	uint32_t texture_internal_format;
	uint32_t texture_format;
};

FormatInfo get_format_info(gen::gl::Format format);

struct GEN_API Device {
	LocklessQueue free_queue_buffer;
	LocklessQueue free_queue_texture;
	LocklessQueue free_queue_sampler;
	LocklessQueue free_queue_program;

	gen::window::wsi::NativeHandles wsi_handles;
	gen::window::Window* window;
	void* context;
	bool debug_mode;
	VertexBinding binding[MAX_VERTEX_BINDINGS];

	thread::ThreadId ctx_thread;

	gl::internal::GLState state;

	struct CachedObject {
		uint64_t hash;
		uint32_t object;
		uint32_t life_counter = CACHE_OBJECT_LIFETIME;
	};

	gen::Buffer<CachedObject> vao_cache;
	gen::Buffer<CachedObject> fbo_cache;

	uint32_t active_topology = 0x0004;
	bool index_ushort = false;

	uint32_t vao = 0;
	bool dsa_support = false;

	Device() :ctx_thread(gen::thread::id()) {}

	bool is_context_active(bool force_active);

	uint32_t get_fbo(const gen::gl::FramebufferState& state);
};

struct GEN_API GLBuffer :public Buffer {
	uint32_t buffer = 0;
	uint32_t gl_usage;
	Device* device;
	BufferDesc desc;
	void* data = nullptr;
	bool own_data = false;

	GLBuffer(Device* device, const BufferDesc& desc);
	void init();
	~GLBuffer();
};

struct GEN_API GLTexture :public Texture {
	uint32_t texture = 0;
	uint32_t target = 0;
	Device* device;
	TextureDesc desc;
	void* data = nullptr;
	bool own_data = false;

	GLTexture(Device* device, const TextureDesc& desc);
	void init();
	~GLTexture();
};

struct GEN_API GLSampler :public Sampler {
	uint32_t sampler = 0;
	Device* device;
	SamplerDesc desc;

	GLSampler(Device* device, const SamplerDesc& desc);
	void init();
	~GLSampler();
};

struct GEN_API GLProgram :public Program {
	uint32_t program = 0;
	Device* device;
	ProgramDesc desc;

	GLProgram(Device* device, const ProgramDesc& desc);
	void init();
	~GLProgram();
};
};

struct Capabilities {
	bool first_instance = false;
	bool first_vertex = false;
	bool multi_draw_indirect = false;
	bool glsl_first_instance = false;

	int32_t ubo_offset_aligment;
	int32_t ssbo_offset_aligment;
};

class GEN_API Device {
	internal::Device impl;
	Capabilities capabilities;
public:
	Device(gen::window::Window* window, bool debug_mode);
	~Device();

	Capabilities get_capabilities() { return capabilities; }
	void display();

	Buffer create_buffer(const BufferDesc& desc);
	Texture create_texture(const TextureDesc& desc);
	Sampler create_sampler(const SamplerDesc& desc);
	Program create_program(const ProgramDesc& desc);

	//Update
	void update_buffer(const Buffer& buffer, size_t offset, size_t size, const void* data);
	void update_texture(const Texture& texture, size_t level, gen::vec3z offset, gen::vec3z size, const void* data);
	void generate_mips(const Texture& texture);

	//Download
	void read_pixels(const Buffer& dst, uint32_t dst_offset, gen::vec2i src_offset, gen::vec2u size, gen::gl::Format format);
	void get_buffer_data(const Buffer& buffer, uint32_t offset, uint32_t size, void* data);

	//Binding
	void bind_storage_buffer(const Buffer& buffer, uint32_t slot, size_t offset = 0, size_t size = 0);
	void bind_uniform_buffer(const Buffer& buffer, uint32_t slot, size_t offset = 0, size_t size = 0);
	void bind_texture(const Texture& texture, uint32_t slot); //Simple version
	void bind_image_texture(const Texture& texture, uint32_t slot, uint32_t level, uint32_t layer, bool layered, TextureAccess access, Format format);
	void bind_sampler(const Sampler& sampler, uint32_t texture_slot);
	void bind_vertex_buffer(const Buffer& buffer, uint32_t binding, size_t offset = 0);
	void bind_index_buffer(const Buffer& buffer, bool index_ushort = false);
	//State
	void set_vertex_format(const VertexFormat& format);
	void set_primitive_state(const PrimitiveState& state);
	void set_depth_state(const DepthState& state);
	void set_blend_state(const BlendState& state);
	void set_framebuffer_state(const FramebufferState& state);
	void set_default_framebuffer();
	void clear_framebuffer(const ClearDesc& desc);
	void blit_framebuffer(const FramebufferState& src, const FramebufferState& dst, gen::vec2u src_off, gen::vec2u src_size, gen::vec2u dst_off, gen::vec2u dst_size, gen::gl::Filtering filter);
	void set_viewport(const Viewport& viewport);
	void set_scissor(const Scissor& scissor);
	void set_program(const Program& program);

	//Drawing
	void draw(uint32_t vertex_count, uint32_t first_vertex = 0, uint32_t instance_count = 1, uint32_t first_instance = 0);
	void draw_indexed(uint32_t index_count, uint32_t first_index = 0, int32_t first_vertex = 0, uint32_t instance_count = 1, uint32_t first_instance = 0);
	void draw_indirect(Buffer buffer, uint32_t offset = 0, uint32_t count = 1, uint32_t stride = 0);
	void draw_indexed_indirect(Buffer buffer, uint32_t offset = 0, uint32_t count = 1, uint32_t stride = 0);

	//Compute
	void dispatch(uint32_t x, uint32_t y, uint32_t z);
	void memory_barrier(uint32_t barrier);
};

/*struct GEN_API Context {
	//render pass
	virtual void start_render_pass(const RenderPassPtr& render_pass) = 0;
	virtual void end_render_pass() = 0;

	//Inside render pass
	virtual void bind_pipeline(const PipelinePtr& pipeline) = 0;
};

GEN_API System* create(gen::window::Window* window);*/
}
