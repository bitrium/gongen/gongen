// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "gl_defines.hpp"
#include <gongen/common/defines.hpp>
#include <string.h>

namespace gen::gl::internal {
	const static uint32_t MAX_BUFFER_SLOTS = 16;

	struct GLState {
		struct IndexedBuffer {
			uint32_t buffer;
			size_t offset;
			size_t size;
		};
		struct VertexFormat {
			uint32_t size;
			uint32_t type;
			bool normalized;
			uint32_t offset;
		};

		//Buffers
		uint32_t STATE_GL_ARRAY_BUFFER;
		uint32_t STATE_GL_COPY_READ_BUFFER;
		uint32_t STATE_GL_COPY_WRITE_BUFFER;
		uint32_t STATE_GL_ELEMENT_ARRAY_BUFFER;
		uint32_t STATE_GL_PIXEL_PACK_BUFFER;
		uint32_t STATE_GL_PIXEL_UNPACK_BUFFER;
		uint32_t STATE_GL_UNIFORM_BUFFER;
		uint32_t STATE_GL_DRAW_INDIRECT_BUFFER;
		//Indexed Buffers
		IndexedBuffer INDEXED_GL_UNIFORM_BUFFER[MAX_BUFFER_SLOTS];
		IndexedBuffer INDEXED_GL_STORAGE_BUFFER[MAX_BUFFER_SLOTS];
		//Textures and Samplers
		uint32_t TEXTURE[MAX_TEXTURE_SLOTS];
		uint32_t SAMPLER[MAX_TEXTURE_SLOTS];
		uint32_t TEXTURE_UNIT;
		//Vertex stuff
		uint32_t VAO;
		bool VERTEX_ATTRIB_ENABLED[gen::gl::MAX_VERTEX_ATTRIBUTES];
		uint32_t VERTEX_ATTRIB_BINDING[gen::gl::MAX_VERTEX_ATTRIBUTES];
		uint32_t VERTEX_BINDING_DIVISOR[gen::gl::MAX_VERTEX_BINDINGS];
		IndexedBuffer VERTEX_BUFFER[gen::gl::MAX_VERTEX_BINDINGS];
		VertexFormat VERTEX_FORMAT[gen::gl::MAX_VERTEX_ATTRIBUTES];
		//Framebuffer
		uint32_t STATE_GL_DRAW_FRAMEBUFFER;
		uint32_t STATE_GL_READ_FRAMEBUFFER;
		float CLEAR_COLOR[4];
		float CLEAR_DEPTH;
		uint32_t DRAW_BUFFERS[gen::gl::MAX_COLOR_ATTACHMENTS];
		uint32_t DRAW_BUFFERS_COUNT;
		//Viewport
		int32_t VIEWPORT_X, VIEWPORT_Y, VIEWPORT_W, VIEWPORT_H;
		float DEPTH_MIN, DEPTH_MAX;
		//Scissor
		bool SCISSOR_ENABLE;
		int32_t SCISSOR_X, SCISSOR_Y, SCISSOR_W, SCISSOR_H;
		//Rasterizer
		uint32_t CULL_MODE;
		bool CULL_FACE;
		uint32_t FRONT_FACE;
		//Depth Stencil
		uint32_t DEPTH_FUNC;
		bool DEPTH_MASK;
		bool DEPTH_TEST;
		//Blend
		bool BLEND_ENABLE;
		uint32_t BLEND_EQUATION_RGB;
		uint32_t BLEND_EQUATION_ALPHA;
		uint32_t BLEND_SRC_RGB;
		uint32_t BLEND_SRC_ALPHA;
		uint32_t BLEND_DST_RGB;
		uint32_t BLEND_DST_ALPHA;
		float BLEND_COLOR[4];
		//Misc
		uint32_t PROGRAM;
		bool PRIMITIVE_RESTART;
		uint32_t PRIMITIVE_RESTART_INDEX;

		GLState() {
			memset(this, 0, sizeof(GLState));
			for (size_t i = 0; i < gen::gl::MAX_VERTEX_ATTRIBUTES; i++) VERTEX_ATTRIB_BINDING[i] = UINT32_MAX;
		}

		void bind_buffer(uint32_t target, uint32_t buffer);
		void bind_storage_buffer(uint32_t index, uint32_t buffer, size_t offset, size_t size);
		void bind_uniform_buffer(uint32_t index, uint32_t buffer, size_t offset, size_t size);

		void bind_texture(uint32_t target, uint32_t slot, uint32_t texture);
		void bind_sampler(uint32_t slot, uint32_t sampler);

		void bind_vao(uint32_t vao);
		void bind_vertex_buffer(uint32_t index, uint32_t buffer, size_t offset, size_t stride);
		void attrib_enable(uint32_t attrib, bool enabled);
		void vertex_attrib_binding(uint32_t attrib, uint32_t binding);
		void vertex_binding_divisor(uint32_t binding, uint32_t divisor);
		void vertex_format(uint32_t attrib, uint32_t size, uint32_t type, bool normalized, uint32_t offset);

		void bind_fbo(uint32_t target, uint32_t fbo);
		void clear_color(float r, float g, float b, float a);
		void clear_depth(float v);
		void draw_buffers(uint32_t* buffers, size_t count);

		void viewport(int32_t x, int32_t y, int32_t w, int32_t h);
		void depth_min_max(float min, float max);

		void scissor_enable(bool enable);
		void scissor(int32_t x, int32_t y, int32_t w, int32_t h);

		void depth_func(uint32_t mode);
		void depth_mask(bool enable);
		void depth_test(bool enable);

		void cull_mode(uint32_t mode);
		void cull_face(bool enable);
		void front_face(uint32_t mode);

		void blend(bool enable);
		void blend_equation(uint32_t rgb, uint32_t alpha);
		void blend_func(uint32_t src_rgb, uint32_t dst_rgb, uint32_t src_alpha, uint32_t dst_alpha);
		void blend_color(float r, float g, float b, float a);

		void bind_program(uint32_t program);
		void primitive_restart(bool enable);
		void primitive_restart_index(uint32_t index);
	};
}
