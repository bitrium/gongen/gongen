// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/hash.hpp>
#include <gongen/common/math/vector.hpp>
#include <gongen/common/containers/string.hpp>
#include <gongen/common/containers/buffer.hpp>
#include <gongen/common/containers/pointers.hpp>
#include <gongen/common/math/color.hpp>

namespace gen::res {
class KindHandler;
} // namespace gen::res

namespace gen::gl {
const static size_t MAX_COLOR_ATTACHMENTS = 8;
const static size_t MAX_VERTEX_ATTRIBUTES = 12;
const static size_t MAX_VERTEX_BINDINGS = 6;
const static size_t MAX_TEXTURE_SLOTS = 8;

class Device;
namespace internal {
struct Buffer { virtual ~Buffer() {} };
struct Texture { virtual ~Texture() {} };
struct Sampler { virtual ~Sampler() {} };
struct Program { virtual ~Program() {} };
};

typedef gen::SharedPtr<internal::Buffer> Buffer;
typedef gen::SharedPtr<internal::Texture> Texture;
typedef gen::SharedPtr<internal::Sampler> Sampler;
typedef gen::SharedPtr<internal::Program> Program;

enum class Format {
	//Normalized
	R8, R8_SNORM, RG8, RG8_SNORM,
	R16, R16_SNORM, RG16, RG16_SNORM,
	RGBA8, RGBA8_SNORM, SRGB8_ALPHA8,
	RGBA16, RGBA16_SNORM,

	//Integer
	R8I, R8UI, R16I, R16UI, R32I, R32UI,
	RG8I, RG8UI, RG16I, RG16UI, RG32I, RG32UI,
	RGBA8I, RGBA8UI, RGBA16I, RGBA16UI, RGBA32I, RGBA32UI,

	//Floating point
	R16F, R32F, RG16F, RG32F, RGBA16F, RGBA32F,
	RGB32I, RGB32UI, RGB32F,

	//Other
	RGB10_A2, RGB10_A2UI, R11F_G11F_B10F, RGB9_E5,

	//Depth
	D16, D24_S8, D32F,

	COUNT
};

enum class Usage {
	STATIC,
	DYNAMIC
};
struct BufferDesc {
	size_t size;
	Usage usage = Usage::STATIC;
	const void* data = nullptr;
};

enum TextureType {
	TEXTURE_2D,
	TEXTURE_3D,
	TEXTURE_CUBEMAP
};
struct TextureDesc {
	Format format;
	vec3z size = vec3z(1);
	TextureType type = TextureType::TEXTURE_2D;
	size_t mip_levels = 1;
	size_t layers = 0; //0 means non array texture
	bool generate_mipmaps = false;
	const void* data = nullptr;
};

enum class Filtering {
	NEAREST,
	LINEAR
};
enum class TextureWrap {
	CLAMP,
	REPEAT,
	MIRRORED_REPEAT
};
struct SamplerDesc {
	Filtering min_filter = Filtering::LINEAR;
	Filtering mag_filter = Filtering::LINEAR;
	Filtering mip_filter = Filtering::LINEAR;
	TextureWrap wrap[3] = { TextureWrap::CLAMP };
	float anisotropy = 1.0f;
};

struct ShaderStage {
	gen::String code;
	gen::String name;
};
struct ProgramDesc {
	ShaderStage vertex;
	ShaderStage fragment;
	ShaderStage compute;
	gen::DynamicArray<gen::String> defines;
};
/**
 * @brief Creates a kind handler for shaders.
 * The handler accepts both joined shader files (vertex and fragment stages in one '.glsl' file) and separated shader
 * files (in '.vert.glsl' and '.frag.glsl' or '.comp.glsl').
 * @param device GL Device. If this is defined, items are compiled Programs. If it is null, items are ProgramDescs.
 * @param kind Kind id.
 * @param definitions Definitions added to every program.
 * @return Shader resource kind handler.
 */
GEN_API res::KindHandler* program_resource_handler(Device* device = nullptr, const String& kind = "shader", const DynamicArray<String>& definitions = gen::DynamicArray<String>());

enum class TextureAccess {
	READ,
	WRITE,
	READ_WRITE
};

struct VertexAttrib {
	uint32_t binding;
	gen::gl::Format format;
	uint32_t offset;
};
struct VertexBinding {
	uint32_t stride;
	bool per_instance;
};

struct GEN_API VertexFormat {
	VertexAttrib attrib[MAX_VERTEX_ATTRIBUTES];
	VertexBinding binding[MAX_VERTEX_BINDINGS];
	uint32_t attrib_count = 1;
};

enum class PrimitiveTopology {
	POINT_LIST,
	LINE_LIST,
	LINE_STRIP,
	TRIANGLE_LIST,
	TRIANGLE_STRIP
};
enum class CullMode {
	NONE,
	BACK,
	FRONT,
	FRONT_AND_BACK
};
enum class FillMode {
	SOLID,
	LINE,
	POINT
};
struct GEN_API PrimitiveState {
	PrimitiveTopology topology = PrimitiveTopology::TRIANGLE_LIST;
	CullMode cull_mode = CullMode::NONE;
	bool front_ccw = true;
	FillMode fill_mode = FillMode::SOLID;
};

enum class CompareOp {
	NEVER,
	LESS,
	EQUAL,
	LESS_OR_EQUAL,
	GREATER,
	NOT_EQUAL,
	GREATER_OR_EQUAL,
	ALWAYS
};
struct GEN_API DepthState {
	bool enabled = false;
	bool depth_test = false;
	bool depth_write = true;
	CompareOp compare_op = CompareOp::LESS;
};

enum class BlendFactor {
	ZERO,
	ONE,

	SRC_COLOR,
	ONE_MINUS_SRC_COLOR,
	DST_COLOR,
	ONE_MINUS_DST_COLOR,

	SRC_ALPHA,
	ONE_MINUS_SRC_ALPHA,
	DST_ALPHA,
	ONE_MINUS_DST_ALPHA
};
enum class BlendOperation {
	ADD,
	SUBTRACT,
	REVERSE_SUBTRACT,
	MIN,
	MAX
};
const static uint8_t WRITE_R = 0x1;
const static uint8_t WRITE_G = 0x2;
const static uint8_t WRITE_B = 0x4;
const static uint8_t WRITE_A = 0x8;
struct GEN_API BlendState {
	bool enabled = false;
	BlendFactor src_color = BlendFactor::SRC_ALPHA;
	BlendFactor dst_color = BlendFactor::ONE_MINUS_SRC_ALPHA;
	BlendOperation color_op = BlendOperation::ADD;
	BlendFactor src_alpha = BlendFactor::ONE;
	BlendFactor dst_alpha = BlendFactor::ZERO;
	BlendOperation alpha_op = BlendOperation::ADD;
	uint8_t write_mask = WRITE_R | WRITE_G | WRITE_B | WRITE_A;
	float blend_color[4];
};

struct GEN_API Viewport {
	vec2u pos = vec2u(0, 0);
	vec2u size = vec2u(0, 0);
	float min_depth = -1.0f;
	float max_depth = 1.0f;
};
struct GEN_API Scissor {
	bool enabled = false;
	vec2u pos;
	vec2u size;
};

struct FramebufferState {
	Texture color_attachments[gen::gl::MAX_COLOR_ATTACHMENTS];
	Texture depth_stencil_attachment;
	size_t color_attachment_count = 1;

	size_t color_attachment_layer[gen::gl::MAX_COLOR_ATTACHMENTS] = { 0 };
	size_t depth_stencil_attachment_layer = 0;

	uint64_t hash() const {
		uint64_t out = 0;
		for (size_t i = 0; i < color_attachment_count; i++) {
			out = gen::hash_combine(color_attachments[i].hash(), out);
			out = gen::hash_combine(color_attachment_layer[i], out);
		}
		if (depth_stencil_attachment.get()) {
			out = gen::hash_combine(depth_stencil_attachment.hash(), out);
			out = gen::hash_combine(depth_stencil_attachment_layer, out);
		}
		return out;
	}
};

struct ClearColorDesc {
	bool clear = false;
	gen::Color clear_color = gen::Color::greyscale(0.0f);
};

struct ClearDesc {
	ClearColorDesc color[gen::gl::MAX_COLOR_ATTACHMENTS];

	bool do_clear_depth = false;
	float clear_depth = 0.0f;
};

#pragma pack(push, 1)
struct DrawIndirectCommand {
	uint32_t count;
	uint32_t instance_count;
	uint32_t first_vertex;
	uint32_t first_instance; //must be 0
};
struct DrawIndexedIndirectCommand {
	uint32_t count;
	uint32_t instance_count;
	uint32_t first_index;
	uint32_t first_vertex;
	uint32_t first_instance; //must be 0
};
#pragma pack(pop)

namespace Barrier {
	enum Barrier {
		VERTEX_ATTRIB = 1,
		INDEX = 2,
		UNIFORM = 4,
		TEXTURE_FETCH = 8,
		SHADER_IMAGE_ACCESS = 16,
		COMMAND = 32,
		PIXEL_BUFFER = 64,
		TEXTURE_UPDATE = 128,
		BUFFER_UPDATE = 256,
		FRAMEBUFFER = 512,
		TRANSFORM_FEEDBACK = 1024,
		ATOMIC_COUNTER = 2048,
		SHADER_STORAGE = 4096
	};
}
}
