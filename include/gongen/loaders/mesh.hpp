// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "loaders.hpp"
#include <gongen/common/containers/buffer.hpp>
#include <gongen/common/containers/string.hpp>
#include <gongen/common/math/vector.hpp>
#include <gongen/common/math/color.hpp>
#include <gongen/common/math/matrix.hpp>

namespace gen::loaders {
	class GEN_API Mesh {
	public:
		enum AttributeType {
			UNKNOWN,
			FLOAT,
			UINT8,
			UINT16,
			UINT32,

			VEC2_FLOAT,
			VEC2_UINT8,
			VEC2_UINT16,
			VEC2_UINT32,

			VEC3_FLOAT,
			VEC3_UINT8,
			VEC3_UINT16,
			VEC3_UINT32,
			
			VEC4_FLOAT,
			VEC4_UINT8,
			VEC4_UINT16,
			VEC4_UINT32
		};
		struct Attribute {
			gen::String name;
			AttributeType type;
			bool normalized;
			uint32_t count;
			union {
				float* f32;
				uint8_t* u8;
				uint16_t* u16;
				uint32_t* u32;

				gen::vec2f* f32_2;
				gen::vec2u* u32_2;
				gen::vec2u16* u16_2;
				gen::vec2u8* u8_2;

				gen::vec3f* f32_3;
				gen::vec3u* u32_3;
				gen::vec3u16* u16_3;
				gen::vec3u8* u8_3;

				gen::vec4f* f32_4;
				gen::vec4u* u32_4;
				gen::vec4u16* u16_4;
				gen::vec4u8* u8_4;
				gen::Color* color;
				gen::Color8* color8;
			};
		};
		struct Skin;
		struct PropertyStr {
			gen::String name;
			gen::String value;
		};
		struct PropertyNumber {
			gen::String name;
			double value;
		};
		struct Material {
			gen::String name;
			gen::Color base_color;
			float metalness;
			float roughness;
			gen::DynamicArray<PropertyStr> properties_str;
			gen::DynamicArray<PropertyNumber> properties;
		};
		struct Primitive {
			Material* material;
			gen::DynamicArray<Attribute> attributes;
		};
		struct _Mesh {
			gen::String name;
			gen::DynamicArray<Primitive> primitives;
		};
		struct Node {
			gen::String name;
			Node* parent = nullptr;
			_Mesh* mesh = nullptr;
			Skin* skin = nullptr;
			gen::Buffer<Node*> children;
			gen::mat4f tranform_matrix;
		};
		struct Scene {
			gen::String name;
			gen::Buffer<Node*> nodes;
		};
		struct Skin {
			gen::String name;
			size_t mat_count = 0;
			gen::mat4f* mat = nullptr;
			Node* root = nullptr;
			gen::Buffer<Node*> joints;
		};
		enum class Interpolation {
			STEP,
			LINEAR
		};
		struct AnimationSampler {
			Attribute input;
			Attribute output;
			Interpolation interpolation;
		};
		enum class AnimationPath {
			TRANSLATION,
			ROTATION,
			SCALE
		};
		struct AnimationChannel {
			AnimationSampler* sampler;
			Node* target;
			AnimationPath path;
		};
		struct Animation {
			gen::String name;
			gen::Buffer<AnimationChannel> channels;
			gen::Buffer<AnimationSampler> samplers;
		};
	private:
		bool own_source = false;
		gen::SeekableDataSource* source = nullptr;

		bool opened = false;
	public:
		gen::ByteBuffer data;
		gen::DynamicArray<Material> materials;
		gen::DynamicArray<_Mesh> meshes;
		gen::DynamicArray<Node> nodes;
		gen::DynamicArray<Scene> scenes;
		gen::DynamicArray<Skin> skins;
		gen::DynamicArray<Animation> animations;
		Scene* main_scene;

		Mesh() {}
		Mesh(gen::SeekableDataSource* source, bool own_source) { open(source, own_source); }
		~Mesh() { close(); }

		bool is_open() { return opened; }

		bool open(gen::SeekableDataSource* source, bool own_source);
		void close();
	};
}
