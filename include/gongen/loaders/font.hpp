// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "loaders.hpp"
#include <gongen/common/math/vector.hpp>
#include <gongen/gl/gl.hpp>

namespace gen::loaders {
namespace internal {
//GenFont File structures
const static uint32_t GFNT_MAGIC = GEN_FOURCC_STR("GFT\0xC8");
const static uint8_t GFNT_VERSION = 1;
#pragma pack(push, 1)
struct gfnt_header {
	uint32_t magic = GFNT_MAGIC;
	uint32_t kerning_pairs = 0;
	uint32_t compressed_size = 0;
	uint32_t uncompressed_size = 0;
	float range = 0.0;
	uint16_t glyph_count = 0;
	uint8_t version = GFNT_VERSION;
	uint8_t flags = 0;
	uint16_t header_size = sizeof(gfnt_header);
	uint8_t margin = 0;
	uint8_t reserved = 0;
};
struct gfnt_kern {
	uint16_t left;
	uint16_t right;
	uint16_t x;
	uint16_t y;
};
struct gfnt_glyph {
	uint32_t codepoint;
	uint32_t offset;
	uint32_t size;

	uint16_t bearingX;
	uint16_t bearingY;
	uint16_t advance;
	uint16_t width;
	uint16_t height;

	uint8_t bitmapW;
	uint8_t bitmapH;
};
#pragma pack(pop)
}

struct GEN_API Font {
	struct KernPair {
		uint16_t left, right;
		float value;
	};
	struct Glyph {
		uint32_t codepoint;
		uint32_t data_offset;

		//uint16_t first_kern_pair;
		//uint16_t kern_pair_count;

		gen::vec2f bearing;
		gen::vec2f size;
		float advance;

		gen::vec2u8 bitmap_size;
		gen::vec2u8 bitmap_pos;

		gen::vec2u16 uv_pos; //as 16 bit normalized uint's (0-1)
		gen::vec2u16 uv_size; //as 16 bit normalized uint's (0-1)
	};
private:
	bool own_source = false;
	gen::SeekableDataSource* source = nullptr;
	bool opened = false;

	internal::gfnt_header header;
	gen::Buffer<KernPair> kern_pairs;
	gen::Buffer<Glyph> glyphs;
	gen::ByteBuffer data;
public:
	Font() {}
	Font(gen::SeekableDataSource* source, bool own_source) { open(source, own_source); }
	~Font() { close(); }

	bool is_open() { return opened; }

	Glyph* get_glyph(uint32_t codepoint);

	bool open(gen::SeekableDataSource* source, bool own_source);
	void close();
	gen::gl::Texture build(gen::gl::Device* device, uint16_t texture_size);
};
}
