// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "loaders.hpp"

namespace gen::loaders {
enum class ImageFormat {
	R = 1,
	RG = 2,
	RGB = 3,
	RGBA = 4,
	BC1 = 101,
	BC2 = 102,
	BC3 = 103,
	BC4 = 104,
	BC5 = 105,
	BC6 = 106,
	BC7 = 107,
};

class GEN_API Image {
	bool own_source = false;
	gen::SeekableDataSource* source = nullptr;

	uint32_t width = 0;
	uint32_t height = 0;
	uint32_t comp = 0;
	uint32_t format = 0;
	bool compressed = false;

	void* data = nullptr;
public:
	Image() {}
	Image(gen::SeekableDataSource* source, bool own_source, uint32_t req_comp = 0) { open(source, own_source, req_comp); }
	~Image() { close(); }

	bool is_open() { return (width != 0); }

	bool open(gen::SeekableDataSource* source, bool own_source, uint32_t req_comp = 0);
	void close();

	uint32_t get_width() const { return width; }
	uint32_t get_height() const { return height; }
	uint32_t get_components() const { return comp; }
	uint32_t get_format() const { return format; }
	uint8_t* get_data() const { return (uint8_t*)data; }
	size_t get_data_size() const;
	bool is_compressed() const { return compressed; }
};
}
