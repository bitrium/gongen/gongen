// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "loaders.hpp"

namespace gen::loaders {
	class GEN_API Audio {
		bool own_source = false;
		gen::SeekableDataSource* source = nullptr;

		uint32_t format = 0;
		void* impl = nullptr;

		uint32_t bitrate = 0;
		uint32_t channels = 0;
		uint32_t sample_rate = 0;
		uint64_t frame_count = 0;
	public:
		Audio() {}
		Audio(gen::SeekableDataSource* source, bool own_source) { open(source, own_source); }
		~Audio() { close(); }

		bool is_open() { return (impl != nullptr); }
		
		bool open(gen::SeekableDataSource* source, bool own_source);
		void close();

		uint32_t get_bitrate() { return bitrate; }
		uint32_t get_channels() { return channels; }
		uint32_t get_sample_rate() { return sample_rate; }
		uint64_t get_frame_count() { return frame_count; }
		double length() { return (((double)frame_count) / ((double)sample_rate)); }

		// returns written frame count
		uint64_t get_samples(int16_t* out, uint64_t frame_count, bool stereo);
		uint64_t get_samples_float(float* out, uint64_t frame_count, bool stereo);

		void seek(uint64_t sample_pos);
		void seek_secs(double seconds) { seek((uint64_t) (seconds * sample_rate)); }
		uint64_t tell();
	};
}
