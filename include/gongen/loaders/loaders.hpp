// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/error.hpp>
#include <gongen/common/data_source.hpp>

namespace gen::loaders {
	GEN_EXCEPTION_TYPE(Exception, RuntimeException, "Loader Error");
	GEN_EXCEPTION_TYPE(FormatException, Exception, "Loader Unsupported Format Error");
	GEN_EXCEPTION_TYPE(OpenException, Exception, "Loader Open Error");
}
