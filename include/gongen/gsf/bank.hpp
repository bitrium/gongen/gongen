// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/hashmap.hpp>
#include "data.hpp"

namespace gen::gsf {
class Bank {
	HashMap<String, Descriptor> descs;
	HashMap<String, Schema> schemas;
	const char* dir;
	friend Bank* get_schema_bank(const char* path);
	Bank(const char* path);
public:
	Schema schema(const String& name);
	Schema schema(const String& name, Context& ctx);
};
Bank* get_schema_bank(const char* path);

#define GEN_GSF_BANK(NAME) gen::gsf::get_schema_bank(GEN_GSF_SCHEMA_BANK_##NAME)
} // namespace gen::gsf
