// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/values.hpp>
#include "types.hpp"

namespace gen {
class SeekableDataSource;
class SeekableWritableDataSource;
} // namespace gen

namespace gen::gsf {

class GEN_API Descriptor {
public:
	void* impl;
	bool own;
	Descriptor() : own(false) { this->impl = nullptr; }
	Descriptor(const Descriptor& other) : own(false) { this->impl = other.impl; }
	Descriptor(void* impl, bool own = false) : own(own) { this->impl = impl; }
	Descriptor& operator=(const Descriptor& other) { this->destroy(); this->impl = other.impl; return *this; }

	Descriptor(const String& group, const String& name, const String& version);
	explicit Descriptor(const String& string);
	Descriptor(const char* buf, size_t len);
	~Descriptor();
	void destroy();

	char* to_string(char* buf, size_t buf_size) const;
	String to_string() const;

	void edit(const String& group, const String& name, const String& version);

	String get_group() const;
	String get_name() const;
	String get_version() const;

	bool match(const Descriptor& other);
};

class Context;
class Class;
class Enum;
class GEN_API Schema {
public:
	void* impl;
	bool own;
	Schema() : own(false) { this->impl = nullptr; }
	Schema(const Schema& other) : own(false) { this->impl = other.impl; }
	Schema(Schema&& other) : own(other.own) { this->impl = other.impl; other.impl = nullptr; }
	Schema(void* impl, bool own = false) : own(own) { this->impl = impl; }
	Schema& operator=(const Schema& other) { this->destroy(); this->impl = other.impl; return *this; }
	Schema& operator=(Schema&& moved) { this->destroy(); this->own = moved.own; this->impl = moved.impl; moved.own = false; return *this; }

	Schema(const Descriptor& descriptor, const TypingRepr& root, size_t class_count);
	~Schema();
	void destroy();

	Descriptor get_descriptor() const;
	void set_descriptor(const Descriptor& descriptor);

	Context get_context() const;

	Typing get_root() const;
	void set_root(const TypingRepr& typing);

	size_t class_count() const;
	Class add_class(const String& name, size_t label_count);
	Optional<Class> get_class(size_t index);

	size_t enum_count() const;
	Enum add_enum(const String& name, size_t variant_count);
	Optional<Enum> get_enum(size_t index);
	// void remove_enum(size_t index);

	static Schema txt_read_to_ctx(Context ctx, const String& string, int flags = 0);
	static Schema txt_read_to_ctx(Context ctx, const char* buf, size_t buf_size, int flags = 0);
	static Schema txt_read_to_ctx(Context ctx, SeekableDataSource* data, int flags = 0);
	static Schema txt_read(const String& string, int flags = 0);
	static Schema txt_read(const char* buf, size_t buf_size, int flags = 0);
	static Schema txt_read(SeekableDataSource* data, int flags = 0);
	static String txt_write(const Schema& schema, int flags = 0);
	static char* txt_write(const Schema& schema, char* buf, size_t buf_size, int flags = 0);
	static void txt_write(const Schema& schema, SeekableWritableDataSource* data, int flags = 0);
};

class Label;
class GEN_API Class {
public:
	void* impl;
	Class() { this->impl = nullptr; }
	explicit Class(void* impl) { this->impl = impl; }
	Class& operator=(const Class& other) { this->impl = other.impl; return *this; }

	Type as_type() const;
	String get_name();
	void set_name(String& string);

	size_t label_count() const;
	Label add_label(const String& key, const TypingRepr& typing);
	Optional<Label> get_label(size_t index);
	Optional<Label> get_label_at_key(const String& key);
	size_t index_at_label(Label label);
	size_t index_at_key(const String& key);
	void remove_label(size_t index);
};

class Value;
class GEN_API Label {
public:
	void* impl;
	explicit Label(void* impl) { this->impl = impl; }
	Label& operator=(const Label& other) { this->impl = other.impl; return *this; }

	String get_key() const;
	void set_key(const String& key);

	Typing get_typing() const;
	void set_typing(const TypingRepr& typing);

	Optional<Value> get_default() const;
	Value access_default();

	Optional<String> get_doc() const;
	void set_doc(const String& doc);
};

class Variant;
class GEN_API Enum {
public:
	void* impl;
	Enum() { this->impl = nullptr; }
	explicit Enum(void* impl) { this->impl = impl; }
	Enum& operator=(const Enum& other) { this->impl = other.impl; return *this; }

	Type as_type() const;

	String get_name() const;

	size_t variant_count() const;
	Variant add_variant(const String& name);
	void remove_variant(size_t index);
	Optional<Variant> get_variant(size_t index);
	Optional<Variant> get_variant_by_name(const String& name);
};

class GEN_API Variant {
public:
	void* impl;
	Variant() { this->impl = nullptr; }
	explicit Variant(void* impl) { this->impl = impl; }
	Variant& operator=(const Variant& other) { this->impl = other.impl; return *this; }

	String get_name() const;
	Optional<String> get_doc() const;
	void set_doc(const String& doc);
};


class Value;
class GEN_API Article {
public:
	void* impl;
	bool own;
	Article() : own(false) { this->impl = nullptr; }
	Article(const Article& other) : own(false) { this->impl = other.impl; }
	Article(Article&& moved)  noexcept : own(false) { this->own = moved.own; this->impl = moved.impl; moved.own = false; }
	explicit Article(void* impl, bool own = false) : own(own) { this->impl = impl; }
	Article& operator=(const Article& other) { this->destroy(); this->impl = other.impl; return *this; }
	Article& operator=(Article&& moved) noexcept { this->destroy(); this->own = moved.own; this->impl = moved.impl; moved.own = false; return *this; }

	Article(const Schema& schema);
	~Article();
	void destroy();

	Schema get_schema() const;

	Value access_root() const;

	static Article bin_read_with_desc(void* user, Schema (*schema_getter)(void* user, const Descriptor& descriptor), const ByteBuffer& data, int flags = 0);
	static Article bin_read_with_desc(void* user, Schema (*schema_getter)(void* user, const Descriptor& descriptor), const void* buf, size_t buf_size, int flags = 0);
	static Article bin_read_with_desc(void* user, Schema (*schema_getter)(void* user, const Descriptor& descriptor), SeekableDataSource* data, int flags = 0);
	static Article bin_read(const Schema& schema, const ByteBuffer& data, int flags = 0);
	static Article bin_read(const Schema& schema, const void* buf, size_t buf_size, int flags = 0);
	static Article bin_read(const Schema& schema, SeekableDataSource* data, int flags = 0);
	static Article bin_read_ctx(const Context& ctx, const ByteBuffer& data, int flags = 0);
	static Article bin_read_ctx(const Context& ctx, const void* buf, size_t buf_size, int flags = 0);
	static Article bin_read_ctx(const Context& ctx, SeekableDataSource* data, int flags = 0);
	static ByteBuffer bin_write(const Article& article, int flags = 0);
	static uint8_t* bin_write(const Article& article, uint8_t* buf, size_t buf_size, int flags = 0);
	static void bin_write(const Article& article, SeekableWritableDataSource* data, int flags = 0);
	static Article txt_read(const Schema& schema, const String& string, int flags = 0);
	static Article txt_read(const Schema& schema, const char* buf, size_t buf_size, int flags = 0);
	static Article txt_read(const Schema& schema, SeekableDataSource* data, int flags = 0);
	static Article txt_read_ctx(const Context& ctx, const String& string, int flags = 0);
	static Article txt_read_ctx(const Context& ctx, const char* buf, size_t buf_size, int flags = 0);
	static Article txt_read_ctx(const Context& ctx, SeekableDataSource* data, int flags = 0);
	static String txt_write(const Article& article, int flags = 0);
	static char* txt_write(const Article& article, char* buf, size_t buf_size, int flags = 0);
	static void txt_write(const Article& article, SeekableWritableDataSource* data, int flags = 0);
};

// TODO iterator
class GEN_API Object {
public:
	void* impl;
	Object() { this->impl = nullptr; }
	explicit Object(void* impl) { this->impl = impl; }
	Object& operator=(const Object& other) { this->impl = other.impl; return *this; }

	Class get_class() const;
	Optional<Value> access_at_label(const Label& label);
	Optional<Value> access_at_key(const String& key);

	size_t apply_defaults();
	size_t count_unset() const;
};

// TODO iterator
class GEN_API List {
public:
	void* impl;
	List() { this->impl = nullptr; }
	explicit List(void* impl) { this->impl = impl; }
	List& operator=(const List& other) { this->impl = other.impl; return *this; }

	Typing get_typing() const;

	size_t get_size() const;
	void resize(size_t size);

	size_t element_count() const;
	Optional<Value> get_element(size_t index);
	Value add_element();
};

// TODO iterator
class GEN_API Map {
public:
	void* impl;
	Map() { this->impl = nullptr; }
	explicit Map(void* impl) { this->impl = impl; }
	Map& operator=(const Map& other) { this->impl = other.impl; return *this; }

	Typing get_typing() const;

	size_t get_size() const;
	void resize(size_t size);

	size_t element_count() const;
	Optional<String> get_key(size_t index) const;
	Optional<Value> get_value(size_t index);
	Optional<Value> get_value_at_key(const String& key);
	Value add_element(const String& key);
};

class GEN_API Array {
public:
	void* impl;
	Array() { this->impl = nullptr; }
	explicit Array(void* impl) { this->impl = impl; }
	Array& operator=(const Array& other) { this->impl = other.impl; return *this; }

	Type get_type() const;

	size_t get_size() const;
	void resize(size_t size);

	int8_t* as_i8();
	int16_t* as_i16();
	int32_t* as_i32();
	int64_t* as_i64();
	uint8_t* as_u8();
	uint16_t* as_u16();
	uint32_t* as_u32();
	uint64_t* as_u64();
	float* as_f32();
	double* as_f64();
};

class GEN_API Value {
public:
	void* impl;
	Value() { this->impl = nullptr; }
	explicit Value(void* impl) { this->impl = impl; }
	Value& operator=(const Value& other) { this->impl = other.impl; return *this; }

	bool is_set() const;
	Typing get_typing() const;
	Optional<Type> get_type() const;

	void unset();
	void copy(const Value& source);

	void decode_null() const; // no other purpose than to "assert" that it is null
	void encode_null();
	Object decode_object() const;
	Object encode_object(const Class* type = nullptr);
	Object as_object(const Class* type = nullptr);
	Map decode_map() const;
	Map encode_map(const TypingRepr* typing, size_t size);
	Map as_map(const TypingRepr* typing, size_t size);
	List decode_list() const;
	List encode_list(const TypingRepr* typing, size_t size);
	List as_list(const TypingRepr* typing, size_t size);
	Array decode_array() const;
	Array encode_array(const TypeRepr* type, size_t size);
	Array as_array(const TypeRepr* type, size_t size);
	Variant decode_enum() const;
	void encode_enum(const Enum* type, const Variant& variant);
	String decode_enum_str() const;
	void encode_enum_str(const Enum* type, const String& variant);
	String decode_str() const;
	void encode_str(const String& value);
	int8_t decode_i8() const;
	void encode_i8(int8_t value);
	int16_t decode_i16() const;
	void encode_i16(int16_t value);
	int32_t decode_i32() const;
	void encode_i32(int32_t value);
	int64_t decode_i64() const;
	void encode_i64(int64_t value);
	uint8_t decode_u8() const;
	void encode_u8(uint8_t value);
	uint16_t decode_u16() const;
	void encode_u16(uint16_t value);
	uint32_t decode_u32() const;
	void encode_u32(uint32_t value);
	uint64_t decode_u64() const;
	void encode_u64(uint64_t value);
	float decode_f32() const;
	void encode_f32(float value);
	double decode_f64() const;
	void encode_f64(double value);
};


class GEN_API Context {
public:
	void* impl;
	bool own;
	Context(const Context& other) : own(false) { this->impl = other.impl; }
	explicit Context(void* impl, bool own = false) : own(own) { this->impl = impl; }
	Context& operator=(const Context& other) { this->destroy(); this->impl = other.impl; return *this; }

	Context();
	~Context();
	void destroy();

	Schema new_schema(const Descriptor& descriptor, const TypingRepr& root, size_t class_count);
	Schema new_schema(const String& descriptor_name, const TypingRepr& root, size_t class_count);
	size_t schema_count() const;
	Optional<Schema> get_schema_by_idx(size_t index);
	Optional<Schema> get_schema(const Descriptor& descriptor);
};

} // namespace gen::gsf
