// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/error.hpp>

namespace gen::gsf {
GEN_EXCEPTION_TYPE(GsfException, RuntimeException, "GSF Error");
void throw_error(void* user = nullptr);
template<typename T>
inline T handle_error(T ret, void (*on_error)(void*) = throw_error, void* user = nullptr) {
	if (ret == 0) on_error(user);
	return ret;
}
}
