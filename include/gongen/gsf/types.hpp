// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/string.hpp>

namespace gen {
} // namespace gen

namespace gen::gsf {

struct GEN_API TypeRepr {
	virtual void* get_impl() const = 0;
	virtual ~TypeRepr() = default;
};
struct GEN_API TypingRepr {
	virtual void* get_impl() const = 0;
	virtual ~TypingRepr() = default;
};

struct GEN_API TypeString : public TypeRepr {
	String string;
	explicit TypeString(const String& string) { this->string = string; }
	void* get_impl() const override { return (void*) this->string.c_str(); }
};
struct GEN_API TypingString : public TypingRepr {
	String string;
	explicit TypingString(const String& string) { this->string = string; }
	void* get_impl() const override { return (void*) this->string.c_str(); }
};


class Typing;
class GEN_API Type : public TypeRepr {
	void* impl;
public:
	explicit Type(void* impl) { this->impl = impl; }
	inline void* get_impl() const override { return this->impl; }

	char* to_string_buf(char* buf, size_t buf_size) const;
	String to_string() const;

	// TODO match functions

	uint8_t scalar_width() const;
	bool is_primitive() const; // allows null
	bool is_null() const;
	bool is_string() const;
	bool is_scalar() const;
	bool is_int() const;
	bool is_uint() const;
	bool is_float() const;
	bool is_enum() const;
	bool is_container() const;
	bool is_collection() const;
	bool is_array() const;
	bool is_list() const;
	bool is_map() const;
	bool is_object() const;

	Type element_type() const;
	Typing element_typing() const;
	void* object_class() const;
	void* get_enum() const;
};

class GEN_API Typing : public TypingRepr {
	void* impl;
public:
	explicit Typing(void* impl) { this->impl = impl; }
	inline void* get_impl() const override { return this->impl; }

	Typing& with_type(const Type& type, bool is_default = false);
	Typing& with_default(size_t index);
	Typing& without_default();

	char* to_string_buf(char* buf, size_t buf_size) const;
	String to_string() const;

	size_t type_count() const;
	Optional<Type> get_type(size_t index) const;
	Optional<Type> get_default() const;

	// TODO match functions

	bool is_omnibus() const;
	size_t find(const Type& type) const;
};

} // namespace gen::gsf
