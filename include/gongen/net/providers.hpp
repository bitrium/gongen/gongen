// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/error.hpp>
#include <gongen/common/platform/system.hpp>
#include <gongen/common/event.hpp>
#include "protocol.hpp"
#include "communication.hpp"

namespace gen::net {

const uint32_t DEFAULT_MAX_PACKET_SIZE = 4 * 1024 * 1024;

GEN_EXCEPTION_TYPE(Exception, RuntimeException, "Network Error");
GEN_EXCEPTION_TYPE(InitException, Exception, "Network Init Error");
GEN_EXCEPTION_TYPE(ConnectException, Exception, "Network Connect Error");
GEN_EXCEPTION_TYPE(UpdateException, Exception, "Network Update Error");

struct GEN_API ConnectionEvent : public event::Event {
	bool connected; // false on disconnect
	uint32_t client_id; // for client providers, always 0
	ip::Endpoint endpoint;

	ConnectionEvent(void* provider) : event::Event("net/connection", provider) {
	}
};

struct GEN_API PacketCustom : public event::Event {
	uint32_t client_id;
	uint32_t type_id;
	ByteBuffer data;

	PacketCustom(void* provider) : event::Event("net/packet", provider) {
	}
};

#ifdef GEN_MOD_GSF
struct GEN_API PacketGsf : public event::Event {
	uint32_t client_id;
	gsf::Article gsf;

	PacketGsf(void* provider) : event::Event("net/gsf", provider) {
	}
};
#endif

class GEN_API NetworkProvider {
private:
	void* thread_data;
	std::atomic<bool> thread_stop_flag;
	Thread* thread = nullptr;
public:
	virtual ~NetworkProvider();

	/**
	 * @brief Read packets or other events and fill the event system.
	 *
	 * On client:
	 * Does not wait for new packets (use wait). If a large packet cannot be read immidiately,
	 * blocks until timeout passes and eventually defers the packet and returns.
	 * Also handles the defered packet (if present) at the beginning.
	 *
	 * On server:
	 * Does not block and the timeout has no effects. Handles all defered packets, but
	 * any delays are immidiately defered to the next update.
	 *
	 * @param limit Maximum amount of events to process (rest will be left for future calls).
	 * @return Amount of added events.
	 */
	virtual size_t update(size_t limit = 8) = 0;
	/**
	 * @brief Block until something is received or another event occurs.
	 * @param timeout Blocking timeout in seconds.
	 * @return Possible amount of waiting events or zero on timeout.
	 */
	virtual size_t wait(double timeout) = 0;

	/**
	 * @brief Creates a simple thread that keeps waiting for events and updating.
	 * @param delay Timeout (in seconds) between close checks; higher value lowers cost (but delays thread shutdown).
	 */
	void start_thread(double delay = 0.25, size_t update_limit = 16);
	/**
	 * @brief Stops the thread created with start_thread.
	 */
	void stop_thread();
	/**
	 * @brief Stops the thread created with start_thread and waits for it to complete.
	 */
	void join_thread();
	/**
	 * @brief Checks if thread is running.
	 */
	bool is_thread_running() { return !thread_stop_flag.load(); }
};

class GEN_API ClientProvider : public NetworkProvider {
	void* impl;
public:
	uint32_t max_packet_size = 524288;
	const ip::Endpoint server_address;
	ClientProvider(Ptr<Communication> communication, const ip::Endpoint& serer_address);
	~ClientProvider() override;

	size_t update(size_t limit = 8) override;
	size_t wait(double timeout) override;

	void status(double timeout);
	gnp::Response<gnp::ServerInfo> status_info();

	bool is_connected();

	/**
	 * @brief Connects to a server provider.
	 */
	void connect();
	/**
	 * @brief Disconnects from a server provider.
	 * If timeout is reached, the server may not be notified about the disconnection.
	 * @param reason Disconnection message to send to the server.
	 */
	void disconnect(const String& reason = "");

	/**
	 * @brief Write a custom packet.
	 * @param type Custom type id of the packet.
	 * @param data The data.
	 * @param size Size of the data.
	 */
	void send(uint32_t type, const void* data, size_t size);
	void send(uint32_t type, const ByteBuffer& data) {
		return this->send(type, data.ptr(), data.size());
	}
#ifdef GEN_MOD_GSF
	/**
	 * @brief Write a GSF packet.
	 * @param gsf GSF article to write.
	 */
	void send(const gsf::Article& gsf);
#endif
};

class GEN_API ServerProvider : public NetworkProvider {
	void* impl;
public:
	uint32_t max_packet_size = 524288;
	const ip::Endpoint address;
	ServerProvider(Ptr<Communication> communication, const ip::Endpoint& address);
	~ServerProvider() override;

	gnp::StatusHandler* status();

	bool is_open();
	void open();
	void close();

	size_t update(size_t limit = 8) override;
	size_t wait(double timeout) override;

	/**
	 * @brief Checks whether a client id is known by the server.
	 * @param client_id Checked client id.
	 * @return Whether the client is connected.
	 */
	bool is_connected(uint32_t client_id);
	/**
	 * @brief Disconnect client.
	 * @param client_id Identifier of the client obtained in a ConnectionEvent.
	 * @param reason Disconnection reason to send to the client.
	 */
	void disconnect(uint32_t client_id, const String& reason = "");

	void send(uint32_t client_id, uint32_t type, const void* data, size_t size);
	void send(uint32_t client_id, uint32_t type, const ByteBuffer& data) {
		this->send(client_id, type, data.ptr(), data.size());
	}
#ifdef GEN_MOD_GSF
	/**
	 * @brief Write a GSF packet.
	 * @param gsf GSF article to write.
	 */
	void send(uint32_t client_id, const gsf::Article& gsf);
#endif

	ip::Endpoint our_endpoint();
	/**
	 * @brief Gets the endpoint of a client.
	 * @param clit_id Client id of the client.
	 * @return The endpoint.
	 */
	ip::Endpoint endpoint(uint32_t client_id);

	/**
	 * @brief Gets a pointer to a client-assigned programmer-controlled data.
	 * @param client_id Identifier of the client obtained in a ConnectionEvent.
	 * @return Pointer to the user data variable.
	 */
	void** user_data(uint32_t client_id);
};

}
