// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/pointers.hpp>
#include <gongen/common/platform/system.hpp>
#include <gongen/common/data_source.hpp>

namespace gen::net {

class GEN_API Communication {
public:
	virtual ~Communication() = default;
	virtual WritableDataSource& source(void* socket) = 0;
	virtual void* socket(WritableDataSource& source) = 0;
	virtual ip::Endpoint endpoint(void* socket) = 0;
	virtual void* open(const ip::Endpoint& endpoint) = 0;
	virtual void close(void* socket) = 0;
	virtual void* accept(void* socket) = 0;
	virtual void* connect(const ip::Endpoint& endpoint) = 0;
	virtual void disconnect(void* socket) = 0;
	virtual bool check(void* socket, double timeout) = 0;
	virtual Buffer<void*> check(const Buffer<void*>& sockets, double timeout) = 0;
};

Ptr<Communication> GEN_API communication_tcp();

}
