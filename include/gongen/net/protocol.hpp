// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/error.hpp>
#include <gongen/common/containers/values.hpp>
#include <gongen/common/containers/hashmap.hpp>
#include <gongen/common/platform/system.hpp>
#ifdef GEN_MOD_GSF
#include <gongen/gsf/data.hpp>
#endif

namespace gen::net::gnp {

const uint8_t VERSION_MAJOR = 0;
const uint8_t VERSION_MINOR = 0;

enum ResponseState : uint8_t {
	RESPONSE_PENDING, RESPONSE_SUCCESS, RESPONSE_TIMEOUT, RESPONSE_UNSUPPORTED, RESPONSE_ERROR, RESPONSE_UNKNOWN, RESPONSE_COUNT_
};
const char* const RESPONSE_INFO[] = {
	[RESPONSE_PENDING] = "waiting for response",
	[RESPONSE_SUCCESS] = "successfully received response",
	[RESPONSE_TIMEOUT] = "request timed out",
	[RESPONSE_UNSUPPORTED] = "unsupported request",
	[RESPONSE_ERROR] = "communication failed",
	[RESPONSE_UNKNOWN] = "bad request serial number",
};

template<typename T>
struct GEN_API Response {
private:
	Flex<T> val;
	ResponseState state_;
public:
	time::Time timeout = time::Time(0);

	Response() : state_(RESPONSE_UNKNOWN) {
	}
	explicit Response(ResponseState state) : state_(state) {
		GEN_ASSERT(!this->is_success(), "response can't be constructed as success without the value");
		GEN_ASSERT(state < RESPONSE_COUNT_, "bad response state");
	}
	template<typename... Args>
	explicit Response(Args&&... val_constructor_args) : state_(RESPONSE_SUCCESS) {
		this->val.construct(std::forward<Args>(val_constructor_args)...);
	}
	Response(const Response& other) : Response() {
		*this = other;
	}
	~Response() {
		if (this->is_success()) this->val.destroy();
	}
	Response& operator=(const Response& other) {
		if (this->is_success()) this->val.destroy();
		this->state_ = other.state_;
		if (this->is_success()) this->val.construct(other.val.val());
		return *this;
	}
	Response& operator=(Response&& moved) {
		if (this->is_success()) this->val.destroy();
		this->state_ = moved.state_;
		moved.state_ = RESPONSE_UNKNOWN;
		if (this->is_success()) this->val.construct(std::move(moved.val.val()));
		return *this;
	}

	ResponseState state() {
		return this->state_;
	}

	bool is_done() { return this->state_ != RESPONSE_PENDING; }
	bool is_success() { return this->state_ == RESPONSE_SUCCESS; }
	T get() {
		if (this->is_success()) return this->val.val();
		throw StateException(RESPONSE_INFO[this->state_]);
	}
	Optional<T> getOrWait() {
		if (!this->is_done()) return {};
		return { this->get() };
	}
};

class PacketHandler {
public:
	virtual ~PacketHandler() = default;
	// accept packet (or part of a packet) and potentially respond; returns true if phase succeeds (see phase docs)
	virtual bool handle(DataSource& in, WritableDataSource& out, ByteBuffer& peer_buf, uint32_t max_size, bool read_phase = true) = 0;
};

enum ProtocolPhase : uint8_t {
	PHASE_UNKNOWN = 0,
	PHASE_STATUS = 1,
	PHASE_HANDSHAKE = 2,
	PHASE_APPLICATION = 3,
	PHASE_COUNT_
};

// read the "magic" part of the packet; returns PHASE_UNKNOWN for short or unrecognized codes
ProtocolPhase read_phase(DataSource& data);
// same as 'read_phase' but compares the phase and throws if it is not the given one
void handle_phase(DataSource& data, ProtocolPhase phase);

struct GEN_API ProtocolInfo {
	// minor protocol version (client's version must be lesser for handshake and connection)
	uint8_t protocol_version;
	// unique application identifier
	String app_name;
	// server's application version
	String app_version;
};
struct GEN_API ServerInfo {
	// human-friendly name; usually set by the administrator
	String name;
	// detailed information about the server; usually set by the administrator
	String description;
	// maximum user amount; 0 for none
	uint16_t user_limit = 0;
};
class GEN_API StatusSeeker : public PacketHandler {
	HashMap<uint16_t, Response<ProtocolInfo>, 8> res_protocol_info;
	HashMap<uint16_t, Response<ServerInfo>, 8> res_server_info;
	HashMap<uint16_t, Response<ByteBuffer>, 8> res_custom;
	uint16_t next_serial = 0;

	template<typename T>
	uint16_t enqueue_req(HashMap<uint16_t, Response<T>, 8>& map, double timeout_sec) {
		Response<T>& response = map[this->next_serial];
		response = Response<T>(RESPONSE_PENDING);
		if (timeout_sec > 0.0) {
			response.timeout = time::get_time().add(timeout_sec);
		}
		return this->next_serial++;
	}
	template<typename T>
	Response<T> dequeue_res(HashMap<uint16_t, Response<T>, 8>& map, uint16_t req) {
		Response<T>& response = map[req];
		if (response.timeout.t != 0 && !response.is_done() && time::get_time() > response.timeout) {
			response = Response<T>(RESPONSE_TIMEOUT);
		}
		Response<T> res = std::move(response);
		if (response.is_done()) {
			map.remove(req);
		}
		return res;
	}
public:
	StatusSeeker();

	// returns true after complete response (should disconnect)
	bool handle(DataSource& in, WritableDataSource& out, ByteBuffer& peer_buf, uint32_t max_size, bool read_phase = true) override;

	// requests all standard information (not custom) with just one packet; use all get functions to get the responses
	uint16_t request_all(WritableDataSource& request, double timeout_sec);

	// request protocol information; returns serial number of the request
	uint16_t request_protocol_info(WritableDataSource& request, double timeout_sec);
	// get requested protocol information; returns response information and removes it if it's done
	Response<ProtocolInfo> get_protocol_info(uint16_t req);

	// request server meta information; returns serial number of the request
	uint16_t request_server_info(WritableDataSource& request, double timeout_sec);
	// get server meta information; returns response information and removes it if it's done
	Response<ServerInfo> get_server_info(uint16_t req);

	uint16_t request_custom(WritableDataSource& request, double timeout_sec, const char* id, uint16_t version);
	Response<ByteBuffer> get_custom(uint16_t req);
};
struct CustomStatusId {
	String name;
	uint16_t version;
};
class GEN_API StatusHandler : public PacketHandler {
public:
	ServerInfo server_info;
	HashMap<CustomStatusId, ByteBuffer, 16> custom;

	StatusHandler();

	// returns true after complete request and response (should disconnect)
	bool handle(DataSource& in, WritableDataSource& out, ByteBuffer& peer_buf, uint32_t max_size, bool read_phase = true) override;
};

class GEN_API HandshakeGuest : public PacketHandler {
public:
	// starts connection
	void init(WritableDataSource& request);

	// returns true if handshake is done (should switch to application phase)
	bool handle(DataSource& in, WritableDataSource& out, ByteBuffer& peer_buf, uint32_t max_size, bool read_phase = true) override;
};
class GEN_API HandshakeHost : public PacketHandler {
public:
	// TODO encryption

	// returns true if handshake is done (should switch to application phase)
	bool handle(DataSource& in, WritableDataSource& out, ByteBuffer& peer_buf, uint32_t max_size, bool read_phase = true) override;
};

extern const uint16_t APP_CUSTOM_MIN;
extern const uint16_t APP_CUSTOM_MAX;
class GEN_API ApplicationHandler : public PacketHandler {
private:
	struct KeepAliveRequest {
		Response<double> response;
		WritableDataSource* source;
		time::Time time;
	};
	HashMap<uint16_t, KeepAliveRequest, 8> keep_alive_info;
	uint16_t next_serial = 0;
public:
#ifdef GEN_MOD_GSF
	HashMap<gsf::Descriptor, gsf::Schema> gsf_schemas;
	HashMap<gsf::Descriptor, void (*)(void* user, gsf::Article gsf, WritableDataSource& out, ByteBuffer& peer_buf)> gsf_handlers;
	void (*gsf_handler_other)(void* user, gsf::Article gsf, WritableDataSource& out, ByteBuffer& peer_buf);
#endif
	HashMap<uint16_t, void (*)(void* user, uint8_t* data, uint32_t size, WritableDataSource& out, ByteBuffer& peer_buf)> custom_handlers;
	void (*custom_handler_other)(void* user, uint8_t type, uint8_t* data, uint32_t size, WritableDataSource& out, ByteBuffer& peer_buf);
	void* user;

	ApplicationHandler();

	void disconnect(WritableDataSource& out, const String& comment = "");

	// send keep alive request
	uint16_t req_keep_alive(WritableDataSource& out, double timeout_sec);
	// get keep alive response info (delay in seconds)
	Response<double> get_keep_alive(uint16_t serial);
	// on keep alive response; usually should not be called manually
	void res_keep_alive(WritableDataSource& source, uint16_t serial);

	void gsf(WritableDataSource& out, const gsf::Article& article);

	// writes header; write the data manually after that
	void custom(WritableDataSource& out, uint16_t type, size_t size);

	// returns true after each fully received custom or gsf packet
	bool handle(DataSource& in, WritableDataSource& out, ByteBuffer& peer_buf, uint32_t max_size, bool read_phase = true) override;
};

} // namespace gen::net::gnp
