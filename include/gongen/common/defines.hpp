// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <stdint.h>

#if defined(_WIN32)
	#define GEN_PLATFORM_WIN32
	#if defined(_WIN64)
		#define GEN_PLATFORM_WIN64
	#endif
#elif defined(__APPLE__)
	#define GEN_PLATFORM_APPLE
#elif defined(__ANDROID__)
	#define GEN_PLATFORM_ANDROID
#elif defined(__linux__)
	#define GEN_PLATFORM_LINUX
#elif defined(__unix__)
	#include <sys/param.h>
	#if defined(BSD)
		#define GEN_PLATFORM_BSD
	#endif
#endif

#if defined(__unix__)
	#define GEN_PLATFORM_UNIX
#endif
#if defined(EMSCRIPTEN)
	#define GEN_PLATFORM_EMSCRIPTEN
#endif

#if defined(GEN_PLATFORM_WIN32) || defined(GEN_PLATFORM_LINUX)
	#define GEN_PLATFORM_DESKTOP
#endif

#if !defined(GEN_PLATFORM_DESKTOP)
	#error Platform not supported
#endif

#if !defined(GEN_BUILD_STATIC)
	#ifdef GEN_PLATFORM_WIN32
		#define GEN_API_EXPORT __declspec(dllexport)
		#define GEN_API_IMPORT __declspec(dllimport)
	#elif defined(__GNUC__)
		#define GEN_API_EXPORT __attribute__((visibility("default")))
		#define GEN_API_IMPORT
	#endif
#else
	#define GEN_API_EXPORT
	#define GEN_API_IMPORT
#endif

#ifdef GEN_BUILD
    #define GEN_API GEN_API_EXPORT
#else
    #define GEN_API GEN_API_IMPORT
#endif

#define GEN_SWAP_UINT16(x) __builtin_bswap16(x)
#define GEN_SWAP_UINT32(x) __builtin_bswap32(x)
#define GEN_SWAP_UINT64(x) __builtin_bswap64(x)

#define GEN_FOURCC(a,b,c,d) ((uint32_t) (((d)<<24) | ((c)<<16) | ((b)<<8) | (a)))
#define GEN_FOURCC_STR(str) GEN_FOURCC(str[0], str[1], str[2], str[3])
#define GEN_EIGHTCC(a,b,c,d,e,f,g,h) ((uint64_t) (((h)<<56) | ((g)<<48) | ((f)<<40) | ((e)<<32) | ((d)<<24) | ((c)<<16) | ((b)<<8) | (a)))
#define GEN_EIGHTCC_STR(str) GEN_EIGHTCC(str[0], str[1], str[2], str[3], str[4], str[5], str[6], str[7])

#define GEN_SAFE_STRLEN(x) (x) ? strlen(x) : 0
#define GEN_ASCII_TOLOWER(c) ((c > 64) && (c < 91)) ? (c + 32) : (c)
#define GEN_HEX_TO_DEC_DIGIT(c) GEN_HEX_TO_DEC_DIGIT_LOWERCASE(GEN_ASCII_TOLOWER(c))
#define GEN_HEX_TO_DEC_DIGIT_LOWERCASE(c) ((c < 58) ? (c - 48) : ((c < 71) ? (c - 55) : (c - 87))) //only works with lowercase numbers
#define GEN_INC_VOID_POINTER(x, v) x = (void*)(((uint8_t*)x) + v)
#define GEN_INC_POINTER(x, v, type) x = (type*)(((uint8_t*)x) + v)

#define GEN_NUM_MAX(x, y) ((x) > (y) ? (x) : (y))
#define GEN_NUM_MIN(x, y) ((x) < (y) ? (x) : (y))

#if defined(_MSC_VER) && !defined(__clang__)
	#define GEN_MSVC_COMPILER
#endif

#ifdef GEN_MSVC_COMPILER
	#define GEN_PRINTF_CHECKING
	#define GEN_PRINTF_CHECKING_CLS
#else
	#define GEN_PRINTF_CHECKING __attribute__((format(printf, 1, 2)))
	#define GEN_PRINTF_CHECKING_CLS __attribute__((format(printf, 2, 3)))
#endif
