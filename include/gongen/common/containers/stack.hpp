// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "dynamic_array.hpp"
#include "values.hpp"

namespace gen {

template<typename T>
class Stack {
private:
public:
	gen::DynamicArray<T> array;

	Stack(size_t(*capacity_function)(size_t, size_t) = gen::capacity_func::npo2) : array(capacity_function) {
	}
	Stack(size_t initial_capacity, size_t(*capacity_function)(size_t, size_t) = gen::capacity_func::npo2) : array(initial_capacity, capacity_function) {
	}

	bool empty() {
		return this->array.empty();
	}
	void push(const T& obj) {
		array.add(obj);
	}
	template<class... Args>
	T* push_emplace(Args&&... args) {
		array.emplace(std::forward<Args>(args)...);
	}
	void push_move(T&& obj) {
		array.add(std::move(obj));
	}
	T pop() {
		T ret = *array.last_ptr();
		array.delete_last();
		return ret;
	}
	gen::Optional<T> pop_safe() {
		if (this->empty()) return {};
		return { this->pop() };
	}
	void pop_forget() {
		array.delete_last();
	}
	T& peek() {
		return array.last();
	}
	T* peek_ptr() {
		return array.last_ptr();
	}
};

}
