// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <stdlib.h>
#include "../defines.hpp"

namespace gen {

namespace internal {
void GEN_API no_impl_error(const char* msg);
} // namespace internal

template<typename T>
class AbstractIterator {
public:
	virtual ~AbstractIterator() = default;

	virtual T* ptr() = 0;
	virtual void add(size_t count) = 0;
	virtual void substract(size_t count) {
		internal::no_impl_error("Iterator does not implement backword iteration.");
	}
	virtual bool equals(AbstractIterator<T>* other) = 0;
};

template<typename T>
class AbstractIterable {
public:
	virtual ~AbstractIterable() = default;

	virtual size_t asize() = 0;
	virtual AbstractIterator<T>* abegin(void* mem = nullptr) = 0;
	virtual AbstractIterator<T>* aend(void* mem = nullptr) = 0;
	virtual AbstractIterator<const T>* acbegin() const {
		internal::no_impl_error("Abstract iterable does not implement const iteration.");
		return nullptr;
	}
	virtual AbstractIterator<const T>* acend() const {
		internal::no_impl_error("Abstract iterable does not implement const iteration.");
		return nullptr;
	}
};

template<typename T>
class List { // TODO : public AbstractIterable<V>
public:
	virtual ~List() = default;

	virtual size_t size() const = 0;
	virtual bool empty() { return this->size() == 0; }

	virtual bool has(const T& item) { return this->has_ptr(&item); };
	virtual bool has_ptr(const T* item) = 0; // compares pointers
	virtual bool contains(const T& preset) const { return this->contains_ptr(&preset); };
	virtual bool contains_ptr(const T* preset) const = 0; // uses ==

	virtual const T& get(size_t index) const = 0;
	virtual T& get(size_t index) = 0;
	virtual size_t index(const T& item) const { return this->index_ptr(&item); }
	virtual size_t index_ptr(const T* item) const = 0;

	virtual void clear() = 0;
	virtual void remove_idx(size_t index) = 0;
	virtual void remove(T& item) { return this->remove_ptr(&item); }
	virtual void remove_ptr(T* item) = 0; // compares pointers
	virtual void remove_match(const T& preset) { return this->remove_match_ptr(&preset); }
	virtual void remove_match_ptr(const T* preset) = 0; // uses ==
	virtual bool remove_match_safe(const T& preset) { return this->remove_match_ptr_safe(&preset); }
	virtual bool remove_match_ptr_safe(const T* preset) = 0; // uses ==

	virtual T& add(const T& item) = 0;
	virtual T& add(T&& item) = 0;
	// template<class... Args>
	// virtual T& emplace(Args&&... args) = 0;

	virtual T& insert(const T& item, size_t index) = 0;
	virtual T& insert(T&& item, size_t index) = 0;
	// template<class... Args>
	// virtual T& insert_emplace(size_t index, Args&&... args) = 0;
};

} // namespace gen
