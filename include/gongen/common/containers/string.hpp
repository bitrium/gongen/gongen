// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include "../platform/memory.hpp"
#include "dynamic_array.hpp"
#include "values.hpp"

namespace gen {
class GEN_API String {
	const static size_t SSO_CAPACITY = (sizeof(char*) + sizeof(size_t) * 2) - sizeof(uint8_t);

#pragma pack(push, 1)
	union {
		struct {
			size_t size_;
			size_t capacity_;
			char* ptr_;
		};
		struct {
			uint8_t sso_size;
			char sso[SSO_CAPACITY];
		};
	};
#pragma pack(pop)
	void set_size(size_t size) {
		if (is_short()) {
			this->sso_size = (size * 2);
		} else {
			this->size_ = (size * 2) | 1;
		}
		*get_ptr(size) = '\0';
	}

	char* get_ptr(size_t offset = 0) const {
		if (is_short()) return ((String*) this)->sso + offset;
		else return this->ptr_ + offset;
	}
public:
	String() { memset(this, 0, sizeof(String)); }
	String(const char* cstr) {
		memset(this, 0, sizeof(String));
		append(cstr);
	}
	String(const char* cstr, size_t len) {
		memset(this, 0, sizeof(String));
		append(cstr, len);
	}
	String(const String& other) {
		memset(this, 0, sizeof(String));
		this->append(other.get_ptr(0), other.size());
	}
	String(String&& other) noexcept {
		memset(this, 0, sizeof(String));
		memcpy(this, &other, sizeof(gen::String));
		memset(&other, 0, sizeof(gen::String));
	}
	String(size_t initial_capacity) {
		memset(this, 0, sizeof(String));
		this->reserve(initial_capacity);
		this->clear();
	}
	~String() {
		if (!is_short()) gen::free(this->ptr_);
		this->ptr_ = nullptr;
		this->size_ = 0;
		this->capacity_ = 0;
	}

	bool is_short() const {
		return !(this->size_ & 1);
	}

	size_t append(char c) {
		char* ptr = this->reserve(1);
		*ptr = c;
		return size();
	}
	size_t append(const String& other) {
		return this->append(other.c_str());
	}
	size_t append(const char* cstr) {
		if (cstr) return append(cstr, strlen(cstr));
		else return 0;
	}
	size_t append(const char* cstr, size_t len) {
		char* ptr = this->reserve(len);
		memcpy(ptr, cstr, len);
		return size();
	}
	size_t appendf(const char* fmt, ...) GEN_PRINTF_CHECKING_CLS {
		va_list va;
		va_start(va, fmt);
		vappendf(fmt, va);
		va_end(va);
		return size();
	}
	size_t vappendf(const char* fmt, va_list va);

	char* reserve(size_t size);
	size_t size() const { 
		if(is_short()) return this->sso_size / 2;
		else return this->size_ / 2;
	}
	size_t capacity() const {
		if (is_short()) return gen::String::SSO_CAPACITY;
		else return capacity_;
	}
	void clear() { set_size(0); }

	// find '\0' and change size to that
	void update_size() {
		this->set_size(strnlen(this->get_ptr(), this->capacity() - 1));
	}
	// trims string to specified size and puts NULL at the end
	void trim(size_t size) {
		if((size + 1) < this->capacity()) this->set_size(size);
	}

	inline const char* c_str(size_t off = 0) const { return (const char*)get_ptr(off); }
	inline char* data(size_t off = 0) { return get_ptr(off); }
	char get(size_t offset) const { return *get_ptr(offset); }

	PtrIterator<char> begin() const { return PtrIterator<char>(get_ptr(0)); }
	PtrIterator<char> end() const { return PtrIterator<char>(get_ptr(size() + 1)); }

	// Utilities
	bool empty() const { return (size() == 0); }

	gen::DynamicArray<gen::String> split(const char* separators) const;
	bool contains(const char* characters) const;
	size_t count(const char* characters) const;
	Optional<size_t> find(const char* matches) const;

	bool starts_with(const String& prefix) const;
	bool ends_with(const String& suffix) const;
	gen::String& segment(size_t start = 0, size_t end = SIZE_MAX);
	gen::String segmented(size_t start = 0, size_t end = SIZE_MAX);

	gen::String& strip_l();
	gen::String& strip_r();
	gen::String& strip();
	gen::String stripped_l();
	gen::String stripped_r();
	gen::String stripped();

	static gen::String createf(const char* fmt, ...) GEN_PRINTF_CHECKING {
		va_list va;
		va_start(va, fmt);
		gen::String str;
		str.vappendf(fmt, va);
		va_end(va);
		return str;
	}
	static gen::String createvf(const char* fmt, va_list va) {
		gen::String str;
		str.vappendf(fmt, va);
		return str;
	}

	// Operators
	String& operator=(const String& other) {
		clear();
		return this->operator+=(other);
	}

	String& operator=(const char* c_str) {
		clear();
		return this->operator+=(c_str);
	}

	String& operator+=(const String& other) {
		append(other);
		return *this;
	}

	String& operator+=(const char* c_str) {
		append(c_str);
		return *this;
	}

	bool operator==(const String& rhs) const {
		const char* cstr1 = rhs.c_str();
		const char* cstr2 = c_str();
		size_t size1 = rhs.size();
		size_t size2 = size();
		if (size1 != size2) return false;
		else return !memcmp(cstr1, cstr2, size1);
	}

	String operator+(const char* cstr) const {
		String str = *this;
		str.append(cstr);
		return str;
	}

	String operator+(const String& other) const {
		String str = *this;
		str.append(other.c_str());
		return str;
	}

	char operator[](ptrdiff_t offset) const {
		return this->get(offset);
	}
};

}
