// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "iteration.hpp"
#include "list.hpp"

namespace gen {

template<typename K, typename V, uint16_t bucket_count = 256>
class HashMap : public AbstractIterable<V> {
	typedef uint16_t Bucket;

	struct Node {
		uint64_t hash; // must be first for remove_safe function validity
		V val;

		Node(const Node& other) : val(other.val) {
			this->hash = other.hash;
		}
		template<class... Args>
		Node(uint64_t hash, Args&&... args) : val(std::forward<Args>(args)...) {
			this->hash = hash;
		}
		// Node(uint64_t hash, const V& val) : val(val) {
		// 	this->hash = hash;
		// }

		bool operator==(const Node& other) const {
			return this->hash == other.hash; // must only compare hashes for remove_safe function validity
		}
	};

	class Iterator : public AbstractIterator<V> {
		HashMap<K, V, bucket_count>* map;
		Bucket bucket;
		UniversalIterator<Node> bucket_iter;
		UniversalIterator<Node> bucket_end;

		void set_bucket(Bucket bucket) {
			this->bucket = bucket;
			if (bucket < bucket_count) {
				this->bucket_iter.begin(&map->nodes[bucket]);
				this->bucket_end.end(&map->nodes[bucket]);
			} else {
				this->bucket_iter.reset();
				this->bucket_end.reset();
			}
		}
	public:
		Iterator(HashMap<K, V, bucket_count>* map, Bucket bucket) {
			this->map = map;
			this->set_bucket(bucket);
		}

		Iterator& to_first() {
			if (this->bucket >= bucket_count) return *this;
			while (this->bucket_iter == this->bucket_end) {
				this->set_bucket(this->bucket + 1);
				if (this->bucket_iter.get() == nullptr) break;
			}
			return *this;
		}

		V* ptr() {
			return &bucket_iter->val;
		}
		void add(size_t count) {
			for (size_t i = 0; i < count; i++) ++(*this);
		}
		bool equals(AbstractIterator<V>* other) {
			if (!other) return false;
			return *this == *dynamic_cast<Iterator*>(other);
		}

 		V& operator*() const { return bucket_iter->val; }
		V* operator->() { return &bucket_iter->val; }

		Iterator operator+(size_t count) {
			Iterator tmp = *this;
			tmp.add(count);
			return tmp;
		}

		Iterator& operator++() {
			++this->bucket_iter;
			this->to_first();
			return *this;
	}
//		Iterator operator++(int) {
//			Iterator tmp = *this;
//			++(*this);
//			return tmp;
//		}

		friend bool operator==(const Iterator& a, const Iterator& b) {
			return a.bucket_iter == b.bucket_iter;
		};
		friend bool operator!=(const Iterator& a, const Iterator& b) {
			return a.bucket_iter != b.bucket_iter;
		};
	};

	uint64_t(*hash)(const K&);
	BufferList<Node> nodes[bucket_count];
	size_t size_ = 0;

	inline BufferList<Node>& get_bucket(uint64_t hash) {
		return this->nodes[hash % bucket_count];
	}
	inline Node* find_hash(uint64_t hash) const {
		for (Node& node : ((HashMap<K, V, bucket_count>*) this)->get_bucket(hash)) {
			if (node.hash == hash) return (Node*) &node;
		}
		return nullptr;
	}
	inline Node* find(const K& key) const {
		return this->find_hash(this->hash(key));
	}
public:
	explicit HashMap(uint64_t(*hash_func)(const K&)) {
		this->hash = hash_func;
	}
	HashMap(const HashMap& copied) {
		this->hash = copied.hash;
		for (Bucket i = 0; i < bucket_count; i++) {
			this->nodes[i] = copied.nodes[i];
		}
	}
	~HashMap() {
		this->clear();
	}

	size_t size() const {
		return this->size_;
	}
	bool empty() {
		return this->size_ == 0;
	}

	void clear() {
		for (Bucket bucket = 0; bucket < bucket_count; bucket++) {
			this->nodes[bucket].clear();
		}
		this->size_ = 0;
	}

	template<class... Args>
	V& emplace(const K& key, Args&&... args) {
		uint64_t hash = this->hash(key);
		V& res = this->get_bucket(hash).emplace(hash, std::forward<Args>(args)...).val;
		this->size_++;
		return res;
	}
	template<class... Args>
	V& emplace_with_hash(uint64_t hash, Args&&... args) {
		V& res = this->get_bucket(hash).emplace(hash, std::forward<Args>(args)...).val;
		this->size_++;
		return res;
	}
	V& add(const K& key, const V& value) {
		uint64_t hash = this->hash(key);
		V& res = this->get_bucket(hash).emplace(hash, value).val;
		this->size_++;
		return res;
	}
	V& add_with_hash(uint64_t hash, const V& value) {
		V& res = this->get_bucket(hash).emplace(hash, value).val;
		this->size_++;
		return res;
	}

	bool remove_safe(const K& key) {
		uint64_t hash = this->hash(key);
		BufferList<Node>* bucket = &this->get_bucket(hash); // TODO make ref
		// nodes are compared using only the hash
		if (!bucket->remove_match_ptr_safe((Node*) &hash)) {
			return false;
		}
		this->size_--;
		return true;
	}
	void remove(const K& key) {
		if (!this->remove_safe(key)) {
			throw gen::KeyException("Map does not contain requested key.");
		}
	}

	bool contains(const K& key) const {
		return this->find(key) != nullptr;
	}

	const V& get(const K& key) const {
		Node* node = this->find(key);
		if (!node) throw gen::KeyException("Map does not contain requested key.");
		return node->val;
	}
	V& get(const K& key) {
		Node* node = this->find(key);
		if (!node) throw gen::KeyException("Map does not contain requested key.");
		return node->val;
	}
	const V& get(const K& key, const V& default_value) const {
		Node* node = this->find(key);
		if (!node) return default_value;
		return node->val;
	}
	V& get(const K& key, V& default_value) {
		Node* node = this->find(key);
		if (!node) return default_value;
		return node->val;
	}
	V* ptr(const K& key) {
		Node* node = this->find(key);
		if (!node) return nullptr;
		else return &node->val;
	}
	const V* ptr(const K& key) const {
		Node* node = this->find(key);
		if (!node) return nullptr;
		else return &node->val;
	}
	V* get_for_hash(uint64_t hash) {
		Node* node = this->find_hash(hash);
		if (!node) return nullptr;
		else return &node->val;
	}
	const V* get_for_hash(uint64_t hash) const {
		Node* node = this->find_hash(hash);
		if (!node) return nullptr;
		else return &node->val;
	}

	V& demand(const K& key) {
		Node* node = this->find(key);
		if (node) return node->val;
		else return this->emplace(key);
	}
	V& demand_hash(uint64_t hash) {
		Node* node = this->find_hash(hash);
		if (node) return node->val;
		else return this->emplace_with_hash(hash);
	}

	V& operator[](const K& key) {
		return this->demand(key);
	}
	const V& operator[](const K& key) const {
		return this->get(key);
	}

	Iterator begin() {
		return Iterator(this, 0).to_first();
	}
	Iterator end() {
		return Iterator(this, bucket_count);
	}

	size_t asize() override {
		return sizeof(Iterator);
	}
	AbstractIterator<V>* abegin(void* ptr) override {
		return &(new (ptr) Iterator(this, 0))->to_first();
	}
	AbstractIterator<V>* aend(void* ptr) override {
		return new (ptr) Iterator(this, bucket_count);
	}
};

} // namespace gen
