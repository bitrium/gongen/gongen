// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <stdint.h>
#include "../platform/memory.hpp"
#include <string.h>

namespace gen {
class GEN_API LocklessQueue {
	void* impl;
	void* wait;
public:
	LocklessQueue();
	~LocklessQueue();
	LocklessQueue(const LocklessQueue& queue) = delete;
	LocklessQueue(LocklessQueue&& queue) noexcept {
		this->impl = queue.impl;
		queue.impl = nullptr;
		this->wait = queue.wait;
		queue.wait = nullptr;
	}

	bool enqueue(void* item) noexcept;
	void* dequeue() noexcept;
	void* dequeue_wait() noexcept;

	size_t size() const noexcept;

	template<typename T>
	friend class TypedLocklessQueue;
};

template<typename T>
class TypedLocklessQueue {
	LocklessQueue queue;
public:
	TypedLocklessQueue() {}
	TypedLocklessQueue(const TypedLocklessQueue& queue) = delete;
	TypedLocklessQueue(TypedLocklessQueue&& queue) noexcept {
		memcpy(this, &queue, sizeof(gen::LocklessQueue));
		memset(&queue, 0, sizeof(gen::LocklessQueue));
	}
	template<class... Args>
	bool enqueue_emplace(Args&&... args) noexcept { return queue.enqueue(new T(args...)); }
	bool enqueue(T* item) noexcept { return queue.enqueue(item); }
	T* dequeue() noexcept { return (T*)queue.dequeue(); }
	T* dequeue_wait() noexcept { return (T*)queue.dequeue_wait(); }

	size_t size() const noexcept { return queue.size(); }
};
}
