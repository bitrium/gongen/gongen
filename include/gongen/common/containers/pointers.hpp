// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <stdint.h>
#include "../platform/memory.hpp"
#include <string.h>
#include <atomic>
#include "../hash.hpp"

namespace gen {
struct TaggedPtr {
	void* ptr_ = nullptr;

	TaggedPtr& operator=(void* ptr) {
		this->ptr_ = ptr;
		return *this;
	}
	bool tag() { return ((uintptr_t)ptr_ & 1); }
	void set(void* ptr, bool tag) {
		uintptr_t uptr = (uintptr_t)ptr;
		if (tag) uptr |= 1;
		ptr_ = (void*)uptr;
	}
	void* ptr() { return (void*)((uintptr_t)ptr_ & (UINTPTR_MAX - 1)); }
};

template<typename T>
class SharedPtr {
	std::atomic<int32_t>* ref_counter = nullptr;
	T* ptr = nullptr;

	bool combined_allocation() {
		return ((uintptr_t)ref_counter & 1);
	}
	std::atomic<int32_t>* ref_counter_ptr() {
		return (std::atomic<int32_t>*)((uintptr_t)ref_counter & (UINTPTR_MAX - 1));
	}
	void increment() {
		if (ref_counter_ptr()) ref_counter_ptr()->fetch_add(1);
	}
	void decrement() {
		if (ref_counter_ptr()) {
			ref_counter_ptr()->fetch_add(-1);
			if (ref_counter_ptr()->load() <= 0) {
				if (!combined_allocation()) delete ref_counter_ptr();
				else ref_counter_ptr()->~atomic();
				if (this->ptr) delete this->ptr;
				this->ptr = nullptr;
				this->ref_counter = nullptr;
			}
		}
	}
public:
	SharedPtr() {}
	SharedPtr(T* pointer) {
		this->ptr = pointer;
		if (pointer) this->ref_counter = new std::atomic<int32_t>(1);
	}
	SharedPtr(const SharedPtr<T>& rhs) {
		this->ptr = rhs.ptr;
		this->ref_counter = rhs.ref_counter;
		this->increment();
	}
	SharedPtr(SharedPtr<T>&& rhs) {
		this->ptr = rhs.ptr;
		this->ref_counter = rhs.ref_counter;
		rhs.ptr = nullptr;
		rhs.ref_counter = nullptr;
	}
	virtual ~SharedPtr() {
		this->decrement();
	}
	template<class... Args>
	void emplace(Args&&... args) {
		this->decrement();
		T* mem = (T*)calloc(sizeof(T) + sizeof(std::atomic<int32_t>));
		this->ptr = new (mem) T(args...);
		this->ref_counter = (std::atomic<int32_t>*)(((uintptr_t)&mem[1]) | 1);
	}
	template<typename U, class... Args>
	void emplace(Args&&... args) {
		this->decrement();
		U* mem = (U*)calloc(sizeof(U) + sizeof(std::atomic<int32_t>));
		this->ptr = new (mem) U(args...);
		this->ref_counter = (std::atomic<int32_t>*)(((uintptr_t)&mem[1]) | 1);
	}
	template<class... Args>
	static SharedPtr<T> construct(Args&&... args) {
		SharedPtr<T> out;
		out.emplace(args...);
		return out;
	}
	template<typename U, class... Args>
	static SharedPtr<T> construct(Args&&... args) {
		SharedPtr<T> out;
		out.emplace<U>(args...);
		return out;
	}

	T& operator*() const { return *ptr; }
	T* operator->() const { return ptr; }

	T* get() const { return ptr; }

	SharedPtr<T>& operator=(const SharedPtr<T>& rhs) noexcept {
		this->decrement();
		this->ptr = rhs.ptr;
		this->ref_counter = rhs.ref_counter;
		this->increment();
		return *this;
	}
	SharedPtr<T>& operator=(SharedPtr<T>&& rhs) noexcept {
		this->ptr = rhs.ptr;
		this->ref_counter = rhs.ref_counter;
		rhs.ptr = nullptr;
		rhs.ref_counter = nullptr;
		return *this;
	}
	SharedPtr<T>& operator=(T* ptr) noexcept {
		this->decrement();
		this->ptr = ptr;
		this->increment();
		return *this;
	}
	uint64_t hash() const {
		intptr_t intptr = (intptr_t)(ptr);
		return gen::hash(&intptr, sizeof(intptr_t));
	}

	explicit operator bool() const noexcept {
		return (bool)(this->ptr);
	}

	bool operator==(const SharedPtr<T>& other) {
		return this->ptr == other.ptr;
	}
};

template<typename T>
class Ptr {
	TaggedPtr ptr_;

	void set_ptr(T* ptr, bool owner) { ptr_.set(ptr, owner); }
	void delete_ptr() {
		if (ptr() && is_owner()) {
			delete ptr();
			ptr_ = nullptr;
		}
	}

public:
	Ptr() {}
	Ptr(T* ptr, bool owner = true) { set_ptr(ptr, owner); }
	Ptr(const Ptr<T>& other) = delete;
	Ptr(Ptr<T>&& other) {
		this->ptr_ = other.ptr_;
		other.ptr_ = nullptr;
	}
	~Ptr() {
		delete_ptr();
	}
	template<class... Args>
	void emplace(Args&&... args) {
		delete_ptr();
		set_ptr(new T(args...), true);
	}
	void set(T* new_ptr) {
		delete_ptr();
		set_ptr(new_ptr, true);
	}
	T* ptr() { return (T*)ptr_.ptr(); }
	T* ptr(size_t offset) { return ptr() + offset; }
	T& get() { return *ptr(); }
	T& get(size_t offset) { return *ptr(offset); }
	T& operator*() const { return *ptr(); }
	T* operator->() { return ptr(); }
	Ptr& operator=(Ptr&& other) {
		this->ptr_ = other.ptr_;
		other.ptr_ = nullptr;
		return *this;
	}
	Ptr& operator=(T* ptr) {
		delete_ptr();
		set_ptr(ptr, true);
		return *this;
	}
	bool operator==(T* rhs) { return ptr() == rhs; }
	bool operator!=(T* rhs) { return ptr() != rhs; }
	bool operator==(const Ptr<T>& rhs) { return ptr() == rhs.ptr(); }
	bool operator!=(const Ptr<T>& rhs) { return ptr() != rhs.ptr(); }
	bool is_owner() { return ptr_.tag(); }
	Ptr clone() {
		return Ptr(ptr(), false);
	}
	Ptr transfer() {
		if (is_owner()) {
			Ptr new_ptr(ptr());
			set_ptr(ptr(), false);
			return new_ptr;
		}
		else return clone();
	}
	T* disown() {
		set_ptr(ptr(), false);
		return ptr();
	}
	SharedPtr<T> share() {
		return SharedPtr<T>(this->disown());
	}
};
}
