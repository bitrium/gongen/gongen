#pragma once

#include "buffer.hpp"

namespace gen {

namespace internal {
	const size_t NOT_SO_FLAG = ~(SIZE_MAX >> 1);
}

// Short Optimized Buffer
template<typename T, size_t sso_size>
class SOBuffer {
	size_t size_ = 0;
	union {
		struct {
			size_t cap;
			T* buf;
		} heap;
		struct {
			T buf[sso_size] = {};
		} sso;
	};
	capacity_func::func capacity_function; // only used for the non-sso data

	inline size_t calc_capacity(size_t capacity) {
		if (capacity_function) return capacity_function(capacity, sizeof(T));
		else return gen::capacity_func::npo2(capacity, sizeof(T));
	}
	inline void set_size(size_t size) {
		size_ = size | (!this->is_short() * internal::NOT_SO_FLAG);
	}
public:
	explicit SOBuffer(capacity_func::func capacity_function) {
		this->capacity_function = capacity_function;
	}
	SOBuffer() : SOBuffer(gen::capacity_func::npo2) {
	}
	explicit SOBuffer(size_t start_capacity, size_t(*capacity_function)(size_t, size_t) = gen::capacity_func::npo2) {
		this->capacity_function_ = capacity_function;
		set_capacity(start_capacity);
	}
	SOBuffer(const SOBuffer<T, sso_size>& other) {
		this->capacity_function = other.capacity_function;
		add(other.ptr_, other.size_);
	}
	SOBuffer(SOBuffer<T, sso_size>&& other) noexcept {
		memcpy((void*) this, (void*) &other, sizeof(gen::SOBuffer<T, sso_size>));
		memset((void*) &other, 0, sizeof(gen::SOBuffer<T, sso_size>));
	}
	~SOBuffer() noexcept { clear(true); }

	inline bool is_short() const {
		return !(this->size_ & internal::NOT_SO_FLAG);
	}

	void set_capacity_function(size_t(*capacity_function)(size_t, size_t) = gen::capacity_func::npo2) {
		this->capacity_function = capacity_function;
	}
	void resize(size_t size) {
		if (size > this->capacity()) set_capacity(size);
		this->set_size(size);
	}
	T* reserve(size_t size) {
		size_t old_size = this->size();
		resize(old_size + size);
		return ptr(old_size);
	}
	T* reserve_insert(size_t idx, size_t size = 1) {
		if (idx >= this->size()) return reserve(size);
		resize(this->size() + size);
		memmove(this->ptr(idx + size), this->ptr(idx), (this->size() - idx) * sizeof(T));
		return this->ptr_ + idx;
	}
	void reserve_capacity(size_t size) {
		if (this->size() + size <= this->capacity_) return;
		this->set_capacity(this->size() + size);
	}
	void set_capacity(size_t capacity) {
		size_t new_capacity = calc_capacity(capacity);
		if (this->is_short()) {
			if (capacity <= sso_size) return;
			// convert to non-sso
			T* ptr = gen::tcalloc<T>(new_capacity);
			memmove(ptr, this->sso.buf, this->size() * sizeof(T));
			this->heap.buf = ptr;
			this->heap.cap = new_capacity;
			this->size_ |= internal::NOT_SO_FLAG;
			return;
		}
		if (new_capacity == this->heap.cap) return;
		if (this->size() > new_capacity) this->set_size(new_capacity);
		this->heap.buf = tcrealloc<T>(this->heap.buf, this->heap.cap, new_capacity);
		this->heap.cap = new_capacity;
	}

	void clear(bool free_memory = false) noexcept {
		resize(0);
		if (free_memory && !this->is_short()) {
			gen::free(this->heap.buf);
			/* these are not needed as we're not using the heap buffer after clearing:
			this->heap.buf = nullptr;
			this->heap.cap = 0;
			 */
		}
		this->size_ = 0; // make sure the non-sso flag is removed
	}
	void shrink_to_fit() {
		if (this->is_short()) return;
		if (this->size() < sso_size) {
			// convert to sso
			void* old_buf = this->heap.buf;
			memmove(this->sso.buf, old_buf, this->size() * sizeof(T));
			gen::free(old_buf);
			this->size_ = this->size(); // remove non-sso flag
		} else {
			this->heap.buf = gen::tcrealloc<T>(this->heap.buf, this->heap.cap, this->size());
		}
	}
	void remove_last(size_t count = 1) {
		size_t size = this->size();
		if (count >= size) this->clear();
		else this->resize(size - count);
	}

	size_t size() const { return size_ & ~internal::NOT_SO_FLAG; }
	size_t size_bytes() const { return (size_ * sizeof(T)); }
	size_t capacity() const { if (this->is_short()) return sso_size; else return this->heap.cap; }
	bool empty() const { return (size_ << 1) == 0; }

	T* add(const T* data, size_t data_size) {
		T* mem = reserve(data_size);
		memcpy((void*)mem, (const void*)data, data_size * sizeof(T));
		return mem;
	}
	T* add(const T& data) { return add(&data, 1); }
	template<class... Args>
	T* emplace(Args&&... args) {
		return new (reserve(1)) T(std::forward<Args>(args)...);
	}

	T* ptr(size_t offset = 0) const {
		return (T*) &(this->is_short() ? this->sso.buf : this->heap.buf)[offset];
	}
	T& get(size_t offset = 0) const {
		return *this->ptr(offset);
	}

	T* get_last_ptr(size_t roffset = 1) const {
		return this->ptr(this->size() - roffset);
	}
	T& get_last(size_t roffset = 1) const {
		return *this->get_last_ptr(roffset);
	}

	T& operator[](ptrdiff_t offset) const {
		if (offset < 0) return this->get_last(-offset);
		return this->get(offset);
	}

	SOBuffer<T, sso_size>& operator=(const SOBuffer<T, sso_size>& other) {
		clear(false);
		this->capacity_function = other.capacity_function;
		add(other.ptr_, other.size_);
		return *this;
	}

	bool operator==(const SOBuffer<T, sso_size>& other) const {
		if (this->size() != other.size()) return false;
		return !memcmp(this->ptr(), other.ptr(), size() * sizeof(T));
	}
	bool operator!=(const SOBuffer<T, sso_size>& other) {
		return !this->operator==(other);
	}
};

}
