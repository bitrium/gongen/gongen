// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <stdint.h>
#include <string.h>
#include <new>
#include <utility>
#include "../platform/memory.hpp"
#include "buffer.hpp"

namespace gen {
template<typename T, typename B = Buffer<T>>
class DynamicArray final : public AbstractIterable<T> {
	B buffer;

	void destroy(size_t start, size_t size) {
		T* local_ptr = ptr(start);
		for (uint32_t i = 0; i < size; i++) {
			local_ptr->~T();
			local_ptr++;
		}
	}
public:
	explicit DynamicArray(size_t(*capacity_function)(size_t, size_t) = gen::capacity_func::npo2) :buffer(capacity_function) {}
	explicit DynamicArray(size_t start_capacity, size_t(*capacity_function)(size_t, size_t) = gen::capacity_func::npo2) :buffer(start_capacity, capacity_function) {}
	DynamicArray(const DynamicArray<T>& other) : buffer() {
		T* ptr = this->buffer.reserve(other.size());
		for (uint32_t i = 0; i < other.size(); i++) {
			new (ptr) T(*other.ptr(i));
			ptr++;
		}
	}
	DynamicArray(DynamicArray<T>&& other) : buffer(true) {
		memcpy((void*) this, (void*) &other, sizeof(gen::DynamicArray<T>));
		memset((void*) &other, 0, sizeof(gen::DynamicArray<T>));
	}
	~DynamicArray() { clear(true); }

	void move_to(gen::DynamicArray<T>& destination, bool free_memory = true) {
		for (size_t i = 0; i < this->size(); i++) {
			destination.add(std::move((T&) this->buffer.get(i)));
		}
		this->buffer.clear(free_memory);
	}

	void reserve(size_t size) {
		buffer.reserve_capacity(size);
	}
	template<class... Args>
	T* extend(size_t size, Args&&... args) { // reserve + init
		T* local_ptr = buffer.reserve(size);
		for (size_t i = 0; i < size; i++) {
			new (local_ptr + i) T(std::forward<Args>(args)...);
		}
		return local_ptr;
	}
	void clear(bool free_memory = false) {
		destroy(0, size());
		buffer.clear(free_memory);
	}
	void shrink_to_fit() {
		buffer.shrink_to_fit();
	}
	void delete_last(size_t count = 1) {
		size_t local_size = size();
		if (count > local_size) this->clear();
		else {
			destroy(local_size - count, count);
			buffer.resize(buffer.size() - count);
		}
	}

	size_t size() const { return buffer.size(); }
	size_t get_capacity() const { return buffer.capacity(); }

	bool empty() const {
		return this->size() == 0;
	}

	T* add(const T& item) {
		T* mem = buffer.reserve(1);
		return new (mem) T(item);
	}
	T* add(T&& item) {
		T* mem = buffer.reserve(1);
		return new (mem) T(std::move(item));
	}
	template<class... Args>
	T* emplace(Args&&... args) {
		T* mem = buffer.reserve(1);
		return new (mem) T(std::forward<Args>(args)...);
	}

	T* insert(size_t idx, const T& item) {
		T* mem = this->buffer.reserve_insert(idx);
		return new (mem) T(item);
	}

	void remove(size_t idx) {
		this->destroy(idx, 1);
		this->buffer.remove(idx);
	}

	T* ptr(size_t offset = 0) const {
		return buffer.ptr(offset);
	}
	T& get(size_t offset = 0) const { return buffer.get(offset); }
	T* last_ptr(size_t roffset = 1) const { return this->buffer.get_last_ptr(roffset); }
	T& last(size_t roffset = 1) const { return this->buffer.get_last(roffset); }
	T& operator[](ptrdiff_t offset) const { return this->buffer[offset]; }

	DynamicArray<T>& operator=(const DynamicArray<T>& rhs) {
		clear(false);
		return this->operator+=(rhs);
	}

	DynamicArray<T>& operator+=(const DynamicArray<T>& rhs) {
		size_t rhs_size = rhs.size();
		size_t i = 0;
		T* ptr_ = this->buffer.reserve(rhs_size);
		while (i < rhs_size) {
			new (ptr_ + i) T(rhs.get(i));
			i++;
		}
		return *this;
	}

	bool operator==(const DynamicArray<T>& other) const {
		return this->buffer == other.buffer;
	}

	PtrIterator<T> begin() const { return buffer.begin(); }
	PtrIterator<T> end() const { return buffer.end(); }

	size_t asize() override { return buffer.asize(); }
	AbstractIterator<T>* abegin(void* ptr) override { return buffer.abegin(ptr); }
	AbstractIterator<T>* aend(void* ptr) override { return buffer.aend(ptr); }
};
}
