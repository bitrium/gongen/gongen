// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "abstract.hpp"
#include "../error.hpp"

namespace gen {

template<typename T>
class LinkedList : public List<T> {
protected:
	struct Elem {
		T value;
		Elem* next;

		template<typename... Args>
		Elem(Args&&... args) : value(args...), next(nullptr) {
		}
	};

	template<typename Tc>
	class Iterator {
		Elem* elem;
	public:
		Iterator(Elem* elem) {
			this->elem = elem;
		}

		Tc& operator*() const {
			return this->elem->value;
		}
		Tc* operator->() const {
			return &this->elem->value;
		}

		Iterator& operator++() {
			if (this->elem) this->elem = this->elem->next;
			return *this;
		}
		Iterator operator++(int) {
			Iterator tmp = *this;
			++(*this);
			return tmp;
		}
		friend bool operator==(const Iterator& a, const Iterator& b) {
			return a.elem == b.elem;
		};
		friend bool operator!=(const Iterator& a, const Iterator& b) {
			return a.elem != b.elem;
		};
	};


	size_t size_ = 0;
	Elem* first = nullptr;
	Elem* last = nullptr;

	Elem* find(size_t index) const {
		if (index >= this->size_) {
			throw IndexException::createf("List index out of bounds (%zu >= %zu).", index, this->size_);
		}
		Elem* elem = this->first;
		for (size_t i = 0; i < index; i++) {
			elem = elem->next;
		}
		return elem;
	}
public:
	LinkedList() {
	}
	~LinkedList() {
		this->clear();
	}

	size_t size() const override {
		return this->size_;
	}

	bool has_ptr(const T* item) override {
		Elem* elem = this->first;
		while (elem) {
			if (&elem->value == item) return true;
			elem = elem->next;
		}
		return false;
	}
	bool contains_ptr(const T* preset) const override {
		Elem* elem = this->first;
		while (elem) {
			if (elem->value == *preset) return true;
			elem = elem->next;
		}
		return false;
	}

	const T& get(size_t index) const override {
		return this->find(index)->value;
	}
	T& get(size_t index) override {
		return this->find(index)->value;
	}
	size_t index_ptr(const T* item) const override {
		Elem* elem = this->first;
		size_t index = 0;
		while (elem) {
			if (&elem->value == item) return index;
			elem = elem->next;
			index++;
		}
		throw KeyException("Could not find the requested value.");
	}

	void clear() override {
		Elem* elem = this->first;
		while (elem) {
			Elem* tmp = elem;
			elem = elem->next;
			delete tmp;
		}
		this->first = nullptr;
		this->last = nullptr;
		this->size_ = 0;
	}
	void remove_idx(size_t index) override {
		if (!this->first) throw IndexException::createf("List index out of bounds (%zu >= 0).", index);
		Elem* elem;
		if (index == 0) {
			elem = this->first;
			if (elem == this->last) this->last = nullptr;
		} else {
			Elem* previous = this->find(index - 1);
			elem = previous->next;
			previous->next = elem->next;
			if (elem == this->last) this->last = previous;
		}
		this->size_--;
		delete elem;
	}
	void remove_ptr(T* item) override {
		if (!this->first) throw KeyException("Could not find the requested value.");
		Elem* elem;
		if (item == &this->first->value) {
			elem = this->first;
			this->first = elem->next;
			if (elem == this->last) this->last = nullptr;
		} else {
			Elem* previous = this->first;
			while (previous->next) {
				if (&previous->next->value == item) break;
				previous = previous->next;
			}
			if (!previous->next) throw KeyException("Could not find the requested value.");
			elem = previous->next;
			previous->next = elem->next;
			if (elem == this->last) this->last = previous;
		}
		this->size_--;
		delete elem;
	}
	void remove_match_ptr(const T* preset) override {
		if (!remove_match_ptr_safe(preset)) {
			throw KeyException("Could not find the requested value.");
		}
	}
	bool remove_match_ptr_safe(const T* preset) override {
		if (!this->first) return false;
		Elem* elem;
		if (*preset == this->first->value) {
			elem = this->first;
			this->first = elem->next;
			if (elem == this->last) this->last = nullptr;
		} else {
			Elem* previous = this->first;
			while (previous->next) {
				if (previous->next->value == *preset) break;
				previous = previous->next;
			}
			if (!previous->next) return false;
			elem = previous->next;
			previous->next = elem->next;
			if (elem == this->last) this->last = previous;
		}
		this->size_--;
		delete elem;
		return true;
	}

	T& add(const T& item) override {
		return this->emplace(item);
	}
	T& add(T&& item) override {
		return this->emplace(std::move(item));
	}
	template<class... Args>
	T& emplace(Args&&... args) /* override */ {
		Elem* elem = new Elem(std::forward<Args>(args)...);
		if (!this->first) {
			this->first = elem;
		} else {
			this->last->next = elem;
		}
		this->last = elem;
		this->size_++;
		return elem->value;
	}

	T& insert(const T& item, size_t index) override {
		return this->insert_emplace(index, item);
	}
	T& insert(T&& item, size_t index) override {
		return this->insert_emplace(index, item);
	}
	template<typename... Args>
	T& insert_emplace(size_t index, Args&&... args) /* override */ {
		if (index >= this->size_ - 1) {
			if (index == this->size_ - 1) return this->emplace(std::forward<Args>(args)...);
			throw IndexException::createf("List index out of bounds (%zu >= %zu).", index, this->size_);
		}
		Elem* elem = new Elem(std::forward<Args>(args)...);
		if (index == 0) {
			elem->next = this->first;
			this->first = elem;
		} else {
			Elem* previous = this->find(index - 1);
			elem->next = previous;
		}
		return elem->value;
	}

	Iterator<const T> cbegin() const {
		return Iterator<const T>(this->first);
	}
	Iterator<T> begin() {
		return Iterator<T>(this->first);
	}
	Iterator<const T> cend() const {
		return Iterator<const T>(nullptr);
	}
	Iterator<T> end() {
		return Iterator<T>(nullptr);
	}
};

// unordered!
template<typename T>
class BufferList : public List<T>, public AbstractIterable<T> {
	struct Element {
		bool deleted = false;
		uint8_t data[sizeof(T)];
		T& get_ref() const { return (T&) *this->data; }
		T* get_ptr() const { return (T*) this->data; }
	};

	class Iterator : public AbstractIterator<T> {
		BufferList<T>* list;
		size_t index = 0;
		size_t offset = 0;
	public:
		Iterator(BufferList* list, size_t index) {
			this->list = list;
			if (index >= list->size_) {
				this->index = list->size_;
				return;
			}
			while (this->list->buffer.get(this->offset).deleted) this->offset++;
			while (this->index < index) {
				++(*this);
			}
		}

		T* ptr() override {
			return this->list->buffer.get(this->offset).get_ptr();
		}
		void add(size_t count) override {
			for (size_t i = 0; i < count; i++) ++(*this);
		}
		bool equals(AbstractIterator<T>* other) override {
			if (!other) return false;
			return *this == *((Iterator*) other);
		}

		T& operator*() const {
			return this->list->buffer.get(this->offset).get_ref();
		}
		T* operator->() const {
			return this->list->buffer.get(this->offset).get_ptr();
		}

		Iterator& operator++() {
			this->index++;
			if (this->index >= this->list->size_) return *this;
			do {
				this->offset++;
			} while (this->list->buffer.get(this->offset).deleted);
			return *this;
		}
		Iterator operator++(int) {
			Iterator tmp = *this;
			++(*this);
			return tmp;
		}
		friend bool operator==(const Iterator& a, const Iterator& b) {
			return a.list == b.list && a.index == b.index;
		};
		friend bool operator!=(const Iterator& a, const Iterator& b) {
			return a.list != b.list || a.index != b.index;
		};
	};

	Buffer<Element> buffer;
	size_t size_ = 0;

	Element& find(size_t index) const {
		if (index >= this->size_) {
			throw IndexException::createf("List index out of bounds (%zu >= %zu).", index, this->size_);
		}
		size_t offset = 0;
		for (Element& elem : this->buffer) {
			if (elem.deleted) continue;
			if (offset == index) return elem;
			offset++;
		}
		throw StateException("Unexpectedly reached end of list.");
	}
	bool remove_internal(Element& elem) {
		if (elem.deleted) return false;
		elem.deleted = true;
		elem.get_ptr()->~T();
		this->size_--;
		return true;
	}
public:
	BufferList() {
	}
	BufferList(const BufferList& copied) {
		for (const T& value : (BufferList&) copied) {
			this->add(value);
		}
	}
	~BufferList() {
		for (Element& elem : this->buffer) {
			if (elem.deleted) continue;
			elem.get_ptr()->~T();
		}
		this->size_ = 0;
	}

	size_t size() const override {
		return this->size_;
	}

	bool has_ptr(const T* item) override {
		for (Element& elem : this->buffer) {
			if (elem.deleted) continue;
			if (item == elem.get_ptr()) return true;
		}
		return false;
	}
	bool contains_ptr(const T* preset) const override {
		for (Element& elem : this->buffer) {
			if (elem.deleted) continue;
			if (elem.get_ref() == *preset) return true;
		}
		return false;
	}

	const T& get(size_t index) const override {
		return this->find(index).get_ref();
	}
	T& get(size_t index) override {
		return (T&) ((const BufferList<T>*) this)->get(index);
	}
	size_t index_ptr(const T* item) const override {
		// FIXME just calculate offset from start pointer...
		size_t offset = 0;
		for (Element& elem : this->buffer) {
			if (elem.get_ptr() == item) return offset;
			offset++;
		}
		throw KeyException("Could not find the requested item.");
	}

	void clear() override {
		this->buffer.clear(true);
		this->size_ = 0;
	}
	void remove_idx(size_t index) override {
		Element& elem = this->find(index);
		this->remove_internal(elem);
	}
	void remove_ptr(T* item) override {
		for (Element& elem : this->buffer) {
			if (elem.get_ptr() != item) continue;
			if (!this->remove_internal(elem)) break;
			return;
		}
		throw KeyException("Could not find the requested item.");
	}
	void remove_match_ptr(const T* preset) override {
		if (!remove_match_ptr_safe(preset)) {
			throw KeyException("Could not find the requested value.");
		}
	}
	bool remove_match_ptr_safe(const T* preset) override {
		for (Element& elem : this->buffer) {
			if (elem.deleted) continue;
			if (elem.get_ref() == *preset) {
				if (!this->remove_internal(elem)) continue;
				return true;
			}
		}
		return false;
	}

	T& add(const T& item) override {
		return this->emplace(item);
	}
	T& add(T&& item) override {
		return this->emplace(std::move(item));
	}
	template<class... Args>
	T& emplace(Args&&... args) {
		Element* elem;
		if (this->size_ < this->buffer.size()) {
			// replace removed elemenet
			for (Element& element : this->buffer) {
				if (!element.deleted) continue;
				elem = &element;
				break;
			}
			GEN_ASSERT(elem, "BufferList size is incorrect.");
		} else {
			// append new element
			elem = this->buffer.emplace();
		}
		elem->deleted = true; // in case constructor throws
		new (elem->get_ptr()) T(std::forward<Args>(args)...);
		elem->deleted = false;
		this->size_++;
		return elem->get_ref();
	}

	T& insert(const T& item, size_t index) override {
		throw NoImplException("BufferList does not support inserting.");
	}
	T& insert(T&& item, size_t index) override {
		throw NoImplException("BufferList does not support inserting.");
	}

	bool operator==(const BufferList<T>& other) const {
		if (this->size_ != other.size_) return false;
		size_t offset_this = 0;
		size_t offset_other = 0;
		for (size_t i = 0; i < this->size_; i++) {
			while (this->buffer.get(offset_this).deleted) offset_this++;
			while (this->buffer.get(offset_other).deleted) offset_other++;
			if (this->buffer.get(offset_this).get_ref() != this->buffer.get(offset_other).get_ref()) return false;
			offset_this++;
			offset_other++;
		}
		return true;
	}

	Iterator begin() {
		return Iterator(this, 0);
	}
	Iterator end() {
		return Iterator(this, this->size_);
	}

	size_t asize() override {
		return sizeof(Iterator);
	}
	AbstractIterator<T>* abegin(void* ptr) override {
		return new (ptr) Iterator(this, 0);
	}
	AbstractIterator<T>* aend(void* ptr) override {
		return new (ptr) Iterator(this, this->size_);
	}
};

} // namespace gen
