// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <stdint.h>
#include <string.h>
#include <utility>
#include "../platform/memory.hpp"
#include "abstract.hpp"

namespace gen {
	
template<typename T>
class PtrIterator : public AbstractIterator<T> {
	T* ptr_;
public:
	PtrIterator(T* ptr_) noexcept { this->ptr_ = ptr_; }

	T& operator*() const noexcept { return *ptr_; }
	T* operator->() const noexcept { return ptr_; }

	T* get() const noexcept { return ptr_; }
	T* ptr() override { return ptr_; }

	void add(size_t count) override {
		ptr_ += count;
	}
	void substract(size_t count) override {
		ptr_ -= count;
	}
	bool equals(AbstractIterator<T>* other) override {
		auto* other_casted = dynamic_cast<PtrIterator<T>*>(other);
		if (!other_casted) return false;
		return ptr_ == other_casted->ptr_;
	}

	PtrIterator<T> operator+(ptrdiff_t count) noexcept {
		return PtrIterator(this->ptr_ + count);
	}
	PtrIterator<T> operator-(ptrdiff_t count) noexcept {
		return this->operator+(-count);
	}

	PtrIterator<T>& operator++() noexcept {
		ptr_++;
		return *this;
	}
	PtrIterator<T> operator++(int) noexcept {
		PtrIterator<T> tmp = *this;
		++(*this);
		return tmp;
	}

	friend bool operator==(const PtrIterator<T>& a, const PtrIterator<T>& b) { return a.ptr_ == b.ptr_; };
	friend bool operator!=(const PtrIterator<T>& a, const PtrIterator<T>& b) { return a.ptr_ != b.ptr_; };
};

namespace capacity_func {
// Next Power of 2 Capacity Function
inline size_t npo2(size_t size, size_t item_size) {
	size--;
	size |= (size >> 1);
	size |= (size >> 2);
	size |= (size >> 4);
	size |= (size >> 8);
	size |= (size >> 16);
#if SIZE_MAX == UINT64_MAX
	size |= (size >> 32);
#endif
	size++;
	return size;
}
// Linear Capacity Function
inline size_t linear(size_t size, size_t item_size) {
	return size;
}
typedef size_t(*func)(size_t, size_t);
} // namespace capacity_func

template<typename T>
class Buffer : public AbstractIterable<T> {
protected:
	T* ptr_ = nullptr;
	size_t size_ = 0;
	size_t capacity_ = 0;

	capacity_func::func capacity_function_ = gen::capacity_func::npo2;

	size_t capacity_function(size_t capacity) {
		if(capacity_function_) return capacity_function_(capacity, sizeof(T));
		else return gen::capacity_func::npo2(capacity, sizeof(T));
	}
public:
	explicit Buffer(capacity_func::func capacity_function) {
		this->capacity_function_ = capacity_function;
	}
	Buffer() : Buffer(gen::capacity_func::npo2) {
	}
	explicit Buffer(size_t start_capacity, size_t(*capacity_function)(size_t, size_t) = gen::capacity_func::npo2) {
		this->capacity_function_ = capacity_function;
		set_capacity(start_capacity);
	}
	Buffer(const Buffer<T>& other) {
		this->capacity_function_ = other.capacity_function_;
		add(other.ptr_, other.size_);
	}
	Buffer(Buffer<T>&& other) noexcept {
		memcpy((void*) this, (void*) &other, sizeof(gen::Buffer<T>));
		memset((void*) &other, 0, sizeof(gen::Buffer<T>));
	}
	~Buffer() noexcept { clear(true); }

	void set_capacity_function(capacity_func::func capacity_function = gen::capacity_func::npo2) {
		this->capacity_function_ = capacity_function;
	}
	void resize(size_t size) {
		if(size > capacity_) set_capacity(size);
		size_ = size;
	}
	T* reserve(size_t size) {
		size_t old_size = size_;
		resize(size_ + size);
		return ptr(old_size);
	}
	T* reserve_insert(size_t idx, size_t size = 1) {
		if (idx >= size_) return reserve(size);
		resize(size_ + size);
		memmove(this->ptr_ + idx + size, this->ptr_ + idx, (size_ - idx) * sizeof(T));
		return this->ptr_ + idx;
	}
	void reserve_capacity(size_t size) {
		if (this->size() + size <= this->capacity_) return;
		this->set_capacity(this->size() + size);
	}
	void set_capacity(size_t capacity) {
		size_t new_capacity = capacity_function(capacity);
		if (new_capacity == capacity_) return;
		if (size_ > new_capacity) size_ = new_capacity;
		ptr_ = tcrealloc<T>(ptr_, capacity_, new_capacity);
		capacity_ = new_capacity;
	}

	void clear(bool free_memory = false) noexcept {
		resize(0);
		if (free_memory) {
			if (ptr_) gen::free(ptr_);
			ptr_ = nullptr;
			capacity_ = 0;
		}
	}
	void shrink_to_fit() {
		if (ptr_) {
			ptr_ = gen::tcrealloc<T>(ptr_, capacity_, size_);
		}
	}
	void remove_last(size_t count = 1) {
		size_t size = this->size();
		if (count >= size) this->clear();
		else this->resize(size - count);
	}

	size_t size() const { return size_; }
	size_t size_bytes() const { return (size_ * sizeof(T)); }
	size_t capacity() const { return capacity_; }
	bool empty() const { return !size_; }

	T* add(const T* data, size_t data_size) {
		T* mem = reserve(data_size);
		memcpy((void*)mem, (const void*)data, data_size * sizeof(T));
		return mem;
	}
	T* add(const T& data) { return add(&data, 1); }
	template<class... Args>
	T* emplace(Args&&... args) {
		return new (reserve(1)) T(std::forward<Args>(args)...);
	}

	void remove(size_t start_idx, size_t count = 1) {
		memmove(this->ptr_ + start_idx, this->ptr_ + start_idx + count, (size_ - start_idx - count) * sizeof(T));
		this->size_ -= count;
	}

	T* ptr(size_t offset = 0) const {
		return (ptr_ + offset);
	}
	T& get(size_t offset = 0) const {
		return *this->ptr(offset);
	}

	T* get_last_ptr(size_t roffset = 1) const {
		return this->ptr_ + this->size() - roffset;
	}
	T& get_last(size_t roffset = 1) const {
		return *this->get_last_ptr(roffset);
	}

	T& operator[](ptrdiff_t offset) const {
		if (offset < 0) return this->get_last(-offset);
		return this->get(offset);
	}

	Buffer<T>& operator=(const Buffer<T>& other) {
		clear(false);
		this->capacity_function_ = other.capacity_function_;
		add(other.ptr_, other.size_);
		return *this;
	}

	bool operator==(const Buffer<T>& other) const {
		if (this->size_ != other.size_) return false;
		return !memcmp(ptr_, other.ptr_, size_ * sizeof(T));
	}
	bool operator!=(const Buffer<T>& other) {
		return !this->operator==(other);
	}

	PtrIterator<T> begin() const noexcept { return PtrIterator<T>(ptr()); }
	PtrIterator<T> end() const noexcept { return PtrIterator<T>(ptr(size())); }

	size_t asize() override {
		return sizeof(PtrIterator<T>);
	}
	AbstractIterator<T>* abegin(void* ptr) override {
		if (!ptr) return new PtrIterator<T>(this->ptr());
		return new (ptr) PtrIterator<T>(this->ptr());
	}
	AbstractIterator<T>* aend(void* ptr) override {
		if (!ptr) return new PtrIterator<T>(this->ptr(this->size()));
		return new (ptr) PtrIterator<T>(this->ptr(this->size()));
	}
};

class ByteBuffer :public Buffer<uint8_t> {
public:
	explicit ByteBuffer(size_t(*capacity_function)(size_t, size_t) = gen::capacity_func::npo2) : Buffer<uint8_t>(capacity_function) {}
	explicit ByteBuffer(size_t start_capacity, size_t(*capacity_function)(size_t, size_t) = gen::capacity_func::npo2) : Buffer<uint8_t>(start_capacity, capacity_function) {}
	~ByteBuffer() { clear(true); }
	ByteBuffer(const ByteBuffer& other) {
		add(other.ptr_, other.size_);
	}

	uint8_t* add(const void* data, size_t data_size) {
		uint8_t* mem = reserve(data_size);
		memcpy(mem, data, data_size);
		return mem;
	}
	uint8_t* add(uint8_t v) {
		return add(&v, 1);
	}

	ByteBuffer& operator=(const ByteBuffer& other) {
		clear(false);
		this->capacity_function_ = other.capacity_function_;
		add(other.ptr_, other.size_);
		return *this;
	}
};

}
