// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "../defines.hpp"
#include <utility>

namespace gen {

namespace internal {
	void GEN_API optional_error(const char* msg);
}

// flex is a value stored on stack, that can be freely constructed and destroyed
template<typename T>
class Flex {
	uint8_t buf[sizeof(T)];
public:
	Flex() {}

	inline T* ptr() {
		return ((T*) this->buf);
	}
	inline T& val() {
		return *((T*) this->buf);
	}

	inline const T* ptr() const {
		return ((T*) this->buf);
	}
	inline const T& val() const {
		return *((T*) this->buf);
	}

	template<typename... Args>
	inline T& construct(Args&&... args) {
		new (this->ptr()) T(std::forward<Args>(args)...);
		return this->val();
	}
	inline void destroy() {
		this->ptr()->~T();
	}
};

template<typename T>
class Optional {
private:
	bool is_set = false;
	Flex<T> flex;
public:
	Optional() {
		this->is_set = false;
	}
	Optional(const T& value) {
		this->set(value);
	}
	Optional(T&& value) {
		this->set(value);
	}
	Optional(const T* copied) {
		if (!copied) {
			this->is_set = false;
			return;
		}
		this->set(*copied);
	}
	~Optional() {
		this->reset();
	}

	bool some() const {
		return this->is_set;
	}

	bool none() const {
		return !this->is_set;
	}

	void reset() {
		if (!this->is_set) return;
		this->flex.destroy();
		this->is_set = false;
	}

	// T&& retrieve() {
	// 	if (!this->is_set) internal::optional_error("Cannot retrieve unset value.");
	// 	this->is_set = false;
	// 	return std::move(this->value);
	// }

	// T&& swap(T&& value) {
	// 	if (!this->is_set) internal::optional_error("Cannot swap unset value.");
	// 	T&& buf = std::move(this->value);
	// 	this->value = std::move(value);
	// 	return buf;
	// }

	template<class... Args>
	T& emplace(Args&&... args) {
		this->reset();
		this->flex.construct(std::forward<Args>(args)...);
		this->is_set = true;
		return this->flex.val();
	}

	T& get() {
		if (!this->is_set) internal::optional_error("Cannot get unset value.");
		return this->flex.val();
	}
	const T& get() const {
		if (!this->is_set) internal::optional_error("Cannot get unset value.");
		return this->flex.val();
	}
	T& get_or(const T& alt) {
		if (!this->is_set) return alt;
		return this->flex.val();
	}

	T& set(const T& value) {
		this->reset();
		this->is_set = true;
		return this->flex.construct(value);
	}
	T& set_move(T&& value) {
		this->reset();
		this->is_set = true;
		return this->flex.construct(value);
	}

	T* ptr() {
		if (!this->is_set) return nullptr;
		return this->flex.ptr();
	}
	const T* ptr() const {
		if (!this->is_set) return nullptr;
		return this->flex.ptr();
	}

	inline const T* operator->() const {
		if (!this->is_set) internal::optional_error("Cannot access unset value.");
		return this->ptr();
	}
	inline T* operator->() {
		if (!this->is_set) internal::optional_error("Cannot access unset value.");
		return this->ptr();
	}

	inline T& operator=(const T& value) {
		return this->set(value);
	}

	inline T& operator=(T&& value) {
		return this->set(std::move(value));
	}

	explicit operator bool() const noexcept {
		return this->some();
	}
};

}
