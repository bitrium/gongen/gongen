// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "dynamic_array.hpp"
#include "pointers.hpp"

namespace gen {

template<typename T>
class UniversalIterator {
	ByteBuffer buffer;
public:
	UniversalIterator() {
	}
	~UniversalIterator() {
		this->reset();
	}

	AbstractIterator<T>* get() const {
		if (buffer.empty()) return nullptr;
		return (AbstractIterator<T>*) this->buffer.ptr();
	}

	void reset() {
		auto* iterator = this->get();
		if (!iterator) return;
		iterator->~AbstractIterator<T>(); // TODO delete?
		this->buffer.clear();
	}

	UniversalIterator<T>& begin(AbstractIterable<T>* iterable) {
		this->reset();
		iterable->abegin(this->buffer.reserve(iterable->asize()));
		return *this;
	}
	UniversalIterator<T>& end(AbstractIterable<T>* iterable) {
		this->reset();
		iterable->aend(this->buffer.reserve(iterable->asize()));
		return *this;
	}

	T& operator*() const noexcept {
		return *this->get()->ptr();
	}
	T* operator->() const noexcept {
		return this->get()->ptr();
	}

	UniversalIterator<T>& operator++() noexcept {
		this->get()->add(1);
		return *this;
	}
//	UniversalIterator<T> operator++(int) noexcept {
//		UniversalIterator<T> tmp = *this;
//		++(*this);
//		return tmp;
//	}

	friend bool operator==(const UniversalIterator<T>& a, const UniversalIterator<T>& b) {
		auto* a_iterator = a.get();
		auto* b_iterator = b.get();
		return a_iterator == b_iterator || (a_iterator && a_iterator->equals(b_iterator));
	};
	friend bool operator!=(const UniversalIterator<T>& a, const UniversalIterator<T>& b) {
		auto* a_iterator = a.get();
		auto* b_iterator = b.get();
		return a_iterator != b_iterator && (!a_iterator || !a_iterator->equals(b_iterator));
	};
};

template<typename T>
class UniversalIterable {
	AbstractIterable<T>* iterable;
	bool ownership;
public:
	UniversalIterable(AbstractIterable<T>* iterable, bool take_ownership = true) {
		this->iterable = iterable;
		this->ownership = take_ownership;
	}
	~UniversalIterable() {
		if (this->ownership) delete this->iterable;
	}

	UniversalIterator<T> begin() {
		UniversalIterator<T> iterator;
		iterator.begin(this->iterable);
		return iterator;
	}
	UniversalIterator<T> end() {
		UniversalIterator<T> iterator;
		iterator.end(this->iterable);
		return iterator;
	}

	void unwrap(DynamicArray<T>& dest) {
		for (const T& t : *this) {
			dest.add(t);
		}
	}
};

template<typename T>
class EmptyIterable : public AbstractIterable<T> {
	size_t asize() override {
		return 0;
	}
	AbstractIterator<T>* abegin(void* mem = nullptr) override {
		return nullptr;
	}
	AbstractIterator<T>* aend(void* mem = nullptr) override {
		return nullptr;
	}
	AbstractIterator<const T>* acbegin() const override {
		return nullptr;
	}
	AbstractIterator<const T>* acend() const override {
		return nullptr;
	}
};

template<typename T, typename U>
class ConvertIterator : public AbstractIterator<T> {
	T* (*converter)(U*);
	AbstractIterator<U>* original;
public:
	ConvertIterator(T* (*converter)(U* original), AbstractIterator<U>* original) {
		this->converter = converter;
		this->original = original;
	}
	T* ptr() override {
		return this->converter(original->ptr());
	}
	void add(size_t count) override {
		this->original->add(count);
	}
	void substract(size_t count) override {
		this->original->substract(count);
	}
	bool equals(AbstractIterator<T>* other) override {
		auto* other_casted = dynamic_cast<ConvertIterator<T, U>*>(other);
		if (!other_casted) return false;
		return this->original->equals(other_casted->original);
	}
};

template<typename T, typename U>
class ConvertIterable : public AbstractIterable<T> {
	T* (*converter)(U*);
	AbstractIterable<U>* original;
	bool ownership;
public:
	ConvertIterable(T* (*converter)(U* original), AbstractIterable<U>* original, bool take_ownership = true) : original(original) {
		this->converter = converter;
		this->ownership = take_ownership;
	}
	~ConvertIterable() {
		if (this->ownership) delete this->original;
	}
	size_t asize() override {
		return sizeof(ConvertIterator<T, U>);
	}
	AbstractIterator<T>* abegin(void* ptr) override {
		return new (ptr) ConvertIterator(this->converter, original->abegin());
	}
	AbstractIterator<T>* aend(void* ptr) override {
		return new (ptr) ConvertIterator(this->converter, original->aend());
	}
};

} // namespace gen
