// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "dynamic_array.hpp"
#include "../error.hpp"

namespace gen {

template<typename K, typename T>
class SortedMap {
public:
	using KeyFunc = K(*)(const T&);
private:
	DynamicArray<T> data;

	size_t find(const K& key) const {
		if (this->data.size() == 0) return SIZE_MAX;
		size_t min = 0;
		size_t max = this->data.size() - 1;
		size_t pos = this->data.size() / 2;
		while (this->get_key(this->data.get(pos)) != key) {
			if (this->get_key(this->data.get(pos)) < key) {
				if (pos == max) return SIZE_MAX;
				min = pos + 1;
			} else {
				if (pos == min) return SIZE_MAX;
				max = pos - 1;
			}
			pos = (max - min) / 2 + min;
		}
		return pos;
	}
public:
	KeyFunc get_key;

	explicit SortedMap(KeyFunc get_key) {
		this->get_key = get_key;
	}

	T* get(const K& key) const {
		size_t pos = this->find(key);
		if (pos == SIZE_MAX) return nullptr;
		return this->data.ptr(pos);
	}

	T* add(const T& value) {
		if (this->data.size() == 0) return this->data.add(value);
		K key = this->get_key(value);
		size_t min = 0;
		size_t max = this->data.size() - 1;
		size_t pos = this->data.size() / 2;
		while (this->get_key(this->data.get(pos)) != key) {
			if (this->get_key(this->data.get(pos)) < key) {
				if (pos == max) {
					pos += 1;
					goto SUCCESS;
				}
				min = pos + 1;
			} else {
				if (pos == min) goto SUCCESS;
				max = pos - 1;
			}
			pos = (max - min) / 2 + min;
		}
		throw KeyException("Element with that key already exists.");

SUCCESS:
		return this->data.insert(pos, value);
	}

	void remove(const K& key) {
		size_t pos = this->find(key);
		if (pos == SIZE_MAX) return;
		this->data.remove(pos);
	}

	PtrIterator<T> begin() const { return this->data.begin(); }
	PtrIterator<T> end() const { return this->data.end(); }
};

}
