// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "buffer.hpp"

namespace gen {
	template<typename T>
	class BucketDynamicArray {
	protected:
		T** buckets = nullptr;
		size_t capacity = 0;
		size_t size = 0;

		size_t bucket_size = 32;
		bool set_to_zero = false;

		T* reserve() {
			size_t bucket_count = (capacity / bucket_size);
			if ((++size) > capacity) {
				size_t new_bucket_count = (bucket_count + 1);
				if (new_bucket_count > bucket_count) {
					if(set_to_zero) buckets = (T**)gen::crealloc(buckets, bucket_count, new_bucket_count);
					else buckets = (T**)gen::realloc(buckets, new_bucket_count);
				}
				capacity += (bucket_size);
				if (set_to_zero) buckets[bucket_count] = (T*)gen::calloc(bucket_size * sizeof(T));
				else buckets[bucket_count] = (T*)gen::alloc(bucket_size * sizeof(T));

				return buckets[bucket_count];
			}
			else {
				return get_ptr(size);
			}
		}
	public:
		BucketDynamicArray(size_t bucket_size = 16, bool set_to_zero = false) {
			this->bucket_size = bucket_size;
			this->set_to_zero = set_to_zero;
		}
		BucketDynamicArray(size_t start_size, size_t bucket_size = 16, bool set_to_zero = false) {
			this->bucket_size = bucket_size;
			this->set_to_zero = set_to_zero;
			reserve(start_size, false);
		}
		BucketDynamicArray(const BucketDynamicArray<T>& other) {
			this->bucket_size = other.bucket_size;
			this->set_to_zero = other.set_to_zero;
			for (uint32_t i = 0; i < other.get_size(); i++) {
				T* ptr = this->buffer.reserve();
				new (ptr) T(*other.get_ptr(i));
			}
		}
		BucketDynamicArray(BucketDynamicArray<T>&& other) noexcept {
			memcpy(this, &other, sizeof(gen::BucketDynamicArray<T>));
			memset(&other, 0, sizeof(gen::BucketDynamicArray<T>));
		}
		~BucketDynamicArray() noexcept { clear(true); }

		void clear(bool free_memory = false) noexcept {
			size = 0;
			size_t filled_buckets = (size / bucket_size);
			size_t last_bucket_size = (size % bucket_size);
			for (size_t i = 0; i < filled_buckets; i++) {
				if (i == (filled_buckets - 1)) {
					for (size_t j = 0; j < last_bucket_size; j++) (&buckets[i][j])->~T();
				}
				else {
					for (size_t j = 0; j < bucket_size; j++) (&buckets[i][j])->~T();
				}
			}
			if (free_memory) {
				size_t bucket_count = (capacity / bucket_size);
				for (size_t i = 0; i < bucket_count; i++) gen::free(buckets[i]);
				gen::free(buckets);
				buckets = nullptr;
				capacity = 0;
			}
		}

		size_t get_size() const { return size; }
		size_t get_size_in_bytes() const { return (size * sizeof(T)); }
		size_t get_capacity() const { return capacity; }

		bool empty() const {
			return (this->get_size() == 0);
		}

		T* add(const T& data) { 
			T* mem = reserve();
			return new (mem) T(data);
		}
		template<class... Args>
		T* emplace(Args&&... args) {
			T* mem = reserve();
			return new (mem) T(std::forward<Args>(args)...);
		}

		T* get_ptr(size_t offset = 0) const {
			size_t bucket_index = (offset / bucket_size);
			size_t in_bucket_index = (offset % bucket_size);
			return &buckets[bucket_index][in_bucket_index];
		}
		T& get(size_t offset = 0) const {
			return *this->get_ptr(offset);
		}

		//TODO
		/*BucketIterator<T> begin() const noexcept { return BucketIterator<T>(buckets, bucket_size, 0); }
		BucketIterator<T> end() const noexcept { 
			size_t bucket_index = (size / bucket_size);
			size_t in_bucket_index = (size % bucket_size);
			if(in_bucket_index != (bucket_size - 1)) return BucketIterator<T>((buckets + bucket_index), bucket_size, in_bucket_index);
			else return BucketIterator<T>((buckets + bucket_index + 1), bucket_size, 0);
		}*/
	};
}
