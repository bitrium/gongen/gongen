// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "math.hpp"

namespace gen {

template<uint32_t SIZE, typename T>
struct Vec;

template<typename T>
using vec2 = Vec<2, T>;
using vec2d = vec2<double>;
using vec2f = vec2<float>;
using vec2u = vec2<uint32_t>;
using vec2i = vec2<int32_t>;
using vec2z = vec2<size_t>;
using vec2u16 = vec2<uint16_t>;
using vec2i16 = vec2<int16_t>;
using vec2u8 = vec2<uint8_t>;
using vec2i8 = vec2<int8_t>;

template<typename T>
using vec3 = Vec<3, T>;
using vec3d = vec3<double>;
using vec3f = vec3<float>;
using vec3u = vec3<uint32_t>;
using vec3i = vec3<int32_t>;
using vec3z = vec3<size_t>;
using vec3u16 = vec3<uint16_t>;
using vec3i16 = vec3<int16_t>;
using vec3u8 = vec3<uint8_t>;
using vec3i8 = vec3<int8_t>;

template<typename T>
using vec4 = Vec<4, T>;
using vec4d = vec4<double>;
using vec4f = vec4<float>;
using vec4u = vec4<uint32_t>;
using vec4i = vec4<int32_t>;
using vec4z = vec4<size_t>;
using vec4u16 = vec4<uint16_t>;
using vec4i16 = vec4<int16_t>;
using vec4u8 = vec4<uint8_t>;
using vec4i8 = vec4<int8_t>;

#pragma pack(push, 1)
template<typename T>
struct Vec<2, T> {
	union {
		T x;
		T w;
	};
	union {
		T y;
		T h;
	};

	Vec(T val = 0) : x(val), y(val) {}
	Vec(T x, T y) : x(x), y(y) {}

	template<typename U>
	Vec(const Vec<2, U>& other) : x((T) other.x), y((T) other.y) {}

	Vec<2, T>& operator=(const Vec<2, T>& other) {
		this->x = other.x;
		this->y = other.y;
		return *this;
	}
	Vec<2, T>& operator=(const T* arr) {
		this->x = arr[0];
		this->y = arr[1];
		return *this;
	}

	bool operator==(const Vec<2, T>& other) {
		return this->x == other.x && this->y == other.y;
	}
	bool operator!=(const Vec<2, T>& other) {
		return this->x != other.x || this->y != other.y;
	}


	Vec<2, T>& operator+=(const Vec<2, T>& other) {
		this->x += other.x;
		this->y += other.y;
		return *this;
	}
	Vec<2, T>& operator-=(const Vec<2, T>& other) {
		this->x -= other.x;
		this->y -= other.y;
		return *this;
	}
	Vec<2, T>& operator*=(const Vec<2, T>& other) {
		this->x *= other.x;
		this->y *= other.y;
		return *this;
	}
	Vec<2, T>& operator*=(T multiplier) {
		this->x *= multiplier;
		this->y *= multiplier;
		return *this;
	}
	Vec<2, T>& operator/=(T multiplier) {
		this->x /= multiplier;
		this->y /= multiplier;
		return *this;
	}

	Vec<2, T> operator+(const Vec<2, T>& other) const {
		return Vec<2, T>(this->x + other.x, this->y + other.y);
	}
	Vec<2, T> operator-(const Vec<2, T>& other) const {
		return Vec<2, T>(this->x - other.x, this->y - other.y);
	}
	Vec<2, T> operator*(const Vec<2, T>& other) const {
		return Vec<2, T>(this->x * other.x, this->y * other.y);
	}
	Vec<2, T> operator*(T multiplier) const {
		return Vec<2, T>(this->x * multiplier, this->y * multiplier);
	}
	Vec<2, T> operator/(T multiplier) const {
		return Vec<2, T>(this->x / multiplier, this->y / multiplier);
	}
	Vec<2, T> operator/(const Vec<2, T>& other) const {
		return Vec<2, T>(this->x / other.x, this->y / other.y);
	}

	bool operator<(const Vec<2, T>& other) const {
		return this->x < other.x && this->y < other.y;
	}
	bool operator>(const Vec<2, T>& other) const {
		return this->x > other.x && this->y > other.y;
	}

	Vec<2, T>& to_abs() {
		if (this->x < 0) this->x *= -1;
		if (this->y < 0) this->y *= -1;
		return *this;
	}
	Vec<2, T> abs() {
		Vec<2, T> result = *this;
		result.to_abs();
		return result;
	}

	Vec<2, T>& to_rotated(T angle) {
		T sin_value = sin(angle);
		T cos_value = cos(angle);
		T x = this->x;
		this->x = (this->y * sin_value) + (x * cos_value);
		this->y = (this->y * cos_value) - (x * sin_value);
		return *this;
	}
	Vec<2, T> rotated(T angle) {
		Vec<2, T> result = *this;
		result.to_rotated(angle);
		return result;
	}

	T distance_sq() const {
		return (x * x) + (y * y);
	}
	T distance() const {
		return gen::sqrt(distance_sq());
	}
	T distance_sq(const Vec<2, T>& other) const {
		const Vec<2, T> diff = *this - other;
		return diff.distance_sq();
	}
	T distance(const Vec<2, T>& other) const {
		return gen::sqrt(distance_sq(other));
	}

	Vec<3, T> xYz(T y) {
		return Vec<3, T>(this->x, y, this->y);
	}
};

template<typename T>
struct Vec<3, T> {
	T x, y, z;

	Vec(T val = 0) : x(val), y(val), z(val) {}
	Vec(T x, T y, T z) : x(x), y(y), z(z) {}

	template<typename U>
	Vec(const Vec<3, U>& other) : x((T)other.x), y((T)other.y), z((T)other.z) {}

	Vec<3, T>& operator=(const Vec<3, T>& other) = default;

	bool operator==(const Vec<3, T>& other) {
		return this->x == other.x && this->y == other.y && this->z == other.z;
	}
	bool operator!=(const Vec<3, T>& other) {
		return this->x != other.x || this->y != other.y || this->z != other.z;
	}

	Vec<3, T> operator+(const Vec<3, T>& other) const {
		return Vec<3, T>(this->x + other.x, this->y + other.y, this->z + other.z);
	}
	Vec<3, T> operator-(const Vec<3, T>& other) const {
		return Vec<3, T>(this->x - other.x, this->y - other.y, this->z - other.z);
	}
	Vec<3, T> operator*(const Vec<3, T>& other) const {
		return Vec<3, T>(this->x * other.x, this->y * other.y, this->z * other.z);
	}
	Vec<3, T> operator*(T multiplier) const {
		return Vec<3, T>(this->x * multiplier, this->y * multiplier, this->z * multiplier);
	}
	Vec<3, T> operator/(T multiplier) const {
		return Vec<3, T>(this->x / multiplier, this->y / multiplier, this->z / multiplier);
	}
	Vec<3, T> operator/(const Vec<3, T>& other) const {
		return Vec<3, T>(this->x / other.x, this->y / other.y, this->z / other.z);
	}
	Vec<3, T>& operator+=(const Vec<3, T>& other) {
		this->x += other.x;
		this->y += other.y;
		this->z += other.z;
		return *this;
	}
	Vec<3, T>& operator-=(const Vec<3, T>& other) {
		this->x -= other.x;
		this->y -= other.y;
		this->z -= other.z;
		return *this;
	}
	Vec<3, T>& operator*=(const Vec<3, T>& other) {
		this->x *= other.x;
		this->y *= other.y;
		this->z *= other.z;
		return *this;
	}
	Vec<3, T>& operator*=(T multiplier) {
		this->x *= multiplier;
		this->y *= multiplier;
		this->z *= multiplier;
		return *this;
	}
	Vec<3, T>& operator/=(const Vec<3, T>& other) {
		this->x /= other.x;
		this->y /= other.y;
		this->z /= other.z;
		return *this;
	}
	Vec<3, T>& operator/=(T multiplier) {
		this->x /= multiplier;
		this->y /= multiplier;
		this->z /= multiplier;
		return *this;
	}
	Vec<3, T> operator/=(const Vec<3, T>& other) const {
		this->x /= other.x;
		this->y /= other.y;
		this->z /= other.z;
		return *this;
	}

	Vec<3, T>& to_abs() {
		if (this->x < 0) this->x *= -1;
		if (this->y < 0) this->y *= -1;
		if (this->z < 0) this->z *= -1;
		return *this;
	}
	Vec<3, T> abs() {
		Vec<3, T> result = *this;
		result.to_abs();
		return result;
	}

	Vec<3, T> clamp_distance(T min = T(0), T max = T(1)) const {
		T distance = this->distance();
		if (distance == 0) return *this;
		distance = clamp_val(distance, min, max) / distance;
		return *this * distance;
	}

	T distance_sq() const {
		return (x * x) + (y * y) + (z * z);
	}
	T distance() const {
		return gen::sqrt(distance_sq());
	}
	T distance_sq(const Vec<3, T>& other) const {
		return (*this - other).distance_sq();
	}
	T distance(const Vec<3, T>& other) const {
		return gen::sqrt(distance_sq(other));
	}

	Vec<2, T> xz() {
		return Vec<2, T>(this->x, this->z);
	}

	bool operator==(const Vec<3, T>& other) const {
		return this->x == other.x && this->y == other.y && this->z == other.z;
	}
};

template<typename T>
struct Vec<4, T> {
	T x, y, z, w;

	Vec(T val = 0) : x(val), y(val), z(val), w(val) {}
	Vec(T x, T y, T z, T w) : x(x), y(y), z(z), w(w) {}

	template<typename U>
	Vec(const Vec<4, U>& other) : x((T)other.x), y((T)other.y), z((T)other.z), w((T)other.w) {}

	template<typename U>
	Vec(const Vec<3, U>& other, T w = 1.0f) : x((T)other.x), y((T)other.y), z((T)other.z), w(w) {}

	Vec<4, T>& operator=(const Vec<4, T>& other) {
		this->x = other.x;
		this->y = other.y;
		this->z = other.z;
		this->w = other.w;
		return *this;
	}

	bool operator==(const Vec<4, T>& other) {
		return this->x == other.x && this->y == other.y && this->z == other.z && this->w == other.w;
	}
	bool operator!=(const Vec<4, T>& other) {
		return this->x != other.x || this->y != other.y || this->z != other.z || this->w != other.w;
	}


	Vec<4, T>& operator+=(const Vec<4, T>& other) {
		this->x += other.x;
		this->y += other.y;
		this->z += other.z;
		this->w += other.w;
		return *this;
	}
	Vec<4, T>& operator-=(const Vec<4, T>& other) {
		this->x -= other.x;
		this->y -= other.y;
		this->z -= other.z;
		this->w -= other.w;
		return *this;
	}
	Vec<4, T>& operator*=(const Vec<4, T>& other) {
		this->x *= other.x;
		this->y *= other.y;
		this->z *= other.z;
		this->w *= other.w;
		return *this;
	}
	Vec<4, T>& operator*=(T multiplier) {
		this->x *= multiplier;
		this->y *= multiplier;
		this->z *= multiplier;
		this->w *= multiplier;
		return *this;
	}
	Vec<4, T>& operator/=(T multiplier) {
		this->x /= multiplier;
		this->y /= multiplier;
		this->z /= multiplier;
		this->w /= multiplier;
		return *this;
	}

	Vec<4, T> operator+(const Vec<4, T>& other) const {
		return Vec<4, T>(this->x + other.x, this->y + other.y, this->z + other.z, this->w + other.w);
	}
	Vec<4, T> operator-(const Vec<4, T>& other) const {
		return Vec<4, T>(this->x - other.x, this->y - other.y, this->z - other.z, this->w - other.w);
	}
	Vec<4, T> operator*(const Vec<4, T>& other) const {
		return Vec<4, T>(this->x * other.x, this->y * other.y, this->z * other.z, this->w * other.w);
	}
	Vec<4, T> operator*(T multiplier) const {
		return Vec<4, T>(this->x * multiplier, this->y * multiplier, this->z * multiplier, this->w * multiplier);
	}
	Vec<4, T> operator/(T multiplier) const {
		return Vec<4, T>(this->x / multiplier, this->y / multiplier, this->z / multiplier, this->w / multiplier);
	}

	Vec<4, T>& to_abs() {
		if (this->x < 0) this->x *= -1;
		if (this->y < 0) this->y *= -1;
		if (this->z < 0) this->z *= -1;
		if (this->w < 0) this->w *= -1;
		return *this;
	}
	Vec<4, T> abs() {
		Vec<4, T> result = *this;
		result.to_abs();
		return result;
	}

	Vec<3, T> perspective_divide() {
		return Vec<3, T>(x / w, y / w, z / w);
	}
};
#pragma pack(pop)

//Vector functions
template<typename T>
T length(const vec2<T>& vector) {
	return gen::sqrt<T>((vector.x * vector.x) + (vector.y * vector.y));
}
template<typename T>
T length(const vec3<T>& vector) {
	return gen::sqrt<T>((vector.x * vector.x) + (vector.y * vector.y) + (vector.z * vector.z));
}
template<typename T>
T length(const vec4<T>& vector) {
	return gen::sqrt<T>((vector.x * vector.x) + (vector.y * vector.y) + (vector.z * vector.z) + (vector.w * vector.w));
}
template<typename T>
T length(const vec2<T>& vector, const vec2<T>& vector2) {
	T dx = vector.x - vector2.x;
	T dy = vector.y - vector2.y;
	return gen::sqrt<T>((dx * dx) + (dy * dy));
}
template<typename T>
T length(const vec3<T>& vector, const vec3<T>& vector2) {
	T dx = vector.x - vector2.x;
	T dy = vector.y - vector2.y;
	T dz = vector.z - vector2.z;
	return gen::sqrt<T>((dx * dx) + (dy * dy) + (dz * dz));
}
template<typename T>
T length(const vec4<T>& vector, const vec4<T>& vector2) {
	T dx = vector.x - vector2.x;
	T dy = vector.y - vector2.y;
	T dz = vector.z - vector2.z;
	T dw = vector.w - vector2.w;
	return gen::sqrt<T>((dx * dx) + (dy * dy) + (dz * dz) + (dw * dw));
}

template<typename T>
T length2(const vec2<T>& vector) {
	return ((vector.x * vector.x) + (vector.y * vector.y));
}
template<typename T>
T length2(const vec3<T>& vector) {
	return ((vector.x * vector.x) + (vector.y * vector.y) + (vector.z * vector.z));
}
template<typename T>
T length2(const vec2<T>& vector, const vec2<T>& vector2) {
	T dx = vector.x - vector2.x;
	T dy = vector.y - vector2.y;
	return ((dx * dx) + (dy * dy));
}
template<typename T>
T length2(const vec3<T>& vector, const vec3<T>& vector2) {
	T dx = vector.x - vector2.x;
	T dy = vector.y - vector2.y;
	T dz = vector.z - vector2.z;
	return ((dx * dx) + (dy * dy) + (dz * dz));
}

template<typename T>
T dot(const vec3<T>& vector, const vec3<T>& vector2) {
	return (vector.x * vector2.x) + (vector.y * vector2.y) + (vector.z * vector2.z);
}
template<typename T>
T dot(const vec4<T>& vector, const vec4<T>& vector2) {
	return (vector.x * vector2.x) + (vector.y * vector2.y) + (vector.z * vector2.z) + (vector.w * vector2.w);
}

template<typename T>
vec3<T> cross(const vec3<T>& vector, const vec3<T>& vector2) {
	vec3<T> out;
	out.x = (vector.y * vector2.z) - (vector.z * vector2.y);
	out.y = (vector.z * vector2.x) - (vector.x * vector2.z);
	out.z = (vector.x * vector2.y) - (vector.y * vector2.x);
	return out;
}

template<uint32_t SIZE, typename T>
Vec<SIZE, T> normalize(const Vec<SIZE, T>& vector) {
	return vector / length(vector);
}
template<uint32_t SIZE, typename T>
Vec<SIZE, T> normalize_safe(const Vec<SIZE, T>& vector) {
	T l = length(vector);
	if (l == 0) return vector;
	return vector / l;
}

template<typename T>
vec2<T> vector_min(const vec2<T>& vector, vec2<T>& min) {
	if (vector.x < min.x) min.x = vector.x;
	if (vector.y < min.y) min.y = vector.y;
	return min;
}
template<typename T>
vec3<T> vector_min(const vec3<T>& vector, vec3<T>& min) {
	if (vector.x < min.x) min.x = vector.x;
	if (vector.y < min.y) min.y = vector.y;
	if (vector.z < min.z) min.z = vector.z;
	return min;
}
template<typename T>
vec4<T> vector_min(const vec4<T>& vector, vec4<T>& min) {
	if (vector.x < min.x) min.x = vector.x;
	if (vector.y < min.y) min.y = vector.y;
	if (vector.z < min.z) min.z = vector.z;
	if (vector.w < min.w) min.w = vector.w;
	return min;
}
template<typename T>
vec2<T> vector_max(const vec2<T>& vector, vec2<T>& min) {
	if (vector.x > min.x) min.x = vector.x;
	if (vector.y > min.y) min.y = vector.y;
	return min;
}
template<typename T>
vec3<T> vector_max(const vec3<T>& vector, vec3<T>& min) {
	if (vector.x > min.x) min.x = vector.x;
	if (vector.y > min.y) min.y = vector.y;
	if (vector.z > min.z) min.z = vector.z;
	return min;
}
template<typename T>
vec4<T> vector_max(const vec4<T>& vector, vec4<T>& min) {
	if (vector.x > min.x) min.x = vector.x;
	if (vector.y > min.y) min.y = vector.y;
	if (vector.z > min.z) min.z = vector.z;
	if (vector.w > min.w) min.w = vector.w;
	return min;
}

} // namespace gen
