// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "vector.hpp"
#include <cstring>

namespace gen {
#pragma pack(push, 8)
struct Color8 {
	uint8_t r, g, b, a;
};
#pragma pack(pop)
enum class ColorSpace {
	LINEAR,
	SRGB
};

#pragma pack(push, 8)
struct GEN_API Color {
public:
	union {
		float data[4];
		struct {
			union {
				float v;
				float r;
			};
			float g;
			float b;
			float a;
		};
	};
	void set(float r, float g, float b, float a) {
		this->data[0] = r; this->data[1] = g; this->data[2] = b; this->data[3] = a;
	}
	void set_u8(uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
		this->data[0] = (r / 255.0f); this->data[1] = (g / 255.0f); this->data[2] = (b / 255.0f); this->data[3] = (a / 255.0f);
	}
	Color() {
		set(0.0f, 0.0f, 0.0f, 1.0f);
	}
	Color(const Color& rhs) { memcpy(this, rhs.data, sizeof(Color)); }
	Color(Color&& color) { memcpy(this, color.data, sizeof(Color)); }
	static Color rgba(float r, float g, float b, float a) {
		Color col; col.set(r, g, b, a); return col;
	}
	static Color rgb(float r, float g, float b) {
		Color col; col.set(r, g, b, 1.0f); return col;
	}
	static Color greyscale_alpha(float v, float a) {
		Color col; col.set(v, v, v, a); return col;
	}
	static Color greyscale(float v) {
		Color col; col.set(v, v, v, 1.0f); return col;
	}
	static Color rgba8(uint8_t r, uint8_t g, uint8_t b, uint8_t a) {
		Color col; col.set_u8(r, g, b, a); return col;
	}
	static Color rgb8(uint8_t r, uint8_t g, uint8_t b) {
		Color col; col.set_u8(r, g, b, 1.0f); return col;
	}
	static Color greyscale_alpha8(uint8_t v, uint8_t a) {
		Color col; col.set_u8(v, v, v, a); return col;
	}
	static Color greyscale8(uint8_t v) {
		Color col; col.set_u8(v, v, v, 1.0f); return col;
	}
	static Color from_vec(const gen::vec4f& v) {
		Color col; col.set_u8(v.x, v.y, v.z, v.w); return col;
	}
	static Color from_vec(const gen::vec3f& v) {
		Color col; col.set_u8(v.x, v.y, v.z, 1.0f); return col;
	}
	static Color from_color8(const Color8& color) {
		Color col; col.set_u8(color.r, color.g, color.b, color.a); return col;
	}
	static Color from_string(const char* hex_string, ColorSpace color_space = ColorSpace::SRGB) {
		Color out;
		size_t str_len = GEN_SAFE_STRLEN(hex_string);
		if (str_len) {
			if (hex_string[0] == '#') {
				str_len--;
				hex_string++;
			}
			if (str_len >= 6) {
				uint8_t r = (GEN_HEX_TO_DEC_DIGIT(hex_string[0]) * 16) + GEN_HEX_TO_DEC_DIGIT(hex_string[1]);
				uint8_t g = (GEN_HEX_TO_DEC_DIGIT(hex_string[2]) * 16) + GEN_HEX_TO_DEC_DIGIT(hex_string[3]);
				uint8_t b = (GEN_HEX_TO_DEC_DIGIT(hex_string[4]) * 16) + GEN_HEX_TO_DEC_DIGIT(hex_string[5]);
				out.set_u8(r, g, b, 255);
			}
		}
		return out;
	}
	uint32_t to_u32() const {
		return GEN_FOURCC((uint8_t)(data[0] * 255), (uint8_t)(data[1] * 255), (uint8_t)(data[2] * 255), (uint8_t)(data[3] * 255));
	}
	Color8 to_color8() const {
		return { (uint8_t)(data[0] * 255), (uint8_t)(data[1] * 255), (uint8_t)(data[2] * 255), (uint8_t)(data[3] * 255) };
	}
	gen::vec4f to_vec() const {
		return gen::vec4f(data[0], data[1], data[2], data[3]);
	}
	Color to_linear(ColorSpace from = ColorSpace::SRGB) const {
		if (from == ColorSpace::LINEAR) return *this;
		Color out;
		for (size_t i = 0; i < 3; i++) {
			if (data[i] <= 0.04045f) {
				out.data[i] = (data[i] / 12.92f);
			}
			else {
				out.data[i] = gen::pow((data[i] + 0.055f) / 1.055f, 2.4f);
			}
		}
		out.data[3] = data[3];
		return out;
	}
	Color to_srgb(ColorSpace from = ColorSpace::LINEAR) const {
		if (from == ColorSpace::SRGB) return *this;
		Color out;
		for (size_t i = 0; i < 3; i++) {
			if (data[i] <= 0.0031308f) {
				out.data[i] = (data[i] * 12.92f);
			}
			else {
				out.data[i] = ((1.055f * gen::pow(data[i], 1.0f / 2.4f)) - 0.055f);
			}
		}
		out.data[3] = data[3];
		return out;
	}
	Color to_grayscale(ColorSpace from = ColorSpace::LINEAR) const {
		Color lcolor = *this;
		lcolor = lcolor.to_linear(from);
		float lum = (lcolor.data[0] * 0.2126f) + (lcolor.data[1] * 0.7152f) + (lcolor.data[2] + 0.0722f);
		return Color::rgba(lum, lum, lum, data[3]);
	}
	gen::Color& operator=(const Color& rhs) {
		this->r = rhs.r; this->g = rhs.g; this->b = rhs.b; this->a = rhs.a;
		return *this;
	}
};
#pragma pack(pop)
}
