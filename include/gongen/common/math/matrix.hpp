// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

// Contains modified code from https://github.com/felselva/mathc (zlib license)

#pragma once

#include "vector.hpp"

namespace gen {
template<uint32_t SIZE, typename T>
struct Mat;

template<typename T>
using mat4 = Mat<4, T>;
using mat4d = mat4<double>;
using mat4f = mat4<float>;

template<typename T>
using mat3 = Mat<3, T>;
using mat3d = mat3<double>;
using mat3f = mat3<float>;

#pragma pack(push, 1)
template<typename T>
struct Mat<4, T> {
	T data[16];

	Mat() {
		memset(data, 0, sizeof(T) * 16);
		data[0] = 1;
		data[5] = 1;
		data[10] = 1;
		data[15] = 1;
	}

	Mat(const Mat<3, T>& other) {
		memset(data, 0, sizeof(T) * 16);
		data[0] = other.data[0];
		data[1] = other.data[1];
		data[2] = other.data[2];
		data[4] = other.data[3];
		data[5] = other.data[4];
		data[6] = other.data[5];
		data[8] = other.data[6];
		data[9] = other.data[7];
		data[10] = other.data[8];
		data[15] = 1;
	}

	Mat<4, T> operator*(const Mat<4, T>& other) const {
		Mat<4, T> out;
		T* A = (T*)this;
		T* B = (T*)&other;
		T* C = (T*)&out;
		C[0] = A[0] * B[0] + A[4] * B[1] + A[8] * B[2] + A[12] * B[3];
		C[1] = A[1] * B[0] + A[5] * B[1] + A[9] * B[2] + A[13] * B[3];
		C[2] = A[2] * B[0] + A[6] * B[1] + A[10] * B[2] + A[14] * B[3];
		C[3] = A[3] * B[0] + A[7] * B[1] + A[11] * B[2] + A[15] * B[3];
		C[4] = A[0] * B[4] + A[4] * B[5] + A[8] * B[6] + A[12] * B[7];
		C[5] = A[1] * B[4] + A[5] * B[5] + A[9] * B[6] + A[13] * B[7];
		C[6] = A[2] * B[4] + A[6] * B[5] + A[10] * B[6] + A[14] * B[7];
		C[7] = A[3] * B[4] + A[7] * B[5] + A[11] * B[6] + A[15] * B[7];
		C[8] = A[0] * B[8] + A[4] * B[9] + A[8] * B[10] + A[12] * B[11];
		C[9] = A[1] * B[8] + A[5] * B[9] + A[9] * B[10] + A[13] * B[11];
		C[10] = A[2] * B[8] + A[6] * B[9] + A[10] * B[10] + A[14] * B[11];
		C[11] = A[3] * B[8] + A[7] * B[9] + A[11] * B[10] + A[15] * B[11];
		C[12] = A[0] * B[12] + A[4] * B[13] + A[8] * B[14] + A[12] * B[15];
		C[13] = A[1] * B[12] + A[5] * B[13] + A[9] * B[14] + A[13] * B[15];
		C[14] = A[2] * B[12] + A[6] * B[13] + A[10] * B[14] + A[14] * B[15];
		C[15] = A[3] * B[12] + A[7] * B[13] + A[11] * B[14] + A[15] * B[15];
		return out;
	}
	Vec<4, T> operator*(const Vec<4, T>& other) const {
		Vec<4, T> out;
		T* A = (T*)this;
		T* B = (T*)&other;
		T* C = (T*)&out;
		C[0] = A[0] * B[0] + A[4] * B[1] + A[8] * B[2] + A[12] * B[3];
		C[1] = A[1] * B[0] + A[5] * B[1] + A[9] * B[2] + A[13] * B[3];
		C[2] = A[2] * B[0] + A[6] * B[1] + A[10] * B[2] + A[14] * B[3];
		C[3] = A[3] * B[0] + A[7] * B[1] + A[11] * B[2] + A[15] * B[3];
		return out;
	}
	static Mat<4, T> scale(const Vec<3, T>& scale) {
		return Mat<4, T>::scale(scale.x, scale.y, scale.z);
	}
	static Mat<4, T> scale(T x, T y, T z) {
		Mat<4, T> out;
		out.data[0] = x;
		out.data[5] = y;
		out.data[10] = z;
		return out;
	}
	static Mat<4, T> translate(const Vec<3, T>& pos) {
		return Mat<4, T>::translate(pos.x, pos.y, pos.z);
	}
	static Mat<4, T> translate(T x, T y, T z) {
		Mat<4, T> out;
		out.data[12] = x;
		out.data[13] = y;
		out.data[14] = z;
		return out;
	}
	static Mat<4, T> rotate(const Vec<3, T>& rotation) {
		return Mat<4, T>::rotate(rotation.x, rotation.y, rotation.z);
	}
	static Mat<4, T> rotate(T x, T y, T z) {
		T sin_x = gen::sin(x); T sin_y = gen::sin(y); T sin_z = gen::sin(z);
		T cos_x = gen::cos(x); T cos_y = gen::cos(y); T cos_z = gen::cos(z);
		Mat<4, T> out;
		out.data[0] = cos_y * cos_z;
		out.data[1] = cos_y * sin_z;
		out.data[2] = -sin_y;
		out.data[4] = (sin_x * sin_y * cos_z) - (cos_x * sin_z);
		out.data[5] = (sin_x * sin_y * sin_z) + (cos_x * cos_z);
		out.data[6] = (sin_x * cos_y);
		out.data[8] = (cos_x * sin_y * cos_z) + (sin_x * sin_z);
		out.data[9] = (cos_x * sin_y * sin_z) - (sin_x * cos_z);
		out.data[10] = (cos_x * cos_y);
		return out;
	}
	static Mat<4, T> rotate(const Vec<4, T>& quaternion) {
		return Mat<4, T>::rotate(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
	}
	static Mat<4, T> rotate(T x, T y, T z, T w) {
		T xx = (x * x);
		T yy = (y * y);
		T zz = (z * z);
		Mat<4, T> out;
		out.data[0] = 1 - 2 * (yy + zz);
		out.data[1] = 2 * (x * y + w * z);
		out.data[2] = 2 * (x * z - w * y);

		out.data[4] = 2 * (x * y - w * z);
		out.data[5] = 1 - 2 * (xx + zz);
		out.data[6] = 2 * (w * x + y * z);

		out.data[8] = 2 * (w * y + x * z);
		out.data[9] = 2 * (y * z - w * x);
		out.data[10] = 1 - 2 * (xx + yy);
		return out;
	}
	static Mat<4, T> rotate_scale(const Vec<3, T>& rotation, const Vec<3, T>& scale) {
		T sin_x = gen::sin(rotation.x); T sin_y = gen::sin(rotation.y); T sin_z = gen::sin(rotation.z);
		T cos_x = gen::cos(rotation.x); T cos_y = gen::cos(rotation.y); T cos_z = gen::cos(rotation.z);
		Mat<3, T> rotate_mat;
		Mat<3, T> scale_mat;
		rotate_mat.data[0] = cos_y * cos_z;
		rotate_mat.data[1] = cos_y * sin_z;
		rotate_mat.data[2] = -sin_y;
		rotate_mat.data[3] = (sin_x * sin_y * cos_z) - (cos_x * sin_z);
		rotate_mat.data[4] = (sin_x * sin_y * sin_z) + (cos_x * cos_z);
		rotate_mat.data[5] = (sin_x * cos_y);
		rotate_mat.data[6] = (cos_x * sin_y * cos_z) + (sin_x * sin_z);
		rotate_mat.data[7] = (cos_x * sin_y * sin_z) - (sin_x * cos_z);
		rotate_mat.data[8] = (cos_x * cos_y);
		scale_mat.data[0] = scale.x;
		scale_mat.data[4] = scale.y;
		scale_mat.data[8] = scale.z;
		Mat<4, T> out(rotate_mat * scale_mat);
		return out;
	}
	static Mat<4, T> transform(const Vec<3, T>& position, const Vec<3, T>& rotation, const Vec<3, T>& scale) {
		Mat<4, T> out = rotate_scale(rotation, scale);
		out.data[12] = position.x;
		out.data[13] = position.y;
		out.data[14] = position.z;
		return out;
	}
	static Mat<4, T> perspective_reverse_z_gl(T z_near, T z_far, T aspect_ratio, T fov) {
		T tan_value = gen::tan(fov / 2);
		Mat<4, T> out;
		out.data[0] = 1 / (aspect_ratio * tan_value);
		out.data[5] = 1 / tan_value;
		out.data[10] = -(z_far + z_near) / (z_near - z_far);
		out.data[11] = -1;
		out.data[14] = (-2 * z_far * z_near) / (z_near - z_far);
		out.data[15] = 0;
		return out;
	}
	Mat<4, T> transpose() const {
		Mat<4, T> out;
		out.data[0] = data[0]; out.data[1] = data[4]; out.data[2] = data[8]; out.data[3] = data[12];
		out.data[4] = data[1]; out.data[5] = data[5]; out.data[6] = data[9]; out.data[7] = data[13];
		out.data[8] = data[2]; out.data[9] = data[6]; out.data[10] = data[10]; out.data[11] = data[14];
		out.data[12] = data[3]; out.data[13] = data[7]; out.data[14] = data[11]; out.data[15] = data[15];
		return out;
	}
	Mat<4, T> inverse() const {
		Mat<4, T> out;
		out.data[0] = data[5] * data[10] * data[15] - data[5] * data[11] * data[14] - data[9] * data[6] * data[15] + data[9] * data[7] * data[14] + data[13] * data[6] * data[11] - data[13] * data[7] * data[10];
		out.data[4] = -data[4] * data[10] * data[15] + data[4] * data[11] * data[14] + data[8] * data[6] * data[15] - data[8] * data[7] * data[14] - data[12] * data[6] * data[11] + data[12] * data[7] * data[10];
		out.data[8] = data[4] * data[9] * data[15] - data[4] * data[11] * data[13] - data[8] * data[5] * data[15] + data[8] * data[7] * data[13] + data[12] * data[5] * data[11] - data[12] * data[7] * data[9];
		out.data[12] = -data[4] * data[9] * data[14] + data[4] * data[10] * data[13] + data[8] * data[5] * data[14] - data[8] * data[6] * data[13] - data[12] * data[5] * data[10] + data[12] * data[6] * data[9];
		out.data[1] = -data[1] * data[10] * data[15] + data[1] * data[11] * data[14] + data[9] * data[2] * data[15] - data[9] * data[3] * data[14] - data[13] * data[2] * data[11] + data[13] * data[3] * data[10];
		out.data[5] = data[0] * data[10] * data[15] - data[0] * data[11] * data[14] - data[8] * data[2] * data[15] + data[8] * data[3] * data[14] + data[12] * data[2] * data[11] - data[12] * data[3] * data[10];
		out.data[9] = -data[0] * data[9] * data[15] + data[0] * data[11] * data[13] + data[8] * data[1] * data[15] - data[8] * data[3] * data[13] - data[12] * data[1] * data[11] + data[12] * data[3] * data[9];
		out.data[13] = data[0] * data[9] * data[14] - data[0] * data[10] * data[13] - data[8] * data[1] * data[14] + data[8] * data[2] * data[13] + data[12] * data[1] * data[10] - data[12] * data[2] * data[9];
		out.data[2] = data[1] * data[6] * data[15] - data[1] * data[7] * data[14] - data[5] * data[2] * data[15] + data[5] * data[3] * data[14] + data[13] * data[2] * data[7] - data[13] * data[3] * data[6];
		out.data[6] = -data[0] * data[6] * data[15] + data[0] * data[7] * data[14] + data[4] * data[2] * data[15] - data[4] * data[3] * data[14] - data[12] * data[2] * data[7] + data[12] * data[3] * data[6];
		out.data[10] = data[0] * data[5] * data[15] - data[0] * data[7] * data[13] - data[4] * data[1] * data[15] + data[4] * data[3] * data[13] + data[12] * data[1] * data[7] - data[12] * data[3] * data[5];
		out.data[14] = -data[0] * data[5] * data[14] + data[0] * data[6] * data[13] + data[4] * data[1] * data[14] - data[4] * data[2] * data[13] - data[12] * data[1] * data[6] + data[12] * data[2] * data[5];
		out.data[3] = -data[1] * data[6] * data[11] + data[1] * data[7] * data[10] + data[5] * data[2] * data[11] - data[5] * data[3] * data[10] - data[9] * data[2] * data[7] + data[9] * data[3] * data[6];
		out.data[7] = data[0] * data[6] * data[11] - data[0] * data[7] * data[10] - data[4] * data[2] * data[11] + data[4] * data[3] * data[10] + data[8] * data[2] * data[7] - data[8] * data[3] * data[6];
		out.data[11] = -data[0] * data[5] * data[11] + data[0] * data[7] * data[9] + data[4] * data[1] * data[11] - data[4] * data[3] * data[9] - data[8] * data[1] * data[7] + data[8] * data[3] * data[5];
		out.data[15] = data[0] * data[5] * data[10] - data[0] * data[6] * data[9] - data[4] * data[1] * data[10] + data[4] * data[2] * data[9] + data[8] * data[1] * data[6] - data[8] * data[2] * data[5];
		T inv_det = 1.0f / (data[0] * out.data[0] + data[1] * out.data[4] + data[2] * out.data[8] + data[3] * out.data[12]);
		for (size_t i = 0; i < 16; i++) {
			out.data[i] *= inv_det;
		}
		return out;
	}
	static Mat<4, T> look_at(const gen::vec3f& eye, const gen::vec3f& center, const gen::vec3f& up) {
		Mat<4, T> out;
		gen::vec3f x, y, z;
		z = gen::normalize(eye - center);
		x = gen::cross(up, z);
		y = gen::cross(z, x);
		x = gen::normalize(x);
		y = gen::normalize(y);

		out.data[0] = x.x;
		out.data[4] = x.y;
		out.data[8] = x.z;
		out.data[12] = -gen::dot(x, eye);

		out.data[1] = y.x;
		out.data[5] = y.y;
		out.data[9] = y.z;
		out.data[13] = -gen::dot(y, eye);

		out.data[2] = z.x;
		out.data[6] = z.y;
		out.data[10] = z.z;
		out.data[14] = -gen::dot(z, eye);

		out.data[3] = 0.0f;
		out.data[7] = 0.0f;
		out.data[11] = 0.0f;
		out.data[15] = 1.0f;
		return out;
	}
	static Mat<4, T> ortho(float min_x, float max_x, float min_y, float max_y, float min_z, float max_z) {
		Mat<4, T> out;
		out.data[0] = 2 / (max_x - min_x);
		out.data[3] = -1 * ((max_x + min_x) / (max_x - min_x));

		out.data[5] = 2 / (max_y - min_y);
		out.data[7] = -1 * ((max_y + min_y) / (max_y - min_y));

		out.data[10] = -2 / (max_z - min_z);
		out.data[11] = -1 * ((max_z + min_z) / (max_z - min_z));

		return out;
	}
};

template<typename T>
struct Mat<3, T> {
	T data[9];

	Mat() {
		memset(data, 0, sizeof(T) * 9);
		data[0] = 1;
		data[4] = 1;
		data[8] = 1;
	}
	Mat(const Mat<4, T>& m) {
		data[0] = m.data[0];
		data[1] = m.data[1];
		data[2] = m.data[2];
		data[3] = m.data[4];
		data[4] = m.data[5];
		data[5] = m.data[6];
		data[6] = m.data[8];
		data[7] = m.data[9];
		data[8] = m.data[10];
	}

	Mat<3, T> operator*(const Mat<3, T>& other) const {
		Mat<3, T> out;
		T* A = (T*)this;
		T* B = (T*)&other;
		T* C = (T*)&out;
		C[0] = A[0] * B[0] + A[3] * B[1] + A[6] * B[2];
		C[1] = A[1] * B[0] + A[4] * B[1] + A[7] * B[2];
		C[2] = A[2] * B[0] + A[5] * B[1] + A[8] * B[2];
		C[3] = A[0] * B[3] + A[3] * B[4] + A[6] * B[5];
		C[4] = A[1] * B[3] + A[4] * B[4] + A[7] * B[5];
		C[5] = A[2] * B[3] + A[5] * B[4] + A[8] * B[5];
		C[6] = A[0] * B[6] + A[3] * B[7] + A[6] * B[8];
		C[7] = A[1] * B[6] + A[4] * B[7] + A[7] * B[8];
		C[8] = A[2] * B[6] + A[5] * B[7] + A[8] * B[8];
		return out;
	}
	Vec<3, T> operator*(const Vec<3, T>& other) const {
		Vec<3, T> out;
		T* A = (T*)this;
		T* B = (T*)&other;
		T* C = (T*)&out;
		C[0] = A[0] * B[0] + A[3] * B[1] + A[6] * B[2];
		C[1] = A[1] * B[0] + A[4] * B[1] + A[7] * B[2];
		C[2] = A[2] * B[0] + A[5] * B[1] + A[8] * B[2];
		return out;
	}
	static Mat<3, T> scale(const Vec<2, T>& scale) {
		return Mat<3, T>::scale(scale.x, scale.y);
	}
	static Mat<3, T> scale(T x, T y) {
		Mat<3, T> out;
		out.data[0] = x;
		out.data[4] = y;
		return out;
	}
	static Mat<3, T> translate(const Vec<2, T>& pos) {
		return Mat<3, T>::translate(pos.x, pos.y);
	}
	static Mat<3, T> translate(T x, T y) {
		Mat<3, T> out;
		out.data[6] = x;
		out.data[7] = y;
		return out;
	}
	static Mat<3, T> rotate(T angle) {
		Mat<3, T> out;
		T angle_sin = gen::sin(angle);
		T angle_cos = gen::cos(angle);
		out.data[0] = angle_cos;
		out.data[1] = -angle_sin;
		out.data[3] = angle_sin;
		out.data[4] = angle_cos;
		return out;
	}
	static Mat<3, T> rotate_origin(T angle, const Vec<2, T>& origin) {
		Mat<3, T> out;
		T angle_sin = gen::sin(angle);
		T angle_cos = gen::cos(angle);
		out.data[0] = angle_cos;
		out.data[1] = -angle_sin;
		out.data[3] = angle_sin;
		out.data[4] = angle_cos;
		return translate(origin * Vec<2, T>(-1, -1)) * out * translate(origin);
	}
	Mat<3, T> transpose() const {
		Mat<3, T> out;
		out.data[0] = data[0];
		out.data[1] = data[3];
		out.data[2] = data[6];
		out.data[3] = data[1];
		out.data[4] = data[4];
		out.data[5] = data[7];
		out.data[6] = data[2];
		out.data[7] = data[5];
		out.data[8] = data[8];
		return out;
	}
	Mat<3, T> inverse() const {
		Mat<3, T> out;
		T det = data[0] * (data[4] * data[8] - data[7] * data[5]) -
			data[1] * (data[3] * data[8] - data[5] * data[6]) +
			data[2] * (data[3] * data[7] - data[4] * data[6]);
		T invdet = 1 / det;
		out.data[0] = (data[4] * data[8] - data[7] * data[5]) * invdet;
		out.data[1] = (data[2] * data[7] - data[1] * data[8]) * invdet;
		out.data[2] = (data[1] * data[5] - data[2] * data[4]) * invdet;
		out.data[3] = (data[5] * data[6] - data[3] * data[8]) * invdet;
		out.data[4] = (data[0] * data[8] - data[2] * data[6]) * invdet;
		out.data[5] = (data[3] * data[2] - data[0] * data[5]) * invdet;
		out.data[6] = (data[3] * data[7] - data[6] * data[4]) * invdet;
		out.data[7] = (data[6] * data[1] - data[0] * data[7]) * invdet;
		out.data[8] = (data[0] * data[4] - data[3] * data[1]) * invdet;
		return out;
	}
};
#pragma pack(pop)
} // namespace gen
