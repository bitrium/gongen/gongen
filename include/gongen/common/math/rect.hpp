// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "vector.hpp"

namespace gen {
template<size_t SIZE, typename T>
struct Rect;

template<typename T>
using rect2 = Rect<2, T>;
using rect2d = rect2<double>;
using rect2f = rect2<float>;
using rect2u = rect2<uint32_t>;
using rect2i = rect2<int32_t>;
using rect2z = rect2<size_t>;

template<typename T>
using rect3 = Rect<3, T>;
using rect3d = rect3<double>;
using rect3f = rect3<float>;
using rect3u = rect3<uint32_t>;
using rect3i = rect3<int32_t>;
using rect3z = rect3<size_t>;

#pragma pack(push, 1)
template<typename T>
struct Rect<2, T> {
	// relative rect should be used with pos and size
	// absolute rect should be used with pos1 and pos2
	union {
		vec2<T> pos;
		vec2<T> pos1;
		struct {
			union {
				T x;
				T x1;
			};
			union {
				T y;
				T y1;
			};
		};
	};
	union {
		vec2<T> size;
		vec2<T> pos2;
		struct {
			union {
				T w;
				T x2;
			};
			union {
				T h;
				T y2;
			};
		};
	};

	Rect() {}
	Rect(vec2<T> pos, vec2<T> size) : pos(pos), size(size) {}
	Rect(T x, T y, T w, T h) : pos(x, y), size(w, h) {}
	template<typename U>
	Rect(const Rect<2, U>& other) : pos(vec2<T>(other.pos)), size(vec2<T>(other.size)) {}

	Rect<2, T>& to_relatively_sized() {
		this->pos2 = this->pos2 - this->pos1;
	};
	Rect<2, T>& to_absolutely_sized() {
		this->pos2 = this->pos + this->size;
	};
	Rect<2, T> relatively_sized() const {
		Rect<2, T> ret = *this;
		ret.to_relatively_sized();
		return ret;
	};
	Rect<2, T> absolutely_sized() const {
		Rect<2, T> ret = *this;
		ret.to_absolutely_sized();
		return ret;
	};

	Rect<2, T>& operator=(const Rect<2, T>& other) {
		this->pos = other.pos;
		this->size = other.size;
		return *this;
	}

	bool operator==(const Rect<2, T>& other) {
		return this->pos == other.pos && this->size == other.size;
	}
	bool operator!=(const Rect<2, T>& other) {
		return this->pos != other.pos || this->size != other.size;
	}

	T area() {
		return (size.x * size.y);
	}

	bool contains(const Vec<2, T>& vec) {
		return vec > this->pos && vec < this->size;
	}
};

template<typename T>
struct Rect<3, T> {
	union {
		vec3<T> pos;
		vec3<T> pos1;
		struct {
			union {
				T x;
				T x1;
			};
			union {
				T y;
				T y1;
			};
			union {
				T z;
				T z1;
			};
		};
	};
	union {
		vec3<T> size;
		vec3<T> pos2;
		struct {
			union {
				T w;
				T x2;
			};
			union {
				T h;
				T y2;
			};
			union {
				T d;
				T z2;
			};
		};
	};

	Rect() {}
	Rect(vec3<T> pos, vec3<T> size) : pos(pos), size(size) {}
	Rect(T position_x, T position_y, T position_z, T size_x, T size_y, T size_z) : pos(position_x, position_y, position_z), size(size_x, size_y, size_z) {}
	template<typename U>
	Rect(const Rect<3, U>& other) : pos(vec3<T>(other.pos)), size(vec3<T>(other.size)) {}

	Rect<3, T>& to_relatively_sized() {
		this->pos2 = this->pos2 - this->pos1;
	};
	Rect<3, T>& to_absolutely_sized() {
		this->pos2 = this->pos + this->size;
	};
	Rect<3, T> relatively_sized() const {
		Rect<3, T> ret = *this;
		ret.to_relatively_sized();
		return ret;
	};
	Rect<3, T> absolutely_sized() const {
		Rect<3, T> ret = *this;
		ret.to_absolutely_sized();
		return ret;
	};

	Rect<3, T>& operator=(const Rect<3, T>& other) {
		this->pos = other.pos;
		this->size = other.size;
		return *this;
	}

	bool operator==(const Rect<3, T>& other) {
		return this->pos == other.pos && this->size == other.size;
	}

	bool operator!=(const Rect<3, T>& other) {
		return this->pos != other.pos || this->size != other.size;
	}

	T area() {
		return (2 * (size.x * size.y)) + (2 * (size.x * size.z)) + (2 * (size.y * size.z));
	}

	T volume() {
		return (size.x * size.y * size.z);
	}

	bool operator==(const Rect<3, T>& other) const {
		return this->pos == other.pos && this->size == other.size;
	}
};
#pragma pack(pop)
} // namespace gen
