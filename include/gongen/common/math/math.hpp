// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "../defines.hpp"
#include <stddef.h>
#include <stdint.h>
#include <tgmath.h>
#include <math.h>

#ifndef M_PI
	#define M_PI 3.14159265358979323846
#endif
#define M_TAU M_PI * 2
#define RAD_TO_DEG_MULTIPLIER (180.0/M_PI)
#define DEG_TO_RAD_MULTIPLIER (M_PI/180.0)

namespace gen {
template<typename T>
void swap(T& a, T& b) { T c = a; b = a; a = c; }

template<typename T>
T sqrt(T value) { return (T)::sqrtl((long double)value); }
template<> inline float sqrt<float>(float value) { return ::sqrtf(value); }
template<> inline double sqrt<double>(double value) { return ::sqrt(value); }
template<> inline long double sqrt<long double>(long double value) { return ::sqrtl(value); }

template<typename T>
T pow(T value, T power) { return (T)::powl((long double)value, (long double)power); }
template<> inline float pow<float>(float value, float power) { return ::powf(value, power); }
template<> inline double pow<double>(double value, double power) { return ::pow(value, power); }
template<> inline long double pow<long double>(long double value, long double power) { return ::powl(value, power); }

template<typename T>
T sin(T value) { return (T)::sinl((long double)value); }
template<> inline float sin<float>(float value) { return ::sinf(value); }
template<> inline double sin<double>(double value) { return ::sin(value); }
template<> inline long double sin<long double>(long double value) { return ::sinl(value); }

template<typename T>
T cos(T value) { return (T)::cosl((long double)value); }
template<> inline float cos<float>(float value) { return ::cosf(value); }
template<> inline double cos<double>(double value) { return ::cos(value); }
template<> inline long double cos<long double>(long double value) { return ::cosl(value); }

template<typename T>
T tan(T value) { return (T)::tanl((long double)value); }
template<> inline float tan<float>(float value) { return ::tanf(value); }
template<> inline double tan<double>(double value) { return ::tan(value); }
template<> inline long double tan<long double>(long double value) { return ::tanl(value); }

template<typename T>
T asin(T value) { return (T)::asinl((long double)value); }
template<> inline float asin<float>(float value) { return ::asinf(value); }
template<> inline double asin<double>(double value) { return ::asin(value); }
template<> inline long double asin<long double>(long double value) { return ::asinl(value); }

template<typename T>
T acos(T value) { return (T)::acosl((long double)value); }
template<> inline float acos<float>(float value) { return ::acosf(value); }
template<> inline double acos<double>(double value) { return ::acos(value); }
template<> inline long double acos<long double>(long double value) { return ::acosl(value); }

template<typename T>
T atan(T value) { return (T)::atanl((long double)value); }
template<> inline float atan<float>(float value) { return ::atanf(value); }
template<> inline double atan<double>(double value) { return ::atan(value); }
template<> inline long double atan<long double>(long double value) { return ::atanl(value); }

template<typename T>
T atan2(T x, T y) { return (T)::atan2l((long double)y, (long double)x); }
template<> inline float atan2<float>(float x, float y) { return ::atan2f(y, x); }
template<> inline double atan2<double>(double x, double y) { return ::atan2(y, x); }
template<> inline long double atan2<long double>(long double x, long double y) { return ::atan2l(y, x); }

template<typename T>
T exp(T value) { return (T)::expl((long double)value); }
template<> inline float exp<float>(float value) { return ::expf(value); }
template<> inline double exp<double>(double value) { return ::exp(value); }
template<> inline long double exp<long double>(long double value) { return ::expl(value); }

template<typename T>
T logn(T value) { return (T)::logl((long double)value); }
template<> inline float logn<float>(float value) { return ::logf(value); }
template<> inline double logn<double>(double value) { return ::log(value); }
template<> inline long double logn<long double>(long double value) { return ::logl(value); }

template<typename T>
T log10(T value) { return (T)::log10l((long double)value); }
template<> inline float log10<float>(float value) { return ::log10f(value); }
template<> inline double log10<double>(double value) { return ::log10(value); }
template<> inline long double log10<long double>(long double value) { return ::log10l(value); }

template<typename T>
T ceil(T value) { return (T)::ceill((long double)value); }
template<> inline float ceil<float>(float value) { return ::ceilf(value); }
template<> inline double ceil<double>(double value) { return ::ceil(value); }
template<> inline long double ceil<long double>(long double value) { return ::ceill(value); }

template<typename T>
T floor(T value) { return (T)::floorl((long double)value); }
template<> inline float floor<float>(float value) { return ::floorf(value); }
template<> inline double floor<double>(double value) { return ::floor(value); }
template<> inline long double floor<long double>(long double value) { return ::floorl(value); }

template<typename T>
T round(T value) { return (T)::roundl((long double)value); }
template<> inline float round<float>(float value) { return ::roundf(value); }
template<> inline double round<double>(double value) { return ::round(value); }
template<> inline long double round<long double>(long double value) { return ::roundl(value); }

template<typename T>
T abs(T value) { return (T)::fabsl((long double)value); }
template<> inline int8_t abs<int8_t>(int8_t value) { return ::labs(value); }
template<> inline int16_t abs<int16_t>(int16_t value) { return ::labs(value); }
template<> inline int32_t abs<int32_t>(int32_t value) { return ::labs(value); }
template<> inline int64_t abs<int64_t>(int64_t value) { return ::llabs(value); }
template<> inline float abs<float>(float value) { return ::fabsf(value); }
template<> inline double abs<double>(double value) { return ::fabs(value); }
template<> inline long double abs<long double>(long double value) { return ::fabsl(value); }

template<typename T>
T clamp_val(T value, T min = T(0), T max = T(1)) {
	if (value < min) return min;
	if (value > max) return max;
	return value;
}
template<typename T>
void clamp(T& value, T min = T(0), T max = T(1)) {
	if (value < min) value = min;
	if (value > max) value = max;
}

template<typename T>
T deg(T value) { return (value * RAD_TO_DEG_MULTIPLIER); }

template<typename T>
T rad(T value) { return (value * DEG_TO_RAD_MULTIPLIER); }

template<typename T>
T lerp(T start, T end, float t) {
	return (1 - t) * start + t * end;
}

template<typename T>
T sign(T val) {
	return (0 < val) - (val < 0);
}
} // namespace gen
