// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "containers/values.hpp"
#include "containers/string.hpp"

namespace gen {

class BasicPath;
class UniversalPath;

class GEN_API AbstractPath {
protected:
	bool is_abs;
	DynamicArray<String> segments;
	String str;

	virtual void update_string() = 0;
	virtual AbstractPath* copy() const = 0;

	friend class UniversalPath;
public:
	// call update_string on subclass construction!
	explicit AbstractPath(bool root = false);
	explicit AbstractPath(const String& str);
	explicit AbstractPath(const char* c_str);
	AbstractPath(const AbstractPath& copied);
	virtual ~AbstractPath();

	UniversalPath as_universal();

	virtual bool is_file_path() const { return false; }

	String string() const;
	const char* c_str() const;

	const DynamicArray<String>& get_segments() const;

	String name() const;
	String extension() const;
	String suffix() const;
	String stem() const;

	virtual bool exists() const {
		internal::no_impl_error("This path does not implement 'exists'.");
		return false;
	}
	virtual bool readable() const {
		internal::no_impl_error("This path does not implement 'readable'.");
		return false;
	}
	virtual bool writeable() const {
		internal::no_impl_error("This path does not implement 'writeable.");
		return false;
	}
	virtual bool is_directory() const {
		internal::no_impl_error("This path does not implement 'is_directory'.");
		return false;
	}

	bool is_absolute() const;
	bool is_empty() const;

	void clear();

	AbstractPath& to_simplified();
	template<typename T = BasicPath>
	T simplify() const {
		T path = T(*this);
		path.to_simplified();
		return path;
	}

	bool is_child(const AbstractPath& parent) const;
	bool is_descendant(const AbstractPath& ancestor) const;

	AbstractPath& to_absolute();
	AbstractPath& to_relative(); // TODO custom origin
	// changes to absolute by prefixing with current directory
	virtual AbstractPath& to_resolved() {
		internal::no_impl_error("This path does not implement resolving.");
		throw nullptr;
	}
	template<typename T = BasicPath>
	T absolute() const {
		T path = T(*this);
		path.to_absolute();
		return path;
	}
	template<typename T = BasicPath>
	T relative() const {
		T path = T(*this);
		path.to_relative();
		return path;
	}
	// gets absolute by adding all ancestors
	template<typename T>
	T resolve() const {
		T path = T(*this);
		path.to_resolved();
		return path;
	}

	AbstractPath& to_parent(size_t ancestor = 1);
	template<typename T = BasicPath>
	T parent(size_t ancestor = 1) const {
		T path = T(*this);
		path.to_parent(ancestor);
		return path;
	}

	AbstractPath& to_child(const String& name);
	AbstractPath& to_child(const AbstractPath& relative);
	template<typename T = BasicPath>
	T child(const String& name) const {
		T path = T(*this);
		path.to_child(name);
		return path;
	}
	template<typename T = BasicPath>
	T child(const AbstractPath& relative) const {
		T path = T(*this);
		path.to_child(relative);
		return path;
	}

	virtual DynamicArray<String> enumerate_names() const {
		internal::no_impl_error("This path does not implement enumeration.");
		return DynamicArray<String>();
	}
	// keeps paths relative to dir
	template<typename T>
	DynamicArray<T> enumerate() const {
		DynamicArray<String> names = this->enumerate_names();
		DynamicArray<T> paths(names.size());
		for (String& name : names) {
			paths.add(T(*this) / name);
		}
		return paths;
	}
	// gets absolute paths
	template<typename T>
	DynamicArray<T> enumerate_absolute() const {
		DynamicArray<String> names = this->enumerate_names();
		DynamicArray<AbstractPath> paths(names.size());
		T abs = T(*this);
		abs.to_absolute();
		for (String& name : names) {
			paths.add(abs / name);
		}
		return paths;
	}
	// uses dir as origin
	template<typename T>
	DynamicArray<T> enumerate_relative() const {
		DynamicArray<String> names = this->enumerate_names();
		DynamicArray<T> paths(names.size());
		for (String& name : names) {
			T elem = T(*this);
			elem.clear();
			elem.to_child(name);
			paths.add(std::move(elem));
		}
		return paths;
	}
	template<typename T>
	DynamicArray<T> enumerate_recursive() const {
		DynamicArray<T> children = this->enumerate<T>();
		DynamicArray<T> results;
		for (T& child : children) {
			if (child.is_directory()) {
				for (T& further : child.template enumerate_recursive<T>()) {
					results.add(further);
				}
			} else {
				results.add(child);
			}
		}
		return results;
	}

	AbstractPath& operator/=(const String& child);
	AbstractPath& operator/=(const AbstractPath& relative);
	template<typename T = BasicPath>
	T operator/(const String& child) const {
		T path = T(*this);
		path /= child;
		return path;
	}
	template<typename T = BasicPath>
	T operator/(const T& relative) const {
		T path = T(*this);
		path /= relative;
		return path;
	}

	gen::PtrIterator<gen::String> begin() const {
		return segments.begin();
	}
	gen::PtrIterator<gen::String> end() const {
		return segments.end();
	}
};

class GEN_API BasicPath : public AbstractPath {
protected:
	void update_string() override;
	AbstractPath* copy() const override {
		return new BasicPath(*this);
	}
public:
	explicit BasicPath(bool root = false) : AbstractPath(root) { update_string(); }
	explicit BasicPath(const String& str) : AbstractPath(str) { update_string(); }
	explicit BasicPath(const char* c_str) : AbstractPath(c_str) { update_string(); }
	explicit BasicPath(const AbstractPath& copied) : AbstractPath(copied) { update_string(); }
};
typedef BasicPath Path;

class GEN_API UniversalPath : public AbstractPath {
protected:
	AbstractPath* base;

	AbstractPath* copy() const override {
		return new UniversalPath(*this);
	}
	void update_string() override {
		this->base->is_abs = this->is_abs;
		this->base->segments = this->segments;
		this->base->update_string();
		this->str = this->base->str;
	}
public:
	explicit UniversalPath(const AbstractPath& base) {
		this->base = base.copy();
		this->is_abs = this->base->is_abs;
		this->segments = this->base->segments;
		this->str = this->base->str;
	}
	UniversalPath(const UniversalPath& copied) {
		this->base = copied.base->copy();
		this->is_abs = this->base->is_abs;
		this->segments = this->base->segments;
		this->str = this->base->str;
	}
	~UniversalPath() {
		delete this->base;
	}

	bool is_file_path() const override {
		return this->base->is_file_path();
	}

	bool exists() const override {
		return this->base->exists();
	}
	bool readable() const override {
		return this->base->readable();
	}
	bool writeable() const override {
		return this->base->writeable();
	}
	bool is_directory() const override {
		return this->base->is_directory();
	}

	AbstractPath& to_resolved() override {
		return this->base->to_resolved();
	}

	DynamicArray<String> enumerate_names() const override {
		return this->base->enumerate_names();
	}

	UniversalPath operator/(const String& child) const {
		UniversalPath path = *this;
		path /= child;
		return path;
	}
	UniversalPath operator/(const AbstractPath& relative) const {
		UniversalPath path = *this;
		path /= relative;
		return path;
	}
};

} // namespace gen
