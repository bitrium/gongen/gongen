// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "containers.hpp"

namespace gen::event {

class System;
class Hopper;

static uint64_t no_events = 0;
static uint64_t all_events = UINT64_MAX;

GEN_API System* system();
GEN_API Hopper* hopper(uint64_t mask = all_events, void* source_filter = nullptr);
GEN_API uint64_t type(const char* name);

class GEN_API Event {
public:
	struct Reference {
		std::atomic<int32_t> ref_counter;
		Event* event;
	};

	const uint64_t type;
	void* source;

	// type_name must be a string literal
	Event(const char* type_name, void* source = nullptr);
	virtual ~Event();
};

class GEN_API Hopper {
	Hopper() = delete;
	Hopper(const Hopper&) = delete;
private:
	const uint64_t mask;
	void* source;
	TypedLocklessQueue<Event::Reference> awaiting;

	Hopper(uint64_t mask, void* source_filter);

	void occur(Event::Reference* event_ref);

	friend class System;
	friend class Buffer<Hopper*>;

	Buffer<Event::Reference*> events_refs;
public:
	Buffer<Event*> events;

	~Hopper();
	void poll();
	void poll_wait();
	void clear();
};

class GEN_API System {
	System(const System&) = delete;
private:
	std::atomic_bool lock;
	Buffer<Hopper*> hoppers;
	TypedLocklessQueue<Event::Reference> awaiting;

	void occur_internal(Event::Reference* event_ref);

	friend GEN_API System* system();
	friend class Hopper;
public:
	System();
	~System();

	void occur(Event* event);

	template<typename T, typename... Args>
	void occur(Args&&... args) {
		this->occur(new T(std::forward<Args>(args)...));
	}
	Hopper* hopper(uint64_t sieve, void* source_filter = nullptr);
};

} // namespace gen::event
