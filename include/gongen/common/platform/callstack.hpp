// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "../containers/string.hpp"

namespace gen {
// Getting callstack and callstack symbols
namespace callstack {
	const static size_t MAX_SYMBOL_NAME_LENGTH = 256;
	struct Symbol {
		gen::String module_name;
		gen::String name;
		size_t line = 0;
		size_t base_addr = 0;
		gen::String filepath;
	};
	class GEN_API Callstack {
		void* impl;
	public:
		size_t count = 0;
		Callstack();

		Callstack(size_t max_count, size_t skip = 0);
		Callstack(const Callstack& other) = delete;
		Callstack(Callstack&& other) noexcept {
			impl = other.impl;
			other.impl = nullptr;
		}
		gen::callstack::Callstack& fill(size_t max_count, size_t skip = 0);
		~Callstack();
		//THIS FUNCTION IS NOT THREAD-SAFE!
		Symbol symbol(size_t index) const;
		void* addr(size_t index) const;
		Callstack& operator=(const Callstack& other);

		String to_string(bool code_info, size_t main_fc_addr) const;
	};
} // namespace callstack
typedef callstack::Callstack Callstack;
} // namespace gen
