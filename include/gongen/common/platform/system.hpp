// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "../containers/dynamic_array.hpp"
#include "../containers/string.hpp"
#include "../error.hpp"
#include "../data_source.hpp"
#include "../path.hpp"

typedef struct sockaddr c_net_addr;

namespace gen {

// Thread management
namespace thread {
class GEN_API ThreadId {
public:
	void* impl;

	ThreadId(void* impl) { this->impl = impl; }
	~ThreadId();
	size_t get_number();
	bool operator==(const ThreadId& id) const;
	bool operator!=(const ThreadId& id) const;
};

GEN_API void sleep(uint64_t ms);
GEN_API ThreadId id();
GEN_API String name();

class GEN_API WaitObject {
private:
	void* impl;
public:
	WaitObject(bool auto_reset = false);
	~WaitObject();
	void wait();
	void signal();
	void reset();
	bool state();
};

const static size_t MAX_NAME_LENGTH = 15;

class GEN_API Thread {
	void* impl;
public:
	Thread() : impl(nullptr) {} // "uninitialized" thread
	explicit Thread(void* (*proc)(void*), void* arg, const char* name = nullptr);
	Thread(const Thread& other) {
		this->operator=(other);
	}
	Thread(Thread&& other) noexcept {
		this->impl = other.impl;
		other.impl = nullptr;
	}
	// will not join nor kill the thread
	~Thread();
	Thread& operator=(const Thread& other);

	ThreadId id() const;
	bool done() const;

	void* join();
	void kill();
	Thread& rethrow_exception();
};

} // namespace thread
typedef thread::Thread Thread;


// File I/O
namespace file {
GEN_EXCEPTION_TYPE(Exception, RuntimeException, "File Error");
GEN_EXCEPTION_TYPE(OpenException, Exception, "Can't open File");
GEN_EXCEPTION_TYPE(MapException, Exception, "Can't map File");

class GEN_API Path : public AbstractPath {
	void update_string() override;
	AbstractPath* copy() const override {
		return new Path(*this);
	}
public:
	explicit Path(bool root = false) : AbstractPath(root) { this->update_string(); }
	explicit Path(const String& str) : AbstractPath(str) { this->update_string(); }
	explicit Path(const char* c_str) : AbstractPath(c_str) { this->update_string(); }
	explicit Path(const AbstractPath& copied) : AbstractPath(copied) { this->update_string(); }
	~Path() = default;

	bool is_file_path() const override { return true; }

	bool exists() const override;
	bool readable() const override;
	bool writeable() const override;
	bool is_directory() const override;

	Path& to_resolved() override;

	DynamicArray<String> enumerate_names() const override;

	Path operator/(const String& child) const {
		return this->AbstractPath::operator/<Path>(child);
	}
	Path operator/(const Path& relative) const {
		return this->AbstractPath::operator/<Path>(relative);
	}
};


GEN_API void make_directory(const AbstractPath& path, bool recursive = true);
GEN_API void make_soft_link(const AbstractPath& src, const AbstractPath& dst);
GEN_API void make_hard_link(const AbstractPath& src, const AbstractPath& dst);

GEN_API void move(const AbstractPath& src, const AbstractPath& dst);
GEN_API void remove(const AbstractPath& path);

enum class StandardDir : uint8_t {
	HOME,	// home directory, $HOME
	CONFIG,	// configuration files
	DATA,	// persistent data files
	CACHE,	// unimportant, cache data files
	STATE,	// state, dynamic data files (logs, history, temp settings, ...)
};

enum class StandardUserDir : uint8_t {
	DESKTOP,
	DOCUMENTS,
	DOWNLOADS,
	PICTURES,
	MUSIC,
	VIDEOS,
};

GEN_API Path get_working_dir();

// a directory will be maked at the resulting directory (suffix must not be a file)
GEN_API Path get_pref_dir(StandardDir dir = StandardDir::HOME,
                  const AbstractPath& suffix = Path(), bool user_specific = true);

// a directory will be maked at the resulting directory (suffix must not be a file)
GEN_API Path get_user_dir(StandardUserDir dir = StandardUserDir::DOCUMENTS,
                  const AbstractPath& suffix = Path());


enum class Mode {
	READ,
	WRITE,
	APPEND,
	READ_WRITE,
};

enum class MapMode {
	READ,
	READ_WRITE,
};

class GEN_API File :public SeekableWritableDataSource {
	void* impl;
public:
	File();
	File(const File& other) {
		this->operator=(other);
	}
	File(const AbstractPath& path, Mode open_mode = Mode::READ);
	inline File(const char* path, Mode open_mode = Mode::READ) : File(gen::file::Path(path), open_mode) {}
	~File();
	File& operator=(const File& other); // takes file from other

	bool is_open();
	void close();

	int64_t time_created();
	int64_t time_modified();
	int64_t time_accessed();

	bool set_size(size_t size);

	size_t size();
	size_t cursor();
	size_t seek(ptrdiff_t offset, Seek type = Seek::SET);

	size_t read(void* data, size_t size);
	size_t write(const void* data, size_t size);
	void flush();
};

gen::String read_txt(const Path& path);
gen::String read_txt(const char* path);

} // namespace file
typedef file::File File;


// General Internet Protocol items
namespace ip {

struct Address {
	bool v6 = false;
	union {
		uint32_t v4;
		uint16_t v6[8];
	} addr;

	Address() { addr.v4 = 0; }

	String to_string() const;
};

struct GEN_API Endpoint {
	Address address;
	uint16_t port;

	Endpoint() { zero(); }
	explicit Endpoint(const c_net_addr* net) { from_network(net); }

	void zero() { memset(&address.addr.v6, 0, sizeof(uint16_t) * 8); port = 0; }
	bool is_filled() const { return address.addr.v4 != 0 || port != 0; }

	String to_string() const;

	// to c std addr with network byte order
	unsigned int to_network(c_net_addr* out) const;
	// from c std addr with network byte order
	void from_network(const c_net_addr* net);
};

GEN_API Optional<Endpoint> resolve(const String& endpoint);
GEN_API Optional<Endpoint> resolve(const String& hostname, Optional<String> port);
GEN_API DynamicArray<Endpoint> resolve_all(const String& endpoint, size_t max = 0);
GEN_API DynamicArray<Endpoint> resolve_all(const String& hostname, Optional<String> port, size_t max = 0);

} // namespace ip


// TCP protocol support
namespace tcp {

// TODO SocketException?
class GEN_API Socket :public WritableDataSource {
	void* impl = nullptr;

	friend class DynamicArray<Socket>; // for emplacement
	explicit Socket(void* impl);
public:
	Socket();
	// if address is 0, all interfaces will be used
	// if port is 0, it will be chosen by the system
	Socket(const ip::Endpoint& endpoint, bool is_server);
	Socket(const String& hostname, bool is_server);
	Socket(const Socket& other) { throw NoImplException("Socket copying is not allowed."); }
	~Socket() override;
	Socket& operator=(Socket&& moved) noexcept { this->impl = moved.impl; moved.impl = nullptr; return *this; }
	bool operator==(const Socket& other) { return this->impl == other.impl; }

	bool is_open();
	void open(const ip::Endpoint& endpoint, bool is_server);
	void close();
	void reopen();

	Socket* accept(bool take_ownership = false); // FIXME return by value

	size_t size() override { return 0; }
	size_t read(void* data, size_t size) override;
	size_t write(const void* data, size_t size) override;
	void flush() override {}

	bool check(uint32_t timeout = 0);

	// server sockets – host endpoint; client/accepted sockets – remote endpoint
	ip::Endpoint endpoint();
	friend Buffer<size_t> check_sockets_ids(const Buffer<Socket*>& sockets, uint32_t timeout);
};

Buffer<Socket*> check_sockets(const Buffer<Socket*>& sockets, uint32_t timeout = 0);
Buffer<size_t> check_sockets_ids(const Buffer<Socket*>& sockets, uint32_t timeout = 0);

} // namesapce tcp


// Time functions
namespace time {

GEN_API int64_t get_unix_timestamp();
GEN_API uint64_t get_time_frequency();
struct GEN_API Time {
	int64_t t;

	Time();
	Time(int64_t t) : t(t) {
	}

	double sec() const {
		return (double)this->t / (double)get_time_frequency();
	}

	Time add(double sec) const {
		return Time(this->t + (double) sec * (double) get_time_frequency());
	}

	Time operator-(const Time& other) const {
		return Time(this->t - other.t);
	}
	Time operator+(const Time& other) const {
		return Time(this->t + other.t);
	}
	bool operator>(const Time& other) const {
		return this->t > other.t;
	}
	bool operator<(const Time& other) const {
		return this->t < other.t;
	}
};
GEN_API Time get_time();
GEN_API Time from_start();
} // namespace time


// Dynamic library loading
namespace dlib {

typedef void* Library;
GEN_API Library open(const AbstractPath& path);
GEN_API void close(Library lib);
GEN_API void* symbol(Library lib, const char* function_name);

} // namespace dlib


// Console support
namespace console {
	GEN_API void init();
	GEN_API bool is_present();
	GEN_API void write(const char* str);
	GEN_API void writef(const char* fmt, ...) GEN_PRINTF_CHECKING;
} // namespace wconsole

// Handling of Fatal Errors(exp: segfault)
namespace feh {
enum Mask {
	SEGFAULT = 1,
	DATATYPE_MISALIGNMENT = 2,
	ILLEGAL_INSTRUCTION = 4,
	INT_DIVIDE_BY_ZERO = 8,
	STACK_OVERFLOW = 16
};

struct handler_info {
	void* userdata = nullptr;
	Callstack callstack;
	Mask exception;
	thread::ThreadId thread_id;
};

enum HandlerResult {
	RETURN, //Return value from gen::feh::wrap or gen::wrap_main
	TERMINATE, //Terminate program with specified return value
	PASS //Don't handle error and pass it to default handler
};

struct handler_ret {
	HandlerResult result = HandlerResult::RETURN;
	int return_value;
};

// Warning: it sets handler for current thread
GEN_API void set_handler(handler_ret (*proc)(handler_info*), void* userdata, uint32_t mask = UINT32_MAX);
GEN_API int wrap(int(*function)(void*), void* userdata);
} // namespace feh

namespace os {
enum class Architecture {
	X86,
	X64,
	ARM,
	ARM64,
	UNKNOWN = 100
};

GEN_API gen::String system_name();
GEN_API size_t memory_amount(); //in megabytes
GEN_API Architecture cpu_type();
GEN_API size_t thread_count();
GEN_API int execute(const gen::String& command);
} // namespace os

#undef ERROR

//Message Box
namespace mb {
enum Type {
	INFO,
	WARNING,
	ERROR
};
enum Buttons {
	OK,
	YES_NO,
	OK_CANCEL
};

GEN_API uint32_t pop_up(const gen::String& title, const gen::String& message, Type type = Type::INFO, Buttons buttons = Buttons::OK);
}
} // namespace gen
#undef ERROR
