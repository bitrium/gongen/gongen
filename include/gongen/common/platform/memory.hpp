// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "../defines.hpp"
#include <cstddef>

namespace gen {

GEN_API void* alloc(size_t size);
GEN_API void* calloc(size_t size);
GEN_API void free(void* ptr);
GEN_API void* realloc(void* ptr, size_t size);
GEN_API void* crealloc(void* ptr, size_t old_size, size_t size);

template<typename T>
T* talloc(size_t count = 1) {
	return (T*)gen::alloc(sizeof(T) * count);
}
template<typename T>
T* tcalloc(size_t count = 1) {
	return (T*)gen::calloc(sizeof(T) * count);
}
template<typename T>
T* trealloc(T* ptr, size_t count) {
	return (T*)gen::realloc(ptr, sizeof(T) * count);
}
template<typename T>
T* tcrealloc(T* ptr, size_t old_count, size_t count) {
	return (T*)gen::crealloc(ptr, sizeof(T) * old_count, sizeof(T) * count);
}

} // namespace gen
