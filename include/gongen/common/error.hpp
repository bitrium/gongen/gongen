// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "containers/string.hpp"
#include "platform/callstack.hpp"
#include <stdio.h>
#include <utility>
#include <functional>
#include <type_traits>
#include <errno.h>

#define GEN_EXCEPTION_TYPE(CLASS_NAME, PARENT, NAME)				\
class CLASS_NAME : public PARENT {									\
protected:															\
	CLASS_NAME(const gen::String& name, const gen::String& message,	\
			const gen::Exception* cause, size_t depth,				\
			bool fill_callstack = true)								\
			: PARENT(name, message, cause, depth + 1,				\
			fill_callstack) {										\
	}																\
public:																\
	CLASS_NAME(const gen::String& message, const gen::Exception* cause)	\
		: PARENT(NAME, message, cause, 1) {							\
	}																\
	explicit CLASS_NAME(const gen::String& message)					\
		: PARENT(NAME, message, nullptr, 1) {						\
	}																\
	CLASS_NAME(const CLASS_NAME& other) :CLASS_NAME(other.name, 	\
		other.message, other.cause, 0, false) {						\
		this->callstack = other.callstack;							\
	}																\
	gen::Exception* copy() const override {							\
		return new CLASS_NAME(*this); }								\
	void rethrow() const override { throw *this; }					\
	static CLASS_NAME from_errno(const char* msg_fmt, ...) {		\
		va_list va;													\
		va_start(va, msg_fmt);										\
		gen::String string = gen::String::createvf(msg_fmt, va);	\
		va_end(va);													\
		string.appendf(": %s (%i).", strerror(errno), errno);		\
		errno = 0;													\
		return CLASS_NAME(string);									\
	}																\
	static CLASS_NAME createf(const char* msg_fmt, ...) GEN_PRINTF_CHECKING {    \
        va_list va;													\
        va_start(va, msg_fmt);										\
        gen::String string = gen::String::createvf(msg_fmt, va);	\
        va_end(va);													\
        return CLASS_NAME(NAME, string, nullptr, 1, true);			\
    }																\
}

namespace gen {

const static size_t EXCEPTION_MAX_CALLSTACK_DEPTH = 128;

class Exception : public std::exception {
private:
	const String print_message;
public:
	const String name;
	const String message;
	Exception* cause = nullptr;
	Callstack callstack;

	Exception(String name, String message,
			const Exception* cause, size_t depth, bool fill_callstack = true)
			: print_message(name + ": " + message), name(std::move(name)),
			message(std::move(message)),
			callstack() {
		if (cause) this->cause = cause->copy();
		else cause = nullptr;
		callstack.fill(EXCEPTION_MAX_CALLSTACK_DEPTH, depth + 1);
	}
	Exception(const Exception& other) = delete;
	virtual ~Exception() {}
	virtual gen::Exception* copy() const = 0;
	virtual void rethrow() const = 0;

	const char* what() const noexcept override {
		return this->print_message.c_str();
	}
	String to_string() const {
		if (!cause) return String::createf("%s: %s", name.c_str(), message.c_str());
		else return String::createf("%s: %s because\n%s", name.c_str(), message.c_str(), cause->to_string().c_str());
	}
};

template<typename F, typename T = F::result_type>
T no_except_wrap(F func) {
	try {
		return func();
	} catch (const Exception& exception) {
		return T();
	}
}
template<typename F, typename T = F::result_type>
T no_except_wrap(F func, const T& default_val) {
	T result;
	try {
		result = func();
	} catch (const Exception& exception) {
		result = default_val;
	}
	return result;
}
template<typename F, typename T = F::result_type>
T no_except_wrap(F func, F alt) {
	T result;
	try {
		result = func();
	} catch (const Exception& exception) {
		result = alt();
	}
	return result;
}
// if the call throws an exception, returns from default constructor
#define GEN_NO_EXCEPT(call) gen::no_except_wrap(std::function([&]() { return (call); }))
// if the call throws an exception, returns the given value
#define GEN_NO_EXCEPT_OR(call, default_val) gen::no_except_wrap([]() { return (call); }, default_val)
// if the call throws an exception, returns from alternative call
#define GEN_NO_EXCEPT_ELSE(call, alt) gen::no_except_wrap(std::function([&]() { return (call); }), std::function([&]() { return alt; }))

GEN_EXCEPTION_TYPE(StdBasedException, Exception, "C++ Standard Exception");

GEN_EXCEPTION_TYPE(NoImplException, Exception, "Unimplemented Function Error");
#define GEN_UNIMPLEMENTED \
	do {throw NoImplException("Unimplemented.");} while (0)

GEN_EXCEPTION_TYPE(StateException, Exception, "Illegal State Error");
GEN_EXCEPTION_TYPE(DepModuleException, Exception, "Missing Module Error");
GEN_EXCEPTION_TYPE(AssertException, StateException, "Assertion Error");

GEN_EXCEPTION_TYPE(FunctionCallException, Exception, "Bad Function Call Error");
GEN_EXCEPTION_TYPE(ArgumentException, FunctionCallException,
	"Invalid or Illegal Argument Error");

GEN_EXCEPTION_TYPE(RuntimeException, Exception, "Runtime Error");
GEN_EXCEPTION_TYPE(IndexException, RuntimeException, "Index out of bounds");
GEN_EXCEPTION_TYPE(RangeException, RuntimeException, "Value out of range");

GEN_EXCEPTION_TYPE(ThreadingException, RuntimeException, "Thread safety error");
GEN_EXCEPTION_TYPE(BusyException, RuntimeException, "Thread busy error");

GEN_EXCEPTION_TYPE(UnsetOptionalException, Exception, "Optional value unset");

GEN_EXCEPTION_TYPE(KeyException, RuntimeException, "Key Error");

GEN_EXCEPTION_TYPE(ResourceException, RuntimeException, "Resource Error");
GEN_EXCEPTION_TYPE(IdentifierException, ResourceException, "Resource ID Error");
GEN_EXCEPTION_TYPE(ResFmtException, ResourceException, "Resource Format Error");
GEN_EXCEPTION_TYPE(ResFilesException, ResourceException, "Resource file structure Error");

GEN_EXCEPTION_TYPE(CompressionException, RuntimeException, "Compression Error");

GEN_EXCEPTION_TYPE(GenToolsException, RuntimeException, "GenTools Error");

GEN_EXCEPTION_TYPE(EventFloodException, RuntimeException, "Event type limit exceeded");
GEN_EXCEPTION_TYPE(ConcurrencyException, RuntimeException, "Thread Concurrency Error");
GEN_EXCEPTION_TYPE(InputException, RuntimeException, "Input Error");
GEN_EXCEPTION_TYPE(KeyInputException, InputException, "Key Input Error");

GEN_EXCEPTION_TYPE(NetworkException, RuntimeException, "Network Error");
GEN_EXCEPTION_TYPE(IOException, NetworkException, "Stream Input/Output Error");
GEN_EXCEPTION_TYPE(NetDisconnectException, NetworkException, "Network Disconnection");
GEN_EXCEPTION_TYPE(NetPacketException, NetworkException, "Network Packet Error");
GEN_EXCEPTION_TYPE(NetProtocolException, NetworkException, "Network Protocol Error");

#define GEN_ASSERT(val, msg) do { if (!(val)) throw gen::AssertException(msg); } while (0)
#define GEN_ASSERTF(val, msg, ...) ASSERT(val, gen::String::createf(msg, __VA_ARGS__))

} // namespace gen
