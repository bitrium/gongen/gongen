// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "defines.hpp"

namespace gen {
GEN_API void init();
GEN_API void simple_feh();
GEN_API int wrap_main(int(*main_function)(int, char**), int argc, char** argv);
} // namespace gen
