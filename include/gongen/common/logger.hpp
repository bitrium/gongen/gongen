// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "event.hpp"
#include <gongen/common/containers/string.hpp>
#include <gongen/common/platform/system.hpp>
#include <stdarg.h>

namespace gen::log {
const static int32_t ALL_LEVELS = INT32_MIN;

const static int32_t LEVEL_TRACE = -2000;
const static int32_t LEVEL_DEBUG = -1000;
const static int32_t LEVEL_INFO = 0;
const static int32_t LEVEL_WARNING = 1000;
const static int32_t LEVEL_ERROR = 2000;

GEN_API void log(int32_t level, const gen::String& msg);
inline void trace(const gen::String& msg) { log(LEVEL_TRACE, msg); }
inline void debug(const gen::String& msg) { log(LEVEL_DEBUG, msg); }
inline void info(const gen::String& msg) { log(LEVEL_INFO, msg); }
inline void warn(const gen::String& msg) { log(LEVEL_WARNING, msg); }
inline void error(const gen::String& msg) { log(LEVEL_ERROR, msg); }

GEN_API void vlogf(int32_t level, const char* fmt, va_list va);
GEN_API void logf(int32_t level, const char* fmt, ...) GEN_PRINTF_CHECKING_CLS;
GEN_API void tracef(const char* fmt, ...) GEN_PRINTF_CHECKING;
GEN_API void debugf(const char* fmt, ...) GEN_PRINTF_CHECKING;
GEN_API void infof(const char* fmt, ...) GEN_PRINTF_CHECKING;
GEN_API void warnf(const char* fmt, ...) GEN_PRINTF_CHECKING;
GEN_API void errorf(const char* fmt, ...) GEN_PRINTF_CHECKING;

uint8_t pushd(uint8_t count = 1); // push depth
uint8_t popd(uint8_t count = 1); // pop depth
struct GEN_API Dive {
	uint8_t depth;
	explicit Dive(uint8_t depth) {
		this->depth = depth;
		pushd(this->depth);
	}
	~Dive() {
		popd(this->depth);
	}
	void done() {
		popd(this->depth);
		this->depth = 0;
	}
};

struct GEN_API Event :public gen::event::Event {
	int32_t level;
	uint8_t depth;
	gen::String msg;
	gen::String thread_name;
	gen::time::Time time;

	Event(int32_t level, uint8_t depth, const gen::String& msg) :gen::event::Event("log") {
		this->time = gen::time::from_start();
		this->level = level;
		this->depth = depth;
		this->msg = msg;
		this->thread_name = gen::thread::name();
	}
	~Event() {}
};

class GEN_API Handler {
	gen::event::Hopper* hopper;
public:
	Handler() { hopper = gen::event::hopper(gen::event::type("log")); }
	virtual ~Handler() = default;
	void update(bool handle_events = true) {
		hopper->poll();
		if (handle_events) {
			for (auto& e : hopper->events) handle((gen::log::Event*)e);
		}
		hopper->clear();
	}
	virtual void handle(gen::log::Event* event) = 0;
};

class GEN_API Thread {
	void* impl;
public:
	Thread(uint32_t sleep_ms = 10);
	~Thread();
	void add_handler(Handler* handler);
};

//(TIME) [THREAD] LEVEL: MESSAGE
GEN_API void default_log_format(gen::String& output, gen::log::Event* event);

typedef void(*log_format)(gen::String&, gen::log::Event*);

class GEN_API FileHandler :public Handler {
	gen::File file;
	int32_t min_level;
	gen::String temp_str;
	log_format log_formatter;
public:
	FileHandler(const gen::AbstractPath& path, int32_t min_level = gen::log::LEVEL_DEBUG, log_format log_formatter = default_log_format);
	~FileHandler();
	void handle(gen::log::Event* event);
};

class GEN_API ConsoleHandler :public Handler {
	int32_t min_level;
	gen::String temp_str;
	log_format log_formatter;
public:
	ConsoleHandler(int32_t min_level = gen::log::LEVEL_INFO, log_format log_formatter = default_log_format);
	~ConsoleHandler();
	void handle(gen::log::Event* event);
};
} // namespace gen::log
