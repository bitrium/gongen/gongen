// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "containers/pointers.hpp"
#include "math/vector.hpp"

namespace gen {
class GEN_API Cypher {
	void* impl;
	uint8_t type;
public:
	Cypher();
	~Cypher();
	Cypher& init(void* impl, uint8_t type);

	void key_gen(ByteBuffer& key); // pub or shared key
	void key_gen(ByteBuffer& skey, ByteBuffer& pkey);
	ByteBuffer key_encode(const ByteBuffer& key); // pub or shared key
	ByteBuffer key_encode(const ByteBuffer& skey, const ByteBuffer& pkey);
	ByteBuffer key_decode(const ByteBuffer& bin);
	ByteBuffer key_decode(const ByteBuffer& bin, const ByteBuffer& pkey);
	void encrypt(const ByteBuffer& pkey, ByteBuffer& dst, const ByteBuffer& src);
	void decrypt(const ByteBuffer& skey, ByteBuffer& dst, const ByteBuffer& src);
	void sign(ByteBuffer skey, ByteBuffer& sig, const ByteBuffer& msg);
	bool verify(ByteBuffer pkey, const ByteBuffer& sig, const ByteBuffer& msg);
};

GEN_API void cypher_rsa(Cypher* cypher, size_t modulus_size = 3072);
GEN_API void cypher_aes(Cypher* cypher);
} // namespace gen
