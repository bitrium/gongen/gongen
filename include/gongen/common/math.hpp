// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include "math/math.hpp"
#include "math/vector.hpp"
#include "math/rect.hpp"
#include "math/color.hpp"
#include "math/matrix.hpp"
