// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "defines.hpp"
#include "containers/string.hpp"

namespace gen {
uint64_t GEN_API hash(const void* data, size_t size);
uint64_t GEN_API hash(const void* data, size_t size, uint64_t seed);
inline uint64_t GEN_API hash(const ByteBuffer& data) { return hash(data.ptr(), data.size()); }
inline uint64_t GEN_API hash(const ByteBuffer& data, uint64_t seed) { return hash(data.ptr(), data.size(), seed); }
inline uint64_t hash(const gen::String& str) { return hash(str.c_str(), str.size()); }
inline uint64_t hash(const gen::String& str, uint64_t seed) { return hash(str.c_str(), str.size(), seed); }
// simple hash function for primitives (hash_pr)
template<typename T>
inline uint64_t hash_pr(const T& primitive) { return hash(&primitive, sizeof(primitive)); }
template<typename T>
inline uint64_t hash_pr(const T& primitive, uint64_t seed) { return hash(&primitive, sizeof(primitive), seed); }

// no_hash can be used whenever a hash callback is needed, but the value must not be hashed
template<typename T>
inline uint64_t no_hash(T value) { return (uint64_t) value; }
template<typename T>
inline uint64_t no_hash(T value, uint64_t seed) { return (uint64_t) value; }

uint64_t GEN_API hash_combine(uint64_t lhs, uint64_t rhs);
uint64_t GEN_API hash_combine_n(size_t count, ...);
inline uint64_t& hash_combine_ref(uint64_t& val, uint64_t mod) { val = hash_combine(val, mod); return val; }
}
