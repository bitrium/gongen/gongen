// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <string.h>
#include "containers/buffer.hpp"

namespace gen {
const static size_t RW_ERROR = SIZE_MAX;

struct GEN_API DataSource {
	virtual ~DataSource() {}
	virtual size_t size() = 0; //Return 0 for streams
	virtual size_t read(void* data, size_t size) = 0; //Return bytes read, on error returns RW_ERROR or throws
};

enum class Seek {
	SET,
	CUR,
	END,
};

struct GEN_API SeekableDataSource :virtual public DataSource {
	virtual size_t cursor() = 0;
	virtual size_t seek(ptrdiff_t offset, Seek type = Seek::SET) = 0;
};

struct GEN_API WritableDataSource :virtual public DataSource {
	virtual size_t write(const void* data, size_t size) = 0;
	virtual void flush() = 0;
};

struct GEN_API SeekableWritableDataSource :virtual public SeekableDataSource, WritableDataSource {};

class GEN_API MemoryDataSource :public SeekableDataSource {
	void* memory;
	size_t memory_size;
	size_t memory_cursor;
	bool own_memory;
public:
	MemoryDataSource() {
		memory = nullptr;
		memory_size = 0;
		memory_cursor = 0;
		own_memory = false;
	}
	MemoryDataSource(void* memory, size_t size, bool own_memory) {
		setup(memory, size, own_memory);
	}
	~MemoryDataSource() {
		if (own_memory) gen::free(memory);
	}
	void setup(void* memory, size_t size, bool own_memory) {
		this->memory = memory;
		this->memory_size = size;
		this->memory_cursor = 0;
		this->own_memory = own_memory;
	}

	size_t size() override { return memory_size; }
	size_t read(void* data, size_t size) override {
		size_t size_to_read = size;
		if ((size + memory_cursor) > memory_size) size_to_read = (memory_size - memory_cursor);
		void* memory_ptr = ((uint8_t*)memory + memory_cursor);
		memory_cursor += size_to_read;
		if(data) memcpy(data, memory_ptr, size_to_read);
		return size_to_read;
	}

	size_t cursor() override { return memory_cursor; }
	size_t seek(ptrdiff_t offset, Seek type = Seek::SET) override {
		if (type == Seek::SET) memory_cursor = offset;
		else if (type == Seek::CUR) memory_cursor += offset;
		else if (type == Seek::END) memory_cursor = (size() - offset);
		return memory_cursor;
	}
};

class GEN_API ByteBufferDataSource :public SeekableWritableDataSource {
	size_t memory_cursor = 0;
public:
	gen::ByteBuffer buffer;

	ByteBufferDataSource() {}
	~ByteBufferDataSource() {}
	size_t size() override { return buffer.size(); }
	size_t read(void* data, size_t size) override {
		size_t memory_size = this->size();
		size_t size_to_read = size;
		if ((size + memory_cursor) > memory_size) size_to_read = (memory_size - memory_cursor);
		void* memory_ptr = buffer.ptr(memory_cursor);
		memory_cursor += size_to_read;
		if (data) memcpy(data, memory_ptr, size_to_read);
		return size_to_read;
	}
	size_t write(const void* data, size_t size) override {
		//size_t memory_size = this->size(false);
		buffer.resize(memory_cursor);
		buffer.add(data, size);
		memory_cursor = buffer.size();
		//if (buffer.size() < memory_size) buffer.resize(memory_size);
		return size;
	}

	size_t cursor() override { return memory_cursor; }
	size_t seek(ptrdiff_t offset, Seek type = Seek::SET) override {
		if (type == Seek::SET) memory_cursor = offset;
		else if (type == Seek::CUR) memory_cursor += offset;
		else if (type == Seek::END) memory_cursor = (this->size() - offset);
		return memory_cursor;
	}
	void flush() override {}
};
}
