// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "containers/string.hpp"

namespace gen::utf8 {
size_t strlen(const char* input);
GEN_API gen::Buffer<uint32_t> decode(const char* input, size_t size);
GEN_API gen::String encode(uint32_t* input, size_t size);
inline gen::Buffer<uint32_t> decode(const char* input) { return decode(input, strlen(input)); }
inline gen::String encode(const gen::Buffer<uint32_t>& input) {return encode(input.ptr(), input.size()); }
}

//Size is in characters
//encode adds NULL to the end of returned buffer
namespace gen::utf16 {
size_t strlen(const char16_t* input);
GEN_API gen::Buffer<uint32_t> decode(const char16_t* input, size_t size);
GEN_API gen::Buffer<char16_t> encode(uint32_t* input, size_t size);
inline gen::Buffer<uint32_t> decode(const char16_t* input) { return decode(input, strlen(input)); }
inline gen::Buffer<char16_t> encode(const gen::Buffer<uint32_t>& input) { return encode(input.ptr(), input.size()); }
}

namespace gen::base64 {
GEN_API size_t encode_len(size_t input_size);
GEN_API size_t decode_len(size_t input_size);
GEN_API gen::ByteBuffer decode(const char* input, size_t size);
GEN_API gen::String encode(const uint8_t* input, size_t size);
inline gen::ByteBuffer decode(const char* input) { return decode(input, strlen(input)); }
inline gen::ByteBuffer decode(const String& input) { return decode(input.c_str(), input.size()); }
inline gen::String encode(const gen::ByteBuffer& input) {return encode(input.ptr(), input.size()); }
}
