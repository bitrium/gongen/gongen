// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "defines.hpp"
#include <stddef.h>
#include <stdint.h>
#include <gongen/common/containers/buffer.hpp>

namespace gen {
class GEN_API Compressor {
public:
	//Max supported level
	virtual uint32_t max_level() = 0;

	//Default level(the same used if compression_level is 0)
	virtual uint32_t default_level() = 0;

	//Max amount that compress function can write for specified src_size
	virtual size_t compress_bound(size_t size) = 0;

	//Compresses data from src and writes compressed data to dst, returns size of written data, on fail returns 0 or throws CompressionException
	//Compression_level of 0 will use default level
	virtual size_t compress(void* dst, size_t dst_size, const void* src, size_t src_size, uint32_t compression_level = 0) = 0;
	size_t compress(gen::ByteBuffer& dst, const void* src, size_t src_size, uint32_t compression_level = 0) {
		size_t dst_presize = dst.size();
		size_t dst_size = compress_bound(src_size);
		size_t compressed_size = compress(dst.reserve(dst_size), dst_size, src, src_size, compression_level);
		dst.resize(dst_presize + compressed_size);
		return compressed_size;
	}

	//Decompressed data from src and writes uncompressed data to dst, returns size of written data, on fail returns 0 or throws CompressionException
	virtual size_t decompress(void* dst, size_t dst_size, const void* src, size_t src_size) = 0;
	size_t decompress(gen::ByteBuffer& dst, size_t dst_size, const void* src, size_t src_size) {
		return decompress(dst.reserve(dst_size), dst_size, src, src_size);
	}
};

#ifdef GEN_DEP_ZSTD
GEN_API Compressor* compressor_zstd();
#endif
GEN_API Compressor* compressor_zlib();

} // namespace gen
