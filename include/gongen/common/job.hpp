// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/defines.hpp>
#include <gongen/common/containers/dynamic_array.hpp>
#include <gongen/common/platform/system.hpp>

namespace gen::job {
class GEN_API Handle {
	void* impl;

	Handle(void* impl) { this->impl = impl; }
public:
	~Handle();
	bool is_done();
	void join();
	void* get_return_value(size_t index = 0);
	void rethrow_exception(size_t index = 0);
	size_t count();

	friend class System;
};

class GEN_API System {
	gen::DynamicArray<gen::thread::Thread> threads;
	void* thread_data;
public:
	System(uint32_t thread_count = 0, gen::feh::handler_ret(*feh_handler)(gen::feh::handler_info*) = nullptr, void* feh_userdata = nullptr, uint32_t feh_mask = (gen::feh::SEGFAULT | gen::feh::STACK_OVERFLOW | gen::feh::ILLEGAL_INSTRUCTION));
	~System();

	Handle add_job(void* (*proc)(void*), void* userdata);
	Handle add_jobs(void* (*proc)(void*, size_t), void* userdata, size_t count, void (*done_callback)(void*));
	bool busy();
};
} // namespace gen::job
