// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/pointers.hpp>

namespace gen::rand {

class Rng {
public:
	virtual ~Rng() = default;
	/**
	 * @brief Reseed the RNG.
	 * @param seed New seed.
	 * @return Processed seed (usually same as the argument).
	 */
	virtual uint64_t seed(uint64_t seed) = 0;
	/**
	 * @brief Generate the next number.
	 * @return Generated number.
	 */
	virtual uint64_t next() = 0;
	/**
	 * @brief Get range of the RNG.
	 * @return Possible combinations; biggest possibly generated number + 1.
	 */
	virtual uint64_t get_limit() = 0;
	/**
	 * @brief Get size of rng internal state
	 * @return Size of internal state in bytes
	 */
	virtual size_t state_size() = 0;
	/**
	 * @brief Get rng internal state
	 * @param state pointer to place where state will be written, it must have at least size of value returned by state_size()
	 */
	virtual void get_state(void* state) = 0;
	/**
	 * @brief Set rng internal state
	 * @param state pointer to place where state was gotten with get_state()
	 * @param size size of rng state to set
	 */
	virtual void set_state(void* state, size_t size) = 0;
};

GEN_API Rng* rng_lcg();

struct GEN_API Random {
	SharedPtr<Rng> rng;
	bool range_errors = true;

	uint64_t seed(); // seeds with current time and returns the seed
	uint64_t seed(uint64_t seed);

	int8_t i8(int8_t min, int8_t max);
	inline int8_t i8(int8_t range) { return this->i8(-range, range); }
	inline int8_t i8() { return this->i8(INT8_MIN, INT8_MAX); }
	int16_t i16(int16_t min, int16_t max);
	inline int16_t i16(int16_t range) { return this->i16(-range, range); }
	inline int16_t i16() { return this->i16(INT16_MIN, INT16_MAX); }
	int32_t i32(int32_t min, int32_t max);
	inline int32_t i32(int32_t range) { return this->i32(-range, range); }
	inline int32_t i32() { return this->i32(INT32_MIN, INT32_MAX); }
	int64_t i64(int64_t min, int64_t max);
	inline int64_t i64(int64_t range) { return this->i64(-range, range); }
	inline int64_t i64() { return this->i64(INT64_MIN, INT64_MAX); }
	uint8_t u8(uint8_t min, uint8_t max);
	inline uint8_t u8(uint8_t limit) { return this->u8(0, limit - 1); }
	inline uint8_t u8() { return this->u8(0, UINT8_MAX); }
	uint16_t u16(uint16_t min, uint16_t max);
	inline uint16_t u16(uint16_t limit) { return this->u16(0, limit - 1); }
	inline uint16_t u16() { return this->u16(0, UINT16_MAX); }
	uint32_t u32(uint32_t min, uint32_t max);
	inline uint32_t u32(uint32_t limit) { return this->u32(0, limit - 1); }
	inline uint32_t u32() { return this->u32(0, UINT32_MAX); }
	uint64_t u64(uint64_t min, uint64_t max);
	inline uint64_t u64(uint64_t limit) { return this->u64(0, limit - 1); }
	inline uint64_t u64() { return this->u64(0, UINT64_MAX); }
	float f32(float min, float max);
	inline float f32(float range) { return f32(-range, range); }
	inline float f32() { return f32(0.0f, 1.0f); }
	double f64(double min, double max);
	inline double f64(double range) { return f64(-range, range); }
	inline double f64() { return f64(0.0, 1.0); }
	bool boolean();

	char alnum(); // random letter (any case) or numeric digit
	char letter(); // random lowercase letter (use math for uppercase)
	char digit(); // random numeric digit
};

} // namespace gen::rand
