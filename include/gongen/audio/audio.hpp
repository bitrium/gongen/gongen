// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include "format.hpp"
#include <gongen/common/containers/string.hpp>
#include <gongen/common/containers/pointers.hpp>

namespace gen::audio {

class Device {
public:
	virtual ~Device() = default;

	virtual FormatScope supported_formats() const = 0;

	/**
	 * @brief Obtains unique implementation-defined device id.
	 * The identifier must keep validity between runtimes.
	 * @return The identifier.
	 */
	virtual String get_id() const = 0;
	/**
	 * @brief Checks if it is an actual device.
	 *
	 * Virtual devices (sources and sinks) should still be labeld real.
	 * "Not real" devices may include any interfaces on the computer that
	 * support audio input or output. This may include, but is not limited to:
	 * bridges, audio monitors, other applications.
	 *
	 * The PipeWire implementation will allow any Nodes that support audio, but
	 * only ones that have devices linked will be marked real.
	 *
	 * @return True if real or unknown.
	 */
	virtual bool is_real() const = 0;
	/**
	 * @brief Obtains the name of the device.
	 * @return Device's name.
	 */
	virtual String get_name() const = 0;
	/**
	 * @brief Checks whether the device's purpose is to be used as an audio source.
	 * @return Whether is a source.
	 */
	virtual bool is_source() const = 0;
	/**
	 * @brief Checks whether the device's purpose is to be used as an audio sink.
	 * @return Whether is a sink.
	 */
	virtual bool is_sink() const = 0;
	/**
	 * @brief Checks whether the device provides audio input.
	 * @return Whether provides input.
	 */
	virtual bool supports_input() const = 0;
	/**
	 * @brief Checks whether the device accepts audio output.
	 * @return Whther accepts output.
	 */
	virtual bool supports_output() const = 0;
};

class Stream {
	float pre_mute_volume = 1.0f;
public:
	virtual ~Stream() = default;

	/**
	 * @brief Get the used audio format.
	 * @return The format.
	 */
	virtual Format get_format() = 0;

	/**
	 * @brief Gets the currently used device.
	 * @return Current device info or null pointer if unknown.
	 */
	virtual const Device* get_device() = 0;
	/**
	 * @brief Chose device for the input/output.
	 * @param device Selected device info or null pointer for default.
	 * @return Whether it was changed succesfully.
	 */
	virtual bool set_device(const Device* device) = 0;

	/**
	 * @brief Obtains the "application name".
	 * @return The name, or empty string if unsupported or unknown.
	 */
	virtual String get_name() = 0;
	/**
	 * @brief Changes the "application name".
	 * @param name The new name.
	 * @return 'true' on success; 'false' if unsupported or unavailable.
	 */
	virtual bool set_name(const String& name) = 0;
	/**
	 * @brief Obtains the "application volume" of the system mixer.
	 * @return The value in range 0..1 or 1 if unsupported or unknown.
	 */
	virtual float get_volume() = 0;
	/**
	 * @brief Changes the "application volume" of the system mixer.
	 * @param volume The new value in range 0..1.
	 * @return 'true' on success; 'false' if unsupported or unavailable.
	 */
	virtual bool set_volume(float volume) = 0;
	/**
	 * @brief Checks whether the "application" is muted in the system mixer.
	 * @return 'true' if muted; 'false' otherwise or if unsupported or unknown.
	 */
	virtual bool is_muted() = 0;
	/**
	 * @brief Sets whether the "application" is muted in the system mixer.
	 * @param mute The new value; whether to mute or unmute the input/output.
	 * @return 'true' on success; 'false' if unsupported or unavailable.
	 */
	virtual bool set_muted(bool mute) = 0;
	/**
	 * @brief Attemts to 'set_muted', and uses 'set_volume' instead if it fails.
	 * @return 'true' on success; 'false' on failure of both functions.
	 */
	bool mute();
	/**
	 * @brief Attemts to 'set_muted', and uses 'set_volume' instead if it fails.
	 * If 'set_muted' fails, uses volume before the last 'mute'.
	 * @return 'true' on success; 'false' on failure of both functions.
	 */
	bool unmute();
};

class Output : public Stream {
public:
	virtual ~Output() = default;
};
typedef size_t (*OutputCallback)(void* user, Output* stream, size_t frame_count, uint8_t* buffer);

class System {
public:
	virtual ~System() = default;

	/**
	 * @brief Enumerates and returns information about available devices.
	 * @param real_only Force only real devices. Other, impl-dependent things may be added if false.
	 * @return The list of enumerated devices (caller-owned).
	 */
	virtual DynamicArray<Ptr<Device>> enumerate_devices(bool real_only = true) = 0;
	/**
	 * @brief Creates an output.
	 * @param device Selected device (use enumerate_devices). Null pointer for default.
	 * @param app_name Application or output name.
	 * @param format Sound format to use.
	 * @param callback Data callback for querying samples.
	 * @param user User data for the callback.
	 * @return An object representing the output.
	 */
	virtual Output* output(Device* device, const String& app_name, Format format,
			OutputCallback callback, void* user) = 0;
};

System* system();

} // namespace gen::audio
