// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/containers/buffer.hpp>

namespace gen::audio {

struct Format {
	uint8_t bit_depth;
	uint32_t sample_rate;
	uint8_t channel_count;

	inline bool operator==(const Format& other) const {
		return bit_depth == other.bit_depth && sample_rate == other.sample_rate && channel_count == other.channel_count;
	}
};

template<typename T>
struct ScopeChoice {
	bool is_range;
	// union {
	struct {
		gen::Buffer<T> select;
		T range[2];
	} val;
	T default_val;

	inline bool supports(T value) const {
		if (!this->is_range) {
			for (T option : this->val.select) {
				if (option == value) return true;
			}
			return false;
		} else {
			return value >= this->val.range[0] && value <= this->val.range[1];
		}
	}
	inline void reset() {
		this->is_range = false;
		this->val.select.clear();
		this->default_val = T(0);
	}
};
struct FormatScope {
	ScopeChoice<uint8_t> bit_depth;
	ScopeChoice<uint32_t> sample_rate;
	ScopeChoice<uint8_t> channel_count;

	inline Format choose_defaults() const {
		return Format {
			.bit_depth = this->bit_depth.default_val,
			.sample_rate = this->sample_rate.default_val,
			.channel_count = this->channel_count.default_val,
		};
	}
	inline bool supports(const Format& format) const {
		return (
			this->bit_depth.supports(format.bit_depth) &&
			this->sample_rate.supports(format.sample_rate) &&
			this->channel_count.supports(format.channel_count)
		);
	}
	inline void reset() {
		this->bit_depth.reset();
		this->sample_rate.reset();
		this->channel_count.reset();
	}
};

} // namespace gen::audio
