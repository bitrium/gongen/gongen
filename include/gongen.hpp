// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#ifdef GEN_MOD_COMMON
#include "gongen/common.hpp"
#endif
#ifdef GEN_MOD_WINDOW
#include "gongen/window.hpp"
#endif
#ifdef GEN_MOD_AUDIO
#include "gongen/audio.hpp"
#endif
#ifdef GEN_MOD_GL
#include "gongen/gl.hpp"
#endif
#ifdef GEN_MOD_IMGUI
#include "gongen/imgui.hpp"
#endif
#ifdef GEN_MOD_GSF
#include "gongen/gsf.hpp"
#endif
#ifdef GEN_MOD_LOADERS_CORE
#include "gongen/loaders.hpp"
#endif
#ifdef GEN_MOD_ECS
#include "gongen/ecs.hpp"
#endif
