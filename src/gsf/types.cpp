// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/gsf/types.hpp>

#include <gsf_types.h>
#include <gongen/gsf/error.hpp>

using namespace gen::gsf;

#define IMPL ((gsf_type*) this->impl)
char* Type::to_string_buf(char* buf, size_t buf_size) const {
	return handle_error(gsf_type_to_string_buf(IMPL, buf, buf_size));
}
gen::String Type::to_string() const {
	return String(handle_error(gsf_type_to_string(IMPL)));
}

uint8_t Type::scalar_width() const {
	return gsf_type_scalar_width(IMPL);
}
bool Type::is_primitive() const {
	return gsf_type_is_primitive(IMPL);
}
bool Type::is_null() const {
	return gsf_type_is_null(IMPL);
}
bool Type::is_string() const {
	return gsf_type_is_string(IMPL);
}
bool Type::is_scalar() const {
	return gsf_type_is_scalar(IMPL);
}
bool Type::is_int() const {
	return gsf_type_is_int(IMPL);
}
bool Type::is_uint() const {
	return gsf_type_is_uint(IMPL);
}
bool Type::is_float() const {
	return gsf_type_is_float(IMPL);
}
bool Type::is_enum() const {
	return gsf_type_is_enum(IMPL);
}
bool Type::is_container() const {
	return gsf_type_is_container(IMPL);
}
bool Type::is_collection() const {
	return gsf_type_is_collection(IMPL);
}
bool Type::is_array() const {
	return gsf_type_is_array(IMPL);
}
bool Type::is_list() const {
	return gsf_type_is_list(IMPL);
}
bool Type::is_map() const {
	return gsf_type_is_map(IMPL);
}
bool Type::is_object() const {
	return gsf_type_is_object(IMPL);
}

Type Type::element_type() const {
	return Type((void*) handle_error(gsf_type_get_element_type(IMPL)));
}
Typing Type::element_typing() const {
	return Typing((void*) handle_error(gsf_type_get_element_typing(IMPL)));
}
void* Type::object_class() const {
	return (void*) handle_error(gsf_type_get_object_class(IMPL));
}
void* Type::get_enum() const {
	return (void*) handle_error(gsf_type_get_enum(IMPL));
}
#undef IMPL


#define IMPL ((gsf_typing*) this->impl)
Typing& Typing::with_type(const Type& type, bool is_default) {
	this->impl = (void*) handle_error(gsf_typing_with_type(IMPL, type.get_impl(), is_default));
	return *this;
}
Typing& Typing::with_default(size_t index) {
	this->impl = (void*) handle_error(gsf_typing_with_default(IMPL, index));
	return *this;
}
Typing& Typing::without_default() {
	this->impl = (void*) handle_error(gsf_typing_without_default(IMPL));
	return *this;
}

char* Typing::to_string_buf(char* buf, size_t buf_size) const {
	return handle_error(gsf_typing_to_string_buf(IMPL, buf, buf_size));
}
gen::String Typing::to_string() const {
	return String(handle_error(gsf_typing_to_string(IMPL)));
}

size_t Typing::type_count() const {
	return gsf_typing_type_count(IMPL);
}
gen::Optional<Type> Typing::get_type(size_t index) const {
	auto* ret = gsf_typing_get_type(IMPL, index);
	if (!ret) return gen::Optional<Type>();
	return gen::Optional<Type>(Type((void*) ret));
}
gen::Optional<Type> Typing::get_default() const {
	auto* ret = gsf_typing_get_default(IMPL);
	if (!ret) return gen::Optional<Type>();
	return gen::Optional<Type>(Type((void*) ret));
}

bool Typing::is_omnibus() const {
	return gsf_typing_is_omnibus(IMPL);
}
size_t Typing::find(const Type& type) const {
	size_t out;
	handle_error(gsf_typing_find(IMPL, type.get_impl(), &out));
	return out;
}
#undef IMPL
