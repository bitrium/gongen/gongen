#include <gongen/gsf/bank.hpp>
#include <gongen/common/hash.hpp>
#include <gongen/common/platform/system.hpp>

using namespace gen;
using namespace gen::gsf;

Bank::Bank(const char* path) : schemas(gen::hash), descs(gen::hash) {
	this->dir = path;
}
Schema Bank::schema(const String& name) {
	if (this->schemas.contains(name)) return this->schemas.get(name);
	gen::file::Path path(this->dir);
	path /= (name + ".gts");
	File file(path);
	return this->schemas.add(name, Schema::txt_read(&file));
}
Schema Bank::schema(const String& name, Context& ctx) {
	if (this->schemas.contains(name)) return this->schemas.get(name);
	gen::file::Path path(this->dir);
	path /= (name + ".gts");
	File file(path);
	return this->schemas.add(name, Schema::txt_read_to_ctx(ctx, &file));
}
HashMap<const char*, Bank> banks(gen::hash_pr);
Bank* gen::gsf::get_schema_bank(const char* path) {
	if (banks.contains(path)) return &banks.get(path);
	return &banks.emplace(path, std::move(Bank(path)));
}
