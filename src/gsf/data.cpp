// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/gsf/data.hpp>

#include <gsf_data.h>
#include <gsf_context.h>
#include <gsf_codec.h>
#include <gongen/common/platform/system.hpp>
#include <gongen/gsf/error.hpp>

#ifdef _MSC_VER
#undef gsf_schema_get_class
#undef gsf_schema_get_enum
#undef gsf_class_get_label
#undef gsf_class_get_label_at_key
#undef gsf_article_access_root
#undef gsf_object_access_at_label
#undef gsf_object_access_at_key
#undef gsf_list_get_element
#undef gsf_map_get_value
#undef gsf_map_get_value_at_key
#undef gsf_context_get_schema_by_idx
#undef gsf_context_get_schema
#endif

using namespace gen::gsf;

#define IMPL ((gsf_descriptor*) (this->impl))
Descriptor::Descriptor(const String& group, const String& name, const String& version) : own(true) {
	this->impl = (void*) handle_error(gsf_descriptor_new(group.c_str(), name.c_str(), version.c_str()));
}
Descriptor::Descriptor(const String& string) : own(true) {
	this->impl = (void*) handle_error(gsf_create_descriptor());
	handle_error(gsf_descriptor_from_str(IMPL, string.c_str()), [](void* user) {
		gsf_delete_descriptor((gsf_descriptor*) user);
		throw_error(user);
	}, this->impl);
}
Descriptor::Descriptor(const char* buf, size_t len) : own(true) {
	this->impl = (void*) handle_error(gsf_create_descriptor());
	handle_error(gsf_descriptor_from_str_buf(IMPL, buf, len), [](void* user) {
		gsf_delete_descriptor((gsf_descriptor*) user);
		throw_error(user);
	}, this->impl);
}
Descriptor::~Descriptor() {
	this->destroy();
}
void Descriptor::destroy() {
	if (!this->own) return;
	gsf_delete_descriptor(IMPL);
}

char* Descriptor::to_string(char* buf, size_t buf_size) const {
	return handle_error(gsf_descriptor_to_str_buf(IMPL, buf, buf_size));
}
gen::String Descriptor::to_string() const {
	return String(handle_error(gsf_descriptor_to_str(IMPL)));
}

void Descriptor::edit(const String& group, const String& name, const String& version) {
	handle_error(gsf_descriptor_edit(IMPL, group.c_str(), name.c_str(), version.c_str()));
}

gen::String Descriptor::get_group() const {
	return gsf_descriptor_get_group(IMPL);
}
gen::String Descriptor::get_name() const {
	return gsf_descriptor_get_name(IMPL);
}
gen::String Descriptor::get_version() const {
	return gsf_descriptor_get_version(IMPL);
}

bool Descriptor::match(const Descriptor& other) {
	return gsf_descriptor_match(IMPL, (gsf_descriptor*) other.impl);
}

#undef IMPL


#define IMPL ((gsf_schema*) this->impl)
Schema::Schema(const Descriptor& descriptor, const TypingRepr& root, size_t class_count) : own(true) {
	this->impl = (void*) handle_error(gsf_schema_new((gsf_descriptor*) descriptor.impl, root.get_impl(), class_count));
}
Schema::~Schema() {
	this->destroy();
}
void Schema::destroy() {
	if (!this->own) return;
	gsf_delete_schema(IMPL);
}

Descriptor Schema::get_descriptor() const {
	return Descriptor((void*) handle_error(gsf_schema_get_descriptor(IMPL)));
}
void Schema::set_descriptor(const Descriptor& descriptor) {
	gsf_schema_set_descriptor(IMPL, (gsf_descriptor*) descriptor.impl);
}

Context Schema::get_context() const {
	const gsf_context* context = handle_error(gsf_schema_get_context(IMPL));
	if (!context) throw RuntimeException("Schema is not context-made.");
	return Context((void*) context);
}

Typing Schema::get_root() const {
	return Typing((void*) handle_error(gsf_schema_get_root(IMPL)));
}
void Schema::set_root(const TypingRepr& typing) {
	handle_error(gsf_schema_set_root(IMPL, typing.get_impl()));
}

size_t Schema::class_count() const {
	return gsf_schema_class_count(IMPL);
}
Class Schema::add_class(const String& name, size_t label_count) {
	return Class((void*) handle_error(gsf_schema_add_class(IMPL, name.c_str(), label_count)));
}
gen::Optional<Class> Schema::get_class(size_t index) {
	auto* ret = gsf_schema_get_class(IMPL, index);
	if (!ret) return gen::Optional<Class>();
	return gen::Optional<Class>(Class((void*) ret));
}

size_t Schema::enum_count() const {
	return gsf_schema_enum_count(IMPL);
}
Enum Schema::add_enum(const String& name, size_t variant_count) {
	return Enum((void*) handle_error(gsf_schema_add_enum(IMPL, name.c_str(), variant_count)));
}
gen::Optional<Enum> Schema::get_enum(size_t index) {
	auto* ret = gsf_schema_get_enum(IMPL, index);
	if (!ret) return gen::Optional<Enum>();
	return gen::Optional<Enum>(Enum((void*) ret));
}
// void Schema::remove_enum(size_t index) {
// 	gsf_schema_remove_enum(IMPL, index);
// }
#undef IMPL


#define IMPL ((gsf_class*) this->impl)
Type Class::as_type() const {
	return Type((void*) handle_error(gsf_class_as_type(IMPL)));
}
gen::String Class::get_name() {
	return String(handle_error(gsf_class_get_name(IMPL)));
}
void Class::set_name(String& string) {
	handle_error(gsf_class_set_name(IMPL, string.c_str()));
}

size_t Class::label_count() const {
	return gsf_class_label_count(IMPL);
}
Label Class::add_label(const String& key, const TypingRepr& typing) {
	return Label((void*) handle_error(gsf_class_add_label(IMPL, key.c_str(), typing.get_impl())));
}
gen::Optional<Label> Class::get_label(size_t index) {
	auto* ret = gsf_class_get_label(IMPL, index);
	if (!ret) return Optional<Label>();
	return Optional<Label>(Label((void*) ret));
}
gen::Optional<Label> Class::get_label_at_key(const String& key) {
	auto* ret = gsf_class_get_label_at_key(IMPL, key.c_str());
	if (!ret) return Optional<Label>();
	return Optional<Label>(Label((void*) ret));
}
size_t Class::index_at_label(Label label) {
	return handle_error(gsf_class_index_at_label(IMPL, (gsf_label*) label.impl)) - 1;
}
size_t Class::index_at_key(const String& key) {
	return handle_error(gsf_class_index_at_key(IMPL, key.c_str())) - 1;
}
void Class::remove_label(size_t index) {
	gsf_class_remove_label(IMPL, index);
}
#undef IMPL


#define IMPL ((gsf_label*) this->impl)
gen::String Label::get_key() const {
	return String(handle_error(gsf_label_get_key(IMPL)));
}
void Label::set_key(const String& key) {
	handle_error(gsf_label_set_key(IMPL, key.c_str()));
}

Typing Label::get_typing() const {
	return Typing((void*) handle_error(gsf_label_get_typing(IMPL)));
}
void Label::set_typing(const TypingRepr& typing) {
	handle_error(gsf_label_set_typing(IMPL, typing.get_impl()));
}

gen::Optional<Value> Label::get_default() const {
	auto* ret = gsf_label_get_default(IMPL);
	if (!ret) return Optional<Value>();
	return Optional<Value>(Value((void*) ret));
}
Value Label::access_default() {
	return Value((void*) handle_error(gsf_label_access_default(IMPL)));
}

gen::Optional<gen::String> Label::get_doc() const {
	auto* ret = gsf_label_get_doc(IMPL);
	if (!ret) return Optional<String>();
	return Optional<String>(String(ret));
}
void Label::set_doc(const String& doc) {
	handle_error(gsf_label_set_doc(IMPL, doc.c_str()));
}
#undef IMPL


#define IMPL ((gsf_enum*) this->impl)
Type Enum::as_type() const {
	return Type((void*) handle_error(gsf_enum_as_type(IMPL)));
}

gen::String Enum::get_name() const {
	return String(handle_error(gsf_enum_get_name(IMPL)));
}

size_t Enum::variant_count() const {
	return handle_error(gsf_enum_variant_count(IMPL));
}
Variant Enum::add_variant(const String& name) {
	return Variant((void*) handle_error(gsf_enum_add_variant(IMPL, name.c_str())));
}
void Enum::remove_variant(size_t index) {
	gsf_enum_remove_variant(IMPL, index);
}
gen::Optional<Variant> Enum::get_variant(size_t index) {
	auto* ret = gsf_enum_get_variant(IMPL, index);
	if (!ret) return Optional<Variant>();
	return Optional<Variant>(Variant((void*) ret));
}
gen::Optional<Variant> Enum::get_variant_by_name(const String& name) {
	auto* ret = gsf_enum_get_variant_by_name(IMPL, name.c_str());
	if (!ret) return Optional<Variant>();
	return Optional<Variant>(Variant((void*) ret));
}
#undef IMPL


#define IMPL ((gsf_variant*) this->impl)
gen::String Variant::get_name() const {
	return handle_error(gsf_variant_get_name(IMPL));
}
gen::Optional<gen::String> Variant::get_doc() const {
	auto* ret = gsf_variant_get_doc(IMPL);
	if (!ret) return Optional<String>();
	return Optional<String>(String(ret));
}
void Variant::set_doc(const String& doc) {
	handle_error(gsf_variant_set_doc(IMPL, doc.c_str()));
}
#undef IMPL



#define IMPL ((gsf_article*) this->impl)
Article::Article(const Schema& schema) : own(true) {
	this->impl = gsf_article_new((gsf_schema*) schema.impl);
}
Article::~Article() {
	this->destroy();
}
void Article::destroy() {
	if (!this->own) return;
	gsf_delete_article(IMPL);
}

Schema Article::get_schema() const {
	return Schema((void*) gsf_article_get_schema(IMPL));
}

Value Article::access_root() const {
	return Value((void*) handle_error(gsf_article_access_root(IMPL)));
}
#undef IMPL


#define IMPL ((gsf_object*) this->impl)
Class Object::get_class() const {
	return Class((void*) handle_error(gsf_object_get_class(IMPL)));
}
gen::Optional<Value> Object::access_at_label(const Label& label) {
	auto* ret = gsf_object_access_at_label(IMPL, (gsf_label*) label.impl);
	if (!ret) Optional<Value>();
	return Optional<Value>(Value((void*) ret));
}
gen::Optional<Value> Object::access_at_key(const String& key) {
	auto* ret = gsf_object_access_at_key(IMPL, key.c_str());
	if (!ret) Optional<Value>();
	return Optional<Value>(Value((void*) ret));
}

size_t Object::apply_defaults() {
	return gsf_object_apply_defaults(IMPL);
}
size_t Object::count_unset() const {
	return gsf_object_count_unset(IMPL);
}
#undef IMPL


#define IMPL ((gsf_list*) this->impl)
Typing List::get_typing() const {
	return Typing((void*) handle_error(gsf_list_get_typing(IMPL)));
}

size_t List::get_size() const {
	return gsf_list_get_size(IMPL);
}
void List::resize(size_t size) {
	handle_error(gsf_list_resize(IMPL, size));
}

size_t List::element_count() const {
	return gsf_list_element_count(IMPL);
}
gen::Optional<Value> List::get_element(size_t index) {
	auto* ret = gsf_list_get_element(IMPL, index);
	if (!ret) return Optional<Value>();
	return Optional<Value>(Value((void*) ret));
}
Value List::add_element() {
	return Value((void*) handle_error(gsf_list_add_element(IMPL)));
}
#undef IMPL


#define IMPL ((gsf_map*) this->impl)
Typing Map::get_typing() const {
	return Typing((void*) handle_error(gsf_map_get_typing(IMPL)));
}

size_t Map::get_size() const {
	return gsf_map_get_size(IMPL);
}
void Map::resize(size_t size) {
	handle_error(gsf_map_resize(IMPL, size));
}

size_t Map::element_count() const {
	return gsf_map_element_count(IMPL);
}
gen::Optional<gen::String> Map::get_key(size_t index) const {
	auto* ret = gsf_map_get_key(IMPL, index);
	if (!ret) return Optional<String>();
	return Optional<String>(String(ret));
}
gen::Optional<Value> Map::get_value(size_t index) {
	auto* ret = gsf_map_get_value(IMPL, index);
	if (!ret) return Optional<Value>();
	return Optional<Value>(Value((void*) ret));
}
gen::Optional<Value> Map::get_value_at_key(const String& key) {
	auto* ret = gsf_map_get_value_at_key(IMPL, key.c_str());
	if (!ret) return Optional<Value>();
	return Optional<Value>(Value((void*) ret));
}
Value Map::add_element(const String& key) {
	return Value((void*) handle_error(gsf_map_add_element(IMPL, key.c_str())));
}
#undef IMPL


#define IMPL ((gsf_array*) this->impl)
Type Array::get_type() const {
	return Type((void*) handle_error(gsf_array_get_type(IMPL)));
}

size_t Array::get_size() const {
	return gsf_array_get_size(IMPL);
}
void Array::resize(size_t size) {
	handle_error(gsf_array_resize(IMPL, size));
}

int8_t* Array::as_i8() {
	return handle_error(gsf_array_as_i8(IMPL));
}
int16_t* Array::as_i16() {
	return handle_error(gsf_array_as_i16(IMPL));
}
int32_t* Array::as_i32() {
	return handle_error(gsf_array_as_i32(IMPL));
}
int64_t* Array::as_i64() {
	return handle_error(gsf_array_as_i64(IMPL));
}
uint8_t* Array::as_u8() {
	return handle_error(gsf_array_as_u8(IMPL));
}
uint16_t* Array::as_u16() {
	return handle_error(gsf_array_as_u16(IMPL));
}
uint32_t* Array::as_u32() {
	return handle_error(gsf_array_as_u32(IMPL));
}
uint64_t* Array::as_u64() {
	return handle_error(gsf_array_as_u64(IMPL));
}
float* Array::as_f32() {
	return handle_error(gsf_array_as_f32(IMPL));
}
double* Array::as_f64() {
	return handle_error(gsf_array_as_f64(IMPL));
}
#undef IMPL


#define IMPL ((gsf_value*) this->impl)
bool Value::is_set() const {
	return gsf_value_get_type(IMPL);
}
Typing Value::get_typing() const {
	return Typing((void*) handle_error(gsf_value_get_typing(IMPL)));
}
gen::Optional<Type> Value::get_type() const {
	auto* ret = gsf_value_get_type(IMPL);
	if (!ret) return Optional<Type>();
	return Optional<Type>(Type((void*) ret));
}

void Value::unset() {
	gsf_value_unset(IMPL);
}
void Value::copy(const Value& source) {
	handle_error(gsf_value_copy(IMPL, (gsf_value*) source.impl));
}

void Value::decode_null() const {
	handle_error(gsf_value_decode_null(IMPL));
}
void Value::encode_null() {
	handle_error(gsf_value_encode_null(IMPL));
}
Object Value::decode_object() const {
	return Object((void*) handle_error(gsf_value_decode_object(IMPL)));
}
Object Value::encode_object(const Class* type) {
	return Object((void*) handle_error(gsf_value_encode_object(IMPL, type ? (gsf_class*) type->impl : nullptr)));
}
Object Value::as_object(const Class* type) {
	return Object((void*) handle_error(gsf_value_as_object(IMPL, type ? (gsf_class*) type->impl : nullptr)));
}
Map Value::decode_map() const {
	return Map((void*) handle_error(gsf_value_decode_map(IMPL)));
}
Map Value::encode_map(const TypingRepr* typing, size_t size) {
	return Map((void*) handle_error(gsf_value_encode_map(IMPL, typing ? typing->get_impl() : nullptr, size)));
}
Map Value::as_map(const TypingRepr* typing, size_t size) {
	return Map((void*) handle_error(gsf_value_as_map(IMPL, typing ? typing->get_impl() : nullptr, size)));
}
List Value::decode_list() const {
	return List((void*) handle_error(gsf_value_decode_list(IMPL)));
}
List Value::encode_list(const TypingRepr* typing, size_t size) {
	return List((void*) handle_error(gsf_value_encode_list(IMPL, typing ? typing->get_impl() : nullptr, size)));
}
List Value::as_list(const TypingRepr* typing, size_t size) {
	return List((void*) handle_error(gsf_value_as_list(IMPL, typing ? typing->get_impl() : nullptr, size)));
}
Array Value::decode_array() const {
	return Array((void*) handle_error(gsf_value_decode_array(IMPL)));
}
Array Value::encode_array(const TypeRepr* type, size_t size) {
	return Array((void*) handle_error(gsf_value_encode_array(IMPL, type ? type->get_impl() : nullptr, size)));
}
Array Value::as_array(const TypeRepr* type, size_t size) {
	return Array((void*) handle_error(gsf_value_as_array(IMPL, type ? type->get_impl() : nullptr, size)));
}
Variant Value::decode_enum() const {
	return Variant((void*) handle_error(gsf_value_decode_enum(IMPL)));
}
void Value::encode_enum(const Enum* type, const Variant& variant) {
	handle_error(gsf_value_encode_enum(IMPL, type ? (gsf_enum*) type->impl : nullptr, (gsf_variant*) variant.impl));
}
gen::String Value::decode_enum_str() const {
	return String(handle_error(gsf_value_decode_enum_str(IMPL)));
}
void Value::encode_enum_str(const Enum* type, const String& variant) {
	handle_error(gsf_value_encode_enum_str(IMPL, type ? (gsf_enum*) type->impl : nullptr, variant.c_str()));
}
gen::String Value::decode_str() const {
	return String(handle_error(gsf_value_decode_str(IMPL)));
}
void Value::encode_str(const String& value) {
	handle_error(gsf_value_encode_str(IMPL, value.c_str()));
}
int8_t Value::decode_i8() const {
	int8_t out;
	gsf_value_decode_i8(IMPL, &out);
	return out;
}
void Value::encode_i8(int8_t value) {
	handle_error(gsf_value_encode_i8(IMPL, value));
}
int16_t Value::decode_i16() const {
	int16_t out;
	gsf_value_decode_i16(IMPL, &out);
	return out;
}
void Value::encode_i16(int16_t value) {
	handle_error(gsf_value_encode_i16(IMPL, value));
}
int32_t Value::decode_i32() const {
	int32_t out;
	gsf_value_decode_i32(IMPL, &out);
	return out;
}
void Value::encode_i32(int32_t value) {
	handle_error(gsf_value_encode_i32(IMPL, value));
}
int64_t Value::decode_i64() const {
	int64_t out;
	gsf_value_decode_i64(IMPL, &out);
	return out;
}
void Value::encode_i64(int64_t value) {
	handle_error(gsf_value_encode_i64(IMPL, value));
}
uint8_t Value::decode_u8() const {
	uint8_t out;
	gsf_value_decode_u8(IMPL, &out);
	return out;
}
void Value::encode_u8(uint8_t value) {
	handle_error(gsf_value_encode_u8(IMPL, value));
}
uint16_t Value::decode_u16() const {
	uint16_t out;
	gsf_value_decode_u16(IMPL, &out);
	return out;
}
void Value::encode_u16(uint16_t value) {
	handle_error(gsf_value_encode_u16(IMPL, value));
}
uint32_t Value::decode_u32() const {
	uint32_t out;
	gsf_value_decode_u32(IMPL, &out);
	return out;
}
void Value::encode_u32(uint32_t value) {
	handle_error(gsf_value_encode_u32(IMPL, value));
}
uint64_t Value::decode_u64() const {
	uint64_t out;
	gsf_value_decode_u64(IMPL, &out);
	return out;
}
void Value::encode_u64(uint64_t value) {
	handle_error(gsf_value_encode_u64(IMPL, value));
}
float Value::decode_f32() const {
	float out;
	gsf_value_decode_f32(IMPL, &out);
	return out;
}
void Value::encode_f32(float value) {
	handle_error(gsf_value_encode_f32(IMPL, value));
}
double Value::decode_f64() const {
	double out;
	gsf_value_decode_f64(IMPL, &out);
	return out;
}
void Value::encode_f64(double value) {
	handle_error(gsf_value_encode_f64(IMPL, value));
}
#undef IMPL



#define IMPL ((gsf_context*) this->impl)
Context::Context() : own(true) {
	this->impl = handle_error(gsf_context_new());
}
Context::~Context() {
	this->destroy();
}
void Context::destroy() {
	if (!this->own) return;
	gsf_delete_context(IMPL);
}

Schema Context::new_schema(const Descriptor& descriptor, const TypingRepr& root, size_t class_count) {
	return Schema((void*) handle_error(gsf_context_new_schema(IMPL, (gsf_descriptor*) descriptor.impl, root.get_impl(), class_count)));
}
Schema Context::new_schema(const String& descriptor_name, const TypingRepr& root, size_t class_count) {
	return Schema((void*) handle_error(gsf_context_new_schema_name(IMPL, descriptor_name.c_str(), root.get_impl(), class_count)));
}
size_t Context::schema_count() const {
	return gsf_context_schema_count(IMPL);
}
gen::Optional<Schema> Context::get_schema_by_idx(size_t index) {
	auto* ret = gsf_context_get_schema_by_idx(IMPL, index);
	if (!ret) return gen::Optional<Schema>();
	return gen::Optional<Schema>(ret);
}
gen::Optional<Schema> Context::get_schema(const Descriptor& descriptor) {
	auto* ret = gsf_context_get_schema(IMPL, (gsf_descriptor*) descriptor.impl);
	if (!ret) return gen::Optional<Schema>();
	return gen::Optional<Schema>(ret);
}
#undef IMPL

struct FileReaderData {
	gen::SeekableDataSource* file;
	gen::ByteBuffer buf;
};
static int reader_read_file(gsf_reader* reader, size_t count) {
	auto* data = (FileReaderData*) reader->user;
	size_t pos = (uint8_t*) reader->ptr - data->buf.ptr();
	if (count > data->file->size() - pos) count = data->file->size() - pos;
	data->file->seek((uint8_t*) reader->ptr - (uint8_t*) reader->end, gen::Seek::CUR);
	data->buf.clear();
	reader->ptr = data->buf.reserve(count);
	reader->end = ((uint8_t*) reader->ptr) + count;
	int ret = data->file->read((uint8_t*) reader->ptr, count);
	return ret;
}
static void reader_create_file(struct gsf_reader* reader, gen::SeekableDataSource* file) {
	reader->user = new FileReaderData { .file = file, .buf = gen::ByteBuffer() };
	reader->read = reader_read_file;
	reader->ptr = nullptr;
	reader->end = nullptr;
}
static void reader_delete_file(struct gsf_reader* reader) {
	delete (FileReaderData*) reader->user;
}

static int writer_write_string(struct gsf_writer* writer, size_t count) {
	auto* str = (gen::String*) writer->user;
	size_t pos = 0;
	if (writer->ptr) pos = (char*) writer->ptr - str->data();
	str->reserve(count - ((char*) writer->end - (char*) writer->ptr));
	writer->ptr = str->data(pos);
	writer->end = str->data(str->size() - 1);
	return 1;
}
static void writer_create_string(struct gsf_writer* writer, gen::String* string) {
	writer->user = string;
	writer->write = writer_write_string;
	writer->ptr = nullptr;
	writer->end = nullptr;
}
static void writer_delete_string(struct gsf_writer* writer) {
}

static int writer_write_buf(struct gsf_writer* writer, size_t count) {
	auto* str = (gen::ByteBuffer*) writer->user;
	size_t pos = 0;
	if (writer->ptr) pos = (uint8_t*) writer->ptr - str->ptr();
	str->reserve(count - ((char*) writer->end - (char*) writer->ptr));
	writer->ptr = str->ptr(pos);
	writer->end = str->get_last_ptr();
	return 1;
}
static void writer_create_buf(struct gsf_writer* writer, gen::ByteBuffer* buf) {
	writer->user = buf;
	writer->write = writer_write_buf;
	writer->ptr = nullptr;
	writer->end = nullptr;
}
static void writer_delete_buf(struct gsf_writer* writer) {
}

Schema Schema::txt_read_to_ctx(Context ctx, const String& string, int flags) {
	struct gsf_reader reader = {};
	reader.format = GSF_CODEC_TEXTUAL;
	reader.flags = flags;
	gsf_reader_create_buf(&reader, string.c_str(), string.size() + 1);
	int ret = gsf_read_schema_to_ctx(&reader, (gsf_context*) ctx.impl);
	gsf_reader_delete_buf(&reader);
	if (!ret) throw_error();
	return ctx.get_schema_by_idx(ret - 1).get();
}
Schema Schema::txt_read_to_ctx(Context ctx, const char* buf, size_t buf_size, int flags) {
	struct gsf_reader reader = {};
	reader.format = GSF_CODEC_TEXTUAL;
	reader.flags = flags;
	gsf_reader_create_buf(&reader, buf, buf_size);
	int ret = gsf_read_schema_to_ctx(&reader, (gsf_context*) ctx.impl);
	gsf_reader_delete_buf(&reader);
	if (!ret) throw_error();
	return ctx.get_schema_by_idx(ret - 1).get();
}
Schema Schema::txt_read_to_ctx(Context ctx, SeekableDataSource* data, int flags) {
	struct gsf_reader reader = {};
	reader.format = GSF_CODEC_TEXTUAL;
	reader.flags = flags;
	reader_create_file(&reader, data);
	int ret = gsf_read_schema_to_ctx(&reader, (gsf_context*) ctx.impl);
	reader_delete_file(&reader);
	if (!ret) throw_error();
	return ctx.get_schema_by_idx(ret - 1).get();
}
Schema Schema::txt_read(const String& string, int flags) {
	return txt_read(string.c_str(), string.size());
}
Schema Schema::txt_read(const char* buf, size_t buf_size, int flags) {
	gsf_schema* schema = gsf_create_schema();
	if (!schema) throw_error();
	struct gsf_reader reader = {};
	reader.format = GSF_CODEC_TEXTUAL;
	reader.flags = flags;
	gsf_reader_create_buf(&reader, buf, buf_size);
	int ret = gsf_read_schema(&reader, schema, nullptr);
	gsf_reader_delete_buf(&reader);
	if (!ret) {
		gsf_delete_schema(schema);
		throw_error();
	}
	return Schema((void*) schema, true);
}
Schema Schema::txt_read(SeekableDataSource* data, int flags) {
	size_t size = data->size();
	char* buf = gen::talloc<char>(size);
	data->read(buf, size);
	Schema ret = txt_read(buf, size, flags);
	free(buf);
	return ret;
}
gen::String Schema::txt_write(const Schema& schema, int flags) {
	struct gsf_writer writer = {};
	writer.format = GSF_CODEC_TEXTUAL;
	writer.flags = flags;
	String str;
	writer_create_string(&writer, &str);
	int ret = gsf_write_schema(&writer, (gsf_schema*) schema.impl);
	writer_delete_string(&writer);
	if (!ret) throw_error();
	return str;
}
char* Schema::txt_write(const Schema& schema, char* buf, size_t buf_size, int flags) {
	struct gsf_writer writer = {};
	writer.format = GSF_CODEC_TEXTUAL;
	writer.flags = flags;
	gsf_writer_create_buf(&writer, buf, buf_size);
	int ret = gsf_write_schema(&writer, (gsf_schema*) schema.impl);
	gsf_writer_delete_buf(&writer);
	if (!ret) throw_error();
	return (char*) writer.ptr;
}
void Schema::txt_write(const Schema& schema, SeekableWritableDataSource* data, int flags) {
	String str = txt_write(schema, flags);
	data->write(str.data(), str.size());
}

Article Article::bin_read_with_desc(void* user, Schema (*schema_getter)(void* user, const Descriptor& descriptor), const ByteBuffer& data, int flags) {
	return bin_read_with_desc(user, schema_getter, data.ptr(), data.size(), flags);
}
Article Article::bin_read_with_desc(void* user, Schema (*schema_getter)(void* user, const Descriptor& descriptor), const void* buf, size_t buf_size, int flags) {
	struct gsf_reader reader = {};
	reader.format = GSF_CODEC_BINARY;
	reader.flags = flags;
	gsf_reader_create_buf(&reader, buf, buf_size);
	gsf_descriptor* descriptor = gsf_create_descriptor();
	int ret = gsf_read_article_desc(&reader, descriptor);
	if (!ret) {
		gsf_reader_delete_buf(&reader);
		gsf_delete_descriptor(descriptor);
		throw_error();
	}
	Schema schema = Schema(nullptr);
	if (*gsf_descriptor_get_name(descriptor) != '\0') {
		schema = schema_getter(user, Descriptor(descriptor));
	}
	gsf_article* article = gsf_create_article();
	ret = gsf_read_article_proper(&reader, article, (gsf_schema*) schema.impl);
	gsf_delete_descriptor(descriptor);
	gsf_reader_delete_buf(&reader);
	if (!ret) {
		gsf_delete_article(article);
		throw_error();
	}
	return Article((void*) article, true);
}
Article Article::bin_read_with_desc(void* user, Schema (*schema_getter)(void* user, const Descriptor& descriptor), SeekableDataSource* data, int flags) {
	struct gsf_reader reader = {};
	reader.format = GSF_CODEC_BINARY;
	reader.flags = flags;
	reader_create_file(&reader, data);
	gsf_descriptor* descriptor = gsf_create_descriptor();
	int ret = gsf_read_article_desc(&reader, descriptor);
	if (!ret) {
		gsf_reader_delete_buf(&reader);
		gsf_delete_descriptor(descriptor);
	}
	Schema schema = Schema(nullptr);
	if (*gsf_descriptor_get_name(descriptor) != '\0') {
		schema = schema_getter(user, Descriptor(descriptor));
	}
	gsf_article* article = gsf_create_article();
	ret = gsf_read_article_proper(&reader, article, (gsf_schema*) schema.impl);
	gsf_delete_descriptor(descriptor);
	reader_delete_file(&reader);
	if (!ret) {
		gsf_delete_article(article);
		throw_error();
	}
	return Article((void*) article, true);
}
Article Article::bin_read(const Schema& schema, const ByteBuffer& data, int flags) {
	return bin_read(schema, data.ptr(), data.size(), flags);
}
Article Article::bin_read(const Schema& schema, const void* buf, size_t buf_size, int flags) {
	gsf_article* article = gsf_create_article();
	struct gsf_reader reader = {};
	reader.format = GSF_CODEC_BINARY;
	reader.flags = flags;
	gsf_reader_create_buf(&reader, buf, buf_size);
	int ret = gsf_read_article(&reader, article, (gsf_schema*) schema.impl);
	gsf_reader_delete_buf(&reader);
	if (!ret) {
		gsf_delete_article(article);
		throw_error();
	}
	return Article((void*) article, true);
}
Article Article::bin_read(const Schema& schema, SeekableDataSource* data, int flags) {
	gsf_article* article = gsf_create_article();
	struct gsf_reader reader = {};
	reader.format = GSF_CODEC_BINARY;
	reader.flags = flags;
	reader_create_file(&reader, data);
	int ret = gsf_read_article(&reader, article, (gsf_schema*) schema.impl);
	reader_delete_file(&reader);
	if (!ret) {
		gsf_delete_article(article);
		throw_error();
	}
	return Article((void*) article, true);
}
Article Article::bin_read_ctx(const Context& ctx, const ByteBuffer& data, int flags) {
	return bin_read_ctx(ctx, data.ptr(), data.size(), flags);
}
Article Article::bin_read_ctx(const Context& ctx, const void* buf, size_t buf_size, int flags) {
	gsf_article* article = gsf_create_article();
	struct gsf_reader reader = {};
	reader.format = GSF_CODEC_BINARY;
	reader.flags = flags;
	gsf_reader_create_buf(&reader, buf, buf_size);
	int ret = gsf_read_article_ctx(&reader, article, (gsf_context*) ctx.impl);
	gsf_reader_delete_buf(&reader);
	if (!ret) {
		gsf_delete_article(article);
		throw_error();
	}
	return Article((void*) article, true);
}
Article Article::bin_read_ctx(const Context& ctx, SeekableDataSource* data, int flags) {
	gsf_article* article = gsf_create_article();
	struct gsf_reader reader = {};
	reader.format = GSF_CODEC_BINARY;
	reader.flags = flags;
	reader_create_file(&reader, data);
	int ret = gsf_read_article_ctx(&reader, article, (gsf_context*) ctx.impl);
	reader_delete_file(&reader);
	if (!ret) {
		gsf_delete_article(article);
		throw_error();
	}
	return Article((void*) article, true);
}
gen::ByteBuffer Article::bin_write(const Article& article, int flags) {
	struct gsf_writer writer = {};
	writer.format = GSF_CODEC_BINARY;
	writer.flags = flags;
	ByteBuffer buf;
	writer_create_buf(&writer, &buf);
	int ret = gsf_write_article(&writer, (gsf_article*) article.impl);
	if (!ret) {
		writer_delete_buf(&writer);
		throw_error();
	}
	buf.resize((uint8_t*) writer.ptr - buf.ptr());
	writer_delete_buf(&writer);
	return buf;
}
uint8_t* Article::bin_write(const Article& article, uint8_t* buf, size_t buf_size, int flags) {
	struct gsf_writer writer = {};
	writer.format = GSF_CODEC_BINARY;
	writer.flags = flags;
	gsf_writer_create_buf(&writer, buf, buf_size);
	int ret = gsf_write_article(&writer, (gsf_article*) article.impl);
	gsf_writer_delete_buf(&writer);
	if (!ret) throw_error();
	return (uint8_t*) writer.ptr;
}
void Article::bin_write(const Article& article, SeekableWritableDataSource* data, int flags) {
	ByteBuffer buf = bin_write(article, flags);
	data->write(buf.ptr(), buf.size());
}
Article Article::txt_read(const Schema& schema, const String& string, int flags) {
	return txt_read(schema, string.c_str(), string.size(), flags);
}
Article Article::txt_read(const Schema& schema, const char* buf, size_t buf_size, int flags) {
	gsf_article* article = gsf_create_article();
	struct gsf_reader reader = {};
	reader.format = GSF_CODEC_TEXTUAL;
	reader.flags = flags;
	gsf_reader_create_buf(&reader, buf, buf_size);
	int ret = gsf_read_article(&reader, article, (gsf_schema*) schema.impl);
	gsf_reader_delete_buf(&reader);
	if (!ret) {
		gsf_delete_article(article);
		throw_error();
	}
	return Article((void*) article, true);
}
Article Article::txt_read(const Schema& schema, SeekableDataSource* data, int flags) {
	gsf_article* article = gsf_create_article();
	struct gsf_reader reader = {};
	reader.format = GSF_CODEC_TEXTUAL;
	reader.flags = flags;
	reader_create_file(&reader, data);
	int ret = gsf_read_article(&reader, article, (gsf_schema*) schema.impl);
	reader_delete_file(&reader);
	if (!ret) {
		gsf_delete_article(article);
		throw_error();
	}
	return Article((void*) article, true);
}
Article Article::txt_read_ctx(const Context& ctx, const String& string, int flags) {
	return txt_read_ctx(ctx, string.c_str(), string.size(), flags);
}
Article Article::txt_read_ctx(const Context& ctx, const char* buf, size_t buf_size, int flags) {
	gsf_article* article = gsf_create_article();
	struct gsf_reader reader = {};
	reader.format = GSF_CODEC_TEXTUAL;
	reader.flags = flags;
	gsf_reader_create_buf(&reader, buf, buf_size);
	int ret = gsf_read_article_ctx(&reader, article, (gsf_context*) ctx.impl);
	gsf_reader_delete_buf(&reader);
	if (!ret) {
		gsf_delete_article(article);
		throw_error();
	}
	return Article((void*) article, true);
}
Article Article::txt_read_ctx(const Context& ctx, SeekableDataSource* data, int flags) {
	gsf_article* article = gsf_create_article();
	struct gsf_reader reader = {};
	reader.format = GSF_CODEC_TEXTUAL;
	reader.flags = flags;
	reader_create_file(&reader, data);
	int ret = gsf_read_article_ctx(&reader, article, (gsf_context*) ctx.impl);
	reader_delete_file(&reader);
	if (!ret) {
		gsf_delete_article(article);
		throw_error();
	}
	return Article((void*) article, true);
}
gen::String Article::txt_write(const Article& article, int flags) {
	struct gsf_writer writer = {};
	writer.format = GSF_CODEC_TEXTUAL;
	writer.flags = flags;
	String str;
	writer_create_string(&writer, &str);
	int ret = gsf_write_article(&writer, (gsf_article*) article.impl);
	writer_delete_string(&writer);
	if (!ret) throw_error();
	return str;
}
char* Article::txt_write(const Article& article, char* buf, size_t buf_size, int flags) {
	struct gsf_writer writer = {};
	writer.format = GSF_CODEC_TEXTUAL;
	writer.flags = flags;
	gsf_writer_create_buf(&writer, buf, buf_size);
	int ret = gsf_write_article(&writer, (gsf_article*) article.impl);
	gsf_writer_delete_buf(&writer);
	if (!ret) throw_error();
	return (char*) writer.ptr;
}
void Article::txt_write(const Article& article, SeekableWritableDataSource* data, int flags) {
	String str = txt_write(article, flags);
	data->write(str.data(), str.size());
}
