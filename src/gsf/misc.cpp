// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/gsf/error.hpp>

#include <gsf_com.h>
#include <gsf_err.h>

void gen_init_gsf() {
	gsf_set_allocator(gen::alloc, gen::realloc, gen::free);
}

void gen::gsf::throw_error(void* user) {
	int code = gsf_errno;
	if (code == gsf_enone) return;
	throw GsfException::createf("(#%i) %s: %s", code, gsf_errno_desc(code), gsf_errno_msg);
}
