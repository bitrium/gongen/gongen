// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/resources/manager.hpp>

#include <atomic>
#include <gongen/common/containers/pointers.hpp>
#include <gongen/common/containers/hashmap.hpp>
#include <gongen/common/containers/lockless_queue.hpp>
#include <gongen/common/hash.hpp>
#include <gongen/common/logger.hpp>
#include <gongen/resources/kinds.hpp>

#define GEN_RES_REQUEUE_LIMIT 16

using namespace gen;
using namespace gen::res;

LoadableFile AbsLoadableFile::get(const Identifier& id, int priority) {
	return LoadableFile {
		.id = id,
		.priority = priority,
		.user = this,
		.read = [](void* user) { return ((AbsLoadableFile*) user)->read(); },
		.done = [](void* user) { delete (AbsLoadableFile*) user; },
	};
}

struct ManualRes {
	Identifier id;
	void* resource;
};
struct LoadImpl {
	HashMap<String, Buffer<LoadableFile>, 32> files_by_kind = HashMap<String, Buffer<LoadableFile>, 32>(gen::hash);
	HashMap<Identifier, LoadableFile*> file_by_id = HashMap<Identifier, LoadableFile*>(Identifier::hash);
	HashMap<Identifier, ManualRes> manual = HashMap<Identifier, ManualRes>(Identifier::hash);
};

LoadConfig::LoadConfig() {
	this->impl = new LoadImpl();
}
LoadConfig::~LoadConfig() {
	delete (LoadImpl*) this->impl;
}
bool LoadConfig::add_file(LoadableFile file) {
	auto* impl = (LoadImpl*) this->impl;
	file.id.require_full_ext();
	if (!impl->file_by_id.contains(file.id)) {
		LoadableFile* listed_file = impl->files_by_kind[file.id.kind].add(file);
		impl->file_by_id.add(file.id, listed_file);
	} else {
		LoadableFile* other = impl->file_by_id.get(file.id);
		if (file.priority < other->priority) return false;
		*other = file; // replace file info
	}
	return true;
}
void LoadConfig::add_resource(Identifier id, void* resource) {
	auto* impl = (LoadImpl*) this->impl;
	id.require_full();
	id.ext.clear();
	impl->manual[id] = ManualRes { .id = id, .resource = resource };
}

struct Entry {
	Identifier id;
	uint64_t data_hash;
	void* item;
	bool reused = false;
};
struct LoadEntry {
	Identifier id;
	void* manual;
	HashMap<String, LoadableFile, 16> files = HashMap<String, LoadableFile, 16>(gen::hash);
	uint16_t requeued = 0;
};
struct Resources {
	HashMap<Identifier, Entry> map = HashMap<Identifier, Entry>(Identifier::hash);
	HashMap<String, DynamicArray<Entry*>, 32> by_kind = HashMap<String, DynamicArray<Entry*>, 32>(gen::hash);
};
struct ManagerImpl {
	LinkedList<SharedPtr<KindHandler>> kinds;
	HashMap<String, KindHandler*, 32> kind_by_id = HashMap<String, KindHandler*, 32>(gen::hash);
	std::atomic<bool> is_busy = false;
	Resources resources_[2];
	std::atomic<Resources*> resources = &resources_[0];

	HashMap<Identifier, LoadEntry> loading_map = HashMap<Identifier, LoadEntry>(Identifier::hash);
	TypedLocklessQueue<LoadEntry> loading_queue;

	Optional<job::Handle> jobs_handle;

	String default_ns = "";

	HashMap<Identifier, LoadableFile> keep_open = HashMap<Identifier, LoadableFile>(Identifier::hash);
};

Manager::Manager() {
	this->impl = new ManagerImpl();
}
Manager::~Manager() {
	auto* impl = (ManagerImpl*) this->impl;
	if (impl->jobs_handle.some()) impl->jobs_handle->join();
	impl->is_busy = false;
	this->cleanup();
	delete impl;
}

void destroy_entries(Resources* resources, HashMap<String, KindHandler*, 32>* kinds, HashMap<Identifier, LoadableFile>* keep_open) {
	for (Entry& entry : resources->map) {
		if (entry.reused) continue;
		KindHandler* handler = kinds->get(entry.id.kind);
		for (KindFile kind_file : handler->get_files()) {
			Identifier file = entry.id;
			file.ext = kind_file.extension;
			if (!keep_open->contains(file)) continue;
			keep_open->get(file).done(keep_open->get(file).user);
		}
		handler->destroy(&entry.item);
	}
	resources->map.clear();
	resources->by_kind.clear();
}

void Manager::empty() {
	auto* impl = (ManagerImpl*) this->impl;
	if (impl->is_busy) throw ThreadingException("Emptying during loading is forbidden.");
	impl->is_busy = true;
	Resources* resources = &impl->resources_[0];
	if (resources == impl->resources) resources = &impl->resources_[1];
	destroy_entries(resources, &impl->kind_by_id, &impl->keep_open);
	impl->resources = resources;
	impl->is_busy = false;
}

void Manager::cleanup() {
	auto* impl = (ManagerImpl*) this->impl;
	if (impl->is_busy) throw ThreadingException("Cleanup during loading is forbidden.");
	impl->is_busy = true;
	destroy_entries(&impl->resources_[0], &impl->kind_by_id, &impl->keep_open);
	destroy_entries(&impl->resources_[1], &impl->kind_by_id, &impl->keep_open);
	impl->kind_by_id.clear();
	impl->kinds.clear();
	impl->is_busy = false;
}

void Manager::add_kind(const SharedPtr<KindHandler>& handler) {
	auto* impl = (ManagerImpl*) this->impl;
	if (impl->is_busy) throw ThreadingException("Adding kinds during loading is forbidden.");
	impl->is_busy = true;
	if (impl->kind_by_id.contains(handler->get_id())) throw KeyException::createf("Kind already exists '%s'.", handler->get_id().c_str());
	impl->kind_by_id.add(handler->get_id(), impl->kinds.add(handler).get());
	impl->is_busy = false;
}

void Manager::set_default_ns(const String& ns) {
	auto* impl = (ManagerImpl*) this->impl;
	impl->default_ns = ns;
}

UniversalIterable<Identifier> Manager::list(String kind) {
	auto* impl = (ManagerImpl*) this->impl;
	if (!impl->resources.load()->by_kind.contains(kind)) {
		return UniversalIterable<Identifier>(new EmptyIterable<Identifier>());
	}
	return UniversalIterable<Identifier>(new ConvertIterable<Identifier, Entry*>([](Entry** entry) {
		return &(*entry)->id;
	}, &impl->resources.load()->by_kind.get(kind), false));
}
UniversalIterable<Identifier> Manager::list_all() {
	auto* impl = (ManagerImpl*) this->impl;
	return UniversalIterable<Identifier>(new ConvertIterable<Identifier, Entry>([](Entry* entry) {
		return &entry->id;
	}, &impl->resources.load()->map, false));
}
bool Manager::has(const Identifier& id) {
	auto* impl = (ManagerImpl*) this->impl;
	return impl->resources.load()->map.contains(id);
}
bool Manager::has(const String& kind, const String& ns, const String& name) {
	return this->has(Identifier(kind, ns, name, ""));
}
bool Manager::has(const String& kind, const String& name) {
	auto* impl = (ManagerImpl*) this->impl;
	return this->has(Identifier(kind, impl->default_ns, name, ""));
}
void* Manager::get(const String& kind, uint64_t id_hash) {
	auto* impl = (ManagerImpl*) this->impl;
	return impl->kind_by_id.get(kind)->get_res(&impl->resources.load()->map.get_for_hash(id_hash)->item);
}
void* Manager::get(const Identifier& id_orig) {
	auto* impl = (ManagerImpl*) this->impl;
	Identifier id = id_orig.expanded("", impl->default_ns);
	return impl->kind_by_id.get(id.kind)->get_res(&impl->resources.load()->map.get(id).item);
}
void* Manager::get(const String& kind, const String& ns, const String& name) {
	return this->get(Identifier(kind, ns, name, ""));
}
void* Manager::get(const String& kind, const String& name) {
	auto* impl = (ManagerImpl*) this->impl;
	return this->get(Identifier(kind, impl->default_ns, name, ""));
}

void fill_loading_queue(ManagerImpl* impl, LoadImpl* load) {
	impl->loading_map.clear();
	for (ManualRes& manual : load->manual) {
		impl->loading_queue.enqueue(&impl->loading_map.emplace(manual.id, manual.id, manual.resource));
	}
	for (SharedPtr<KindHandler>& kind : impl->kinds) {
		if (!load->files_by_kind.contains(kind->get_id())) continue;
		Buffer<LoadableFile>& files = load->files_by_kind.get(kind->get_id());
		for (LoadableFile& file : files) {
			Identifier id = file.id;
			id.ext.clear();
			LoadEntry* entry;
			if (!impl->loading_map.contains(id)) {
				entry = &impl->loading_map.emplace(id, id);
				impl->loading_queue.enqueue(entry);
			} else {
				entry = &impl->loading_map.get(id);
				if (entry->manual) continue;
			}
			entry->files.add(file.id.ext, file);
		}
	}
}

Resources* load_begin(ManagerImpl* impl, LoadImpl* load) {
	fill_loading_queue(impl, load);
	Resources* resources = &impl->resources_[0];
	if (resources == impl->resources) resources = &impl->resources_[1];
	destroy_entries(resources, &impl->kind_by_id, &impl->keep_open);
	return resources;
}
LoadEntry* load_entry(ManagerImpl* impl, Resources* resources, LoadEntry* load_entry) {
	GEN_ASSERT(impl->kind_by_id.contains(load_entry->id.kind), "loading queue fill accepted unknown kind");
	KindHandler* handler = impl->kind_by_id.get(load_entry->id.kind);
	Entry* entry;
	if (load_entry->manual) {
		entry = &resources->map.add(load_entry->id, Entry {
			.id = load_entry->id,
			.data_hash = 0,
			.item = load_entry->manual,
		});
	} else {
		uint64_t hash = 0;
		void* item = &load_entry->id;
		try {
			handler->create(&item);
		} catch (Exception& exception) {
			log::errorf("Could not load resource '%s' due to exception during creation: %s", Identifier::to_str(load_entry->id).c_str(), exception.what());
			return nullptr;
		}
		bool allow_reuse = true;
		ByteBuffer hash_buf;
		for (KindFile& kind_file : handler->get_files()) {
			if (kind_file.keep_open) {
				allow_reuse = false;
				break;
			}
			if (!load_entry->files.contains(kind_file.extension)) {
				if (kind_file.is_optional) continue;
				log::errorf("Could not load resource '%s' due to missing required file with extension '%s'", Identifier::to_str(load_entry->id).c_str(), kind_file.extension.c_str());
				handler->cancel(&item);
				return nullptr;
			}
			LoadableFile& file = load_entry->files.get(kind_file.extension);
			// TODO "vfs" support as alternative to data
			SeekableDataSource* data;
			try {
				data = file.read(file.user);
			} catch (Exception& exception) {
				log::errorf("Could not load file '%s' due to exception during file read: %s", Identifier::to_str(file.id).c_str(), exception.what());
				handler->cancel(&item);
				return nullptr;
			}
			if (kind_file.is_complex) {
				Buffer<Identifier> deps;
				try {
					gen::hash_combine_ref(hash, kind_file.prepare.data(handler, file.id, &item, data, deps));
				} catch (Exception& exception) {
					log::errorf("Could not load file '%s' due to exception in preparation function: %s", Identifier::to_str(file.id).c_str(), exception.what());
					handler->cancel(&item);
					return nullptr;
				}
				for (Identifier& dep : deps) {
					if (resources->map.contains(dep)) continue;
					if (load_entry->requeued >= GEN_RES_REQUEUE_LIMIT) {
						log::errorf("Could not load file '%s' due to it being requeued too many times (recursive resource dependency?).", Identifier::to_str(file.id).c_str());
					}
					// dep is missing but could be later in queue; requeue this
					handler->cancel(&item);
					load_entry->requeued++;
					return load_entry;
				}
			} else {
				try {
					size_t old_cur = data->cursor();
					data->seek(0);
					size_t size = data->size();
					data->read(hash_buf.reserve(size), size);
					gen::hash_combine_ref(hash, gen::hash(hash_buf));
					data->seek(old_cur);
				} catch (Exception& exception) {
					log::errorf("Could not load file '%s' due to exception during hashing: %s", Identifier::to_str(file.id).c_str(), exception.what());
					handler->cancel(&item);
					return nullptr;
				}
			}
		}
		Entry* old_entry = allow_reuse ? impl->resources.load()->map.ptr(load_entry->id) : nullptr;
		if (old_entry && hash == old_entry->data_hash) {
			handler->cancel(&item);
			item = old_entry->item;
			handler->reaccept(&item);
			old_entry->reused = true;
			goto ITEM_DONE;
		}
		for (KindFile& kind_file : handler->get_files()) {
			if (!load_entry->files.contains(kind_file.extension)) continue;
			LoadableFile& file = load_entry->files.get(kind_file.extension);
			SeekableDataSource* data;
			try {
				data = file.read(file.user);
			} catch (Exception& exception) {
				log::errorf("Could not load file '%s' due to exception during file read: %s", Identifier::to_str(file.id).c_str(), exception.what());
				handler->cancel(&item);
				return nullptr;
			}
			try {
				kind_file.load.data(handler, file.id, &item, data);
			} catch (Exception& exception) {
				// delete data;
				log::errorf("Could not load file '%s' due to exception in load function: %s", Identifier::to_str(file.id).c_str(), exception.what());
				handler->cancel(&item);
				return nullptr;
			}
			// delete data;
			if (kind_file.keep_open) {
				impl->keep_open.add(file.id, file);
			}
		}
		try {
			handler->finish(&item);
		} catch (Exception& exception) {
			log::errorf("Could not load resource '%s' due to exception in finish function: %s", Identifier::to_str(load_entry->id).c_str(), exception.what());
			handler->cancel(&item); // FIXME close keep_open files
			return nullptr;
		}
		ITEM_DONE:
		entry = &resources->map.add(load_entry->id, Entry {
			.id = load_entry->id,
			.data_hash = hash,
			.item = item,
		});
	}
	resources->by_kind[entry->id.kind].add(entry);
	handler->accept(&entry->item);
	return nullptr;
}
void load_end(ManagerImpl* impl, Resources* resources) {
	for (LoadEntry& entry : impl->loading_map) {
		for (LoadableFile& file : entry.files) {
			if (!impl->keep_open.contains(file.id)) file.done(file.user);
		}
	}
	impl->loading_map.clear();
	impl->resources = resources;
}

bool Manager::busy() const {
	auto* impl = (ManagerImpl*) this->impl;
	return impl->is_busy;
}
void Manager::load(const LoadConfig& config, bool clear) {
	if (!clear) throw NoImplException("Adding to resource manager without clearing is not supported yet."); // TODO
	auto* impl = (ManagerImpl*) this->impl;
	if (impl->is_busy) throw BusyException("Resources are already being loaded.");
	impl->is_busy = true;
	auto* load = (LoadImpl*) config.impl;
	Resources* resources = load_begin(impl, load);
	while (LoadEntry* entry = impl->loading_queue.dequeue()) {
		if ((entry = load_entry(impl, resources, entry))) {
			impl->loading_queue.enqueue(entry);
		}
	}
	load_end(impl, resources);
	impl->is_busy = false;
}
job::Handle Manager::load_job(job::System& jobs, const LoadConfig& config, bool clear) {
	if (!clear) throw NoImplException("Adding to resource manager without clearing is not supported yet."); // TODO
	auto* impl = (ManagerImpl*) this->impl;
	if (impl->is_busy) throw BusyException("Resources are already being loaded.");
	impl->is_busy = true;
	auto* load = (LoadImpl*) config.impl;
	load_begin(impl, load);
	// each job is responsible for one entry unless its requeued – then it takes another
	impl->jobs_handle.set(jobs.add_jobs([](void* user, size_t) {
		auto* impl = (ManagerImpl*) user;
		Resources* resources = &impl->resources_[0];
		if (resources == impl->resources) resources = &impl->resources_[1];
		while (true) {
			LoadEntry* entry = impl->loading_queue.dequeue_wait();
			entry = load_entry(impl, resources, entry);
			if (!entry) break;
			else {
				impl->loading_queue.enqueue(entry);
			}
		}
		return (void*) nullptr;
	}, impl, impl->loading_queue.size(), [](void* user) {
		auto* impl = (ManagerImpl*) user;
		Resources* resources = &impl->resources_[0];
		if (resources == impl->resources) resources = &impl->resources_[1];
		load_end(impl, resources);
		impl->is_busy = false;
	}));
	return impl->jobs_handle.get();
}
