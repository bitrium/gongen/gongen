// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/resources/meta.hpp>
#include <gongen/common/error.hpp>
#include <gongen/common/hash.hpp>

using namespace gen;
using namespace gen::res;

uint64_t Identifier::hash(const Identifier& id) {
	return gen::hash_combine_n(4, gen::hash(id.kind), gen::hash(id.ns), gen::hash(id.name), gen::hash(id.ext));
}

Identifier Identifier::from_path(const String& path) {
	throw NoImplException("");
}
Identifier Identifier::from_path(const AbstractPath& path) {
	throw NoImplException("");
}

Identifier Identifier::from_str(const String& str) {
	Identifier id;
	DynamicArray<String> split = str.split(":");
	if (split.size() > 3) throw IdentifierException("Identifier string has to many components.");
	GEN_ASSERT(!split.empty(), "split always returns at least 1 elem");
	if (split.size() < 2) {
		id.name = split[0];
	} else if (split.size() < 3) {
		id.ns = split[0];
		id.name = split[1];
	} else {
		id.kind = split[0];
		id.ns = split[1];
		id.name = split[2];
	}
	split = id.name.split(".");
	if (split.size() <= 1) return id;
	id.ext = id.name.segmented(split[0].size() + 1);
	id.name = split[0];
	return id;
}
String Identifier::to_str(const Identifier& id) {
	if (id.name.empty()) throw IdentifierException("Cannot convert deficient identifier to string (missing name).");
	String str;
	if (!id.kind.empty()) {
		str.append(id.kind);
		if (!id.ns.empty()) str.append(":");
		else str.append("::");
	}
	if (!id.ns.empty()) {
		str.append(id.ns);
		str.append(":");
	}
	str.append(id.name);
	if (!id.ext.empty()) {
		str.append(".");
		str.append(id.ext);
	}
	return str;
}
Identifier Identifier::from_str_ex(const String& str, String kind, String ns) {
	return Identifier::from_str(str).expanded(kind, ns);
}
String Identifier::to_str_ex(const Identifier& id, String kind, String ns) {
	return Identifier::to_str(id.expanded(kind, ns));
}

bool Identifier::is_empty() const {
	return this->kind.empty() && this->ns.empty() && this->name.empty() && this->ext.empty();
}
void Identifier::clear() {
	this->kind.clear();
	this->ns.clear();
	this->name.clear();
	this->ext.clear();
}

bool Identifier::verification() const {
	return true; // TODO
}
void Identifier::verify() const {
	if (this->verification()) return;
	throw IdentifierException("Identifier has invalid characters.");
}

Identifier Identifier::expanded(String kind, String ns) const {
	Identifier id = *this;
	id.expand(kind, ns);
	return id;
}
Identifier& Identifier::expand(String kind, String ns) {
	if (this->kind.empty()) this->kind = kind;
	if (this->ns.empty()) this->ns = ns;
	return *this;
}

bool Identifier::is_full_ext() const {
	return !this->kind.empty() && !this->ns.empty() && !this->name.empty() && !this->ext.empty();
}
bool Identifier::is_full() const {
	return !this->kind.empty() && !this->ns.empty() && !this->name.empty();
}
void Identifier::require_full_ext() const {
	if (this->is_full_ext()) return;
	throw IdentifierException::createf("Identifier '%s' is not complete (with extension).", Identifier::to_str(*this).c_str());
}
void Identifier::require_full() const {
	if (this->is_full()) return;
	throw IdentifierException::createf("Identifier '%s' is not complete.", Identifier::to_str(*this).c_str());
}
