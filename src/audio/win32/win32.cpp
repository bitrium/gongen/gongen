// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/audio.hpp>
#include <mmdeviceapi.h>
#include <audioclient.h>
#include <functiondiscoverykeys_devpkey.h>
#include <audiopolicy.h>
#include <gongen/common/error.hpp>
#include <gongen/common/codecs.hpp>
#include <gongen/common/platform/system.hpp>

class WasapiDevice :public gen::audio::Device {
	IMMDevice* device;

	gen::audio::FormatScope format_scope;
	gen::String name;
	gen::String id;
	bool input_support;
	bool output_support;
	bool real;
public:
	void check_formatSupport(IAudioClient* client, uint8_t bit_depth, uint32_t sample_rate, uint8_t channels) {
		WAVEFORMATEX format;
		format.wFormatTag = WAVE_FORMAT_PCM;
		format.nChannels = channels;
		format.nSamplesPerSec = sample_rate;
		format.wBitsPerSample = bit_depth;
		format.nBlockAlign = ((format.wBitsPerSample * format.nChannels) / 8);
		format.nAvgBytesPerSec = (format.nSamplesPerSec * format.nBlockAlign);
		format.cbSize = 0;
		WAVEFORMATEX* match_format;
		HRESULT result = client->IsFormatSupported(AUDCLNT_SHAREMODE_SHARED, &format, &match_format);
		if (result == S_OK) {
			format_scope.bit_depth.val.select.add(bit_depth);
			format_scope.sample_rate.val.select.add(sample_rate);
			format_scope.channel_count.val.select.add(channels);
		}
	}

	WasapiDevice(IMMDevice* device) {
		IPropertyStore* properties;
		device->OpenPropertyStore(STGM_READ, &properties);
		PROPVARIANT property;
		properties->GetValue(PKEY_Device_DeviceDesc, &property);
		name = gen::utf::utf16to8((const char16_t*)property.pwszVal, 0);
		properties->Release();
		real = true;

		IMMEndpoint* endpoint;
		device->QueryInterface(IID_PPV_ARGS(&endpoint));
		EDataFlow data_flow;
		endpoint->GetDataFlow(&data_flow);
		endpoint->Release();
		input_support = (data_flow == eCapture);
		output_support = (data_flow == eRender);

		LPWSTR wstr;
		device->GetId(&wstr);
		id = gen::utf::utf16to8((const char16_t*)wstr, 0);

		format_scope.bit_depth.is_range = false;
		format_scope.bit_depth.default_val = 32;
		format_scope.channel_count.is_range = false;
		format_scope.channel_count.default_val = 2;
		format_scope.sample_rate.is_range = false;
		format_scope.sample_rate.default_val = 48000;

		IAudioClient* client;
		device->Activate(__uuidof(IAudioClient), CLSCTX_ALL, NULL, (void**)&client);
		check_formatSupport(client, 16, 44100, 2);
		check_formatSupport(client, 16, 48000, 2);
		client->Release();
	}
	WasapiDevice(const WasapiDevice& rhs) { device->AddRef(); }
	~WasapiDevice() { device->Release(); }

	gen::String get_id() const { return id; }
	gen::audio::FormatScope supported_formats() const { return format_scope; }
	bool is_real() const { return real; }
	gen::String get_name() const { return name; }
	bool is_source() const { return supports_input(); }
	bool is_sink() const { return supports_output(); }
	bool supports_input() const { return input_support; }
	bool supports_output() const { return output_support; }
	IMMDevice* ptr() { return device; }
};

struct WasapiAudioOutputThreadData {
	IAudioClient* client;
	gen::audio::OutputCallback callback;
	gen::audio::Output* stream;
	void* callback_user;
	bool stopped;
};

void* WasapiAudioOutputThread(void* userdata) {
	WasapiAudioOutputThreadData* data = (WasapiAudioOutputThreadData*)userdata;
	IAudioRenderClient* render;
	data->client->GetService(__uuidof(IAudioRenderClient), (void**)&render);

	UINT32 buffer_size;
	HANDLE buffer_event = CreateEvent(0, 0, 1, 0);
	data->client->GetBufferSize(&buffer_size);
	data->client->SetEventHandle(buffer_event);

	uint8_t* buffer;
	render->GetBuffer(buffer_size, (BYTE**)&buffer);
	UINT32 frames_written = 0;
	if (buffer) frames_written = data->callback(data->callback_user, data->stream, buffer_size, buffer);
	render->ReleaseBuffer(frames_written, 0);

	data->client->Start();
	while (true) {
		if (data->stopped) break;
		WaitForSingleObject(buffer_event, INFINITE);
		
		UINT32 padding;
		data->client->GetCurrentPadding(&padding);
		UINT32 frames_to_fill = (buffer_size - padding);
		
		render->GetBuffer(frames_to_fill, (BYTE**)&buffer);
		frames_written = 0;
		if(buffer) frames_written = data->callback(data->callback_user, data->stream, frames_to_fill, buffer);
		render->ReleaseBuffer(frames_written, 0);
	}
	render->Release();
	data->client->Release();
	return nullptr;
}

class WasapiOutput :public gen::audio::Output {
	IAudioClient* client;
	ISimpleAudioVolume* simple_volume;
	IAudioSessionControl* session_control;
	WasapiAudioOutputThreadData* thread_data;
	gen::audio::Device* gen_device;
	gen::audio::OutputCallback callback;
	gen::audio::Format format;

	float volume = 1.0f;
	bool muted = false;
public:
	WasapiOutput(IAudioClient* client, gen::audio::Device* gen_device, gen::audio::OutputCallback callback, void* callback_user, const gen::String& app_name, gen::audio::Format format) {
		this->client = client;
		this->gen_device = gen_device;
		this->callback = callback;

		client->GetService(__uuidof(ISimpleAudioVolume), (void**)&simple_volume);
		client->GetService(__uuidof(IAudioSessionControl), (void**)&session_control);

		this->format = format;
		set_name(app_name);

		client->AddRef();
		thread_data = new WasapiAudioOutputThreadData();
		thread_data->client = client;
		thread_data->stream = this;
		thread_data->callback = callback;
		thread_data->callback_user = callback_user;
		thread_data->stopped = false;
		gen::Thread thread(WasapiAudioOutputThread, thread_data, "AudioOutput");
	}
	~WasapiOutput() {
		thread_data->stopped = true;
		client->Stop();
		simple_volume->Release();
		session_control->Release();
		client->Release();
	}

	gen::audio::Format get_format() { return this->format; }
	const gen::audio::Device* get_device() { return gen_device; }
	bool set_device(const gen::audio::Device* device) { return false; }
	gen::String get_name() { 
		LPWSTR wstr;
		session_control->GetDisplayName(&wstr);
		return gen::utf::utf16to8((const char16_t*)wstr, 0);
	}
	bool set_name(const gen::String& name) { 
		gen::Buffer<char16_t> wstr;
		return (session_control->SetDisplayName((LPWSTR)gen::utf::utf8to16(name.c_str(), 0, wstr), nullptr) == S_OK);
	}
	float get_volume() {
		float ret;
		simple_volume->GetMasterVolume(&ret);
		return ret;
	}
	bool set_volume(float volume) {
		this->volume = volume;
		return (simple_volume->SetMasterVolume(volume, NULL) == S_OK);
	}
	bool is_muted() { 
		BOOL ret;
		return simple_volume->GetMute(&ret);
	}
	bool set_muted(bool mute) {
		this->muted = mute;
		return (simple_volume->SetMute(mute, NULL) == S_OK);
	}
	bool mute() { return set_muted(true); }
	bool unmute() { return set_muted(false); }
};

class WasapiSystem :public gen::audio::System {
	IMMDeviceEnumerator* device_enumarator;
public:
	WasapiSystem() {
		CoInitializeEx(nullptr, COINIT_MULTITHREADED);
		HRESULT result;
		if ((result = CoCreateInstance(__uuidof(MMDeviceEnumerator), nullptr, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&device_enumarator)))) {
			throw gen::RuntimeException("Can't create MMDeviceEnumerator");
		}
	}
	~WasapiSystem() {
		device_enumarator->Release();
	}
	gen::DynamicArray<gen::Ptr<gen::audio::Device>> enumerate_devices(bool real_only = true) {
		IMMDeviceCollection* collection;
		if (device_enumarator->EnumAudioEndpoints(eAll, DEVICE_STATE_ACTIVE, &collection) != S_OK) {
			throw gen::RuntimeException("Can't enumarate audio endpoints");
		}
		gen::DynamicArray<gen::Ptr<gen::audio::Device>> output;
		UINT device_count;
		collection->GetCount(&device_count);
		for (size_t i = 0; i < device_count; i++) {
			IMMDevice* device;
			collection->Item(i, &device);
			output.add(gen::Ptr<gen::audio::Device>(new WasapiDevice(device)));
		}
		return output;
	}
	gen::audio::Output* output(gen::audio::Device* device, const gen::String& app_name, gen::audio::Format format, gen::audio::OutputCallback callback, void* user) {
		IMMDevice* imm_device = nullptr;
		if (device && device->supports_output()) {
			imm_device = ((WasapiDevice*)device)->ptr();
		}
		if (!imm_device) {
			device_enumarator->GetDefaultAudioEndpoint(eRender, eConsole, &imm_device);
		}

		WAVEFORMATEX wave_format;
		wave_format.wFormatTag = WAVE_FORMAT_PCM;
		wave_format.nChannels = format.channel_count;
		wave_format.nSamplesPerSec = format.sample_rate;
		wave_format.wBitsPerSample = format.bit_depth;
		wave_format.nBlockAlign = ((wave_format.wBitsPerSample * wave_format.nChannels) / 8);
		wave_format.nAvgBytesPerSec = (wave_format.nSamplesPerSec * wave_format.nBlockAlign);
		wave_format.cbSize = 0;

		IAudioClient* client;
		imm_device->Activate(__uuidof(IAudioClient), CLSCTX_ALL, NULL, (void**)&client);
		HRESULT result = client->Initialize(AUDCLNT_SHAREMODE_SHARED, AUDCLNT_STREAMFLAGS_EVENTCALLBACK | AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM | AUDCLNT_STREAMFLAGS_SRC_DEFAULT_QUALITY, 0, 0, &wave_format, NULL);
		if (result != S_OK) {
			throw gen::RuntimeException("Can't initialize audio device");
		}
		return new WasapiOutput(client, device, callback, user, app_name, format);
	}
};

gen::audio::System* gen::audio::system() {
	return new WasapiSystem();
}
