// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include "pipewire.hpp"

gen::audio::System* gen::audio::system() {
	return new PwSystem();
}
