// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/audio/audio.hpp>
#include <gongen/common/logger.hpp>
#include <gongen/common/platform.hpp>
#include <gongen/common/containers/hashmap.hpp>
#include <gongen/common/hash.hpp>
#include <spa/param/audio/format-utils.h>
#include <pipewire/pipewire.h>

#include <spa/debug/pod.h>
namespace gen::audio {

uint8_t pw_format_to_bit_depth(spa_audio_format id);
spa_audio_format pw_format_from_bit_depth(uint8_t depth);

class PwDevice : public Device {
public:
	struct spa_hook listener;
	struct pw_node_info* info = nullptr;
	uint32_t media_type;
	uint32_t media_subtype;
	FormatScope formats;

	PwDevice(const PwDevice& other) {
		spa_zero(this->listener);
		uint64_t change_mask = other.info->change_mask;
		other.info->change_mask = UINT64_MAX;
		this->node_info(other.info);
		other.info->change_mask = change_mask;
		this->media_type = other.media_type;
		this->media_subtype = other.media_subtype;
		this->formats = other.formats;
	}
	PwDevice(uint32_t id, const struct spa_dict* props) {
		struct pw_node_info info = {0};
		info.id = id;
		info.props = (struct spa_dict*) props; // it won't be modified, i promise :)
		info.change_mask |= PW_NODE_CHANGE_MASK_PROPS;
		this->node_info(&info);
	}
	~PwDevice() {
		pw_node_info_free(this->info);
		spa_hook_remove(&this->listener);
	}

	void node_info(const struct pw_node_info* info) {
		this->info = pw_node_info_update(this->info, info);
	}
	void node_param(const struct spa_pod* param, uint32_t param_id) {
		if (param_id != SPA_PARAM_EnumFormat) return;
		if (spa_format_parse(param, &this->media_type, &this->media_subtype) < 0) {
			this->media_type = SPA_MEDIA_TYPE_unknown;
			return;
		}
		if (!this->is_audio()) return;

		this->formats.reset();
		const struct spa_pod_prop* prop;
		prop = spa_pod_find_prop(param, nullptr, SPA_FORMAT_AUDIO_format);
		if (spa_pod_is_choice(&prop->value) && spa_pod_is_id(&((const struct spa_pod_choice*) &prop->value)->body.child)) {
			auto* choice = (const struct spa_pod_choice*) &prop->value;
			if (choice->body.type == SPA_CHOICE_None || choice->body.type == SPA_CHOICE_Enum) {
				const uint32_t* id;
				SPA_POD_CHOICE_FOREACH(choice, id) {
					this->formats.bit_depth.val.select.add(pw_format_to_bit_depth((enum spa_audio_format) *id));
				}
				this->formats.bit_depth.default_val = this->formats.bit_depth.val.select[0];
			}
		} else if (spa_pod_is_id(&prop->value)) {
			auto* id = (const struct spa_pod_id*) &prop->value;
			this->formats.bit_depth.val.select.add(pw_format_to_bit_depth((enum spa_audio_format) id->value));
		} else {
			gen::log::warnf(
				"Cannot use PipeWire device #%u: unknown value type for key %u.",
				this->info->id, SPA_FORMAT_AUDIO_format
			);
			this->media_type = SPA_MEDIA_TYPE_unknown;
			return;
		}
		prop = spa_pod_find_prop(param, nullptr, SPA_FORMAT_AUDIO_rate);
		if (spa_pod_is_choice(&prop->value) && spa_pod_is_int(&((const struct spa_pod_choice*) &prop->value)->body.child)) {
			auto* choice = (const struct spa_pod_choice*) &prop->value;
			if (choice->body.type == SPA_CHOICE_None || choice->body.type == SPA_CHOICE_Enum) {
				const int32_t* rate;
				SPA_POD_CHOICE_FOREACH(choice, rate) {
					this->formats.sample_rate.val.select.add(*rate);
				}
				this->formats.sample_rate.default_val = this->formats.sample_rate.val.select[0];
			} else if (choice->body.type == SPA_CHOICE_Range) {
				this->formats.sample_rate.is_range = true;
				uint32_t* destinations[3] = {
					&this->formats.sample_rate.default_val,
					&this->formats.sample_rate.val.range[0],
					&this->formats.sample_rate.val.range[1],
				};
				int i = 0;
				const int32_t* rate;
				SPA_POD_CHOICE_FOREACH(choice, rate) {
					*destinations[i] = *rate;
					i++;
				}
			}
		} else if (spa_pod_is_int(&prop->value)) {
			auto* rate = (const struct spa_pod_int*) &prop->value;
			this->formats.sample_rate.val.select.add(rate->value);
		} else {
			gen::log::warnf(
				"Cannot use PipeWire device #%u: unknown value type for key %u.",
				this->info->id, SPA_FORMAT_AUDIO_rate
			);
			this->media_type = SPA_MEDIA_TYPE_unknown;
			return;
		}
		prop = spa_pod_find_prop(param, nullptr, SPA_FORMAT_AUDIO_channels);
		if (spa_pod_is_choice(&prop->value) && spa_pod_is_int(&((const struct spa_pod_choice*) &prop->value)->body.child)) {
			auto* choice = (const struct spa_pod_choice*) &prop->value;
			if (choice->body.type == SPA_CHOICE_None || choice->body.type == SPA_CHOICE_Enum) {
				const int32_t* channels;
				SPA_POD_CHOICE_FOREACH(choice, channels) {
					this->formats.channel_count.val.select.add(*channels);
				}
				this->formats.channel_count.default_val = this->formats.channel_count.val.select[0];
			} else if (choice->body.type == SPA_CHOICE_Range) {
				this->formats.channel_count.is_range = true;
				uint8_t* destinations[3] = {
					&this->formats.channel_count.default_val,
					&this->formats.channel_count.val.range[0],
					&this->formats.channel_count.val.range[1],
				};
				int i = 0;
				const int32_t* channels;
				SPA_POD_CHOICE_FOREACH(choice, channels) {
					*destinations[i] = *channels;
					i++;
				}
			}
		} else if (spa_pod_is_int(&prop->value)) {
			auto* rate = (const struct spa_pod_int*) &prop->value;
			this->formats.sample_rate.val.select.add(rate->value);
		} else {
			gen::log::warnf(
				"Cannot use PipeWire device #%u: unknown value type for key %u.",
				this->info->id, SPA_FORMAT_AUDIO_channels
			);
			this->media_type = SPA_MEDIA_TYPE_unknown;
			return;
		}
	}

	bool is_audio() const {
		return (
			this->media_type == SPA_MEDIA_TYPE_audio &&
			this->media_subtype == SPA_MEDIA_SUBTYPE_raw
		);
	}

	const char* get_serial() const {
		return spa_dict_lookup(this->info->props, PW_KEY_OBJECT_SERIAL);
	}

	String get_id() const override {
		return this->info->id;
	}
	FormatScope supported_formats() const override {
		return this->formats;
	}
	bool is_real() const override {
		return spa_dict_lookup(this->info->props, PW_KEY_DEVICE_ID) != nullptr;
	}
	String get_name() const override {
		const char* name = spa_dict_lookup(this->info->props, PW_KEY_NODE_DESCRIPTION);
		if (!name) name = spa_dict_lookup(this->info->props, PW_KEY_NODE_NAME);
		if (!name) return "(unnamed)";
		return String(name);
	}
	bool is_source() const override {
		const char* type = spa_dict_lookup(this->info->props, PW_KEY_MEDIA_CLASS);
		if (!type) return false;
		return strcmp(type, "Audio/Source") == 0;
	}
	bool is_sink() const override {
		const char* type = spa_dict_lookup(this->info->props, PW_KEY_MEDIA_CLASS);
		if (!type) return false;
		return strcmp(type, "Audio/Sink") == 0;
	}
	bool supports_input() const override {
		return this->info->n_output_ports > 0;
	}
	bool supports_output() const override {
		return this->info->n_input_ports > 0;
	}
};


class PwOutput : public Output {
public:
	struct pw_stream_events stream_events = {
		.version = PW_VERSION_STREAM_EVENTS,
		.state_changed = [](void* self, enum pw_stream_state old, enum pw_stream_state state, const char* error) {
			if (state != PW_STREAM_STATE_STREAMING) return;
			((PwOutput*) self)->stream_ready();
		},
		.process = [](void* self) {
			((PwOutput*) self)->process();
		},
	};
	struct pw_node_events node_events = {
		.version = PW_VERSION_NODE_EVENTS,
		.info = [](void* self, const struct pw_node_info* info) {
			((PwOutput*) self)->node_info(info);
		},
	};

	gen::String app_name;
	Format format;
	OutputCallback callback;
	void* user;
	void (*on_delete)(void* system, PwOutput* output);
	PwDevice* (*find_device)(void* system, uint32_t id);
	void* system;

	PwDevice* device = nullptr;
	pw_registry* registry = nullptr;
	pw_node* node = nullptr;
	pw_stream* stream = nullptr;
	struct spa_hook node_listener;
	struct spa_hook stream_listener;
	struct pw_node_info* info = nullptr;

	PwOutput(
		const gen::String& app_name, Format format, OutputCallback callback, void* user,
		void (*on_delete)(void* system, PwOutput* output),
		PwDevice* (*find_device)(void* system, uint32_t id),
		void* system
	) {
		this->app_name = app_name;
		this->format = format;
		this->callback = callback;
		this->user = user;
		this->on_delete = on_delete;
		this->find_device = find_device;
		this->system = system;
	}
	~PwOutput() {
		pw_stream_disconnect(this->stream);
		spa_hook_remove(&this->node_listener);
		spa_hook_remove(&this->stream_listener);
		this->on_delete(this->system, this);
	}
	void init(pw_core* core, pw_registry* registry) {
		this->registry = registry;
		this->stream = pw_stream_new(
			core, this->app_name.c_str(),
			pw_properties_new(
				PW_KEY_MEDIA_TYPE, "Audio",
				PW_KEY_MEDIA_CATEGORY, "Playback",
				PW_KEY_MEDIA_ROLE, "Music",
				nullptr
			)
		);
		this->connect_to_device(this->device);
		spa_zero(this->stream_listener);
		pw_stream_add_listener(this->stream, &this->stream_listener, &this->stream_events, this);
	}
	void stream_ready() {
		if (this->node) return;
		uint32_t node_id = pw_stream_get_node_id(this->stream);
		this->node = (pw_node*) pw_registry_bind(this->registry, node_id, PW_TYPE_INTERFACE_Node, PW_VERSION_NODE, 0);
		spa_zero(this->node_listener);
		pw_node_add_listener(this->node, &this->node_listener, &this->node_events, this);
		this->set_name(this->app_name);
	}
	void node_info(const struct pw_node_info* info) {
		this->info = pw_node_info_update(this->info, info);
		// printf("stream #%u props:\n", this->info->id);
		// const struct spa_dict_item* prop;
		// spa_dict_for_each(prop, this->info->props) {
		// 	printf("| %s: %s\n", prop->key, prop->value);
		// }
	}
	void process() {
		pw_buffer* buffer = pw_stream_dequeue_buffer(this->stream);
		if (buffer == nullptr) {
			gen::log::warnf("PipeWire audio is out of buffers for stream #%u.", this->info->id);
			return;
		}
		spa_buffer* buf = buffer->buffer;
		uint8_t* data = (uint8_t*) buf->datas[0].data;
		if (data == nullptr) return;

		size_t stride = ((this->format.bit_depth + 7) / 8) * this->format.channel_count;
		size_t frame_count = buf->datas[0].maxsize / stride;
		if (buffer->requested && buffer->requested < frame_count) {
			frame_count = buffer->requested;
		}

		frame_count = this->callback(this->user, this, frame_count, data);

		buf->datas[0].chunk->offset = 0;
		buf->datas[0].chunk->stride = stride;
		buf->datas[0].chunk->size = frame_count * stride;

		pw_stream_queue_buffer(this->stream, buffer);
	}

	const char* get_prop(const char* key) {
		return pw_properties_get(pw_stream_get_properties(this->stream), key);
	}
	bool set_prop(const char* key, const char* value) {
		struct spa_dict_item device_prop = SPA_DICT_ITEM_INIT(key, value);
		struct spa_dict update_props = SPA_DICT_INIT(&device_prop, 1);
		if (pw_stream_update_properties(this->stream, &update_props) < 0) {
			gen::log::warnf("Failed to change props of PipeWire stream #%u.", this->info->id);
			return false;
		}
		return true;
	}

	void connect_to_device(const PwDevice* device) {
		this->set_prop(PW_KEY_TARGET_OBJECT, this->device ? this->device->get_serial() : nullptr);
		uint8_t format_buffer[1024];
		struct spa_pod_builder format_builder = SPA_POD_BUILDER_INIT(format_buffer, sizeof(format_buffer));
		spa_audio_info_raw audio_info = SPA_AUDIO_INFO_RAW_INIT(
			.format = pw_format_from_bit_depth(this->format.bit_depth),
			.rate = this->format.sample_rate,
			.channels = this->format.channel_count,
		);
		const struct spa_pod* params[] = {
			spa_format_audio_raw_build(
				&format_builder, SPA_PARAM_EnumFormat, &audio_info
			),
		};
		if (pw_stream_connect(
			this->stream, PW_DIRECTION_OUTPUT, PW_ID_ANY,
			(pw_stream_flags) (
				PW_STREAM_FLAG_AUTOCONNECT |
				PW_STREAM_FLAG_MAP_BUFFERS// |
				// PW_STREAM_FLAG_RT_PROCESS
			),
			params, sizeof(params) / sizeof(const struct spa_pod*)
		) < 0) {
			this->stream = nullptr;
			this->node = nullptr;
			throw RuntimeException::from_errno("Could not connect a PipeWire stream.");
		}
	}

	Format get_format() override {
		return this->format;
	}

	const Device* get_device() override {
		if (!this->stream) return this->device;
		// TODO obtain actually connected device from props
		return this->device;
	}
	bool set_device(const Device* base_device) override {
		if (!this->stream) {
			if (base_device == nullptr) {
				this->device = nullptr;
				return true;
			}
			auto* device = dynamic_cast<const PwDevice*>(base_device);
			if (!device || !device->is_audio() || !device->supports_output()) return false;
			this->device = new PwDevice(*device);
			return true;
		}
		pw_stream_disconnect(this->stream);
		this->connect_to_device(base_device != nullptr ? dynamic_cast<const PwDevice*>(base_device) : nullptr);
		return true;
	}

	String get_name() override {
		if (!this->stream) return this->app_name;
		const char* val = this->get_prop(PW_KEY_NODE_DESCRIPTION);
		if (!val) val = this->get_prop(PW_KEY_NODE_NAME);
		return String(val);
	}
	bool set_name(const String& name) override {
		if (!this->node) {
			this->app_name = name;
			return true;
		}
		return this->set_prop(PW_KEY_NODE_DESCRIPTION, name.c_str());
	}
	float get_volume() override {
		// TODO
		return 1.0f;
	}
	bool set_volume(float volume) override {
		// TODO
		return false;
	}
	bool is_muted() override {
		// TODO
		return false;
	}
	bool set_muted(bool mute) override {
		// TODO
		return false;
	}
};

static std::atomic<size_t> system_count = 0;

class PwSystem : public System {
public:
	HashMap<uint32_t, PwDevice> devices;
	TypedLocklessQueue<PwOutput> outputs_to_init;
	Thread thread;
	struct pw_main_loop* loop;
	struct pw_core* core = nullptr;
	struct pw_registry* registry = nullptr;

	PwSystem() : devices(no_hash) {
		system_count++;
		pw_init(0, 0);

		this->loop = pw_main_loop_new(nullptr);
		this->thread = Thread([](void* user) {
			PwSystem* self = (PwSystem*) user;
			struct pw_main_loop* loop = self->loop;
			struct pw_context* context = pw_context_new(pw_main_loop_get_loop(loop), nullptr, 0);
			if (context == nullptr) {
				throw RuntimeException::from_errno("Could not connect to the PipeWire core.");
			}
			struct pw_core* core = pw_context_connect(context, nullptr, 0);
			if (core == nullptr) {
				throw RuntimeException::from_errno("Could not connect to the PipeWire core.");
			}

			struct pw_node_events node_events = {
				.version = PW_VERSION_NODE_EVENTS,
				.info = [](void* self, const struct pw_node_info* info) {
					((PwDevice*) self)->node_info(info);
				},
				.param = [](void* self, int seq, uint32_t id, uint32_t index, uint32_t next, const struct spa_pod* param) {
					((PwDevice*) self)->node_param(param, id);
				}
			};
			struct registry_user_data {
				PwSystem* self;
				struct pw_registry* registry;
				pw_node_events node_events;
			} registry_data = {
				.self = self,
				.node_events = node_events,
			};

			struct pw_registry* registry = pw_core_get_registry(core, PW_VERSION_REGISTRY, 0);
			registry_data.registry = registry;
			struct spa_hook registry_listener;
			spa_zero(registry_listener);
			struct pw_registry_events registry_events = {
				.version = PW_VERSION_REGISTRY_EVENTS,
				.global = [](void* user, uint32_t id, uint32_t perms, const char* type, uint32_t version, const struct spa_dict* props) {
					if (strcmp(type, PW_TYPE_INTERFACE_Node) != 0) return;
					if (!PW_PERM_IS_X(perms)) return; // node permissions too restrictive
					auto* data = (struct registry_user_data*) user;
					auto* node = (struct pw_node*) pw_registry_bind(data->registry, id, type, PW_VERSION_DEVICE, 0);
					PwDevice& device = data->self->devices.emplace(id, id, props);
					spa_zero(device.listener);
					gen::log::debugf("Registered audio node #%u: '%s'.", id, device.get_name().c_str());
					pw_node_add_listener(node, &device.listener, &data->node_events, &device);
					// uint32_t params[] = { SPA_PARAM_EnumFormat };
					// pw_node_subscribe_params(node, params, sizeof(params) / sizeof(uint32_t));
					pw_node_enum_params(node, 0, SPA_PARAM_EnumFormat, 0, UINT32_MAX, nullptr);
				},
				.global_remove = [](void* user, uint32_t id) {
					PwSystem* self = ((struct registry_user_data*) user)->self;
					if (!self->devices.remove_safe(id)) return;
					gen::log::debugf("Unregistered audio node #%u.", id);
				},
			};
			pw_registry_add_listener(registry, &registry_listener, &registry_events, &registry_data);

			while (PwOutput* output = self->outputs_to_init.dequeue()) {
				output->init(core, registry);
			}

			self->core = core; // core is ready for adding streams
			self->registry = registry;
			pw_main_loop_run(loop);

			spa_hook_remove(&registry_listener);
			self->devices.clear();
			pw_proxy_destroy((struct pw_proxy*) registry);
			pw_core_disconnect(core);
			pw_context_destroy(context);
			return user;
		}, (void*) this, "pipewire");
	}
	~PwSystem() {
		pw_main_loop_quit(this->loop);
		this->thread.join();
		pw_main_loop_destroy(this->loop);
		if (system_count > 0) system_count--;
		if (system_count == 0) pw_deinit();
	}
	void check_thread() {
		if (!this->thread.done()) return;
		this->thread.rethrow_exception();
		throw RuntimeException("The PipeWire thread has stopped unexpectedly.");
	}
	DynamicArray<Ptr<Device>> enumerate_devices(bool real_only) override {
		this->check_thread();
		DynamicArray<Ptr<Device>> devices;
		for (PwDevice& device : this->devices) {
			if (!device.is_audio() || (real_only && !device.is_real())) continue;
			devices.add(Ptr<Device>(new PwDevice(device)));
		}
		return devices;
	}
	Output* output(Device* device, const String& app_name, Format format, OutputCallback callback, void* user) override {
		this->check_thread();
		PwOutput* output = new PwOutput(app_name, format, callback, user,
		[](void* system, PwOutput* deleted) {
			PwSystem* self = (PwSystem*) system;
			if (self->core) return;
			// on output destruction, delete it from the queue to avoid bad pointer
			int count = self->outputs_to_init.size();
			for (int i = 0; i < count; i++) {
				PwOutput* queued = self->outputs_to_init.dequeue();
				if (queued == deleted) continue;
				self->outputs_to_init.enqueue(queued);
			}
		}, [](void* system, uint32_t device_id) {
			return ((PwSystem*) system)->devices.ptr(device_id);
		}, this);
		output->set_device(device);
		if (this->core) {
			output->init(this->core, this->registry);
		} else {
			this->outputs_to_init.enqueue(output);
		}
		return output;
	}
};

#define FORMAT(key, val) [key - SPA_AUDIO_FORMAT_START_Interleaved] = val
const static uint8_t bit_depth_conversion_table[] = {
	FORMAT(SPA_AUDIO_FORMAT_S8, 8),
	FORMAT(SPA_AUDIO_FORMAT_U8, 8),
	FORMAT(SPA_AUDIO_FORMAT_S16_LE, 16),
	FORMAT(SPA_AUDIO_FORMAT_S16_BE, 16),
	FORMAT(SPA_AUDIO_FORMAT_U16_LE, 16),
	FORMAT(SPA_AUDIO_FORMAT_U16_BE, 16),
	FORMAT(SPA_AUDIO_FORMAT_S24_32_LE, 32),
	FORMAT(SPA_AUDIO_FORMAT_S24_32_BE, 32),
	FORMAT(SPA_AUDIO_FORMAT_U24_32_LE, 32),
	FORMAT(SPA_AUDIO_FORMAT_U24_32_BE, 32),
	FORMAT(SPA_AUDIO_FORMAT_S32_LE, 32),
	FORMAT(SPA_AUDIO_FORMAT_S32_BE, 32),
	FORMAT(SPA_AUDIO_FORMAT_U32_LE, 32),
	FORMAT(SPA_AUDIO_FORMAT_U32_BE, 32),
	FORMAT(SPA_AUDIO_FORMAT_S24_LE, 24),
	FORMAT(SPA_AUDIO_FORMAT_S24_BE, 24),
	FORMAT(SPA_AUDIO_FORMAT_U24_LE, 24),
	FORMAT(SPA_AUDIO_FORMAT_U24_BE, 24),
	FORMAT(SPA_AUDIO_FORMAT_S20_LE, 20),
	FORMAT(SPA_AUDIO_FORMAT_S20_BE, 20),
	FORMAT(SPA_AUDIO_FORMAT_U20_LE, 20),
	FORMAT(SPA_AUDIO_FORMAT_U20_BE, 20),
	FORMAT(SPA_AUDIO_FORMAT_S18_LE, 18),
	FORMAT(SPA_AUDIO_FORMAT_S18_BE, 18),
	FORMAT(SPA_AUDIO_FORMAT_U18_LE, 18),
	FORMAT(SPA_AUDIO_FORMAT_U18_BE, 18),
	FORMAT(SPA_AUDIO_FORMAT_F32_LE, 32),
	FORMAT(SPA_AUDIO_FORMAT_F32_BE, 32),
	FORMAT(SPA_AUDIO_FORMAT_F64_LE, 64),
	FORMAT(SPA_AUDIO_FORMAT_F64_BE, 64),
	FORMAT(SPA_AUDIO_FORMAT_U8P, 8),
	FORMAT(SPA_AUDIO_FORMAT_S16P, 16),
	FORMAT(SPA_AUDIO_FORMAT_S24_32P, 32),
	FORMAT(SPA_AUDIO_FORMAT_S32P, 32),
	FORMAT(SPA_AUDIO_FORMAT_S24P, 24),
	FORMAT(SPA_AUDIO_FORMAT_F32P, 32),
	FORMAT(SPA_AUDIO_FORMAT_F64P, 64),
	FORMAT(SPA_AUDIO_FORMAT_S8P, 8),
};
#undef FORMAT

uint8_t pw_format_to_bit_depth(spa_audio_format id) {
	size_t table_len = sizeof(bit_depth_conversion_table) / sizeof(uint8_t);
	size_t key = id - SPA_AUDIO_FORMAT_START_Interleaved;
	if (key > table_len) return 16;
	if (bit_depth_conversion_table[key] == 0) return 16;
	return bit_depth_conversion_table[key];
}

spa_audio_format pw_format_from_bit_depth(uint8_t depth) {
	if (depth == 8) return SPA_AUDIO_FORMAT_S8;
	if (depth == 16) return SPA_AUDIO_FORMAT_S16;
	if (depth == 18) return SPA_AUDIO_FORMAT_S18;
	if (depth == 20) return SPA_AUDIO_FORMAT_S20;
	if (depth == 24) return SPA_AUDIO_FORMAT_S24;
	if (depth == 32) return SPA_AUDIO_FORMAT_S32;
	return SPA_AUDIO_FORMAT_S16;
}

} // namespace gen::audio
