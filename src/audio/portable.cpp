// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/audio.hpp>

bool gen::audio::Stream::mute() {
	if (this->set_muted(true)) return true;
	this->pre_mute_volume = this->get_volume();
	return this->set_volume(0.0f);
}
bool gen::audio::Stream::unmute() {
	if (this->set_muted(false)) return true;
	return this->set_volume(this->pre_mute_volume);
}
