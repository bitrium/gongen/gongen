// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include "gl_internal.hpp"
#include <gongen/common/error.hpp>
#include <gongen/common/logger.hpp>

gen::gl::internal::GLBuffer::GLBuffer(Device* device, const BufferDesc& desc) {
	this->device = device;
	this->desc = desc;
	if (device->is_context_active(false)) {
		this->data = (void*)desc.data;
		init();
	}
	else {
		if (desc.data) {
			this->data = gen::alloc(desc.size);
			memcpy(this->data, desc.data, desc.size);
			this->own_data = true;
		}
	}
}
void gen::gl::internal::GLBuffer::init() {
	if (!buffer) {
		glGenBuffers(1, &buffer);
		device->state.bind_buffer(GL_UNIFORM_BUFFER, buffer);

		gl_usage = GL_STATIC_DRAW;
		if (desc.usage == Usage::DYNAMIC) gl_usage = GL_STREAM_DRAW;
		glBufferData(GL_UNIFORM_BUFFER, desc.size, data, gl_usage);

		if (own_data) {
			free(data);
			data = nullptr;
		}
	}
}
gen::gl::internal::GLBuffer::~GLBuffer() {
	if (buffer) {
		if (device->is_context_active(false)) glDeleteBuffers(1, &buffer);
		else device->free_queue_buffer.enqueue((void*)(intptr_t)buffer);
	}
	if (own_data && data) {
		free(data);
	}
}

gen::gl::internal::GLTexture::GLTexture(Device* device, const TextureDesc& desc) {
	this->device = device;
	this->desc = desc;
	if (device->is_context_active(false)) {
		this->data = (void*)desc.data;
		init();
	}
	else {
		if (desc.data) {
			size_t dataSize = get_texture_size(desc.format, desc.size, desc.layers);
			this->data = gen::alloc(dataSize);
			memcpy(this->data, desc.data, dataSize);
			this->own_data = true;
		}
	}
}

extern void update_gl_texture(gen::gl::internal::Device* device, gen::gl::Format format, uint32_t target, uint32_t texture, uint32_t level, gen::vec3z offset, gen::vec3z size, const void* data, bool dsa_support);

void gen::gl::internal::GLTexture::init() {
	if (!texture) {
		gen::gl::internal::FormatInfo formatInfo = get_format_info(desc.format);
		if (desc.type == gen::gl::TextureType::TEXTURE_2D) {
			if (desc.layers) target = GL_TEXTURE_2D_ARRAY;
			else target = GL_TEXTURE_2D;
		}
		else if (desc.type == gen::gl::TextureType::TEXTURE_3D) target = GL_TEXTURE_3D;
		else if (desc.type == gen::gl::TextureType::TEXTURE_CUBEMAP) target = GL_TEXTURE_CUBE_MAP;
		
		glGenTextures(1, &texture);
		device->state.bind_texture(target, device->state.TEXTURE_UNIT, texture);
		if (target == GL_TEXTURE_2D) glTexStorage2D(target, desc.mip_levels, formatInfo.texture_internal_format, desc.size.x, desc.size.y);
		else if (target == GL_TEXTURE_2D_ARRAY) glTexStorage3D(target, desc.mip_levels, formatInfo.texture_internal_format, desc.size.x, desc.size.y, desc.layers);
		else if (target == GL_TEXTURE_3D) glTexStorage3D(target, desc.mip_levels, formatInfo.texture_internal_format, desc.size.x, desc.size.y, desc.size.z);
		else if (target == GL_TEXTURE_CUBE_MAP) glTexStorage2D(target, desc.mip_levels, formatInfo.texture_internal_format, desc.size.x, desc.size.y);
		glTexParameteri(target, GL_TEXTURE_BASE_LEVEL, 0);
		glTexParameteri(target, GL_TEXTURE_MAX_LEVEL, desc.mip_levels - 1);

		if (data) {
			update_gl_texture(device, desc.format, target, texture, 0, vec3z(), desc.size, desc.data, device->dsa_support);
			if (desc.generate_mipmaps) {
				glGenerateMipmap(target);
			}
		}
		if (own_data) {
			free(data);
			data = nullptr;
		}
	}
}
gen::gl::internal::GLTexture::~GLTexture() {
	if (texture) {
		if (device->is_context_active(false)) glDeleteTextures(1, &texture);
		else device->free_queue_texture.enqueue((void*)(intptr_t)texture);
	}
	if (own_data && data) free(data);
}

uint32_t get_gl_filtering(gen::gl::Filtering filter) {
	if (filter == gen::gl::Filtering::NEAREST) return GL_NEAREST;
	return GL_LINEAR;
}
uint32_t get_gl_filtering(gen::gl::Filtering filter, gen::gl::Filtering mipFilter) {
	if (mipFilter == gen::gl::Filtering::LINEAR) {
		if (filter == gen::gl::Filtering::NEAREST) return GL_NEAREST_MIPMAP_LINEAR;
		else return GL_LINEAR_MIPMAP_LINEAR;
	}
	else {
		if (filter == gen::gl::Filtering::NEAREST) return GL_NEAREST_MIPMAP_NEAREST;
		else return GL_LINEAR_MIPMAP_NEAREST;
	}
	return GL_LINEAR;
}
uint32_t get_gl_texture_wrap(gen::gl::TextureWrap wrap) {
	if (wrap == gen::gl::TextureWrap::CLAMP) return GL_CLAMP_TO_EDGE;
	else if (wrap == gen::gl::TextureWrap::REPEAT) return GL_REPEAT;
	else if (wrap == gen::gl::TextureWrap::MIRRORED_REPEAT) return GL_MIRRORED_REPEAT;
	return GL_CLAMP_TO_EDGE;
}

gen::gl::internal::GLSampler::GLSampler(Device* device, const SamplerDesc& desc) {
	this->device = device;
	this->desc = desc;
	if (device->is_context_active(false)) {
		init();
	}
}
void gen::gl::internal::GLSampler::init() {
	if (!sampler) {
		glGenSamplers(1, &sampler);
		glSamplerParameteri(sampler, GL_TEXTURE_MIN_FILTER, get_gl_filtering(desc.min_filter, desc.mip_filter));
		glSamplerParameteri(sampler, GL_TEXTURE_MAG_FILTER, get_gl_filtering(desc.min_filter));
		glSamplerParameteri(sampler, GL_TEXTURE_WRAP_S, get_gl_texture_wrap(desc.wrap[0]));
		glSamplerParameteri(sampler, GL_TEXTURE_WRAP_T, get_gl_texture_wrap(desc.wrap[1]));
		glSamplerParameteri(sampler, GL_TEXTURE_WRAP_R, get_gl_texture_wrap(desc.wrap[2]));
		if (GLAD_GL_ARB_texture_filter_anisotropic || GLAD_GL_EXT_texture_filter_anisotropic) {
			glSamplerParameterf(sampler, GL_TEXTURE_MAX_ANISOTROPY, desc.anisotropy);
		}
	}
}
gen::gl::internal::GLSampler::~GLSampler() {
	if (sampler) {
		if (device->is_context_active(false)) glDeleteSamplers(1, &sampler);
		else device->free_queue_sampler.enqueue((void*)(intptr_t)sampler);
	}
}

gen::gl::internal::GLProgram::GLProgram(Device* device, const ProgramDesc& desc) {
	this->device = device;
	this->desc = desc;
	if (device->is_context_active(false)) {
		init();
	}
}
void gen::gl::internal::GLProgram::init() {
	if (!program) {
		GLint state = 0;
		GLint infoLogLenght = 0;
		gen::String infoLog, defines;
		for (auto& d : desc.defines) {
			defines.appendf("#define %s\n", d.c_str());
		}
		gen::Buffer<uint32_t> shaders;
		for (size_t i = 0; i < 3; i++) {
			ShaderStage* stage = nullptr;
			const char* sources[4];
#ifdef GEN_GLES
			if (i == 0) sources[0] = "#version 310 es\n";
			else if (i == 1) {
				sources[0] = "#version 310 es\n \
precision mediump float;\n";
			}
			else if (i == 2) sources[0] = "#version 310 es\n";
#else
			sources[0] = "#version 430 core\n";
#endif
			if (i == 0) {
				sources[1] = "#define VERTEX_SHADER\n";
				stage = &desc.vertex;
			}
			else if (i == 1) {
				sources[1] = "#define FRAGMENT_SHADER\n";
				stage = &desc.fragment;
			}
			else if (i == 2) {
				sources[1] = "#define COMPUTE_SHADER\n";
				stage = &desc.compute;
			}
			if (stage && !stage->code.empty()) {
				uint32_t shader = 0;
				if (i == 0) shader = glCreateShader(GL_VERTEX_SHADER);
				else if (i == 1) shader = glCreateShader(GL_FRAGMENT_SHADER);
				else if (i == 2) shader = glCreateShader(GL_COMPUTE_SHADER);

				sources[2] = defines.c_str();
				sources[3] = stage->code.c_str();

				glShaderSource(shader, 4, sources, nullptr);
				glCompileShader(shader);
				infoLog.clear();
				glGetShaderiv(shader, GL_COMPILE_STATUS, &state);
				if (state == GL_FALSE) {
					glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLogLenght);
					glGetShaderInfoLog(shader, infoLogLenght, NULL, infoLog.reserve(infoLogLenght));
					glDeleteShader(shader);
					throw gen::gl::RuntimeException::createf("Can't compile shader %s: %s", stage->name.c_str(), infoLog.c_str());
				}
				shaders.add(shader);
			}
		}

		program = glCreateProgram();
		for (auto& s : shaders) {
			glAttachShader(program, s);
		}
		glLinkProgram(program);
		for (auto& s : shaders) {
			glDetachShader(program, s);
			glDeleteShader(s);
		}

		infoLog.clear();
		glGetProgramiv(program, GL_LINK_STATUS, &state);
		if (state == GL_FALSE) {
			glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infoLogLenght);
			glGetProgramInfoLog(program, infoLogLenght, NULL, infoLog.reserve(infoLogLenght));
			glDeleteProgram(program);
			throw gen::gl::RuntimeException(gen::String::createf("Can't link shader: %s", infoLog.c_str()));
		}

		device->state.bind_program(program);
	}
}
gen::gl::internal::GLProgram::~GLProgram() {
	if (program) {
		if (device->is_context_active(false)) glDeleteProgram(program);
		else device->free_queue_program.enqueue((void*)(uintptr_t)program);
	}
}

gen::gl::Buffer gen::gl::Device::create_buffer(const BufferDesc& desc) {
	return gen::gl::Buffer(new internal::GLBuffer(&impl, desc));
}
gen::gl::Texture gen::gl::Device::create_texture(const TextureDesc& desc) {
	return gen::gl::Texture(new internal::GLTexture(&impl, desc));
}
gen::gl::Sampler gen::gl::Device::create_sampler(const SamplerDesc& desc) {
	return gen::gl::Sampler(new internal::GLSampler(&impl, desc));
}
gen::gl::Program gen::gl::Device::create_program(const ProgramDesc& desc) {
	return gen::gl::Program(new internal::GLProgram(&impl, desc));
}
