// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include "gl_internal.hpp"

void update_gl_texture(gen::gl::internal::Device* device, gen::gl::Format format, uint32_t target, uint32_t texture, uint32_t level, gen::vec3z offset, gen::vec3z size, const void* data, bool dsa_support) {
	gen::gl::internal::FormatInfo formatInfo = gen::gl::internal::get_format_info(format);
	if (dsa_support) {
		if (target == GL_TEXTURE_2D) glTextureSubImage2D(texture, level, offset.x, offset.y, size.x, size.y, formatInfo.texture_format, formatInfo.type, data);
		else if (target == GL_TEXTURE_2D_ARRAY || target == GL_TEXTURE_3D) glTextureSubImage3D(texture, level, offset.x, offset.y, offset.z, size.x, size.y, size.z, formatInfo.texture_format, formatInfo.type, data);
		else if (target == GL_TEXTURE_CUBE_MAP) glTextureSubImage3D(texture, level, offset.x, offset.y, offset.z, size.x, size.y, size.z, formatInfo.texture_format, formatInfo.type, data);
	}
	else {
		device->state.bind_texture(target, device->state.TEXTURE_UNIT, texture);
		if (target == GL_TEXTURE_2D) glTexSubImage2D(target, level, offset.x, offset.y, size.x, size.y, formatInfo.texture_format, formatInfo.type, data);
		else if (target == GL_TEXTURE_2D_ARRAY || target == GL_TEXTURE_3D) glTexSubImage3D(target, level, offset.x, offset.y, offset.z, size.x, size.y, size.z, formatInfo.texture_format, formatInfo.type, data);
		else if (target == GL_TEXTURE_CUBE_MAP) {
			for (size_t i = offset.z; i < (offset.z + size.z); i++) {
				glTexSubImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, level, offset.x, offset.y, size.x, size.y, formatInfo.texture_format, formatInfo.type, data);
				if (data) {
					size_t tx_size = gen::gl::get_texture_size(format, gen::vec3z(size.x, size.y, 1), 1);
					GEN_INC_VOID_POINTER(data, tx_size);
				}
			}
		}
	}
}
uint32_t gen::gl::internal::Device::get_fbo(const gen::gl::FramebufferState& state) {
	uint64_t hash = state.hash();
	CachedObject* free_obj = nullptr;
	for (auto& c : fbo_cache) {
		if (c.object) {
			if (c.hash == hash) {
				if (c.life_counter < CACHE_OBJECT_LIFETIME) c.life_counter++;
				return c.object;
			}
			else if (!c.object) free_obj = &c;
		}
	}
	if (!free_obj) {
		free_obj = fbo_cache.add(CachedObject());
		free_obj->hash = hash;
	}
	glGenFramebuffers(1, &free_obj->object);
	this->state.bind_fbo(GL_FRAMEBUFFER, free_obj->object);
	for (size_t i = 0; i < state.color_attachment_count; i++) {
		internal::GLTexture* gl_texture = (internal::GLTexture*)state.color_attachments[i].get();
		if (!gl_texture) throw gen::gl::RuntimeException("Texture is null");
		gl_texture->init();
		if(gl_texture->target == GL_TEXTURE_2D) glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, gl_texture->target, gl_texture->texture, 0);
		else if (gl_texture->target == GL_TEXTURE_2D_ARRAY) glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + i, gl_texture->texture, 0, state.color_attachment_layer[i]);
	}
	if (state.depth_stencil_attachment.get()) {
		internal::GLTexture* gl_texture = (internal::GLTexture*)state.depth_stencil_attachment.get();
		gl_texture->init();
		if (gl_texture->target == GL_TEXTURE_2D) {
			if (gl_texture->desc.format == gen::gl::Format::D24_S8) glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, gl_texture->target, gl_texture->texture, 0);
			else glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, gl_texture->target, gl_texture->texture, 0);
		}
		else if (gl_texture->target == GL_TEXTURE_2D_ARRAY) {
			if (gl_texture->desc.format == gen::gl::Format::D24_S8) glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, gl_texture->texture, 0, state.depth_stencil_attachment_layer);
			else glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, gl_texture->texture, 0, state.depth_stencil_attachment_layer);
		}
	}

	return free_obj->object;
}

void gen::gl::Device::update_buffer(const Buffer& buffer, size_t offset, size_t size, const void* data) {
	internal::GLBuffer* gl_buffer = (internal::GLBuffer*)buffer.get();
	if (!gl_buffer) throw gen::gl::RuntimeException("Buffer is null");
	gl_buffer->init();

	if (impl.dsa_support) {
		if ((offset == 0) && (size == gl_buffer->desc.size)) glNamedBufferData(gl_buffer->buffer, size, data, gl_buffer->gl_usage);
		else glNamedBufferSubData(gl_buffer->buffer, offset, size, data);
	}
	else {
		impl.state.bind_buffer(GL_UNIFORM_BUFFER, gl_buffer->buffer);
		if ((offset == 0) && (size == gl_buffer->desc.size)) glBufferData(GL_UNIFORM_BUFFER, size, data, gl_buffer->gl_usage);
		else glBufferSubData(GL_UNIFORM_BUFFER, offset, size, data);
	}
}
void gen::gl::Device::update_texture(const Texture& texture, size_t level, gen::vec3z offset, gen::vec3z size, const void* data) {
	internal::GLTexture* gl_texture = (internal::GLTexture*)texture.get();
	if (!gl_texture) throw gen::gl::RuntimeException("Texture is null");
	gl_texture->init();
	update_gl_texture(&impl, gl_texture->desc.format, gl_texture->target, gl_texture->texture, level, offset, size, data, impl.dsa_support);
}
void gen::gl::Device::generate_mips(const Texture& texture) {
	internal::GLTexture* gl_texture = (internal::GLTexture*)texture.get();
	if (!gl_texture) throw gen::gl::RuntimeException("Texture is null");
	gl_texture->init();
	if (impl.dsa_support) {
		glGenerateTextureMipmap(gl_texture->texture);
	}
	else {
		impl.state.bind_texture(gl_texture->target, impl.state.TEXTURE_UNIT, gl_texture->texture);
		glGenerateMipmap(gl_texture->target);
	}
}

void gen::gl::Device::read_pixels(const Buffer& dst, uint32_t dst_offset, gen::vec2i src_offset, gen::vec2u size, gen::gl::Format format) {
	internal::GLBuffer* gl_buffer = (internal::GLBuffer*)dst.get();
	if (!gl_buffer) throw gen::gl::RuntimeException("Buffer is null");
	gl_buffer->init();
	impl.state.bind_buffer(GL_PIXEL_PACK_BUFFER, gl_buffer->buffer);
	gen::gl::internal::FormatInfo formatInfo = gen::gl::internal::get_format_info(format);
	glReadPixels(src_offset.x, src_offset.y, size.x, size.y, formatInfo.texture_format, formatInfo.type, (void*) (uintptr_t) dst_offset);
}
void gen::gl::Device::get_buffer_data(const Buffer& buffer, uint32_t offset, uint32_t size, void* data) {
	internal::GLBuffer* gl_buffer = (internal::GLBuffer*)buffer.get();
	if (!gl_buffer) throw gen::gl::RuntimeException("Buffer is null");
	gl_buffer->init();
	impl.state.bind_buffer(GL_COPY_READ_BUFFER, gl_buffer->buffer);
	glGetBufferSubData(GL_COPY_READ_BUFFER, offset, size, data);
}

void gen::gl::Device::bind_storage_buffer(const Buffer& buffer, uint32_t slot, size_t offset, size_t size) {
	internal::GLBuffer* gl_buffer = (internal::GLBuffer*)buffer.get();
	if (!gl_buffer) throw gen::gl::RuntimeException("Buffer is null");
	gl_buffer->init();
	impl.state.bind_storage_buffer(slot, gl_buffer->buffer, offset, size);
}
void gen::gl::Device::bind_uniform_buffer(const Buffer& buffer, uint32_t slot, size_t offset, size_t size) {
	internal::GLBuffer* gl_buffer = (internal::GLBuffer*)buffer.get();
	if (!gl_buffer) throw gen::gl::RuntimeException("Buffer is null");
	gl_buffer->init();
	impl.state.bind_uniform_buffer(slot, gl_buffer->buffer, offset, size);
}
void gen::gl::Device::bind_texture(const Texture& texture, uint32_t slot) {
	internal::GLTexture* gl_texture = (internal::GLTexture*)texture.get();
	if (!gl_texture) throw gen::gl::RuntimeException("TextureView is null");
	gl_texture->init();
	impl.state.bind_texture(gl_texture->target, slot, gl_texture->texture);
}
void gen::gl::Device::bind_image_texture(const Texture& texture, uint32_t slot, uint32_t level, uint32_t layer, bool layered, TextureAccess access, Format format) {
	internal::GLTexture* gl_texture = (internal::GLTexture*)texture.get();
	if (!gl_texture) throw gen::gl::RuntimeException("TextureView is null");
	gl_texture->init();
	uint32_t gl_access = GL_READ_ONLY;
	if (access == TextureAccess::WRITE) gl_access = GL_WRITE_ONLY;
	else if (access == TextureAccess::READ_WRITE) gl_access = GL_READ_WRITE;
	glBindImageTexture(slot, gl_texture->texture, level, layered, layer, gl_access, internal::get_format_info(format).texture_internal_format);
}
void gen::gl::Device::bind_sampler(const Sampler& sampler, uint32_t texture_slot) {
	internal::GLSampler* gl_sampler = (internal::GLSampler*)sampler.get();
	if (!gl_sampler) throw gen::gl::RuntimeException("Sampler is null");
	gl_sampler->init();
	impl.state.bind_sampler(texture_slot, gl_sampler->sampler);
}
void gen::gl::Device::bind_vertex_buffer(const Buffer& buffer, uint32_t binding, size_t offset) {
	internal::GLBuffer* gl_buffer = (internal::GLBuffer*)buffer.get();
	if (!gl_buffer) throw gen::gl::RuntimeException("Buffer is null");
	gl_buffer->init();
	impl.state.bind_vertex_buffer(binding, gl_buffer->buffer, offset, impl.binding[binding].stride);
}
void gen::gl::Device::bind_index_buffer(const Buffer& buffer, bool index_ushort) {
	internal::GLBuffer* gl_buffer = (internal::GLBuffer*)buffer.get();
	if (!gl_buffer) throw gen::gl::RuntimeException("Buffer is null");
	gl_buffer->init();
	impl.state.bind_buffer(GL_ELEMENT_ARRAY_BUFFER, gl_buffer->buffer);
	impl.index_ushort = index_ushort;
}
void gen::gl::Device::draw(uint32_t vertex_count, uint32_t first_vertex, uint32_t instance_count, uint32_t first_instance) {
	bool primitive_restart = impl.state.PRIMITIVE_RESTART;
	if (primitive_restart) impl.state.primitive_restart(false);

	if (first_instance) glDrawArraysInstancedBaseInstance(impl.active_topology, first_vertex, vertex_count, instance_count, first_instance);
	else {
		if (instance_count == 1) glDrawArrays(impl.active_topology, first_vertex, vertex_count);
		else glDrawArraysInstanced(impl.active_topology, first_vertex, vertex_count, instance_count);
	}

	if (primitive_restart) impl.state.primitive_restart(true);
}
void gen::gl::Device::draw_indexed(uint32_t index_count, uint32_t first_index, int32_t first_vertex, uint32_t instance_count, uint32_t first_instance) {
	uint32_t gl_index_type = GL_UNSIGNED_INT;
	size_t index_size = 4;
	if (impl.index_ushort) {
		gl_index_type = GL_UNSIGNED_SHORT;
		index_size = 2;
#ifndef GEN_GLES
		impl.state.primitive_restart_index(UINT16_MAX - 1);
#endif
	}
	else {
#ifndef GEN_GLES
		impl.state.primitive_restart_index(UINT32_MAX - 1);
#endif
	}
	if ((instance_count == 1) && !first_instance) {
		if (first_vertex) glDrawElementsBaseVertex(impl.active_topology, index_count, gl_index_type, (void*)(first_index * index_size), first_vertex);
		else glDrawElements(impl.active_topology, index_count, gl_index_type, (void*)(first_index * index_size));
	}
	else {
		if (first_instance) {
			if (first_vertex) glDrawElementsInstancedBaseVertexBaseInstance(impl.active_topology, index_count, gl_index_type, (void*)(first_index * index_size), instance_count, first_vertex, first_instance);
			else glDrawElementsInstancedBaseInstance(impl.active_topology, index_count, gl_index_type, (void*)(first_index * index_size), instance_count, first_instance);
		}
		else {
			if (first_vertex) glDrawElementsInstancedBaseVertex(impl.active_topology, index_count, gl_index_type, (void*)(first_index * index_size), instance_count, first_vertex);
			else glDrawElementsInstanced(impl.active_topology, index_count, gl_index_type, (void*)(first_index * index_size), instance_count);
		}
	}
}
void gen::gl::Device::draw_indirect(Buffer buffer, uint32_t offset, uint32_t count, uint32_t stride) {
	internal::GLBuffer* gl_buffer = (internal::GLBuffer*)buffer.get();
	if (!gl_buffer) throw gen::gl::RuntimeException("Buffer is null");
	gl_buffer->init();
	impl.state.bind_buffer(GL_DRAW_INDIRECT_BUFFER, gl_buffer->buffer);

	bool primitive_restart = impl.state.PRIMITIVE_RESTART;
	if (primitive_restart) impl.state.primitive_restart(false);

	if (count) glMultiDrawArraysIndirect(impl.active_topology, (const void*)(intptr_t)offset, count, stride);
	else glDrawArraysIndirect(impl.active_topology, (const void*)(intptr_t)offset);

	if (primitive_restart) impl.state.primitive_restart(true);
}
void gen::gl::Device::draw_indexed_indirect(Buffer buffer, uint32_t offset, uint32_t count, uint32_t stride) {
	internal::GLBuffer* gl_buffer = (internal::GLBuffer*)buffer.get();
	if (!gl_buffer) throw gen::gl::RuntimeException("Buffer is null");
	gl_buffer->init();
	impl.state.bind_buffer(GL_DRAW_INDIRECT_BUFFER, gl_buffer->buffer);

	uint32_t gl_index_type = GL_UNSIGNED_INT;
	if (impl.index_ushort) {
		gl_index_type = GL_UNSIGNED_SHORT;
#ifndef GEN_GLES
		impl.state.primitive_restart_index(UINT16_MAX - 1);
#endif
	}
	else {
#ifndef GEN_GLES
		impl.state.primitive_restart_index(UINT32_MAX - 1);
#endif
	}

	if (count) glMultiDrawElementsIndirect(impl.active_topology, gl_index_type, (const void*)(intptr_t)offset, count, stride);
	else glDrawElementsIndirect(impl.active_topology, gl_index_type, (const void*)(intptr_t)offset);
}
void gen::gl::Device::dispatch(uint32_t x, uint32_t y, uint32_t z) {
	glDispatchCompute(x, y, z);
}
void gen::gl::Device::memory_barrier(uint32_t barrier) {
	GLbitfield gl_barriers = 0;
	if (barrier & gen::gl::Barrier::VERTEX_ATTRIB) gl_barriers |= GL_VERTEX_ATTRIB_ARRAY_BARRIER_BIT;
	if (barrier & gen::gl::Barrier::INDEX) gl_barriers |= GL_ELEMENT_ARRAY_BARRIER_BIT;
	if (barrier & gen::gl::Barrier::UNIFORM) gl_barriers |= GL_UNIFORM_BARRIER_BIT;
	if (barrier & gen::gl::Barrier::TEXTURE_FETCH) gl_barriers |= GL_TEXTURE_FETCH_BARRIER_BIT;
	if (barrier & gen::gl::Barrier::SHADER_IMAGE_ACCESS) gl_barriers |= GL_SHADER_IMAGE_ACCESS_BARRIER_BIT;
	if (barrier & gen::gl::Barrier::COMMAND) gl_barriers |= GL_COMMAND_BARRIER_BIT;
	if (barrier & gen::gl::Barrier::PIXEL_BUFFER) gl_barriers |= GL_PIXEL_BUFFER_BARRIER_BIT;
	if (barrier & gen::gl::Barrier::TEXTURE_UPDATE) gl_barriers |= GL_TEXTURE_UPDATE_BARRIER_BIT;
	if (barrier & gen::gl::Barrier::BUFFER_UPDATE) gl_barriers |= GL_BUFFER_UPDATE_BARRIER_BIT;
	if (barrier & gen::gl::Barrier::FRAMEBUFFER) gl_barriers |= GL_FRAMEBUFFER_BARRIER_BIT;
	if (barrier & gen::gl::Barrier::TRANSFORM_FEEDBACK) gl_barriers |= GL_TRANSFORM_FEEDBACK_BARRIER_BIT;
	if (barrier & gen::gl::Barrier::ATOMIC_COUNTER) gl_barriers |= GL_ATOMIC_COUNTER_BARRIER_BIT;
	if (barrier & gen::gl::Barrier::SHADER_STORAGE) gl_barriers |= GL_SHADER_STORAGE_BARRIER_BIT;
	glMemoryBarrier(gl_barriers);
}

GLenum get_gl_depth_func(gen::gl::CompareOp op) {
	if (op == gen::gl::CompareOp::NEVER) return GL_NEVER;
	else if (op == gen::gl::CompareOp::LESS) return GL_LESS;
	else if (op == gen::gl::CompareOp::EQUAL) return GL_EQUAL;
	else if (op == gen::gl::CompareOp::LESS_OR_EQUAL) return GL_LEQUAL;
	else if (op == gen::gl::CompareOp::GREATER) return GL_GREATER;
	else if (op == gen::gl::CompareOp::NOT_EQUAL) return GL_NOTEQUAL;
	else if (op == gen::gl::CompareOp::GREATER_OR_EQUAL) return GL_GEQUAL;
	else if (op == gen::gl::CompareOp::ALWAYS) return GL_ALWAYS;
	return GL_LESS;
}
GLenum get_gl_blend_op(gen::gl::BlendOperation op) {
	if (op == gen::gl::BlendOperation::ADD) return GL_FUNC_ADD;
	else if (op == gen::gl::BlendOperation::MAX) return GL_MAX;
	else if (op == gen::gl::BlendOperation::SUBTRACT) return GL_FUNC_SUBTRACT;
	else if (op == gen::gl::BlendOperation::REVERSE_SUBTRACT) return GL_FUNC_REVERSE_SUBTRACT;
	else if (op == gen::gl::BlendOperation::MIN) return GL_MIN;
	return GL_FUNC_ADD;
}
GLenum get_gl_blend_factor(gen::gl::BlendFactor factor) {
	if (factor == gen::gl::BlendFactor::ZERO) return GL_ZERO;
	else if (factor == gen::gl::BlendFactor::ONE) return GL_ONE;
	else if (factor == gen::gl::BlendFactor::SRC_COLOR) return GL_SRC_COLOR;
	else if (factor == gen::gl::BlendFactor::ONE_MINUS_SRC_COLOR) return GL_ONE_MINUS_SRC_COLOR;
	else if (factor == gen::gl::BlendFactor::DST_COLOR) return GL_DST_COLOR;
	else if (factor == gen::gl::BlendFactor::ONE_MINUS_DST_COLOR) return GL_ONE_MINUS_DST_COLOR;
	else if (factor == gen::gl::BlendFactor::SRC_ALPHA) return GL_SRC_ALPHA;
	else if (factor == gen::gl::BlendFactor::ONE_MINUS_SRC_ALPHA) return GL_ONE_MINUS_SRC_ALPHA;
	else if (factor == gen::gl::BlendFactor::DST_ALPHA) return GL_DST_ALPHA;
	else if (factor == gen::gl::BlendFactor::ONE_MINUS_DST_ALPHA) return GL_ONE_MINUS_DST_ALPHA;
	return GL_ONE;
}

void gen::gl::Device::set_vertex_format(const VertexFormat& format) {
	if (impl.vao == 0) {
		glGenVertexArrays(1, &impl.vao);
		glBindVertexArray(impl.vao);
	}
	memcpy(impl.binding, format.binding, sizeof(VertexBinding) * MAX_VERTEX_BINDINGS);
	for (size_t i = format.attrib_count; i < gen::gl::MAX_VERTEX_ATTRIBUTES; i++) {
		impl.state.attrib_enable(i, false);
	}
	for (size_t i = 0; i < format.attrib_count; i++) {
		impl.state.attrib_enable(i, true);
		gen::gl::internal::FormatInfo finfo = gen::gl::internal::get_format_info(format.attrib[i].format);
		impl.state.vertex_format(i, finfo.vertex_size, finfo.type, finfo.vertex_normalized, format.attrib[i].offset);
		impl.state.vertex_attrib_binding(i, format.attrib[i].binding);
		if (format.binding[format.attrib[i].binding].per_instance) impl.state.vertex_binding_divisor(format.attrib[i].binding, 1);
		else impl.state.vertex_binding_divisor(format.attrib[i].binding, 0);
	}
}
void gen::gl::Device::set_primitive_state(const PrimitiveState& state) {
	if (state.cull_mode == gen::gl::CullMode::NONE) impl.state.cull_face(false);
	else {
		impl.state.cull_face(true);
		if (state.cull_mode == gen::gl::CullMode::FRONT) impl.state.cull_mode(GL_FRONT);
		else if (state.cull_mode == gen::gl::CullMode::BACK) impl.state.cull_mode(GL_BACK);
		else if (state.cull_mode == gen::gl::CullMode::FRONT_AND_BACK) impl.state.cull_mode(GL_FRONT_AND_BACK);
	}

	if (state.front_ccw) impl.state.front_face(GL_CCW);
	else impl.state.front_face(GL_CW);

	if (state.topology == gen::gl::PrimitiveTopology::POINT_LIST) impl.active_topology = GL_POINTS;
	else if (state.topology == gen::gl::PrimitiveTopology::LINE_LIST) impl.active_topology = GL_LINES;
	else if (state.topology == gen::gl::PrimitiveTopology::LINE_STRIP) impl.active_topology = GL_LINE_STRIP;
	else if (state.topology == gen::gl::PrimitiveTopology::TRIANGLE_LIST) impl.active_topology = GL_TRIANGLES;
	else if (state.topology == gen::gl::PrimitiveTopology::TRIANGLE_STRIP) impl.active_topology = GL_TRIANGLE_STRIP;
}
void gen::gl::Device::set_depth_state(const DepthState& state) {
	if (state.enabled) {
		if (state.depth_test) {
			impl.state.depth_test(true);
			impl.state.depth_mask(state.depth_write);
			impl.state.depth_func(get_gl_depth_func(state.compare_op));
		}
		else {
			if (state.depth_write) {
				impl.state.depth_test(true);
				impl.state.depth_mask(true);
				impl.state.depth_func(GL_ALWAYS);
			}
			else {
				impl.state.depth_test(false);
				impl.state.depth_mask(false);
				impl.state.depth_func(GL_ALWAYS);
			}
		}
	}
	else {
		impl.state.depth_test(false);
		impl.state.depth_mask(false);
		impl.state.depth_func(GL_ALWAYS);
	}
}
void gen::gl::Device::set_blend_state(const BlendState& state) {
	impl.state.blend(state.enabled);
	if (state.enabled) {
		impl.state.blend_color(state.blend_color[0], state.blend_color[1], state.blend_color[2], state.blend_color[3]);
		impl.state.blend_equation(get_gl_blend_op(state.color_op), get_gl_blend_op(state.alpha_op));
		impl.state.blend_func(get_gl_blend_factor(state.src_color), get_gl_blend_factor(state.dst_color), get_gl_blend_factor(state.src_alpha), get_gl_blend_factor(state.dst_alpha));
	}
}
void gen::gl::Device::set_framebuffer_state(const FramebufferState& state) {
	uint32_t fbo = impl.get_fbo(state);
	impl.state.bind_fbo(GL_FRAMEBUFFER, fbo);
	if (fbo != 0) {
		GLenum buffers[MAX_COLOR_ATTACHMENTS];
		for (size_t i = 0; i < state.color_attachment_count; i++) buffers[i] = GL_COLOR_ATTACHMENT0 + i;
		impl.state.draw_buffers(buffers, state.color_attachment_count);
	}
}
void gen::gl::Device::set_default_framebuffer() {
	impl.state.bind_fbo(GL_FRAMEBUFFER, 0);
}
void gen::gl::Device::clear_framebuffer(const ClearDesc& desc) {
	bool scissor_enabled = impl.state.SCISSOR_ENABLE;
	if (scissor_enabled) impl.state.scissor_enable(false);

	for (size_t i = 0; i < gen::gl::MAX_COLOR_ATTACHMENTS; i++) {
		if (desc.color[i].clear) {
			glClearBufferfv(GL_COLOR, i, desc.color[i].clear_color.data);
		}
	}
	if (desc.do_clear_depth) {
		bool depth_mask = impl.state.DEPTH_MASK;
		if (!depth_mask) impl.state.depth_mask(true);
		glClearBufferfi(GL_DEPTH_STENCIL, 0, desc.clear_depth, 0);
		if (!depth_mask) impl.state.depth_mask(false);
	}

	if (scissor_enabled) impl.state.scissor_enable(true);
}
void gen::gl::Device::blit_framebuffer(const FramebufferState& src, const FramebufferState& dst, gen::vec2u src_off, gen::vec2u src_size, gen::vec2u dst_off, gen::vec2u dst_size, gen::gl::Filtering filter) {
	uint32_t fbo_src = impl.get_fbo(src);
	uint32_t fbo_dst = impl.get_fbo(dst);
	impl.state.bind_fbo(GL_READ_FRAMEBUFFER, fbo_src);
	impl.state.bind_fbo(GL_DRAW_FRAMEBUFFER, fbo_dst);
	uint32_t gl_filter = GL_NEAREST;
	if (filter == gen::gl::Filtering::LINEAR) gl_filter = GL_LINEAR;
	glBlitFramebuffer(src_off.x, src_off.y, src_size.x, src_size.y, dst_off.x, dst_off.y, dst_size.x, dst_size.y, GL_COLOR_BUFFER_BIT, gl_filter);
}
void gen::gl::Device::set_viewport(const Viewport& viewport) {
	impl.state.viewport(viewport.pos.x, viewport.pos.y, viewport.size.x, viewport.size.y);
	impl.state.depth_min_max(viewport.min_depth, viewport.max_depth);
}
void gen::gl::Device::set_scissor(const Scissor& scissor) {
	impl.state.scissor_enable(scissor.enabled);
	impl.state.scissor(scissor.pos.x, impl.state.VIEWPORT_H - (scissor.pos.y + scissor.size.y), scissor.size.x, scissor.size.y);
}
void gen::gl::Device::set_program(const Program& program) {
	internal::GLProgram* gl_program = (internal::GLProgram*)program.get();
	if (!gl_program) throw gen::gl::RuntimeException("Program is null");
	gl_program->init();
	impl.state.bind_program(gl_program->program);
}
