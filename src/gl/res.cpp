// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#ifdef GEN_MOD_RESOURCES

#include <gongen/gl/gl.hpp>
#include <gongen/resources/kinds.hpp>
#include <gongen/common/error.hpp>

using namespace gen;
using namespace gen::gl;

void load_shader_stage(res::KindHandler* handler, const res::Identifier& id, ShaderStage& stage, SeekableDataSource* data) {
	if (!stage.name.empty()) {
		throw gen::ResFilesException::createf("Shader stage '%s' already defined in joined file (all stages in one).", res::Identifier::to_str(id).c_str());
	}
	stage.name = res::Identifier::to_str(id);
	data->read(stage.code.reserve(data->size()), data->size());
}
void load_shader_vertex(res::KindHandler* handler, const res::Identifier& id, void** item, SeekableDataSource* data) {
	load_shader_stage(handler, id, ((ProgramDesc*) *item)->vertex, data);
}
void load_shader_fragment(res::KindHandler* handler, const res::Identifier& id, void** item, SeekableDataSource* data) {
	load_shader_stage(handler, id, ((ProgramDesc*) *item)->fragment, data);
}
void load_shader_compute(res::KindHandler* handler, const res::Identifier& id, void** item, SeekableDataSource* data) {
	load_shader_stage(handler, id, ((ProgramDesc*) *item)->compute, data);
}
void load_shader_joined(res::KindHandler* handler, const res::Identifier& id, void** item, SeekableDataSource* data) {
	auto* desc = (ProgramDesc*) *item;
	if (!desc->vertex.name.empty() || !desc->fragment.name.empty() || !desc->compute.name.empty()) {
		throw gen::ResFilesException::createf("Shader '%s' already defined in separate files (one per stage).", res::Identifier::to_str(id).c_str());
	}
	desc->vertex.name = res::Identifier::to_str(id);
	data->read(desc->vertex.code.reserve(data->size()), data->size());
	desc->fragment = desc->vertex;
}

struct Shader {
	ProgramDesc desc;
	Program program = nullptr;
};
struct ProgramKindHandler : public res::KindHandler {
	gl::Device* device;
	String id;
	DynamicArray<String> definitions;

	explicit ProgramKindHandler(gl::Device* device, const String& id, const DynamicArray<String>& definitions) {
		this->device = device;
		this->id = id;
		this->definitions = definitions;
	}

	String get_id() const override {
		return this->id;
	}
	gen::Buffer<res::KindFile> get_files() const override {
		gen::Buffer<res::KindFile> ret;
		ret.add(res::KindFile {
			.extension = "glsl",
			.is_optional = true,
			.load = { .data = load_shader_joined },
		});
		ret.add(res::KindFile {
			.extension = "vert.glsl",
			.is_optional = true,
			.load = { .data = load_shader_vertex },
		});
		ret.add(res::KindFile {
			.extension = "vert",
			.is_optional = true,
			.load = { .data = load_shader_vertex },
		});
		ret.add(res::KindFile {
			.extension = "frag.glsl",
			.is_optional = true,
			.load = { .data = load_shader_fragment },
		});
		ret.add(res::KindFile {
			.extension = "frag",
			.is_optional = true,
			.load = { .data = load_shader_fragment },
		});
		ret.add(res::KindFile {
			.extension = "comp.glsl",
			.is_optional = true,
			.load = { .data = load_shader_compute },
		});
		ret.add(res::KindFile {
			.extension = "comp",
			.is_optional = true,
			.load = { .data = load_shader_compute },
		});
		return ret;
	}
	void create(void** item) override {
		auto* desc = new ProgramDesc();
		desc->defines = this->definitions;
		*item = desc;
	}
	void accept(void** item) override {
		auto* desc = (ProgramDesc*) *item;
		auto* shader = new Shader();
		*item = shader;
		shader->desc = *desc;
		delete desc;
		if (!this->device) return;
		shader->program = this->device->create_program(shader->desc);
	}
	void reaccept(void** item) override {
		if (!this->device) return;
		auto* shader = (Shader*) *item;
		shader->program = this->device->create_program(shader->desc);
	}
	void cancel(void** item) override {
		delete (ProgramDesc*) *item;
	}
	void destroy(void** item) override {
		delete (Shader*) *item;
	}
	virtual void* get_res(void** item) override {
		auto* shader = (Shader*) *item;
		if (!this->device) return &shader->desc;
		return &shader->program;
	}
};

res::KindHandler* gen::gl::program_resource_handler(gl::Device* device, const String& kind, const DynamicArray<String>& definitions) {
	return new ProgramKindHandler(device, kind, definitions);
}

#endif
