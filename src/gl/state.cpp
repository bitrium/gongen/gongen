// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include "gl_internal.hpp"

void gen::gl::internal::GLState::bind_buffer(uint32_t target, uint32_t buffer) {
	uint32_t* buffer_state = nullptr;
	if (target == GL_ARRAY_BUFFER) buffer_state = &STATE_GL_ARRAY_BUFFER;
	else if (target == GL_COPY_READ_BUFFER) buffer_state = &STATE_GL_COPY_READ_BUFFER;
	else if (target == GL_COPY_WRITE_BUFFER) buffer_state = &STATE_GL_COPY_WRITE_BUFFER;
	else if (target == GL_ELEMENT_ARRAY_BUFFER) buffer_state = &STATE_GL_ELEMENT_ARRAY_BUFFER;
	else if (target == GL_PIXEL_PACK_BUFFER) buffer_state = &STATE_GL_PIXEL_PACK_BUFFER;
	else if (target == GL_PIXEL_UNPACK_BUFFER) buffer_state = &STATE_GL_PIXEL_UNPACK_BUFFER;
	else if (target == GL_UNIFORM_BUFFER) buffer_state = &STATE_GL_UNIFORM_BUFFER;
	else if (target == GL_DRAW_INDIRECT_BUFFER) buffer_state = &STATE_GL_DRAW_INDIRECT_BUFFER;
	else return;

	if (buffer_state && (*buffer_state != buffer)) {
		*buffer_state = buffer;
		glBindBuffer(target, buffer);
	}
}
void gen::gl::internal::GLState::bind_storage_buffer(uint32_t index, uint32_t buffer, size_t offset, size_t size) {
	if (index >= gen::gl::internal::MAX_BUFFER_SLOTS) return;
	IndexedBuffer* ibuffer = nullptr;
	ibuffer = &INDEXED_GL_STORAGE_BUFFER[index];

	if ((ibuffer->buffer != buffer) || (ibuffer->offset != offset) || (ibuffer->size != size)) {
		ibuffer->buffer = buffer;
		ibuffer->offset = offset;
		ibuffer->size = size;
		if ((offset == 0) && (size == 0)) glBindBufferBase(GL_SHADER_STORAGE_BUFFER, index, buffer);
		else glBindBufferRange(GL_SHADER_STORAGE_BUFFER, index, buffer, offset, size);
	}
}
void gen::gl::internal::GLState::bind_uniform_buffer(uint32_t index, uint32_t buffer, size_t offset, size_t size) {
	if (index >= gen::gl::internal::MAX_BUFFER_SLOTS) return;
	IndexedBuffer* ibuffer = nullptr;
	ibuffer = &INDEXED_GL_UNIFORM_BUFFER[index];
	
	if ((ibuffer->buffer != buffer) || (ibuffer->offset != offset) || (ibuffer->size != size)) {
		ibuffer->buffer = buffer;
		ibuffer->offset = offset;
		ibuffer->size = size;
		if ((offset == 0) && (size == 0)) glBindBufferBase(GL_UNIFORM_BUFFER, index, buffer);
		else glBindBufferRange(GL_UNIFORM_BUFFER, index, buffer, offset, size);
	}
}

void gen::gl::internal::GLState::bind_texture(uint32_t target, uint32_t slot, uint32_t texture) {
	if ((slot < gen::gl::MAX_TEXTURE_SLOTS) && (TEXTURE[slot] != texture)) {
		if (TEXTURE_UNIT != slot) {
			TEXTURE_UNIT = slot;
			glActiveTexture(GL_TEXTURE0 + slot);
		}
		TEXTURE[slot] = texture;
		glBindTexture(target, texture);
	}
}
void gen::gl::internal::GLState::bind_sampler(uint32_t slot, uint32_t sampler) {
	if ((slot < gen::gl::MAX_TEXTURE_SLOTS) && (SAMPLER[slot] != sampler)) {
		SAMPLER[slot] = sampler;
		glBindSampler(slot, sampler);
	}
}

void gen::gl::internal::GLState::bind_vao(uint32_t vao) {
	if (this->VAO != vao) {
		this->VAO = vao;
		glBindVertexArray(vao);
	}
}
void gen::gl::internal::GLState::bind_vertex_buffer(uint32_t index, uint32_t buffer, size_t offset, size_t stride) {
	if (index >= gen::gl::internal::MAX_BUFFER_SLOTS) return;
	IndexedBuffer* ibuffer = nullptr;
	ibuffer = &VERTEX_BUFFER[index];

	if ((ibuffer->buffer != buffer) || (ibuffer->offset != offset) || (ibuffer->size != stride)) {
		ibuffer->buffer = buffer;
		ibuffer->offset = offset;
		ibuffer->size = stride;
		glBindVertexBuffer(index, buffer, offset, stride);
	}
}
void gen::gl::internal::GLState::attrib_enable(uint32_t attrib, bool enabled) {
	if (attrib >= gen::gl::MAX_VERTEX_ATTRIBUTES) return;
	if (VERTEX_ATTRIB_ENABLED[attrib] != enabled) {
		if (VERTEX_ATTRIB_ENABLED[attrib]) glDisableVertexAttribArray(attrib);
		else glEnableVertexAttribArray(attrib);
		VERTEX_ATTRIB_ENABLED[attrib] = enabled;
	}
}
void gen::gl::internal::GLState::vertex_attrib_binding(uint32_t attrib, uint32_t binding) {
	if (attrib >= gen::gl::MAX_VERTEX_ATTRIBUTES) return;
	if (VERTEX_ATTRIB_BINDING[attrib] != binding) {
		glVertexAttribBinding(attrib, binding);
		VERTEX_ATTRIB_BINDING[attrib] = binding;
	}
}
void gen::gl::internal::GLState::vertex_binding_divisor(uint32_t binding, uint32_t divisor) {
	if (binding >= gen::gl::MAX_VERTEX_BINDINGS) return;
	if (VERTEX_BINDING_DIVISOR[binding] != divisor) {
		glVertexBindingDivisor(binding, divisor);
		VERTEX_BINDING_DIVISOR[binding] = divisor;
	}
}
void gen::gl::internal::GLState::vertex_format(uint32_t attrib, uint32_t size, uint32_t type, bool normalized, uint32_t offset) {
	if (attrib >= gen::gl::MAX_VERTEX_ATTRIBUTES) return;
	VertexFormat* vf = &VERTEX_FORMAT[attrib];
	if ((type != GL_FLOAT) && !(normalized)) {
		if ((vf->size != size) || (vf->type != type) || (vf->offset != offset)) {
			glVertexAttribIFormat(attrib, size, type, offset);
			vf->size = size;
			vf->type = type;
			vf->offset = offset;
		}
	}
	else {
		if ((vf->size != size) || (vf->type != type) || (vf->offset != offset) || (vf->normalized != normalized)) {
			glVertexAttribFormat(attrib, size, type, normalized, offset);
			vf->size = size;
			vf->type = type;
			vf->offset = offset;
			vf->normalized = normalized;
		}
	}
}

void gen::gl::internal::GLState::bind_fbo(uint32_t target, uint32_t fbo) {
	if (target == GL_FRAMEBUFFER) {
		if ((this->STATE_GL_READ_FRAMEBUFFER != fbo) || (this->STATE_GL_DRAW_FRAMEBUFFER != fbo)) {
			this->STATE_GL_READ_FRAMEBUFFER = fbo;
			this->STATE_GL_DRAW_FRAMEBUFFER = fbo;
			glBindFramebuffer(GL_FRAMEBUFFER, fbo);
		}
	}
	else if (target == GL_DRAW_FRAMEBUFFER) {
		if (this->STATE_GL_DRAW_FRAMEBUFFER != fbo) {
			this->STATE_GL_DRAW_FRAMEBUFFER = fbo;
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fbo);
		}
	}
	else if (target == GL_READ_FRAMEBUFFER) {
		if (this->STATE_GL_READ_FRAMEBUFFER != fbo) {
			this->STATE_GL_READ_FRAMEBUFFER = fbo;
			glBindFramebuffer(GL_READ_FRAMEBUFFER, fbo);
		}
	}
}
void gen::gl::internal::GLState::clear_color(float r, float g, float b, float a) {
	if ((CLEAR_COLOR[0] != r) || (CLEAR_COLOR[1] != g) || (CLEAR_COLOR[2] != b) || (CLEAR_COLOR[3] != a)) {
		CLEAR_COLOR[0] = r;
		CLEAR_COLOR[1] = g;
		CLEAR_COLOR[2] = b;
		CLEAR_COLOR[3] = a;
		glClearColor(r, g, b, a);
	}
}
void gen::gl::internal::GLState::clear_depth(float v) {
	if (CLEAR_DEPTH != v) {
		CLEAR_DEPTH = v;
		glClearDepth(v);
	}
}
void gen::gl::internal::GLState::draw_buffers(uint32_t* buffers, size_t count) {
	if (count > gen::gl::MAX_COLOR_ATTACHMENTS) count = gen::gl::MAX_COLOR_ATTACHMENTS;
	if (count == 0) {
		if (DRAW_BUFFERS_COUNT != count) glDrawBuffers(0, nullptr);
	} else {
		if (DRAW_BUFFERS_COUNT == count) {
			if (memcmp(DRAW_BUFFERS, buffers, sizeof(uint32_t) * count)) {
				glDrawBuffers(count, buffers);
			}
		}
		else {
			for (size_t i = 0; i < count; i++) {
				if (DRAW_BUFFERS[i] != buffers[i]) {
					glDrawBuffers(count, buffers);
					break;
				}
			}
		}
	}
	DRAW_BUFFERS_COUNT = count;
	memset(DRAW_BUFFERS, 0, sizeof(uint32_t) * count);
	memcpy(DRAW_BUFFERS, buffers, sizeof(uint32_t) * count);
}

void gen::gl::internal::GLState::viewport(int32_t x, int32_t y, int32_t w, int32_t h) {
	if ((VIEWPORT_X != x) || (VIEWPORT_Y != y) || (VIEWPORT_W != w) || (VIEWPORT_H != h)) {
		VIEWPORT_X = x;
		VIEWPORT_Y = y;
		VIEWPORT_W = w;
		VIEWPORT_H = h;
		glViewport(x, y, w, h);
	}
}
void gen::gl::internal::GLState::depth_min_max(float min, float max) {
	if ((DEPTH_MIN != min) || (DEPTH_MAX != max)) {
		DEPTH_MIN = min;
		DEPTH_MAX = max;
		glDepthRange(min, max);
	}
}

void gen::gl::internal::GLState::scissor_enable(bool enable) {
	if (SCISSOR_ENABLE != enable) {
		SCISSOR_ENABLE = enable;
		if (enable) glEnable(GL_SCISSOR_TEST);
		else glDisable(GL_SCISSOR_TEST);
	}
}
void gen::gl::internal::GLState::scissor(int32_t x, int32_t y, int32_t w, int32_t h) {
	if ((SCISSOR_X != x) || (SCISSOR_Y != y) || (SCISSOR_W != w) || (SCISSOR_H != h)) {
		SCISSOR_X = x;
		SCISSOR_Y = y;
		SCISSOR_W = w;
		SCISSOR_H = h;
		glScissor(x, y, w, h);
	}
}
void gen::gl::internal::GLState::depth_func(uint32_t mode) {
	if (DEPTH_FUNC != mode) {
		DEPTH_FUNC = mode;
		glDepthFunc(mode);
	}
}
void gen::gl::internal::GLState::depth_mask(bool enable) {
	if (DEPTH_MASK != enable) {
		DEPTH_MASK = enable;
		glDepthMask(enable);
	}
}
void gen::gl::internal::GLState::depth_test(bool enable) {
	if (DEPTH_TEST != enable) {
		DEPTH_TEST = enable;
		if (enable) glEnable(GL_DEPTH_TEST);
		else glDisable(GL_DEPTH_TEST);
	}
}

void gen::gl::internal::GLState::cull_mode(uint32_t mode) {
	if (CULL_MODE != mode) {
		CULL_MODE = mode;
		glCullFace(mode);
	}
}
void gen::gl::internal::GLState::cull_face(bool enable) {
	if (CULL_FACE != enable) {
		CULL_FACE = enable;
		if (enable) glEnable(GL_CULL_FACE);
		else glDisable(GL_CULL_FACE);
	}
}
void gen::gl::internal::GLState::front_face(uint32_t mode) {
	if (FRONT_FACE != mode) {
		FRONT_FACE = mode;
		glFrontFace(mode);
	}
}

void gen::gl::internal::GLState::blend(bool enable) {
	if (BLEND_ENABLE != enable) {
		BLEND_ENABLE = enable;
		if (enable) glEnable(GL_BLEND);
		else glDisable(GL_BLEND);
	}
}
void gen::gl::internal::GLState::blend_equation(uint32_t rgb, uint32_t alpha) {
	if ((BLEND_EQUATION_RGB != rgb) || (BLEND_EQUATION_ALPHA != alpha)) {
		BLEND_EQUATION_RGB = rgb;
		BLEND_EQUATION_ALPHA = alpha;
		glBlendEquationSeparate(rgb, alpha);
	}
}
void gen::gl::internal::GLState::blend_func(uint32_t src_rgb, uint32_t dst_rgb, uint32_t src_alpha, uint32_t dst_alpha) {
	if ((BLEND_SRC_RGB != src_rgb) || (BLEND_DST_RGB != dst_rgb) || (BLEND_SRC_ALPHA != src_alpha) || (BLEND_DST_ALPHA != dst_alpha)) {
		BLEND_SRC_RGB = src_rgb;
		BLEND_DST_RGB = dst_rgb;
		BLEND_SRC_ALPHA = src_alpha;
		BLEND_DST_ALPHA = dst_alpha;
		glBlendFuncSeparate(src_rgb, dst_rgb, src_alpha, dst_alpha);
	}
}
void gen::gl::internal::GLState::blend_color(float r, float g, float b, float a) {
	if ((BLEND_COLOR[0] != r) || (BLEND_COLOR[1] != g) || (BLEND_COLOR[2] != b) || (BLEND_COLOR[3] != a)) {
		BLEND_COLOR[0] = r;
		BLEND_COLOR[1] = g;
		BLEND_COLOR[2] = b;
		BLEND_COLOR[3] = a;
		glBlendColor(r, g, b, a);
	}
}

void gen::gl::internal::GLState::bind_program(uint32_t program) {
	if (PROGRAM != program) {
		PROGRAM = program;
		glUseProgram(program);
	}
}
void gen::gl::internal::GLState::primitive_restart(bool enable) {
	if (PRIMITIVE_RESTART != enable) {
		PRIMITIVE_RESTART = enable;
#ifdef GEN_GLES
		if (enable) glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
		else glDisable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
#else
		if (enable) glEnable(GL_PRIMITIVE_RESTART);
		else glDisable(GL_PRIMITIVE_RESTART);
#endif
	}
}
void gen::gl::internal::GLState::primitive_restart_index(uint32_t index) {
	if (PRIMITIVE_RESTART_INDEX != index) {
		PRIMITIVE_RESTART_INDEX = index;
		glPrimitiveRestartIndex(index);
	}
}
