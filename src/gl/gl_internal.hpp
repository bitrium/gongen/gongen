// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/gl/gl.hpp>

#define khronos_int8_t int8_t
#define khronos_int16_t int16_t
#define khronos_int32_t int32_t
#define khronos_int64_t int64_t

#define khronos_uint8_t uint8_t
#define khronos_uint16_t uint16_t
#define khronos_uint32_t uint32_t
#define khronos_uint64_t uint64_t

#ifndef GEN_GLES
	#include <glad/gl.h>
#else
	#include <glad/gles2.h>

#define glad_glDebugMessageCallback glad_glDebugMessageCallbackKHR
#define glDebugMessageCallback glDebugMessageCallbackKHR
#define GL_DEBUG_OUTPUT GL_DEBUG_OUTPUT_KHR
#define GL_DEBUG_OUTPUT_SYNCHRONOUS GL_DEBUG_OUTPUT_SYNCHRONOUS_KHR
#define GL_DEBUG_TYPE_ERROR GL_DEBUG_TYPE_ERROR_KHR
#define GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_KHR
#define GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_KHR
#define GL_DEBUG_TYPE_PORTABILITY GL_DEBUG_TYPE_PORTABILITY_KHR
#define GL_DEBUG_TYPE_PERFORMANCE GL_DEBUG_TYPE_PERFORMANCE_KHR
#define glClearDepth glClearDepthf
#define glDepthRange glDepthRangef

#define gladLoadGL gladLoadGLES2
#endif
