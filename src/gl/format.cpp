// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include "gl_internal.hpp"
#include <gongen/common/error.hpp>

gen::gl::internal::FormatInfo formatTable[] = {
	{GL_UNSIGNED_BYTE, 1, true, GL_R8, GL_RED},
	{GL_BYTE, 1, true, GL_R8_SNORM, GL_RED},
	{GL_UNSIGNED_BYTE, 2, true, GL_RG8, GL_RG},
	{GL_BYTE, 2, true, GL_RG8_SNORM, GL_RG},

	{GL_UNSIGNED_SHORT, 1, true, GL_R16, GL_RED},
	{GL_SHORT, 1, true, GL_R16_SNORM, GL_RED},
	{GL_UNSIGNED_SHORT, 2, true, GL_RG16, GL_RG},
	{GL_SHORT, 2, true, GL_RG16_SNORM, GL_RG},

	{GL_UNSIGNED_BYTE, 4, true, GL_RGBA8, GL_RGBA},
	{GL_BYTE, 4, true, GL_RGBA8_SNORM, GL_RGBA},
	{GL_UNSIGNED_BYTE, 4, true, GL_SRGB8_ALPHA8, GL_RGBA},

	{GL_UNSIGNED_SHORT, 4, true, GL_RGBA16, GL_RGBA},
	{GL_SHORT, 4, true, GL_RGBA16_SNORM, GL_RGBA},

	{GL_BYTE, 1, false, GL_R8I, GL_RED_INTEGER},
	{GL_UNSIGNED_BYTE, 1, false, GL_R8UI, GL_RED_INTEGER},
	{GL_SHORT, 1, false, GL_R16I, GL_RED_INTEGER},
	{GL_UNSIGNED_SHORT, 1, false, GL_R16UI, GL_RED_INTEGER},
	{GL_INT, 1, false, GL_R32I, GL_RED_INTEGER},
	{GL_UNSIGNED_INT, 1, false, GL_R32UI, GL_RED_INTEGER},

	{GL_BYTE, 2, false, GL_RG8I, GL_RG_INTEGER},
	{GL_UNSIGNED_BYTE, 2, false, GL_RG8UI, GL_RG_INTEGER},
	{GL_SHORT, 2, false, GL_RG16I, GL_RG_INTEGER},
	{GL_UNSIGNED_SHORT, 2, false, GL_RG16UI, GL_RG_INTEGER},
	{GL_INT, 2, false, GL_RG32I, GL_RG_INTEGER},
	{GL_UNSIGNED_INT, 2, false, GL_RG32UI, GL_RG_INTEGER},

	{GL_BYTE, 4, false, GL_RGBA8I, GL_RGBA_INTEGER},
	{GL_UNSIGNED_BYTE, 4, false, GL_RGBA8UI, GL_RGBA_INTEGER},
	{GL_SHORT, 4, false, GL_RGBA16I, GL_RGBA_INTEGER},
	{GL_UNSIGNED_SHORT, 4, false, GL_RGBA16UI, GL_RGBA_INTEGER},
	{GL_INT, 4, false, GL_RGBA32I, GL_RGBA_INTEGER},
	{GL_UNSIGNED_INT, 4, false, GL_RGBA32UI, GL_RGBA_INTEGER},

	{GL_HALF_FLOAT , 1, false, GL_R16F, GL_RED},
	{GL_FLOAT, 1, false, GL_R32F, GL_RED},
	{GL_HALF_FLOAT , 2, false, GL_RG16F, GL_RG},
	{GL_FLOAT, 2, false, GL_RG32F, GL_RG},
	{GL_HALF_FLOAT , 4, false, GL_RGBA16F, GL_RGBA},
	{GL_FLOAT, 4, false, GL_RGBA32F, GL_RGBA},
	{GL_INT, 3, false, GL_RGB32I, GL_RGB_INTEGER},
	{GL_UNSIGNED_INT, 3, false, GL_RGB32UI, GL_RGB_INTEGER},
	{GL_FLOAT, 3, false, GL_RGB32F, GL_RGB},

	{GL_INT_2_10_10_10_REV, 4, false, GL_RGB10_A2, GL_RGBA},
	{GL_UNSIGNED_INT_2_10_10_10_REV, 4, false, GL_RGB10_A2UI, GL_RGBA},
	{GL_UNSIGNED_INT_10F_11F_11F_REV , 3, false, GL_R11F_G11F_B10F, GL_RGB},
	{GL_UNSIGNED_INT_5_9_9_9_REV, 3, false, GL_RGB9_E5, GL_RGB},

	{GL_UNSIGNED_SHORT, 1, false, GL_DEPTH_COMPONENT16, GL_DEPTH_COMPONENT},
	{GL_UNSIGNED_INT_24_8, 1, false, GL_DEPTH24_STENCIL8, GL_DEPTH_STENCIL},
	{GL_FLOAT, 1, false, GL_DEPTH_COMPONENT32F, GL_DEPTH_COMPONENT},
};

gen::gl::internal::FormatInfo gen::gl::internal::get_format_info(gen::gl::Format format) {
	if ((size_t)format >= (sizeof(formatTable) / sizeof(gen::gl::internal::FormatInfo))) throw gen::gl::RuntimeException("Invalid GPU Format");
	return formatTable[(size_t)format];
}

uint8_t formatSizeTable[] = {
	1, 1, 2, 2,
	2, 2, 4, 4,
	4, 4, 4,
	8, 8,

	1, 1, 2, 2, 4, 4,
	2, 2, 4, 4, 8, 8,
	4, 4, 8, 8, 16, 16,

	2, 4, 4, 8, 8, 16,
	12, 12, 12,

	4, 4, 4, 4,

	2, 4, 4
};

size_t gen::gl::get_texture_size(Format format, vec3z size, size_t layers) {
	if ((size_t)format > (sizeof(formatSizeTable) / sizeof(uint8_t))) throw gen::gl::RuntimeException("Invalid GPU Format");
	return (formatSizeTable[(size_t)format] * size.x * size.y * size.z * layers);
}

size_t gen::gl::get_mip_levels(gen::vec3z size, bool compressed) {
	size_t out = 1;
	while (true) {
		if (compressed) {
			if ((size.x <= 4) || (size.y <= 4)) return out;
		}
		else {
			if ((size.x <= 1) || (size.y <= 1)) return out;
		}
		size = size / 2;
		out++;
	}
	return out;
}
