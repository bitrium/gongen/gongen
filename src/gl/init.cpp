// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include "gl_internal.hpp"
#include <gongen/common/error.hpp>
#include <gongen/common/platform/system.hpp>
#include <gongen/common/logger.hpp>
#include <stdio.h>

#ifdef GEN_PLATFORM_LINUX
	#define GLAD_GLX_IMPLEMENTATION
	#include <glad/glx.h>
#endif // GEN_PLATFORM_LINUX

void* gl_create_context(gen::window::wsi::NativeHandles* handles);
void gl_destroy_context(gen::window::wsi::NativeHandles* handles, void* ctx);
void gl_make_context_current(gen::window::wsi::NativeHandles* handles, void* ctx);
void gl_display(gen::window::wsi::NativeHandles* handles, void* ctx);
void* gl_proc(const char* name);

void GLAPIENTRY gl_msg_callback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
	if (type == GL_DEBUG_TYPE_ERROR) {
		gen::log::error(message);
	}
	else if ((type == GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR) || (type == GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR) || (type == GL_DEBUG_TYPE_PORTABILITY) || (type == GL_DEBUG_TYPE_PERFORMANCE)) {
		gen::log::warn(message);
	}
}

gen::gl::Device::Device(gen::window::Window* window, bool debug_mode) {
	impl.window = window;

	window->wsi_func(gen::window::wsi::GET_NATIVE_HANDLES, &impl.wsi_handles);
	impl.context = gl_create_context(&impl.wsi_handles);
	if (impl.context) {
		if (!gladLoadGL((GLADloadfunc)gl_proc)) {
			throw gen::gl::InitException("Could not load OpenGL.");
		}
	}
	glDisable(GL_DITHER);
#ifndef GEN_GLES
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);
#endif

	if (debug_mode && glad_glDebugMessageCallback) {
		glEnable(GL_DEBUG_OUTPUT);
		glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
		glDebugMessageCallback(gl_msg_callback, nullptr);
	}

#ifndef GEN_GLES
	capabilities.first_instance = true;
	capabilities.first_vertex = true;
	capabilities.multi_draw_indirect = true;
	capabilities.glsl_first_instance = GLAD_GL_ARB_shader_draw_parameters;
#endif
	glGetIntegerv(GL_UNIFORM_BUFFER_OFFSET_ALIGNMENT, &capabilities.ubo_offset_aligment);
	glGetIntegerv(GL_SHADER_STORAGE_BUFFER_OFFSET_ALIGNMENT, &capabilities.ssbo_offset_aligment);

	if (GLAD_GL_ARB_direct_state_access) {
		impl.dsa_support = true;
	}

	impl.state.viewport(0, 0, window->get_size().x, window->get_size().y);
}
gen::gl::Device::~Device() {
	gl_make_context_current(&impl.wsi_handles, impl.context);
	gl_destroy_context(&impl.wsi_handles, impl.context);
}

void gen::gl::Device::display() {
	impl.is_context_active(true);
	gl_display(&impl.wsi_handles, impl.context);

	void* res = nullptr;
	while ((res = impl.free_queue_buffer.dequeue())) {
		uint32_t res_u32 = (uint32_t)(intptr_t)res;
		glDeleteBuffers(1, &res_u32);
	}
	while ((res = impl.free_queue_program.dequeue())) glDeleteProgram((uint32_t)(intptr_t)res);
	while ((res = impl.free_queue_sampler.dequeue())) {
		uint32_t res_u32 = (uint32_t)(intptr_t)res;
		glDeleteSamplers(1, &res_u32);
	}
	while ((res = impl.free_queue_texture.dequeue())) {
		uint32_t res_u32 = (uint32_t)(intptr_t)res;
		glDeleteTextures(1, &res_u32);
	}
	for(auto &c : impl.vao_cache) {
		c.life_counter--;
		if (!c.life_counter) {
			glDeleteVertexArrays(1, &c.object);
			c.object = 0;
		}
	}
	for (auto& c : impl.fbo_cache) {
		c.life_counter--;
		if (!c.life_counter) {
			glDeleteFramebuffers(1, &c.object);
			c.object = 0;
		}
	}
}
bool gen::gl::internal::Device::is_context_active(bool force_active) {
	if (force_active) {
		if (gen::thread::id() != this->ctx_thread) {
			gl_make_context_current(&this->wsi_handles, this->context);
			this->ctx_thread = gen::thread::id();
		}
		return true;
	}
	return gen::thread::id() == this->ctx_thread;
}

#ifdef GEN_PLATFORM_WIN32
#include "../common/platform/win32/winapi.hpp"

HGLRC(*wglCreateContextAttribsARB)(HDC hDC, HGLRC hShareContext, const int* attribList);
BOOL(*wglSwapIntervalEXT)(int interval);

#define WGL_CONTEXT_MAJOR_VERSION_ARB 0x2091
#define WGL_CONTEXT_MINOR_VERSION_ARB 0x2092
#define WGL_CONTEXT_FLAGS_ARB 0x2094
#define WGL_CONTEXT_PROFILE_MASK_ARB 0x9126

#define WGL_CONTEXT_DEBUG_BIT_ARB 0x0001
#define WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB 0x0002
#define WGL_CONTEXT_CORE_PROFILE_BIT_ARB 0x00000001
#define WGL_CONTEXT_ES2_PROFILE_BIT_EXT 0x00000004

HMODULE gl_lib = nullptr;

void* gl_create_context(gen::window::wsi::NativeHandles* handles) {
	if(!gl_lib) gl_lib = LoadLibraryA("opengl32");

	PIXELFORMATDESCRIPTOR pfd =
	{
		sizeof(PIXELFORMATDESCRIPTOR),
		1,
		PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,    // Flags
		PFD_TYPE_RGBA,        // The kind of framebuffer. RGBA or palette.
		32,                   // Colordepth of the framebuffer.
		0, 0, 0, 0, 0, 0,
		0,
		0,
		0,
		0, 0, 0, 0,
		24,                   // Number of bits for the depthbuffer
		8,                    // Number of bits for the stencilbuffer
		0,                    // Number of Aux buffers in the framebuffer.
		PFD_MAIN_PLANE,
		0,
		0, 0, 0
	};

	HDC dc = GetDC((HWND)handles->window);
	SetPixelFormat(dc, ChoosePixelFormat(dc, &pfd), &pfd);
	HGLRC context = wglCreateContext(dc);
	if (context == NULL) throw gen::RuntimeException("GL Context Creation Failed: wglCreateContext");
	wglMakeCurrent(dc, context);

	wglCreateContextAttribsARB = (HGLRC(*)(HDC, HGLRC, const int*))wglGetProcAddress("wglCreateContextAttribsARB");
	if (wglCreateContextAttribsARB == NULL) throw gen::RuntimeException("GL Context Creation Failed: wglGetProcAddress");
	wglDeleteContext(context);

#ifndef GEN_GLES
	const int attribList[] = {
		WGL_CONTEXT_MAJOR_VERSION_ARB, 4,
		WGL_CONTEXT_MINOR_VERSION_ARB, 3,
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_DEBUG_BIT_ARB,
		WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_CORE_PROFILE_BIT_ARB,
		0
	};
#else
	const int attribList[] = {
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 1,
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_DEBUG_BIT_ARB,
		WGL_CONTEXT_PROFILE_MASK_ARB, WGL_CONTEXT_ES2_PROFILE_BIT_EXT,
		0
	};
#endif
	context = wglCreateContextAttribsARB(dc, nullptr, attribList);
	if (context == NULL) throw gen::RuntimeException("GL Context Creation Failed: wglCreateContextAttribsARB");
	wglMakeCurrent(dc, context);

	wglSwapIntervalEXT = (BOOL(*)(int))wglGetProcAddress("wglSwapIntervalEXT");
	if (wglSwapIntervalEXT != NULL) wglSwapIntervalEXT(1);

	return (void*)context;
}
void gl_destroy_context(gen::window::wsi::NativeHandles* handles, void* ctx) {
	wglDeleteContext((HGLRC)ctx);
}
void gl_make_context_current(gen::window::wsi::NativeHandles* handles, void* ctx) {
	HDC dc = GetDC((HWND)handles->window);
	wglMakeCurrent(dc, (HGLRC)ctx);
}
void gl_display(gen::window::wsi::NativeHandles* handles, void* ctx) {
	HDC dc = GetDC((HWND)handles->window);
	SwapBuffers(dc);
}
void* gl_proc(const char* name) {
	PROC ptr = wglGetProcAddress(name);
	if (!ptr) {
		return (void*)GetProcAddress(gl_lib, name);
	}
	return (void*)ptr;
}
#endif // GEN_PLATFORM_WIN32
#ifdef GEN_PLATFORM_LINUX
void* gl_create_context(gen::window::wsi::NativeHandles* handles) {
	Display* display = XOpenDisplay(nullptr);
	handles->display = display;
	int screen = DefaultScreen(display);
	XWindowAttributes attributes = {0};
	XGetWindowAttributes(display, handles->window, &attributes);
	if (!gladLoaderLoadGLX(display, screen)) {
		throw gen::RuntimeException("Could not load GLX.");
	}
	int config_count;
	GLXFBConfig* configs = glXGetFBConfigs(display, screen, &config_count);
	GLXFBConfig config = nullptr;
	for (int i = 0; i < config_count; i++) {
		int id;
		int ret = glXGetFBConfigAttrib(display, configs[i], GLX_VISUAL_ID, &id);
		if (ret != Success) {
			continue;
		}
		if (id == attributes.visual->visualid) {
			config = configs[i];
			break;
		}
	}
	if (!config) {
		throw gen::RuntimeException("Could not find matching GLX framebuffer config.");
	}
	static const int attribs[] = {
		GLX_CONTEXT_MAJOR_VERSION_ARB, 4,
		GLX_CONTEXT_MINOR_VERSION_ARB, 3,
		GLX_CONTEXT_FLAGS_ARB, GLX_CONTEXT_DEBUG_BIT_ARB,
		GLX_CONTEXT_PROFILE_MASK_ARB, GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		0
	};
	GLXContext ctx = glXCreateContextAttribsARB(display, config, nullptr, true, attribs);
	if (!ctx) {
		throw gen::RuntimeException("Could not initialize GLX context.");
	}
	gen::free(configs);
	glXMakeCurrent(display, handles->window, ctx);
	return (void*) ctx;
}
void gl_destroy_context(gen::window::wsi::NativeHandles* handles, void* ctx) {
	//TODO
}
void gl_make_context_current(gen::window::wsi::NativeHandles* handles, void* ctx) {
	glXMakeCurrent((Display*) handles->display, handles->window, (GLXContext) ctx);
}
void gl_display(gen::window::wsi::NativeHandles* handles, void* ctx) {
	glXSwapBuffers((Display*) handles->display, handles->window);
}
void* gl_proc(const char* name) {
	return (void*) glXGetProcAddressARB((const GLubyte*) name);
}
#endif // GEN_PLATFORM_LINUX
