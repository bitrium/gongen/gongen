// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include "glfw.hpp"
#ifdef GEN_PLATFORM_WIN32
	#define GLFW_EXPOSE_NATIVE_WIN32
#elif defined(GEN_PLATFORM_LINUX)
	#define GLFW_EXPOSE_NATIVE_X11
#endif // GEN_PLATFORM_WIN32
#include <GLFW/glfw3native.h>
#include <gongen/common/error.hpp>
#include <gongen/common/codecs.hpp>

static gen::input::Key key_glfw_to_gen(int key);

static const GLFWallocator GLFW_ALLOCATOR = {
	.allocate = [](size_t size, void*) { return gen::alloc(size); },
	.reallocate = [](void* ptr, size_t size, void*) { return gen::realloc(ptr, size); },
	.deallocate = [](void* ptr, void*) { return gen::free(ptr); },
};

gen::window::GlfwWindow::GlfwWindow(const gen::String& title, vec2u size, uint32_t flags) {
	if (glfwInit() != GLFW_TRUE) {
		throw gen::RuntimeException("Could not initialize GLFW.");
	}

	glfwInitAllocator(&GLFW_ALLOCATOR);

	glfwWindowHint(GLFW_DECORATED, (int)(!(flags & gen::window::NO_DECORATION)));
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

	this->window = glfwCreateWindow(size.x, size.y, title.c_str(), nullptr, nullptr);
	if (!window) {
		throw gen::RuntimeException("Could not create a window.");
	}

	glfwSetWindowUserPointer(this->window, this);

	glfwSetWindowPosCallback(this->window, [](GLFWwindow* glfw, int x, int y) {
		GlfwWindow* window = (GlfwWindow*) glfwGetWindowUserPointer(glfw);
		gen::event::system()->occur<WindowEvent>(window, WindowEventAction::MOVE, vec2i(x, y));
	});
	glfwSetWindowSizeCallback(this->window, [](GLFWwindow* glfw, int width, int height) {
		GlfwWindow* window = (GlfwWindow*) glfwGetWindowUserPointer(glfw);
		gen::event::system()->occur<WindowEvent>(window, WindowEventAction::RESIZE, vec2i(width, height));
	});
	glfwSetWindowCloseCallback(this->window, [](GLFWwindow* glfw) {
		GlfwWindow* window = (GlfwWindow*) glfwGetWindowUserPointer(glfw);
		gen::event::system()->occur<WindowEvent>(window, WindowEventAction::CLOSE);
	});
	glfwSetWindowFocusCallback(this->window, [](GLFWwindow* glfw, int state) {
		GlfwWindow* window = (GlfwWindow*) glfwGetWindowUserPointer(glfw);
		gen::event::system()->occur<WindowEvent>(window, state ? WindowEventAction::FOCUS : WindowEventAction::UNFOCUS);
	});
	glfwSetWindowMaximizeCallback(this->window, [](GLFWwindow* glfw, int state) {
		GlfwWindow* window = (GlfwWindow*) glfwGetWindowUserPointer(glfw);
		gen::event::system()->occur<WindowEvent>(window, state ? WindowEventAction::MAXIMIZE : WindowEventAction::UNMAXIMIZE);
	});
	glfwSetWindowIconifyCallback(this->window, [](GLFWwindow* glfw, int state) {
		GlfwWindow* window = (GlfwWindow*) glfwGetWindowUserPointer(glfw);
		gen::event::system()->occur<WindowEvent>(window, state ? WindowEventAction::MINIMIZE : WindowEventAction::UNMINIMIZE);
	});
	glfwSetCursorEnterCallback(this->window, [](GLFWwindow* glfw, int state) {
		GlfwWindow* window = (GlfwWindow*) glfwGetWindowUserPointer(glfw);
		gen::event::system()->occur<WindowEvent>(window, state ? WindowEventAction::CURSOR_ENTER : WindowEventAction::CURSOR_EXIT);
	});
}
gen::window::GlfwWindow::~GlfwWindow() {
	delete this->input_src;
	gen::free(this->pressed_keys);
	glfwDestroyWindow(this->window);
	glfwTerminate();
}
bool gen::window::GlfwWindow::is_open() {
	return !glfwWindowShouldClose(this->window);
}

void gen::window::GlfwWindow::on_key(int action, int key) {
	if (action == GLFW_PRESS) {
		int* pressed_key = this->pressed_keys;
		while (*pressed_key > 0) {
			pressed_key++;
			if (pressed_key - this->pressed_keys > 15) return;
		}
		*pressed_key = key;
	} else if (action == GLFW_RELEASE) {
		int* pressed_key = this->pressed_keys;
		while (*pressed_key != key) {
			if (*pressed_key == 0) return;
			pressed_key++;
		}
		if (*(pressed_key + 1) == 0) *pressed_key = 0;
		else *pressed_key = -1;
	}
}

void gen::window::GlfwWindow::poll_events() {
	glfwPollEvents();
	if (this->input_src) {
		int* pressed_key = this->pressed_keys;
		while (*pressed_key != 0) {
			input::Key key = key_glfw_to_gen(*pressed_key);
			gen::event::system()->occur<input::KeyEvent>(this->input_src, input::KeyAction::HOLD, key);
			pressed_key++;
		}
		if (pressed_key != this->pressed_keys) pressed_key--;
		if (*pressed_key == -1) *pressed_key = 0;
	}
}

gen::vec2d gen::window::GlfwWindow::cursor_pos() {
	vec2d vec;
	glfwGetCursorPos(this->window, &vec.x, &vec.y);
	return vec;
}

gen::vec2u gen::window::GlfwWindow::get_size() {
	int x, y;
	glfwGetWindowSize(this->window, &x, &y);
	return vec2u(x, y);
}

void gen::window::GlfwWindow::grab_cursor(bool grab) {
	glfwSetInputMode(this->window, GLFW_CURSOR, grab ? GLFW_CURSOR_DISABLED : GLFW_CURSOR_NORMAL);
}
bool gen::window::GlfwWindow::is_cursor_grabbed() {
	return glfwGetInputMode(this->window, GLFW_CURSOR) == GLFW_CURSOR_DISABLED;
}

void gen::window::GlfwWindow::set_clipboard(const char* text) {
	glfwSetClipboardString(this->window, text);
}
const char* gen::window::GlfwWindow::get_clipboard() {
	return glfwGetClipboardString(this->window);
}

gen::input::InputSource* gen::window::GlfwWindow::input() {
	// TODO gamepad support
	if (this->input_src) return this->input_src;
	this->input_src = new GlfwInputSource(this, UINT8_MAX);
	// pressed_keys:
	// 0 = no more keys pressed (last element is always 0)
	// -1 = key has been released, but more keys follow
	// <key> = the given glfw key is pressed
	this->pressed_keys = (int*) gen::calloc(sizeof(int) * 16);
	glfwSetMouseButtonCallback(this->window, [](GLFWwindow* glfw, int button, int glfw_action, int mods) {
		GlfwWindow* window = (GlfwWindow*) glfwGetWindowUserPointer(glfw);
		input::KeyAction action;
		if (glfw_action == GLFW_PRESS) action = input::KeyAction::PRESS;
		else if (glfw_action == GLFW_RELEASE) action = input::KeyAction::RELEASE;
		else return;
		input::Key key;
		if (button == GLFW_MOUSE_BUTTON_LEFT) key = input::Key::MOUSE_LEFT;
		else if (button == GLFW_MOUSE_BUTTON_MIDDLE) key = input::Key::MOUSE_MIDDLE;
		else if (button == GLFW_MOUSE_BUTTON_RIGHT) key = input::Key::MOUSE_RIGHT;
		else if (button == GLFW_MOUSE_BUTTON_4) key = input::Key::MOUSE_BACK;
		else if (button == GLFW_MOUSE_BUTTON_5) key = input::Key::MOUSE_FORWARD;
		else return;
		gen::event::system()->occur<input::KeyEvent>(window->input_src, action, key);
		window->on_key(glfw_action, button + 1); // buttons start with 0
	});
	glfwSetKeyCallback(this->window, [](GLFWwindow* glfw, int glfw_key, int scancode, int glfw_action, int mods) {
		GlfwWindow* window = (GlfwWindow*) glfwGetWindowUserPointer(glfw);
		input::KeyAction action;
		if (glfw_action == GLFW_PRESS) action = input::KeyAction::PRESS;
		else if (glfw_action == GLFW_RELEASE) action = input::KeyAction::RELEASE;
		else if (glfw_action == GLFW_REPEAT) action = input::KeyAction::REPEAT;
		else return;
		input::Key key = key_glfw_to_gen(glfw_key);
		if (key == input::Key::NONE) return;
		gen::event::system()->occur<input::KeyEvent>(window->input_src, action, key);
		window->on_key(glfw_action, glfw_key);
	});
	glfwSetCursorPosCallback(this->window, [](GLFWwindow* glfw, double x, double y) {
		GlfwWindow* window = (GlfwWindow*) glfwGetWindowUserPointer(glfw);
		gen::event::system()->occur<input::OffsetEvent>(window->input_src, input::Offsetter::MOUSE_CURSOR, vec2d(x, y));
	});
	glfwSetScrollCallback(this->window, [](GLFWwindow* glfw, double x, double y) {
		GlfwWindow* window = (GlfwWindow*) glfwGetWindowUserPointer(glfw);
		gen::event::system()->occur<input::OffsetEvent>(window->input_src, input::Offsetter::MOUSE_SCROLL, vec2d(x, y));
	});
	glfwSetCharCallback(this->window, [](GLFWwindow* glfw, unsigned int codepoint) {
		GlfwWindow* window = (GlfwWindow*)glfwGetWindowUserPointer(glfw);
		gen::String utf8_str = gen::utf8::encode(&codepoint, 1);
		gen::event::system()->occur<input::TextEvent>(window->input_src, utf8_str);
	});
	return this->input_src;
}

void gen::window::GlfwWindow::wsi_func(wsi::WsiFunction function, void* data) {
	if (function == wsi::WsiFunction::GET_NATIVE_HANDLES) {
		wsi::NativeHandles* handles = (wsi::NativeHandles*)data;

#ifdef GEN_PLATFORM_WIN32
		handles->window = glfwGetWin32Window(window);
#elif defined(GEN_PLATFORM_LINUX)
		handles->window = glfwGetX11Window(window);
#endif // GEN_PLATFORM_WIN32
	}
	else GEN_UNIMPLEMENTED;
}

gen::window::Window* gen::window::create(const gen::String& title, vec2u size, uint32_t flags) {
	return new gen::window::GlfwWindow(title, size, flags);
}

gen::window::GlfwInputSource::GlfwInputSource(const GlfwWindow* window, uint8_t gamepad_number)
		: window(window), gamepad_number(gamepad_number) {
}

bool gen::window::GlfwInputSource::houses(const gen::input::InputSource* other) const {
	if (other == this) return true;
	if (this->is_gamepad()) return false;
	auto glfw_other = dynamic_cast<const GlfwInputSource*>(other);
	if (!glfw_other) return false;
	return glfw_other->window == this->window;
}

bool gen::window::GlfwInputSource::is_gamepad() const {
	return this->gamepad_number != UINT8_MAX;
}

static bool key_remap_filled = false;
static gen::input::Key key_remap_table[GLFW_KEY_LAST];

static gen::input::Key key_glfw_to_gen(int key) {
	if (key_remap_filled) goto TRANSLATE;

	memset(key_remap_table, 0, GLFW_KEY_LAST * sizeof(gen::input::Key));

	key_remap_table[GLFW_MOUSE_BUTTON_LEFT + 1] = gen::input::Key::MOUSE_LEFT;
	key_remap_table[GLFW_MOUSE_BUTTON_MIDDLE + 1] = gen::input::Key::MOUSE_MIDDLE;
	key_remap_table[GLFW_MOUSE_BUTTON_RIGHT + 1] = gen::input::Key::MOUSE_RIGHT;
	key_remap_table[GLFW_MOUSE_BUTTON_4 + 1] = gen::input::Key::MOUSE_BACK;
	key_remap_table[GLFW_MOUSE_BUTTON_5 + 1] = gen::input::Key::MOUSE_FORWARD;

	// unassigned: GLFW_KEY_F25, GLFW_KEY_KP_DECIMAL, GLFW_KEY_KP_MINUS
	key_remap_table[GLFW_KEY_SPACE] = gen::input::Key::KB_SPACE;
	key_remap_table[GLFW_KEY_APOSTROPHE] = gen::input::Key::KB_APOSTROPHE;
	key_remap_table[GLFW_KEY_COMMA] = gen::input::Key::KB_COMMA;
	key_remap_table[GLFW_KEY_MINUS] = gen::input::Key::KB_MINUS;
	key_remap_table[GLFW_KEY_PERIOD] = gen::input::Key::KB_PERIOD;
	key_remap_table[GLFW_KEY_SLASH] = gen::input::Key::KB_SLASH;
	key_remap_table[GLFW_KEY_0] = gen::input::Key::KB_0;
	key_remap_table[GLFW_KEY_1] = gen::input::Key::KB_1;
	key_remap_table[GLFW_KEY_2] = gen::input::Key::KB_2;
	key_remap_table[GLFW_KEY_3] = gen::input::Key::KB_3;
	key_remap_table[GLFW_KEY_4] = gen::input::Key::KB_4;
	key_remap_table[GLFW_KEY_5] = gen::input::Key::KB_5;
	key_remap_table[GLFW_KEY_6] = gen::input::Key::KB_6;
	key_remap_table[GLFW_KEY_7] = gen::input::Key::KB_7;
	key_remap_table[GLFW_KEY_8] = gen::input::Key::KB_8;
	key_remap_table[GLFW_KEY_9] = gen::input::Key::KB_9;
	key_remap_table[GLFW_KEY_SEMICOLON] = gen::input::Key::KB_SEMICOLON;
	key_remap_table[GLFW_KEY_EQUAL] = gen::input::Key::KB_EQUAL;
	key_remap_table[GLFW_KEY_A] = gen::input::Key::KB_A;
	key_remap_table[GLFW_KEY_B] = gen::input::Key::KB_B;
	key_remap_table[GLFW_KEY_C] = gen::input::Key::KB_C;
	key_remap_table[GLFW_KEY_D] = gen::input::Key::KB_D;
	key_remap_table[GLFW_KEY_E] = gen::input::Key::KB_E;
	key_remap_table[GLFW_KEY_F] = gen::input::Key::KB_F;
	key_remap_table[GLFW_KEY_G] = gen::input::Key::KB_G;
	key_remap_table[GLFW_KEY_H] = gen::input::Key::KB_H;
	key_remap_table[GLFW_KEY_I] = gen::input::Key::KB_I;
	key_remap_table[GLFW_KEY_J] = gen::input::Key::KB_J;
	key_remap_table[GLFW_KEY_K] = gen::input::Key::KB_K;
	key_remap_table[GLFW_KEY_L] = gen::input::Key::KB_L;
	key_remap_table[GLFW_KEY_M] = gen::input::Key::KB_M;
	key_remap_table[GLFW_KEY_N] = gen::input::Key::KB_N;
	key_remap_table[GLFW_KEY_O] = gen::input::Key::KB_O;
	key_remap_table[GLFW_KEY_P] = gen::input::Key::KB_P;
	key_remap_table[GLFW_KEY_Q] = gen::input::Key::KB_Q;
	key_remap_table[GLFW_KEY_R] = gen::input::Key::KB_R;
	key_remap_table[GLFW_KEY_S] = gen::input::Key::KB_S;
	key_remap_table[GLFW_KEY_T] = gen::input::Key::KB_T;
	key_remap_table[GLFW_KEY_U] = gen::input::Key::KB_U;
	key_remap_table[GLFW_KEY_W] = gen::input::Key::KB_W;
	key_remap_table[GLFW_KEY_X] = gen::input::Key::KB_X;
	key_remap_table[GLFW_KEY_Y] = gen::input::Key::KB_Y;
	key_remap_table[GLFW_KEY_Z] = gen::input::Key::KB_Z;
	key_remap_table[GLFW_KEY_LEFT_BRACKET] = gen::input::Key::KB_LEFT_SQUARE_BRACKET;
	key_remap_table[GLFW_KEY_BACKSLASH] = gen::input::Key::KB_BACKSLASH;
	key_remap_table[GLFW_KEY_RIGHT_BRACKET] = gen::input::Key::KB_RIGHT_SQUARE_BRACKET;
	key_remap_table[GLFW_KEY_GRAVE_ACCENT] = gen::input::Key::KB_TILDE;
	key_remap_table[GLFW_KEY_ESCAPE] = gen::input::Key::KB_ESCAPE;
	key_remap_table[GLFW_KEY_ENTER] = gen::input::Key::KB_ENTER;
	key_remap_table[GLFW_KEY_TAB] = gen::input::Key::KB_TAB;
	key_remap_table[GLFW_KEY_BACKSPACE] = gen::input::Key::KB_BACKSPACE;
	key_remap_table[GLFW_KEY_INSERT] = gen::input::Key::KB_INSERT;
	key_remap_table[GLFW_KEY_DELETE] = gen::input::Key::KB_DELETE;
	key_remap_table[GLFW_KEY_RIGHT] = gen::input::Key::KB_RIGHT;
	key_remap_table[GLFW_KEY_LEFT] = gen::input::Key::KB_LEFT;
	key_remap_table[GLFW_KEY_DOWN] = gen::input::Key::KB_DOWN;
	key_remap_table[GLFW_KEY_UP] = gen::input::Key::KB_UP;
	key_remap_table[GLFW_KEY_PAGE_UP] = gen::input::Key::KB_PAGE_UP;
	key_remap_table[GLFW_KEY_PAGE_DOWN] = gen::input::Key::KB_PAGE_DOWN;
	key_remap_table[GLFW_KEY_HOME] = gen::input::Key::KB_HOME;
	key_remap_table[GLFW_KEY_END] = gen::input::Key::KB_END;
	key_remap_table[GLFW_KEY_CAPS_LOCK] = gen::input::Key::KB_CAPS_LOCK;
	key_remap_table[GLFW_KEY_SCROLL_LOCK] = gen::input::Key::KB_SCROLL_LOCK;
	key_remap_table[GLFW_KEY_NUM_LOCK] = gen::input::Key::KB_NUM_LOCK;
	key_remap_table[GLFW_KEY_PRINT_SCREEN] = gen::input::Key::KB_PRINT_SCREEN;
	key_remap_table[GLFW_KEY_PAUSE] = gen::input::Key::KB_PAUSE;
	key_remap_table[GLFW_KEY_F1] = gen::input::Key::KB_F1;
	key_remap_table[GLFW_KEY_F2] = gen::input::Key::KB_F2;
	key_remap_table[GLFW_KEY_F3] = gen::input::Key::KB_F3;
	key_remap_table[GLFW_KEY_F4] = gen::input::Key::KB_F4;
	key_remap_table[GLFW_KEY_F5] = gen::input::Key::KB_F5;
	key_remap_table[GLFW_KEY_F6] = gen::input::Key::KB_F6;
	key_remap_table[GLFW_KEY_F7] = gen::input::Key::KB_F7;
	key_remap_table[GLFW_KEY_F8] = gen::input::Key::KB_F8;
	key_remap_table[GLFW_KEY_F9] = gen::input::Key::KB_F9;
	key_remap_table[GLFW_KEY_F10] = gen::input::Key::KB_F10;
	key_remap_table[GLFW_KEY_F11] = gen::input::Key::KB_F11;
	key_remap_table[GLFW_KEY_F12] = gen::input::Key::KB_F12;
	key_remap_table[GLFW_KEY_F13] = gen::input::Key::KB_F13;
	key_remap_table[GLFW_KEY_F14] = gen::input::Key::KB_F14;
	key_remap_table[GLFW_KEY_F15] = gen::input::Key::KB_F15;
	key_remap_table[GLFW_KEY_F16] = gen::input::Key::KB_F16;
	key_remap_table[GLFW_KEY_F17] = gen::input::Key::KB_F17;
	key_remap_table[GLFW_KEY_F18] = gen::input::Key::KB_F18;
	key_remap_table[GLFW_KEY_F19] = gen::input::Key::KB_F19;
	key_remap_table[GLFW_KEY_F20] = gen::input::Key::KB_F20;
	key_remap_table[GLFW_KEY_F21] = gen::input::Key::KB_F21;
	key_remap_table[GLFW_KEY_F22] = gen::input::Key::KB_F22;
	key_remap_table[GLFW_KEY_F23] = gen::input::Key::KB_F23;
	key_remap_table[GLFW_KEY_F24] = gen::input::Key::KB_F24;
	key_remap_table[GLFW_KEY_KP_0] = gen::input::Key::KB_NUM_0;
	key_remap_table[GLFW_KEY_KP_1] = gen::input::Key::KB_NUM_1;
	key_remap_table[GLFW_KEY_KP_2] = gen::input::Key::KB_NUM_2;
	key_remap_table[GLFW_KEY_KP_3] = gen::input::Key::KB_NUM_3;
	key_remap_table[GLFW_KEY_KP_4] = gen::input::Key::KB_NUM_4;
	key_remap_table[GLFW_KEY_KP_5] = gen::input::Key::KB_NUM_5;
	key_remap_table[GLFW_KEY_KP_6] = gen::input::Key::KB_NUM_6;
	key_remap_table[GLFW_KEY_KP_7] = gen::input::Key::KB_NUM_7;
	key_remap_table[GLFW_KEY_KP_8] = gen::input::Key::KB_NUM_8;
	key_remap_table[GLFW_KEY_KP_9] = gen::input::Key::KB_NUM_9;
	key_remap_table[GLFW_KEY_KP_DIVIDE] = gen::input::Key::KB_NUM_DIV;
	key_remap_table[GLFW_KEY_KP_MULTIPLY] = gen::input::Key::KB_NUM_MULT;
	key_remap_table[GLFW_KEY_KP_SUBTRACT] = gen::input::Key::KB_NUM_SUB;
	key_remap_table[GLFW_KEY_KP_ADD] = gen::input::Key::KB_NUM_ADD;
	key_remap_table[GLFW_KEY_KP_ENTER] = gen::input::Key::KB_NUM_ENTER;
	key_remap_table[GLFW_KEY_LEFT_SHIFT] = gen::input::Key::KB_LSHIFT;
	key_remap_table[GLFW_KEY_LEFT_CONTROL] = gen::input::Key::KB_LCTRL;
	key_remap_table[GLFW_KEY_LEFT_ALT] = gen::input::Key::KB_LALT;
	key_remap_table[GLFW_KEY_RIGHT_SHIFT] = gen::input::Key::KB_RSHIFT;
	key_remap_table[GLFW_KEY_RIGHT_CONTROL] = gen::input::Key::KB_RCTRL;
	key_remap_table[GLFW_KEY_RIGHT_ALT] = gen::input::Key::KB_RALT;
	key_remap_filled = true;

	TRANSLATE:
	if (key < 0 || key > GLFW_KEY_LAST) return gen::input::Key::NONE;
	return key_remap_table[key];
}
