// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/window/window.hpp>
#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

namespace gen::window {

class GlfwInputSource;

class GlfwWindow : public Window {
	GLFWwindow* window;
	GlfwInputSource* input_src = nullptr;
	DynamicArray<GlfwInputSource> pad_input_srcs;
	int* pressed_keys = nullptr;

	void on_key(int action, int key);
public:
	GlfwWindow(const gen::String& title, vec2u size, uint32_t flags);
	~GlfwWindow();
	bool is_open() override;
	void poll_events() override;
	vec2d cursor_pos() override;
	vec2u get_size() override;

	void grab_cursor(bool grab) override;
	bool is_cursor_grabbed() override;

	void set_clipboard(const char* text) override;
	const char* get_clipboard() override;

	gen::input::InputSource* input() override;

	void wsi_func(wsi::WsiFunction function, void* data) override;
};

class GlfwInputSource : public gen::input::InputSource {
public:
	const GlfwWindow* window;
	uint8_t gamepad_number;

	bool houses(const InputSource* other) const override;
	bool is_gamepad() const;

private:
	GlfwInputSource(const GlfwWindow* window, uint8_t gamepad_number);

	friend class GlfwWindow;
};
} // namespace gen::window
