// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/ecs/simulation.hpp>

using namespace gen::ecs;

Object::Object(uint64_t id, CompBitfield components) {
	this->id = id;
	this->components = components;
	this->component_size = 0;
	for (int i = 0; i < internal::registered_type_count; i++) {
		if (!this->has_component(i)) continue;
		this->component_size += internal::types[i].size;
	}
}
void* Object::get_component_internal(CompId id) {
	if (!this->has_component(id)) return nullptr;
	uint8_t* ptr = (uint8_t*) this + sizeof(Object);
	for (int i = 0; i < id; i++) {
		if (!this->has_component(i)) continue;
		ptr += internal::types[i].size;
	}
	return ptr;
}
bool Object::has_components(CompBitfield components) const {
	return (this->components & components) == components;
}
bool Object::has_component(CompId id) const {
	return this->has_components(comp_id_to_bit(id));
}


void SimulationState::get_rng(gen::rand::Random& random) const {
	if (this->rng_state.empty() || !random.rng.get()) return;
	random.rng->set_state(this->rng_state.ptr(), this->rng_state.size());
}
void SimulationState::set_rng(const gen::rand::Random& random) {
	if (!random.rng) return;
	this->rng_state.clear();
	random.rng->get_state(this->rng_state.reserve(random.rng->state_size()));
}

void Simulation::rewrite_object_map() {
	ObjectMap& objects = this->get_current_object_map();
	objects.clear();
	gen::ByteBuffer& object_buffer = this->get_current_state()->object_buffer;
	size_t off = 0;
	while (off < object_buffer.size()) {
		Object* object = (Object*) object_buffer.ptr(off);
		off += object->get_size();
		if (object->id == INVALID_OBJECT) continue;
		objects[object->id] = object;
	}
}

Simulation::Simulation(size_t state_memory_size, double base_speed) {
	if (state_memory_size < 2) {
		throw gen::ArgumentException("State memory must at least hold two States (activa and current).");
	}
	this->states.clear(true);
	this->states.extend(state_memory_size);
	this->object_maps.clear(true);
	this->object_maps.extend<uint64_t(*)(const uint64_t&)>(state_memory_size, gen::no_hash);
	this->set_base_speed(base_speed);
	// reset and tick to make sure active state is valid
	this->clear();
	this->tick(nullptr); // no components anyways, so null user data is ok
}

bool Simulation::is_valid_tick(uint64_t tick) {
	return tick >= this->get_oldest_tick() && tick <= this->current_tick;
}
size_t Simulation::get_active_tick() {
	return this->active_tick;
}
size_t Simulation::get_current_tick() {
	return this->current_tick;
}
size_t Simulation::get_oldest_tick() {
	uint64_t tick = this->current_tick - this->state_count() + 1;
	if (tick < this->reset_tick) return this->reset_tick;
	return tick;
}

uint64_t Simulation::tick(void* user) {
	SimulationState* previous_state = this->get_current_state();
	ObjectMap& previous_objects = this->get_current_object_map();

	// update components of all objects of the completed tick
	for (Object* object : previous_objects) {
		uint8_t* ptr = (uint8_t*) object;
		ptr += sizeof(Object);
		for (CompId i = 0; i < internal::registered_type_count; i++) {
			if (!object->has_component(i)) continue;
			internal::types[i].update(this, object, ptr, this->real_speed, user);
			ptr += internal::types[i].size;
		}
	}

	// save rng state
	previous_state->set_rng(this->random);

	// change tick
	this->active_tick = this->current_tick++;

	// emit tick event
	auto* event = new TickEvent(this);
	event->tick = this->active_tick;
	event->time = gen::time::get_time();
	if (gen::event::system()) gen::event::system()->occur(event);

	// fill next state's object buffer and object list
	SimulationState* next_state = this->get_current_state();
	ObjectMap& next_objects = this->get_current_object_map();
	next_state->object_buffer.clear();
	next_objects.clear();
	for (Object* object : previous_objects) {
		if (object->get_id() != INVALID_OBJECT) next_state->object_buffer.add(object, object->get_size());
	}
	next_state->next_object_id = previous_state->next_object_id;
	this->rewrite_object_map();

	return this->current_tick;
}
uint64_t Simulation::sprint(uint64_t tick, void* user) {
	if (tick < this->current_tick) return INVALID_TICK;
	uint64_t diff = tick - this->current_tick;
	while (this->tick(user) < tick) {}
	return diff;
}
void Simulation::reset(uint64_t tick, const SimulationState& state) {
	this->reset_tick = tick;
	this->current_tick = tick;
	*this->get_current_state() = state;
	this->rewrite_object_map();
	state.get_rng(this->random);
}
uint64_t Simulation::rewind(uint64_t tick) {
	if (tick == this->current_tick) return 0;
	if (!this->is_valid_tick(tick)) return INVALID_TICK;
	uint64_t diff = this->current_tick - tick;
	this->current_tick = tick;
	this->get_current_state()->get_rng(this->random);
	return diff;
}
void Simulation::clear() {
	this->reset(0, SimulationState());
}

size_t Simulation::state_count() {
	return this->states.size();
}
SimulationState* Simulation::get_state(uint64_t tick) {
	return this->states.ptr(tick % this->state_count());
}
SimulationState* Simulation::get_active_state() {
	return this->get_state(this->active_tick);
}
SimulationState* Simulation::get_current_state() {
	return this->get_state(this->current_tick);
}

Object* Simulation::add_object(CompBitfield components) {
	size_t size = Object(INVALID_OBJECT, components).get_size();
	uint8_t* old_buf = get_current_state()->object_buffer.ptr();
	uint8_t* buf_ptr = get_current_state()->object_buffer.reserve(size);
	Object* object = new (buf_ptr) Object(get_current_state()->next_object_id++, components);
	buf_ptr += sizeof(Object);
	for (CompId i = 0; i < internal::registered_type_count; i++) {
		if (!object->has_component(i)) continue;
		internal::types[i].create(buf_ptr);
		buf_ptr += internal::types[i].size;
	}
	// if buffer didn't move during reserve, add the object, otherwise rewrite list
	if (old_buf == get_current_state()->object_buffer.ptr()) {
		this->get_current_object_map().add(object->id, object);
	} else {
		this->rewrite_object_map();
	}
	auto* event = new ObjectAddEvent(this);
	event->tick = this->current_tick;
	event->object = object->id;
	gen::event::system()->occur(event);
	return object;
}
bool Simulation::remove_object(uint64_t id) {
	Object* object = this->get_current_object(id);
	if (!object) return false;
	auto* event = new ObjectRemoveEvent(this);
	event->tick = this->current_tick;
	event->object = id;
	event->object_copy.add(object, object->get_size());
	gen::event::system()->occur(event);
	this->get_current_object_map().remove(object->get_id());
	object->id = INVALID_OBJECT;
	return true;
}
Object* Simulation::get_object(uint64_t tick, uint64_t id) {
	return this->get_object_map(tick).get(id, nullptr);
}
Object* Simulation::get_active_object(uint64_t id) {
	return this->get_object(this->active_tick, id);
}
Object* Simulation::get_current_object(uint64_t id) {
	return this->get_object(this->current_tick, id);
}

Simulation::ObjectMap& Simulation::get_object_map(uint64_t tick) {
	return this->object_maps[tick % this->state_count()];
}
Simulation::ObjectMap& Simulation::get_active_object_map() {
	return this->get_object_map(this->active_tick);
}
Simulation::ObjectMap& Simulation::get_current_object_map() {
	return this->get_object_map(this->current_tick);
}

double Simulation::get_base_speed() {
	return this->base_speed;
}
void Simulation::set_base_speed(double new_base_speed) {
	double speed = this->get_speed();
	this->base_speed = new_base_speed;
	this->set_speed(speed);
}
double Simulation::get_speed() {
	return this->real_speed / this->base_speed;
}
void Simulation::set_speed(double new_speed) {
	this->real_speed = this->base_speed * new_speed;
}
double Simulation::get_actual_speed() {
	return this->real_speed;
}

Object* ObjectAddEvent::get_object() {
	return this->get_simulation()->get_object(this->tick, this->object);
}
