// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/ecs/ticker.hpp>
#include <gongen/ecs/simulation.hpp>

using namespace gen::ecs;

Ticker::Ticker(Simulation* simulation, double base_tps) {
	this->simulation = simulation;
	this->base_tps = base_tps;
	this->set_tick_rate(base_tps);
	this->leftover_time = 0.0;
}
void Ticker::set_tick_rate(double new_tps) {
	this->tps = new_tps;
	this->spt = 1.0 / new_tps;
	if (!this->simulation) return;
	this->simulation->set_base_speed(this->base_tps / this->tps);
}
size_t Ticker::update(void* tick_user) {
	return this->update([](void* user) {}, nullptr, tick_user);
}
size_t Ticker::update(void (*on_tick)(void*), void* user, void* tick_user) {
	gen::time::Time time = gen::time::get_time();
	double time_diff = (time - this->last_update).sec();
	this->leftover_time += time_diff;
	size_t count = 0;
	while (this->leftover_time >= this->spt) {
		if (this->simulation) this->simulation->tick(tick_user);
		on_tick(user);
		count++;
		this->leftover_time -= this->spt;
	}
	this->last_update = time;
	return count;
}
