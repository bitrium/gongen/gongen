// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/ecs/common.hpp>
#include <gongen/common/error.hpp>

gen::ecs::internal::TypeSpec gen::ecs::internal::types[COMPONENT_TYPE_COUNT];
gen::ecs::CompId gen::ecs::internal::registered_type_count = 0;

gen::ecs::CompId gen::ecs::internal::comp_register(TypeSpec spec) {
	if (registered_type_count >= COMPONENT_TYPE_COUNT) {
		throw gen::IndexException("Registered too many components.");
	}
	CompId id = registered_type_count;
	types[id] = spec;
	registered_type_count++;
	return id;
}

gen::ecs::CompBitfield gen::ecs::comp_id_to_bit(gen::ecs::CompId id) {
	return (gen::ecs::CompBitfield(1) << id);
}

GEN_COMP_IMPL_STATIC(NAME)
GEN_COMP_IMPL_STATIC(PROPS)
