// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/logger.hpp>

gen::thread::WaitObject logger_wait_object(true);
thread_local static int32_t log_depth = 0;

void gen::log::log(int32_t level, const gen::String& msg) {
	gen::event::system()->occur(new gen::log::Event(level, log_depth, msg));
	logger_wait_object.signal();
}

#define CALL_VLOGF(level, fmt) {		\
	va_list va;							\
	va_start(va, fmt);					\
	gen::log::vlogf(level, fmt, va);	\
	va_end(va);							\
}

void gen::log::vlogf(int32_t level, const char* fmt, va_list va) {
	gen::log::Event* log_event = new gen::log::Event(level, log_depth, "");
	log_event->msg.vappendf(fmt, va);
	gen::event::system()->occur(log_event);
	logger_wait_object.signal();
}
void gen::log::logf(int32_t level, const char* fmt, ...) { CALL_VLOGF(level, fmt); }
void gen::log::tracef(const char* fmt, ...) { CALL_VLOGF(gen::log::LEVEL_TRACE, fmt); }
void gen::log::debugf(const char* fmt, ...) { CALL_VLOGF(gen::log::LEVEL_DEBUG, fmt); }
void gen::log::infof(const char* fmt, ...) { CALL_VLOGF(gen::log::LEVEL_INFO, fmt); }
void gen::log::warnf(const char* fmt, ...) { CALL_VLOGF(gen::log::LEVEL_WARNING, fmt); }
void gen::log::errorf(const char* fmt, ...) { CALL_VLOGF(gen::log::LEVEL_ERROR, fmt); }

uint8_t gen::log::pushd(uint8_t count) {
	log_depth += count;
	return log_depth;
}
uint8_t gen::log::popd(uint8_t count) {
	if (log_depth) log_depth -= count;
	return log_depth;
}

struct LogThreadImpl {
	gen::Buffer<gen::log::Handler*> handlers;
	gen::TypedLocklessQueue<gen::log::Handler> new_handlers;
	uint32_t sleep_ms = 0;
	std::atomic_bool end_flag;
};

void* log_thread_proc(void* impl) {
	LogThreadImpl* log_impl = (LogThreadImpl*)impl;
	while (!log_impl->end_flag) {
		while (gen::log::Handler* handler = log_impl->new_handlers.dequeue()) {
			log_impl->handlers.add(handler);
		}
		for (auto& h : log_impl->handlers) {
			h->update(true);
		}
		logger_wait_object.wait();
	}
	delete log_impl;
	return nullptr;
}

gen::log::Thread::Thread(uint32_t sleep_ms) {
	this->impl = new LogThreadImpl();
	LogThreadImpl* log_impl = (LogThreadImpl*)impl;
	log_impl->end_flag = false;
	log_impl->sleep_ms = sleep_ms;
	gen::Thread thread(log_thread_proc, impl, "Log");
}
gen::log::Thread::~Thread() {
	LogThreadImpl* log_impl = (LogThreadImpl*)impl;
	for (gen::log::Handler* handler : log_impl->handlers) {
		delete handler;
	}
	log_impl->end_flag = true;
	delete log_impl;
}
void gen::log::Thread::add_handler(Handler* handler) {
	LogThreadImpl* log_impl = (LogThreadImpl*)impl;
	log_impl->new_handlers.enqueue(handler);
}

void gen::log::default_log_format(gen::String& output, gen::log::Event* event) {
	double seconds = event->time.sec();
	uint64_t sec = (uint64_t) seconds;
	uint64_t ms = (seconds - sec) * 1000;
	uint64_t min = (sec / 60);
	sec = (sec % 60);

	gen::String level_name;
	if (event->level == gen::log::LEVEL_INFO) level_name = "INFO";
	else if (event->level == gen::log::LEVEL_WARNING) level_name = "WARNING";
	else if (event->level == gen::log::LEVEL_ERROR) level_name = "ERROR";
	else if (event->level == gen::log::LEVEL_DEBUG) level_name = "DEBUG";
	else if (event->level == gen::log::LEVEL_TRACE) level_name = "TRACE";

#ifdef _MSC_VER
	char* indent = (char*)_alloca(event->depth * 2 + 1);
#else
	char indent[event->depth * 2 + 1];
#endif
	for (uint8_t i = 0; i < event->depth * 2; i += 2) {
		indent[i] = '|';
		indent[i + 1] = ' ';
	}
	indent[event->depth * 2] = '\0';

	if (!level_name.empty()) output.appendf("(%02lu:%02lu:%03lu) [%s] %s: %s%s\n", min, sec, ms, event->thread_name.c_str(), level_name.c_str(), indent, event->msg.c_str());
	else output.appendf("(%02lu:%02lu:%04lu) [%s]: %s%s\n", min, sec, ms, event->thread_name.c_str(), indent, event->msg.c_str());
}

gen::log::FileHandler::FileHandler(const gen::AbstractPath& path, int32_t min_level, log_format log_formatter) :file(path, gen::file::Mode::WRITE) {
	this->min_level = min_level;
	this->log_formatter = log_formatter;
}
gen::log::FileHandler::~FileHandler() {
	if(file.is_open()) file.flush();
}
void gen::log::FileHandler::handle(gen::log::Event* event) {
	if ((file.is_open()) && (event->level >= min_level)) {
		temp_str.clear();
		log_formatter(temp_str, event);
		file.write(temp_str.c_str(), temp_str.size());
	}
}

gen::log::ConsoleHandler::ConsoleHandler(int32_t min_level, log_format log_formatter) {
	gen::console::init();
	this->min_level = min_level;
	this->log_formatter = log_formatter;
}
gen::log::ConsoleHandler::~ConsoleHandler() {}
void gen::log::ConsoleHandler::handle(gen::log::Event* event) {
	if (event->level >= min_level) {
		temp_str.clear();
		log_formatter(temp_str, event);
		gen::console::write(temp_str.c_str());
	}
}
