// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/codecs.hpp>
#include <string.h>
#include <csetjmp>

size_t gen::utf8::strlen(const char* input) {
	if (!input) return 0;
	size_t out = 0;
	while (*input != '\0') {
		out++;
		input++;
	}
	return out;
}
gen::Buffer<uint32_t> gen::utf8::decode(const char* input, size_t size) {
	gen::Buffer<uint32_t> out;
	size_t codepointsRead = 0;
	size_t bytesToRead = 0;
	uint32_t codepoint = 0;
	for (uint32_t i = 0; i < size; i++) {
		uint8_t c = *input;
		if (bytesToRead == 0) {
			if (!(c & 0x80)) {
				codepoint = c & 0x7F;
			}
			else {
				if (!(c & 0x20)) {
					bytesToRead = 1;
					codepoint = c & 0x1F;
				}
				else if (!(c & 0x10)) {
					bytesToRead = 2;
					codepoint = c & 0xF;
				}
				else if (!(c & 0x8)) {
					bytesToRead = 3;
					codepoint = 0x7;
				}
				codepoint = (codepoint << 6);
			}
		}
		else {
			codepoint |= (c & 0x3F);
			bytesToRead--;
			if (bytesToRead != 0) codepoint = (codepoint << 6);
		}
		if (bytesToRead == 0) {
			out.add(codepoint);
			codepointsRead++;
		}
		input++;
	}
	return out;
}
gen::String gen::utf8::encode(uint32_t* input, size_t size) {
	gen::String out;
	size_t out_size = 0;
	for (size_t i = 0; i < size; i++) {
		uint32_t c = input[i];
		if (c <= 0x7F) {
			char* utf8 = out.reserve(1);
			utf8[0] = c & 0x7F;
		}
		else if (c <= 0x7FF) {
			char* utf8 = out.reserve(2);
			utf8[0] = 0xC0 | ((c >> 6) & 0x1F);
			utf8[1] = c & 0x3F;
		}
		else if (c <= 0xFFFF) {
			char* utf8 = out.reserve(3);
			utf8[0] = 0xE0 | ((c >> 12) & 0xF);
			utf8[1] = ((c >> 6) & 0x3F);
			utf8[2] = c & 0x3F;
		}
		else if (c <= 0x1FFFFF) {
			char* utf8 = out.reserve(4);
			utf8[0] = 0xF0 | ((c >> 18) & 0x7);
			utf8[1] = ((c >> 12) & 0x3F);
			utf8[2] = ((c >> 6) & 0x3F);
			utf8[3] = c & 0x3F;
		}
	}
	return out;
}

size_t gen::utf16::strlen(const char16_t* input) {
	if (!input) return 0;
	size_t out = 0;
	while (*input != '\0') {
		out++;
		input++;
	}
	return out;
}
gen::Buffer<uint32_t> gen::utf16::decode(const char16_t* input, size_t size) {
	gen::Buffer<uint32_t> out;
	size_t codepointsRead = 0;
	size_t bytesToRead = 0;
	uint32_t codepoint = 0;
	for (uint32_t i = 0; i < size; i++) {
		char16_t c = input[i];
		if (bytesToRead == 0) {
			if ((c < 0xD7FF) || (c > 0xE000)) {
				out.add(c);
				codepointsRead++;
			}
			else {
				codepoint = ((c - 0xD800) << 10);
				bytesToRead = 1;
			}
		}
		else {
			codepoint |= (c - 0xDC00);
			codepoint += 0x10000;
			out.add(codepoint);
			codepointsRead++;
			bytesToRead--;
		}
	}
	return out;
}
gen::Buffer<char16_t> gen::utf16::encode(uint32_t* input, size_t size) {
	gen::Buffer<char16_t> out;
	size_t out_size = 0;
	for (size_t i = 0; i < size; i++) {
		uint32_t c = input[i];
		if (c > 0xFFFF) {
			uint32_t c2 = c - 0x10000;
			uint16_t w1 = (c2 & 0x3FF);
			uint16_t w2 = ((c2 >> 10) & 0x3FF);
			out.add(0xD800 + w1);
			out.add(0xDC00 + w2);
			out_size += 2;
		}
		else {
			out.add(c & 0xFFFF);
			out_size += 1;
		}
	}
	out.add(0);
	return out;
}

size_t gen::base64::encode_len(size_t size) {
	if (size % 3 != 0) size += 3;
	return size / 3 * 4;
}
size_t gen::base64::decode_len(size_t size) {
	if (size % 4 != 0) size += 4;
	return size / 4 * 3;
}
static char base64_to[64] = {0};
static uint8_t base64_from['z' - '+'] = {0};
#define B64_TO(x) base64_to[x]
#define B64_FROM(x) base64_from[x - '+']
static void base64_table_fill() {
	if (B64_TO(B64_FROM('x')) == 'x') return;
	for (int i = 0; i < 26; i++) {
		B64_TO(i) = 'A' + i;
		B64_FROM('A' + i) = i;
	}
	for (int i = 0; i < 26; i++) {
		B64_TO(i + 26) = 'a' + i;
		B64_FROM('a' + i) = i + 26;
	}
	for (int i = 0; i < 10; i++) {
		B64_TO(i + 26 + 26) = '0' + i;
		B64_FROM('0' + i) = i + 26 + 26;
	}
	B64_TO(62) = '+';
	B64_FROM('+') = 62;
	B64_TO(63) = '/';
	B64_FROM('/') = 63;
}
gen::ByteBuffer gen::base64::decode(const char* input, size_t size) {
	gen::ByteBuffer ret;
	size_t leading_bytes = 3;
	uint8_t* out = ret.reserve(decode_len(size) + leading_bytes);
	size_t i = 0;
	while (i < size) {
		*out++ = B64_FROM(input[i]) << 2 | B64_FROM(input[i + 1]) >> 4;
		*out++ = B64_FROM(input[i + 1]) << 4 | B64_FROM(input[i + 2]) >> 2;
		*out++ = B64_FROM(input[i + 2]) << 6 | B64_FROM(input[i + 3]);
		i += 4;
	}
	if (size > 4) {
		for (i = 0; i < 3; i++) {
			if (input[size - i] == '=') leading_bytes++;
		}
	}
	ret.resize(ret.size() - leading_bytes);
	return ret;
}
gen::String gen::base64::encode(const uint8_t* input, size_t size) {
	base64_table_fill();
	gen::String ret;
	char* out = ret.reserve(encode_len(size));
	size_t i = 0;
	while (i + 2 < size) {
		*out++ = B64_TO((input[i] >> 2) & 0b00111111);
		*out++ = B64_TO((input[i] << 4 | input[i + 1] >> 4) & 0b00111111);
		*out++ = B64_TO((input[i + 1] << 2 | input[i + 2] >> 6) & 0b00111111);
		*out++ = B64_TO(input[i + 2] & 0b00111111);
		i += 3;
	}
	if (i < size) {
		uint8_t b0 = input[i];
		*out++ = B64_TO((b0 >> 2) & 0b00111111);
		uint8_t b1 = i + 1 < size ? input[i + 1] : 0;
		*out++ = B64_TO((b0 << 4 | b1 >> 4) & 0b00111111);
		if (i + 1 < size) {
			b0 = i + 2 < size ? input[i + 2] : 0;
			*out++ = B64_TO((b1 << 2 | b0 >> 6) & 0b00111111);
			if (i + 2 < size) *out++ = B64_TO(b0 & 0b00111111);
			else *out++ = '=';
		} else {
			*out++ = '=';
			*out++ = '=';
		}
	}
	return ret;
}
