// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/compression.hpp>
#include <gongen/common/error.hpp>
#ifdef GEN_DEP_ZSTD
#include <zstd.h>

thread_local ZSTD_CCtx* zstd_cctx = nullptr;
thread_local ZSTD_DCtx* zstd_dctx = nullptr;

class ZstdCompressor :public gen::Compressor {
public:
	uint32_t max_level() { return ZSTD_maxCLevel(); }
	uint32_t default_level() { return ZSTD_defaultCLevel(); }

	size_t compress_bound(size_t size) {
		return ZSTD_compressBound(size);
	}
	size_t compress(void* dst, size_t dst_size, const void* src, size_t src_size, uint32_t compression_level) {
		if (!zstd_cctx) {
			zstd_cctx = ZSTD_createCCtx();
			ZSTD_CCtx_setParameter(zstd_cctx, ZSTD_c_contentSizeFlag, 0);
			ZSTD_CCtx_setParameter(zstd_cctx, ZSTD_c_checksumFlag, 0);
			ZSTD_CCtx_setParameter(zstd_cctx, ZSTD_c_dictIDFlag, 0);
		}
		size_t ret = ZSTD_compressCCtx(zstd_cctx, dst, dst_size, src, src_size, compression_level);
		if (ZSTD_isError(ret)) throw gen::CompressionException(ZSTD_getErrorName(ret));
		return ret;
	}

	size_t decompress(void* dst, size_t dst_size, const void* src, size_t src_size) {
		if (!zstd_dctx) zstd_dctx = ZSTD_createDCtx();
		if (!compression_level) compression_level = default_level();
		size_t ret = ZSTD_decompressDCtx(zstd_dctx, dst, dst_size, src, src_size);
		if (ZSTD_isError(ret)) throw gen::CompressionException(ZSTD_getErrorName(ret));
		return ret;
	}
};

ZstdCompressor zstd_compressor_static;

gen::Compressor* gen::compressor_zstd() { return (gen::Compressor*)&zstd_compressor_static; }
#endif

#define MINIZ_NO_ZLIB_COMPATIBLE_NAMES
#include <miniz.h>

class ZlibCompressor :public gen::Compressor {
public:
	uint32_t max_level() override { return MZ_BEST_COMPRESSION; }
	uint32_t default_level() override { return MZ_DEFAULT_LEVEL; }

	size_t compress_bound(size_t size) override {
		return mz_compressBound(size);
	}
	size_t compress(void* dst, size_t dst_size, const void* src, size_t src_size, uint32_t compression_level) override {
		mz_ulong out_len;
		out_len = dst_size;
		if (!compression_level) compression_level = default_level();
		int ret = mz_compress2((unsigned char*)dst, &out_len, (const unsigned char*)src, src_size, compression_level);
		if (ret) throw gen::CompressionException(mz_error(ret));
		return out_len;
	}

	size_t decompress(void* dst, size_t dst_size, const void* src, size_t src_size) override {
		mz_ulong out_len, in_len;
		out_len = dst_size;
		in_len = src_size;
		int ret = mz_uncompress2((unsigned char*)dst, &out_len, (const unsigned char*)src, &in_len);
		if (ret) throw gen::CompressionException(mz_error(ret));
		return out_len;
	}
};

ZlibCompressor zlib_compressor_static;

gen::Compressor* gen::compressor_zlib() { return (gen::Compressor*)&zlib_compressor_static; }
