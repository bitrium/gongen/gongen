// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/random.hpp>

struct RngLcg : public gen::rand::Rng {
	uint64_t s = 0;

	uint64_t seed(uint64_t seed) override {
		this->s = seed;
		return this->s;
	}
	uint64_t next() override {
		this->s = 6364136223846793005 * this->s + 1;
		return this->s >> 33;
	}
	uint64_t get_limit() override {
		return 0x7fffffff;
	}
	size_t state_size() override { return sizeof(uint64_t); }
	void get_state(void* state) override { *((uint64_t*)state) = s;}
	void set_state(void* state, size_t size) override {
		if(size >= state_size()) {
			s = *((uint64_t*)state);
		}
	}
};

gen::rand::Rng* gen::rand::rng_lcg() {
	return new RngLcg();
}
