// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/random.hpp>

#include <gongen/common/math.hpp>
#include <gongen/common/platform.hpp>

using namespace gen::rand;

uint64_t Random::seed() {
	return this->rng->seed(time::get_unix_timestamp());
}
uint64_t Random::seed(uint64_t seed) {
	return this->rng->seed(seed);
}

#define CHECK_ARGS(min, max) do { if (min >= max) { if (min == max) return min; gen::swap(min, max); } } while (0)
int8_t Random::i8(int8_t min, int8_t max) {
	return (int8_t) this->i64(min, max);
}
int16_t Random::i16(int16_t min, int16_t max) {
	return (int16_t) this->i64(min, max);
}
int32_t Random::i32(int32_t min, int32_t max) {
	return (int32_t) this->i64(min, max);
}
int64_t Random::i64(int64_t min, int64_t max) {
	CHECK_ARGS(min, max);
	uint64_t limit = max - min;
	if (this->range_errors && limit > this->rng->get_limit()) {
		throw RangeException::createf(
			"This RNG only supports %lu combinations while %lu [%li; %li] possibilities were requested.",
			this->rng->get_limit(), limit, min, max
		);
	}
	return this->rng->next() % limit + min; // FIXME possible overflow?
}
uint8_t Random::u8(uint8_t min, uint8_t max) {
	return (uint8_t) this->u64(min, max);
}
uint16_t Random::u16(uint16_t min, uint16_t max) {
	return (uint16_t) this->u64(min, max);
}
uint32_t Random::u32(uint32_t min, uint32_t max) {
	return (uint32_t) this->u64(min, max);
}
uint64_t Random::u64(uint64_t min, uint64_t max) {
	CHECK_ARGS(min, max);
	max -= min - 1;
	if (this->range_errors && max >= this->rng->get_limit()) {
		throw RangeException::createf(
			"This RNG only supports %lu combinations while %lu [%lu; %lu] possibilities were requested.",
			this->rng->get_limit(), max, min, max + (min - 1)
		);
	}
	return this->rng->next() % max + min;
}
float Random::f32(float min, float max) {
	CHECK_ARGS(min, max);
	return (float) this->rng->next() / (float) this->rng->get_limit() * (max - min) + min;
}
double Random::f64(double min, double max) {
	CHECK_ARGS(min, max);
	return (double) this->rng->next() / (double) this->rng->get_limit() * (max - min) + min;
}
bool Random::boolean() {
	return this->rng->next() % 2;
}
#undef CHECK_ARGS

char Random::alnum() {
	uint8_t rand = this->u8('9' - '0' + 1 + 'Z' - 'A' + 1 + 'z' - 'a' + 1);
	if (rand <= '9' - '0') {
		return '0' + rand;
	}
	rand -= '9' - '0' + 1;
	if (rand <= 'Z' - 'A') {
		return 'A' + rand;
	}
	rand -= 'Z' - 'A' + 1;
	return 'a' + rand;
}
char Random::letter() {
	return (char) this->u8('a', 'z');
}
char Random::digit() {
	return (char) this->u8('0', '9');
}
