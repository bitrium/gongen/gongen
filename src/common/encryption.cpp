// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/encryption.hpp>
#include <gongen/common/error.hpp>
#include <bearssl.h>

using namespace gen;

struct bearssl_impl {
	br_hmac_drbg_context rng;
};
thread_local Optional<bearssl_impl> bearssl_;
bearssl_impl* get_bearssl() {
	if (bearssl_.some()) return bearssl_.ptr();
	bearssl_.emplace();
	br_prng_seeder seeder = br_prng_seeder_system(nullptr);
	if (!seeder) throw gen::RuntimeException("System does not support suitable PRNG");
	br_hmac_drbg_init(&bearssl_->rng, &br_sha256_vtable, nullptr, 0);
	if (!seeder(&bearssl_->rng.vtable)) throw gen::RuntimeException("Could not seed HMAC DRBG");
	return bearssl_.ptr();
}

enum {
	CYPHER_UNSET,
	CYPHER_RSA,
	CYPHER_AES,
};
struct crypto_rsa {
	size_t key_length;
};
struct crypto_aes {
};

Cypher::Cypher() {
	this->impl = nullptr;
	this->type = CYPHER_UNSET;
}
Cypher::~Cypher() {
	if (this->type == CYPHER_RSA) delete (crypto_rsa*) this->impl;
}
Cypher& Cypher::init(void* impl, uint8_t type) {
	if (this->impl) throw RuntimeException("Cypher already initialized");
	this->impl = impl;
	this->type = type;
	return *this;
}
void Cypher::key_gen(ByteBuffer& key) {
	if (this->type == CYPHER_AES) {
		// TODO
	} else throw NoImplException("Cypher does not support shared key generation.");
}
void Cypher::key_gen(ByteBuffer& skey, ByteBuffer& pkey) {
	if (this->type == CYPHER_RSA) {
		auto* impl = (crypto_rsa*) this->impl;
		br_rsa_keygen keygen = br_rsa_keygen_get_default();
		skey.reserve(sizeof(br_rsa_private_key));
		uint8_t* sk_buf = skey.reserve(BR_RSA_KBUF_PRIV_SIZE(impl->key_length));
		auto* sk = (br_rsa_private_key*) skey.ptr();
		pkey.reserve(sizeof(br_rsa_public_key));
		uint8_t* pk_buf = pkey.reserve(BR_RSA_KBUF_PUB_SIZE(impl->key_length));
		auto* pk = (br_rsa_public_key*) pkey.ptr();
		keygen(&get_bearssl()->rng.vtable, sk, sk_buf, pk, pk_buf, impl->key_length, 0);
	} else throw NoImplException("Cypher does not support public key generation.");
}
ByteBuffer Cypher::key_encode(const ByteBuffer& key) {
	if (this->type == CYPHER_RSA) {
		auto* pk = (br_rsa_public_key*) key.ptr();
		ByteBuffer ret;
		ret.reserve(8 + pk->nlen + 8 + pk->elen);
		*((uint64_t*) ret.ptr(0)) = pk->nlen;
		memcpy(ret.ptr(8), pk->n, pk->nlen);
		*((uint64_t*) ret.ptr(8 + pk->nlen)) = pk->elen;
		memcpy(ret.ptr(8 + pk->nlen + 8), pk->e, pk->elen);
		return ret;
	} else throw NoImplException("Cypher does not support key encoding.");
}
ByteBuffer Cypher::key_encode(const ByteBuffer& skey, const ByteBuffer& pkey) {
	if (this->type == CYPHER_RSA) {
		auto* impl = (crypto_rsa*) this->impl;
		auto* sk = (br_rsa_private_key*) skey.ptr();
		auto* pk = (br_rsa_public_key*) pkey.ptr();
		ByteBuffer mod;
		br_rsa_compute_modulus_get_default()(mod.reserve((impl->key_length + 7) / 8), sk);
		ByteBuffer ret;
		ret.reserve(br_encode_rsa_raw_der(nullptr, sk, pk, mod.ptr(), mod.size()));
		br_encode_rsa_raw_der(ret.ptr(), sk, pk, mod.ptr(), mod.size());
		return ret;
	} else throw NoImplException("Cypher does not support secret key encoding.");
}
ByteBuffer Cypher::key_decode(const ByteBuffer& bin) {
	if (this->type == CYPHER_RSA) {
		ByteBuffer ret;
		auto* sk = (br_rsa_public_key*) ret.reserve(sizeof(br_rsa_private_key));
		sk->nlen = *((uint64_t*) bin.ptr());
		sk->elen = *((uint64_t*) bin.ptr(8 + sk->nlen));
		ret.reserve(sk->nlen + sk->elen);
		sk->n = ret.ptr(sizeof(br_rsa_private_key));
		sk->e = ret.ptr(sizeof(br_rsa_private_key) + sk->elen);
		memcpy(sk->n, bin.ptr(8), sk->nlen);
		memcpy(sk->e, bin.ptr(8 + sk->nlen + 8), sk->elen);
		return ret;
	} else throw gen::NoImplException("Cypher does not support key decoding.");
}
ByteBuffer Cypher::key_decode(const ByteBuffer& bin, const ByteBuffer& pkey) {
	if (this->type == CYPHER_RSA) {
		br_skey_decoder_context decoder;
		br_skey_decoder_init(&decoder);
		br_skey_decoder_push(&decoder, bin.ptr(), bin.size());
		const br_rsa_private_key* key = br_skey_decoder_get_rsa(&decoder);
		if (key == nullptr) throw RuntimeException("Could not decode RSA key.");
		ByteBuffer ret;
		auto* sk = (br_rsa_private_key*) ret.reserve(sizeof(br_rsa_private_key));
		uint8_t* ptr;
		sk->n_bitlen = key->n_bitlen;
#define COPY_INTEGER(x) do { \
	sk->x##len = key->x##len; \
    ptr = ret.reserve(sk->x##len); \
    sk = (br_rsa_private_key*) ret.ptr(); \
    sk->x = ptr; \
    memcpy(sk->x, key->x, sk->x##len); \
} while (0)
		COPY_INTEGER(p);
		COPY_INTEGER(q);
		COPY_INTEGER(dp);
		COPY_INTEGER(dq);
		COPY_INTEGER(iq);
#undef COPY_INTEGER
		return ret;
	} else throw NoImplException("Cypher does not support secret key decoding.");
}
void Cypher::encrypt(const ByteBuffer& pkey, ByteBuffer& dst, const ByteBuffer& src) {
	if (this->type == CYPHER_RSA) {
		auto* impl = (crypto_rsa*) this->impl;
		br_rsa_oaep_encrypt encrypt = br_rsa_oaep_encrypt_get_default();
		auto* pk = (br_rsa_public_key*) pkey.ptr();
		dst.reserve((impl->key_length + 7) / 8);
		size_t len = encrypt(&get_bearssl()->rng.vtable, &br_sha256_vtable, nullptr, 0, pk, dst.ptr(), dst.size(), src.ptr(), src.size());
		if (!len) throw RuntimeException("Failed to encrypt a message.");
		dst.resize(len);
	} else throw NoImplException("Cypher does not support encryption.");
}
void Cypher::decrypt(const ByteBuffer& skey, ByteBuffer& dst, const ByteBuffer& src) {
	if (this->type == CYPHER_RSA) {
		br_rsa_oaep_decrypt decrypt = br_rsa_oaep_decrypt_get_default();
		auto* sk = (br_rsa_private_key*) skey.ptr();
		dst = src;
		size_t len = dst.size();
		if (!decrypt(&br_sha256_vtable, nullptr, 0, sk, dst.ptr(), &len)) {
			throw RuntimeException("Failed to decrypt a message.");
		}
		dst.resize(len);
	} else throw NoImplException("Cypher does not support decryption.");
}
void Cypher::sign(ByteBuffer skey, ByteBuffer& sig, const ByteBuffer& msg) {
	if (this->type == CYPHER_RSA) {
		br_rsa_pkcs1_sign sign = br_rsa_pkcs1_sign_get_default();
		auto* sk = (br_rsa_private_key*) skey.ptr();
		br_sha256_context sha256;
		br_sha256_init(&sha256);
		br_sha256_update(&sha256, msg.ptr(), msg.size());
		uint8_t hash[32];
		br_sha256_out(&sha256, hash);
		sig.reserve((sk->n_bitlen + 7) / 8);
		if (!sign(BR_HASH_OID_SHA256, hash, 32, sk, sig.ptr())) {
			throw RuntimeException("Could not sign data.");
		}
	} else throw NoImplException("Cypher does not support signing.");
}
bool Cypher::verify(ByteBuffer pkey, const ByteBuffer& sig, const ByteBuffer& msg) {
	if (this->type == CYPHER_RSA) {
		br_rsa_pkcs1_vrfy vrfy = br_rsa_pkcs1_vrfy_get_default();
		auto* pk = (br_rsa_public_key *) pkey.ptr();
		br_sha256_context sha256;
		br_sha256_init(&sha256);
		br_sha256_update(&sha256, msg.ptr(), msg.size());
		uint8_t hash[32];
		br_sha256_out(&sha256, hash);
		uint8_t verified[32];
		if (!vrfy(sig.ptr(), sig.size(), BR_HASH_OID_SHA256, 32, pk, verified)) {
			throw RuntimeException("Could not verify data.");
		}
		if (memcmp(hash, verified, 32) != 0) {
			throw RuntimeException("Could not verify data (bad hash).");
		}
		return true;
	} else throw NoImplException("Cypher does not support verification.");
}

void gen::cypher_rsa(Cypher* cypher, size_t modulus_size) {
	auto* impl = new crypto_rsa();
	impl->key_length = modulus_size;
	cypher->init(impl, CYPHER_RSA);
}
void gen::cypher_aes(Cypher* cypher) {
	auto* impl = new crypto_aes();
	cypher->init(impl, CYPHER_AES);
}
