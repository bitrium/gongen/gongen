// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/platform/memory.hpp>
#include <exception>

void* std_alloc(size_t size) {
	void* mem = gen::alloc(size);
	if (mem) return mem;
	else throw std::bad_alloc();
}

void* __cdecl operator new(std::size_t size) { return std_alloc(size); }
void* __cdecl operator new[](std::size_t size) { return std_alloc(size); }
void __cdecl operator delete(void* const ptr) noexcept { gen::free(ptr); }
void __cdecl operator delete(void* const ptr, std::size_t size) noexcept { gen::free(ptr); }
void __cdecl operator delete[](void* const ptr) noexcept { gen::free(ptr); }
void __cdecl operator delete[](void* const ptr, std::size_t size) noexcept { gen::free(ptr); }
