// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/error.hpp>
#include <gongen/common/event.hpp>

using namespace gen::event;

static System* event_system = nullptr;

void gen_init_event() {
	event_system = new System();
}

System* gen::event::system() {
	return event_system;
}

Hopper* gen::event::hopper(uint64_t mask, void* source_filter) {
	return system()->hopper(mask, source_filter);
}

static gen::DynamicArray<const char*> event_types;

uint64_t gen::event::type(const char* name) {
	uint64_t flag = 1;
	for (const char* event_type : event_types) {
		if (event_type == name || strcmp(event_type, name) == 0) {
			return flag;
		}
		if (flag == UINT64_MAX) {
			throw gen::EventFloodException("The event system only supports 64 different event types.");
		}
		flag *= 2;
	}
	event_types.add(name);
	return flag;
}


Event::Event(const char* type_name, void* source) : type(gen::event::type(type_name)), source(source) {
}

Event::~Event() {
}


Hopper::Hopper(uint64_t mask, void* source_filter) : mask(mask), source(source_filter) {
}

Hopper::~Hopper() {
	this->poll();
	this->clear();
	for (Hopper*& ptr : event_system->hoppers) {
		if (ptr != this) continue;
		ptr = nullptr;
	}
}

#define DELETE_EVENT_REF(ref) ref->ref_counter.fetch_add(-1); \
if(ref->ref_counter.load() <= 0) { \
	delete ref->event; \
	delete ref; \
}

#define DELETE_CHECK_EVENT_REF(ref) if(ref->ref_counter.load() <= 0) { \
	delete ref->event; \
	delete ref; \
}

void Hopper::occur(Event::Reference* event_ref) {
	event_ref->ref_counter.fetch_add(1);
	this->awaiting.enqueue(event_ref);
}

void Hopper::poll() {
	while (Event::Reference* event = this->awaiting.dequeue()) {
		this->events.add(event->event);
		this->events_refs.add(event);
	}
}

void Hopper::poll_wait() {
	Event::Reference* event = this->awaiting.dequeue_wait();
	while (event) {
		this->events.add(event->event);
		this->events_refs.add(event);

		event = this->awaiting.dequeue();
	}
}


void Hopper::clear() {
	for (auto& ref : events_refs) {
		DELETE_EVENT_REF(ref);
	}
	this->events_refs.clear();
	this->events.clear();
}


System::System() {
	this->lock = false;
}
System::~System() {
	this->lock = true;
	for (auto& h : hoppers) {
		delete h;
	}
}

void System::occur_internal(Event::Reference* event_ref) {
	for (Hopper* hopper : this->hoppers) {
		if (!hopper) continue;
		if (!(hopper->mask & event_ref->event->type)) continue;
		if (hopper->source && event_ref->event->source != hopper->source) continue;
		hopper->occur(event_ref);
	}
	DELETE_CHECK_EVENT_REF(event_ref);
}

void System::occur(Event* event) {
	Event::Reference* event_ref = new Event::Reference();
	event_ref->event = event;
	event_ref->ref_counter.store(0);
	if (this->lock) {
		this->awaiting.enqueue(event_ref);
	} else {
		occur_internal(event_ref);
	}
}

Hopper* System::hopper(uint64_t sieve, void* source_filter) {
	if (this->lock) throw ConcurrencyException("A hopper is being added on another thread.");
	this->lock = true;
	Hopper* hopper = new Hopper(sieve, source_filter);
	this->hoppers.add(hopper);
	this->lock = false;
	while (Event::Reference* event = this->awaiting.dequeue()) {
		this->occur_internal(event);
	}
	return hopper;
}
