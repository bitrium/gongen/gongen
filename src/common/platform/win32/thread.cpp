// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/platform/system.hpp>
#include "winapi.hpp"

HRESULT(*SetThreadDescriptionFc)(HANDLE, PCWSTR) = nullptr;

thread_local void* thread_id = (void*)(-1);
thread_local gen::String thread_name = gen::String();

bool gen::thread::ThreadId::operator==(const ThreadId& id) const {
    return (impl == id.impl);
}
bool gen::thread::ThreadId::operator!=(const ThreadId& id) const {
    return (impl != id.impl);
}
size_t gen::thread::ThreadId::get_number() {
    return (size_t)((intptr_t)impl);
}

gen::thread::ThreadId::~ThreadId() {
}

extern SYSTEM_INFO sys_info;

struct Win32WaitObject {
    HANDLE handle;
    std::atomic<uint32_t> waiting_thread_count;
    bool auto_reset;
};

gen::thread::WaitObject::WaitObject(bool auto_reset) {
    impl = new Win32WaitObject();
    ((Win32WaitObject*)impl)->handle = CreateEventA(nullptr, true, false, nullptr);
    ((Win32WaitObject*)impl)->waiting_thread_count = 0;
    ((Win32WaitObject*)impl)->auto_reset = auto_reset;
}
gen::thread::WaitObject::~WaitObject() {
    auto* impl = (Win32WaitObject*) this->impl;
    CloseHandle(impl->handle);
    delete impl;
}
void gen::thread::WaitObject::wait() {
    auto* impl = (Win32WaitObject*) this->impl;
    impl->waiting_thread_count++;
    WaitForSingleObject(impl->handle, INFINITE);
    impl->waiting_thread_count--;
    if (impl->auto_reset && !impl->waiting_thread_count) {
        reset();
    }
}
void gen::thread::WaitObject::signal() {
    auto* impl = (Win32WaitObject*) this->impl;
    SetEvent(impl->handle);
}
void gen::thread::WaitObject::reset() {
    auto* impl = (Win32WaitObject*) this->impl;
    ResetEvent(impl->handle);
}
bool gen::thread::WaitObject::state() {
    auto* impl = (Win32WaitObject*) this->impl;
    return (WaitForSingleObject(impl->handle, 0) == WAIT_OBJECT_0);
}

void gen::thread::sleep(uint64_t ms) {
	Sleep(ms);
}
gen::thread::ThreadId gen::thread::id() {
    if (thread_id == (void*)(-1)) {
        thread_id = (void*)(intptr_t)GetCurrentThreadId();
    }
    return gen::thread::ThreadId(thread_id);
}
size_t gen::os::thread_count() {
    GetNativeSystemInfo(&sys_info);
    return sys_info.dwNumberOfProcessors;
}
gen::String gen::thread::name() {
    if (thread_name.empty()) return "Unnamed";
    else return thread_name;
}

struct Win32Thread {
    gen::Exception* exception = nullptr;
    HANDLE thread = INVALID_HANDLE_VALUE;
    std::atomic<bool> done_flag;
    std::atomic<uint32_t> ref_counter;
    void* arg = nullptr;
    void* (*proc)(void*) = nullptr;
    char* name = nullptr;
    void* return_value = nullptr;
};

#include <process.h>

void thread_destructor(Win32Thread* thread) {
    thread->ref_counter--;
    if (thread->ref_counter == 0) {
        CloseHandle(thread->thread);
        delete thread;
    }
}

int thread_wrapper(LPVOID param) {
    Win32Thread* thread = (Win32Thread*)param;
    try {
        thread->return_value = thread->proc(thread->arg);
    }
    catch (gen::Exception& e) {
        thread->exception = e.copy();
    }
    catch (std::exception& e) {
        thread->exception = new gen::StdBasedException(e.what());
    }
    thread->done_flag = true;
    thread_destructor(thread);
	  return 0;
}

int win32_feh_wrapper(int (*proc)(void*), void* userdata);

const DWORD MS_VC_EXCEPTION = 0x406D1388;
#pragma pack(push,8)
struct THREADNAME_INFO {
    DWORD dwType; // Must be 0x1000.
    LPCSTR szName; // Pointer to name (in user addr space).
    DWORD dwThreadID; // Thread ID (-1=caller thread).
    DWORD dwFlags; // Reserved for future use, must be zero.
};
#pragma pack(pop)

void set_thread_name_exception(const char* name) {
    THREADNAME_INFO info;
    info.dwType = 0x1000;
    info.szName = name;
    info.dwThreadID = GetCurrentThreadId();
    info.dwFlags = 0;
    __try {
        RaiseException(MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), (ULONG_PTR*)&info);
    }
    __except (EXCEPTION_EXECUTE_HANDLER) {}
}

void set_thread_name(const char* name) {
    if (SetThreadDescriptionFc) {
        gen::Buffer<char16_t> wstr = gen::cstr_to_wstr(name);
        SetThreadDescriptionFc(GetCurrentThread(), (PCWSTR)wstr.ptr());
    }
    if (IsDebuggerPresent()) {
        set_thread_name_exception(name);
    }
}

unsigned thread_wrapper_feh(LPVOID param) {
    Win32Thread* thread = (Win32Thread*)param;
    thread_name = thread->name;
    if (thread->name) {
        set_thread_name(thread->name);
    }
    return win32_feh_wrapper(thread_wrapper, param);
}

gen::thread::Thread::Thread(void* (*proc)(void*), void* arg, const char* name) {
    this->impl = new Win32Thread();
    Win32Thread* thread = (Win32Thread*)impl;
    thread->exception = nullptr;
    thread->arg = arg;
    thread->proc = proc;
    thread->done_flag = false;
    thread->ref_counter = 2;
    thread->return_value = nullptr;
    if (name) {
        thread->name = (char*)gen::calloc(strlen(name) + 1);
        memcpy(thread->name, name, strlen(name));
    }
    else {
        thread->name = nullptr;
    }
    thread->thread = (HANDLE)_beginthreadex(NULL, 0, thread_wrapper_feh, thread, 0, NULL);
}
gen::thread::Thread& gen::thread::Thread::operator=(const Thread& other) {
    if (impl) {
        Win32Thread* thread = (Win32Thread*)impl;
        thread_destructor(thread);
    }
    this->impl = other.impl;
    if (impl) {
        Win32Thread* thread = (Win32Thread*)impl;
        thread->ref_counter++;
    }
    return *this;
}
gen::thread::Thread::~Thread() {
    if (impl) {
        Win32Thread* thread = (Win32Thread*)impl;
        thread_destructor(thread);
    }
}

void* gen::thread::Thread::join() {
    if (impl) {
        Win32Thread* thread = (Win32Thread*)impl;
        WaitForSingleObject(thread->thread, INFINITE);
        return thread->return_value;
    }
    else return nullptr;
}
bool gen::thread::Thread::done() const {
    if (impl) {
        Win32Thread* thread = (Win32Thread*)impl;
        return thread->done_flag;
    }
    else return true;
}
void gen::thread::Thread::kill() {
    if (impl) {
        Win32Thread* thread = (Win32Thread*)impl;
        TerminateThread(thread->thread, -1);
    }
}
gen::Thread& gen::thread::Thread::rethrow_exception() {
    if (impl) {
        Win32Thread* thread = (Win32Thread*)impl;
        if (thread->exception) {
            thread->exception->rethrow();
        }
    }
    return *this;
}

#include <gongen/common/platform/callstack.hpp>
#include <dbghelp.h>

extern HANDLE process_handle;

struct win32_callstack {
    gen::Buffer<void*> addrs;
};

gen::Callstack::Callstack() {
    this->impl = new win32_callstack();
}
gen::Callstack::Callstack(size_t max_count, size_t skip) {
    this->impl = new win32_callstack();
    fill(max_count, skip);
}
gen::callstack::Callstack& gen::callstack::Callstack::operator=(const Callstack& other) {
    ((win32_callstack*)this->impl)->addrs = ((win32_callstack*)other.impl)->addrs;
    this->count = other.count;
    return *this;
}
gen::callstack::Callstack::~Callstack() {
    if (impl) {
        delete (win32_callstack*)impl;
    }
}
gen::callstack::Callstack& gen::callstack::Callstack::fill(size_t max_count, size_t skip) {
    skip++;
    size_t real_count = (max_count + skip);
    this->count = CaptureStackBackTrace(skip, real_count, ((win32_callstack*)impl)->addrs.reserve(max_count + 1), nullptr);
    ((win32_callstack*)impl)->addrs.resize(this->count);
    return *this;
}
gen::callstack::Symbol gen::Callstack::symbol(size_t index) const {
    if (index >= ((win32_callstack*)impl)->addrs.size()) {
        return gen::callstack::Symbol();
    }
    void* addr = ((win32_callstack*)impl)->addrs.get(index);
    char buffer[sizeof(SYMBOL_INFO) + MAX_SYMBOL_NAME_LENGTH];
    PSYMBOL_INFO sym = (PSYMBOL_INFO)buffer;

    sym->SizeOfStruct = sizeof(SYMBOL_INFO);
    sym->MaxNameLen = MAX_SYMBOL_NAME_LENGTH;

    gen::callstack::Symbol out;
    DWORD64 disp = 0;
    if (SymFromAddr(process_handle, (DWORD64)addr, &disp, sym)) {
        out.base_addr = ((size_t)addr - disp);
        out.name.append(sym->Name, sym->NameLen);
        IMAGEHLP_LINE64 line;
        line.SizeOfStruct = sizeof(IMAGEHLP_LINE64);
        DWORD disp2 = 0;
        if (SymGetLineFromAddr64(process_handle, (DWORD64)addr, &disp2, &line)) {
            out.line = line.LineNumber;
            out.filepath = String(line.FileName);
        }
        DWORD64 module_base = SymGetModuleBase64(process_handle, (DWORD64)addr);
        if (module_base) {
            IMAGEHLP_MODULE64 module_info;
            module_info.SizeOfStruct = sizeof(IMAGEHLP_MODULE64);
            SymGetModuleInfo64(process_handle, module_base, &module_info);
            out.module_name = gen::String(module_info.ModuleName);
        }
    }
    return out;
}
void* gen::Callstack::addr(size_t index) const {
    if (index >= ((win32_callstack*)impl)->addrs.size()) {
        return nullptr;
    }
    return ((win32_callstack*)impl)->addrs.get(index);
}

struct win32_feh_handler_t {
    gen::feh::handler_ret(*proc)(gen::feh::handler_info*) = nullptr;
    void* userdata = nullptr;
    uint32_t mask = 0;
    gen::thread::ThreadId thread_id = gen::thread::ThreadId(nullptr);
    win32_feh_handler_t* next = nullptr;
};

thread_local win32_feh_handler_t* win32_feh_handler;

void gen::feh::set_handler(gen::feh::handler_ret (*proc)(handler_info*), void* userdata, uint32_t mask) {
    win32_feh_handler_t* handler = nullptr;
    if (!win32_feh_handler) {
        handler = new win32_feh_handler_t();
        win32_feh_handler = handler;
    }
    else {
        handler = new win32_feh_handler_t();
        win32_feh_handler->next = handler;
    }

    handler->proc = proc;
    handler->userdata = userdata;
    handler->mask = mask;
    handler->thread_id = gen::thread::id();
}

int win32_feh_handle(int n_except, int* ret) {
    win32_feh_handler_t* prev_handler = nullptr;
    win32_feh_handler_t* handler = nullptr;
    if (win32_feh_handler) {
        handler = win32_feh_handler;
        while (handler->next) {
            prev_handler = handler;
            handler = handler->next;
        }
    }
    if (handler) {
        if (prev_handler) prev_handler->next = nullptr;
        if (handler == win32_feh_handler) win32_feh_handler = nullptr;
        win32_feh_handler_t feh_handler = *handler;
        delete handler;

        auto error = (gen::feh::Mask)0;
        if ((n_except == EXCEPTION_ACCESS_VIOLATION) && (feh_handler.mask & gen::feh::SEGFAULT)) error = gen::feh::SEGFAULT;
        else if ((n_except == EXCEPTION_DATATYPE_MISALIGNMENT) && (feh_handler.mask & gen::feh::DATATYPE_MISALIGNMENT)) error = gen::feh::DATATYPE_MISALIGNMENT;
        else if ((n_except == EXCEPTION_ILLEGAL_INSTRUCTION) && (feh_handler.mask & gen::feh::ILLEGAL_INSTRUCTION)) error = gen::feh::ILLEGAL_INSTRUCTION;
        else if ((n_except == EXCEPTION_INT_DIVIDE_BY_ZERO) && (feh_handler.mask & gen::feh::INT_DIVIDE_BY_ZERO)) error = gen::feh::INT_DIVIDE_BY_ZERO;
        else if ((n_except == EXCEPTION_STACK_OVERFLOW) && (feh_handler.mask & gen::feh::STACK_OVERFLOW)) error = gen::feh::STACK_OVERFLOW;

        if (error) {
            gen::feh::handler_info info = {
                .userdata = feh_handler.userdata,
                .callstack = gen::Callstack(gen::EXCEPTION_MAX_CALLSTACK_DEPTH, 7),
                .exception = error,
                .thread_id = feh_handler.thread_id
            };
            gen::feh::handler_ret hret = feh_handler.proc(&info);
            (*ret) = hret.return_value;
            if (hret.result == gen::feh::TERMINATE) ExitProcess(*ret);
            else if (hret.result == gen::feh::RETURN) return EXCEPTION_EXECUTE_HANDLER;
            else if (hret.result == gen::feh::PASS) return EXCEPTION_CONTINUE_SEARCH;
        }
    }
    return EXCEPTION_CONTINUE_SEARCH;
}

int win32_feh_wrapper(int (*proc)(void*), void* userdata) {
    int ret = 0;
    __try {
        return proc(userdata);
    }
    __except (win32_feh_handle(GetExceptionCode(), &ret)) {
        return ret;
    }
}
