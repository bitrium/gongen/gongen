// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/platform/system.hpp>
#include "winapi.hpp"
#include <shlwapi.h>
#include <shlobj_core.h>
#include <wchar.h>

void gen::file::Path::update_string() {
	if (!segments.empty()) {
		this->str.clear();
		for (PtrIterator<gen::String> it = this->segments.begin(); it != this->segments.end() - 1; it++) {
			this->str.append(*it.get());
			this->str.append("/");
		}
		this->str.append(*(this->segments.end() - 1).get());
	}
}

bool gen::file::Path::exists() const {
	return PathFileExistsW(THIS_PATH_WC_STR);
}
bool gen::file::Path::readable() const {
	return (_waccess(THIS_PATH_WC_STR, 4) != -1);
}
bool gen::file::Path::writeable() const {
	return (_waccess(THIS_PATH_WC_STR, 2) != -1);
}

bool gen::file::Path::is_directory() const {
	DWORD attribs = GetFileAttributesW(THIS_PATH_WC_STR);
	if (attribs == INVALID_FILE_ATTRIBUTES) return false;
	return (attribs & FILE_ATTRIBUTE_DIRECTORY);
}

gen::file::Path& gen::file::Path::to_resolved() {
	if (this->is_abs) return *this;
	else {
		gen::String new_str = gen::utf8::encode(gen::utf16::decode(cstr_to_path(c_str()).ptr(4)));
		*this = gen::file::Path(new_str);
		return *this;
	}
}

gen::DynamicArray<gen::String> gen::file::Path::enumerate_names() const {
	gen::DynamicArray<gen::String> out;
	WIN32_FIND_DATAW ffd;
	gen::Buffer<char16_t> wstr = cstr_to_path(c_str());
	if (segments.empty()) wstr = cstr_to_path(gen::file::get_working_dir().c_str());
	wchar_t* wstr_ptr = (wchar_t*)wstr.reserve(2);
	wstr_ptr[-1] = L'\\';
	wstr_ptr[0] = L'*';
	wstr_ptr[1] = L'\0';
	HANDLE hFind = FindFirstFileW((const wchar_t*)wstr.ptr(), &ffd);
	if (hFind != INVALID_HANDLE_VALUE) {
		do {
			gen::String str = gen::utf8::encode(gen::utf16::decode((const char16_t*)ffd.cFileName));
			if (!(str == ".") && !(str == "..")) out.add(str);
		} while (FindNextFileW(hFind, &ffd) != 0);
	}
	return out;
}

void gen::file::make_directory(const AbstractPath& path, bool recursive) {
	if (path.exists()) {
		if (path.is_directory()) return;
		throw RuntimeException("Attempted to make directory at path of a file.");
	}
	if (recursive) {
		Path dir_path;
		for (auto& s : path) {
			dir_path = (dir_path / s);
			if (!dir_path.exists()) {
				if (!CreateDirectoryW(PATH_WC_STR(dir_path), nullptr)) {
					throw RuntimeException("Failed to create a directory.");
				}
			}
		}
	} else {
		if (!CreateDirectoryW(PATH_WC_STR(path.string()), nullptr)) {
			throw RuntimeException("Failed to create a directory.");
		}
	}
}

void gen::file::make_soft_link(const AbstractPath& src, const AbstractPath& dst) {
	if (dst.exists()) throw RuntimeException("File or directory exists.");
	DWORD flags = 0;
	if (src.is_directory()) flags = SYMBOLIC_LINK_FLAG_DIRECTORY;
	if (CreateSymbolicLinkW(PATH_WC_STR(dst), PATH_WC_STR(src), flags)) {
		throw RuntimeException("Failed to create a soft link.");
	}
}

void gen::file::make_hard_link(const AbstractPath& src, const AbstractPath& dst) {
	if (dst.exists()) throw RuntimeException("File or directory exists.");
	if (CreateHardLinkW(PATH_WC_STR(dst), PATH_WC_STR(src), nullptr)) {
		throw RuntimeException("Failed to create a hard link.");
	}
}

void gen::file::move(const AbstractPath& src, const AbstractPath& dst) {
	if (dst.exists()) throw RuntimeException("File or directory exists.");
	if (MoveFileW(PATH_WC_STR(src), PATH_WC_STR(dst))) {
		throw RuntimeException("Failed to move a file or directory.");
	}
}

void gen::file::remove(const AbstractPath& path) {
	if (path.is_directory()) {
		if (RemoveDirectoryW(PATH_WC_STR(path))) {
			throw RuntimeException("Failed to remove a directory.");
		}
	}
	else {
		if (DeleteFileW(PATH_WC_STR(path))) {
			throw RuntimeException("Failed to remove a directory.");
		}
	}
}

gen::file::Path gen::file::get_working_dir() {
	size_t size = GetCurrentDirectoryW(0, nullptr);
	if (size == 0) throw gen::RuntimeException("Could not retrive the current"
		" working directory.");
	gen::Buffer<char16_t> wstr;
	GetCurrentDirectoryW(size, (LPWSTR)wstr.reserve(size));
	return Path(gen::utf8::encode(gen::utf16::decode((const char16_t*)wstr.ptr())));
}

gen::file::Path get_known_folder(REFKNOWNFOLDERID id, const gen::AbstractPath& suffix) {
	PWSTR wstr;
	if (SHGetKnownFolderPath(id, KF_FLAG_CREATE, NULL, &wstr) == S_OK) {
		gen::file::Path path = (gen::file::Path(gen::utf8::encode(gen::utf16::decode((const char16_t*)wstr))) / suffix.string());
		gen::file::make_directory(path);
		return path;
	}
	else return gen::file::Path();
}

gen::file::Path gen::file::get_pref_dir(StandardDir dir, const AbstractPath& suffix, bool user_specific) {
	if (user_specific) {
		if (dir == StandardDir::HOME) return get_known_folder(FOLDERID_Documents, suffix);
		else if ((dir == StandardDir::CONFIG) || (dir == StandardDir::DATA) || (dir == StandardDir::STATE)) return get_known_folder(FOLDERID_RoamingAppData, suffix);
		else if (dir == StandardDir::CACHE) return (get_known_folder(FOLDERID_LocalAppData, suffix) / "Temp");
	}
	else return get_known_folder(FOLDERID_ProgramData, suffix);

	return get_known_folder(FOLDERID_RoamingAppData, suffix);
}

gen::file::Path gen::file::get_user_dir(StandardUserDir dir, const AbstractPath& suffix) {
	if (dir == StandardUserDir::DESKTOP) return get_known_folder(FOLDERID_Desktop, suffix);
	else if (dir == StandardUserDir::DOCUMENTS) return get_known_folder(FOLDERID_Documents, suffix);
	else if (dir == StandardUserDir::DOWNLOADS) return get_known_folder(FOLDERID_Downloads, suffix);
	else if (dir == StandardUserDir::PICTURES) return get_known_folder(FOLDERID_Pictures, suffix);
	else if (dir == StandardUserDir::MUSIC) return get_known_folder(FOLDERID_Music, suffix);
	else if (dir == StandardUserDir::VIDEOS) return get_known_folder(FOLDERID_Videos, suffix);
	else return get_known_folder(FOLDERID_Documents, suffix);
}
