// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <gongen/common/containers.hpp>
#include <gongen/common/codecs.hpp>

namespace gen {
inline gen::Buffer<char16_t> cstr_to_wstr(const char* cstr) {
	auto ucs4 = gen::utf8::decode(cstr);
	return gen::utf16::encode(ucs4);
}

inline gen::Buffer<char16_t> cstr_to_path(const char* cstr) {
	gen::Buffer<char16_t> temp;
	gen::Buffer<char16_t> out;

	temp = gen::utf16::encode(gen::utf8::decode(cstr));

	size_t path_size = GetFullPathNameW((LPCWSTR)temp.ptr(), 0, nullptr, nullptr);
	out.reserve(path_size + 4);
	out[0] = '\\'; out[1] = '\\'; out[2] = '?'; out[3] = '\\';
	GetFullPathNameW((LPCWSTR)temp.ptr(), path_size, (LPWSTR)out.ptr(4), nullptr);
	return out;
}

extern thread_local gen::Buffer<char16_t> path_buffer;

inline const wchar_t* cstr_to_path_wstr(const char* cstr) {
	path_buffer = cstr_to_path(cstr);
	return (const wchar_t* )path_buffer.ptr();
}
}

#define THIS_PATH_WC_STR cstr_to_path_wstr(c_str())
#define PATH_WC_STR(path) cstr_to_path_wstr(path.c_str())
