// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include "winapi.hpp"
#include <DbgHelp.h>
#include <gongen/common/main.hpp>
#include <gongen/common/platform/system.hpp>
#include <gongen/common/codecs.hpp>

extern void init_wsock2();
extern void gen_init_event();
SYSTEM_INFO sys_info;
HANDLE process_handle = INVALID_HANDLE_VALUE;
gen::time::Time start_time;
bool initialized = false;
namespace gen {
	thread_local gen::Buffer<char16_t> path_buffer;
}

extern thread_local gen::String thread_name;
extern HRESULT(*SetThreadDescriptionFc)(HANDLE, PCWSTR);

extern void set_thread_name(const char* name);

BOOL GEN_API WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved) {
	if (fdwReason == DLL_PROCESS_ATTACH) {
		process_handle = GetModuleHandleA(NULL);
	}
	return TRUE;
}

void GEN_API gen::init() {
	if (!initialized) {
		if(process_handle == INVALID_HANDLE_VALUE) process_handle = GetModuleHandleA(NULL);
		start_time = gen::time::get_time();
		init_wsock2();
		SymSetOptions(SYMOPT_LOAD_LINES);
		SymInitialize(process_handle, nullptr, true);
		GetNativeSystemInfo(&sys_info);
		SetThreadDescriptionFc = (HRESULT(*)(HANDLE, PCWSTR))GetProcAddress(GetModuleHandleA("kernel32"), "SetThreadDescription");
		set_thread_name("Main");
		thread_name = "Main";
		gen_init_event();
	}
	initialized = true;
}

extern GEN_API int win32_feh_wrapper(int (*proc)(void*), void* userdata);

struct main_userdata {
	int argc;
	char** argv;
	int(*proc)(int, char**);
};

int gen_main_wrapper(void* userdata) {
	main_userdata* ud = (main_userdata*)userdata;
	return ud->proc(ud->argc, ud->argv);
}

int gen::wrap_main(int(*main_function)(int, char**), int argc, char** argv) {
	main_userdata ud;
	ud.argc = argc;
	ud.argv = argv;
	ud.proc = main_function;
	return win32_feh_wrapper(gen_main_wrapper, &ud);
}

int gen::feh::wrap(int(*function)(void*), void* userdata) {
	return win32_feh_wrapper(function, userdata);
}
