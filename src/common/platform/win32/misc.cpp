// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/platform/system.hpp>
#include "winapi.hpp"
#include <stdio.h>
#include <gongen/common/codecs.hpp>

#define TICKS_PER_SECOND 10000000
#define EPOCH_DIFFERENCE 11644473600LL

int64_t windowsTimeToUnixTime(FILETIME& tm) {
	LARGE_INTEGER largeInt;
	largeInt.LowPart = tm.dwLowDateTime;
	largeInt.HighPart = tm.dwHighDateTime;
	int64_t windowsTime = largeInt.QuadPart;
	int64_t unixTime = windowsTime / TICKS_PER_SECOND;
	if (unixTime < EPOCH_DIFFERENCE) return 0;
	unixTime = unixTime - EPOCH_DIFFERENCE;
	return unixTime;
}

extern gen::time::Time start_time;

int64_t gen::time::get_unix_timestamp() {
    FILETIME ft;
	GetSystemTimeAsFileTime(&ft);
	return windowsTimeToUnixTime(ft);
}
uint64_t gen::time::get_time_frequency() {
    LARGE_INTEGER li;
	QueryPerformanceFrequency(&li);
	return li.QuadPart;
}
gen::time::Time::Time() { t = get_time().t; }
gen::time::Time gen::time::get_time() {
    LARGE_INTEGER li;
	QueryPerformanceCounter(&li);
	return gen::time::Time(li.QuadPart);
}
gen::time::Time gen::time::from_start() {
	return (get_time() - start_time);
}

gen::dlib::Library gen::dlib::open(const AbstractPath& path) {
	if (path.string().empty()) return (gen::dlib::Library)GetModuleHandle(NULL);
	else {
		file::Path path2 = gen::file::Path(path.string() + ".dll");
		return LoadLibraryW((LPCWSTR)gen::cstr_to_path(path2.c_str()).ptr());
	}
}
void gen::dlib::close(Library lib) {
	if (lib) FreeLibrary((HMODULE)lib);
}
void* gen::dlib::symbol(Library lib, const char* function_name) {
	if (lib) return (void*)GetProcAddress((HMODULE)lib, function_name);
	else {
		return (void*)GetProcAddress(GetModuleHandleA(NULL), function_name);
	}
}

HANDLE stdHandle = INVALID_HANDLE_VALUE;

void gen::console::init() {
    AllocConsole();
	stdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
}
bool gen::console::is_present() {
	if (stdHandle == INVALID_HANDLE_VALUE) stdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	return (stdHandle != INVALID_HANDLE_VALUE);
}
void gen::console::write(const char* str) {
	if (stdHandle == INVALID_HANDLE_VALUE) stdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
    if (stdHandle != INVALID_HANDLE_VALUE) {
		gen::Buffer<char16_t> wstr = gen::cstr_to_wstr(str);
		WriteConsoleW(stdHandle, wstr.ptr(), wstr.size() - 1, NULL, NULL);
	}
	else printf("%s", str);
}

#undef ERROR

uint32_t gen::mb::pop_up(const gen::String& title, const gen::String& message, Type type, Buttons buttons) {
	auto wstr = gen::cstr_to_wstr(title.c_str());
	auto wstr2 = gen::cstr_to_wstr(message.c_str());
	DWORD flags = MB_OK;
	if (buttons == Buttons::OK_CANCEL) flags = MB_OKCANCEL;
	else if (buttons == Buttons::YES_NO) flags = MB_YESNO;
	if (type == Type::INFO) flags |= MB_ICONINFORMATION;
	else if (type == Type::WARNING) flags |= MB_ICONINFORMATION;
	else if (type == Type::ERROR) flags |= MB_ICONINFORMATION;
	int ret = MessageBoxW(nullptr, (LPCWSTR)wstr2.ptr(), (LPCWSTR)wstr.ptr(), flags);
	if (buttons == Buttons::OK_CANCEL) {
		if (ret == IDOK) return 0;
		else return 1;
	}
	else if (buttons == Buttons::YES_NO) {
		if (ret == IDYES) return 0;
		else return 1;
	}
	return 0;
}
