// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include "winapi.hpp"
#include <WinSock2.h>
#include <ws2tcpip.h>
#include <string.h>
#include <gongen/common/error.hpp>
#include <gongen/common/main.hpp>
#include <gongen/common/codecs.hpp>
#include <gongen/common/platform/system.hpp>

void init_wsock2() {
	WSADATA wsaData;
	int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (result != 0) {
		throw gen::RuntimeException("Can't initialize WinSock 2");
	}
}

GEN_API gen::Optional<gen::ip::Endpoint> gen::ip::resolve(const String& hostname, Optional<String> port) {
	DynamicArray<Endpoint> found = resolve_all(hostname, port, 1);
	if (found.empty()) return Optional<Endpoint>();
	return Optional<Endpoint>(found[0]);
}
GEN_API gen::DynamicArray<gen::ip::Endpoint> gen::ip::resolve_all(const String& hostname, Optional<String> port, size_t max) {
	uint16_t port_number = 0;
	if (port.some()) {
		port_number = (uint16_t)atoi(port.get().c_str());
	}

	addrinfoW addrInfo;
	RtlZeroMemory(&addrInfo, sizeof(addrinfoW));
	addrInfo.ai_flags = 0;
	addrInfo.ai_family = AF_UNSPEC;
	addrInfo.ai_socktype = SOCK_STREAM;
	addrInfo.ai_protocol = IPPROTO_TCP;

	gen::DynamicArray<gen::ip::Endpoint> out;

	gen::Buffer<char16_t> wstr = gen::cstr_to_wstr(hostname.c_str());
	ADDRINFOW* result;
	if (!GetAddrInfoW((PCWSTR)wstr.ptr(), NULL, &addrInfo, &result)) {
		ADDRINFOW* result2 = result;
		while (result2) {
			gen::ip::Endpoint endpoint;

			if (result2->ai_family == PF_INET) {
				sockaddr_in* address = (sockaddr_in*)result2->ai_addr;

				endpoint.port = address->sin_port;
				if (port_number) endpoint.port = port_number;
				endpoint.address.v6 = false;
				endpoint.address.addr.v4 = address->sin_addr.s_addr;
				out.add(endpoint);
			}
			else if (result2->ai_family == PF_INET6) {
				sockaddr_in6* address = (sockaddr_in6*)result2->ai_addr;

				endpoint.port = address->sin6_port;
				if (port_number) endpoint.port = port_number;
				endpoint.address.v6 = true;
				memcpy(endpoint.address.addr.v6, address->sin6_addr.u.Word, sizeof(uint16_t) * 8);
				out.add(endpoint);
			}
			if (max && (out.size() >= max)) break;

			result2 = result2->ai_next;
		}
		FreeAddrInfoW(result);
	}
	return out;
}

struct win32TCPSocket {
	SOCKET socket;
	gen::ip::Endpoint remote_address;
	bool is_server;
	gen::DynamicArray<gen::tcp::Socket*> owned_sockets;
	gen::ip::Endpoint endpoint;
};

gen::ip::Endpoint sock_get_remote_address(SOCKET sock) {
	int addrLen;
	SOCKADDR_STORAGE addr;
	RtlZeroMemory(&addr, sizeof(SOCKADDR_STORAGE));
	gen::ip::Endpoint out;
	if (getpeername((SOCKET)sock, (sockaddr*)&addr, &addrLen) != SOCKET_ERROR) {
		if (addr.ss_family == PF_INET) {
			sockaddr_in* address = (sockaddr_in*)&addr;

			out.port = address->sin_port;
			out.address.v6 = false;
			out.address.addr.v4 = address->sin_addr.s_addr;
		}
		else {
			sockaddr_in6* address = (sockaddr_in6*)&addr;

			out.port = address->sin6_port;
			out.address.v6 = false;
			memcpy(out.address.addr.v6, address->sin6_addr.u.Word, sizeof(uint16_t) * 8);
		}
	}
	else {
		ZeroMemory(&out, sizeof(gen::ip::Endpoint));
	}
	return out;
}

gen::String winsock_error_str();

win32TCPSocket* openSocket(const gen::ip::Endpoint& endpoint, bool is_server) {
	uint16_t portBigEndian = htons(endpoint.port);
	size_t addr_len = 0;
	SOCKADDR_STORAGE addr;
	RtlZeroMemory(&addr, sizeof(SOCKADDR_STORAGE));
	if (endpoint.address.v6) {
		sockaddr_in6* address = (sockaddr_in6*)&addr;
		addr_len = sizeof(sockaddr_in6);

		address->sin6_family = AF_INET6;
		memcpy(address->sin6_addr.u.Word, endpoint.address.addr.v6, sizeof(uint16_t) * 8);
		address->sin6_port = portBigEndian;
	}
	else {
		sockaddr_in* address = (sockaddr_in*)&addr;
		addr_len = sizeof(sockaddr_in);

		address->sin_family = AF_INET;
		address->sin_addr.S_un.S_addr = endpoint.address.addr.v4;
		address->sin_port = portBigEndian;
	}

	win32TCPSocket* out = new win32TCPSocket();
	out->socket = INVALID_SOCKET;
	out->is_server = is_server;
	out->endpoint = endpoint;

	SOCKET sock = INVALID_SOCKET;
	if (endpoint.address.v6) {
		sock = socket(AF_INET6, SOCK_STREAM, IPPROTO_TCP);
	}
	else {
		sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	}
	if (sock == INVALID_SOCKET) {
		throw gen::RuntimeException::createf("Failed to create a socket %s, because: %s", endpoint.to_string().c_str(), winsock_error_str().c_str());
	}
	//int temp = 1;
	//setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, (char*)&temp, sizeof(temp));
	if (endpoint.address.v6) {
		int temp = 0;
		setsockopt(sock, SOL_IPV6, IPV6_V6ONLY, (char*)&temp, sizeof(temp));
	}

	if (is_server) {
		int result = bind(sock, (const sockaddr*)&addr, addr_len);
		if (result == SOCKET_ERROR) {
			gen::String error = winsock_error_str();
			closesocket(sock);
			throw gen::RuntimeException::createf("Failed to bind a socket %s, because: %s", endpoint.to_string().c_str(), error.c_str());
		}
		if (sock == INVALID_SOCKET) return out;
		if (listen(sock, SOMAXCONN) == SOCKET_ERROR) {
			gen::String error = winsock_error_str();
			closesocket(sock);
			throw gen::RuntimeException::createf("Failed to listen on a socket %s, because: %s", endpoint.to_string().c_str(), error.c_str());
		}
		out->socket = sock;
		out->remote_address = sock_get_remote_address(sock);
	}
	else {
		int result = connect(sock, (const sockaddr*)&addr, addr_len);
		if (result == SOCKET_ERROR) {
			int a = WSAGetLastError();
			gen::String error = winsock_error_str();
			closesocket(sock);
			throw gen::RuntimeException::createf("Failed to connect on a socket %s, because: %s", endpoint.to_string().c_str(), error.c_str());
		}
		if (sock == INVALID_SOCKET) return out;

		out->socket = sock;
		out->remote_address = sock_get_remote_address(sock);
	}
	//unsigned long mode = 1;
	//ioctlsocket(sock, FIONBIO, &mode);

	return out;
}

gen::tcp::Socket::Socket(void* impl) {
	this->impl = impl;
}
gen::tcp::Socket::Socket() {
	this->impl = new win32TCPSocket();
	((win32TCPSocket*) this->impl)->socket = INVALID_SOCKET;
}
gen::tcp::Socket::Socket(const ip::Endpoint& endpoint, bool is_server) {
	this->impl = openSocket(endpoint, is_server);
}
gen::tcp::Socket::Socket(const String& hostname, bool is_server) {
	this->impl = openSocket(gen::ip::resolve(hostname).get(), is_server);
}
gen::tcp::Socket::~Socket() {
	win32TCPSocket* win32sock = (win32TCPSocket*)this->impl;
	close();
	delete win32sock;
}
bool gen::tcp::Socket::is_open() {
	win32TCPSocket* win32sock = (win32TCPSocket*)this->impl;
	return (win32sock->socket != INVALID_SOCKET);
}
void gen::tcp::Socket::close() {
	win32TCPSocket* win32sock = (win32TCPSocket*)this->impl;
	closesocket((SOCKET)win32sock->socket);
	for (auto& s : win32sock->owned_sockets) delete s;
}

gen::tcp::Socket* gen::tcp::Socket::accept(bool take_ownership) {
	win32TCPSocket* win32sock = (win32TCPSocket*)this->impl;
	SOCKET sock = INVALID_SOCKET;
	sock = ::accept(win32sock->socket, NULL, NULL);

	win32TCPSocket* out = new win32TCPSocket();
	out->is_server = false;
	out->socket = INVALID_SOCKET;
	if (sock != INVALID_SOCKET) {
		out->socket = sock;
		out->remote_address = sock_get_remote_address(sock);
		out->endpoint = out->remote_address;
		gen::tcp::Socket* sock = new gen::tcp::Socket(out);
		if (take_ownership) win32sock->owned_sockets.add(sock);
		return sock;
	}
	else return nullptr;
}

size_t gen::tcp::Socket::read(void* data, size_t size) {
	win32TCPSocket* win32sock = (win32TCPSocket*)this->impl;
	int result = ::recv(win32sock->socket, (char*)data, size, 0);
	if (result == SOCKET_ERROR) throw RuntimeException::createf("Socket read error: %i", WSAGetLastError());
	return result;
}
size_t gen::tcp::Socket::write(const void* data, size_t size) {
	win32TCPSocket* win32sock = (win32TCPSocket*)this->impl;
	int result = ::send(win32sock->socket, (const char*)data, size, 0);
	if (result == SOCKET_ERROR) throw RuntimeException::createf("Socket write error: %i", WSAGetLastError());
	return result;
}
bool gen::tcp::Socket::check(uint32_t timeout) {
	Buffer<Socket*> buff;
	buff.add(this);
	return !check_sockets_ids(buff, timeout).empty();
}
gen::ip::Endpoint gen::tcp::Socket::endpoint() {
	win32TCPSocket* win32sock = (win32TCPSocket*)this->impl;
	if (win32sock->is_server) return win32sock->endpoint;
	return win32sock->remote_address;
}

struct CheckSocket {
	size_t index;
	gen::tcp::Socket* socket;
};

gen::Buffer<size_t> gen::tcp::check_sockets_ids(const Buffer<Socket*>& sockets, uint32_t timeout) {
	gen::Buffer<size_t> out;

	gen::Buffer<CheckSocket> check_sockets;
	for (size_t i = 0; i < sockets.size(); i++) {
		if (sockets[i]) {
			CheckSocket cs;
			cs.index = i;
			cs.socket = sockets[i];
			check_sockets.add(cs);
		}
	}

	size_t count = check_sockets.size();
	if (!count) return out;
	size_t sets_count = ((count - 1) / FD_SETSIZE) + 1;

	uint32_t timeout2 = (timeout / sets_count);
	timeval timeout_val;
	timeout_val.tv_sec = timeout2 / 1000;
	timeout_val.tv_usec = (timeout2 % 1000) * 1000;

	for (size_t s = 0; s < sets_count; s++) {
		fd_set sockSet;
		if (count > FD_SETSIZE) {
			sockSet.fd_count = FD_SETSIZE;
			count -= FD_SETSIZE;
		}
		else {
			sockSet.fd_count = count;
			count = 0;
		}
		size_t check_sockets_index = (s * FD_SETSIZE);
		for (size_t f = 0; f < sockSet.fd_count; f++) {
			sockSet.fd_array[f] = ((win32TCPSocket*)check_sockets.get(check_sockets_index + f).socket->impl)->socket;
		}
		select(1, &sockSet, nullptr, nullptr, &timeout_val);
		for (size_t f = 0; f < sockSet.fd_count; f++) {
			win32TCPSocket* win32sock = ((win32TCPSocket*)check_sockets.get(check_sockets_index + f).socket->impl);
			if (FD_ISSET(win32sock->socket, &sockSet)) {
				out.add(check_sockets.get(check_sockets_index).index);
			}
		}
	}

	return out;
}

gen::String winsock_error_str() {
	wchar_t* s = NULL;
	FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, WSAGetLastError(),
		MAKELANGID(LANG_ENGLISH, SUBLANG_ENGLISH_US),
		(LPWSTR)&s, 0, NULL);
	gen::String out = gen::utf8::encode(gen::utf16::decode((const char16_t*)s));
	LocalFree(s);
	return out;
}
