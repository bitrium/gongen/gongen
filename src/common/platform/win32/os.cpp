// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#define BUILD_WINDOWS
#include "winapi.hpp"
#include <gongen/common/platform/system.hpp>

extern SYSTEM_INFO sys_info;
typedef DWORD(CALLBACK* RTLGETVERSION) (PRTL_OSVERSIONINFOW lpVersionInformation);
typedef BOOL(WINAPI* ISWOW64PROCESS) (HANDLE, PBOOL);

bool os_is_64bit();

gen::String gen::os::system_name() {
	bool is_x64 = os_is_64bit();
	OSVERSIONINFOA version_info;
	version_info.dwOSVersionInfoSize = sizeof(version_info);
	GetVersionExA(&version_info);
	if ((version_info.dwMajorVersion < 6) || ((version_info.dwMajorVersion == 6) && (version_info.dwMinorVersion < 1))) {
		if (is_x64) return "Windows Pre-7 (x64)";
		else return "Windows Pre-7 (x86)";
	}
	else if ((version_info.dwMajorVersion == 6) && (version_info.dwMinorVersion == 1)) {
		if (is_x64) return "Windows 7 (x64)";
		else return "Windows 7 (x86)";
	}
	else if ((version_info.dwMajorVersion == 6) && (version_info.dwMinorVersion == 2)) {
		HMODULE ntdll = LoadLibraryA("Ntdll.dll");
		RTLGETVERSION pRtlGetVersion = (RTLGETVERSION)GetProcAddress(ntdll, "RtlGetVersion");;
		if (pRtlGetVersion) {
			RTL_OSVERSIONINFOW ovi = { 0 };
			ovi.dwOSVersionInfoSize = sizeof(ovi);
			DWORD nt_status = pRtlGetVersion(&ovi);
			if (nt_status == 0) {
				if ((ovi.dwMajorVersion == 6) && (ovi.dwMinorVersion == 3)) {
					if (is_x64) return "Windows 8.1 (x64)";
					else return "Windows 8.1 (x86)";
				}
				if (ovi.dwMajorVersion == 10) {
					if (ovi.dwBuildNumber >= 22000) {
						if (is_x64) return gen::String::createf("Windows 11 %lu (x64)", ovi.dwBuildNumber);
						else return gen::String::createf("Windows 11 %lu (x86)", ovi.dwBuildNumber);
					}
					else {
						if (is_x64) return gen::String::createf("Windows 10 %lu (x64)", ovi.dwBuildNumber);
						else return gen::String::createf("Windows 10 %lu (x86)", ovi.dwBuildNumber);
					}
				}
			}
		}
		if (is_x64) return "Windows 8 (x64)";
		else return "Windows 8 (x86)";
	}
	if (is_x64) return "Unknown Windows (x64)";
	else return "Unknown Windows (x86)";
}
size_t gen::os::memory_amount() {
	MEMORYSTATUSEX mem_info;
	mem_info.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&mem_info);
	return (((mem_info.ullTotalPhys) / 1024) / 1024);
}
gen::os::Architecture gen::os::cpu_type() {
	if (sys_info.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_AMD64) return gen::os::Architecture::X64;
	else if (sys_info.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_INTEL) return gen::os::Architecture::X86;
	else if (sys_info.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_ARM) return gen::os::Architecture::ARM;
	else if (sys_info.wProcessorArchitecture == PROCESSOR_ARCHITECTURE_ARM64) return gen::os::Architecture::ARM64;
	return gen::os::Architecture::UNKNOWN;
}
int gen::os::execute(const gen::String& command) {
	auto wstr = cstr_to_wstr(command.c_str());
	return _wsystem((const wchar_t*)wstr.ptr());
}

bool os_is_64bit() {
#if defined(_WIN64)
	return true;
#else
	ISWOW64PROCESS IsWow64Process_fc = (ISWOW64PROCESS)GetProcAddress(GetModuleHandleA("kernel32"), "IsWow64Process");
	if (!IsWow64Process_fc) return false;
	else {
		BOOL is_x64 = false;
		IsWow64Process_fc(GetCurrentProcess(), &is_x64);
		return is_x64;
	}
#endif
}
