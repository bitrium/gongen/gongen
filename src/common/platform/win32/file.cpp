// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/platform/system.hpp>
#include <gongen/common/error.hpp>
#include "winapi.hpp"

struct win32File {
	gen::file::Mode open_mode;
	gen::String filepath;
	HANDLE os_handle = INVALID_HANDLE_VALUE;
};

gen::File::File() {
	this->impl = new win32File();
}

gen::file::File::File(const AbstractPath& path, Mode open_mode) : File() {
	win32File* file_impl = (win32File*)impl;

	auto wstr = cstr_to_path(path.c_str());
	DWORD dwDesiredAccess = 0;
	DWORD dwShareMode = 0;
	DWORD dwCreationDisposition = 0;
	if (open_mode == Mode::READ) {
		dwDesiredAccess = GENERIC_READ;
		dwShareMode = FILE_SHARE_READ;
		dwCreationDisposition = OPEN_EXISTING;
	}
	else if (open_mode == Mode::WRITE) {
		dwDesiredAccess = GENERIC_WRITE;
		dwShareMode = FILE_SHARE_READ | FILE_SHARE_WRITE;
		dwCreationDisposition = OPEN_ALWAYS;
	}
	else if (open_mode == Mode::APPEND) {
		dwDesiredAccess = GENERIC_WRITE | FILE_APPEND_DATA;
		dwShareMode = FILE_SHARE_READ;
		dwCreationDisposition = OPEN_ALWAYS;
	}
	else if (open_mode == Mode::READ_WRITE) {
		dwDesiredAccess = GENERIC_WRITE | GENERIC_READ;
		dwShareMode = FILE_SHARE_READ;
		dwCreationDisposition = OPEN_ALWAYS;
	}

	HANDLE handle = CreateFileW((const wchar_t*)wstr.ptr(), dwDesiredAccess, dwShareMode, nullptr, dwCreationDisposition, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (open_mode == Mode::WRITE) SetEndOfFile(handle);
	file_impl->filepath = path.string();

	if (handle == INVALID_HANDLE_VALUE) throw gen::file::OpenException(gen::String::createf("Can't open file: %s", file_impl->filepath.c_str()));
	file_impl->open_mode = open_mode;
	file_impl->os_handle = handle;
}
gen::file::File::~File() {
	win32File* file_impl = (win32File*)impl;
	close();
	delete file_impl;
}

gen::File& gen::file::File::operator=(const gen::File& other) {
	auto* impl = (win32File*) this->impl;
	this->close();
	auto* other_impl = (win32File*) other.impl;
	impl->filepath = other_impl->filepath;
	impl->open_mode = other_impl->open_mode;
	impl->os_handle = other_impl->os_handle;
	other_impl->os_handle = INVALID_HANDLE_VALUE;
	return *this;
}

bool gen::file::File::is_open() {
	win32File* file_impl = (win32File*)impl;
	return (file_impl->os_handle != INVALID_HANDLE_VALUE);
}
void gen::file::File::close() {
	if (!is_open()) return;
	win32File* file_impl = (win32File*)impl;
	CloseHandle((HANDLE)file_impl->os_handle);
	file_impl->os_handle = INVALID_HANDLE_VALUE;
}

extern int64_t windowsTimeToUnixTime(FILETIME& tm);

int64_t gen::file::File::time_created() {
	win32File* file_impl = (win32File*)impl;
	if (!is_open()) throw gen::file::Exception("File is not open.");
	FILETIME tm;
	GetFileTime((HANDLE)file_impl->os_handle, &tm, nullptr, nullptr);
	return windowsTimeToUnixTime(tm);
}
int64_t gen::file::File::time_modified() {
	win32File* file_impl = (win32File*)impl;
	if (!is_open()) throw gen::file::Exception("File is not open.");
	FILETIME tm;
	GetFileTime((HANDLE)file_impl->os_handle, nullptr, nullptr, &tm);
	return windowsTimeToUnixTime(tm);
}
int64_t gen::file::File::time_accessed() {
	win32File* file_impl = (win32File*)impl;
	if (!is_open()) throw gen::file::Exception("File is not open.");
	FILETIME tm;
	GetFileTime((HANDLE)file_impl->os_handle, nullptr, &tm, nullptr);
	return windowsTimeToUnixTime(tm);
}

uint64_t gen::file::File::size() {
	win32File* file_impl = (win32File*)impl;
	if (!is_open()) throw gen::file::Exception("File is not open.");
	LARGE_INTEGER integer;
	GetFileSizeEx((HANDLE)file_impl->os_handle, &integer);
	return integer.QuadPart;
}
bool gen::file::File::set_size(size_t size) {
	win32File* file_impl = (win32File*)impl;
	if (!is_open()) throw gen::file::Exception("File is not open.");
	uint64_t cursorPos = cursor();
	seek(size);
	SetEndOfFile((HANDLE)file_impl->os_handle);
	seek(cursorPos);
	return true;
}
uint64_t gen::file::File::cursor() {
	return seek(0, Seek::CUR);
}
uint64_t gen::file::File::seek(ptrdiff_t offset, Seek type) {
	win32File* file_impl = (win32File*)impl;
	if (!is_open()) throw gen::file::Exception("File is not open.");
	LARGE_INTEGER out;
	LARGE_INTEGER in;
	in.QuadPart = offset;
	out.QuadPart = 0;
	if (type == Seek::SET) SetFilePointerEx((HANDLE)file_impl->os_handle, in, &out, FILE_BEGIN);
	else if (type == Seek::CUR) SetFilePointerEx((HANDLE)file_impl->os_handle, in, &out, FILE_CURRENT);
	else if (type == Seek::END) SetFilePointerEx((HANDLE)file_impl->os_handle, in, &out, FILE_END);
	return out.QuadPart;
}

size_t gen::file::File::read(void* data, size_t size) {
	win32File* file_impl = (win32File*)impl;
	if (!is_open()) throw gen::file::Exception("File is not open.");
	if (!((file_impl->open_mode == Mode::READ) || (file_impl->open_mode == Mode::READ_WRITE))) {
		throw gen::file::Exception(gen::String::createf("File open mode does not allow reading: %s", file_impl->filepath.c_str()));
	}
	DWORD out;
	if (!ReadFile((HANDLE)file_impl->os_handle, data, size, &out, nullptr)) {
		throw RuntimeException(gen::String::createf("Failed to read from file: %s", file_impl->filepath.c_str()));
	}
	return out;
}
size_t gen::file::File::write(const void* data, size_t size) {
	win32File* file_impl = (win32File*)impl;
	if (!is_open()) {
		throw gen::file::Exception("File is not open.");
	}
	if (!((file_impl->open_mode == Mode::WRITE) || (file_impl->open_mode == Mode::APPEND) || (file_impl->open_mode == Mode::READ_WRITE))) {
		throw gen::file::Exception(gen::String::createf("File open mode does not allow writing: %s", file_impl->filepath.c_str()));
	}
	DWORD out;
	if (!WriteFile((HANDLE)file_impl->os_handle, data, size, &out, nullptr)) {
		throw RuntimeException(gen::String::createf("Failed to write to file: %s", file_impl->filepath.c_str()));
	}
	return out;
}
void gen::file::File::flush() {
	win32File* file_impl = (win32File*)impl;
	if (!is_open()) throw gen::file::Exception("File is not open.");
	FlushFileBuffers((HANDLE)file_impl->os_handle);
}
