// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/platform/system.hpp>
#include "unistd.h"
#include <sys/stat.h>
#include <fcntl.h>

struct FileImpl {
	int desc;
	gen::file::Mode mode;
};

gen::File::File() {
	auto* impl = new FileImpl();
	impl->desc = -1;
	this->impl = impl;
}

gen::File::File(const AbstractPath& ab_path, Mode open_mode) : File() {
	gen::file::Path path(ab_path);
	if (path.exists() && path.is_directory()) throw ArgumentException("File is a directory.");
	int flags = 0;
	switch (open_mode) {
	case file::Mode::APPEND:
		flags = O_CREAT | O_WRONLY | O_APPEND;
		break;
	case file::Mode::READ:
		flags = O_RDONLY;
		break;
	case file::Mode::READ_WRITE:
		flags = O_CREAT | O_RDWR;
		break;
	case file::Mode::WRITE:
		flags = O_CREAT | O_WRONLY | O_TRUNC;
		break;
	}
	auto* impl = (FileImpl*) this->impl;
	impl->desc = open(path.resolve<Path>().c_str(), flags, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);
	if (impl->desc == -1) throw RuntimeException::from_errno("Could not open file '%s'", path.c_str());
	impl->mode = open_mode;
}

gen::file::File::~File() {
	this->close();
	delete (FileImpl*) this->impl;
	this->impl = nullptr;
}

gen::File& gen::file::File::operator=(const gen::File& other) {
	auto* impl = (FileImpl*) this->impl;
	this->close();
	auto* other_impl = (FileImpl*) other.impl;
	impl->desc = other_impl->desc;
	impl->mode = other_impl->mode;
	other_impl->desc = -1;
	return *this;
}


bool gen::File::is_open() {
	return ((FileImpl*) this->impl)->desc != -1;
}

void gen::File::close() {
	if (!this->is_open()) return;
	auto* impl = (FileImpl*) this->impl;
	::close(impl->desc);
	impl->desc = -1;
}

int64_t gen::File::time_created() {
	if (!this->is_open()) throw RuntimeException("File is not open.");
	struct stat st = {0};
	fstat(((FileImpl*) this->impl)->desc, &st);
	return st.st_ctim.tv_sec;
}

int64_t gen::File::time_modified() {
	if (!this->is_open()) throw RuntimeException("File is not open.");
	struct stat st = {0};
	fstat(((FileImpl*) this->impl)->desc, &st);
	return st.st_mtim.tv_sec;
}

int64_t gen::File::time_accessed() {
	if (!this->is_open()) throw RuntimeException("File is not open.");
	struct stat st = {0};
	fstat(((FileImpl*) this->impl)->desc, &st);
	return st.st_atim.tv_sec;
}


uint64_t gen::File::size() {
	if (!this->is_open()) throw RuntimeException("File is not open.");
	uint64_t cur = this->cursor();
	uint64_t size = this->seek(0, Seek::END);
	this->seek(cur, Seek::SET);
	return size;
}

bool gen::File::set_size(uint64_t size) {
	if (!this->is_open()) throw RuntimeException("File is not open.");
	ftruncate(((FileImpl*) this->impl)->desc, size);
	return true;
}

uint64_t gen::File::cursor() {
	if (!this->is_open()) throw RuntimeException("File is not open.");
	return this->seek(0, Seek::CUR);
}

uint64_t gen::File::seek(int64_t offset, Seek type) {
	if (!this->is_open()) throw RuntimeException("File is not open.");
	int base = 0;
	if (type == Seek::CUR) base = SEEK_CUR;
	else if (type == Seek::SET) base = SEEK_SET;
	else if (type == Seek::END) base = SEEK_END;
	off_t result = lseek(((FileImpl*) this->impl)->desc, offset, base);
	if (result == -1) throw RuntimeException::from_errno("Failed to seek file");
	return result;
}


uint64_t gen::File::read(void* data, uint64_t size) {
	if (!this->is_open()) throw RuntimeException("File is not open.");
	auto* impl = ((FileImpl*) this->impl);
	if (impl->mode != file::Mode::READ && impl->mode != file::Mode::READ_WRITE) {
		throw RuntimeException("File open mode does not allow reading.");
	}
	ssize_t result = ::read(impl->desc, data, size);
	if (result == -1) throw RuntimeException::from_errno("Failed to read file");
	return (uint64_t) result;
}

uint64_t gen::File::write(const void* data, uint64_t size) {
	if (!this->is_open()) throw RuntimeException("File is not open.");
	auto* impl = ((FileImpl*) this->impl);
	if (impl->mode == file::Mode::READ) throw RuntimeException("File open mode does not allow writing.");
	ssize_t result = ::write(impl->desc, data, size);
	if (result == -1) throw RuntimeException::from_errno("Failed to write file");
	return (uint64_t) result;
}

void gen::File::flush() {
	if (!this->is_open()) throw RuntimeException("File is not open.");
	auto* impl = ((FileImpl*) this->impl);
	if (impl->mode == file::Mode::READ) throw RuntimeException("File open mode does not allow writing.");
	ssize_t result = fsync(impl->desc);
	if (result == -1) throw RuntimeException::from_errno("Failed to flush file");
}
