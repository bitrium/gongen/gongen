// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/platform/system.hpp>
#include <gongen/common/main.hpp>
#include <time.h>
#include <dlfcn.h>
#include <stdio.h>

static inline time_t c_time(time_t* ptr) {
	return time(ptr);
}

int64_t gen::time::get_unix_timestamp() {
	return c_time(nullptr);
}

uint64_t gen::time::get_time_frequency() {
	return 1000000;
}

gen::time::Time::Time() {
	this->t = get_time().t;
}
gen::time::Time gen::time::get_time() {
	struct timespec spec;
	clock_gettime(CLOCK_REALTIME, &spec);
	return Time(spec.tv_sec * 1000000 + spec.tv_nsec / 1000);
}

gen::time::Time start_time;
void gen_init_time() {
	start_time = gen::time::get_time();
}
gen::time::Time gen::time::from_start() {
	return gen::time::get_time() - start_time;
}


gen::dlib::Library gen::dlib::open(const gen::AbstractPath& path) {
	void* handle = dlopen(path.c_str(), RTLD_LAZY);
	if (!handle) throw RuntimeException("Could not open a dynamic library.");
	return handle;
}

void gen::dlib::close(gen::dlib::Library lib) {
	if (!lib) return;
	dlclose(lib);
}

void* gen::dlib::symbol(gen::dlib::Library lib,
                        const char* function_name) {
	void* result = dlsym(lib, function_name);
	if (!result) {
		// TODO diagnose error with 'dlerror()'
		throw RuntimeException("Could not obtain a library symbol.");
	}
	return result;
}

extern void populate_standard_user_dirs();

void gen::console::init() {
	populate_standard_user_dirs();
}

bool gen::console::is_present() {
	return true; //TODO: Macol please implement this
}

void gen::console::write(const char* str) {
	printf("%s", str);
}
