// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/containers/pointers.hpp>
#include <gongen/common/platform/system.hpp>
#include <gongen/common/platform/memory.hpp>
#include <pthread.h>
#include <unistd.h>
#include <atomic>

void gen::thread::sleep(uint64_t ms) {
	usleep(ms * 1000);
}

bool gen::thread::ThreadId::operator==(const ThreadId& id) const {
	return (bool)pthread_equal((pthread_t)this->impl, (pthread_t)id.impl);
}

bool gen::thread::ThreadId::operator!=(const ThreadId& id) const {
	return !(*this == id);
}

gen::thread::ThreadId::~ThreadId() {
}

size_t gen::thread::ThreadId::get_number() {
	return (size_t) (pthread_t) this->impl;
}

gen::thread::ThreadId gen::thread::id() {
	pthread_t number = pthread_self();
    return gen::thread::ThreadId((void*) number);
}

gen::String gen::thread::name() {
	pthread_t number = pthread_self();
	char buf[16];
	int ret = pthread_getname_np(number, buf, 16);
	if (ret != 0) {
		// TODO error values
		throw gen::RuntimeException("Could not retrieve current thread name.");
	}
	return gen::String(buf);
}


struct ThreadImpl {
	pthread_t id;
	void* (*proc)(void*);
	void* user;
	std::atomic<int> ref_counter = 2;
	std::atomic<bool> done = false;
	gen::Ptr<gen::Exception> exception;
};

void* wrap_thread(void* arg) {
	ThreadImpl* impl = (ThreadImpl*) arg;
	try {
		impl->user = impl->proc(impl->user);
	} catch (const gen::Exception& exception) {
		impl->exception = exception.copy();
	} catch (const std::exception& exception) {
		impl->exception = new gen::StdBasedException(exception.what());
	}
	impl->done = true;
	impl->ref_counter--;
	if (impl->ref_counter <= 0) {
		gen::free(impl);
	}
	return nullptr;
}

gen::Thread::Thread(void* (*proc)(void*), void* arg, const char* name) {
	ThreadImpl* impl = new ThreadImpl();
	impl->proc = proc;
	impl->user = arg;
	int result = pthread_create(&impl->id, nullptr, wrap_thread, impl);
	if (result != 0) {
		// TODO handle errors separately
		throw RuntimeException("Could not create a thread.");
	}
	if (name) {
		char* short_name = nullptr;
		if (strlen(name) > 15) {
			short_name = (char*) gen::calloc(16);
			strncpy(short_name, name, 15);
			name = short_name;
		}
		pthread_setname_np(impl->id, name);
		gen::free(short_name);
	}
	this->impl = impl;
}

gen::thread::Thread::~Thread() {
	if (!this->impl) return;
	ThreadImpl* impl = (ThreadImpl*) this->impl;
	if (--impl->ref_counter <= 0) {
		delete impl;
	}
}

gen::thread::Thread& gen::thread::Thread::operator=(const Thread& other) {
	this->impl = other.impl;
	((ThreadImpl*) this->impl)->ref_counter++;
	return *this;
}

gen::thread::ThreadId gen::thread::Thread::id() const {
	if (!this->impl) throw FunctionCallException("Uninitialized thread.");
	pthread_t* id = gen::talloc<pthread_t>();
	memcpy(id, &((ThreadImpl*) this->impl)->id, sizeof(pthread_t));
    return gen::thread::ThreadId(id);
}

bool gen::Thread::done() const {
	if (!this->impl) return true;
	return ((ThreadImpl*) this->impl)->done;
}

void* gen::Thread::join() {
	if (!this->impl) throw FunctionCallException("Uninitialized thread.");
	ThreadImpl* impl = (ThreadImpl*) this->impl;
	if (!impl->done) {
		int result = pthread_join(impl->id, 0);
		if (result != 0) {
			// TODO handle errors separately
			throw RuntimeException("Could not join a thread.");
		}
	}
	return impl->user;
}

void gen::Thread::kill() {
	if (!this->impl) return;
	ThreadImpl* impl = (ThreadImpl*) this->impl;
	if (impl->done) return;
	int result = pthread_cancel(impl->id);
	if (result != 0) {
		// TODO handle errors separately
		throw RuntimeException("Could not kill a thread.");
	}
}

gen::Thread& gen::Thread::rethrow_exception() {
	if (!this->impl) return *this;
	ThreadImpl* impl = (ThreadImpl*) this->impl;
	if (!impl->done || !impl->exception.ptr()) return *this;
	impl->exception->rethrow();
	return *this;
}
