// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/platform/system.hpp>
#include <sys/utsname.h>
#include <thread>

// remaining functions:
// memory_amount

gen::String gen::os::system_name() {
	struct utsname buf = {};
	int res = uname(&buf);
	if (res != 0) {
		throw gen::RuntimeException("Could not retrieve system information.");
	}
	gen::String name;
	name += buf.sysname;
	name += " ";
	name += buf.release;
	return name;
}

gen::os::Architecture gen::os::cpu_type() {
	struct utsname buf = {};
	int res = uname(&buf);
	if (res != 0) {
		throw gen::RuntimeException("Could not retrieve system information.");
	}
	if (strcmp(buf.machine, "x86_64") == 0 || strcmp(buf.machine, "amd64") == 0) {
		return Architecture::X64;
	}
	if (strcmp(buf.machine, "i686") == 0 || strcmp(buf.machine, "i386") == 0) {
		return Architecture::X86;
	}
	if (strcmp(buf.machine, "arm64") == 0) {
		return Architecture::ARM64;
	}
	if (strncmp(buf.machine, "arm", 3) == 0) {
		return Architecture::ARM;
	}
	return Architecture::UNKNOWN;
}

size_t gen::os::thread_count() {
	return std::thread::hardware_concurrency();
}
