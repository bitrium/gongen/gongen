// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/platform/system.hpp>
#include <gongen/common/error.hpp>
#include <unistd.h>
#include <sys/stat.h>
#include <dirent.h>
#include <limits.h>

using namespace gen::file;

void Path::update_string() {
	this->str.clear();
	if (this->is_absolute()) {
		this->str.append("/");
	}
	if (this->segments.empty()) {
		if (this->str.empty()) this->str = ".";
		return;
	}
	for (PtrIterator<gen::String> it = this->segments.begin();
			it != this->segments.end() - 1; it++) {
		this->str.append(*it);
		this->str.append("/");
	}
	this->str.append(this->segments.last());
}

bool Path::exists() const {
	return access(this->c_str(), F_OK) == 0;
}

bool Path::readable() const {
	return access(this->c_str(), R_OK) == 0;
}

bool Path::writeable() const {
	return access(this->c_str(), W_OK) == 0;
}

bool Path::is_directory() const {
	struct stat buf;
	if (stat(this->c_str(), &buf) != 0) {
		throw gen::RuntimeException::from_errno("Could not gen information for path '%s'", this->c_str());
	}
	return S_ISDIR(buf.st_mode);
}

Path& Path::to_resolved() {
	if (this->is_abs) return *this;
	DynamicArray<String> copy = this->segments;
	this->segments.clear();
	this->segments = get_working_dir().get_segments();
	this->segments += copy;
	this->is_abs = true;
	update_string();
	return *this;
}

gen::DynamicArray<gen::String> Path::enumerate_names() const {
	auto names = DynamicArray<String>();
	if (!this->is_directory()) return names;
	DIR* d;
	d = opendir(this->c_str());
	if (!d) return names;
	struct dirent* dir = readdir(d);
	while (dir) {
		const char* name = dir->d_name;
		if (
			(strlen(name) != 1 || *name != '.') &&
			(strlen(name) != 2 || memcmp(name, "..", 2) != 0)
		) {
			names.emplace(name);
		}
		dir = readdir(d);
	}
	closedir(d);
	return names;
}

void gen::file::make_directory(const AbstractPath& path, bool recursive) {
	if (path.exists()) {
		if(path.is_directory()) return;
		throw RuntimeException("Attempted to make directory at path of a file.");
	}
	if(recursive) {
		gen::file::Path p2 = path.absolute<gen::file::Path>();
		gen::file::Path p("/");
		for(auto segment : path.get_segments()) {
			p /= segment;
			make_directory(p, false);
		}
	} else {
		mkdir(path.absolute<gen::file::Path>().c_str(), 0700);
	}
}

void gen::file::make_soft_link(const AbstractPath& src, const AbstractPath& dst) {
	if (dst.exists()) throw RuntimeException("File or directory exists.");
	int result = symlink(src.c_str(), dst.c_str());
	if (result != 0) {
		// TODO handle specific errors
		throw RuntimeException("Failed to create a soft link.");
	}
}

void gen::file::make_hard_link(const AbstractPath& src, const AbstractPath& dst) {
	if (dst.exists()) throw RuntimeException("File or directory exists.");
	int result = link(src.c_str(), dst.c_str());
	if (result != 0) {
		// TODO handle specific errors
		throw RuntimeException("Failed to create a hard link.");
	}
}

void gen::file::move(const AbstractPath& src, const AbstractPath& dst) {
	if (dst.exists()) throw RuntimeException("File or directory exists.");
	int result = rename(src.c_str(), dst.c_str());
	if (result != 0) {
		// TODO handle specific errors
		throw RuntimeException("Failed to move a file or directory.");
	}
}

void gen::file::remove(const AbstractPath& path) {
	// TODO recursive remove support
	if (path.is_directory()) {
		int result = rmdir(path.c_str());
		if (result != 0) {
			// TODO handle specific errors
			throw RuntimeException("Failed to remove a directory.");
		}
	} else {
		int result = unlink(path.c_str());
		if (result != 0) {
			// TODO handle specific errors
			throw RuntimeException("Failed to remove a file.");
		}
	}
}

Path gen::file::get_working_dir() {
	char cwd[PATH_MAX];
	if (!getcwd(cwd, PATH_MAX)) {
		// TODO handle errors
		throw gen::RuntimeException("Could not retrive the current"
                                    " working directory.");
	}
	return Path(cwd);
}
