// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/platform/system.hpp>
#include <sys/sysinfo.h>

size_t gen::os::memory_amount() {
	struct sysinfo info;
	sysinfo(&info);
	return info.totalram / 1024 / 1024;
}
