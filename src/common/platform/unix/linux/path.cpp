// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/platform/system.hpp>
#include <gongen/common/containers.hpp>
#include "unistd.h"
#include <stdlib.h>
#include <pwd.h>

using namespace gen::file;

Path gen::file::get_pref_dir(StandardDir dir,
		const gen::AbstractPath& suffix, bool user_specific) {
	String path;
	if (!user_specific) {
		switch (dir) {
		case StandardDir::HOME:
		case StandardDir::CONFIG:
			path = "/etc";
			break;
		case StandardDir::DATA:
			path = "/usr/share";
			break;
		case StandardDir::CACHE:
			path = "/var/cache";
			break;
		case StandardDir::STATE:
			path = "/var/lib";
			break;
		}
	} else {
		const char* env_home = getenv("HOME");
		if (env_home) {
			path = String(env_home);
		} else {
			struct passwd* pw = getpwuid(getuid());
			path = String(pw->pw_dir);
		}
		const char* env_dir;
		switch (dir) {
		case StandardDir::HOME:
			env_dir = nullptr;
			break;
		case StandardDir::CONFIG:
			env_dir = "XDG_CONFIG_HOME";
			path += "/.config";
			break;
		case StandardDir::DATA:
			env_dir = "XDG_DATA_HOME";
			path += "/.local/share";
			break;
		case StandardDir::CACHE:
			env_dir = "XDG_CACHE_HOME";
			path += "/.cache";
			break;
		case StandardDir::STATE:
			env_dir = "XDG_STATE_HOME";
			path += "/.local/state";
			break;
		}
		if (env_dir) {
			env_dir = getenv(env_dir);
			if (env_dir) path = String(env_dir);
		}
	}
	Path result = Path(path);
	if (!suffix.is_empty()) {
		result /= suffix;
	}
	make_directory(result);
	return result;
}

bool std_user_dirs_populated = false;
gen::file::Path std_user_dirs[(uint8_t) StandardUserDir::VIDEOS + 1];

void populate_standard_user_dirs() {
	if (std_user_dirs_populated) return;
	for (uint8_t i = 0; i < sizeof(std_user_dirs) / sizeof(std_user_dirs)[i]; i++) {
		std_user_dirs[i].to_relative();
	}
	static const char* key_start = "XDG_";
	static const char* key_end = "_DIR";
	static const char* value_home = "$HOME/";
	Path config_path = get_pref_dir(StandardDir::CONFIG) / "user-dirs.dirs";
	Path home_path = get_pref_dir(StandardDir::HOME);
	while (config_path.exists() && !config_path.is_directory()
			&& config_path.readable()) { // only runs once
		File config = GEN_NO_EXCEPT(File(config_path, Mode::READ));
		if (!config.is_open()) break;
		gen::String buffer;
		buffer.reserve(config.size());
		config.read(buffer.data(), buffer.size());
		config.close();
		gen::DynamicArray<gen::String> lines = buffer.split("\n");
		for (gen::String& line : lines) {
			line.strip();
			if (!line.starts_with(key_start)) continue;
			gen::Optional<size_t> equals_pos = line.find("=");
			if (equals_pos.none()) continue;
			gen::String key = line.segmented(0, equals_pos.get()).stripped_r();
			if (!key.ends_with(key_end)) continue;
			key.segment(strlen(key_start), key.size() - strlen(key_end));
			StandardUserDir dir;
			if (key == "DESKTOP") {
				dir = StandardUserDir::DESKTOP;
			} else if (key == "DOCUMENTS") {
				dir = StandardUserDir::DOCUMENTS;
			} else if (key == "DOWNLOAD") {
				dir = StandardUserDir::DOWNLOADS;
			} else if (key == "PICTURES") {
				dir = StandardUserDir::PICTURES;
			} else if (key == "MUSIC") {
				dir = StandardUserDir::MUSIC;
			} else if (key == "VIDEOS") {
				dir = StandardUserDir::VIDEOS;
			} else continue;
			gen::String string = line.segmented(equals_pos.get() + 1).stripped_l();
			if (!string.starts_with("\"") || !string.ends_with("\"")) continue;
			string.segment(1, string.size() - 1).strip();
			if (string.starts_with(value_home)) {
				string.segment(strlen(value_home));
				std_user_dirs[(uint8_t) dir] = home_path / string;
			} else {
				std_user_dirs[(uint8_t) dir] = Path(string);
			}
		}
		break;
	}
	if (!std_user_dirs[(uint8_t) StandardUserDir::DESKTOP].is_absolute()) {
		std_user_dirs[(uint8_t) StandardUserDir::DESKTOP]
				= home_path / "Desktop";
	}
	if (!std_user_dirs[(uint8_t) StandardUserDir::DOCUMENTS].is_absolute()) {
		std_user_dirs[(uint8_t) StandardUserDir::DOCUMENTS]
				= home_path / "Documents";
	}
	if (!std_user_dirs[(uint8_t) StandardUserDir::DOWNLOADS].is_absolute()) {
		std_user_dirs[(uint8_t) StandardUserDir::DOWNLOADS]
				= home_path / "Downloads";
	}
	if (!std_user_dirs[(uint8_t) StandardUserDir::PICTURES].is_absolute()) {
		std_user_dirs[(uint8_t) StandardUserDir::PICTURES]
				= home_path / "Pictures";
	}
	if (!std_user_dirs[(uint8_t) StandardUserDir::MUSIC].is_absolute()) {
		std_user_dirs[(uint8_t) StandardUserDir::MUSIC]
				= home_path / "Music";
	}
	if (!std_user_dirs[(uint8_t) StandardUserDir::VIDEOS].is_absolute()) {
		std_user_dirs[(uint8_t) StandardUserDir::VIDEOS]
				= home_path / "Videos";
	}
	std_user_dirs_populated = true;
}

Path gen::file::get_user_dir(StandardUserDir dir, const gen::AbstractPath& suffix) {
	Path path = std_user_dirs[(uint8_t) dir];
	if (!suffix.is_empty()) {
		path /= suffix;
	}
	make_directory(path);
	return path;
}
