// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <stdio.h>
#ifdef CGEN_DEP_GTK
#include <gtk/gtk.h>
#endif // CGEN_DEP_GTK
#include <gongen/common/platform/system.hpp>

#ifdef CGEN_DEP_GTK
void mb_callback(GtkDialog* dialog, gint response_id, gpointer user_data) {
	uint32_t* ret = (uint32_t*)user_data;
	if (response_id == GTK_RESPONSE_CANCEL) *ret = 1;
	else if (response_id == GTK_RESPONSE_NO) *ret = 1;
}
#endif // CGEN_DEP_GTK

uint32_t gen::mb::pop_up(const gen::String& title, const gen::String& message, Type type, Buttons buttons)  {
	const char* str_type;
	if (type == INFO) {
		str_type = "INFO";
	} else if (type == WARNING) {
		str_type = "WARNING";
	} else {
		str_type = "ERROR";
	}
	printf("\n---\nMessage box (%s): %s\n%s\n---\n", str_type, title.c_str(), message.c_str());
	
#ifdef CGEN_DEP_GTK
	if (!gtk_init_check(nullptr, nullptr)) {
		puts("Failed to create message box (GTK error).");
		return;
	}
	GtkMessageType gtk_type;
	if (type == INFO) {
		gtk_type = GTK_MESSAGE_INFO;
	} else if (type == WARNING) {
		gtk_type = GTK_MESSAGE_WARNING;
	} else {
		gtk_type = GTK_MESSAGE_ERROR;
	}
	auto buttons_type = GTK_BUTTONS_OK;
	if (buttons == Buttons::YES_NO) buttons_type = GTK_BUTTONS_YES_NO;
	else if (buttons == Buttons::OK_CANCEL) buttons_type = GTK_BUTTONS_OK_CANCEL;
	GtkWidget* dialog = gtk_message_dialog_new(
		nullptr, GTK_DIALOG_MODAL, gtk_type, GTK_BUTTONS_OK,
		"%s: %s\n\n%s", str_type, title.c_str(), message.c_str()
	);
	uint32_t ret = 0;
	g_signal_connect(dialog, "response", mb_callback, &ret);
	puts("Opening message box.");
	gtk_dialog_run(GTK_DIALOG(dialog));
	puts("Closed message box.");
	gtk_widget_destroy(dialog);
	return ret;
#else
	return 0;
#endif // CGEN_DEP_GTK
}
