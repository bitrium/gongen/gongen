// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <signal.h>
#include <stdlib.h>
#ifdef __GLIBC__
	#define GEN_UNIX_BACKTRACE
	#include <execinfo.h>
#endif
#include <gongen/common/platform/system.hpp>
#include <gongen/common/main.hpp>

struct CallstackImpl {
	void** buffer;
	char** symbols;
};

gen::Callstack::Callstack() {
	this->impl = new CallstackImpl { nullptr, nullptr };
}

gen::Callstack::Callstack(size_t max_count, size_t skip) : Callstack() {
	skip++;
	this->fill(max_count, skip);
}

gen::callstack::Callstack::~Callstack() {
	auto* impl = (CallstackImpl*) this->impl;
	gen::free(impl->buffer);
	gen::free(impl->symbols);
	delete impl;
}

gen::callstack::Callstack& gen::Callstack::operator=(const Callstack& other) {
	auto* this_impl = (CallstackImpl*) this->impl;
	auto* other_impl = (CallstackImpl*) other.impl;
	gen::free(this_impl->buffer);
	gen::free(this_impl->symbols);
	if (other.count > 0) {
		this_impl->buffer = (void**) gen::alloc(other.count);
		memcpy(this_impl->buffer, other_impl->buffer, other.count);
		this_impl->symbols = (char**) gen::alloc(other.count);
		memcpy(this_impl->symbols, other_impl->symbols, other.count);
	}
	this->count = other.count;
	return *this;
}

gen::callstack::Callstack& gen::callstack::Callstack::fill(size_t max_count, size_t skip) {
	skip++;
	void** result = (void**) gen::alloc(sizeof(void*) * (max_count + skip));
#ifdef GEN_UNIX_BACKTRACE
	this->count = (size_t) backtrace(result, max_count) - skip;
#else
	this->count = 0;
#endif
	auto* impl = (CallstackImpl*) this->impl;
	impl->buffer = (void**) gen::calloc(sizeof(void*) * this->count);
	memcpy(impl->buffer, result + skip, this->count);
#ifdef GEN_UNIX_BACKTRACE
	impl->symbols = backtrace_symbols(impl->buffer, this->count);
#else
	impl->symbols = nullptr;
#endif
	return *this;
}

gen::callstack::Symbol gen::Callstack::symbol(size_t index) const {
	// auto impl = (CallstackImpl*) this->impl;
	// const char* ptr = impl->symbols[index];
	Symbol s = { // TODO
		.module_name = gen::String(),
		.name = gen::String(),
		.line = 0,
		.filepath = gen::String()
	};
	return s;
}

thread_local struct {
	void* userdata = nullptr;
	gen::feh::handler_ret (*proc)(gen::feh::handler_info*) = nullptr;
} application_feh;

static void feh_handle(int signal);

void gen::feh::set_handler(handler_ret (*proc)(handler_info*), void* userdata, uint32_t mask) {
	for (uint32_t error = 1; mask; error *= 2) {
		if (!(mask & error)) continue;
		mask &= ~error;
		int sig = 0;
		if (error == Mask::SEGFAULT) sig = SIGSEGV;
#ifdef SIGBUS
		else if (error == Mask::DATATYPE_MISALIGNMENT) sig = SIGBUS;
#endif // SIGBUS
		else if (error == Mask::ILLEGAL_INSTRUCTION) sig = SIGILL;
		else if (error == Mask::INT_DIVIDE_BY_ZERO) sig = SIGFPE;
		else if (error == Mask::STACK_OVERFLOW) sig = SIGSEGV;
		else continue;
		struct sigaction act;
		act.sa_handler = feh_handle;
		sigemptyset(&act.sa_mask);
		act.sa_flags = 0;
		sigaction(sig, &act, 0);
	}
	application_feh.userdata = userdata;
	application_feh.proc = proc;
}

extern void gen_init_event();
extern void gen_init_time();
// TODO switch to CGEN_MOD_GSF when it's added in cgen
#ifdef CGEN_DEP_GSF
extern void gen_init_gsf();
#endif

bool initialized = false;
void gen::init() {
	if (initialized) return;
	gen_init_event();
	gen_init_time();
#ifdef CGEN_DEP_GSF
	gen_init_gsf();
#endif
	initialized = true;
}

static void feh_handle(int signal) {
	auto error = (gen::feh::Mask) 0;
	if (signal == SIGSEGV) error = gen::feh::Mask::SEGFAULT;
#ifdef SIGBUS
	else if (signal == SIGBUS) error = gen::feh::Mask::DATATYPE_MISALIGNMENT;
#endif // SIGBUS
	else if (signal == SIGILL) error = gen::feh::Mask::ILLEGAL_INSTRUCTION;
	else if (signal == SIGFPE) error = gen::feh::Mask::INT_DIVIDE_BY_ZERO;
	else if (signal == SIGSEGV) error = gen::feh::Mask::STACK_OVERFLOW;
	else return;
	gen::feh::handler_info info = {
		.userdata = application_feh.userdata,
		// TODO callstack skip count
		.callstack = gen::Callstack(gen::EXCEPTION_MAX_CALLSTACK_DEPTH, 1),
		.exception = error,
		.thread_id = gen::thread::id(),
	};
	gen::feh::handler_ret ret = application_feh.proc(&info);
	if (ret.result == gen::feh::HandlerResult::RETURN) throw ret;
	if (ret.result == gen::feh::HandlerResult::TERMINATE) exit(ret.return_value);
}

int gen::wrap_main(int(*main_function)(int, char**), int argc, char** argv) {
	try {
		return main_function(argc, argv);
	} catch (gen::feh::handler_ret ret) {
		return ret.return_value;
	}
}
