// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/platform/system.hpp>
#include <netinet/ip.h>
#include <unistd.h>
#include <netdb.h>
#include <poll.h>

using namespace gen;
using namespace gen::ip;
using namespace gen::tcp;

Optional<Endpoint> gen::ip::resolve(const String& hostname, Optional<String> port) {
	DynamicArray<Endpoint> found = resolve_all(hostname, port, 1);
	if (found.empty()) return Optional<Endpoint>();
	return Optional<Endpoint>(found[0]);
}
DynamicArray<Endpoint> gen::ip::resolve_all(const String& hostname, Optional<String> port, size_t max) {
	const char* service = nullptr;
	if (port.some()) service = port.get().c_str();

	DynamicArray<Endpoint> endpoints;
	struct addrinfo* addrs;
	int result = getaddrinfo(hostname.c_str(), service, nullptr, &addrs);
	if (result != 0) {
		return endpoints;
	}
	while (addrs) {
		if (max != 0 && endpoints.size() >= max) break;
		if (addrs->ai_family != AF_INET && addrs->ai_family != AF_INET6) continue;
		Endpoint* endpoint = endpoints.emplace();
		endpoint->from_network(addrs->ai_addr);
		addrs = addrs->ai_next;
	}
	freeaddrinfo(addrs);
	return endpoints;
}


struct SocketImpl {
	int fd = -1;
	Endpoint endpoint;
	DynamicArray<Socket> remote_sockets; // sockets created with accept(false)
};
Socket::Socket(void* impl) {
	this->impl = impl;
}
Socket::Socket() {
	this->impl = new SocketImpl();
}
Socket::Socket(const Endpoint& endpoint, bool is_server) : Socket() {
	this->open(endpoint, is_server);
}
Socket::Socket(const String& hostname, bool is_server)
		: Socket(resolve(hostname).get(), is_server) {
}
Socket::~Socket() {
	this->close();
	delete (SocketImpl*) this->impl;
}

bool Socket::is_open() {
	if (!this->impl) return false;
	auto* impl = (SocketImpl*) this->impl;
	if (impl->fd == -1) return false;
	return true;
}
void Socket::open(const ip::Endpoint& endpoint, bool is_server) {
	if (!this->impl) this->impl = new SocketImpl();
	if (this->is_open()) throw RuntimeException("Socket is already open.");
	auto* impl = (SocketImpl*) this->impl;
	this->impl = impl;

	impl->fd = -1;
	int fd = socket(endpoint.address.v6 ? AF_INET6 : AF_INET, SOCK_STREAM, 0);
	if (fd == -1) throw RuntimeException::from_errno("Failed to create a socket", endpoint.to_string().c_str());

	sockaddr addr;
	socklen_t addr_len = endpoint.to_network(&addr);
	if (is_server) {
		if (bind(fd, &addr, addr_len) != 0) {
			::close(fd);
			throw RuntimeException::from_errno("Failed to bind '%s'", endpoint.to_string().c_str());
		}
		if (listen(fd, SOMAXCONN) != 0) {
			::close(fd);
			throw RuntimeException::from_errno("Failed to listen on '%s'", endpoint.to_string().c_str());
		}
	} else {
		if (connect(fd, &addr, addr_len) != 0) {
			::close(fd);
			throw RuntimeException::from_errno("Failed to connect to '%s'", endpoint.to_string().c_str());
		}
	}

	if ((is_server ? getsockname(fd, &addr, &addr_len) : getpeername(fd, &addr, &addr_len)) != 0) {
		throw RuntimeException::from_errno("Could not retrieve name of a created socket");
	}

	impl->endpoint.from_network(&addr);
	impl->fd = fd;
}
void Socket::close() {
	if (!this->is_open()) return;
	auto* impl = (SocketImpl*) this->impl;
	::close(impl->fd);
	impl->fd = -1;
}
void Socket::reopen() {
	if (!this->impl) throw RuntimeException("Socket was not open.");
	auto* impl = (SocketImpl*) this->impl;
	if (!impl->endpoint.is_filled()) throw RuntimeException("Socket was not open.");
	this->open(impl->endpoint, false);
}

Socket* Socket::accept(bool take_ownership) {
	auto* impl = (SocketImpl*) this->impl;

	struct sockaddr addr = {};
	socklen_t addr_len;
	int fd = ::accept(impl->fd, &addr, &addr_len);
	if (fd == -1) return nullptr;
	if (getpeername(fd, &addr, &addr_len) != 0) return nullptr;

	SocketImpl* remote = new SocketImpl();
	remote->fd = fd;
	try {
		remote->endpoint.from_network(&addr);
	} catch (const ArgumentException& exception) {
		// unsupported address family
		return nullptr;
	}
	return take_ownership ? impl->remote_sockets.emplace(remote) : new Socket(remote);
}

size_t Socket::read(void* data, size_t size) {
	auto* impl = (SocketImpl*) this->impl;

	ssize_t result = recv(impl->fd, data, size, 0);
	if (result < 0) {
		throw IOException::from_errno("Could not read from socket");
	}
	return result;
}
size_t Socket::write(const void* data, size_t size) {
	auto* impl = (SocketImpl*) this->impl;

	ssize_t result = send(impl->fd, data, size, MSG_NOSIGNAL);
	if (result < 0) {
		throw IOException::from_errno("Could not write to socket");
	}
	return result;
}

bool Socket::check(uint32_t timeout) {
	Buffer<Socket*> buff;
	buff.add(this);
	return !check_sockets_ids(buff, timeout).empty();
}
Endpoint Socket::endpoint() {
	auto* impl = (SocketImpl*) this->impl;
	return impl->endpoint;
}


Buffer<size_t> gen::tcp::check_sockets_ids(const Buffer<Socket*>& sockets, uint32_t timeout) {
	gen::Buffer<struct pollfd> fds(sockets.size());
	for (Socket* socket : sockets) {
		pollfd fd = {};
		if (socket) {
			auto* impl = (SocketImpl*) socket->impl;
			fd.fd = impl->fd;
		} else {
			fd.fd = -1; // ignore
		}
		fd.events = POLLIN;
		fd.revents = 0;
		fds.add(fd);
	}
	int result = poll(fds.ptr(), fds.size(), timeout);
	if (result == 0) return Buffer<size_t>();
	if (result == -1) {
		throw RuntimeException::from_errno("Could not poll socket events");
	}
	Buffer<size_t> results(fds.size());
	for (size_t i = 0; i < fds.size(); i++) {
		short events = fds.get(i).revents;
		if (events & POLLIN) {
			results.add(i);
		}
	}
	return results;
}
