// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/platform/system.hpp>
#include <gongen/common/main.hpp>
#include <gongen/common/logger.hpp>
#ifdef GEN_PLATFORM_WIN32
#include "win32/winapi.hpp"
#include <WinSock2.h>
#include <ws2tcpip.h>
#define s6_addr16 u.Word
#else
#include <atomic>
#include <netinet/ip.h>
#endif
#include <malloc.h>
#undef ERROR

using namespace gen;

void* gen::alloc(size_t size) {
	return ::malloc(size);
}
void* gen::calloc(size_t size) {
	return ::calloc(size, 1);
}
void gen::free(void* ptr) {
	return ::free(ptr);
}
void* gen::realloc(void* ptr, size_t size) {
	return ::realloc(ptr, size);
}
void* gen::crealloc(void* ptr, size_t old_size, size_t size) {
	ptr = ::realloc(ptr, size);
	if (ptr && size > old_size) {
		::memset(((char*)ptr) + old_size, 0, size - old_size);
	}
	return ptr;
}

#ifdef CGEN_DEP_LTALLOC
#include <ltalloc.h>

void* gen::alloc(size_t size) {
	return ltmalloc(size);
}
void* gen::calloc(size_t size) {
	void* mem = ltmalloc(size);
	if (mem) memset(mem, 0, size);
	return mem;
}
void gen::free(void* ptr) {
	if (ptr) ltfree(ptr);
}
void* gen::realloc(void* ptr, size_t size) {
	return ltrealloc(ptr, size);
}
void* gen::crealloc(void* ptr, size_t old_size, size_t size) {
	void* mem = ltrealloc(ptr, size);
	if (ptr && size > old_size) {
		memset(((char*)ptr) + old_size, 0, size - old_size);
	}
	return mem;
}
#endif

String gen::ip::Address::to_string() const {
	if (!this->v6) {
		const uint8_t* data = (const uint8_t*) &this->addr.v4;
		return String::createf("%hhu.%hhu.%hhu.%hhu", data[3], data[2], data[1], data[0]);
	} else {
		// TODO shortened format?
		const uint16_t* data = this->addr.v6;
		return String::createf(
			"%hu:%hu:%hu:%hu:%hu:%hu:%hu:%hu",
			data[0], data[1], data[2], data[3],
			data[4], data[5], data[6], data[7]
		);
	}
}

String gen::ip::Endpoint::to_string() const {
	const char* format = !this->address.v6 ? "%s:%hu" : "[%s]:%hu";
	return String::createf(format, this->address.to_string().c_str(), this->port);
}
unsigned int gen::ip::Endpoint::to_network(struct sockaddr* out) const {
	if (!this->address.v6) {
		struct sockaddr_in* addr = (struct sockaddr_in*) out;
		addr->sin_family = AF_INET;
		addr->sin_addr.s_addr = htonl(this->address.addr.v4);
		addr->sin_port = htons(this->port);
		return sizeof(*addr);
	} else {
		struct sockaddr_in6* addr = (struct sockaddr_in6*) out;
		addr->sin6_family = AF_INET6;
		memcpy(addr->sin6_addr.s6_addr16, this->address.addr.v6, 8 * sizeof(uint16_t));
		addr->sin6_port = htons(this->port);
		return sizeof(*addr);
	}
}

void gen::ip::Endpoint::from_network(const struct sockaddr* net) {
	if (net->sa_family == AF_INET) {
		this->address.v6 = false;
		const struct sockaddr_in* addr = (const struct sockaddr_in*) net;
		this->address.addr.v4 = ntohl(addr->sin_addr.s_addr);
		this->port = ntohs(addr->sin_port);
	} else if (net->sa_family == AF_INET6) {
		this->address.v6 = true;
		const struct sockaddr_in6* addr = (const struct sockaddr_in6*) net;
		// TODO store address in host byte order
		memcpy(this->address.addr.v6, addr->sin6_addr.s6_addr16, 8 * sizeof(uint16_t));
		this->port = ntohs(addr->sin6_port);
	} else throw ArgumentException("Unsupported network address family.");
}

GEN_API Optional<gen::ip::Endpoint> gen::ip::resolve(const String& endpoint) {
	size_t colon_count = endpoint.count(":");
	if (colon_count == 0) {
		return resolve(endpoint, Optional<String>());
	} else if (colon_count == 1) {
		DynamicArray<String> split = endpoint.split(":");
		return resolve(split[0], split[1]);
	} else {
		// most likely IPv6
		if (endpoint.starts_with("[")) {
			// with port
			if (endpoint.count("]") != 1) throw ArgumentException("Unknown address format.");
			DynamicArray<String> split = endpoint.split("]");
			return resolve(split[0].segment(1), split[1]);
		} else {
			// without port
			return resolve(endpoint, Optional<String>());
		}
	}
}
GEN_API DynamicArray<gen::ip::Endpoint> gen::ip::resolve_all(const String& endpoint, size_t max) {
	size_t colon_count = endpoint.count(":");
	if (colon_count == 0) {
		return resolve_all(endpoint, Optional<String>(), max);
	} else if (colon_count == 1) {
		DynamicArray<String> split = endpoint.split(":");
		return resolve_all(split[0], split[1], max);
	} else {
		// most likely IPv6
		if (endpoint.starts_with("[")) {
			// with port
			if (endpoint.count("]") != 1) throw ArgumentException("Unknown address format.");
			DynamicArray<String> split = endpoint.split("]");
			return resolve_all(split[0].segment(1), split[1], max);
		} else {
			// without port
			return resolve_all(endpoint, Optional<String>(), max);
		}
	}
}

Buffer<tcp::Socket*> tcp::check_sockets(const Buffer<tcp::Socket*>& sockets, uint32_t timeout) {
	Buffer<tcp::Socket*> results;
	for (size_t idx : tcp::check_sockets_ids(sockets, timeout)) {
		results.add(sockets.get(idx));
	}
	return results;
}

void gen::console::writef(const char* fmt, ...) {
	va_list va;
	va_start(va, fmt);
	gen::String str;
	str.vappendf(fmt, va);
	gen::console::write(str.c_str());
	va_end(va);
}

#ifndef GEN_PLATFORM_WIN32
struct WaitObjectImpl {
	std::atomic<uint32_t> state;
	std::atomic<uint32_t> waiting_thread_count;
	bool auto_reset;
};
gen::thread::WaitObject::WaitObject(bool auto_reset) {
	auto* impl = new WaitObjectImpl();
	impl->state = 0;
	impl->waiting_thread_count = 0;
	impl->auto_reset = auto_reset;
	this->impl = impl;
}
gen::thread::WaitObject::~WaitObject() {
	delete (WaitObjectImpl*) this->impl;
}
void gen::thread::WaitObject::wait() {
	auto* impl = (WaitObjectImpl*) this->impl;
	impl->waiting_thread_count++;
	impl->state.wait(0);
	impl->waiting_thread_count--;
	if (impl->auto_reset && !impl->waiting_thread_count) reset();
}
void gen::thread::WaitObject::signal() {
	auto* impl = (WaitObjectImpl*) this->impl;
	impl->state = 1;
	impl->state.notify_all();
}
void gen::thread::WaitObject::reset() {
	auto* impl = (WaitObjectImpl*) this->impl;
	impl->state = 0;
}
bool gen::thread::WaitObject::state() {
	auto* impl = (WaitObjectImpl*) this->impl;
	return (impl->state != 0);
}
#endif

gen::String gen::Callstack::to_string(bool code_info, size_t main_fc_addr) const {
	gen::String out = "Callstack:\n";
	for (uint32_t i = 0; i < this->count; i++) {
		gen::callstack::Symbol sym = this->symbol(i);
		if (code_info) out.appendf("%s, %s: %zu, %s\n", sym.module_name.c_str(), sym.name.c_str(), sym.line, sym.filepath.c_str());
		else out.appendf("%s, %s, %zu\n", sym.module_name.c_str(), sym.name.c_str(), sym.line);
		if (sym.base_addr == main_fc_addr) break;
	}
	return out;
}


gen::feh::handler_ret simple_feh_callback(gen::feh::handler_info* info) {
	gen::String err_str = "Fatal error occured";
	if (info->exception & gen::feh::SEGFAULT) err_str += " (Segfault)!";
	else if (info->exception & gen::feh::DATATYPE_MISALIGNMENT) err_str += " (Datatype Misaligment)!";
	else if (info->exception & gen::feh::ILLEGAL_INSTRUCTION) err_str += " (Illegal Instruction)!";
	else if (info->exception & gen::feh::INT_DIVIDE_BY_ZERO) err_str += " (Int Divide By Zero)!";
	else if (info->exception & gen::feh::STACK_OVERFLOW) err_str += " (Stack Overflow)!";
	else err_str += "!";
	gen::log::error(err_str);
	gen::log::error(info->callstack.to_string(true, (size_t) info->userdata));
	gen::mb::pop_up(err_str, info->callstack.to_string(true, (size_t) info->userdata), gen::mb::ERROR);
	return gen::feh::handler_ret { .result = gen::feh::HandlerResult::TERMINATE, .return_value = 1 };
}
void gen::simple_feh() {
	gen::feh::set_handler(simple_feh_callback, (void*) gen::Callstack().fill(1).symbol(0).base_addr);
}


gen::String gen::file::read_txt(const Path& path) {
	gen::File file(path);
	gen::String out;
	file.read(out.reserve(file.size()), file.size());
	return out;
}
gen::String gen::file::read_txt(const char* path) {
	return read_txt(Path(path));
}
