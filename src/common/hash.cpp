// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/hash.hpp>
#include <xxhash.h>

uint64_t gen::hash(const void* data, size_t size, uint64_t seed) {
	return XXH3_64bits_withSeed(data, size, seed);
}
uint64_t gen::hash(const void* data, size_t size) {
	return XXH3_64bits(data, size);
}
uint64_t gen::hash_combine(uint64_t lhs, uint64_t rhs) {
	lhs ^= rhs + 0x517cc1b727220a95 + (lhs << 6) + (lhs >> 2);
	return lhs;
}
uint64_t gen::hash_combine_n(size_t count, ...) {
	if (count == 0) return 0;
	va_list args;
	va_start(args, count);
	uint64_t result = va_arg(args, uint64_t);
	for (size_t i = 1; i < count; i++) {
		hash_combine_ref(result, va_arg(args, uint64_t));
	}
	va_end(args);
	return result;
}
