// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/error.hpp>
#include <gongen/common/path.hpp>

using namespace gen;

AbstractPath::AbstractPath(bool root) {
	this->is_abs = root;
}

AbstractPath::AbstractPath(const AbstractPath& copied) : segments(copied.segments) {
	this->is_abs = copied.is_abs;
	this->str = copied.str;
}

AbstractPath::AbstractPath(const String& str) {
	gen::DynamicArray<String> str_segments = str.split("/\\");
	// absolute if starts with '/'
	this->is_abs = (
		(str_segments.begin().get()->empty() && str_segments.size() > 1) ||
		str_segments.begin().get()->contains(":")
	);

	// remove empty segments
	this->segments = gen::DynamicArray<String>(str_segments.size());
	for (auto& segment : str_segments) {
		if (!segment.empty()) {
			this->segments.add(segment);
		}
	}
	// this->segments.shrink_to_fit();
}

AbstractPath::AbstractPath(const char* c_str) : AbstractPath(String(c_str)) {
}

AbstractPath::~AbstractPath() {
}

UniversalPath AbstractPath::as_universal() {
	return UniversalPath(*this);
}

gen::String AbstractPath::string() const {
	return this->str;
}

const char* AbstractPath::c_str() const {
	return this->str.c_str();
}

const DynamicArray<String>& AbstractPath::get_segments() const {
	return this->segments;
}

gen::String AbstractPath::name() const {
	if (this->segments.empty()) return "";
	return this->segments[-1];
}

gen::String AbstractPath::extension() const {
	if (this->segments.empty()) return "";
	auto split = this->segments[-1].split(".");
	if (split.size() <= 1) return gen::String();
	return split[-1];
}

gen::String AbstractPath::suffix() const {
	if (this->segments.empty()) return "";
	auto split = this->segments[-1].split(".");
	if (split.size() <= 1) return gen::String();
	return this->segments[-1].segmented(split[0].size() + 1);
}

gen::String AbstractPath::stem() const {
	if (this->segments.empty()) return "";
	const char* name = this->segments[-1].c_str();
	const char* ext = strchr(name, '.');
	if (!ext) return gen::String(name);
	auto str = gen::String();
	str.append(name, ext - name);
	return str;
}

bool AbstractPath::is_absolute() const {
	return this->is_abs;
}

bool AbstractPath::is_empty() const {
	if (this->segments.size() < 1) return true;
	return false;
}

void AbstractPath::clear() {
	this->segments.clear();
	this->update_string();
}

AbstractPath& AbstractPath::to_absolute() {
	this->is_abs = true;
	this->update_string();
	return *this;
}

AbstractPath& AbstractPath::to_relative() {
	this->is_abs = false;
	this->update_string();
	return *this;
}

AbstractPath& AbstractPath::to_parent(size_t ancestor) {
	this->segments.delete_last(ancestor);
	this->update_string();
	return *this;
}

AbstractPath& AbstractPath::to_child(const String& name) {
	this->segments.add(name);
	this->update_string();
	return *this;
}

AbstractPath& AbstractPath::to_child(const AbstractPath& relative) {
	if (relative.is_abs) {
		throw ArgumentException("Cannot get child from an absolute path.");
	}
	this->segments += relative.segments;
	this->update_string();
	return *this;
}

AbstractPath& AbstractPath::operator/=(const String& child) {
	return this->to_child(child);
}

AbstractPath& AbstractPath::operator/=(const AbstractPath& relative) {
	return this->to_child(relative);
}

void BasicPath::update_string() {
	this->str.clear();
	if (this->is_absolute()) {
		this->str.append("/");
	}
	if (this->segments.empty()) {
		return;
	}
	for (PtrIterator<gen::String> it = this->segments.begin();
			it != this->segments.end() - 1; it++) {
		this->str.append(*it.get());
		this->str.append("/");
	}
	this->str.append(this->segments.last());
}
