// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/containers/lockless_queue.hpp>

#include <gongen/common/platform/system.hpp>
#include <lfqueue.h>

void* lfqueue_malloc(void* userdata, size_t size) { return gen::alloc(size); }
void lfqueue_free(void* userdata, void* ptr) { return gen::free(ptr); }

gen::LocklessQueue::LocklessQueue() {
	impl = gen::talloc<lfqueue_t>();
	lfqueue_init_mf((lfqueue_t*)impl, nullptr, lfqueue_malloc, lfqueue_free);
	wait = new thread::WaitObject(true);
}
gen::LocklessQueue::~LocklessQueue() {
	if (impl) {
		lfqueue_destroy((lfqueue_t*)impl);
		free(impl);
	}
	if (wait) delete (thread::WaitObject*) wait;
	impl = nullptr;
	wait = nullptr;
}

bool gen::LocklessQueue::enqueue(void* item) noexcept {
	if (lfqueue_enq((lfqueue_t*)impl, item) == -1) return false;
	((thread::WaitObject*) wait)->signal();
	return true;
}
void* gen::LocklessQueue::dequeue() noexcept {
	return lfqueue_deq((lfqueue_t*)impl);
}
void* gen::LocklessQueue::dequeue_wait() noexcept {
	void* result = nullptr;
	while (!(result = lfqueue_deq((lfqueue_t*)impl))) {
		((thread::WaitObject*) wait)->wait();
	}
	return result;
}
size_t gen::LocklessQueue::size() const noexcept {
	return lfqueue_size((lfqueue_t*)impl);
}
