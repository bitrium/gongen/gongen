// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/error.hpp>
#include <gongen/common/containers/values.hpp>
#include <gongen/common/containers/abstract.hpp>

void gen::internal::no_impl_error(const char* msg) {
	throw NoImplException(msg);
}
void gen::internal::optional_error(const char* msg) {
	throw UnsetOptionalException(msg);
}
