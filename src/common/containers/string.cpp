// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/containers/string.hpp>

#include <stdio.h>
#include <gongen/common/error.hpp>

char* gen::String::reserve(size_t size_to_reserve) {
	size_t osize = size();
	size_t nsize = (osize + size_to_reserve);
	size_t ncapacity = nsize + 1;
	if (ncapacity > capacity()) {
		if (is_short()) {
			// Short->Long Transition
			char sso_content[gen::String::SSO_CAPACITY];
			if (osize != 0) memcpy(sso_content, get_ptr(), osize);
			capacity_ = ncapacity;
			ptr_ = (char*)gen::alloc(ncapacity);
			size_ |= 1;
			set_size(nsize);
			if (osize != 0) memcpy(get_ptr(), sso_content, osize);
		}
		else {
			ptr_ = (char*)gen::realloc(ptr_, ncapacity);
			capacity_ = ncapacity;
			set_size(nsize);
		}
	} else set_size(nsize);
	return get_ptr(osize);
}

#define STB_SPRINTF_IMPLEMENTATION
#include <stb_sprintf.h>

size_t gen::String::vappendf(const char* fmt, va_list va) {
	va_list va_clone;
	va_copy(va_clone, va);
	size_t fsize = vsnprintf(nullptr, 0, fmt, va);
	char* ptr = this->reserve(fsize);
	vsnprintf(ptr, this->capacity(), fmt, va_clone);
	return size();
}

gen::DynamicArray<gen::String> gen::String::split(const char* separators) const {
	gen::DynamicArray<gen::String> out;
	size_t next_split_pos = 0;
	size_t current_pos = 0;
	size_t separators_len = GEN_SAFE_STRLEN(separators);
	for (auto it = begin(); it != end(); it++) {
		for (uint32_t i = 0; i < separators_len; i++) {
			if (*it == separators[i]) {
				out.add(gen::String(get_ptr(next_split_pos), current_pos - next_split_pos));
				next_split_pos = (current_pos + 1);
			}
		}
		current_pos++;
	}
	if (next_split_pos != current_pos) {
		out.add(get_ptr(next_split_pos));
	}
	return out;
}
bool gen::String::contains(const char* characters) const {
	size_t characters_len = GEN_SAFE_STRLEN(characters);
	if (characters_len == 0) return false;
	for (auto& c : *this) {
		for (uint32_t i = 0; i < characters_len; i++) {
			if (c == characters[i]) return true;
		}
	}
	return false;
}
size_t gen::String::count(const char* characters) const {
	size_t out = 0;
	size_t characters_len = GEN_SAFE_STRLEN(characters);
	if (characters_len == 0) return 0;
	for (auto& c : *this) {
		for (uint32_t i = 0; i < characters_len; i++) {
			if (c == characters[i]) out++;
		}
	}
	return out;
}
gen::Optional<size_t> gen::String::find(const char* matches) const {
	size_t matches_len = GEN_SAFE_STRLEN(matches);
	if (matches_len == 0) return gen::Optional<size_t>();
	size_t pos = 0;
	for (auto& c : *this) {
		for (uint32_t i = 0; i < matches_len; i++) {
			if (c == matches[i]) return gen::Optional<size_t>(pos);
		}
		pos++;
	}
	return gen::Optional<size_t>();
}

bool gen::String::starts_with(const String& prefix) const {
	if (this->size() < prefix.size()) return false;
	for (size_t i = 0; i < prefix.size(); i++) {
		if (this->get(i) != prefix.get(i)) return false;
	}
	return true;
}
bool gen::String::ends_with(const gen::String& suffix) const {
	if (this->size() < suffix.size()) return false;
	for (size_t i = 0; i < suffix.size(); i++) {
		if (this->get(this->size() - i - 1) != suffix.get(suffix.size() - i - 1)) return false;
	}
	return true;
}

gen::String& gen::String::segment(size_t start, size_t end) {
	if (end == SIZE_MAX) {
		if (start == 0) return *this;
		end = this->size();
	}
	if (start > 0) {
		memmove(this->data(), this->data(start), end - start);
	}
	this->set_size(end - start);
	return *this;
}
gen::String gen::String::segmented(size_t start, size_t end) {
	gen::String copy = *this;
	copy.segment(start, end);
	return copy;
}

gen::String& gen::String::strip_l() {
	if (this->size() == 0) return *this;
	size_t i = 0;
	while (isspace(this->get(i))) {
		i++;
	}
	this->segment(i);
	return *this;
}
gen::String& gen::String::strip_r() {
	if (this->size() == 0) return *this;
	size_t i = this->size() - 1;
	while (isspace(this->get(i))) {
		i--;
	}
	this->segment(0, i + 1);
	return *this;
}
gen::String& gen::String::strip() {
	if (this->size() == 0) return *this;
	size_t i = 0;
	while (isspace(this->get(i))) {
		i++;
	}
	size_t j = this->size() - 1;
	while (isspace(this->get(j))) {
		j--;
	}
	this->segment(i, j + 1);
	return *this;
}
gen::String gen::String::stripped_l() {
	gen::String copy = *this;
	copy.strip_l();
	return copy;
}
gen::String gen::String::stripped_r() {
	gen::String copy = *this;
	copy.strip_r();
	return copy;
}
gen::String gen::String::stripped() {
	gen::String copy = *this;
	copy.strip();
	return copy;
}
