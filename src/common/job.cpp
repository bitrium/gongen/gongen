// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/common/job.hpp>
#include <gongen/common/containers/lockless_queue.hpp>
#include <atomic>

void* job_proc(void* userdata);

struct JobData {
	std::atomic<bool> handle_exist = true;
	std::atomic<size_t> ref_counter = 2;
	gen::thread::WaitObject wait_object;

	void* (*proc)(void*);
	void* (*proc_multi)(void*, size_t);
	void (*done_callback)(void*);

	size_t start_count;
	size_t count;
	void* usedata;
	void* ret = nullptr;
	gen::Buffer<void*> rets;
	gen::Exception* exception;
	gen::Buffer<gen::Exception*> exceptions;

	~JobData() {
		for (auto& e : exceptions) {
			if (e) delete e;
		}
	}
};

struct JobThreadData {
	bool running = true;
	std::atomic<size_t> ref_counter;
	gen::thread::WaitObject wait_object;
	gen::TypedLocklessQueue<JobData> job_queue;
	gen::feh::handler_ret(*feh_handler)(gen::feh::handler_info*) = nullptr;
	uint32_t feh_mask;
	void* feh_userdata;

	void push_job(JobData* data) {
		job_queue.enqueue(data);
		wait_object.signal();
	}
};

gen::job::System::System(uint32_t thread_count, gen::feh::handler_ret(*feh_handler)(gen::feh::handler_info*), void* feh_userdata, uint32_t feh_mask) {
	if (!thread_count) thread_count = gen::os::thread_count();

	thread_data = new JobThreadData();
	JobThreadData* data = (JobThreadData*)thread_data;
	data->ref_counter = thread_count + 2;
	data->feh_handler = feh_handler;
	data->feh_userdata = feh_userdata;
	data->feh_mask = feh_mask;

	for (size_t i = 0; i < thread_count; i++) {
		threads.emplace(job_proc, thread_data, gen::String::createf("Worker%zu", i).c_str());
	}
}
gen::job::System::~System() {
	JobThreadData* data = (JobThreadData*)thread_data;
	data->running = false;
	data->wait_object.signal();
	if (!--data->ref_counter) delete data;
}
gen::job::Handle gen::job::System::add_job(void* (*proc)(void*), void* userdata) {
	JobThreadData* data = (JobThreadData*)thread_data;
	JobData* job_data = new JobData();
	job_data->proc = proc;
	job_data->usedata = userdata;
	data->push_job(job_data);
	return gen::job::Handle(job_data);
}
gen::job::Handle gen::job::System::add_jobs(void* (*proc)(void*, size_t), void* userdata, size_t count, void (*done_callback)(void*)) {
	JobThreadData* data = (JobThreadData*)thread_data;
	JobData* job_data = new JobData();
	job_data->proc_multi = proc;
	job_data->done_callback = done_callback;
	job_data->count = count;
	job_data->ref_counter = count + 1;
	job_data->start_count = count;
	job_data->usedata = userdata;
	job_data->rets.reserve(count);
	job_data->exceptions.reserve(count);
	data->push_job(job_data);
	return gen::job::Handle(job_data);
}
bool gen::job::System::busy() {
	JobThreadData* data = (JobThreadData*)thread_data;
	if (data->job_queue.size()) return true;
	else return data->wait_object.state();
}
gen::job::Handle::~Handle() {
	JobData* data = (JobData*)impl;
	data->handle_exist = false;
	if (!--data->ref_counter) delete data;
}
bool gen::job::Handle::is_done() {
	JobData* data = (JobData*)impl;
	return data->wait_object.state();
}
void gen::job::Handle::join() {
	JobData* data = (JobData*)impl;
	return data->wait_object.wait();
}
void* gen::job::Handle::get_return_value(size_t index) {
	JobData* data = (JobData*)impl;
	if (data->proc_multi) return data->rets.get(index);
	else return data->ret;
}
void gen::job::Handle::rethrow_exception(size_t index) {
	JobData* data = (JobData*)impl;
	if (data->proc_multi) {
		gen::Exception* excep = data->exceptions.get(index);
		if (excep) excep->rethrow();
	}
	else {
		if (data->exception) data->exception->rethrow();
	}
}
size_t gen::job::Handle::count() {
	JobData* data = (JobData*)impl;
	if (data->proc_multi) return data->count;
	else return 1;
}

gen::feh::handler_ret job_feh_handler(gen::feh::handler_info* info) {
	JobThreadData* data = (JobThreadData*)info->userdata;
	info->userdata = data->feh_userdata;
	gen::feh::handler_ret(*feh_handler)(gen::feh::handler_info*) = data->feh_handler;
	if (--data->ref_counter) delete data;
	if (feh_handler) return feh_handler(info);
	else {
		return { gen::feh::PASS, -1 };
	}
}

void* job_proc(void* userdata) {
	JobThreadData* data = (JobThreadData*)userdata;
	gen::feh::set_handler(job_feh_handler, userdata, data->feh_mask);
	while (data->running) {
		data->wait_object.wait();
		while (JobData* job = data->job_queue.dequeue()) {
			if (job->proc_multi) {
				size_t index = (job->start_count - job->count);
				job->count--; if (job->count) data->push_job(job);
				try {
					job->rets[index] = job->proc_multi(job->usedata, index);
				}
				catch (gen::Exception& e) {
					job->wait_object.signal();
					job->exceptions[index] = e.copy();
				}
				job->ref_counter--;
				if (job->handle_exist) {
					if (job->ref_counter == 1) {
						job->wait_object.signal();
						if (job->done_callback) job->done_callback(job->usedata);
					}
				}
				else {
					if (!job->ref_counter) {
						if (job->done_callback) job->done_callback(job->usedata);
					}
				}
				if (!job->ref_counter) delete job;
			}
			else {
				try {
					job->ret = job->proc(job->usedata);
				}
				catch (gen::Exception& e) {
					job->wait_object.signal();
					job->exception = e.copy();
				}
				job->wait_object.signal();
				job->ref_counter--;
				if (!job->ref_counter) delete job;
			}
		}
		if (data->wait_object.state()) {
			data->wait_object.reset();
		}
	}
	if (!--data->ref_counter) delete data;
	return nullptr;
}
