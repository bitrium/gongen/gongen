#include <gongen/loaders/font.hpp>
#include <gongen/common/compression.hpp>
#include <stb_rect_pack.h>
#include <stdlib.h>

#define UV_COORD(v) ((v / (float)texture_size) * UINT16_MAX)

bool gen::loaders::Font::open(gen::SeekableDataSource* source, bool own_source) {
	close();
	this->source = source;
	this->own_source = own_source;

	if (source->read(&header, sizeof(header)) != sizeof(header)) {
		throw gen::loaders::FormatException("Unexcepted file end");
	}
	if (header.magic != internal::GFNT_MAGIC) {
		throw gen::loaders::FormatException("Not a GenFont file");
	}
	if (header.version != internal::GFNT_VERSION) {
		throw gen::loaders::FormatException("Unsupported GenFont version");
	}
	gen::ByteBuffer compressed_data, data;
	if (source->read(compressed_data.reserve(header.compressed_size), header.compressed_size) != header.compressed_size) {
		throw gen::loaders::FormatException("Unexcepted file end");
	}
	if (!gen::compressor_zlib()->decompress(data, header.uncompressed_size, compressed_data.ptr(), header.compressed_size)) {
		throw gen::loaders::FormatException("Decompression failed");
	}
	uint8_t* sdf_data = (uint8_t*)(data.ptr() + (header.glyph_count * sizeof(internal::gfnt_glyph) + (header.kerning_pairs * sizeof(internal::gfnt_kern))));
	internal::gfnt_glyph* glyph = (internal::gfnt_glyph*)data.ptr();
	Glyph* glyph2 = glyphs.reserve(header.glyph_count);
	for (size_t i = 0; i < header.glyph_count; i++) {
		glyph2->codepoint = glyph->codepoint;
		glyph2->bearing = gen::vec2f(glyph->bearingX / 256.0f, glyph->bearingX / 256.0f);
		glyph2->size = gen::vec2f(glyph->width / 256.0f, glyph->height / 256.0f);
		glyph2->advance = glyph->advance / 256.0f;
		glyph2->bitmap_pos = gen::vec2u8(header.margin, header.margin);
		glyph2->bitmap_size = gen::vec2u8(glyph->bitmapW, glyph->bitmapH);
		glyph2->data_offset = this->data.size_bytes();
		this->data.add(sdf_data + glyph->offset, glyph->size);
		glyph++;
		glyph2++;
	}
	internal::gfnt_kern* kern = (internal::gfnt_kern*)glyph;
	KernPair* kern2 = kern_pairs.reserve(header.kerning_pairs);
	for (size_t i = 0; i < header.kerning_pairs; i++) {
		kern2->left = kern->left;
		kern2->right = kern->right;
		kern2->value = kern->x / 256.0f;
		kern++;
		kern2++;
	}
	this->opened = true;
	return true;
}
void gen::loaders::Font::close() {
	if (this->opened) {
		if (own_source) delete source;
		source = nullptr;
		own_source = false;
		kern_pairs.clear();
		glyphs.clear();
		data.clear();
	}
	this->opened = false;
}
gen::gl::Texture gen::loaders::Font::build(gen::gl::Device* device, uint16_t texture_size) {
	gen::gl::TextureDesc desc;
	desc.format = gen::gl::Format::RGBA8;
	desc.size = gen::vec3z(texture_size, texture_size, 1);
	gen::gl::Texture tx = device->create_texture(desc);

	gen::Buffer<stbrp_node> nodes;
	stbrp_context ctx;
	stbrp_init_target(&ctx, texture_size, texture_size, nodes.reserve(texture_size), texture_size);
	gen::Buffer<stbrp_rect> rects;
	stbrp_rect* rect = rects.reserve(glyphs.size());
	for (size_t i = 0; i < glyphs.size(); i++) {
		auto* glyph = glyphs.ptr(i);
		rect->was_packed = 0;
		rect->x = 0;
		rect->y = 0;
		rect->w = glyph->bitmap_size.x;
		rect->h = glyph->bitmap_size.y;
		rect++;
	}
	stbrp_pack_rects(&ctx, rects.ptr(), rects.size());
	rect = rects.ptr();
	for (size_t i = 0; i < glyphs.size(); i++) {
		auto* glyph = glyphs.ptr(i);
		glyph->uv_pos = gen::vec2u16(UV_COORD(rect->x), UV_COORD(rect->y));
		glyph->uv_size = gen::vec2u16(UV_COORD(rect->w), UV_COORD(rect->h));
		device->update_texture(tx, 0, gen::vec3z(rect->x, rect->y, 0), gen::vec3z(rect->w, rect->h, 1), data.ptr(glyph->data_offset));
		rect++;
	}
	return tx;
}

int glyph_bseach_compare(void const* a, void const* b) {
	gen::loaders::Font::Glyph* aa = (gen::loaders::Font::Glyph*)a;
	gen::loaders::Font::Glyph* bb = (gen::loaders::Font::Glyph*)b;
	return (aa->codepoint - bb->codepoint);
}

gen::loaders::Font::Glyph* gen::loaders::Font::get_glyph(uint32_t codepoint) {
	gen::loaders::Font::Glyph g;
	g.codepoint = codepoint;
	return (gen::loaders::Font::Glyph*)bsearch(&g, glyphs.ptr(), glyphs.size(), sizeof(gen::loaders::Font::Glyph), glyph_bseach_compare);
}
