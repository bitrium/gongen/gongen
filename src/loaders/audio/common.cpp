// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/loaders/audio.hpp>
#include <gongen/common/error.hpp>

#define FORMAT_OPUS 1

#ifdef GEN_MOD_LOADER_OPUS
size_t opus_test(gen::SeekableDataSource* source);
void* opus_open(gen::SeekableDataSource* source, uint32_t& bitrate, uint32_t& sample_rate, uint64_t& sample_count, uint32_t& channels);
void opus_close(void* impl);
uint64_t opus_get_samples(void* impl, int16_t* out, uint64_t samples, bool stereo);
uint64_t opus_get_samples_float(void* impl, float* out, uint64_t samples, bool stereo);
void opus_seek(void* impl, uint64_t pos);
uint64_t opus_tell(void* impl);
#endif

bool gen::loaders::Audio::open(gen::SeekableDataSource* source, bool own_source) {
	close();
	this->source = source;
	this->own_source = own_source;

	size_t cursor_pos = source->cursor();
	size_t test_result = 0;
#ifdef GEN_MOD_LOADER_OPUS
	if ((test_result = opus_test(source))) {
		if (test_result == gen::RW_ERROR) {
			source->seek(cursor_pos);
			throw gen::loaders::OpenException("Data Source Read Failed");
		}
		else {
			impl = opus_open(source, sample_rate, bitrate, frame_count, channels);
			if (impl) format = FORMAT_OPUS;
		}
	}
#endif
	if(!test_result) {
		source->seek(cursor_pos);
		throw gen::loaders::FormatException("Unsupported Audio Format");
	}
	return true;
}
void gen::loaders::Audio::close() {
	if (this->own_source) delete source;
#ifdef GEN_MOD_LOADER_OPUS
	if (format == FORMAT_OPUS) opus_close(impl);
#endif
	impl = nullptr;
	format = 0;
}
uint64_t gen::loaders::Audio::get_samples(int16_t* out, uint64_t samples, bool stereo) {
#ifdef GEN_MOD_LOADER_OPUS
	if (format == FORMAT_OPUS) return opus_get_samples(impl, out, samples, stereo);
#endif
	return 0;
}
uint64_t gen::loaders::Audio::get_samples_float(float* out, uint64_t samples, bool stereo) {
#ifdef GEN_MOD_LOADER_OPUS
	if (format == FORMAT_OPUS) return opus_get_samples_float(impl, out, samples, stereo);
#endif
	return 0;
}
void gen::loaders::Audio::seek(uint64_t sample_pos) {
#ifdef GEN_MOD_LOADER_OPUS
	if (format == FORMAT_OPUS) opus_seek(impl, sample_pos);
#endif
}
uint64_t gen::loaders::Audio::tell() {
#ifdef GEN_MOD_LOADER_OPUS
	if (format == FORMAT_OPUS) return opus_tell(impl);
#endif
	return 0;
}
