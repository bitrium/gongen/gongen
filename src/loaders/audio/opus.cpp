// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <opusfile.h>
#include <gongen/common/error.hpp>
#include <gongen/loaders/audio.hpp>

gen::String opus_error_string(int err) {
	if (err == OP_FALSE) return "A request did not succeed";
	else if (err == OP_EOF) return "End of file";
	else if (err == OP_HOLE) return "Missing or corrupted page";
	else if (err == OP_EREAD) return "Read/Seek/Tell failed";
	else if (err == OP_EFAULT) return "Unexpected NULL pointer";
	else if (err == OP_EIMPL) return "Unsupported feature";
	else if (err == OP_EINVAL) return "Invalid parameter";
	else if (err == OP_ENOTFORMAT) return "Not opus format";
	else if (err == OP_EBADHEADER) return "Invalid header";
	else if (err == OP_EVERSION) return "Invalid version";
	else if (err == OP_ENOTAUDIO) return "Not audio";
	else if (err == OP_EBADPACKET) return "Invalid packet";
	else if (err == OP_EBADLINK) return "Failed to find data";
	else if (err == OP_ENOSEEK) return "Seeking required but unsupported";
	else if (err == OP_EBADTIMESTAMP) return "Invalid time stamp";
	return "Unknown error";
}

int cb_opus_read(void* stream, uint8_t* out, int nbytes) {
	gen::SeekableDataSource* source = (gen::SeekableDataSource*)stream;
	size_t bytes_read = source->read(out, nbytes);
	if (bytes_read == gen::RW_ERROR) return -1;
	else return bytes_read;
}
int cb_opus_seek(void* stream, int64_t offset, int type) {
	gen::SeekableDataSource* source = (gen::SeekableDataSource*)stream;
	if (type == SEEK_SET) {
		if (source->seek(offset, gen::Seek::SET) == gen::RW_ERROR) return -1;
	}
	else if (type == SEEK_CUR) {
		if (source->seek(offset, gen::Seek::CUR) == gen::RW_ERROR) return -1;
	}
	else if (type == SEEK_END) {
		if (source->seek(offset, gen::Seek::END) == gen::RW_ERROR) return -1;
	}
	return 0;
}
opus_int64 cb_opus_tell(void* stream) {
	gen::SeekableDataSource* source = (gen::SeekableDataSource*)stream;
	return source->cursor();
}

size_t opus_test(gen::SeekableDataSource* source) {
	source->seek(0);

	uint8_t initial_data[512];
	size_t bytes_read = source->read(initial_data, 512);
	if (bytes_read == gen::RW_ERROR) return gen::RW_ERROR;

	if (op_test(nullptr, initial_data, bytes_read) == 0) return 1;
	else return 0;
}
void* opus_open(gen::SeekableDataSource* source, uint32_t& bitrate, uint32_t& sample_rate, uint64_t& frame_count, uint32_t& channels) {
	OpusFileCallbacks file_callbacks;
	file_callbacks.read = cb_opus_read;
	file_callbacks.seek = cb_opus_seek;
	file_callbacks.tell = cb_opus_tell;
	file_callbacks.close = nullptr;

	source->seek(0);
	uint8_t initial_data[512];
	size_t bytes_read = source->read(initial_data, 512);

	int error;
	OggOpusFile* opus_file = op_open_callbacks(source, &file_callbacks, initial_data, bytes_read, &error);
	if (error) {
		if (opus_file) op_free(opus_file);
		throw gen::loaders::OpenException(opus_error_string(error));
	}
	bitrate = op_bitrate(opus_file, 0);
	channels = op_channel_count(opus_file, 0);
	sample_rate = 48000;
	frame_count = 0;
	size_t link_count = op_link_count(opus_file);
	for (size_t i = 0; i < link_count; i++) {
		frame_count += op_pcm_total(opus_file, i);
		if (op_channel_count(opus_file, i) != channels) {
			throw gen::loaders::OpenException("Opus files with variable number of channels are not supported");
		}
	}
	return opus_file;
}
void opus_close(void* impl) {
	if (impl) {
		op_free((OggOpusFile*)impl);
	}
}
uint64_t opus_get_samples(void* impl, int16_t* out, uint64_t frame_count, bool stereo) {
	int ret = 0;
	if(stereo)ret = op_read_stereo((OggOpusFile*)impl, out, frame_count * 2);
	else ret = op_read((OggOpusFile*)impl, out, frame_count, nullptr);
	if (ret < 0) throw gen::loaders::OpenException(opus_error_string(ret));
	return ret;
}
uint64_t opus_get_samples_float(void* impl, float* out, uint64_t frame_count, bool stereo) {
	int ret = 0;
	if (stereo)ret = op_read_float_stereo((OggOpusFile*)impl, out, frame_count * 2);
	else ret = op_read_float((OggOpusFile*)impl, out, frame_count, nullptr);
	if (ret < 0) throw gen::loaders::OpenException(opus_error_string(ret));
	return ret;
}
void opus_seek(void* impl, uint64_t pos) {
	op_pcm_seek((OggOpusFile*)impl, pos);
}
uint64_t opus_tell(void* impl) {
	return op_pcm_tell((OggOpusFile*)impl);
}
