// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/loaders/image.hpp>
#include <gongen/common/error.hpp>

#ifdef GEN_MOD_LOADER_STB
bool stb_check(gen::SeekableDataSource* source);
bool stb_load(gen::SeekableDataSource* source, void** data, uint32_t &width, uint32_t& height, uint32_t& comp, uint32_t& format, bool& compressed, uint32_t req_comp);
#endif

bool gen::loaders::Image::open(gen::SeekableDataSource* source, bool own_source, uint32_t req_comp) {
	close();
	this->source = source;
	this->own_source = own_source;

	size_t test_result = 0;
#ifdef GEN_MOD_LOADER_STB
	if ((test_result = stb_check(source))) {
		if (test_result == gen::RW_ERROR) {
			throw gen::loaders::OpenException("Data Source Read Failed");
		}
		else {
			return stb_load(source, &data, width, height, comp, format, compressed, req_comp);
		}
	}
#endif
	
	if (!test_result) {
		throw gen::loaders::FormatException("Unsupported Image Format");
	}

	return true;
}
void gen::loaders::Image::close() {
	if (own_source) delete source;
	if (data) gen::free(data);
	own_source = false;
	source = nullptr;
	data = nullptr;
	width = 0;
	height = 0;
	comp = 0;
	format = 0;
	compressed = false;
}
size_t gen::loaders::Image::get_data_size() const {
	if(!compressed) return (width * height * comp);
	else {
		//TODO: FIX ME!
		return 0;
	}
}
