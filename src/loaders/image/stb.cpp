// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/loaders/image.hpp>
#include <gongen/common/error.hpp>
#define STB_IMAGE_IMPLEMENTATION
#define STBI_ONLY_PNG
#define STBI_ONLY_PSD
#define STBI_NO_LINEAR
#define STBI_NO_HDR
#define STBI_NO_STDIO
#define STBI_MALLOC gen::alloc
#define STBI_FREE gen::free
#define STBI_REALLOC gen::realloc
#include <stb_image.h>

int cb_stb_read(void* user, char* data, int size) {
	gen::SeekableDataSource* source = (gen::SeekableDataSource*)user;
	try { return source->read(data, size); }
	catch (...) { return 0; }
}
void cb_stb_skip(void* user, int n) {
	gen::SeekableDataSource* source = (gen::SeekableDataSource*)user;
	try { source->seek(n, gen::Seek::CUR); }
	catch (...) { return; }
}
int cb_stb_eof(void* user) {
	gen::SeekableDataSource* source = (gen::SeekableDataSource*)user;
	try { return !(source->cursor() == source->size()); }
	catch (...) { return 1; }
}

stbi_io_callbacks stb_get_callbacks() {
	stbi_io_callbacks cb;
	cb.read = cb_stb_read;
	cb.skip = cb_stb_skip;
	cb.eof = cb_stb_eof;
	return cb;
}
bool stb_check(gen::SeekableDataSource* source) {
	source->seek(0);
	stbi_io_callbacks cb = stb_get_callbacks();

	int x, y, comp;
	x = y = comp = 0;
	stbi_info_from_callbacks(&cb, source, &x, &y, &comp);
	return (x != 0);
}
bool stb_load(gen::SeekableDataSource* source, void** data, uint32_t& width, uint32_t& height, uint32_t& comp, uint32_t& format, bool& compressed, uint32_t req_comp) {
	source->seek(0);
	stbi_io_callbacks cb = stb_get_callbacks();

	int x, y, stb_comp;
	x = y = stb_comp = 0;
	*data = stbi_load_from_callbacks(&cb, source, &x, &y, &stb_comp, req_comp);
	if (*data) {
		if ((req_comp != 0) && (req_comp != stb_comp)) comp = req_comp;
		else comp = stb_comp;
		width = x;
		height = y;
		compressed = false;
		format = comp;
		return true;
	}
	else {
		throw gen::loaders::OpenException(stbi_failure_reason());
	}
}
