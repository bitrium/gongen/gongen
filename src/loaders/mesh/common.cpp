// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/loaders/mesh.hpp>

#ifdef GEN_MOD_LOADER_PLY
extern bool load_ply(gen::loaders::Mesh& mesh, gen::SeekableDataSource* source);
#endif
#ifdef GEN_MOD_LOADER_GLTF
extern bool load_gltf(gen::loaders::Mesh& mesh, gen::SeekableDataSource* source);
#endif

bool gen::loaders::Mesh::open(gen::SeekableDataSource* source, bool own_source) {
	uint8_t magic[4];
	if (source->read(&magic, 4) != 4) return false;
#ifdef GEN_MOD_LOADER_GLTF
	if (*((uint32_t*)magic) == 0x46546C67) {
		return load_gltf(*this, source);
	}
#endif
#ifdef GEN_MOD_LOADER_PLY
	if ((magic[0] == 'p') && (magic[1] == 'l') && (magic[2] == 'y')) {
		return load_ply(*this, source);
	}
#endif
	throw gen::loaders::FormatException("Unsupported mesh format");
}
void gen::loaders::Mesh::close() {
	if (own_source) delete source;
	own_source = false;
	data.clear();
	materials.clear();
	meshes.clear();
	nodes.clear();
	scenes.clear();
	main_scene = nullptr;
	source = nullptr;
	opened = false;
}
