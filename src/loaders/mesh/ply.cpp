// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/loaders/mesh.hpp>

struct PlyProperty {
	bool is_float;
	gen::String name;
	size_t offset;
};

bool load_ply(gen::loaders::Mesh& mesh, gen::SeekableDataSource* source) {
	gen::ByteBuffer data;
	source->seek(0);
	size_t data_size = source->size();
	if (!source->read(data.reserve(data_size), data_size)) return false;
	gen::DynamicArray<gen::String> strings;
	char* cstr_data_start = (char*)data.ptr();
	char* cstr_data = cstr_data_start;
	size_t str_start = 0;
	while (true) {
		char c = *(cstr_data++);
		if ((c == ' ') || (c == '\n')) {
			gen::String str = gen::String(&cstr_data_start[str_start], ((cstr_data - cstr_data_start) - str_start) - 1);
			if (str == "comment") {
				while (c != '\n') {
					c = *(cstr_data++);
				}
				str_start = (cstr_data - cstr_data_start);
			}
			else {
				strings.add(str);
				str_start = (cstr_data - cstr_data_start);
				if (str == "end_header") break;
			}
		}
	}
	gen::DynamicArray<PlyProperty> properties;

	size_t vertex_count = 0;
	size_t face_count = 0;
	gen::String prev_str;
	for (auto& s : strings) {
		if ((s == "binary_big_endian") || (s == "ascii")) {
			throw gen::loaders::OpenException("Unsupported ply format(only binary_little_endian is supported)");
		}
		else if (prev_str == "vertex") {
			vertex_count = atoi(s.c_str());
		}
		else if (prev_str == "face") {
			face_count = atoi(s.c_str());
		}
		else if ((prev_str == "float") || (prev_str == "uchar")) {
			PlyProperty property;
			property.is_float = (prev_str == "float");
			property.name = s;
			properties.add(property);
		}
		prev_str = s;
	}
	strings.clear();

	size_t off = 0;
	size_t pos_off = SIZE_MAX;
	size_t normal_off = SIZE_MAX;
	size_t color_off = SIZE_MAX;
	size_t uv_off = SIZE_MAX;
	size_t in_pos_off = SIZE_MAX;
	size_t in_normal_off = SIZE_MAX;
	size_t in_color_off = SIZE_MAX;
	size_t in_uv_off = SIZE_MAX;
	for (auto& p : properties) {
		if (p.name == "x") in_pos_off = off;
		else if (p.name == "nx") in_normal_off = off;
		else if (p.name == "red") in_color_off = off;
		else if (p.name == "s") in_uv_off = off;

		if (p.is_float) off += 4;
		else off += 1;
	}
	off--;

	gen::loaders::Mesh::Material mat;
	gen::loaders::Mesh::Primitive prim;
	gen::loaders::Mesh::_Mesh _mesh;
	gen::loaders::Mesh::Node node;
	gen::loaders::Mesh::Scene scene;

	gen::loaders::Mesh::Attribute color_attr = { "COLOR", gen::loaders::Mesh::VEC4_UINT8, true, (uint32_t)vertex_count };
	gen::loaders::Mesh::Attribute pos_attr = { "POSITION", gen::loaders::Mesh::VEC3_FLOAT, true, (uint32_t)vertex_count };
	gen::loaders::Mesh::Attribute normal_attr = { "NORMAL", gen::loaders::Mesh::VEC3_FLOAT, true, (uint32_t)vertex_count };
	gen::loaders::Mesh::Attribute uv_attr = { "TEX_COORD", gen::loaders::Mesh::VEC2_FLOAT, true, (uint32_t)vertex_count };
	if (in_pos_off != SIZE_MAX) {
		pos_off = mesh.data.size();
		mesh.data.reserve(sizeof(gen::vec3f) * vertex_count);
	}
	if (in_normal_off != SIZE_MAX) {
		normal_off = mesh.data.size();
		mesh.data.reserve(sizeof(gen::vec3f) * vertex_count);
	}
	if (in_color_off != SIZE_MAX) {
		color_off = mesh.data.size();
		mesh.data.reserve(sizeof(gen::Color8) * vertex_count);
	}
	if (in_uv_off != SIZE_MAX) {
		uv_off = mesh.data.size();
		mesh.data.reserve(sizeof(gen::vec2f) * vertex_count);
	}
	size_t index_off = mesh.data.size();
	mesh.data.reserve(sizeof(uint32_t) * face_count * 3);

	mat.name = "Material(ply)";
	mat.base_color = gen::Color::greyscale(1.0f);
	mat.roughness = 1.0f;
	mat.metalness = 1.0f;
	prim.material = mesh.materials.add(mat);
	_mesh.name = "Mesh(ply)";
	if (pos_off != SIZE_MAX) {
		pos_attr.f32 = (float*)mesh.data.ptr(pos_off);
		prim.attributes.add(pos_attr);
	}
	if (normal_off != SIZE_MAX) {
		normal_attr.f32 = (float*)mesh.data.ptr(normal_off);
		prim.attributes.add(normal_attr);
	}
	if (color_off != SIZE_MAX) {
		color_attr.f32 = (float*)mesh.data.ptr(color_off);
		prim.attributes.add(color_attr);
	}
	if (uv_off != SIZE_MAX) {
		uv_attr.f32 = (float*)mesh.data.ptr(uv_off);
		prim.attributes.add(uv_attr);
	}
	gen::loaders::Mesh::Attribute index_attr = { "INDEX", gen::loaders::Mesh::UINT32, true, (uint32_t)(face_count * 3) };
	index_attr.f32 = (float*)mesh.data.ptr(index_off);
	prim.attributes.add(index_attr);
	_mesh.primitives.add(prim);
	node.mesh = mesh.meshes.add(_mesh);
	node.name = "Node(ply)";
	scene.nodes.add(mesh.nodes.add(node));
	scene.name = "Scene(ply)";
	mesh.main_scene = mesh.scenes.add(scene);

	uint8_t* data_ptr = (uint8_t*)cstr_data;
	for (size_t i = 0; i < vertex_count; i++) {
		if (pos_off != SIZE_MAX) pos_attr.f32_3[i] = *((gen::vec3f*)(data_ptr + in_pos_off));
		if (normal_off != SIZE_MAX) normal_attr.f32_3[i] = *((gen::vec3f*)(data_ptr + in_normal_off));
		if (color_off != SIZE_MAX) color_attr.color8[i] = *((gen::Color8*)(data_ptr + in_color_off));
		if (uv_off != SIZE_MAX) uv_attr.f32_2[i] = *((gen::vec2f*)(data_ptr + in_uv_off));
		data_ptr += (off);
	}
	for (size_t i = 0; i < face_count; i++) {
		uint8_t polygon_type = *data_ptr;
		if (polygon_type != 3) {
			throw gen::loaders::OpenException("Unsupported ply polygon type(only triangles are supported)");
		}
		gen::vec3u* triangle = (gen::vec3u*)(data_ptr + 1);
		index_attr.u32[(i * 3) + 0] = triangle->x;
		index_attr.u32[(i * 3) + 1] = triangle->y;
		index_attr.u32[(i * 3) + 2] = triangle->z;
		data_ptr += (1 + (sizeof(uint32_t) * 3));
	}
	data.clear();

	return true;
}
