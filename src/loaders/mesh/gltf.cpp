// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/loaders/mesh.hpp>
#include <cJSON.h>

#pragma pack(push, 1)
struct gltf_header {
	uint32_t magic;
	uint32_t version;
	uint32_t length;
};
struct gltf_chunk {
	uint32_t length;
	uint32_t type;
	uint8_t data[];
};
#pragma pack(pop)

struct gltfBuffer {
	uint32_t offset;
	uint32_t length;
};
struct gltfBufferView {
	gltfBuffer* buffer;
	uint32_t length;
	uint32_t offset;
};
struct gltfAccesor {
	gltfBufferView* view;
	uint32_t count;
	uint32_t component_type;
	bool normalized;
	gen::String type;
};
void attrib_from_accesor(gen::loaders::Mesh::Attribute* attrib, gltfAccesor* accessor, const gen::String& name, uint8_t* data) {
	uint32_t data_offset = (accessor->view->offset + accessor->view->buffer->offset);
	data = (data + data_offset);
	attrib->name = name;
	attrib->normalized = accessor->normalized;
	attrib->count = accessor->count;
	attrib->f32 = (float*)data;
	attrib->type = gen::loaders::Mesh::UNKNOWN;
	if (accessor->type == "SCALAR") {
		if (accessor->component_type == 5126) attrib->type = gen::loaders::Mesh::FLOAT;
		else if (accessor->component_type == 5121) attrib->type = gen::loaders::Mesh::UINT8;
		else if (accessor->component_type == 5123) attrib->type = gen::loaders::Mesh::UINT16;
		else if (accessor->component_type == 5125) attrib->type = gen::loaders::Mesh::UINT32;
	}
	else if (accessor->type == "VEC2") {
		if (accessor->component_type == 5126) attrib->type = gen::loaders::Mesh::VEC2_FLOAT;
		else if (accessor->component_type == 5121) attrib->type = gen::loaders::Mesh::VEC2_UINT8;
		else if (accessor->component_type == 5123) attrib->type = gen::loaders::Mesh::VEC2_UINT16;
		else if (accessor->component_type == 5125) attrib->type = gen::loaders::Mesh::VEC2_UINT32;
	}
	else if (accessor->type == "VEC3") {
		if (accessor->component_type == 5126) attrib->type = gen::loaders::Mesh::VEC3_FLOAT;
		else if (accessor->component_type == 5121) attrib->type = gen::loaders::Mesh::VEC3_UINT8;
		else if (accessor->component_type == 5123) attrib->type = gen::loaders::Mesh::VEC3_UINT16;
		else if (accessor->component_type == 5125) attrib->type = gen::loaders::Mesh::VEC3_UINT32;
	}
	else if (accessor->type == "VEC4") {
		if (accessor->component_type == 5126) attrib->type = gen::loaders::Mesh::VEC4_FLOAT;
		else if (accessor->component_type == 5121) attrib->type = gen::loaders::Mesh::VEC4_UINT8;
		else if (accessor->component_type == 5123) attrib->type = gen::loaders::Mesh::VEC4_UINT16;
		else if (accessor->component_type == 5125) attrib->type = gen::loaders::Mesh::VEC4_UINT32;
	}
}

bool load_gltf(gen::loaders::Mesh& mesh, gen::SeekableDataSource* source) {
	source->seek(0);
	gltf_header header;
	if (!source->read(&header, sizeof(gltf_header))) return false;
	if (header.version != 2) {
		throw gen::loaders::OpenException("Unsupported gltf version");
	}
	gen::ByteBuffer data;
	data.reserve(header.length);
	if (!source->read(data.ptr(), data.size())) return false;

	gen::Buffer<gltf_chunk*> chunks;
	size_t pos = 0;
	while (pos < (header.length - sizeof(gltf_header))) {
		gltf_chunk* chunk = (gltf_chunk*)data.ptr(pos);
		chunks.add(chunk);
		pos += (chunk->length + sizeof(gltf_chunk));
	}
	gltf_chunk* json_chunk = nullptr;
	gltf_chunk* bin_chunk = nullptr;
	for (auto& c : chunks) {
		if (c->type == 0x4E4F534A) json_chunk = c;
		else if (c->type == 0x004E4942) bin_chunk = c;
	}
	if (!json_chunk) {
		throw gen::loaders::OpenException("No gltf json chunk");
	}
	if (!bin_chunk) {
		throw gen::loaders::OpenException("No gltf bin chunk");
	}
	mesh.data.add(bin_chunk->data, bin_chunk->length);
	cJSON* json = cJSON_ParseWithLength((const char*)json_chunk->data, json_chunk->length);
	if (!json) {
		throw gen::loaders::OpenException("Can't parse json");
	}

	uint32_t last_buffer_offset = 0;
	gen::Buffer<gltfBuffer> gltf_buffers;
	if (cJSON* buffers = cJSON_GetObjectItem(json, "buffers")) {
		for (size_t i = 0; i < cJSON_GetArraySize(buffers); i++) {
			cJSON* buffer = cJSON_GetArrayItem(buffers, i);
			uint32_t length = cJSON_GetNumberValue(cJSON_GetObjectItem(buffer, "byteLength"));
			gltfBuffer buf = { last_buffer_offset, length };
			gltf_buffers.add(buf);
			last_buffer_offset += length;
		}
	}
	gen::Buffer<gltfBufferView> gltf_buffers_views;
	if (cJSON* views = cJSON_GetObjectItem(json, "bufferViews")) {
		for (size_t i = 0; i < cJSON_GetArraySize(views); i++) {
			cJSON* view = cJSON_GetArrayItem(views, i);
			gltfBufferView buf_view;
			buf_view.buffer = gltf_buffers.ptr((uint32_t)cJSON_GetNumberValue(cJSON_GetObjectItem(view, "buffer")));
			buf_view.length = (uint32_t)cJSON_GetNumberValue(cJSON_GetObjectItem(view, "byteLength"));
			buf_view.offset = (uint32_t)cJSON_GetNumberValue(cJSON_GetObjectItem(view, "byteOffset"));
			gltf_buffers_views.add(buf_view);
		}
	}
	gen::DynamicArray<gltfAccesor> gltf_accesors;
	if (cJSON* accessors = cJSON_GetObjectItem(json, "accessors")) {
		for (size_t i = 0; i < cJSON_GetArraySize(accessors); i++) {
			cJSON* accessor = cJSON_GetArrayItem(accessors, i);
			gltfAccesor acces;
			acces.view = gltf_buffers_views.ptr((uint32_t)cJSON_GetNumberValue(cJSON_GetObjectItem(accessor, "bufferView")));
			acces.component_type = (uint32_t)cJSON_GetNumberValue(cJSON_GetObjectItem(accessor, "componentType"));
			acces.count = (uint32_t)cJSON_GetNumberValue(cJSON_GetObjectItem(accessor, "count"));
			acces.normalized = cJSON_IsTrue(cJSON_GetObjectItem(accessor, "normalized"));
			acces.type = cJSON_GetStringValue(cJSON_GetObjectItem(accessor, "type"));
			gltf_accesors.add(acces);
		}
	}
	if (cJSON* materials = cJSON_GetObjectItem(json, "materials")) {
		for (size_t i = 0; i < cJSON_GetArraySize(materials); i++) {
			cJSON* material = cJSON_GetArrayItem(materials, i);
			gen::loaders::Mesh::Material new_material;
			new_material.name = cJSON_GetStringValue(cJSON_GetObjectItem(material, "name"));
			if (cJSON* pbr = cJSON_GetObjectItem(material, "pbrMetallicRoughness")) {
				if (cJSON* pbr_color = cJSON_GetObjectItem(pbr, "baseColorFactor")) {
					float color[4];
					for (size_t i = 0; i < cJSON_GetArraySize(pbr_color); i++) {
						if (i < 4) color[i] = cJSON_GetNumberValue(cJSON_GetArrayItem(pbr_color, i));
					}
					new_material.base_color = gen::Color::rgba(color[0], color[1], color[2], color[3]);
				}
				if (cJSON_HasObjectItem(pbr, "metallicFactor")) new_material.metalness = cJSON_GetNumberValue(cJSON_GetObjectItem(pbr, "metallicFactor"));
				else new_material.metalness = 1.0f;
				if (cJSON_HasObjectItem(pbr, "roughnessFactor")) new_material.roughness = cJSON_GetNumberValue(cJSON_GetObjectItem(pbr, "roughnessFactor"));
				else new_material.roughness = 1.0f;
			}
			if (cJSON* extras = cJSON_GetObjectItem(material, "extras")) {
				for (size_t i = 0; i < cJSON_GetArraySize(extras); i++) {
					cJSON* extra = cJSON_GetArrayItem(extras, i);
					if (cJSON_IsNumber(extra)) {
						new_material.properties.add({ gen::String(extra->string), cJSON_GetNumberValue(extra) });
					}
					else if (cJSON_IsString(extra)) {
						new_material.properties_str.add({ gen::String(extra->string), cJSON_GetStringValue(extra) });
					}
				}
			}
			mesh.materials.add(new_material);
		}
	}
	if (cJSON* meshes = cJSON_GetObjectItem(json, "meshes")) {
		for (size_t i = 0; i < cJSON_GetArraySize(meshes); i++) {
			cJSON* _mesh = cJSON_GetArrayItem(meshes, i);

			gen::loaders::Mesh::_Mesh new_mesh;
			new_mesh.name = cJSON_GetStringValue(cJSON_GetObjectItem(_mesh, "name"));
			if (cJSON* primitives = cJSON_GetObjectItem(_mesh, "primitives")) {
				for (size_t i = 0; i < cJSON_GetArraySize(primitives); i++) {
					gen::loaders::Mesh::Primitive new_primitive;
					cJSON* primitive = cJSON_GetArrayItem(primitives, i);
					double indices = cJSON_GetNumberValue(cJSON_GetObjectItem(primitive, "indices"));
					double material = cJSON_GetNumberValue(cJSON_GetObjectItem(primitive, "material"));
					double mode = cJSON_GetNumberValue(cJSON_GetObjectItem(primitive, "mode"));
					if (cJSON* attributes = cJSON_GetObjectItem(primitive, "attributes")) {
						cJSON* attrib = attributes->child;
						while (attrib) {
							gen::loaders::Mesh::Attribute attr;
							attrib_from_accesor(new_primitive.attributes.add(attr), gltf_accesors.ptr((uint32_t)cJSON_GetNumberValue(attrib)), attrib->string, mesh.data.ptr());
							attrib = attrib->next;
						}
					}
					if (!isnan(indices)) {
						gen::loaders::Mesh::Attribute attr;
						uint32_t indices_id = (uint32_t)indices;
						auto* accesor = gltf_accesors.ptr(indices_id);
						attrib_from_accesor(new_primitive.attributes.add(attr), accesor, "INDEX", mesh.data.ptr());
					}

					if (!isnan(material)) {
						uint32_t material_id = (uint32_t)material;
						new_primitive.material = mesh.materials.ptr(material_id);
					}
					else new_primitive.material = nullptr;

					if (!isnan(mode)) {
						uint32_t imode = (uint32_t)mode;
						if (imode != 4) {
							throw gen::loaders::OpenException("Non triangle-list meshes are not supported");
						}
					}
					new_mesh.primitives.add(new_primitive);
				}
			}
			mesh.meshes.add(new_mesh);
		}
	}
	if (cJSON* nodes = cJSON_GetObjectItem(json, "nodes")) {
		for (size_t i = 0; i < cJSON_GetArraySize(nodes); i++) {
			cJSON* node = cJSON_GetArrayItem(nodes, i);

			gen::loaders::Mesh::Node mesh_node;
			mesh_node.tranform_matrix = gen::mat4f();
			mesh_node.name = cJSON_GetStringValue(cJSON_GetObjectItem(node, "name"));
			if (cJSON* children = cJSON_GetObjectItem(node, "children")) {
				for (size_t i = 0; i < cJSON_GetArraySize(children); i++) {
					cJSON* child = cJSON_GetArrayItem(children, i);
					double child_id = cJSON_GetNumberValue(child);
					if (!isnan(child_id)) {
						uint32_t child_id2 = (uint32_t)child_id;
						mesh_node.children.add((gen::loaders::Mesh::Node*)(intptr_t)child_id2);
					}
				}
			}
			if (cJSON* translations = cJSON_GetObjectItem(node, "translation")) {
				float translate_values[3];
				for (size_t i = 0; i < cJSON_GetArraySize(translations); i++) {
					if (i < 3) translate_values[i] = cJSON_GetNumberValue(cJSON_GetArrayItem(translations, i));
				}
				mesh_node.tranform_matrix = mesh_node.tranform_matrix * gen::mat4f::translate(translate_values[0], translate_values[1], translate_values[2]);
			}
			if (cJSON* rotations = cJSON_GetObjectItem(node, "rotation")) {
				float rotation_values[4];
				for (size_t i = 0; i < cJSON_GetArraySize(rotations); i++) {
					if(i < 4) rotation_values[i] = cJSON_GetNumberValue(cJSON_GetArrayItem(rotations, i));
				}
				mesh_node.tranform_matrix = mesh_node.tranform_matrix * gen::mat4f::rotate(rotation_values[0], rotation_values[1], rotation_values[2], rotation_values[3]);
			}
			if (cJSON* scales = cJSON_GetObjectItem(node, "scale")) {
				float scale_values[3];
				for (size_t i = 0; i < cJSON_GetArraySize(scales); i++) {
					if(i < 3) scale_values[i] = cJSON_GetNumberValue(cJSON_GetArrayItem(scales, i));
				}
				mesh_node.tranform_matrix = mesh_node.tranform_matrix * gen::mat4f::scale(scale_values[0], scale_values[1], scale_values[2]);
			}
			if (cJSON* matrix = cJSON_GetObjectItem(node, "matrix")) {
				float matrix_values[16];
				for (size_t i = 0; i < cJSON_GetArraySize(matrix); i++) {
					if (i < 16) matrix_values[i] = cJSON_GetNumberValue(cJSON_GetArrayItem(matrix, i));
				}
				memcpy(mesh_node.tranform_matrix.data, matrix_values, sizeof(float) * 16);
			}
			double mesh_id = cJSON_GetNumberValue(cJSON_GetObjectItem(node, "mesh"));
			if (!isnan(mesh_id)) {
				mesh_node.mesh = mesh.meshes.ptr((uint32_t)mesh_id);
			}
			double skin_id = cJSON_GetNumberValue(cJSON_GetObjectItem(node, "skin"));
			if (!isnan(skin_id)) {
				mesh_node.skin = (gen::loaders::Mesh::Skin*)(intptr_t)(mesh_id + 1);
			}
			else mesh_node.skin = 0;
			mesh.nodes.add(mesh_node);
		}
	}
	if (cJSON* skins = cJSON_GetObjectItem(json, "skins")) {
		for (size_t i = 0; i < cJSON_GetArraySize(skins); i++) {
			gen::loaders::Mesh::Skin new_skin;
			cJSON* skin = cJSON_GetArrayItem(skins, i);
			new_skin.name = cJSON_GetStringValue(cJSON_GetObjectItem(skin, "name"));
			double mat_id = (double)cJSON_GetNumberValue(cJSON_GetObjectItem(skin, "inverseBindMatrices"));
			if (!isnan(mat_id)) {
				auto* access = gltf_accesors.ptr((uint32_t)mat_id);
				new_skin.mat_count = access->count;
				new_skin.mat = (gen::mat4f*)mesh.data.ptr(access->view->offset + access->view->buffer->offset);
			}
			if (cJSON* joints = cJSON_GetObjectItem(skin, "joints")) {
				for (size_t i = 0; i < cJSON_GetArraySize(joints); i++) {
					uint32_t joint_id = (uint32_t)cJSON_GetNumberValue(cJSON_GetArrayItem(joints, i));
					new_skin.joints.add(mesh.nodes.ptr(joint_id));
				}
			}
			double skeleton_id = (double)cJSON_GetNumberValue(cJSON_GetObjectItem(skin, "skeleton"));
			if (!isnan(skeleton_id)) {
				new_skin.root = mesh.nodes.ptr((uint32_t)skeleton_id);
			}
			mesh.skins.add(new_skin);
		}
	}
	if (cJSON* animations = cJSON_GetObjectItem(json, "animations")) {
		for (size_t i = 0; i < cJSON_GetArraySize(animations); i++) {
			gen::loaders::Mesh::Animation* new_anim = mesh.animations.add(gen::loaders::Mesh::Animation());
			cJSON* animation = cJSON_GetArrayItem(animations, i);
			new_anim->name = cJSON_GetStringValue(cJSON_GetObjectItem(animation, "name"));
			if (cJSON* samplers = cJSON_GetObjectItem(animation, "samplers")) {
				for (size_t i = 0; i < cJSON_GetArraySize(samplers); i++) {
					cJSON* sampler = cJSON_GetArrayItem(samplers, i);
					gen::loaders::Mesh::AnimationSampler new_sampler;
					attrib_from_accesor(&new_sampler.input, gltf_accesors.ptr((uint32_t)cJSON_GetNumberValue(cJSON_GetObjectItem(sampler, "input"))), "ANIM_INPUT", mesh.data.ptr());
					attrib_from_accesor(&new_sampler.output, gltf_accesors.ptr((uint32_t)cJSON_GetNumberValue(cJSON_GetObjectItem(sampler, "output"))), "ANIM_OUTPUT", mesh.data.ptr());
					gen::String interpolation = cJSON_GetStringValue(cJSON_GetObjectItem(sampler, "interpolation"));
					new_sampler.interpolation = gen::loaders::Mesh::Interpolation::LINEAR;
					if (interpolation == "STEP") new_sampler.interpolation = gen::loaders::Mesh::Interpolation::STEP;
					else if (interpolation == "LINEAR") new_sampler.interpolation = gen::loaders::Mesh::Interpolation::LINEAR;
					new_anim->samplers.add(new_sampler);
				}
			}
			if (cJSON* channels = cJSON_GetObjectItem(animation, "channels")) {
				for (size_t i = 0; i < cJSON_GetArraySize(channels); i++) {
					cJSON* channel = cJSON_GetArrayItem(channels, i);
					gen::loaders::Mesh::AnimationChannel new_channel;
					new_channel.sampler = new_anim->samplers.ptr((uint32_t)cJSON_GetNumberValue(cJSON_GetObjectItem(channel, "sampler")));
					if (cJSON* target = cJSON_GetObjectItem(channel, "target")) {
						new_channel.target = mesh.nodes.ptr((uint32_t)cJSON_GetNumberValue(cJSON_GetObjectItem(target, "node")));
						gen::String path = cJSON_GetStringValue(cJSON_GetObjectItem(target, "path"));
						if (path == "translation") new_channel.path = gen::loaders::Mesh::AnimationPath::TRANSLATION;
						else if (path == "rotation") new_channel.path = gen::loaders::Mesh::AnimationPath::ROTATION;
						else if (path == "scale") new_channel.path = gen::loaders::Mesh::AnimationPath::SCALE;
					}
					new_anim->channels.add(new_channel);
				}
			}
		}
	}
	for (size_t i = 0; i < mesh.nodes.size(); i++) {
		for (auto& c : mesh.nodes[i].children) {
			uint32_t child_id = (uint32_t)(intptr_t)c;
			auto* child = mesh.nodes.ptr(child_id);
			child->parent = mesh.nodes.ptr(i);
			c = child;
		}
		if (mesh.nodes[i].skin) {
			uint32_t skin_id = ((uint32_t)(intptr_t)mesh.nodes[i].skin) - 1;
			mesh.nodes[i].skin = mesh.skins.ptr(skin_id);
		}
	}
	if (cJSON* scenes = cJSON_GetObjectItem(json, "scenes")) {
		for (size_t i = 0; i < cJSON_GetArraySize(scenes); i++) {
			cJSON* scene = cJSON_GetArrayItem(scenes, i);
			gen::loaders::Mesh::Scene new_scene;
			new_scene.name = cJSON_GetStringValue(cJSON_GetObjectItem(scene, "name"));
			if (cJSON* nodes = cJSON_GetObjectItem(scene, "nodes")) {
				for (size_t i = 0; i < cJSON_GetArraySize(nodes); i++) {
					double node = cJSON_GetNumberValue(cJSON_GetArrayItem(nodes, i));
					if (!isnan(node)) {
						uint32_t node_id = (uint32_t)node;
						new_scene.nodes.add(mesh.nodes.ptr(node_id));
					}
				}
			}
			mesh.scenes.add(new_scene);
		}
	}
	double scene = cJSON_GetNumberValue(cJSON_GetObjectItem(json, "scene"));
	if (!isnan(scene)) {
		mesh.main_scene = mesh.scenes.ptr((uint32_t)scene);
	}
	else mesh.main_scene = nullptr;
	cJSON_free(json);

	return true;
}
