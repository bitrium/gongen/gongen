#include <gongen/gui/mesher.hpp>

using namespace gen;
using namespace gen::gui;

void DefaultMesher::mesh(const Widget& widget, Buffer<Vertex>& vertices, rect2d& interact_area) {
	if (widget.type == WidgetType::BUTTON) {
		vertices.add({{2.0, 2.0}});
		vertices.add({{30, 2.0}});
		vertices.add({{2.0, 10}});
		vertices.add({{30, 10}});
		vertices.add({{30, 2.0}});
		vertices.add({{2.0, 10}});
		interact_area = {2.0, 2.0, 30.0, 10.0};
	}
}
