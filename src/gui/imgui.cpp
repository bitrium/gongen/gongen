#include <gongen/gui/imgui.hpp>

#include <gongen/gui/mesher.hpp>
#include <gongen/gl/gl.hpp>

using namespace gen;
using namespace gen::gui;

struct WidgetImpl {
	Widget info;
	ButtonState click = gen::gui::ButtonState::IDLE;
};
struct BlockImpl {
	gen::DynamicArray<WidgetImpl> widgets;
};
WidgetImpl* block_widget(BlockImpl* impl, size_t& idx) {
	WidgetImpl* ret;
	if (idx >= impl->widgets.size()) {
		ret = impl->widgets.emplace();
	} else {
		ret = impl->widgets.ptr(idx);
	}
	idx++;
	return ret;
}
ButtonState Block::button(const String& name, uint32_t flags) {
	auto* impl = (BlockImpl*) this->impl;
	WidgetImpl* widget = block_widget(impl, this->idx);
	widget->info.type = WidgetType::BUTTON;
	widget->info.flags = flags;
	widget->info.text = name;
	return widget->click;
}

struct ContextImpl {
	window::Window* window = nullptr;
	rect2d area;
	Ptr<Mesher> mesher;
	event::Hopper* hopper = nullptr;
	struct BlockImpl root;
	vec2d mouse_pos;
	ButtonState mouse_click = gen::gui::ButtonState::IDLE;
	gl::Buffer gl_buf;
	gl::Program gl_program;
};
Context::Context(window::Window* window, rect2d area, Ptr<Mesher> mesher) {
	auto* impl = new ContextImpl;
	impl->window = window;
	impl->area = area;
	impl->mesher = mesher.transfer();
	impl->hopper = event::hopper(event::type("input/key") | event::type("input/offset"), window->input());
	this->impl = impl;
}
Context::~Context() {
	auto* impl = (ContextImpl*) this->impl;
	delete impl;
}
Block Context::root() {
	auto* impl = (ContextImpl*) this->impl;
	return Block(&impl->root);
}
void Context::update() {
	auto* impl = (ContextImpl*) this->impl;
	impl->hopper->poll();
	impl->mouse_click = ButtonState::IDLE;
	for (const event::Event* base : impl->hopper->events) {
		if (base->type == event::type("input/offset")) {
			auto* event = (input::OffsetEvent*) base;
			if (event->controller != input::Offsetter::MOUSE_CURSOR) continue;
			impl->mouse_pos = event->value;
		} else if (base->type == event::type("input/key")) {
			auto* event = (input::KeyEvent*) base;
			if (event->action != input::KeyAction::RELEASE) continue;
			if (event->key == input::Key::MOUSE_LEFT) impl->mouse_click = ButtonState::LEFT_CLICKED;
			else if (event->key == input::Key::MOUSE_RIGHT) impl->mouse_click = ButtonState::RIGHT_CLICKED;
		}
	}
	impl->hopper->clear();
}
struct ShaderVertex {
	gen::vec2f pos;
};
void Context::setup_gl(gl::Device& gl) {
	auto* impl = (ContextImpl*) this->impl;
	// TODO only add the attrib and binding instead of replacing
	gl.set_vertex_format({
		.attrib = {
			{
				.binding = 0,
				.format = gl::Format::RG32F,
				.offset = 0,
			},
		},
		.binding = {
			{
				.stride = sizeof(ShaderVertex),
				.per_instance = false,
			},
		},
		.attrib_count = 1,
	});
	// TODO dynamically-sized buffer
	impl->gl_buf = gl.create_buffer(gen::gl::BufferDesc {
		.size = 120 * sizeof(ShaderVertex),
		.usage = gl::Usage::DYNAMIC,
//		.data = triangle,
	});
	gl::ProgramDesc program_desc;
	// TODO fix paths
	program_desc.vertex = {
		.code = gen::file::read_txt("data/shaders/gui.vert"),
		.name = "vertex shader",
	};
	program_desc.fragment = {
		.code = gen::file::read_txt("data/shaders/gui.frag"),
		.name = "fragment shader",
	};
	impl->gl_program = gl.create_program(program_desc);
}
void Context::draw(gl::Device& gl) {
	auto* impl = (ContextImpl*) this->impl;
	Stack<BlockImpl*> blocks;
	Buffer<Vertex> vertices;
	Buffer<ShaderVertex> gl_vertices;
	rect2d interact_area;
	blocks.push(&impl->root);
	do {
		BlockImpl* block = blocks.peek();
		for (WidgetImpl& widget : block->widgets) {
			vertices.clear();
			interact_area.pos = 0.0;
			interact_area.size = 0.0;
			impl->mesher->mesh(widget.info, vertices, interact_area);
			if (interact_area.contains(impl->mouse_pos)) {
				widget.click = impl->mouse_click;
			} else {
				widget.click = ButtonState::IDLE;
			}
			for (Vertex& vertex : vertices) {
				ShaderVertex result;
				result.pos = (impl->area.pos + vertex.pos) * 2.0 / impl->area.size - 1.0;
				result.pos.y *= -1;
				gl_vertices.add(result);
			}
		}
		blocks.pop();
	} while (!blocks.empty());

	gl.set_program(impl->gl_program);
	gl.update_buffer(impl->gl_buf, 0, gl_vertices.size() * sizeof(ShaderVertex), gl_vertices.ptr());
	gl.bind_vertex_buffer(impl->gl_buf, 0);
	gl.draw(gl_vertices.size());
	gl.display();
}
