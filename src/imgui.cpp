// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/imgui/imgui.hpp>
#include <gongen/common/event.hpp>
#include <gongen/common/error.hpp>
#ifdef GEN_MOD_RESOURCES
#include <gongen/resources/manager.hpp>
#endif

void* imgui_alloc(size_t size, void* ud) { return gen::alloc(size); }
void imgui_free(void* ptr, void* ud) { gen::free(ptr); }

static void ImGui_SetClipboardText(void* userdata, const char* text) {
	((gen::window::Window*)userdata)->set_clipboard(text);
}
static const char* ImGui_GetClipboardText(void* userdata) {
	return ((gen::window::Window*)userdata)->get_clipboard();
}

struct imgui_texture {
	gen::gl::Texture texture;
	gen::gl::Sampler sampler;
};

struct imgui_data {
	gen::window::Window* window;
	gen::gl::Device* device;
	gen::gl::Sampler sampler;
	gen::gl::Texture font_texture;

	gen::ByteBuffer vtx_data;
	gen::ByteBuffer idx_data;
	gen::gl::Buffer vtx_buffer;
	gen::gl::Buffer idx_buffer;
	gen::gl::Buffer uniform_buffer;
	size_t vtx_buffer_size = 0;
	size_t idx_buffer_size = 0;

	uint64_t event_type_key = 0;
	uint64_t event_type_offset = 0;
	uint64_t event_type_text = 0;
	uint64_t event_type_window = 0;
	gen::event::Hopper* hopper;

	gen::DynamicArray<imgui_texture> textures;

	gen::gl::PrimitiveState primitive_state;
	gen::gl::DepthState depth_state;
	gen::gl::BlendState blend_state;
	gen::gl::VertexFormat vertex_format;

	gen::gl::Program program;
	gen::res::Handle<gen::gl::Program> program_res;
};

void resize_imgui_buffer(size_t vtx_size, size_t idx_size) {
	imgui_data* im_data = (imgui_data*)ImGui::GetIO().BackendPlatformUserData;
	gen::gl::BufferDesc buffer_desc;
	buffer_desc.usage = gen::gl::Usage::DYNAMIC;
	if (vtx_size > im_data->vtx_buffer_size) {
		buffer_desc.size = gen::capacity_func::npo2(vtx_size, 1);
		im_data->vtx_buffer = im_data->device->create_buffer(buffer_desc);
		im_data->vtx_buffer_size = gen::capacity_func::npo2(vtx_size, 1);
	}
	if (idx_size > im_data->idx_buffer_size) {
		buffer_desc.size = gen::capacity_func::npo2(idx_size, 1);
		im_data->idx_buffer = im_data->device->create_buffer(buffer_desc);
		im_data->idx_buffer_size = gen::capacity_func::npo2(idx_size, 1);
	}
}
void gen::imgui::init(gl::Device* device, window::Window* window, res::Handle<gl::Program> imgui_program) {
	ImGui::SetAllocatorFunctions(imgui_alloc, imgui_free, nullptr);
	ImGui::SetCurrentContext(ImGui::CreateContext());
	
	imgui_data* im_data = new imgui_data();
	im_data->window = window;
	im_data->device = device;
	im_data->program = *imgui_program.load();
	im_data->program_res.transfer_from(imgui_program);

	ImGuiIO& io = ImGui::GetIO();
	io.BackendPlatformName = "GongEn";
	io.BackendRendererName = "GongEn";
	io.BackendPlatformUserData = im_data;
	io.BackendRendererUserData = im_data;
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable | ImGuiConfigFlags_NavEnableKeyboard | ImGuiConfigFlags_NavEnableGamepad;
	io.BackendFlags |= ImGuiBackendFlags_HasGamepad;
	io.SetClipboardTextFn = ImGui_SetClipboardText;
	io.GetClipboardTextFn = ImGui_GetClipboardText;
	io.ClipboardUserData = window;
	vec2u size = window->get_size();
	io.DisplaySize.x = size.x;
	io.DisplaySize.y = size.y;

	im_data->primitive_state.cull_mode = gen::gl::CullMode::NONE;
	im_data->primitive_state.front_ccw = true;
	im_data->primitive_state.topology = gen::gl::PrimitiveTopology::TRIANGLE_LIST;
	im_data->depth_state.enabled = false;
	im_data->blend_state.enabled = true;

	im_data->vertex_format.attrib_count = 3;
	im_data->vertex_format.attrib[0] = { 0, gen::gl::Format::RG32F, offsetof(ImDrawVert, pos) };
	im_data->vertex_format.attrib[1] = { 0, gen::gl::Format::RG32F, offsetof(ImDrawVert, uv) };
	im_data->vertex_format.attrib[2] = { 0, gen::gl::Format::RGBA8, offsetof(ImDrawVert, col) };
	im_data->vertex_format.binding[0] = {sizeof(ImDrawVert), false};

	gen::gl::SamplerDesc sampler_desc;
	im_data->sampler = device->create_sampler(sampler_desc);
	refresh_font_texture();
	resize_imgui_buffer(4 * 1024 * sizeof(ImDrawVert), 16 * 1024 * sizeof(ImDrawIdx));
	im_data->uniform_buffer = im_data->device->create_buffer({ sizeof(float) * 16, gen::gl::Usage::DYNAMIC });
	
	im_data->event_type_key = gen::event::type("input/key");
	im_data->event_type_offset = gen::event::type("input/offset");
	im_data->event_type_text = gen::event::type("input/text");
	im_data->event_type_window = gen::event::type("window");
	im_data->hopper = gen::event::hopper(im_data->event_type_key | im_data->event_type_offset | im_data->event_type_text | im_data->event_type_window);
}
void gen::imgui::refresh_font_texture() {
	imgui_data* im_data = (imgui_data*)ImGui::GetIO().BackendPlatformUserData;
	ImGuiIO& io = ImGui::GetIO();
	io.Fonts->AddFontDefault();
	io.Fonts->Flags = ImFontAtlasFlags_NoPowerOfTwoHeight;
	io.Fonts->Build();
	uint8_t* tx_data;
	int tx_w, tx_h;
	io.Fonts->GetTexDataAsRGBA32(&tx_data, &tx_w, &tx_h);
	gen::gl::TextureDesc tx_desc;
	tx_desc.format = gen::gl::Format::RGBA8;
	tx_desc.data = tx_data;
	tx_desc.size.x = tx_w;
	tx_desc.size.y = tx_h;
	im_data->font_texture = im_data->device->create_texture(tx_desc);
}
ImGuiKey gen_to_imgui_key(gen::input::Key key);
void gen::imgui::new_frame() {
	ImGuiIO& io = ImGui::GetIO();
	imgui_data* im_data = (imgui_data*)io.BackendPlatformUserData;
	im_data->hopper->poll();
	for (auto& e : im_data->hopper->events) {
		if (e->type == im_data->event_type_key) {
			auto event = (gen::input::KeyEvent*)e;
			if ((event->action == gen::input::KeyAction::PRESS) || (event->action == gen::input::KeyAction::RELEASE)) {
				bool pressed = ((event->action == gen::input::KeyAction::PRESS) || (event->action == gen::input::KeyAction::REPEAT));
				uint32_t mouse_button = ImGuiMouseButton_COUNT;
				if (event->key == gen::input::Key::MOUSE_LEFT) mouse_button = ImGuiMouseButton_Left;
				else if (event->key == gen::input::Key::MOUSE_RIGHT) mouse_button = ImGuiMouseButton_Right;
				else if (event->key == gen::input::Key::MOUSE_MIDDLE) mouse_button = ImGuiMouseButton_Middle;
				if (mouse_button != ImGuiMouseButton_COUNT) io.AddMouseButtonEvent(mouse_button, pressed);
				else {
					ImGuiKey imgui_key = gen_to_imgui_key(event->key);
					io.AddKeyEvent(imgui_key, pressed);
				}
			}
		}
		else if (e->type == im_data->event_type_offset) {
			auto event = (gen::input::OffsetEvent*)e;
			if (event->controller == gen::input::Offsetter::MOUSE_SCROLL) {
				io.AddMouseWheelEvent(event->value.x, event->value.y);
			}
		}
		else if (e->type == im_data->event_type_text) {
			auto event = (gen::input::TextEvent*)e;
			io.AddInputCharactersUTF8(event->str.c_str());
		}
		else if (e->type == im_data->event_type_window) {
			auto event = (gen::window::WindowEvent*)e;
			if (event->action == gen::window::WindowEventAction::FOCUS) io.AddFocusEvent(true);
			else io.AddFocusEvent(false);
		}
	}
	im_data->hopper->clear();
	vec2d mouse_pos = im_data->window->cursor_pos();
	io.AddMousePosEvent(mouse_pos.x, mouse_pos.y);
	ImGui::NewFrame();

	im_data->textures.clear();
}
void gen::imgui::render() {
	imgui_data* im_data = (imgui_data*)ImGui::GetIO().BackendPlatformUserData;
	ImGui::Render();
	ImDrawData* draw_data = ImGui::GetDrawData();
	im_data->vtx_data.clear();
	im_data->idx_data.clear();

	gen::gl::Device* dev = im_data->device;

	for (size_t i = 0; i < draw_data->CmdListsCount; i++) {
		ImDrawList* draw_list = draw_data->CmdLists[i];
		size_t vtx_offset = (im_data->vtx_data.size() / sizeof(ImDrawVert));
		im_data->vtx_data.add(draw_list->VtxBuffer.Data, draw_list->VtxBuffer.Size * sizeof(ImDrawVert));
		ImDrawIdx* idx = (ImDrawIdx*)im_data->idx_data.add(draw_list->IdxBuffer.Data, draw_list->IdxBuffer.Size * sizeof(ImDrawIdx));
		if (vtx_offset != 0) {
			for (size_t j = 0; j < (draw_list->IdxBuffer.Size); j++) {
				idx[j] += vtx_offset;
			}
		}
	}
	if (im_data->vtx_data.empty()) return;
	resize_imgui_buffer(im_data->vtx_data.size(), im_data->idx_data.size());
	dev->update_buffer(im_data->vtx_buffer, 0, im_data->vtx_data.size(), im_data->vtx_data.ptr());
	dev->update_buffer(im_data->idx_buffer, 0, im_data->idx_data.size(), im_data->idx_data.ptr());

	float L = draw_data->DisplayPos.x;
	float R = draw_data->DisplayPos.x + draw_data->DisplaySize.x;
	float T = draw_data->DisplayPos.y;
	float B = draw_data->DisplayPos.y + draw_data->DisplaySize.y;
	float ortho_projection[4][4] =
	{
		{2.0f / (R - L),   0.0f,         0.0f,   0.0f},
		{0.0f,         2.0f / (T - B),   0.0f,   0.0f},
		{0.0f,         0.0f,        -1.0f,   0.0f},
		{(R + L) / (L - R),  (T + B) / (B - T),  0.0f,   1.0f}
	};
	dev->update_buffer(im_data->uniform_buffer, 0, sizeof(float) * 16, ortho_projection);

	im_data->program_res.reload_deref(im_data->program);
	dev->set_program(im_data->program);
	dev->set_vertex_format(im_data->vertex_format);
	dev->bind_vertex_buffer(im_data->vtx_buffer, 0);
	dev->bind_index_buffer(im_data->idx_buffer);
	dev->set_blend_state(im_data->blend_state);
	dev->set_depth_state(im_data->depth_state);
	dev->set_primitive_state(im_data->primitive_state);
	dev->bind_uniform_buffer(im_data->uniform_buffer, 0);
	dev->bind_texture(im_data->font_texture, 0);
	dev->bind_sampler(im_data->sampler, 0);

	ImVec2 clip_off = draw_data->DisplayPos;
	ImVec2 clip_scale = draw_data->FramebufferScale;

	// size_t vtx_offset = 0;
	size_t idx_offset = 0;
	for (size_t i = 0; i < draw_data->CmdListsCount; i++) {
		ImDrawList* draw_list = draw_data->CmdLists[i];
		for (size_t j = 0; j < draw_list->CmdBuffer.Size; j++) {
			ImDrawCmd* cmd = &draw_list->CmdBuffer[j];

			if (cmd->TextureId == 0) dev->bind_texture(im_data->font_texture, 0);
			else {
				size_t real_texture_id = (cmd->TextureId - 1);
				if (real_texture_id < im_data->textures.size()) {
					dev->bind_texture(im_data->textures.get(real_texture_id).texture, 0);
					dev->bind_sampler(im_data->textures.get(real_texture_id).sampler, 0);
				}
			}

			vec2f clip_min((cmd->ClipRect.x - clip_off.x) * clip_scale.x, (cmd->ClipRect.y - clip_off.y) * clip_scale.y);
			vec2f clip_max((cmd->ClipRect.z - clip_off.x) * clip_scale.x, (cmd->ClipRect.w - clip_off.y) * clip_scale.y);

			dev->set_scissor({true, clip_min, clip_max - clip_min});
			dev->draw_indexed(cmd->ElemCount, cmd->IdxOffset + idx_offset);
		}
		idx_offset += draw_list->IdxBuffer.Size;
		// vtx_offset += draw_list->VtxBuffer.Size;
	}
}
void gen::imgui::shutdown() {
	imgui_data* im_data = (imgui_data*)ImGui::GetIO().BackendPlatformUserData;
	ImGui::GetIO().BackendPlatformUserData = nullptr;
	ImGui::GetIO().BackendRendererUserData = nullptr;
	ImGui::DestroyContext(ImGui::GetCurrentContext());
	delete im_data;
}
ImTextureID gen::imgui::texture_id(const gen::gl::Texture& texture, const gen::gl::Sampler& sampler) {
	imgui_data* im_data = (imgui_data*)ImGui::GetIO().BackendPlatformUserData;
	imgui_texture tx;
	tx.texture = texture;
	tx.sampler = sampler;
	im_data->textures.add(tx);
	return im_data->textures.size();
}

uint16_t imgui_key_table[] = {
	ImGuiKey_None,

	ImGuiKey_None, ImGuiKey_None, ImGuiKey_None,
	ImGuiKey_None, ImGuiKey_None,

	// Keyboard
	ImGuiKey_LeftSuper, ImGuiKey_LeftCtrl, ImGuiKey_LeftShift, ImGuiKey_LeftAlt,
	ImGuiKey_RightSuper, ImGuiKey_RightCtrl, ImGuiKey_RightShift, ImGuiKey_RightAlt,
	ImGuiKey_Escape, ImGuiKey_CapsLock, ImGuiKey_Backspace,
	ImGuiKey_PrintScreen, ImGuiKey_ScrollLock, ImGuiKey_Pause,
	ImGuiKey_Delete, ImGuiKey_Insert, ImGuiKey_Home, ImGuiKey_End, ImGuiKey_PageUp, ImGuiKey_PageDown,
	ImGuiKey_UpArrow, ImGuiKey_DownArrow, ImGuiKey_LeftArrow, ImGuiKey_RightArrow,
	ImGuiKey_Space, ImGuiKey_Tab, ImGuiKey_Enter,
	ImGuiKey_NumLock, ImGuiKey_KeypadEnter,
	ImGuiKey_KeypadAdd, ImGuiKey_KeypadSubtract, ImGuiKey_KeypadMultiply, ImGuiKey_KeypadDivide,
	ImGuiKey_Keypad0, ImGuiKey_Keypad1, ImGuiKey_Keypad2, ImGuiKey_Keypad3, ImGuiKey_Keypad4,
	ImGuiKey_Keypad5, ImGuiKey_Keypad6, ImGuiKey_Keypad7, ImGuiKey_Keypad8, ImGuiKey_Keypad9,
	ImGuiKey_F1, ImGuiKey_F2, ImGuiKey_F3, ImGuiKey_F4, ImGuiKey_F5, ImGuiKey_F6, ImGuiKey_F7, ImGuiKey_F8,
	ImGuiKey_F9, ImGuiKey_F10, ImGuiKey_F11, ImGuiKey_F12, ImGuiKey_None, ImGuiKey_None, ImGuiKey_None, ImGuiKey_None,
	ImGuiKey_None, ImGuiKey_None, ImGuiKey_None, ImGuiKey_None, ImGuiKey_None, ImGuiKey_None, ImGuiKey_None, ImGuiKey_None,
	ImGuiKey_0, ImGuiKey_1, ImGuiKey_2, ImGuiKey_3, ImGuiKey_4, ImGuiKey_5, ImGuiKey_6, ImGuiKey_7, ImGuiKey_8, ImGuiKey_9,
	ImGuiKey_Q, ImGuiKey_W, ImGuiKey_E, ImGuiKey_R, ImGuiKey_T, ImGuiKey_Y, ImGuiKey_U, ImGuiKey_I, ImGuiKey_O, ImGuiKey_P,
	ImGuiKey_A, ImGuiKey_S, ImGuiKey_D, ImGuiKey_F, ImGuiKey_G, ImGuiKey_H, ImGuiKey_J, ImGuiKey_K, ImGuiKey_L,
	ImGuiKey_Z, ImGuiKey_X, ImGuiKey_C, ImGuiKey_Y, ImGuiKey_B, ImGuiKey_N, ImGuiKey_M,
	ImGuiKey_GraveAccent, ImGuiKey_Minus, ImGuiKey_Equal,
	ImGuiKey_LeftBracket, ImGuiKey_RightBracket, ImGuiKey_Backslash,
	ImGuiKey_Semicolon, ImGuiKey_Apostrophe,
	ImGuiKey_Comma, ImGuiKey_Period, ImGuiKey_Slash,

	// Gamepad
	ImGuiKey_GamepadFaceUp, ImGuiKey_GamepadFaceDown, ImGuiKey_GamepadFaceLeft, ImGuiKey_GamepadFaceRight,
	ImGuiKey_GamepadDpadUp, ImGuiKey_GamepadDpadDown, ImGuiKey_GamepadDpadLeft, ImGuiKey_GamepadDpadRight,
	ImGuiKey_GamepadBack, ImGuiKey_None, ImGuiKey_GamepadStart,
	ImGuiKey_GamepadL1, ImGuiKey_GamepadR1, ImGuiKey_GamepadL2, ImGuiKey_GamepadR2,
	ImGuiKey_GamepadL3, ImGuiKey_GamepadR3,
};

ImGuiKey gen_to_imgui_key(gen::input::Key key) {
	if ((size_t)key > (sizeof(imgui_key_table) / sizeof(uint16_t))) throw gen::KeyInputException("Invalid Key(ImGui)");
	return (ImGuiKey)imgui_key_table[(size_t)key];
}

struct InputTextCallback_UserData {
	gen::String* str;
	ImGuiInputTextCallback callback;
	void* user;
};

static int ImGui_GenStringCallback(ImGuiInputTextCallbackData* data) {
	InputTextCallback_UserData* ud = (InputTextCallback_UserData*)data->UserData;
	gen::String* str = ud->str;
	if (data->EventFlag == ImGuiInputTextFlags_CallbackResize) {
		str->reserve(data->BufTextLen - str->size());
		data->Buf = str->data();
	} else if(ud->callback) {
		data->UserData = ud->user;
		return ud->callback(data);
	}
	return 0;
}

bool ImGui::InputText(const char* label, gen::String& str, ImGuiInputTextFlags flags, ImGuiInputTextCallback callback, void* user_data) {
	InputTextCallback_UserData ud;
	ud.str = &str;
	ud.callback = callback;
	ud.user = user_data;
	return ImGui::InputText(label, str.data(), str.size() + 1, flags | ImGuiInputTextFlags_CallbackResize, ImGui_GenStringCallback, &ud);
}
bool ImGui::InputTextMultiline(const char* label, gen::String& str, const ImVec2& size, ImGuiInputTextFlags flags, ImGuiInputTextCallback callback, void* user_data) {
	InputTextCallback_UserData ud;
	ud.str = &str;
	ud.callback = callback;
	ud.user = user_data;
	return ImGui::InputTextMultiline(label, str.data(), str.size() + 1, size, flags | ImGuiInputTextFlags_CallbackResize, ImGui_GenStringCallback, &ud);
}
bool ImGui::InputTextWithHint(const char* label, const char* hint, gen::String& str, ImGuiInputTextFlags flags, ImGuiInputTextCallback callback, void* user_data) {
	InputTextCallback_UserData ud;
	ud.str = &str;
	ud.callback = callback;
	ud.user = user_data;
	return ImGui::InputTextWithHint(label, hint, str.data(), str.size() + 1, flags | ImGuiInputTextFlags_CallbackResize, ImGui_GenStringCallback, &ud);
}
