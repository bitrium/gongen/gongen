// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/sound/resample.hpp>
#include <gongen/common/error.hpp>
#include <gongen/common/platform/system.hpp>
#include <gongen/common/math.hpp>

size_t gen::sound::resample_calc_dst(audio::Format from, audio::Format to, size_t src_frames) {
	if (from.sample_rate == to.sample_rate) return src_frames;
	return (size_t) gen::ceil(((double) to.sample_rate / (double) from.sample_rate) * (double) src_frames);
}

size_t gen::sound::resample_calc_src(audio::Format from, audio::Format to, size_t dst_frames) {
	if (from.sample_rate == to.sample_rate) return dst_frames;
	return (size_t) gen::ceil(((double) from.sample_rate / (double) to.sample_rate) * (double) dst_frames);
}

static void copy_sample_float_to_int(uint8_t from, uint8_t** src, uint8_t to, uint8_t** dst, uint8_t* bit) {
	double sample;
	if (from == 32) {
		sample = **((float**) src);
	} else if (from == 64) {
		sample = **((double**) src);
	} else {
		throw gen::RuntimeException("Only 32- and 64-bit floating-point samples are supported.");
	}
	gen::clamp(sample, -1.0, 1.0);
	if (to == 0 || to > 64) {
		throw gen::RuntimeException("Only up to 64-bit integer samples are supported.");
	}
	int64_t max = (1 << (to - 1));
	union {
		int64_t val;
		uint8_t data[8];
	} res;
	res.val = (int64_t) round((double) max * sample);
	if (res.val == max) res.val--;

	uint8_t byte = 0;
	int8_t left = to;
	while (left > 0) {
		**dst &= ~(0xFF >> *bit);
		**dst |= res.data[byte] >> *bit;
		(*dst)++;
		byte++;
		*bit = (8 - *bit);
		left -= *bit;
		*bit %= 8;
		if (left <= 0) break;
		**dst &= ~(0xFF << *bit);
		**dst |= res.data[byte] >> *bit;
	}

	(*src) += 4;
}

void gen::sound::resample(
	audio::Format from, uint8_t* src, size_t* src_frames,
	audio::Format to, uint8_t* dst, size_t* dst_frames
) {
	if (from == to) {
		memcpy(dst, src, *src_frames * to.bit_depth * to.channel_count);
		*dst_frames = *src_frames;
		return;
	}
	void (*copy_sample)(uint8_t from, uint8_t** src, uint8_t to, uint8_t** dst, uint8_t* bit);
	copy_sample = &copy_sample_float_to_int;
	size_t src_frame = 0;
	size_t dst_frame = 0;
	uint8_t bit_storage = 0;
	double rate_state = 0.0f;
	while (src_frame < *src_frames * from.bit_depth) {
		rate_state += (double) to.sample_rate / (double) from.sample_rate;
		while (rate_state >= 1.0f) {
			if (dst_frame >= *dst_frames) goto done;
			if (from.channel_count == to.channel_count) {
				for (uint8_t i = 0; i < from.channel_count; i++) {
					copy_sample(from.bit_depth, &src, to.bit_depth, &dst, &bit_storage);
				}
			} else if (from.channel_count == 1) {
				uint8_t* result = dst;
				copy_sample(from.bit_depth, &src, to.bit_depth, &dst, &bit_storage);
				for (uint8_t i = 0; i < to.channel_count; i++) {
					memcpy(dst, result, to.bit_depth);
				}
			} else {
				throw gen::ArgumentException::createf("Unsupported channel count conversion %hhu->%hhu", from.channel_count, to.channel_count);
			}
			dst_frame++;
			rate_state -= 1.0f;
		}
		src_frame++;
	}
	done:
	*src_frames = src_frame;
	*dst_frames = dst_frame;
}
