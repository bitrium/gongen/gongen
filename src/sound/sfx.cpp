// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/sound/sfx.hpp>
// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/sound/music.hpp>
#include <gongen/loaders/audio.hpp>
#include <gongen/resources.hpp>
#include <gongen/sound/mixer.hpp>

using namespace gen;
using namespace gen::sound;

struct SfxImpl {
	gen::Buffer<float> samples;
};

Sfx::Sfx(const res::Identifier& id) : id(id) {
	this->impl = new SfxImpl();
}
Sfx::~Sfx() {
	delete (SfxImpl*) this->impl;
}

SfxPlayer::SfxPlayer(res::Manager* resources, const String& kind) : kind_id(kind), resources(resources) {
}
SfxPlayback* SfxPlayer::play(const res::Identifier& id, gen::vec3f pos) {
	auto* sfx = (Sfx*) this->resources->get(id.expanded(this->kind_id));
	if (!sfx) return nullptr;
	this->changing.wait(true);
	this->changing = true;
	SfxPlayback* playback = &this->playback.add(SfxPlayback { .id = sfx->id });
	playback->pos = pos;
	this->changing = false;
	return playback;
}
size_t SfxPlayer::stop(const res::Identifier& id) {
	gen::Buffer<size_t> to_remove;
	this->changing.wait(true);
	this->changing = true;
	for (size_t i = 0; i < this->playback.size(); i++) {
		if (this->playback.get(i).id == id) to_remove.add(i);
	}
	for (size_t i : to_remove) {
		this->playback.remove_idx(i);
	}
	this->changing = false;
	return to_remove.size();
}
void SfxPlayer::data(Buffer<float>& buf, size_t frame_count) {
	GEN_ASSERT(MIXER_FORMAT.channel_count >= 2, "sfx player requires at least 2 channels");
	float* samples = buf.reserve(frame_count * MIXER_FORMAT.channel_count);
	memset(samples, 0, frame_count * MIXER_FORMAT.channel_count * sizeof(float));
	if (this->pause) return;
	this->playback_buf.clear();
	this->changing.wait(true);
	this->changing = true;
	this->playback_buf.reserve(this->playback.size());
	for (SfxPlayback& sfx_playback : this->playback) {
		sfx_playback.impl_id = this->next_impl_id; // we will use that to identify the original later
		this->playback_buf.add(sfx_playback);
	}
	this->changing = false;
	this->to_remove_buf.clear();
	size_t idx = 0;
	for (SfxPlayback& sfx_playback : this->playback_buf) {
		Sfx* sfx = (Sfx*) this->resources->get(sfx_playback.id);
		SfxImpl* impl = (SfxImpl*) sfx->impl;
		double speed_prog = 0.0;
		for (size_t frame = 0; frame < frame_count; frame++) {
			if (sfx_playback.frame >= impl->samples.size()) break;
			// TODO pan according to position
			samples[frame * MIXER_FORMAT.channel_count] += impl->samples[sfx_playback.frame] * sfx_playback.volume;
			samples[frame * MIXER_FORMAT.channel_count + 1] += impl->samples[sfx_playback.frame] * sfx_playback.volume;
			speed_prog += sfx_playback.speed;
			if (speed_prog >= 1.0) {
				sfx_playback.frame++;
				speed_prog -= 1.0;
			}
		}
		if (sfx_playback.frame >= impl->samples.size()) {
			this->to_remove_buf.add(idx);
		}
		idx++;
	}
	this->changing.wait(true);
	this->changing = true;
	idx = 0;
	for (SfxPlayback& sfx_playback : this->playback) {
		if (sfx_playback.impl_id != this->next_impl_id) continue; // must have been just added
		sfx_playback.frame += frame_count * sfx_playback.speed;
		for (size_t to_remove : this->to_remove_buf) {
			if (idx != to_remove) continue;
			this->playback.remove(sfx_playback);
		}
		idx++;
	}
	this->changing = false;
	this->playback_buf.clear();
	this->next_impl_id++;
}

class SfxHandler : public res::KindHandler {
public:
	const String kind_id;
	SfxHandler(const String& kind) : kind_id(kind) {
	}
	String get_id() const {
		return this->kind_id;
	}
	Buffer<res::KindFile> get_files() const {
		Buffer<res::KindFile> ret;
		ret.add(res::KindFile {
			.extension = "opus",
			.keep_open = true,
			.load { .data = [](KindHandler* handler, const res::Identifier& id, void** item, SeekableDataSource* data) {
				Sfx* sfx = (Sfx*) *item;
				auto* impl = (SfxImpl*) sfx->impl;
				gen::loaders::Audio audio(data, false);
				size_t channels = audio.get_channels();
				size_t frame_count = audio.get_frame_count();
				size_t frames = 0;
				float* samples = impl->samples.reserve(frame_count * channels);
				while (frames < frame_count) {
					frames += audio.get_samples_float(samples + frames * channels, frame_count - frames, channels == 2);
				}
				// TODO resample to mixer format
				if (channels <= 1) return;
				for (frames = 0; frames < frame_count; frames++) {
					impl->samples[frames] = impl->samples[frames * channels];
				}
				impl->samples.resize(frame_count);
				impl->samples.shrink_to_fit();
			} },
		});
		return ret;
	}
	void create(void** item) {
		*item = new Sfx(*((res::Identifier*) *item));
	}
	void destroy(void** item) {
		delete (Music*) *item;
	}
};

gen::res::KindHandler* gen::sound::sfx_handler(const String& kind) {
	return new SfxHandler(kind);
}
