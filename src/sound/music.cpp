// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/sound/music.hpp>
#include <gongen/loaders/audio.hpp>
#include <gongen/resources.hpp>
#include <gongen/sound/mixer.hpp>

using namespace gen;
using namespace gen::sound;

struct MusicImpl {
	gen::loaders::Audio audio;
	uint8_t channels = 2;
	gen::Buffer<float> samples;
	bool is_buffered = false;
	size_t pos = 0;
};

size_t music_request_samples(MusicImpl* impl, size_t frame_count, double buffer_secs) {
	size_t frames = 0;
	while (impl->samples.size() / impl->channels - impl->pos < frame_count) {
		if (impl->pos > 0) {
			// move buffer data to the beginning
			memmove(impl->samples.ptr(), impl->samples.ptr(impl->pos * impl->channels), (impl->samples.size() - impl->pos * impl->channels) * sizeof(float));
			impl->samples.resize(impl->samples.size() - impl->pos * impl->channels); // FIXME set size not capacity
			impl->pos = 0;
		}
		float* dst = impl->samples.reserve((size_t) (48000.0 * buffer_secs) * impl->channels);
		while (frames < (size_t) (48000.0 * buffer_secs)) {
			size_t new_frames = impl->audio.get_samples_float(dst, (size_t) (48000.0 * buffer_secs) - frames, impl->channels == 2);
			if (!new_frames) return impl->pos + frames;
			frames += new_frames;
			dst += new_frames * impl->channels;
		}
	}
	return impl->pos + frames;
}

Music::Music(const res::Identifier& id) : id(id) {
	this->impl = new MusicImpl();
}
Music::~Music() {
	auto* impl = (MusicImpl*) this->impl;
	if (!impl) return;
	impl->audio.close();
	delete impl;
}

void Music::buffer() {
	auto* impl = (MusicImpl*) this->impl;
	impl->samples.clear();
	size_t frames = impl->audio.get_frame_count();
	impl->pos = impl->audio.tell() / impl->channels;
	impl->audio.seek(0);
	impl->samples.reserve(frames * impl->audio.get_channels());
	size_t frame = 0;
	while (frames > 0) {
		size_t res = impl->audio.get_samples_float(impl->samples.ptr(frame * impl->channels), 10000, impl->channels == 2 ? true : false);
		if (res == 0) break;
		frames -= res;
		frame += res;
	}
	impl->is_buffered = true;
}
void Music::free() {
	auto* impl = (MusicImpl*) this->impl;
	impl->samples.clear(true);
	impl->is_buffered = false;
	impl->audio.seek(impl->pos);
	impl->pos = 0;
}

MusicPlayer::MusicPlayer(res::Manager* resources, const String& kind, uint64_t rng_seed) : kind_id(kind), resources(resources) {
	this->rng.rng = gen::rand::rng_lcg();
	if (rng_seed == UINT64_MAX) this->rng.seed();
	else this->rng.seed(rng_seed);
}
void MusicPlayer::reset_interval() {
	this->interval = ((double) this->rng.u32(this->intervals.x, this->intervals.y)) / 1000.0;
}
bool MusicPlayer::is_playing() {
	return !this->current.is_empty();
}
void MusicPlayer::play(const res::Identifier& track) {
	auto* music = (Music*) this->resources->get(track.expanded(this->kind_id));
	auto* impl = (MusicImpl*) music->impl;
	impl->audio.seek(0);
	impl->pos = 0;
	this->current = music->id;
}
void MusicPlayer::skip() {
	this->reset_interval();
	this->current.clear();
}
bool MusicPlayer::is_paused() {
	return this->paused;
}
void MusicPlayer::pause() {
	this->paused = true;
}
void MusicPlayer::unpause() {
	this->paused = false;
}
bool MusicPlayer::is_held() {
	return !this->holding.is_empty();
}
void MusicPlayer::hold(const res::Identifier& filler) {
	auto* music = (Music*) this->resources->get(filler.expanded(this->kind_id));
	auto* impl = (MusicImpl*) music->impl;
	impl->audio.seek(0);
	impl->pos = 0;
	this->holding = music->id;
}
void MusicPlayer::unhold() {
	this->holding.clear();
}
void MusicPlayer::data(Buffer<float>& buf, size_t frame_count) {
	// TODO resample to mixer format
	Music* music;
	MusicImpl* impl;
	size_t frame = 0;
	if (this->paused) goto fill_zeros;
	if (!this->holding.is_empty()) {
		music = (Music*) this->resources->get(this->holding);
	} else if (!this->current.is_empty()) {
		music = (Music*) this->resources->get(this->current);
	} else {
		if (this->interval > 0) {
			this->interval -= (double) frame_count / (double) MIXER_FORMAT.sample_rate;
			goto fill_zeros;
		}
		if (res::Identifier* next = this->queue.dequeue()) {
			this->current = *next;
			music = (Music*) this->resources->get(this->current);
			delete next;
		} else if (this->select_random) {
			gen::DynamicArray<res::Identifier> tracks;
			this->resources->list(this->kind_id).unwrap(tracks);
			if (tracks.empty()) goto fill_zeros;
			this->current = tracks.get(rng.u32(tracks.size()));
			music = (Music*) this->resources->get(this->current);
		}
		if (music) {
			auto* impl = (MusicImpl*) music->impl;
			impl->audio.seek(0);
			impl->pos = 0;
		} else this->current.clear();
	}
	if (!music) goto fill_zeros;
	impl = (MusicImpl*) music->impl;
	if (impl->is_buffered) {
		frame = impl->samples.size() / impl->channels - impl->pos;
	} else {
		frame = music_request_samples(impl, frame_count, this->buffer_secs);
	}
	if (frame > frame_count) frame = frame_count;
	memcpy(buf.reserve(frame * impl->channels), impl->samples.ptr(impl->pos * impl->channels), frame * impl->channels * sizeof(float));
	impl->pos += frame;
	if (frame < frame_count) {
		// music ended
		if (!this->holding.is_empty()) this->holding.clear();
		else if (!this->current.is_empty()) this->reset_interval();
	}
	fill_zeros:
	while (frame < frame_count) {
		for (int i = 0; i < MIXER_FORMAT.channel_count; i++) {
			buf.add(0.0f);
		}
		frame++;
	}
}

class MusicHandler : public res::KindHandler {
public:
	const String kind_id;
	MusicHandler(const String& kind) : kind_id(kind) {
	}
	String get_id() const {
		return this->kind_id;
	}
	Buffer<res::KindFile> get_files() const {
		Buffer<res::KindFile> ret;
		ret.add(res::KindFile {
			.extension = "opus",
			.keep_open = true,
			.load { .data = [](KindHandler* handler, const res::Identifier& id, void** item, SeekableDataSource* data) {
				auto* music = (Music*) *item;
				auto* impl = (MusicImpl*) music->impl;
				impl->audio.open(data, false);
				impl->channels = impl->audio.get_channels();
			} },
		});
		return ret;
	}
	void create(void** item) {
		*item = new Music(*((res::Identifier*) *item));
	}
	void destroy(void** item) {
		delete (Music*) *item;
	}
};

gen::res::KindHandler* gen::sound::music_handler(const String& kind) {
	return new MusicHandler(kind);
}
