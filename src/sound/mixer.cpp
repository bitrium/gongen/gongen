// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/sound/mixer.hpp>
#include <gongen/sound/resample.hpp>

using namespace gen::sound;

Effect* Track::add_effect(Effect* effect) {
	return this->effects.add(effect);
}
void Track::process(Buffer<float>& samples) {
	for (Effect* effect : this->effects) {
		effect->process(samples);
	}
}
void Track::clear_effects() {
	this->effects.clear();
}


void Mixer::clear_buffers() {
	for (int i = 0; i < 0x100; i++) {
		this->process_buffers[i].clear(true);
	}
}

uint8_t Mixer::link(uint8_t dst, uint8_t src, uint8_t count) {
	// TODO prevent creating loops
	LinkedList<uint8_t>& links = this->links[dst];
	uint8_t amount = 0;
	for (uint8_t i = 0; i < count; i++) {
		if (links.contains(i + src)) continue;
		links.add(i + src);
		amount++;
	}
	return amount;
}
uint8_t Mixer::unlink(uint8_t dst, uint8_t src, uint8_t count) {
	LinkedList<uint8_t>& links = this->links[dst];
	uint8_t amount = 0;
	for (uint8_t i = 0; i < count; i++) {
		if (links.remove_match_safe(i + src)) amount++;
	}
	return amount;
}
uint8_t Mixer::unlink_all(uint8_t dst) {
	uint8_t amount = this->links[dst].size();
	this->links[dst].clear();
	return amount;
}
size_t Mixer::unlink_recursive(uint8_t dst) {
	// FIXME infinite recursion?
	LinkedList<uint8_t>& links = this->links[dst];
	size_t amount = links.size();
	for (uint8_t link : links) {
		amount += this->unlink_recursive(link);
	}
	links.clear();
	return amount;
}
size_t Mixer::clear_links() {
	size_t amount = 0;
	for (size_t i = 0; i < 0x100; i++) {
		amount += this->unlink_all(i);
	}
	return amount;
}

void Mixer::process(uint8_t master, size_t frame_count) {
	uint8_t track_stack[0x100] = {0};
	uint8_t iter_stack[0x100] = {0};
	bool processed[0x100] = {0};
	int depth = 0;
	track_stack[0] = master;
	while (depth >= 0) {
		// skip processed tracks (ready samples already lay in a buffer)
		if (processed[track_stack[depth]]) {
			depth--;
			continue;
		}

		// repeat the entire process for each of the source tracks
		LinkedList<uint8_t>& links = this->links[track_stack[depth]];
		if (iter_stack[depth] < links.size()) {
			track_stack[depth + 1] = links.get(iter_stack[depth]++);
			depth++;
			continue;
		}

		// after sources are done, process the track
		Buffer<float>& buffer = this->process_buffers[track_stack[depth]];
		buffer.clear();
		if (this->tracks[track_stack[depth]].input_.get()) this->tracks[track_stack[depth]].input_->data(buffer, frame_count);
		size_t clear_samples = frame_count * MIXER_FORMAT.channel_count - buffer.size();
		if (clear_samples > frame_count * MIXER_FORMAT.channel_count) clear_samples = 0;
		memset(buffer.reserve(clear_samples), 0, clear_samples * sizeof(float));
		for (uint8_t i = 0; i < links.size(); i++) {
			Buffer<float>& other_buffer = this->process_buffers[links.get(i)];
			for (size_t frame = 0; frame < frame_count; frame++) {
				for (uint8_t channel = 0; channel < MIXER_FORMAT.channel_count; channel++) {
					size_t sample = frame * MIXER_FORMAT.channel_count + channel;
					buffer[sample] += other_buffer[sample] * this->tracks[links.get(i)].volume;
				}
			}
		}
		this->tracks[track_stack[depth]].process(buffer);
		processed[track_stack[depth]] = true;
		depth--;
	}
	Buffer<float>& buffer = this->process_buffers[master];
	for (size_t frame = 0; frame < frame_count; frame++) {
		for (uint8_t channel = 0; channel < MIXER_FORMAT.channel_count; channel++) {
			buffer[frame * MIXER_FORMAT.channel_count + channel] *= this->tracks[master].volume;
		}
	}
}
gen::audio::OutputCallback Mixer::audio_output() {
	return [](void* user, gen::audio::Output* stream, size_t dst_frames, uint8_t* dst) {
		Mixer* self = (Mixer*) user;
		size_t src_frames = resample_calc_src(stream->get_format(), MIXER_FORMAT, dst_frames);
		self->process(0, src_frames);
		uint8_t* src = (uint8_t*) self->process_buffers[0].ptr();
		resample(MIXER_FORMAT, src, &src_frames, stream->get_format(), dst, &dst_frames);
		return dst_frames;
	};
}
