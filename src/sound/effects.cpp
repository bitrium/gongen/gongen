// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/sound/effects.hpp>
#include <gongen/common/math.hpp>
#include <gongen/sound/mixer.hpp>

using namespace gen::sound::effects;

void Limiter::process(gen::Buffer<float>& samples) {
	for (size_t i = 0; i < samples.size(); i++) {
		clamp(samples[i], -this->threshold, this->threshold);
	}
}

void Panning::process(gen::Buffer<float>& samples) {
	// TODO
}

void WeirdWave::process(gen::Buffer<float>& samples) {
	for (size_t i = 0; i < samples.size(); i += MIXER_FORMAT.channel_count) {
		samples[i] *= this->dry;
		float value = this->amplitude * sinf((this->offset + this->shape) * offset) + this->width * sinf(this->quality * M_PI * offset);
		samples[i] += value * this->wet;
		for (size_t c = 1; c < MIXER_FORMAT.channel_count; c++) {
			samples[i + c] = samples[i];
		}
		this->offset += 0.01;
	}
}

void SinWave::process(gen::Buffer<float>& samples) {
	for (size_t i = 0; i < samples.size(); i += MIXER_FORMAT.channel_count) {
		float value = this->volume * sinf(this->time);
		for (size_t c = 0; c < MIXER_FORMAT.channel_count; c++) {
			samples[i] += value;
		}
		this->time += M_TAU * this->frequency / (double) MIXER_FORMAT.sample_rate;
		if (this->time >= M_TAU) this->time -= M_TAU;
	}
}
