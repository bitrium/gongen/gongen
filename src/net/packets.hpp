// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/defines.hpp>

#pragma pack(push, 1)

struct GnpHeader {
	uint32_t magic;
	union {
		struct {
			uint32_t size : 20;
			uint32_t type : 11;
//			uint32_t compressed : 1;
		} complex; // used in app
		uint32_t size; // used in status and handshake
	};
};

enum GnpErrorCode : uint8_t {
	ERROR_NONE = 0,
	ERROR_OTHER = 1,
	ERROR_UNSUPPORTED = 2,
};

enum GnpStatusRequestType : uint8_t {
	REQ_ALL = 1,
	REQ_CUSTOM = 2,
	REQ_PROTOCOL = 3,
	REQ_INFO = 4,
	REQ_COUNT_,
};
struct GnpStatusRequest {
	uint8_t type = 0;
	uint16_t serial = 0;
};
struct GnpStatusRequestCustom {
	GnpStatusRequest request = {.type = REQ_CUSTOM};
	uint16_t version = 0;
};
struct GnpStatusResponse {
	uint8_t type = 0;
	uint16_t serial = 0;
	uint8_t error = ERROR_NONE;
};
namespace GnpCompressionSupport {
enum {
	ZLIB = 1,
	ZSTD = 2
};
}
namespace GnpEncryptionSupport {
enum {
	RSA = 1,
	AES = 2
};
}
struct GnpStatusResponseProtocol {
	GnpStatusResponse response;
	uint8_t protocol_version = 0;
	// TODO
//	uint8_t compression = 0;
//	uint8_t encryption = 0;
};
struct GnpStatusResponseInfo {
	GnpStatusResponse response;
	uint16_t user_limit = 0;
	uint8_t name_len = 0;
};

enum GnpHandshakePacketType : uint8_t {
	HS_INIT = 1,
	HS_CANCEL = 2,
	HS_COMPLETE = 3,
	HS_COUNT_,
};
struct GnpHandshakePacket {
	uint8_t type;
};
struct GnpHandshakeInit {
	GnpHandshakePacket packet;
	uint8_t protocol_version;
	// TODO
};

enum GnpAppPacketType : uint16_t {
	APP_GSF = 1,
	APP_DISCONNECT = 2,
	APP_KEEP_ALIVE_REQ = 3,
	APP_KEEP_ALIVE_RES = 4,
	APP_COUNT_BASIC_,
	APP_MIN_CUSTOM_ = 100,
	APP_MAX_CUSTOM_ = 1 << 11,
};

#pragma pack(pop)
