// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/net/protocol.hpp>
#include <gongen/common/hash.hpp>
#include "packets.hpp"

using namespace gen;
using namespace gen::net::gnp;

const uint32_t GNP_MAGICS[] = {
	[PHASE_STATUS] = GEN_FOURCC_STR("GNPs"),
	[PHASE_HANDSHAKE] = GEN_FOURCC_STR("GNPh"),
	[PHASE_APPLICATION] = GEN_FOURCC_STR("GNPa"),
};

ProtocolPhase gen::net::gnp::read_phase(DataSource& data) {
	uint32_t magic;
	if (data.read(&magic, sizeof(uint32_t)) < 4) return PHASE_UNKNOWN;
	for (uint8_t phase = PHASE_STATUS; phase < PHASE_COUNT_; phase++) {
		if (magic == GNP_MAGICS[phase]) return (ProtocolPhase) phase;
	}
	return PHASE_UNKNOWN;
}
void gen::net::gnp::handle_phase(DataSource& data, ProtocolPhase phase) {
	ProtocolPhase got = read_phase(data);
	if (got != phase) throw NetProtocolException::createf("Expected phase %hhu, got %hhu.", phase, got);
}

template<typename T>
static inline T* read_packet(DataSource& packet, ByteBuffer& peer_buf, uint32_t max_size, bool read_phase, ProtocolPhase phase, uint8_t** ptr, uint8_t** end, bool complex = false) {
	uint32_t size;
	uint32_t type = 0;
	uint32_t min_size = !complex ? sizeof(T) : 0;
	if (peer_buf.empty()) {
		if (read_phase) handle_phase(packet, phase);
		// TODO check if readable before subsequent reads
		uint32_t size_field;
		packet.read(&size_field, sizeof(uint32_t));
		if (complex) {
			GnpHeader header = {};
			header.size = size_field;
			type = header.complex.type;
			size = header.complex.size;
			if (type == 0) throw NetPacketException("Received packet with invalid type.");
		} else {
			size = size_field;
		}
		if (size < min_size || size > max_size) {
			peer_buf.clear(true);
			throw NetPacketException::createf("Packet size exceeds limit (checked %u < %u < %u).", min_size, size, max_size);
		}
		peer_buf.add(&size_field, sizeof(uint32_t));
		peer_buf.reserve_capacity(size);
	} else {
		size = *((uint32_t*) peer_buf.ptr(0)) - (peer_buf.size() - sizeof(uint32_t));
		if (complex) {
			GnpHeader header = {};
			header.size = size;
			type = header.complex.type;
			size = header.complex.size;
		}
	}
	if (!complex || size > 0) {
		uint32_t read;
		do {
			read = packet.read(peer_buf.ptr(peer_buf.size()), size);
			peer_buf.resize(peer_buf.size() + read);
			size -= read;
		} while (read != 0 && size > 0);
		if (size > 0) return nullptr;
	}
	size = peer_buf.size() - sizeof(uint32_t);
	*ptr = peer_buf.ptr(sizeof(uint32_t) + min_size);
	*end = *ptr + size - min_size;
	if (complex) return (T*) (uintptr_t) type;
	return (T*) peer_buf.ptr(sizeof(uint32_t));
}

StatusSeeker::StatusSeeker() : res_protocol_info(gen::no_hash), res_server_info(gen::no_hash), res_custom(gen::no_hash) {
}
template<typename T>
static inline Response<T>& status_check_req(ByteBuffer& peer_buf, GnpStatusResponse* base, uint32_t min_size, HashMap<uint16_t, Response<T>, 8>& map, uint8_t** res_ptr, uint8_t** res_end) {
	if (!map.contains(base->serial)) {
		peer_buf.clear();
		throw NetProtocolException("Received status protocol response for unknown (possibly already handled) request.");
	}
	Response<T>& res = map.get(base->serial);
	if (base->error != ERROR_NONE) {
		if (base->error == ERROR_UNSUPPORTED) res = Response<T>(RESPONSE_UNSUPPORTED);
		else res = Response<T>(RESPONSE_ERROR);
		peer_buf.clear();
		return res;
	}
	if ((*res_end - *res_ptr) < min_size) {
		peer_buf.clear();
		throw NetPacketException("Status response is too short.");
	}
	if (res.is_done()) {
		peer_buf.clear();
		throw NetProtocolException("Received status response for already handled request.");
	}
	*res_ptr += min_size;
	return res;
}
bool StatusSeeker::handle(DataSource& response, WritableDataSource& out, ByteBuffer& peer_buf, uint32_t max_size, bool read_phase) {
	uint8_t* res_ptr;
	uint8_t* res_end;
	auto* base = read_packet<GnpStatusResponse>(response, peer_buf, max_size, read_phase, PHASE_STATUS, &res_ptr, &res_end);
	if (!base) return false;
	if (base->type == REQ_PROTOCOL) {
		Response<ProtocolInfo>& res = status_check_req(peer_buf, base, sizeof(GnpStatusResponseProtocol) - sizeof(GnpStatusResponse), this->res_protocol_info, &res_ptr, &res_end);
		if (res.is_done()) return true;
		auto* packet = (GnpStatusResponseProtocol*) base;
		ProtocolInfo info;
		info.protocol_version = packet->protocol_version;
		// TODO
		res = Response<ProtocolInfo>(info);
	} else if (base->type == REQ_INFO) {
		Response<ServerInfo>& res = status_check_req(peer_buf, base, sizeof(GnpStatusResponseInfo) - sizeof(GnpStatusResponse), this->res_server_info, &res_ptr, &res_end);
		if (res.is_done()) return true;
		auto* packet = (GnpStatusResponseInfo*) base;
		ServerInfo info;
		info.user_limit = packet->user_limit;
		if (packet->name_len > res_end - res_ptr) {
			peer_buf.clear();
			throw NetProtocolException::createf("Packet declares %hhu long name but only %zu bytes left.", packet->name_len, res_end - res_ptr);
		}
		info.name.append((char*) res_ptr, packet->name_len);
		res_ptr += packet->name_len;
		if (res_ptr < res_end) {
			info.description.append((char*) res_ptr, res_end - res_ptr);
		}
		res = Response<ServerInfo>(info);
	} else if (base->type == REQ_CUSTOM) {
		Response<ByteBuffer>& res = status_check_req(peer_buf, base, 0, this->res_custom, &res_ptr, &res_end);
		if (res.is_done()) return true;
		ByteBuffer data_buf(res_end - res_ptr);
		data_buf.add(res_ptr, res_end - res_ptr);
		res = Response<ByteBuffer>(std::move(data_buf));
	} else {
		peer_buf.clear();
		throw NetProtocolException::createf("Received status response of unsupported type %u.", base->type);
	}
	peer_buf.clear();
	return true;
}

uint16_t StatusSeeker::request_all(WritableDataSource& request, double timeout_sec) {
	GnpHeader header = {};
	header.magic = GNP_MAGICS[PHASE_STATUS];
	header.size = sizeof(GnpStatusRequest);
	request.write(&header, sizeof(header));
	GnpStatusRequest req;
	req.type = REQ_ALL;
	req.serial = this->next_serial;
	request.write(&req, sizeof(req));
	this->next_serial = this->enqueue_req(this->res_protocol_info, timeout_sec);
	return this->enqueue_req(this->res_server_info, timeout_sec);
}

uint16_t StatusSeeker::request_protocol_info(WritableDataSource& request, double timeout_sec) {
	GnpHeader header = {};
	header.magic = GNP_MAGICS[PHASE_STATUS];
	header.size = sizeof(GnpStatusRequest);
	request.write(&header, sizeof(header));
	GnpStatusRequest req;
	req.type = REQ_PROTOCOL;
	req.serial = this->next_serial;
	request.write(&req, sizeof(req));
	return this->enqueue_req(this->res_protocol_info, timeout_sec);
}
Response<ProtocolInfo> StatusSeeker::get_protocol_info(uint16_t req) {
	return this->dequeue_res(this->res_protocol_info, req);
}

uint16_t StatusSeeker::request_server_info(WritableDataSource& request, double timeout_sec) {
	GnpHeader header = {};
	header.magic = GNP_MAGICS[PHASE_STATUS];
	header.size = sizeof(GnpStatusRequest);
	request.write(&header, sizeof(header));
	GnpStatusRequest req;
	req.type = REQ_INFO;
	req.serial = this->next_serial;
	request.write(&req, sizeof(req));
	return this->enqueue_req(this->res_server_info, timeout_sec);
}
Response<ServerInfo> StatusSeeker::get_server_info(uint16_t req) {
	return this->dequeue_res(this->res_server_info, req);
}

uint16_t StatusSeeker::request_custom(WritableDataSource& request, double timeout_sec, const char* id, uint16_t version) {
	size_t id_len = strlen(id);
	GnpHeader header = {};
	header.magic = GNP_MAGICS[PHASE_STATUS];
	header.size = sizeof(GnpStatusRequestCustom) + id_len;
	request.write(&header, sizeof(header));
	GnpStatusRequestCustom req;
	req.request.type = REQ_CUSTOM;
	req.request.serial = this->next_serial;
	req.version = version;
	request.write(&req, sizeof(req));
	request.write(id, id_len);
	return this->enqueue_req(this->res_custom, timeout_sec);
}
Response<ByteBuffer> StatusSeeker::get_custom(uint16_t req) {
	return this->dequeue_res(this->res_custom, req);
}

StatusHandler::StatusHandler() : custom([](const CustomStatusId& id) { return gen::hash(id.name) + id.version; }) {
}
template<typename T>
static inline void status_send_response(WritableDataSource& response, const T& res, uint32_t appendix_size = 0) {
	GnpHeader header = {};
	header.magic = GNP_MAGICS[PHASE_STATUS];
	header.size = sizeof(T) + appendix_size;
	response.write(&header, sizeof(header));
	response.write(&res, sizeof(res));
}
template<typename T>
static inline T* status_cast_req(ByteBuffer& peer_buf, GnpStatusRequest* base, uint8_t** req_ptr, uint8_t** req_end) {
	*req_ptr -= sizeof(GnpStatusRequest); // back to the beginning (base)
	if (*req_end - *req_ptr < sizeof(T)) {
		peer_buf.clear();
		throw NetPacketException("Status protocol request is too short.");
	}
	*req_ptr += sizeof(T); // pass base (again) + the rest of the request
	return (T*) base;
}
void (*REQUEST_HANDLERS[REQ_COUNT_])(StatusHandler* handler, GnpStatusResponse& res_base, WritableDataSource& response, GnpStatusRequest* base, ByteBuffer& peer_buf, uint8_t* req_ptr, uint8_t* req_end) {
	[REQ_PROTOCOL] = [](StatusHandler* handler, GnpStatusResponse& res_base, WritableDataSource& response, GnpStatusRequest* base, ByteBuffer& peer_buf, uint8_t* req_ptr, uint8_t* req_end) {
		GnpStatusResponseProtocol res;
		res.response = res_base;
		res.protocol_version = VERSION_MINOR;
		status_send_response(response, res);
	},
	[REQ_INFO] = [](StatusHandler* handler, GnpStatusResponse& res_base, WritableDataSource& response, GnpStatusRequest* base, ByteBuffer& peer_buf, uint8_t* req_ptr, uint8_t* req_end) {
		GnpStatusResponseInfo res;
		res.response = res_base;
		res.user_limit = handler->server_info.user_limit;
		res.name_len = GEN_NUM_MIN(handler->server_info.name.size(), UINT8_MAX);
		status_send_response(response, res, res.name_len + handler->server_info.description.size());
		response.write(handler->server_info.name.c_str(), res.name_len);
		response.write(handler->server_info.description.c_str(), handler->server_info.description.size());
	},
	[REQ_CUSTOM] = [](StatusHandler* handler, GnpStatusResponse& res_base, WritableDataSource& response, GnpStatusRequest* base, ByteBuffer& peer_buf, uint8_t* req_ptr, uint8_t* req_end) {
		auto* req = status_cast_req<GnpStatusRequestCustom>(peer_buf, base, &req_ptr, &req_end);
		String name((char*) req_ptr, req_end - req_ptr);
		CustomStatusId id = { name, req->version };
		if (!handler->custom.contains(id)) {
			res_base.error = ERROR_UNSUPPORTED;
			status_send_response(response, res_base);
			return;
		}
		ByteBuffer& response_data = handler->custom.get(id);
		status_send_response(response, res_base, response_data.size());
		response.write(response_data.ptr(), response_data.size());
	},
	[REQ_ALL] = [](StatusHandler* handler, GnpStatusResponse& res_base, WritableDataSource& response, GnpStatusRequest* base, ByteBuffer& peer_buf, uint8_t* req_ptr, uint8_t* req_end) {
		res_base.type = REQ_PROTOCOL;
		REQUEST_HANDLERS[REQ_PROTOCOL](handler, res_base, response, base, peer_buf, req_ptr, req_end);
		res_base.type = REQ_INFO;
		REQUEST_HANDLERS[REQ_INFO](handler, res_base, response, base, peer_buf, req_ptr, req_end);
	},
};
bool StatusHandler::handle(DataSource& request, WritableDataSource& response, ByteBuffer& peer_buf, uint32_t max_size, bool read_phase) {
	uint8_t* req_ptr;
	uint8_t* req_end;
	auto* base = read_packet<GnpStatusRequest>(request, peer_buf, max_size, read_phase, PHASE_STATUS, &req_ptr, &req_end);
	if (!base) return false;
	GnpStatusResponse res_base;
	res_base.type = base->type;
	res_base.serial = base->serial;
	if (base->type < REQ_COUNT_ && REQUEST_HANDLERS[base->type] != nullptr) {
		REQUEST_HANDLERS[base->type](this, res_base, response, base, peer_buf, req_ptr, req_end);
	} else {
		res_base.error = ERROR_UNSUPPORTED;
		status_send_response(response, res_base);
	}
	peer_buf.clear();
	return true;
}

void HandshakeGuest::init(WritableDataSource& request) {
	GnpHeader header = {};
	header.magic = GNP_MAGICS[PHASE_HANDSHAKE];
	header.size = sizeof(GnpHandshakeInit);
	request.write(&header, sizeof(header));
	GnpHandshakeInit req = {};
	req.packet.type = HS_INIT;
	req.protocol_version = VERSION_MINOR;
	request.write(&req, sizeof(req));
}
bool (*HS_GUEST_HANDLERS[HS_COUNT_])(HandshakeGuest* handler, GnpHandshakePacket& res_base, DataSource& in, WritableDataSource& out, GnpHandshakePacket* base, ByteBuffer& peer_buf, uint8_t* req_ptr, uint8_t* req_end) {
	[HS_CANCEL] = [](HandshakeGuest* handler, GnpHandshakePacket& res_base, DataSource& in, WritableDataSource& out, GnpHandshakePacket* base, ByteBuffer& peer_buf, uint8_t* req_ptr, uint8_t* req_end) {
		peer_buf.clear();
		throw NetDisconnectException("Host cancelled handshake.");
		return false;
	},
	[HS_COMPLETE] = [](HandshakeGuest* handler, GnpHandshakePacket& res_base, DataSource& in, WritableDataSource& out, GnpHandshakePacket* base, ByteBuffer& peer_buf, uint8_t* req_ptr, uint8_t* req_end) {
		return true;
	},
};
bool HandshakeGuest::handle(DataSource& in, WritableDataSource& out, ByteBuffer& peer_buf, uint32_t max_size, bool read_phase) {
	uint8_t* packet_ptr;
	uint8_t* packet_end;
	auto* base = read_packet<GnpHandshakePacket>(in, peer_buf, max_size, read_phase, PHASE_HANDSHAKE, &packet_ptr, &packet_end);
	if (!base) return false;
	GnpHandshakePacket res_base = {};
	if (base->type >= HS_COUNT_ || HS_GUEST_HANDLERS[base->type] == nullptr) {
		GnpHeader header = {};
		header.magic = GNP_MAGICS[PHASE_HANDSHAKE];
		header.size = sizeof(GnpHandshakePacket);
		out.write(&header, sizeof(header));
		res_base.type = HS_CANCEL;
		out.write(&res_base, sizeof(res_base));
		peer_buf.clear();
		throw NetProtocolException::createf("Unsupported handshake packet type %hhu.", base->type);
	}
	res_base.type = base->type;
	bool ret = HS_GUEST_HANDLERS[base->type](this, res_base, in, out, base, peer_buf, packet_ptr, packet_end);
	peer_buf.clear();
	return ret;
}
bool (*HS_HOST_HANDLERS[HS_COUNT_])(HandshakeHost* handler, GnpHandshakePacket& res_base, DataSource& in, WritableDataSource& out, GnpHandshakePacket* base, ByteBuffer& peer_buf, uint8_t* req_ptr, uint8_t* req_end) {
	[HS_INIT] = [](HandshakeHost* handler, GnpHandshakePacket& res_base, DataSource& in, WritableDataSource& out, GnpHandshakePacket* base, ByteBuffer& peer_buf, uint8_t* req_ptr, uint8_t* req_end) {
		GnpHeader header = {};
		header.magic = GNP_MAGICS[PHASE_HANDSHAKE];
		header.size = sizeof(GnpHandshakePacket);
		out.write(&header, sizeof(header));
		res_base.type = HS_COMPLETE;
		out.write(&res_base, sizeof(res_base));
		return true;
	},
	[HS_CANCEL] = [](HandshakeHost* handler, GnpHandshakePacket& res_base, DataSource& in, WritableDataSource& out, GnpHandshakePacket* base, ByteBuffer& peer_buf, uint8_t* req_ptr, uint8_t* req_end) {
		peer_buf.clear();
		throw NetDisconnectException("Guest cancelled handshake.");
		return false;
	},
};
bool HandshakeHost::handle(DataSource& in, WritableDataSource& out, ByteBuffer& peer_buf, uint32_t max_size, bool read_phase) {
	uint8_t* packet_ptr;
	uint8_t* packet_end;
	auto* base = read_packet<GnpHandshakePacket>(in, peer_buf, max_size, read_phase, PHASE_HANDSHAKE, &packet_ptr, &packet_end);
	if (!base) return false;
	GnpHandshakePacket res_base = {};
	if (base->type >= HS_COUNT_ || HS_HOST_HANDLERS[base->type] == nullptr) {
		GnpHeader header = {};
		header.magic = GNP_MAGICS[PHASE_HANDSHAKE];
		header.size = sizeof(GnpHandshakePacket);
		out.write(&header, sizeof(header));
		res_base.type = HS_CANCEL;
		out.write(&res_base, sizeof(res_base));
		peer_buf.clear();
		throw NetProtocolException::createf("Unsupported handshake packet type %hhu.", base->type);
	}
	res_base.type = base->type;
	bool ret = HS_HOST_HANDLERS[base->type](this, res_base, in, out, base, peer_buf, packet_ptr, packet_end);
	peer_buf.clear();
	return ret;
}

namespace gen::net::gnp {
const uint16_t APP_CUSTOM_MIN = APP_MIN_CUSTOM_;
const uint16_t APP_CUSTOM_MAX = APP_MAX_CUSTOM_;
}
ApplicationHandler::ApplicationHandler()
		: keep_alive_info(gen::no_hash)
#ifdef GEN_MOD_GSF
		, gsf_schemas([](const gsf::Descriptor& desc) { return gen::hash(desc.to_string()); })
		, gsf_handlers([](const gsf::Descriptor& desc) { return gen::hash(desc.to_string()); })
#endif
		, custom_handlers(gen::no_hash) {
}
void ApplicationHandler::disconnect(WritableDataSource& out, const String& comment) {
	GnpHeader header = {};
	header.magic = GNP_MAGICS[PHASE_APPLICATION];
	header.complex.size = comment.size();
	header.complex.type = APP_DISCONNECT;
	out.write(&header, sizeof(header));
	out.write(comment.c_str(), comment.size());
}
uint16_t ApplicationHandler::req_keep_alive(WritableDataSource& out, double timeout_sec) {
	GnpHeader header = {};
	header.magic = GNP_MAGICS[PHASE_APPLICATION];
	header.complex.size = sizeof(this->next_serial);
	header.complex.type = APP_KEEP_ALIVE_REQ;
	out.write(&header, sizeof(header));
	out.write(&this->next_serial, header.complex.size);
	KeepAliveRequest& request = this->keep_alive_info[this->next_serial];
	request.response = Response<double>(RESPONSE_PENDING);
	if (timeout_sec > 0.0) {
		request.response.timeout = time::get_time().add(timeout_sec);
	}
	return this->next_serial++;
}
Response<double> ApplicationHandler::get_keep_alive(uint16_t serial) {
	KeepAliveRequest& request = this->keep_alive_info[serial];
	if (request.response.timeout.t != 0 && !request.response.is_done() && time::get_time() > request.response.timeout) {
		request.response = Response<double>(RESPONSE_TIMEOUT);
	}
	Response<double> res = request.response;
	if (request.response.is_done()) {
		this->keep_alive_info.remove(serial);
	}
	return res;
}
void ApplicationHandler::res_keep_alive(WritableDataSource& source, uint16_t serial) {
	if (!this->keep_alive_info.contains(serial)) return; // ignore bad serial numbers
	KeepAliveRequest info = this->keep_alive_info[serial];
	if (info.response.is_done() || info.source != &source) return; // ignore old serial numbers
	info.response = Response<double>((time::get_time() - info.time).sec());
}
void ApplicationHandler::gsf(WritableDataSource& out, const gsf::Article& article) {
#ifdef GEN_MOD_GSF
	ByteBufferDataSource packet; // TODO max size
	packet.buffer.resize(sizeof(GnpHeader));
	packet.seek(sizeof(GnpHeader));
	gsf::Article::bin_write(article, &packet);
	auto* header = (GnpHeader*) packet.buffer.ptr();
	header->magic = GNP_MAGICS[PHASE_APPLICATION];
	header->complex.type = APP_GSF;
	header->complex.size = packet.cursor() - sizeof(GnpHeader);
	out.write(packet.buffer.ptr(), packet.buffer.size());
#else
	throw DepModuleException("This function requires the GSF module.");
#endif
}
void ApplicationHandler::custom(WritableDataSource& out, uint16_t type, size_t size) {
	if (type < APP_CUSTOM_MIN || type > APP_CUSTOM_MAX) {
		throw ArgumentException::createf("Custom packet type id must be between %hu and %hu.", APP_CUSTOM_MIN, APP_CUSTOM_MAX);
	}
	GnpHeader header = {};
	header.magic = GNP_MAGICS[PHASE_APPLICATION];
	header.complex.size = size;
	header.complex.type = type;
	out.write(&header, sizeof(header));
}
void (*APP_HANDLERS[APP_COUNT_BASIC_])(ApplicationHandler* handler, DataSource& in, WritableDataSource& out, ByteBuffer& peer_buf, uint8_t* ptr, uint8_t* end) {
#ifdef GEN_MOD_GSF
	[APP_GSF] = [](ApplicationHandler* handler, DataSource& in, WritableDataSource& out, ByteBuffer& peer_buf, uint8_t* ptr, uint8_t* end) {
		gsf::Article article(std::move(gsf::Article::bin_read_with_desc(handler, [](void* user, const gsf::Descriptor& desc) {
			auto* handler = (ApplicationHandler*) user;
			return handler->gsf_schemas.get(desc);
		}, ptr, end - ptr)));
		auto* callback = article.get_schema().impl ? handler->gsf_handlers.get(article.get_schema().get_descriptor(), nullptr) : 0;
		if (!callback) {
			callback = handler->gsf_handler_other;
			if (!callback) {
				peer_buf.clear();
				if (article.get_schema().impl) throw NetProtocolException::createf("Unsupported GSF packet descriptor '%s'.", article.get_schema().get_descriptor().to_string().c_str());
				else throw NetProtocolException::createf("Unsupported GSF packet (schemaless).");
			}
		}
		callback(handler->user, article, out, peer_buf);
	},
#endif
	[APP_DISCONNECT] = [](ApplicationHandler* handler, DataSource& in, WritableDataSource& out, ByteBuffer& peer_buf, uint8_t* ptr, uint8_t* end) {
		peer_buf.clear();
		throw NetDisconnectException::createf("%.*s", (int) (end - ptr), ptr);
	},
	[APP_KEEP_ALIVE_REQ] = [](ApplicationHandler* handler, DataSource& in, WritableDataSource& out, ByteBuffer& peer_buf, uint8_t* ptr, uint8_t* end) {
		GnpHeader header = {};
		header.magic = GNP_MAGICS[PHASE_APPLICATION];
		header.complex.size = sizeof(uint16_t);
		header.complex.type = APP_KEEP_ALIVE_RES;
		out.write(&header, sizeof(header));
		out.write(ptr, end - ptr);
	},
	[APP_KEEP_ALIVE_RES] = [](ApplicationHandler* handler, DataSource& in, WritableDataSource& out, ByteBuffer& peer_buf, uint8_t* ptr, uint8_t* end) {
		if (end - ptr < sizeof(uint16_t)) {
			peer_buf.clear();
			throw NetProtocolException("Keep alive response is missing serial number.");
		}
		uint16_t serial = *(uint16_t*) ptr;
		handler->res_keep_alive(out, serial);
	},
};
bool ApplicationHandler::handle(DataSource& in, WritableDataSource& out, ByteBuffer& peer_buf, uint32_t max_size, bool read_phase) {
	uint8_t* packet_ptr;
	uint8_t* packet_end;
	auto* packet_info = read_packet<uint32_t>(in, peer_buf, max_size, read_phase, PHASE_APPLICATION, &packet_ptr, &packet_end, true);
	if (!packet_info) return false;
	uint32_t type = (uintptr_t) packet_info;
	if (type >= APP_MIN_CUSTOM_ && type <= APP_MAX_CUSTOM_ && (this->custom_handlers.contains(type) || this->custom_handler_other)) {
		auto* callback = this->custom_handlers.get(type, nullptr);
		if (callback) callback(this->user, packet_ptr, packet_end - packet_ptr, out, peer_buf);
		else this->custom_handler_other(this->user, type, packet_ptr, packet_end - packet_ptr, out, peer_buf);
	} else if (type < APP_COUNT_BASIC_ && APP_HANDLERS[type] != nullptr) {
		APP_HANDLERS[type](this, in, out, peer_buf, packet_ptr, packet_end);
	} else {
		peer_buf.clear();
		throw NetProtocolException::createf("Unsupported packet type %hhu.", type);
	}
	peer_buf.clear();
	return true;
}
