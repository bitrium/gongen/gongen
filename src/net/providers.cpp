// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/net/providers.hpp>
#include <gongen/common/logger.hpp>
#include <gongen/common/hash.hpp>

using namespace gen;
using namespace gen::net;

struct thread_data {
	double delay;
	size_t update_limit;
	NetworkProvider* provider;
};
static void* worker(void* user) {
	auto* data = (struct thread_data*) user;
	while (data->provider->is_thread_running()) {
		bool update = data->provider->wait(data->delay);
		if (!update) continue;
		if (!data->provider) break;
		try {
			data->provider->update(data->update_limit);
		} catch (const gen::Exception& exception) {
			gen::log::errorf("Unhandled exception during update in network provider thread: %s", exception.what());
		}
	}
	gen::free(data);
	return data;
}
NetworkProvider::~NetworkProvider() {
	this->join_thread();
}
void NetworkProvider::start_thread(double delay, size_t update_limit) {
	this->join_thread();
	auto* data = gen::talloc<struct thread_data>();
	data->delay = delay;
	data->update_limit = update_limit;
	data->provider = this;
	thread_stop_flag.store(false);
	thread = new Thread(worker, data, "net_provider");
	this->thread_data = data;
}
void NetworkProvider::stop_thread() {
	auto* data = (struct thread_data*) this->thread_data;
	if (!data || !this->thread) return;
	thread_stop_flag.store(true);
	this->thread_data = nullptr;
}
void NetworkProvider::join_thread() {
	auto* data = (struct thread_data*) this->thread_data;
	if (!data || !this->thread) return;
	thread_stop_flag.store(true);
	this->thread_data = nullptr;
	this->thread->join();
	delete this->thread;
	this->thread = nullptr;
}

struct ClientImpl {
	Ptr<Communication> comm;
	void* socket = nullptr;
	ByteBuffer peer_buf;
	uint32_t status_serial = 0;
	gnp::Response<gnp::ServerInfo> last_status_info;
	gnp::StatusSeeker hs;
	gnp::HandshakeGuest hh;
	gnp::ApplicationHandler ha;
	gnp::PacketHandler* h = nullptr;
};
static inline void on_connect(ClientProvider* provider, ClientImpl* impl) {
	impl->h = &impl->ha;
	auto* event = new ConnectionEvent(provider);
	event->connected = true;
	event->client_id = 0;
	event->endpoint = provider->server_address;
	gen::event::system()->occur(event);
	log::infof("Connected to server (%s)", provider->server_address.to_string().c_str());
}
static inline void on_disconnect(ClientProvider* provider, ClientImpl* impl, const String& reason) {
	impl->comm->disconnect(impl->socket);
	impl->socket = nullptr;
	impl->h = nullptr;
	auto* event = new ConnectionEvent(provider);
	event->connected = false;
	event->client_id = 0;
	event->endpoint = provider->server_address;
	gen::event::system()->occur(event);
	log::infof("Disconnected from server (%s): %s", provider->server_address.to_string().c_str(), reason.c_str());
}
ClientProvider::ClientProvider(gen::Ptr<Communication> communication, const ip::Endpoint& serer_address) : server_address(serer_address) {
	auto* impl = new ClientImpl();
	impl->comm = communication.transfer();
	impl->ha.user = this;
	impl->ha.custom_handler_other = [](void* user, uint8_t type, uint8_t* data, uint32_t size, WritableDataSource& out, ByteBuffer& peer_buf) {
		auto* event = new PacketCustom(user);
		event->client_id = 0;
		event->type_id = type;
		event->data.add(data, size);
		gen::event::system()->occur(event);
	};
#ifdef GEN_MOD_GSF
	impl->ha.gsf_handler_other = [](void* user, gsf::Article gsf, WritableDataSource& out, ByteBuffer& peer_buf) {
		auto* event = new PacketGsf(user);
		event->client_id = 0;
		event->gsf = std::move(gsf);
		gen::event::system()->occur(event);
	};
#endif
	this->impl = impl;
}
ClientProvider::~ClientProvider() {
	this->join_thread();
	auto* impl = (ClientImpl*) this->impl;
	if (impl->socket) impl->comm->disconnect(impl->socket);
	delete impl;
}
size_t ClientProvider::update(size_t limit) {
	auto* impl = (ClientImpl*) this->impl;
	if (!impl->socket || !impl->h) return 0;
	size_t count = 0;
	while (impl->comm->check(impl->socket, 0)) {
		if (count >= limit) break;
		try {
			if (!impl->h->handle(impl->comm->source(impl->socket), impl->comm->source(impl->socket), impl->peer_buf, this->max_packet_size)) continue;
		} catch (const NetDisconnectException& exception) {
			impl->comm->disconnect(impl->socket);
			on_disconnect(this, impl, exception.message); // FIXME what about handshake?
			break;
		} catch (const NetworkException& exception) {
			log::errorf("Error during communication with server: %s", exception.what());
			// TODO error strategies (simple API class)?
			continue;
		}
		if (impl->h == &impl->hs) {
			impl->comm->disconnect(impl->socket);
			impl->socket = nullptr;
			impl->h = nullptr;
			break;
		} else if (impl->h == &impl->hh) {
			on_connect(this, impl);
		}
		count++;
	}
	return count;
}
size_t ClientProvider::wait(double timeout) {
	auto* impl = (ClientImpl*) this->impl;
	if (!impl->socket) return 0;
	return impl->comm->check(impl->socket, timeout);
}
void ClientProvider::status(double timeout) {
	auto* impl = (ClientImpl*) this->impl;
	if (impl->h && impl->h != &impl->hs) return;
	impl->socket = impl->comm->connect(this->server_address);
	impl->status_serial = impl->hs.request_server_info(impl->comm->source(impl->socket), timeout);
	impl->h = &impl->hs;
}
gnp::Response<gnp::ServerInfo> ClientProvider::status_info() {
	auto* impl = (ClientImpl*) this->impl;
	gnp::Response<gnp::ServerInfo> info = impl->hs.get_server_info(impl->status_serial);
	if ((!info.is_done() || info.state() == gnp::RESPONSE_UNKNOWN) && impl->last_status_info.is_done()) return impl->last_status_info;
	impl->last_status_info = info;
	return info;
}
bool ClientProvider::is_connected() {
	auto* impl = (ClientImpl*) this->impl;
	return impl->h == &impl->ha;
}
void ClientProvider::connect() {
	auto* impl = (ClientImpl*) this->impl;
	if (impl->h == &impl->hs) {
		if (impl->socket) impl->comm->disconnect(impl->socket);
		impl->socket = nullptr;
	} else if (impl->h) return;
	impl->socket = impl->comm->connect(this->server_address);
	impl->hh.init(impl->comm->source(impl->socket));
	impl->h = &impl->hh;
}
void ClientProvider::disconnect(const String& reason) {
	auto* impl = (ClientImpl*) this->impl;
	if (!impl->socket) return;
	if (impl->h == &impl->ha) {
		impl->ha.disconnect(impl->comm->source(impl->socket), reason);
		on_disconnect(this, impl, reason);
		return;
	}
	impl->comm->disconnect(impl->socket);
	impl->socket = nullptr;
	impl->h = nullptr;
}
void ClientProvider::send(uint32_t type, const void* data, size_t size) {
	if (!this->is_connected()) throw RuntimeException("Not connected to server.");
	auto* impl = (ClientImpl*) this->impl;
	impl->ha.custom(impl->comm->source(impl->socket), type, size);
	impl->comm->source(impl->socket).write(data, size);
}
#ifdef GEN_MOD_GSF
void ClientProvider::send(const gsf::Article& gsf) {
	if (!this->is_connected()) throw RuntimeException("Not connected to server.");
	auto* impl = (ClientImpl*) this->impl;
	impl->ha.gsf(impl->comm->source(impl->socket), gsf);
}
#endif

struct PeerClient {
	uint32_t id = 0;
	void* socket = nullptr;
	ip::Endpoint endpoint;
	ByteBuffer peer_buf;
	void* user;
};
struct PeerGuest {
	void* socket;
	ByteBuffer peer_buf;
};
struct ServerImpl {
	ServerProvider* provider;
	Ptr<Communication> comm;
	void* socket = nullptr;
	uint32_t next_client_id = 1;
	HashMap<uint32_t, PeerClient, 64> clients = HashMap<uint32_t, PeerClient, 64>(gen::no_hash);
	HashMap<void*, PeerClient*, 64> clients_by_sockets = HashMap<void*, PeerClient*, 64>(gen::hash_pr);
	HashMap<void*, PeerGuest, 8> guests = HashMap<void*, PeerGuest, 8>(gen::hash_pr); // TODO reuse peer buffers
	Buffer<void*> sockets;
	gnp::StatusHandler hs;
	gnp::HandshakeHost hh;
	gnp::ApplicationHandler ha;
};
static inline void reindex_sockets(ServerImpl* impl) {
	impl->sockets.clear();
	if (impl->socket) impl->sockets.add(impl->socket);
	for (PeerClient& client : impl->clients) {
		impl->sockets.add(client.socket);
	}
	for (PeerGuest& guest : impl->guests) {
		impl->sockets.add(guest.socket);
	}
}
static inline PeerClient& on_connect(void* client_socket, ServerProvider* provider, ServerImpl* impl) {
	PeerClient& client = impl->clients.emplace(impl->next_client_id);
	client.id = impl->next_client_id++;
	client.socket = client_socket;
	client.endpoint = impl->comm->endpoint(client_socket);
	impl->clients_by_sockets.add(client.socket, &client);
	impl->sockets.add(client_socket);
	auto* event = new ConnectionEvent(provider);
	event->connected = true;
	event->client_id = client.id;
	event->endpoint = client.endpoint;
	gen::event::system()->occur(event);
	log::infof("Connected client #%u (%s)", client.id, client.endpoint.to_string().c_str());
	return client;
}
static inline void on_disconnect(PeerClient& client, ServerProvider* provider, ServerImpl* impl, const String& reason, bool reindex = true) {
	impl->comm->disconnect(client.socket);
	auto* event = new ConnectionEvent(provider);
	event->connected = false;
	event->client_id = client.id;
	event->endpoint = client.endpoint;
	gen::event::system()->occur(event);
	log::infof("Disconnected client #%u (%s): %s", client.id, client.endpoint.to_string().c_str(), reason.c_str());
	if (!reindex) return;
	impl->clients_by_sockets.remove_safe(client.socket);
	impl->clients.remove_safe(client.id);
	reindex_sockets(impl);
}
static inline void remove_guest(ServerImpl* impl, PeerGuest& guest) {
	impl->guests.remove(guest.socket);
	if (impl->sockets.get_last() == guest.socket) impl->sockets.remove_last();
	else reindex_sockets(impl);
}
ServerProvider::ServerProvider(gen::Ptr<Communication> communication, const ip::Endpoint& address) : address(address) {
	auto* impl = new ServerImpl();
	impl->provider = this;
	impl->comm = communication.transfer();
	impl->ha.user = impl;
	impl->ha.custom_handler_other = [](void* user, uint8_t type, uint8_t* data, uint32_t size, WritableDataSource& out, ByteBuffer& peer_buf) {
		auto* impl = (ServerImpl*) user;
		PeerClient* client = impl->clients_by_sockets.get(impl->comm->socket(out));
		auto* event = new PacketCustom(impl->provider);
		event->client_id = client->id;
		event->type_id = type;
		event->data.add(data, size);
		gen::event::system()->occur(event);
	};
#ifdef GEN_MOD_GSF
	impl->ha.gsf_handler_other = [](void* user, gsf::Article gsf, WritableDataSource& out, ByteBuffer& peer_buf) {
		auto* impl = (ServerImpl*) user;
		PeerClient* client = impl->clients_by_sockets.get(impl->comm->socket(out));
		auto* event = new PacketGsf(impl->provider);
		event->client_id = client->id;
		event->gsf = std::move(gsf);
		gen::event::system()->occur(event);
	};
#endif
	this->impl = impl;
}
ServerProvider::~ServerProvider() {
	this->join_thread();
	auto* impl = (ServerImpl*) this->impl;
	this->close();
	delete impl;
}
gnp::StatusHandler* ServerProvider::status() {
	auto* impl = (ServerImpl*) this->impl;
	return &impl->hs;
}
bool ServerProvider::is_open() {
	auto* impl = (ServerImpl*) this->impl;
	return impl->socket != nullptr;
}
void ServerProvider::open() {
	auto* impl = (ServerImpl*) this->impl;
	impl->socket = impl->comm->open(this->address);
	reindex_sockets(impl);
}
void ServerProvider::close() {
	if (!this->is_open()) return;
	auto* impl = (ServerImpl*) this->impl;
	// TODO also cancel handshakes
	for (PeerClient& client : impl->clients) {
		impl->ha.disconnect(impl->comm->source(client.socket));
		on_disconnect(client, this, impl, "Server closed.", false);
	}
	impl->clients.clear();
	impl->clients_by_sockets.clear();
	impl->sockets.clear(true);
	impl->comm->close(impl->socket);
	impl->socket = nullptr;
}
size_t ServerProvider::update(size_t limit) {
	auto* impl = (ServerImpl*) this->impl;
	if (!impl->socket) return 0;
	Buffer<void*> sockets = impl->comm->check(impl->sockets, 0);
	size_t count = 0;
	for (void* socket : sockets) {
		if (count >= limit) break;
		if (socket == impl->socket) {
			impl->sockets.add(impl->comm->accept(impl->socket)); // FIXME could be removed by a socket reindex if it doesn't send a packet fast enough
			count++;
			continue;
		}
		PeerClient* client = impl->clients_by_sockets.get(socket, nullptr);
		ip::Endpoint endpoint = client ? client->endpoint : impl->comm->endpoint(socket);
		WritableDataSource& source = impl->comm->source(socket);
		try {
			if (client) {
				if (!impl->ha.handle(source, source, client->peer_buf, this->max_packet_size)) continue;
			} else {
				PeerGuest& guest = impl->guests[socket]; // already added to sockets after accept; add to guests to keep it during socket reindexing
				guest.socket = socket; // in case it's new
				gnp::ProtocolPhase phase = gnp::read_phase(source);
				if (phase == gnp::PHASE_STATUS) {
					if (!impl->hs.handle(source, source, guest.peer_buf, this->max_packet_size, false)) continue;
					impl->comm->disconnect(socket);
				} else if (phase == gnp::PHASE_HANDSHAKE) {
					if (!impl->hh.handle(source, source, guest.peer_buf, this->max_packet_size, false)) continue;
					client = &on_connect(socket, this, impl);
				} else {
					remove_guest(impl, guest);
					throw NetProtocolException::createf("New client initialized connection with invalid phase %hhu.", phase);
				}
				// on both status and handshake complete
				remove_guest(impl, guest);
			}
		} catch (const NetDisconnectException& exception) {
			impl->comm->disconnect(socket);
			if (client) on_disconnect(*client, this, impl, exception.message);
			continue;
		} catch (const NetworkException& exception) {
			if (client) {
				log::warnf("Error during communication with client %u (%s): %s", client->id, endpoint.to_string().c_str(), exception.what());
				// TODO error strategies (simple API class)?
			} else {
				log::warnf("Error during communication with unregistered client (%s): %s", endpoint.to_string().c_str(), exception.what());
				impl->comm->disconnect(socket);
			}
			continue;
		}
		count++;
	}
	return 0;
}
size_t ServerProvider::wait(double timeout) {
	auto* impl = (ServerImpl*) this->impl;
	if (!impl->socket) return 0;
	return impl->comm->check(impl->sockets, timeout).size();
}
bool ServerProvider::is_connected(uint32_t client_id) {
	auto* impl = (ServerImpl*) this->impl;
	return impl->clients.contains(client_id);
}
void ServerProvider::disconnect(uint32_t client_id, const String& reason) {
	auto* impl = (ServerImpl*) this->impl;
	PeerClient& client = impl->clients.get(client_id);
	impl->ha.disconnect(impl->comm->source(client.socket), reason);
	on_disconnect(client, this, impl, reason);
}
void ServerProvider::send(uint32_t client_id, uint32_t type, const void* data, size_t size) {
	auto* impl = (ServerImpl*) this->impl;
	PeerClient& client = impl->clients.get(client_id);
	impl->ha.custom(impl->comm->source(client.socket), type, size);
	impl->comm->source(client.socket).write(data, size);
}
#ifdef GEN_MOD_GSF
void ServerProvider::send(uint32_t client_id, const gsf::Article& gsf) {
	auto* impl = (ServerImpl*) this->impl;
	PeerClient& client = impl->clients.get(client_id);
	impl->ha.gsf(impl->comm->source(client.socket), gsf);
}
#endif
ip::Endpoint ServerProvider::our_endpoint() {
	if (!this->is_open()) return this->address;
	auto* impl = (ServerImpl*) this->impl;
	return impl->comm->endpoint(impl->socket);
}
gen::ip::Endpoint ServerProvider::endpoint(uint32_t client_id) {
	auto* impl = (ServerImpl*) this->impl;
	return impl->clients.get(client_id).endpoint;
}
void** ServerProvider::user_data(uint32_t client_id) {
	auto* impl = (ServerImpl*) this->impl;
	return &impl->clients.get(client_id).user;
}
