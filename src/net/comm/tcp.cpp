// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/net/communication.hpp>
#include <gongen/common/containers/list.hpp>

using namespace gen;

class GEN_API CommunicationTcp : public net::Communication {
public:
	BufferList<tcp::Socket> sockets;

	WritableDataSource& source(void* socket) override {
		return *(tcp::Socket*) socket;
	}
	void* socket(WritableDataSource& source) override {
		return &source;
	}
	ip::Endpoint endpoint(void* socket) override {
		return ((tcp::Socket*) socket)->endpoint();
	}
	void* open(const ip::Endpoint& endpoint) override {
		return &this->sockets.emplace(endpoint, true);
	}
	void close(void* socket) override {
		auto* sock = (tcp::Socket*) socket;
		sock->close();
		GEN_NO_EXCEPT(this->sockets.remove_ptr(sock));
	}
	void* accept(void* socket) override {
		auto* sock = (tcp::Socket*) socket;
		return sock->accept(); // TODO add to sockets list
	}
	void* connect(const ip::Endpoint& endpoint) override {
		return &this->sockets.emplace(endpoint, false);
	}
	void disconnect(void* socket) override {
		return this->close(socket);
	}
	bool check(void* socket, double timeout) override {
		auto* sock = (tcp::Socket*) socket;
		return sock->check((uint32_t) (timeout * 1000.0));
	}
	Buffer<void*> check(const Buffer<void*>& sockets, double timeout) override {
		auto* check_sockets = (Buffer<void*> (*)(const Buffer<void*>& sockets, uint32_t timeout)) tcp::check_sockets;
		return check_sockets(sockets, (uint32_t) (timeout * 1000.0));
	}
};

Ptr<net::Communication> net::communication_tcp() {
	return { (net::Communication*) new CommunicationTcp() };
}
