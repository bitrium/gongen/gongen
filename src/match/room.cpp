// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include <gongen/match/room.hpp>
#include <gongen/gsf/bank.hpp>
#include <gongen/common/hash.hpp>
#include <gongen/common/logger.hpp>

using namespace gen;
using namespace gen::match;

#define IS_HOST 		0b00000001
#define GOT_INFO		0b00000100
#define GOT_SETTINGS	0b00001000
#define GOT_PERMS		0b00010000
#define GOT_MEMBERS		0b00100000
#define GOT_ALL			0b00111100

struct RoomImpl {
	uint8_t flags;
	uint32_t id = 0;
	event::Hopper* hopper;
	union {
		struct {
			net::ServerProvider* provider;
		} host;
		struct {
			net::ClientProvider* provider;
		} guest;
	};
	RoomInfo info;
	RoomSettings settings;
	Permissions perms;
	HashMap<uint32_t, Endpoint> endpoints = HashMap<uint32_t, Endpoint>(gen::no_hash);
};

Room::Room(net::ClientProvider* client) {
	auto* impl = new RoomImpl();
	impl->flags = 0;
	impl->hopper = event::hopper(event::type("net/gsf"), client);
	this->impl = impl;
	GEN_GSF_BANK(gongen)->schema("match/room/join");
}
Room::Room(net::ServerProvider* server) {
	auto* impl = new RoomImpl();
	impl->flags = IS_HOST;
	impl->hopper = event::hopper(event::type("net/gsf"), server);
	this->impl = impl;
}
Room::~Room() {
	auto* impl = (RoomImpl*) this->impl;
	delete impl;
}
struct PacketHandler {
	const char* schema;
	void (*as_host)(RoomImpl* impl, net::PacketGsf* packet);
	void (*as_guest)(RoomImpl* impl, net::PacketGsf* packet);
};
static gsf::Article info_to_gsf(const RoomInfo& info) {
	gsf::Article gsf(GEN_GSF_BANK(gongen)->schema("match/room/info"));
	gsf::Object gsf_info = gsf.access_root().encode_object();
	gsf_info.access_at_key("id")->encode_u32(info.id);
	gsf_info.access_at_key("name")->encode_str(info.name);
	gsf_info.access_at_key("desc")->encode_str(info.desc);
	return gsf;
}
static RoomInfo info_from_gsf(const gsf::Article& gsf) {
	RoomInfo info = {};
	gsf::Object gsf_info = gsf.access_root().decode_object();
	info.id = gsf_info.access_at_key("id")->decode_u32();
	info.name = gsf_info.access_at_key("name")->decode_str();
	info.desc = gsf_info.access_at_key("desc")->decode_str();
	return info;
}
static gsf::Article settings_to_gsf(const RoomSettings& settings) {
	gsf::Article gsf(GEN_GSF_BANK(gongen)->schema("match/room/settings"));
	gsf::Object gsf_settings = gsf.access_root().encode_object();
	gsf_settings.access_at_key("id")->encode_u16(settings.max_members);
	gsf_settings.access_at_key("name")->encode_u16(settings.max_endpoint_members);
	gsf_settings.access_at_key("desc")->encode_u16(settings.want_members);
	return gsf;
}
static RoomSettings settings_from_gsf(const gsf::Article& gsf) {
	RoomSettings settings = {};
	gsf::Object gsf_settings = gsf.access_root().decode_object();
	settings.max_members = gsf_settings.access_at_key("max_members")->decode_u16();
	settings.max_endpoint_members = gsf_settings.access_at_key("max_endpoint_members")->decode_u16();
	settings.want_members = gsf_settings.access_at_key("want_members")->decode_u16();
	return settings;
}
static gsf::Article endpoints_to_gsf(HashMap<uint32_t, Endpoint>& endpoints) {
	gsf::Article gsf(GEN_GSF_BANK(gongen)->schema("match/room/endpoints"));
	gsf::Object gsf_data = gsf.access_root().encode_object();
	gsf::List gsf_members = gsf_data.access_at_key("members")->encode_list(nullptr, endpoints.size());
	gsf::List gsf_endpoints = gsf_data.access_at_key("endpoints")->encode_list(nullptr, endpoints.size());
	for (const Endpoint& endpoint : endpoints) {
		for (const Member& member : endpoint.members) {
			gsf::Object gsf_member = gsf_members.add_element().encode_object();
			gsf_member.access_at_key("endpoint")->encode_u32(member.endpoint_id);
			gsf_member.access_at_key("name")->encode_str(member.name);
		}
		gsf::Object gsf_endpoint = gsf_endpoints.add_element().encode_object();
		gsf_endpoint.access_at_key("id")->encode_u32(endpoint.id);
		gsf_endpoint.access_at_key("rank")->encode_u32(endpoint.id);
	}
	return gsf;
}
static HashMap<uint32_t, Endpoint> endpoints_from_gsf(const gsf::Article& gsf) {
	gsf::Object gsf_data = gsf.access_root().decode_object();
	HashMap<uint32_t, Endpoint> endpoints(gen::no_hash);
	gsf::List gsf_endpoints = gsf_data.access_at_key("endpoints")->decode_list();
	for (size_t i = 0; i < gsf_endpoints.get_size(); i++) {
		gsf::Object gsf_endpoint = gsf_endpoints.get_element(i)->decode_object();
		Endpoint endpoint = {};
		endpoint.id = gsf_endpoint.access_at_key("id")->decode_u32();
		endpoint.rank = gsf_endpoint.access_at_key("rank")->decode_u32();
		endpoints.add(endpoint.id, endpoint);
	}
	gsf::List gsf_members = gsf_data.access_at_key("members")->decode_list();
	for (size_t i = 0; i < gsf_members.get_size(); i++) {
		gsf::Object gsf_member = gsf_members.get_element(i)->decode_object();
		Member member = {};
		member.endpoint_id = gsf_member.access_at_key("endpoint")->decode_u32();
		member.name = gsf_member.access_at_key("name")->decode_str();
		endpoints.get(member.endpoint_id).members.add(member);
	}
	return endpoints;
}
static gsf::Article perms_to_gsf(const Permissions& perms) {
	gsf::Article gsf(GEN_GSF_BANK(gongen)->schema("match/room/perms"));
	gsf::Map gsf_perms = gsf.access_root().encode_map(nullptr, 6);
	gsf_perms.add_element("manage_endpoints").encode_u16(perms.rank_manage_endpoints);
	gsf_perms.add_element("manage_members").encode_u16(perms.rank_manage_members);
	gsf_perms.add_element("change_settings").encode_u16(perms.rank_change_settings);
	gsf_perms.add_element("change_info").encode_u16(perms.rank_change_info);
	gsf_perms.add_element("start").encode_u16(perms.rank_start);
	gsf_perms.add_element("view_match").encode_u16(perms.rank_view_match);
	return gsf;
}
static inline uint16_t decode_perm(const Optional<gsf::Value>& val) {
	if (val.none()) return ranks::HOST;
	return val->decode_u16();
}
static Permissions perms_from_gsf(const gsf::Article& gsf) {
	Permissions perms = {};
	gsf::Map gsf_perms = gsf.access_root().decode_map();
	perms.rank_manage_endpoints = decode_perm(gsf_perms.get_value_at_key("manage_endpoints"));
	perms.rank_manage_members = decode_perm(gsf_perms.get_value_at_key("manage_members"));
	perms.rank_change_settings = decode_perm(gsf_perms.get_value_at_key("change_settings"));
	perms.rank_change_info = decode_perm(gsf_perms.get_value_at_key("change_info"));
	perms.rank_start = decode_perm(gsf_perms.get_value_at_key("start"));
	perms.rank_view_match = decode_perm(gsf_perms.get_value_at_key("view_match"));
	return perms;
}
static gsf::Article resp_to_gsf(const String& status = "ACCEPTED") {
	gsf::Article gsf(GEN_GSF_BANK(gongen)->schema("match/room/response"));
	gsf.access_root().encode_enum_str(nullptr, status);
	return gsf;
}
static String resp_from_gsf(const gsf::Article& gsf) {
	return gsf.access_root().decode_enum_str();
}

void host_on_join(RoomImpl* impl, net::PacketGsf* packet) {
	if (packet->gsf.access_root().decode_u32() != impl->id) return;
	impl->host.provider->send(packet->client_id, info_to_gsf(impl->info));
	impl->host.provider->send(packet->client_id, settings_to_gsf(impl->settings));
	impl->host.provider->send(packet->client_id, endpoints_to_gsf(impl->endpoints));
	impl->host.provider->send(packet->client_id, perms_to_gsf(impl->perms));
}
void guest_on_join(RoomImpl* impl, net::PacketGsf* packet) {
	impl->guest.provider->send(resp_to_gsf("UNSUPPORTED"));
}
void host_change_info(RoomImpl* impl, const RoomInfo& info) {
	impl->info = info;
	for (Endpoint& endpoint : impl->endpoints) {
		impl->host.provider->send(endpoint.id, info_to_gsf(info));
	}
}
void host_on_info(RoomImpl* impl, net::PacketGsf* packet) {
	if (impl->endpoints.get(packet->client_id).rank < impl->perms.rank_change_info) {
		impl->guest.provider->send(resp_to_gsf("UNPERMITTED"));
		return;
	}
	host_change_info(impl, info_from_gsf(packet->gsf));
}
void guest_on_info(RoomImpl* impl, net::PacketGsf* packet) {
	impl->info = info_from_gsf(packet->gsf);
	impl->flags |= GOT_INFO;
}
void host_change_settings(RoomImpl* impl, const RoomSettings& settings) {
	impl->settings = settings;
	for (Endpoint& endpoint : impl->endpoints) {
		impl->host.provider->send(endpoint.id, settings_to_gsf(settings));
	}
}
void host_on_settings(RoomImpl* impl, net::PacketGsf* packet) {
	if (impl->endpoints.get(packet->client_id).rank < impl->perms.rank_change_settings) {
		impl->guest.provider->send(resp_to_gsf("UNPERMITTED"));
		return;
	}
	host_change_settings(impl, settings_from_gsf(packet->gsf));
}
void guest_on_settings(RoomImpl* impl, net::PacketGsf* packet) {
	impl->settings = settings_from_gsf(packet->gsf);
	impl->flags |= GOT_SETTINGS;
}
void host_change_endpoints(RoomImpl* impl, HashMap<uint32_t, Endpoint>& endpoints) {
	impl->endpoints = endpoints;
	for (Endpoint& endpoint : impl->endpoints) {
		impl->host.provider->send(endpoint.id, endpoints_to_gsf(endpoints));
	}
}
void host_on_endpoints(RoomImpl* impl, net::PacketGsf* packet) {
	if (impl->endpoints.get(packet->client_id).rank < impl->perms.rank_manage_endpoints) {
		impl->guest.provider->send(resp_to_gsf("UNPERMITTED"));
		return;
	}
	HashMap<uint32_t, Endpoint> endpoints = endpoints_from_gsf(packet->gsf);
	host_change_endpoints(impl, endpoints);
}
void guest_on_endpoints(RoomImpl* impl, net::PacketGsf* packet) {
	impl->endpoints = endpoints_from_gsf(packet->gsf);
	impl->flags |= GOT_MEMBERS;
}
void host_change_perms(RoomImpl* impl, const Permissions& perms) {
	impl->perms = perms;
	for (Endpoint& endpoint : impl->endpoints) {
		impl->host.provider->send(endpoint.id, perms_to_gsf(perms));
	}
}
void host_on_perms(RoomImpl* impl, net::PacketGsf* packet) {
	impl->guest.provider->send(resp_to_gsf("UNPERMITTED"));
}
void guest_on_perms(RoomImpl* impl, net::PacketGsf* packet) {
	impl->perms = perms_from_gsf(packet->gsf);
	impl->flags |= GOT_PERMS;
}
void host_on_members(RoomImpl* impl, net::PacketGsf* packet) {
	// TODO
}
void guest_on_members(RoomImpl* impl, net::PacketGsf* packet) {
	impl->guest.provider->send(resp_to_gsf("UNSUPPORTED"));
}
void host_on_match(RoomImpl* impl, net::PacketGsf* packet) {
	if (impl->endpoints.get(packet->client_id).rank < impl->perms.rank_start) {
		impl->guest.provider->send(resp_to_gsf("UNPERMITTED"));
		return;
	}
	// TODO
}
void guest_on_match(RoomImpl* impl, net::PacketGsf* packet) {
	// TODO
}
void host_on_response(RoomImpl* impl, net::PacketGsf* packet) {
	// host ignores responses
}
void guest_on_response(RoomImpl* impl, net::PacketGsf* packet) {
	String resp = resp_from_gsf(packet->gsf);
	if (resp != "ACCEPTED") log::errorf("Match room host responded with %s.", resp.c_str());
}
PacketHandler handlers[] = {
	{"match/room/join", host_on_join, guest_on_join},
	{"match/room/info", host_on_info, guest_on_info},
	{"match/room/settings", host_on_settings, guest_on_settings},
	{"match/room/endpoints", host_on_endpoints, guest_on_endpoints},
	{"match/room/perms", host_on_perms, guest_on_perms},
	{"match/room/members", host_on_members, guest_on_members},
	{"match/room/match", host_on_match, guest_on_match},
	{"match/room/response", host_on_response, guest_on_response},
};
void Room::update() {
	auto* impl = (RoomImpl*) this->impl;
	impl->hopper->poll();
	for (event::Event* base : impl->hopper->events) {
		auto* packet = (net::PacketGsf*) base;
		for (const PacketHandler& handler : handlers) {
			if (!packet->gsf.get_schema().get_descriptor().match(GEN_GSF_BANK(gongen)->schema(handler.schema).get_descriptor())) continue;
			// TODO catch errors
			if (impl->flags & IS_HOST) handler.as_host(impl, packet);
			else handler.as_guest(impl, packet);
			break;
		}
	}
	impl->hopper->clear();
}
void Room::join(uint32_t id) {
	auto* impl = (RoomImpl*) this->impl;
	if (impl->flags & IS_HOST) return; // TODO error?
	gsf::Article article(GEN_GSF_BANK(gongen)->schema("match/room/join"));
	article.access_root().encode_u32(id);
	impl->guest.provider->send(article);
	impl->id = id;
	impl->flags &= ~GOT_ALL;
}
bool Room::is_ready() {
	auto* impl = (RoomImpl*) this->impl;
	return impl->flags & IS_HOST || impl->flags & GOT_ALL;
}
RoomInfo Room::get_info() {
	if (!this->is_ready()) throw StateException("Room is not ready.");
	auto* impl = (RoomImpl*) this->impl;
	return impl->info;
}
RoomSettings Room::get_settings() {
	if (!this->is_ready()) throw StateException("Room is not ready.");
	auto* impl = (RoomImpl*) this->impl;
	return impl->settings;
}
HashMap<uint32_t, Endpoint> Room::get_endpoints() {
	if (!this->is_ready()) throw StateException("Room is not ready.");
	auto* impl = (RoomImpl*) this->impl;
	return impl->endpoints;
}
Permissions Room::get_permissions() {
	if (!this->is_ready()) throw StateException("Room is not ready.");
	auto* impl = (RoomImpl*) this->impl;
	return impl->perms;
}
void Room::set_info(const RoomInfo& info) {
	if (!this->is_ready()) throw StateException("Room is not ready.");
	auto* impl = (RoomImpl*) this->impl;
	if (impl->flags & IS_HOST) {
		host_change_info(impl, info);
	} else {
		throw NoImplException("Not yet implemented.");
	}
}
void Room::set_settings(const RoomSettings& settings) {
	if (!this->is_ready()) throw StateException("Room is not ready.");
	auto* impl = (RoomImpl*) this->impl;
	if (impl->flags & IS_HOST) {
		host_change_settings(impl, settings);
	} else {
		throw NoImplException("Not yet implemented.");
	}
}
void Room::set_endpoints(HashMap<uint32_t, Endpoint>& endpoints) {
	if (!this->is_ready()) throw StateException("Room is not ready.");
	auto* impl = (RoomImpl*) this->impl;
	if (impl->flags & IS_HOST) {
		host_change_endpoints(impl, endpoints);
	} else {
		throw NoImplException("Not yet implemented.");
	}
}
void Room::set_permissions(const Permissions& perms) {
	if (!this->is_ready()) throw StateException("Room is not ready.");
	auto* impl = (RoomImpl*) this->impl;
	if (impl->flags & IS_HOST) {
		host_change_perms(impl, perms);
	} else {
		throw NoImplException("Not yet implemented.");
	}
}
Optional<MatchInfo> Room::match_info() {
	if (!this->is_ready()) throw StateException("Room is not ready.");
	auto* impl = (RoomImpl*) this->impl;
	// TODO
	return {};
}
MatchInfo Room::match_start() {
	if (!this->is_ready()) throw StateException("Room is not ready.");
	auto* impl = (RoomImpl*) this->impl;
	throw NoImplException("Not yet implemented.");
}
