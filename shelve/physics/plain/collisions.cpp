// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include "collisions.hpp"
#include <gongen/common/hash.hpp>
#include <gongen/common/error.hpp>
#include <gongen/ecs/simulation.hpp>
#include "components.hpp"

using namespace gen::plainph;

comp::Properties* CollisionManager::get_props(ecs::Object* object, bool no_except) {
	auto* props = object->get_component<comp::Properties>(gen::ecs::COMP_PLAINPH_PHYSICS_ID);
	if (!props && !no_except) {
		throw ArgumentException("Object does not have physics properties component.");
	}
	return props;
}
void CollisionManager::check_collision(ecs::Object* object, uint64_t other_id) {
	ecs::Object* other = this->simulation->get_current_object(other_id);
	if (!other) {
		this->remove_object(other_id, false);
		return;
	}
	// check basic collision
	comp::Properties* object_props = this->get_props(object);
	comp::Properties* other_props = this->get_props(other);
	uint64_t groups = object_props->collision.groups & other_props->collision.groups;
	if (!groups) return; // no colliding groups
	PhysCoord strength = 1.0f;
	if (!object_props->collision.is_sphere && !other_props->collision.is_sphere) {
		// box collision
		vec3<PhysCoord> diff = (other_props->position - object_props->position).abs();
		vec3<PhysCoord> min = other_props->collision.bounding.box / 2 + object_props->collision.bounding.box / 2;
		if (diff.x > min.x || diff.y > min.y || diff.z > min.z) return;
		strength *= min.distance(diff);
	} else if (object_props->collision.is_sphere && other_props->collision.is_sphere) {
		// sphere collision
		PhysCoord min_distance = object_props->collision.bounding.sphere + other_props->collision.bounding.sphere;
		PhysCoord distance = object_props->position.distance(other_props->position);
		if (distance > min_distance) return;
		strength = min_distance / distance;
	} else {
		// sphere-box collision
		comp::Properties* sphere_props;
		comp::Properties* box_props;
		if (object_props->collision.is_sphere) {
			sphere_props = object_props;
			box_props = other_props;
		} else {
			sphere_props = other_props;
			box_props = object_props;
		}
		rect3<PhysCoord> box_aabb = box_props->get_aabb();
		vec3<PhysCoord> box_point = sphere_props->position;
		if (sphere_props->position.x < box_aabb.position.x) {
			box_point.x = box_aabb.position.x;
		} else if (sphere_props->position.x > box_aabb.position.x + box_aabb.size.x) {
			box_point.x = box_aabb.position.x + box_aabb.size.x;
		}
		if (sphere_props->position.y < box_aabb.position.y) {
			box_point.y = box_aabb.position.y;
		} else if (sphere_props->position.y > box_aabb.position.y + box_aabb.size.y) {
			box_point.y = box_aabb.position.y + box_aabb.size.y;
		}
		if (sphere_props->position.z < box_aabb.position.z) {
			box_point.z = box_aabb.position.z;
		} else if (sphere_props->position.z > box_aabb.position.z + box_aabb.size.z) {
			box_point.z = box_aabb.position.z + box_aabb.size.z;
		}
		vec3<PhysCoord> diff = box_point - sphere_props->position;
		PhysCoord radius = sphere_props->collision.bounding.sphere;
		PhysCoord distance = diff.distance();
		if (distance >= radius) return;
		strength = radius / distance;
	}

	// check custom collision
	for (CollisionChecker& callback : this->checkers) {
		if (!callback(object, other)) return;
	}

	// collision occured
	if (object_props->collision.emit_event || other_props->collision.emit_event) {
		CollisionEvent* event = new CollisionEvent(this);
		event->object_a = object->get_id();
		event->object_b = other_id;
		event->groups = groups;
		event::system()->occur(event);
	}
	// push objects from each other
	if (strength < 0) strength = 0;
	if (strength > this->strength_limit) strength = this->strength_limit;
	comp::Motion* motion;
	if (
		object_props->collision.change_motion &&
		(motion = object->get_component<comp::Motion>(gen::ecs::COMP_PLAINPH_MOTION_ID))
	) {
		vec3<PhysCoord> direction = (object_props->position - other_props->position);
		direction.x = 1 / direction.x;
		direction.y = 1 / direction.y;
		motion->velocity = direction * strength * motion->velocity.distance();
	}
	if (
		other_props->collision.change_motion &&
		(motion = other->get_component<comp::Motion>(gen::ecs::COMP_PLAINPH_MOTION_ID))
	) {
		vec3<PhysCoord> direction = (other_props->position - object_props->position);
		direction.x = 1 / direction.x;
		direction.y = 1 / direction.y;
		motion->velocity = direction * strength * motion->velocity.distance();
	}
}
CollisionManager::CollisionManager(ecs::Simulation* simulation) : simulation(simulation) {
}
void CollisionManager::register_checker(CollisionChecker callback) {
	this->checkers.add(callback);
}
void CollisionManager::fix_objects() {
	for (ecs::Object* object : this->simulation->get_current_object_map()) {
		comp::Properties* props = this->get_props(object, true);
		if (!props || !props->collision.manager) continue;
		props->collision.manager = this;
	}
}
void CollisionManager::add_object(ecs::Object* object) {
	this->get_props(object)->collision.manager = this;
	this->update_pos(object);
}
void CollisionManager::remove_object(ecs::Object* object) {
	return this->remove_object(object->get_id(), true);
}

gen::vec3i RegionCollisionManager::get_region(vec3<PhysCoord> position) {
	return gen::vec3i(
		(int) position.x / this->region_size.x,
		(int) position.y / this->region_size.y,
		(int) position.z / this->region_size.z
	);
}
RegionCollisionManager::RegionCollisionManager(
	ecs::Simulation* simulation, vec3<PhysCoord> region_size
) : CollisionManager(simulation), regions(gen::hash_pr<vec3i>),
		objects(gen::no_hash), region_size(region_size) {
}
void RegionCollisionManager::remove_object(uint64_t object_id, bool is_valid) {
	if (is_valid) this->simulation->remove_object(object_id);
	BufferList<vec3i>* regions = this->objects.ptr(object_id);
	if (!regions) return;
	for (vec3i& region : *regions) {
		this->regions[region].remove_match(object_id);
	}
	this->objects.remove(object_id);
}
void RegionCollisionManager::update_pos(ecs::Object* object) {
	comp::Properties* props = this->get_props(object);
	this->regions_buf.clear();
	gen::rect3<PhysCoord> aabb = props->get_aabb();
	vec3i start = this->get_region(aabb.position);
	vec3i end = this->get_region(aabb.position + aabb.size);
	for (int x = start.x; x <= end.x; x++) {
		for (int y = start.y; y <= end.y; y++) {
			for (int z = start.z; z <= end.z; z++) {
				this->regions_buf.emplace(x, y, z);
			}
		}
	}
	BufferList<vec3i>& regions = objects[object->get_id()];
	if (this->regions_buf == regions) return;
	for (const vec3i& region : regions) {
		BufferList<uint64_t>& objects = this->regions[region];
		objects.remove_match(object->get_id());
		if (objects.empty()) {
			this->regions.remove(region);
		}
	}
	regions.clear();
	for (const vec3i& region : this->regions_buf) {
		regions.add(region);
		this->regions[region].add(object->get_id());
	}
}
void RegionCollisionManager::check_collisions(ecs::Object* object) {
	uint64_t object_id = object->get_id();
	this->regions_buf = this->objects[object_id];
	for (vec3i& region : this->regions_buf) {
		this->object_buf = this->regions[region];
		for (uint64_t other_id : this->object_buf) {
			if (other_id == object_id) continue;
			this->check_collision(object, other_id);
		}
	}
}
