// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#include "components.hpp"
#include "collisions.hpp"
#include <gongen/ecs/object.hpp>

using namespace gen::plainph;

void comp::Properties::mark_dirty() {
	this->collision.dirty = true;
}
gen::rect3<PhysCoord> comp::Properties::get_aabb() {
	if (!this->collision.is_sphere) {
		return rect3<PhysCoord>(
			this->position - this->collision.bounding.box / 2,
			this->collision.bounding.box
		);
	} else {
		return rect3<PhysCoord>(
			this->position - this->collision.bounding.box / 2,
			gen::vec3<PhysCoord>(this->collision.bounding.sphere * 2)
		);
	}
}
void comp::Properties::bounding_box(rect3<PhysCoord> aabb) {
	this->collision.is_sphere = 0;
	this->position += aabb.position + aabb.size / 2;
	this->collision.bounding.box = aabb.size;
}
void comp::Properties::bounding_sphere(vec3<PhysCoord> center, PhysCoord radius) {
	this->collision.is_sphere = 1;
	this->position += center;
	this->collision.bounding.sphere = radius;
}

COMPONENT_IMPL(PLAINPH_PHYSICS, sim, obj, comp, speed, user) {
	if (comp->collision.manager && comp->collision.dirty) {
		((CollisionManager*) comp->collision.manager)->update_pos(obj);
		((CollisionManager*) comp->collision.manager)->check_collisions(obj);
	}
	comp->collision.dirty = false;
}

COMPONENT_IMPL(PLAINPH_MOTION, sim, obj, comp, speed, user) {
	comp->velocity += comp->acceleration * speed;
	if (comp->velocity.x == 0 && comp->velocity.y == 0 && comp->velocity.z == 0) return;
	comp::Properties* props = obj->get_component<comp::Properties>(COMP_PLAINPH_PHYSICS_ID);
	if (!props) return;
	props->position += comp->velocity * speed;
	props->mark_dirty();
	if (comp->friction < 0) comp->friction = 0;
	else if (comp->friction > 1) comp->friction = 1;
	comp->velocity *= 1 - comp->friction;
}

COMPONENT_IMPL(PLAINPH_MOVEMENT, sim, obj, comp, speed, user) {
	if (comp->velocity.x == 0 && comp->velocity.y == 0 && comp->velocity.z == 0) return;
	comp::Properties* props = obj->get_component<comp::Properties>(COMP_PLAINPH_PHYSICS_ID);
	if (!props) return;
	vec3<PhysCoord> velocity = comp->velocity * speed;
	props->position += vec3<PhysCoord>(
		velocity.z * sin(comp->direction.y) * cos(comp->direction.x),
		velocity.z * sin(comp->direction.y) * sin(comp->direction.x),
		velocity.z * cos(comp->direction.y)
	);
	props->position += vec3<PhysCoord>(
		velocity.y * sin(comp->direction.y) * cos(comp->direction.x - M_PI / 2),
		velocity.y * sin(comp->direction.y) * sin(comp->direction.x - M_PI / 2),
		velocity.y * cos(comp->direction.y)
	);
	props->position += vec3<PhysCoord>(
		velocity.x * sin(comp->direction.y - M_PI / 2) * cos(comp->direction.x),
		velocity.x * sin(comp->direction.y - M_PI / 2) * sin(comp->direction.x),
		velocity.x * cos(comp->direction.y - M_PI / 2)
	);
	props->mark_dirty();
}
