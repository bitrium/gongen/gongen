// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/common/math/vector.hpp>
#include <gongen/common/event.hpp>
#include <gongen/ecs/definitions.hpp>
#include "definitions.hpp"

namespace gen::plainph {

namespace comp {
struct Properties;
} // namespace comp

class GEN_API CollisionEvent : public event::Event {
public:
	uint64_t object_a;
	uint64_t object_b;
	uint64_t groups;
	CollisionEvent(void* manager) : event::Event("plainph/collision", manager) {
	}
};

typedef bool (*CollisionChecker)(void* object, void* other);
class GEN_API CollisionManager {
private:
	LinkedList<CollisionChecker> checkers;
protected:
	comp::Properties* get_props(ecs::Object* object, bool no_except = false);
	void check_collision(ecs::Object* object, uint64_t other_id);
public:
	ecs::Simulation* const simulation;
	PhysCoord strength_limit = 100.0;

	CollisionManager(ecs::Simulation* simulation);

	void register_checker(CollisionChecker callback);

	/**
	 * @brief Fix manager pointers after loading a Simulation from binary data.
	 */
	virtual void fix_objects();
	virtual void add_object(ecs::Object* object);
	virtual void remove_object(uint64_t object_id, bool is_valid) {}
	void remove_object(ecs::Object* object);
	virtual void update_pos(ecs::Object* object) = 0;

	virtual void check_collisions(ecs::Object* object) = 0; // call on_collide
};

class GEN_API RegionCollisionManager : public CollisionManager {
private:
	HashMap<vec3i, BufferList<uint64_t>, 1024> regions; // region to objects
	HashMap<uint64_t, BufferList<vec3i>, 1024> objects; // object to regions
	BufferList<vec3i> regions_buf; // working buffer
	BufferList<uint64_t> object_buf; // working buffer

	vec3i get_region(vec3<PhysCoord> position);
public:
	const vec3<PhysCoord> region_size;

	RegionCollisionManager(
		ecs::Simulation* simulation,
		vec3<PhysCoord> region_size = vec3<PhysCoord>(32)
	);
	void remove_object(uint64_t object_id, bool is_valid) override;
	void update_pos(ecs::Object* object) override;
	void check_collisions(ecs::Object* object) override;
};

} // namespace gen::plainph
