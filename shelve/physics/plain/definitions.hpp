// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

namespace gen::plainph {
#ifdef GEN_PLAINPH_DOUBLE_COORDS
typedef double PhysCoord;
#else
typedef float PhysCoord;
#endif
} // namespace gen::plainph
