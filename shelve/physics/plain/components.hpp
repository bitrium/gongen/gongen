// SPDX-License-Identifier: BSD-3-Clause
// Copyright (c) 2024 GongEn Contributors

#pragma once

#include <gongen/ecs/component.hpp>
#include <gongen/common/math.hpp>
#include "definitions.hpp"

namespace gen::plainph::comp {
struct Properties;
struct Motion;
struct Movement;
struct Ground2D;
} // namespace gen::plainph::comp

#pragma pack(push, 8)

COMPONENT(gen::plainph::comp::Properties, PLAINPH_PHYSICS) {
	struct {
		void* manager = nullptr; // do not modify manually
		bool dirty : 1 = true;
		bool emit_event : 1 = true;
		bool is_sphere : 1 = false;
		bool change_motion : 1 = true;
		uint64_t groups = 1; // groups bitfield
		union {
			vec3<PhysCoord> box; // size of box with position as center
			PhysCoord sphere; // radius of the sphere with position as center
		} bounding = { .box = vec3<PhysCoord>() };
	} collision;
	vec3<PhysCoord> position;
	vec3<PhysCoord> rotation; // TODO bounding box support
	float mass = 1.0f;

	/**
	 * @brief Do this whenever position or collision bounding info change.
	 */
	void mark_dirty();
	/**
	 * @brief Gets the collision bounding box (if uses sphere, a fitting cube).
	 * @return The AABB bounding box.
	 */
	rect3<PhysCoord> get_aabb();
	/**
	 * @brief Sets a bounding box (aligns the position to the box'es origin).
	 * @param aabb The AABB bounding box.
	 */
	void bounding_box(rect3<PhysCoord> aabb);
	/**
	 * @brief Sets a bounding sphere (aligns the position to the sphere's origin).
	 * @param center Origin of the sphere.
	 * @param radius Radius of the sphere.
	 */
	void bounding_sphere(vec3<PhysCoord> center, PhysCoord radius);
};

// COMPONENT(gen::plainph::comp::Ground2D, PLAINPH_2D_GROUND) {
// };

COMPONENT(gen::plainph::comp::Motion, PLAINPH_MOTION) {
	vec3<PhysCoord> velocity;
	vec3<PhysCoord> acceleration;
	PhysCoord friction = 0.0f; // [0; 1]
};

COMPONENT(gen::plainph::comp::Movement, PLAINPH_MOVEMENT) {
	vec2<PhysCoord> direction;
	vec3<PhysCoord> velocity;
};

#pragma pack(pop)
