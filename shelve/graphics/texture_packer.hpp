#pragma once

#include <gongen/gl/gl.hpp>

namespace gen::graphics {
	struct TextureCoords {
		uint16_t x, y, w, h;
		uint8_t layer;
	};

	class GEN_API TexturePacker {
		gen::gl::Texture texture;
	public:
		struct Texture {
			gen::String name;
			uint32_t x, y, w, h;
			const void* data;
			bool own_data;
			TextureCoords coords;
		};

		gen::Buffer<Texture> textures;

		TexturePacker() {}
		~TexturePacker();
		void build(gen::gl::Device* device, uint32_t texture_size, gen::gl::Format format, bool free_data = true, bool edge_coords_fix = true, bool force_non_array_texture = false);
		gen::gl::Texture get_texture() { return texture; }
	};
};
