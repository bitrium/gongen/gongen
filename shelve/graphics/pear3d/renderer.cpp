#include "renderer.hpp"

struct CameraUBO {
	gen::mat4f vp_matrix;
	gen::mat4f skybox_matrix;
	gen::mat4f view_matrix;
	gen::vec4f camera_pos;
};
struct Pear3D_Type {
	gen::pear3d::Type info;
	uint64_t info_hash;

	size_t offset;
	size_t count;
	size_t first_mat;

	gen::Buffer<gen::pear3d::Object*> objects;

	Pear3D_Type() {}
};

#pragma pack(push, 4)
struct Pear3D_Material {
	gen::Color color;
	float rougness;
	float metalness;
	float uv_layer;
	float padding[1];
	gen::vec2f uv_multiplier;
	gen::vec2f uv_bias;
};
struct Pear3D_InstanceData {
	gen::mat4f transform_matrix;
	gen::mat3f normal_matrix;
	gen::Color color;
	uint32_t first_material;
	uint32_t first_bone;
	float padding[1];
};
struct Pear3D_DepthInstanceData {
	gen::mat4f transform_matrix;
};
#pragma pack(pop)

gen::gl::DepthState depth_pass_depth_state = { true, true, true, gen::gl::CompareOp::GREATER };
gen::gl::PrimitiveState depth_pass_primitive_state = { gen::gl::PrimitiveTopology::TRIANGLE_LIST, gen::gl::CullMode::BACK, true, gen::gl::FillMode::SOLID };

gen::gl::DepthState opaque_pass_depth_state = { true, true, false, gen::gl::CompareOp::GREATER_OR_EQUAL };
gen::gl::DepthState opaque_pass_depth_state_animated = { true, true, true, gen::gl::CompareOp::GREATER_OR_EQUAL };
gen::gl::PrimitiveState opaque_pass_primitive_state = { gen::gl::PrimitiveTopology::TRIANGLE_LIST, gen::gl::CullMode::BACK, true, gen::gl::FillMode::SOLID };
gen::gl::BlendState opaque_pass_blend_state = { false };

gen::gl::DepthState disabled_depth_state = { false, false, false, gen::gl::CompareOp::ALWAYS };

gen::gl::DepthState skybox_depth_state = { true, true, false, gen::gl::CompareOp::GREATER_OR_EQUAL };
gen::gl::PrimitiveState skybox_primitive_state = { gen::gl::PrimitiveTopology::TRIANGLE_LIST, gen::gl::CullMode::NONE, true, gen::gl::FillMode::SOLID };

class Pear3DRenderer :public gen::pear3d::Renderer {
	gen::gl::Capabilities gl_caps;
	gen::pear3d::RendererConfig config;
	bool mdi_support = false;

	gen::gl::FramebufferState depth_pass_fb;
	gen::gl::FramebufferState opaque_pass_fb;

	gen::gl::Sampler point_sampler;
	gen::gl::Sampler linear_sampler;

	CameraUBO camera_ubo;
	gen::gl::Buffer camera_buffer;
	gen::gl::Buffer ldr2hdr_buffer;
	gen::gl::Buffer light_buffer;

	gen::graphics::DynamicBuffer<Pear3D_Material> material_buffer;
	gen::graphics::DynamicBuffer<Pear3D_InstanceData> instance_buffer;
	gen::graphics::DynamicBuffer<Pear3D_DepthInstanceData> instance_buffer_depth;
	gen::graphics::DynamicBuffer<gen::gl::DrawIndexedIndirectCommand> indirect_buffer;
	gen::graphics::DynamicBuffer<gen::mat4f> bone_buffer;

	gen::gl::Program opaque_pass_textured;
	gen::gl::Program opaque_pass_animated;

	gen::DynamicArray<Pear3D_Type> types;
public:
	void init(const gen::pear3d::RendererConfig& config) {
		this->config = config;
		gl_caps = config.gl_device->get_capabilities();
		mdi_support = (gl_caps.first_instance && gl_caps.multi_draw_indirect && gl_caps.glsl_first_instance);

		gen::gl::TextureDesc texture_desc;
		texture_desc.format = gen::gl::Format::D32F;
		texture_desc.size = gen::vec3z((size_t)config.fb_width, (size_t)config.fb_height, 1);
		gen::gl::Texture depth_texture = config.gl_device->create_texture(texture_desc);

		texture_desc.format = gen::gl::Format::RGBA16F;
		gen::gl::Texture color_texture = config.gl_device->create_texture(texture_desc);

		depth_pass_fb.color_attachment_count = 0;
		depth_pass_fb.depth_stencil_attachment = depth_texture;

		opaque_pass_fb.color_attachment_count = 1;
		opaque_pass_fb.color_attachments[0] = color_texture;
		opaque_pass_fb.depth_stencil_attachment = depth_texture;

		point_lights.resize(config.max_lights);

		camera_buffer = config.gl_device->create_buffer({ sizeof(CameraUBO) });
		ldr2hdr_buffer = config.gl_device->create_buffer({ sizeof(hdr2ldr_config) });
		light_buffer = config.gl_device->create_buffer({ sizeof(dir_light) + point_lights.size_bytes() });

		point_sampler = config.gl_device->create_sampler({ gen::gl::Filtering::NEAREST, gen::gl::Filtering::NEAREST, gen::gl::Filtering::NEAREST });
		linear_sampler = config.gl_device->create_sampler({});

		reload_shaders(config.shaders);
	}
	void reload_shaders(const gen::pear3d::Shaders& shaders) {
		this->config.shaders = shaders;

		this->config.shaders.depth_pass.desc.defines.clear();
		this->config.shaders.depth_pass.desc.defines.add("DEPTH_ONLY");
		if (mdi_support) this->config.shaders.depth_pass.desc.defines.add("MDI_RENDERING");
		this->config.shaders.depth_pass.build(config.gl_device);

		this->config.shaders.opaque_pass.desc.defines.clear();
		this->config.shaders.opaque_pass.desc.defines.add(gen::String::createf("LIGHT_COUNT %zu", config.max_lights));
		if (mdi_support) this->config.shaders.opaque_pass.desc.defines.add("MDI_RENDERING");
		this->config.shaders.opaque_pass.build(config.gl_device);
		this->config.shaders.opaque_pass.desc.defines.add("TEXTURED");
		opaque_pass_textured = config.gl_device->create_program(this->config.shaders.opaque_pass.desc);

		this->config.shaders.opaque_pass.desc.defines.clear();
		this->config.shaders.opaque_pass.desc.defines.add(gen::String::createf("LIGHT_COUNT %zu", config.max_lights));
		if (mdi_support) this->config.shaders.opaque_pass.desc.defines.add("MDI_RENDERING");
		this->config.shaders.opaque_pass.desc.defines.add("ANIMATED");
		opaque_pass_animated = config.gl_device->create_program(this->config.shaders.opaque_pass.desc);

		this->config.shaders.hdr2ldr_pass.desc.defines.clear();
		this->config.shaders.hdr2ldr_pass.build(config.gl_device);

		this->config.shaders.skybox_shader.desc.defines.clear();
		this->config.shaders.skybox_shader.build(config.gl_device);
	}
	gen::mat4f camera_view_matrix() {
		if (camera.mode == gen::pear3d::Mode::LOOK_AT) {
			gen::mat4f v = gen::mat4f::look_at(camera.transform.position, gen::vec3f(-camera.look_at.x, camera.look_at.y, camera.look_at.z), gen::vec3f(0.0f, 1.0f, 0.0f));
			return v;
		}
		else {
			return camera.transform.get_view_matrix(camera.offset);
		}
	}
	void render() {
		gen::gl::Viewport viewport;
		viewport.size = gen::vec2u(config.fb_width, config.fb_height);
		config.gl_device->set_viewport(viewport);
		gen::gl::Scissor scissor;
		scissor.enabled = false;
		scissor.size = gen::vec2u(config.fb_width, config.fb_height);
		config.gl_device->set_scissor(scissor);

		gen::mat4f projection_matrix = gen::mat4f::perspective_reverse_z_infinite_far(camera.near_plane, camera.aspect_ration, camera.fov);
		
		camera_ubo.vp_matrix = projection_matrix * camera_view_matrix();
		camera_ubo.skybox_matrix = projection_matrix * gen::mat4f(gen::mat3f(camera_view_matrix()));
		camera_ubo.view_matrix = camera_view_matrix();
		camera_ubo.camera_pos = gen::vec4f(camera.transform.position.x, camera.transform.position.y, camera.transform.position.z, 0.0f);

		config.gl_device->update_buffer(camera_buffer, 0, sizeof(CameraUBO), &camera_ubo);
		config.gl_device->bind_uniform_buffer(camera_buffer, 0);

		config.gl_device->set_default_framebuffer();
		gen::gl::ClearDesc clear_desc;
		clear_desc.color[0] = { true, gen::Color(0.0f) };
		config.gl_device->clear_framebuffer(clear_desc);

		process_objects();
		process_lights();
		depth_pass();
		opaque_pass();
		hdr2ldr_pass();

		types.clear();
	}
	void process_objects() {
		for (auto& o : objects) { o.internal = 0; }
		for (auto& o : objects) {
			if (!(o.internal & 1)) {
				uint64_t type_hash = o.type->hash();
				for (auto& t : types) {
					if (t.info_hash == type_hash) {
						t.objects.add(&o);
						type_hash = UINT64_MAX;
					}
				}
				if (type_hash != UINT64_MAX) {
					Pear3D_Type type{};
					type.info = *o.type;
					type.info_hash = type.info.hash();
					type.objects.add(&o);
					if (type.info.mesh->type == gen::pear3d::MeshType::ANIMATED) type.info.flags &= ~(gen::pear3d::Flag::DEPTH_PASS);
					types.add(type);
				}
			}
		}
		gen::DynamicArray<gen::pear3d::Material> mats;
		for (auto& t : types) {
			t.offset = instance_buffer.data.size();
			t.count = t.objects.size();
			if (!mdi_support) {
				size_t mod = t.count % 4;
				if (mod) t.count += (4 - mod);
			}
			Pear3D_InstanceData* instance_data = instance_buffer.data.reserve(t.count);
			Pear3D_DepthInstanceData* depth_instance_data = instance_buffer_depth.data.reserve(t.count);
			if (!mdi_support) t.count = (t.count - (t.count % 4));

			size_t mat_offset = material_buffer.data.size();
			mats.clear();
			if (t.info.materials.empty()) {
				for (auto& m : t.info.mesh->default_materials) mats.add(m);
			}
			else {
				for(auto& m : t.info.materials) mats.add(m);
				if (t.info.materials.size() < t.info.mesh->mat_count) {
					for (size_t i = t.info.materials.size(); i < t.info.mesh->mat_count; i++) {
						mats.add(t.info.mesh->default_materials[i]);
					}
				}
			}
			Pear3D_Material* pear3d_mat = material_buffer.data.reserve(mats.size());
			for (auto& m : mats) {
				pear3d_mat->color = m.color;
				pear3d_mat->metalness = m.metalness;
				pear3d_mat->rougness = m.roughness;
				pear3d_mat->padding[0] = 0.0f;
				pear3d_mat->uv_layer = m.tx_coord.layer;
				pear3d_mat->uv_multiplier = gen::vec2f(m.tx_coord.w / (float)UINT16_MAX, m.tx_coord.h / (float)UINT16_MAX);
				pear3d_mat->uv_bias = gen::vec2f(m.tx_coord.x / (float)UINT16_MAX, m.tx_coord.y / (float)UINT16_MAX);
				pear3d_mat++;
			}

			for (auto& ot : t.objects) {
				gen::mat4f transform_matrix = ot->transform.get_matrix();
				gen::mat3f normal_matrix = ot->transform.get_normal_matrix();

				instance_data->transform_matrix = transform_matrix;
				instance_data->normal_matrix = normal_matrix;
				instance_data->color = ot->color;
				instance_data->first_bone = 0;
				instance_data->first_material = mat_offset;
				instance_data->padding[0] = 0.0f;
				if (t.info.mesh->type == gen::pear3d::MeshType::ANIMATED) {
					instance_data->first_bone = bone_buffer.data.size();
					if (ot->animation_state) bone_buffer.data.add(ot->animation_state->bone_mats.ptr(), t.info.mesh->bone_count);
					else bone_buffer.data.add(t.info.mesh->bone_inversebind.ptr(), t.info.mesh->bone_count);
				}
				depth_instance_data->transform_matrix = transform_matrix;

				instance_data++;
				depth_instance_data++;
			}

			t.objects.clear();
		}

		material_buffer.update(config.gl_device, true);
		instance_buffer.update(config.gl_device, true);
		instance_buffer_depth.update(config.gl_device, true);
		bone_buffer.update(config.gl_device, true);
	}
	static int light_sort(void const* a, void const* b) {
		gen::pear3d::PointLight* la = (gen::pear3d::PointLight*)a;
		gen::pear3d::PointLight* lb = (gen::pear3d::PointLight*)b;
		return la->position.z - lb->position.z;
	}
	void process_lights() {
		config.gl_device->update_buffer(light_buffer, 0, sizeof(dir_light), &dir_light);
		config.gl_device->update_buffer(light_buffer, sizeof(dir_light), point_lights.size_bytes(), point_lights.ptr());
	}
	void depth_pass() {
		gen::gl::ClearDesc clear_desc;
		clear_desc.do_clear_depth = true;
		clear_desc.clear_depth = 0.0f;

		config.gl_device->set_framebuffer_state(depth_pass_fb);
		config.gl_device->clear_framebuffer(clear_desc);
		if (objects.empty()) return;

		for (auto& h : config.hooks) {
			if (h.pre && h.depth_pass_hook) h.depth_pass_hook(h.userdata);
		}

		config.gl_device->set_program(config.shaders.depth_pass.program);
		config.gl_device->set_depth_state(depth_pass_depth_state);
		config.gl_device->set_primitive_state(depth_pass_primitive_state);
		draw_types(gen::pear3d::Flag::DEPTH_PASS, sizeof(Pear3D_DepthInstanceData), instance_buffer_depth.get(), gen::pear3d::MeshType::POS);

		for (auto& h : config.hooks) {
			if (!h.pre && h.depth_pass_hook) h.depth_pass_hook(h.userdata);
		}
	}
	void opaque_pass() {
		gen::gl::ClearDesc clear_desc;
		clear_desc.color[0] = { true, gen::Color(0.0f) };
		clear_desc.color[1] = { true, gen::Color(0.0f) };

		config.gl_device->set_framebuffer_state(opaque_pass_fb);
		config.gl_device->clear_framebuffer(clear_desc);
		if (config.skybox) {
			config.gl_device->set_vertex_format(config.mesh_manager->get_vertex_format(gen::pear3d::MeshType::STATIC));
			config.gl_device->bind_vertex_buffer(config.mesh_manager->get_vertex_buffer(gen::pear3d::MeshType::STATIC), 0);
			config.gl_device->bind_index_buffer(config.mesh_manager->get_index_buffer(gen::pear3d::MeshType::STATIC), false);

			config.gl_device->set_program(config.shaders.skybox_shader.program);
			config.gl_device->set_depth_state(skybox_depth_state);
			config.gl_device->set_primitive_state(skybox_primitive_state);
			config.gl_device->bind_texture(skybox_texture, 0);
			config.gl_device->bind_sampler(linear_sampler, 0);
			config.gl_device->draw_indexed(config.skybox->idx_count, config.skybox->idx_offset, 0, 1, 0);
		}
		if (objects.empty()) return;

		for (auto& h : config.hooks) {
			if (h.pre && h.opaque_pass_hook) h.opaque_pass_hook(h.userdata);
		}

		config.gl_device->set_program(config.shaders.opaque_pass.program);
		config.gl_device->set_depth_state(opaque_pass_depth_state);
		config.gl_device->set_primitive_state(opaque_pass_primitive_state);
		config.gl_device->set_blend_state(opaque_pass_blend_state);
		config.gl_device->bind_storage_buffer(material_buffer.get(), 1);
		config.gl_device->bind_storage_buffer(light_buffer, 2);
		if(bone_buffer.get()) config.gl_device->bind_storage_buffer(bone_buffer.get(), 3);
		// config.gl_device->bind_texture(config.tx_packer->get_texture(), 0);
		draw_types(gen::pear3d::Flag::OPAQUE_PASS, sizeof(Pear3D_InstanceData), instance_buffer.get(), gen::pear3d::MeshType::STATIC);
		config.gl_device->set_program(opaque_pass_textured);
		draw_types(gen::pear3d::Flag::OPAQUE_PASS, sizeof(Pear3D_InstanceData), instance_buffer.get(), gen::pear3d::MeshType::TEXTURED);
		config.gl_device->set_program(opaque_pass_animated);
		config.gl_device->set_depth_state(opaque_pass_depth_state_animated);
		draw_types(gen::pear3d::Flag::OPAQUE_PASS, sizeof(Pear3D_InstanceData), instance_buffer.get(), gen::pear3d::MeshType::ANIMATED);

		for (auto& h : config.hooks) {
			if (!h.pre && h.opaque_pass_hook) h.opaque_pass_hook(h.userdata);
		}
	}
	void hdr2ldr_pass() {
		config.gl_device->update_buffer(ldr2hdr_buffer, 0, sizeof(hdr2ldr_config), &hdr2ldr_config);
		config.gl_device->bind_uniform_buffer(ldr2hdr_buffer, 1);
		config.gl_device->bind_texture(opaque_pass_fb.color_attachments[0], 0);
		config.gl_device->set_default_framebuffer();
		config.gl_device->set_depth_state(disabled_depth_state);
		config.gl_device->set_program(config.shaders.hdr2ldr_pass.program);
		gen::gl::VertexFormat vf;
		vf.attrib_count = 0;
		config.gl_device->set_vertex_format(vf);
		config.gl_device->draw(3);
	}
	void draw_types(uint32_t flags, size_t instance_data_size, const gen::gl::Buffer& instance_buffer, gen::pear3d::MeshType mesh_type) {
		gen::Buffer<Pear3D_Type*> draw_types;
		for (auto& t : types) {
			if ((t.info.mesh->type == mesh_type) || (mesh_type == gen::pear3d::MeshType::POS)) {
				if ((t.info.flags & flags) || !flags) draw_types.add(&t);
			}
		}
		if (draw_types.empty()) return;

		config.gl_device->set_vertex_format(config.mesh_manager->get_vertex_format(mesh_type));
		config.gl_device->bind_vertex_buffer(config.mesh_manager->get_vertex_buffer(mesh_type), 0);
		config.gl_device->bind_index_buffer(config.mesh_manager->get_index_buffer(mesh_type), false);

		if (mdi_support) {
			auto cmd = indirect_buffer.data.reserve(draw_types.size(), true);
			for (size_t i = 0; i < draw_types.size(); i++) {
				cmd->first_vertex = 0;
				cmd->first_instance = draw_types[i]->offset;
				cmd->instance_count = draw_types[i]->count;
				cmd->first_index = draw_types[i]->info.mesh->idx_offset;
				if(mesh_type == gen::pear3d::MeshType::POS) cmd->first_index = draw_types[i]->info.mesh->idx_offset_pos;
				cmd->count = draw_types[i]->info.mesh->idx_count;
				cmd++;
			}
			indirect_buffer.update(config.gl_device, true);
			config.gl_device->bind_storage_buffer(instance_buffer, 0);
			config.gl_device->draw_indexed_indirect(indirect_buffer.get(), 0, draw_types.size());
		}
		else {
			for (size_t i = 0; i < draw_types.size(); i++) {
				size_t idx_offset = draw_types[i]->info.mesh->idx_offset;
				if (mesh_type == gen::pear3d::MeshType::POS) idx_offset = draw_types[i]->info.mesh->idx_offset_pos;
				config.gl_device->bind_storage_buffer(instance_buffer, 0, draw_types[i]->offset * instance_data_size, draw_types[i]->count * instance_data_size);
				config.gl_device->draw_indexed(draw_types[i]->info.mesh->idx_count, idx_offset, 0, draw_types[i]->count, 0);
			}
		}
	}
};

gen::pear3d::Renderer* gen::pear3d::create_renderer() { return new Pear3DRenderer(); }
/*void get_bone_transform(gen::mat4f& mat, gen::mat4f* local_transforms, size_t index, gen::pear3d::Mesh* mesh) {
	if (mesh->bone_parents[index] != SIZE_MAX) get_bone_transform(mat, local_transforms, mesh->bone_parents[index], mesh);
	mat = mat * local_transforms[index];
}*/
gen::mat4f get_bone_transform_matrix(gen::pear3d::Mesh* mesh, gen::pear3d::MeshAnimation* anim, size_t index, float sec) {
	if(index == SIZE_MAX) return gen::mat4f();
	else return (anim->bones[index].transform_matrix(sec) * get_bone_transform_matrix(mesh, anim, mesh->bone_parents[index], sec));
}
gen::pear3d::AnimationState gen::pear3d::get_animation_state(gen::pear3d::Mesh* mesh, gen::pear3d::MeshAnimation* anim, float sec) {
	gen::pear3d::AnimationState state;
	if (!anim) {
		state.bone_mats = mesh->bone_inversebind;
		return state;
	}
	sec = fmodf(sec, anim->time);
	if (sec < 0.0f) sec = 0.0f;
	if (sec >= anim->time) sec = anim->time;

	//gen::Buffer<gen::mat4f> local_transform_mats;
	//local_transform_mats.reserve(mesh->bone_count);
	//for (size_t i = 0; i < anim->bones.size(); i++) {
		//local_transform_mats[i] = anim->bones[i].transform_matrix(sec) * mesh->bone_mats[i];
	//}
	state.bone_mats.reserve(mesh->bone_count);
	for (size_t i = 0; i < anim->bones.size(); i++) {
		gen::mat4f local_transform = get_bone_transform_matrix(mesh, anim, i, sec);
		gen::mat4f world_transform = mesh->bone_transform[i] * local_transform;
		state.bone_mats[i] = world_transform * mesh->bone_inversebind[i];
		//gen::mat4f world_transform = mesh->bone_transform[i];
		//gen::mat4f transform_mat = get_bone_transform_matrix(mesh, anim, i, sec) * mesh->bone_inversebind[i];
		//gen::mat4f transform_mat = get_bone_transform_matrix(mesh, anim, i, sec) * mesh->bone_transform[i];
		//state.bone_mats[i] = transform_mat * mesh->bone_inversebind[i];
		//state.bone_mats[i] = mesh->bone_transform[i] * transform_mat;
		//get_bone_transform(state.bone_mats[i], local_transform_mats.ptr(), i, mesh);
		//state.bone_mats[i] = state.bone_mats[i] * mesh->bone_mats[i];
	}

	/*gen::mat4f* bone_mat = mesh->bone_mats.ptr();
	for (auto& b : anim->bones) {
		state.bone_mats.add(*bone_mat * b.transform_matrix(sec));
		bone_mat++;
	}*/
	return state;
}
