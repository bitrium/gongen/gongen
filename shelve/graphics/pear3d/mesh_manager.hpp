#pragma once

#include <gongen/gl/gl.hpp>
#include <gongen/common/math/color.hpp>
#include <gongen/common/math/rect.hpp>
#include <gongen/resources/kinds.hpp>
#include <gongen/loaders/mesh.hpp>
#include "../texture_packer.hpp"

namespace gen::pear3d {
	struct Mesh;
	namespace Flag {
		enum Flag {
			DEPTH_PASS = 1,
			OPAQUE_PASS = 2
		};
	}

	const static uint32_t DEFAULT_FLAGS_OPAQUE = Flag::DEPTH_PASS | Flag::OPAQUE_PASS;

	enum class MeshType {
		POS, //Position only for depth passes, automaticly generated for all meshes
		STATIC,
		TEXTURED,
		ANIMATED,

		COUNT_
	};

#pragma pack(push, 1)
	struct StaticVertex {
		gen::vec3f pos;
		uint8_t normal[3];
		uint8_t material_id;
	};
	struct TexturedVertex {
		gen::vec3f pos;
		uint8_t normal[3];
		uint8_t material_id;
		uint16_t tex_coord[2];
	};
	struct AnimatedVertex {
		gen::vec3f pos;
		uint8_t normal[3];
		uint8_t material_id;
		uint8_t bone_ids[4];
		gen::vec4u16 bone_weights;
	};
#pragma pack(pop)
	struct Material {
		gen::String name;
		gen::String texture_id;
		gen::Color color;
		float roughness;
		float metalness;
		gen::graphics::TextureCoords tx_coord;
	};
	struct Type {
		gen::pear3d::Mesh* mesh = nullptr;
		gen::DynamicArray<gen::pear3d::Material> materials;
		uint32_t flags = 0;

		uint64_t hash() const { return gen::hash_pr<Type>(*this); }

		bool operator==(const Type& other) const {
			return hash() == other.hash();
		}
	};
	struct GEN_API MeshAnimationSampler {
		gen::loaders::Mesh::Interpolation interpolation = gen::loaders::Mesh::Interpolation::STEP;
		bool is_vec4 = false;
		gen::Buffer<float> times;
		gen::ByteBuffer data;

		void get_time_range(float sec, size_t& min, size_t& max);

		MeshAnimationSampler() {}
		gen::vec3f get_data3(float sec);
		gen::vec4f get_data4(float sec);
	};
	struct GEN_API MeshBoneAnimation {
		MeshAnimationSampler translation;
		MeshAnimationSampler rotation;
		MeshAnimationSampler scale;

		gen::mat4f transform_matrix(float sec);
	};
	struct MeshAnimation {
		gen::String name;
		gen::DynamicArray<MeshBoneAnimation> bones;
		float time;
	};
	struct Mesh {
		res::Identifier identifier;
		MeshType type = MeshType::COUNT_;
		uint32_t idx_offset;
		uint32_t idx_offset_pos;
		uint32_t idx_count;

		uint32_t mat_count;
		gen::DynamicArray<Material> default_materials;

		uint32_t bone_count;
		gen::Buffer<gen::mat4f> bone_inversebind;
		gen::Buffer<gen::mat4f> bone_transform;
		gen::Buffer<size_t> bone_parents;
		gen::DynamicArray<MeshAnimation> animations;

		gen::vec3f bounding_sphere_pos;
		float bounding_sphere_radius;
		gen::rect3f bounding_box;

		gen::pear3d::Type default_type;

		// I hate C++. FIXME buffer contents should be compared here instead of their data pointers
		bool operator==(const Mesh& other) const {
			return gen::hash_pr(*this) == gen::hash_pr(other);
		}
	};

	class GEN_API MeshHandler : public res::KindHandler {
		gen::ByteBuffer vtx_data[(size_t)MeshType::COUNT_];
		gen::Buffer<uint32_t> idx_data[(size_t)MeshType::COUNT_];

		gen::gl::Buffer vtx_buffer[(size_t)MeshType::COUNT_];
		gen::gl::Buffer idx_buffer[(size_t)MeshType::COUNT_];

		gen::gl::VertexFormat vs_format[(size_t)MeshType::COUNT_];

		BufferList<Mesh> meshes;

		Mesh add_mesh(const gen::loaders::Mesh& mesh, const res::Identifier& identifier, MeshType type);
		Mesh add_gmesh(gen::DataSource* source, const res::Identifier& identifier);
		void clear();
	public:
		void setup_buffers(gen::gl::Device* gl_device, gen::graphics::TexturePacker& tx_packer, bool clear_data = true);

		String get_id() const override { return "mesh"; };
		Buffer<res::KindFile> get_files() const override;

		void create(void** item) override;
		void destroy(void** item) override;
		void* get_res(void** item) override;

		gen::gl::VertexFormat get_vertex_format(MeshType type) const { return vs_format[(size_t)type]; }
		const gen::gl::Buffer& get_vertex_buffer(MeshType type) const { return vtx_buffer[(size_t)type]; }
		const gen::gl::Buffer& get_index_buffer(MeshType type) const { return idx_buffer[(size_t)type]; }
	};
}
