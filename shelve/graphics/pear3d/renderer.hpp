#pragma once

#include "mesh_manager.hpp"
#include <gongen/common/math/matrix.hpp>
#include <gongen/common/math/vector.hpp>
#include <gongen/common/hash.hpp>
#include "../dynamic_buffer.hpp"

namespace gen::pear3d {
	struct Transform {
		gen::vec3f position;
		gen::vec3f rotation;
		gen::vec3f scale = gen::vec3f(1.0f);

		gen::mat4f get_matrix() {
			return gen::mat4f::translate(position) * gen::mat4f::rotate(rotation) * gen::mat4f::scale(scale);
		}
		gen::mat3f get_normal_matrix() {
			return gen::mat3f(gen::mat4f::rotate_scale(rotation, scale).inverse().transpose());
		}
		gen::mat4f get_view_matrix(gen::vec3f offset = gen::vec3f()) {
			return gen::mat4f::translate(-offset.x, -offset.y, -offset.z) * gen::mat4f::rotate(-rotation.x, -rotation.y, -rotation.z) * gen::mat4f::translate(-position.x, -position.y, -position.z) * gen::mat4f::scale(1.0f/scale.x, 1.0f /scale.y, 1.0f /scale.z);
		}
	};

	struct AnimationState {
		gen::Buffer<gen::mat4f> bone_mats;

		AnimationState() {}
	};

	AnimationState get_animation_state(gen::pear3d::Mesh* mesh, gen::pear3d::MeshAnimation* anim, float sec);

	struct Object {
		gen::pear3d::Type* type = nullptr;
		AnimationState* animation_state = nullptr;
		Transform transform;
		gen::Color color = gen::Color(1.0f);
		uint8_t internal;
	};

	enum class Mode {
		TRANSFORM,
		LOOK_AT
	};

	struct Camera {
		Mode mode;
		Transform transform;
		gen::vec3f offset;
		gen::vec3f look_at;
		float near_plane = 0.1f;
		float fov = 60.0f;
		float aspect_ration = (16.0f / 9.0f);
		float exposure = 1.0f;
		float gamma = 2.2f;
	};

	struct Shader {
		gen::gl::ProgramDesc desc;
		gen::gl::Program program;

		void build(gen::gl::Device* gl_device) { this->program = gl_device->create_program(this->desc); }
	};

	struct Shaders {
		Shader depth_pass;
		Shader opaque_pass;
		Shader hdr2ldr_pass;
		Shader skybox_shader;
	};

	struct Hook {
		void* userdata = nullptr;
		bool pre = false;
		void (*depth_pass_hook)(void*) = nullptr;
		void (*opaque_pass_hook)(void*) = nullptr;
	};

	struct RendererConfig {
		uint32_t fb_width, fb_height;
		Shaders shaders;
		gen::gl::Device* gl_device;
		gen::pear3d::MeshHandler* mesh_manager;
		gen::graphics::TexturePacker* tx_packer;
		gen::Buffer<Hook> hooks;
		size_t max_lights = 32;
		gen::pear3d::Mesh* skybox; // skybox cube mesh; nullptr means no skybox
	};

	struct PointLight {
		gen::vec3f position = gen::vec3f(0.0f, 0.0f, 0.0f);
		float radius = 0.0f;
		gen::Color color = gen::Color(1.0f);
		float intensity = 0.0f;
		float falloff = 1.0f;
		float padding[2];
	};

	struct DirLight {
		gen::vec3f direction = gen::vec3f(0.0f, -1.0f, 0.0f);
		float intensity = 1.0f;
		gen::Color color = gen::Color(1.0f);
	};

	struct Ldr2HdrConfig {
		float gamma = 2.2f;
		float exposure = 1.0f;
	};

	class GEN_API Renderer {
	public:
		gen::Buffer<Object> objects;
		gen::Buffer<PointLight> point_lights;
		DirLight dir_light;
		Camera camera;
		Ldr2HdrConfig hdr2ldr_config;

		gen::gl::Texture skybox_texture;

		virtual void init(const RendererConfig& config) = 0;
		virtual void reload_shaders(const Shaders& shaders) = 0;
		virtual void render() = 0;
	};

	GEN_API Renderer* create_renderer();
}
