#include "mesh_manager.hpp"
#include <float.h>
#include <gongen/common/compression.hpp>

template<typename T>
void calculate_mash_bounds(gen::pear3d::Mesh& mesh, T* vtx, size_t vtx_count) {
	gen::vec3f min(FLT_MAX, FLT_MAX, FLT_MAX);
	gen::vec3f max(FLT_MIN, FLT_MIN, FLT_MIN);
	gen::vec3d point_sum(0.0f, 0.0f, 0.0f);
	for (size_t i = 0; i < vtx_count; i++) {
		gen::vec3f pos = vtx[i];
		point_sum += gen::vec3d(pos.x, pos.y, pos.z);
		if ((min.x > pos.x)) min.x = pos.x;
		if ((min.y > pos.y)) min.y = pos.y;
		if ((min.z > pos.z)) min.z = pos.z;
		if ((max.x < pos.x)) max.x = pos.x;
		if ((max.y < pos.y)) max.y = pos.y;
		if ((max.z < pos.z)) max.z = pos.z;
	}
	gen::vec3f center_of_mass = gen::vec3f(point_sum.x / vtx_count, point_sum.y / vtx_count, point_sum.z / vtx_count);
	float max_distance2 = 0.0f;
	for (size_t i = 0; i < vtx_count; i++) {
		float distance2 = vtx[i].distance_sq(center_of_mass);
		if (distance2 > max_distance2) max_distance2 = distance2;
	}
	mesh.bounding_sphere_pos = center_of_mass;
	mesh.bounding_sphere_radius = gen::sqrt(max_distance2);
	mesh.bounding_box.position = min;
	mesh.bounding_box.size = max - min;
}

gen::mat4f get_node_translation_matrix(gen::loaders::Mesh::Node* node) {
	gen::Buffer<gen::loaders::Mesh::Node*> node_hierarchy;
	gen::loaders::Mesh::Node* transform_node = node;
	while (transform_node) {
		node_hierarchy.add(transform_node);
		transform_node = transform_node->parent;
	}
	gen::mat4f transform_matrix;
	for (size_t i = 0; i < node_hierarchy.size(); i++) {
		size_t i2 = (node_hierarchy.size() - i) - 1;
		transform_matrix = transform_matrix * node_hierarchy[i2]->tranform_matrix;
	}
	return transform_matrix;
}

gen::loaders::Mesh::Attribute* get_primitive_attrib(gen::loaders::Mesh::Primitive* prim, const gen::String prefix, const gen::res::Identifier& id, const gen::String& attr_name) {
	for (auto& a : prim->attributes) {
		if (a.name.starts_with(prefix)) return &a;
	}
	throw gen::RuntimeException::createf("Mesh %s don't have %s attribute(or they can't be found)", gen::res::Identifier::to_str(id).c_str(), attr_name.c_str());
}

template<typename T>
void primitive_process_static_attribs(gen::loaders::Mesh::Primitive* prim, uint32_t& vtx_count, uint32_t& idx_count, gen::mat4f transform_matrix, T*& start_vtx, gen::ByteBuffer* vtx_data, gen::Buffer<uint32_t>* idx_data, const gen::res::Identifier& id, uint8_t mat_id, gen::ByteBuffer* pos_vtx_data, gen::Buffer<uint32_t>* pos_idx_data) {
	gen::loaders::Mesh::Attribute* pos_attr = get_primitive_attrib(prim, "POS", id, "position");
	gen::loaders::Mesh::Attribute* normal_attr = get_primitive_attrib(prim, "NORMAL", id, "normal");
	gen::loaders::Mesh::Attribute* index_attr = get_primitive_attrib(prim, "INDEX", id, "index");
	vtx_count = pos_attr->count;
	idx_count = index_attr->count;
	uint32_t vtx_base = (vtx_data->size() / sizeof(T));
	start_vtx = (T*)vtx_data->reserve(vtx_count * sizeof(T), true);
	uint32_t* idx = idx_data->reserve(idx_count, true);
	T* vtx = start_vtx;
	uint32_t pos_vtx_base = (pos_vtx_data->size() / sizeof(gen::vec3f));
	gen::vec3f* pos_vtx = (gen::vec3f*)pos_vtx_data->reserve(vtx_count * sizeof(gen::vec3f), true);
	for (size_t i = 0; i < vtx_count; i++) {
		gen::vec4f pos4 = transform_matrix * gen::vec4f(pos_attr->f32_3[i].x, pos_attr->f32_3[i].y, pos_attr->f32_3[i].z, 1.0f);
		vtx->pos = gen::vec3f(pos4.x, pos4.y, pos4.z);
		*pos_vtx = gen::vec3f(pos4.x, pos4.y, pos4.z);

		gen::vec4f normal_raw = transform_matrix.inverse().transpose() * gen::vec4f(normal_attr->f32_3[i].x, normal_attr->f32_3[i].y, normal_attr->f32_3[i].z, 0.0f);
		gen::vec3f normalf = gen::normalize(gen::vec3f(normal_raw.x, normal_raw.y, normal_raw.z));
		gen::vec3f normaln = (normalf + gen::vec3f(1.0f)) / 2.0f;
		gen::vec3u8 normal = gen::vec3u8(normaln.x * 255, normaln.y * 255, normaln.z * 255);
		vtx->normal[0] = normal.x;
		vtx->normal[1] = normal.y;
		vtx->normal[2] = normal.z;
		vtx->material_id = mat_id;
		vtx++;
		pos_vtx++;
	}
	uint32_t* pos_idx = pos_idx_data->reserve(idx_count, true);
	for (size_t i = 0; i < idx_count; i++) {
		uint32_t index = 0;
		if (index_attr->type == gen::loaders::Mesh::UINT32) index = index_attr->u32[i];
		else if (index_attr->type == gen::loaders::Mesh::UINT16) index = index_attr->u16[i];
		else if (index_attr->type == gen::loaders::Mesh::UINT8) index = index_attr->u8[i];
		*idx = (index + vtx_base);
		*pos_idx = (index + pos_vtx_base);
		idx++;
		pos_idx++;
	}
}

template<typename T>
void primitive_process_animated_attribs(gen::loaders::Mesh::Primitive* prim, uint32_t& vtx_count, gen::mat4f transform_matrix, T*& start_vtx, gen::ByteBuffer* vtx_data, const gen::res::Identifier& id) {
	gen::loaders::Mesh::Attribute* bone_attr = get_primitive_attrib(prim, "JOINTS", id, "bone ID");;
	gen::loaders::Mesh::Attribute* weight_attr = get_primitive_attrib(prim, "WEIGHTS", id, "bone weights");;
	T* vtx = start_vtx;
	for (size_t i = 0; i < vtx_count; i++) {
		if (bone_attr->type == gen::loaders::Mesh::VEC4_UINT8) {
			vtx->bone_ids[0] = bone_attr->u8_4[i].x;
			vtx->bone_ids[1] = bone_attr->u8_4[i].y;
			vtx->bone_ids[2] = bone_attr->u8_4[i].z;
			vtx->bone_ids[3] = bone_attr->u8_4[i].w;
		}
		if (weight_attr->type == gen::loaders::Mesh::VEC4_FLOAT) {
			uint16_t w0 = (uint16_t)(weight_attr->f32_4[i].x * UINT16_MAX);
			uint16_t w1 = (uint16_t)(weight_attr->f32_4[i].y * UINT16_MAX);
			uint16_t w2 = (uint16_t)(weight_attr->f32_4[i].z * UINT16_MAX);
			uint16_t w3 = (uint16_t)(weight_attr->f32_4[i].w * UINT16_MAX);
			vtx->bone_weights = gen::vec4u16(w0, w1, w2, w3);
		}
		vtx++;
	}
}


template<typename T>
void mesh_process_pos(uint32_t vtx_count, gen::vec3f* vtx, T* vtx_in, uint32_t idx_count, uint32_t* idx, uint32_t* idx_in, uint32_t idx_offset, uint32_t vtx_base) {
	for (size_t i = 0; i < vtx_count; i++) {
		*vtx = vtx_in[i].pos;
		vtx++;
		vtx_in++;
	}
	for (size_t i = 0; i < idx_count; i++) {
		uint32_t index = (*idx_in) - idx_offset;
		*idx = (index + vtx_base);
		idx++;
		idx_in++;
	}
}

struct MeshMat {
	gen::pear3d::Material pear_mat;
	gen::loaders::Mesh::Material* mat;
};
void add_node(gen::Buffer<gen::loaders::Mesh::Node*>& nodes, gen::loaders::Mesh::Node* node) {
	bool already_present = false;
	for (auto& n : nodes) {
		if (n == node) already_present = true;
	}
	if (!already_present) {
		nodes.add(node);
		for (auto& c : node->children) {
			add_node(nodes, c);
		}
	}
}
void add_mat(gen::DynamicArray<MeshMat>& mats, gen::loaders::Mesh::Material* mat, const gen::String& texture_id) {
	bool already_present = false;
	for (auto& n : mats) {
		if (n.mat == mat) already_present = true;
	}
	if (!already_present) {
		MeshMat new_mat;
		new_mat.mat = mat;
		new_mat.pear_mat.name = mat->name;
		new_mat.pear_mat.color = mat->base_color;
		new_mat.pear_mat.roughness = mat->roughness;
		new_mat.pear_mat.metalness = mat->metalness;
		new_mat.pear_mat.texture_id = texture_id;
		mats.add(new_mat);
	}
}

gen::Buffer<gen::res::KindFile> gen::pear3d::MeshHandler::get_files() const {
	Buffer<res::KindFile> files;
	files.add(res::KindFile {
		.extension = "ply",
		.is_optional = true,
		.load { .data = [](KindHandler* handler, void** item, SeekableDataSource* data) {
			Mesh& mesh = ((MeshHandler*) handler)->meshes.get((size_t) *item);
			if (mesh.type != MeshType::COUNT_) throw gen::ResFmtException::createf("Trying to load mesh '%s' twice.", gen::res::Identifier::to_str(mesh.identifier).c_str());
			mesh = ((MeshHandler*) handler)->add_mesh(gen::loaders::Mesh(data, false), mesh.identifier, MeshType::STATIC);
		}},
	});
	files.add(res::KindFile {
		.extension = "glb",
		.is_optional = true,
		.load { .data = [](KindHandler* handler, void** item, SeekableDataSource* data) {
			Mesh& mesh = ((MeshHandler*) handler)->meshes.get((size_t) *item);
			if (mesh.type != MeshType::COUNT_) throw gen::ResFmtException::createf("Trying to load mesh '%s' twice.", gen::res::Identifier::to_str(mesh.identifier).c_str());
			mesh = ((MeshHandler*) handler)->add_mesh(gen::loaders::Mesh(data, false), mesh.identifier, MeshType::STATIC);
		}},
	});
	files.add(res::KindFile {
		.extension = "a.glb",
		.is_optional = true,
		.load { .data = [](KindHandler* handler, void** item, SeekableDataSource* data) {
			Mesh& mesh = ((MeshHandler*) handler)->meshes.get((size_t) *item);
			if (mesh.type != MeshType::COUNT_) throw gen::ResFmtException::createf("Trying to load mesh '%s' twice.", gen::res::Identifier::to_str(mesh.identifier).c_str());
			mesh = ((MeshHandler*) handler)->add_mesh(gen::loaders::Mesh(data, false), mesh.identifier, MeshType::ANIMATED);
		}},
	});
	files.add(res::KindFile {
		.extension = "t.glb",
		.is_optional = true,
		.load { .data = [](KindHandler* handler, void** item, SeekableDataSource* data) {
			Mesh& mesh = ((MeshHandler*) handler)->meshes.get((size_t) *item);
			if (mesh.type != MeshType::COUNT_) throw gen::ResFmtException::createf("Trying to load mesh '%s' twice.", gen::res::Identifier::to_str(mesh.identifier).c_str());
			mesh = ((MeshHandler*) handler)->add_mesh(gen::loaders::Mesh(data, false), mesh.identifier, MeshType::TEXTURED);
		}},
	});
	files.add(res::KindFile{
		.extension = "gmesh",
		.is_optional = true,
		.load {.data = [](KindHandler* handler, void** item, SeekableDataSource* data) {
			Mesh& mesh = ((MeshHandler*)handler)->meshes.get((size_t)*item);
			if (mesh.type != MeshType::COUNT_) throw gen::ResFmtException::createf("Trying to load mesh '%s' twice.", gen::res::Identifier::to_str(mesh.identifier).c_str());
			mesh = ((MeshHandler*)handler)->add_gmesh(data, mesh.identifier);
		}},
	});
	return files;
}

void gen::pear3d::MeshHandler::create(void** item) {
	Mesh* mesh = &this->meshes.emplace();
	mesh->identifier = *((res::Identifier*) *item);
	*item = (void*) this->meshes.index_ptr(mesh);
}
void gen::pear3d::MeshHandler::destroy(void** item) {
	this->meshes.remove_idx((size_t) *item);
	if (this->meshes.empty()) this->clear();
}
void* gen::pear3d::MeshHandler::get_res(void** item) {
	return &this->meshes.get((size_t) *item);
}

gen::pear3d::Mesh gen::pear3d::MeshHandler::add_mesh(const gen::loaders::Mesh& mesh, const res::Identifier& identifier, MeshType type) {
	gen::Buffer<gen::loaders::Mesh::Node*> nodes;
	if (mesh.main_scene) {
		for (auto& n : mesh.main_scene->nodes) add_node(nodes, n);
	}
	else {
		for (auto& s : mesh.scenes) {
			for (auto& n : s.nodes) add_node(nodes, n);
		}
	}
	gen::DynamicArray<MeshMat> mats;
	for (auto& n : nodes) {
		if (!n->mesh) continue;
		for (auto& p : n->mesh->primitives) {
			if (!p.material) continue;
			gen::String texture_id;
			for (auto& p : p.material->properties_str) {
				if (p.name != "texture_id") continue;
				texture_id = p.value;
				break;
			}
			add_mat(mats, p.material, texture_id);
		}
	}

	size_t type_index = (size_t)type;

	size_t total_vtx_count = 0;
	size_t first_vtx_index = vtx_data[type_index].size();
	size_t first_vtx_index_pos = vtx_data[0].size();

	gen::Buffer<gen::loaders::Mesh::Node*> skeleton;

	Mesh new_mesh;
	new_mesh.type = type;
	new_mesh.identifier = identifier;
	new_mesh.idx_count = 0;
	new_mesh.idx_offset = idx_data[type_index].size();
	new_mesh.idx_offset_pos = idx_data[0].size();
	new_mesh.mat_count = mats.size();
	for (auto& m : mats) new_mesh.default_materials.add(m.pear_mat);
	for (auto& n : nodes) {
		if (!n->mesh) continue;
		for (auto& p : n->mesh->primitives) {
			uint32_t mat_id = 0;
			for (size_t i = 0; i < mats.size(); i++) {
				if (p.material == mats[i].mat) mat_id = i;
			}
			gen::mat4f transform_matrix = get_node_translation_matrix(n);
			void* start_vtx;
			uint32_t vtx_count = 0, idx_count = 0;

			if (type == MeshType::STATIC) {
				primitive_process_static_attribs<StaticVertex>(&p, vtx_count, idx_count, transform_matrix, (StaticVertex*&)start_vtx, &vtx_data[type_index], &idx_data[type_index], identifier, mat_id, &vtx_data[0], &idx_data[0]);
			}
			else if (type == MeshType::TEXTURED) {
				primitive_process_static_attribs<TexturedVertex>(&p, vtx_count, idx_count, transform_matrix, (TexturedVertex*&)start_vtx, &vtx_data[type_index], &idx_data[type_index], identifier, mat_id, &vtx_data[0], &idx_data[0]);
				gen::loaders::Mesh::Attribute* uv_attr = get_primitive_attrib(&p, "TEXCOORD", identifier, "texture coord");
				TexturedVertex* vtx = (TexturedVertex*)start_vtx;
				for (size_t i = 0; i < vtx_count; i++) {
					vtx->tex_coord[0] = (uv_attr->f32_2[i].x * UINT16_MAX);
					vtx->tex_coord[1] = (uv_attr->f32_2[i].y * UINT16_MAX);
					vtx++;
				}
			}
			else if (type == MeshType::ANIMATED) {
				if (!n->skin) continue;
				primitive_process_static_attribs<AnimatedVertex>(&p, vtx_count, idx_count, transform_matrix, (AnimatedVertex*&)start_vtx, &vtx_data[type_index], &idx_data[type_index], identifier, mat_id, &vtx_data[0], &idx_data[0]);
				primitive_process_animated_attribs<AnimatedVertex>(&p, vtx_count, transform_matrix, (AnimatedVertex*&)start_vtx, &vtx_data[type_index], identifier);
				new_mesh.bone_count = n->skin->mat_count;
				gen::mat4f inverse_mesh_transform = get_node_translation_matrix(n).inverse();
				auto bind_matrix = n->skin->mat;
				for (auto& j : n->skin->joints) {
					if (j->parent) {
						size_t i = 0;
						bool parent_set = false;
						for (auto& j2 : n->skin->joints) {
							if (j->parent == j2) {
								new_mesh.bone_parents.add(i);
								parent_set = true;
								break;
							}
							i++;
						}
						if(!parent_set) new_mesh.bone_parents.add(SIZE_MAX);
					}
					else new_mesh.bone_parents.add(SIZE_MAX);

					gen::mat4f joint_transform = get_node_translation_matrix(j->parent);
					gen::mat4f bmatrix = *bind_matrix;
					new_mesh.bone_transform.add(inverse_mesh_transform * get_node_translation_matrix(j));
					new_mesh.bone_inversebind.add(bmatrix);
					skeleton.add(j);
					bind_matrix++;
				}
			}
			new_mesh.idx_count += idx_count;
			total_vtx_count += vtx_count;
		}
	}
	calculate_mash_bounds<gen::vec3f>(new_mesh, (gen::vec3f*)vtx_data[0].ptr(first_vtx_index_pos), total_vtx_count);
	if (!skeleton.empty()) {
		for (auto& a : mesh.animations) {
			float max_time = 0.0f;
			MeshAnimation ma;
			ma.name = a.name;
			for (auto& j : skeleton) ma.bones.add({});
			for (auto& c : a.channels) {
				size_t bone_id = 0;
				for (size_t i = 0; i < skeleton.size(); i++) {
					if (skeleton[i] == c.target) bone_id = i;
				}
				auto* bone_animation = &ma.bones[bone_id];
				MeshAnimationSampler sampler;
				sampler.interpolation = c.sampler->interpolation;
				if (c.path == gen::loaders::Mesh::AnimationPath::ROTATION) sampler.is_vec4 = true;
				for (size_t i = 0; i < c.sampler->input.count; i++) {
					sampler.times.add(c.sampler->input.f32[i]);
					if (c.sampler->input.f32[i] > max_time) max_time = c.sampler->input.f32[i];
				}
				for (size_t i = 0; i < c.sampler->output.count; i++) {
					if (sampler.is_vec4) sampler.data.add(&c.sampler->output.f32_4[i], sizeof(gen::vec4f));
					else sampler.data.add(&c.sampler->output.f32_3[i], sizeof(gen::vec3f));
				}
				if (c.path == gen::loaders::Mesh::AnimationPath::TRANSLATION) bone_animation->translation = sampler;
				else if (c.path == gen::loaders::Mesh::AnimationPath::ROTATION) bone_animation->rotation = sampler;
				else if (c.path == gen::loaders::Mesh::AnimationPath::SCALE) bone_animation->scale = sampler;
			}
			ma.time = max_time;
			new_mesh.animations.add(ma);
		}
	}
	return new_mesh;
}
#pragma pack(push, 1)
#define GMESH_FLAG_MATERIALS (1 << 0)
#define GMESH_FLAG_ANIMATED (1 << 2)

const static uint32_t GMESH_MAGIC = GEN_FOURCC(0xC7, 'G', 'M', 0x1);
struct GMeshHeader {
	uint32_t magic;
	uint32_t format;
	uint8_t format_version;
	uint8_t flags;
	uint8_t mat_count;
	uint8_t bone_count;
	uint32_t material_size;
	uint32_t vertex_size;
	uint32_t index_size;
	uint32_t bone_size;
	uint32_t compressed_size;
	uint8_t animation_count;
	uint8_t reserved[3];
};
struct PW24_Material {
	float roughness;
	float metallic;
	float base_color[4];
	float specular_color[3];
};
#pragma pack(pop)
gen::String gmesh_read_string(gen::DataSource* source) {
	uint8_t len;
	source->read(&len, sizeof(uint8_t));
	if (len) {
		gen::String str;
		source->read(str.reserve(len), len);
		return str;
	}
	else return "";
}
template<typename T>
void process_gmesh_pos(T* vtx, size_t vtx_count, gen::vec3f* pos) {
	for (size_t i = 0; i < vtx_count; i++) {
		*pos = vtx->pos;
		pos++;
		vtx++;
	}
}
gen::pear3d::Mesh gen::pear3d::MeshHandler::add_gmesh(gen::DataSource* source, const res::Identifier& identifier) {
	GMeshHeader header;
	source->read(&header, sizeof(GMeshHeader));
	if (header.magic != GMESH_MAGIC) throw gen::RuntimeException::createf("Mesh %s is not a GenMesh File", gen::res::Identifier::to_str(identifier).c_str());
	if ((header.format != GEN_FOURCC_STR("PW24")) || (header.format_version != 4)) throw gen::RuntimeException::createf("GenMesh %s is in unsupported format(only PearWars 2024 Format(version 4) is support)", gen::res::Identifier::to_str(identifier).c_str());
	gen::String name = gmesh_read_string(source);
	MeshType type = MeshType::STATIC;
	if((header.flags & GMESH_FLAG_ANIMATED)) type = MeshType::ANIMATED;
	size_t type_index = (size_t)type;
	Mesh new_mesh;
	new_mesh.type = type;
	new_mesh.identifier = identifier;
	new_mesh.idx_count = 0;
	new_mesh.idx_offset = idx_data[type_index].size();
	new_mesh.idx_offset_pos = idx_data[0].size();
	new_mesh.mat_count = header.mat_count;
	
	size_t first_vtx_offset = vtx_data[type_index].size();
	size_t first_vtx_offset_pos = vtx_data[0].size();

	size_t uncompressed_size = header.bone_size + header.material_size + header.vertex_size + header.index_size;
	gen::ByteBuffer data_raw;
	gen::ByteBuffer data;
	source->read(data_raw.reserve(header.compressed_size), header.compressed_size);
	gen::zlib_compressor()->decompress(data, uncompressed_size, data_raw.ptr(), header.compressed_size);
	uint8_t* material_data = (uint8_t*)(data.ptr());
	uint8_t* vertex_data = (uint8_t*)(material_data + header.material_size);
	uint8_t* index_data = (uint8_t*)(vertex_data + header.vertex_size);
	uint8_t* bone_data = (uint8_t*)(index_data + header.index_size);

	gen::MemoryDataSource mat_source(material_data, header.material_size, false);
	for (size_t i = 0; i < header.mat_count; i++) {
		gen::String mat_name = gmesh_read_string(&mat_source);
		PW24_Material pw24_mat;
		mat_source.read(&pw24_mat, sizeof(PW24_Material));
		auto mat = new_mesh.default_materials.add({});
		mat->name = mat_name;
		mat->roughness = pw24_mat.roughness;
		mat->metalness = pw24_mat.metallic;
		mat->color = gen::Color(pw24_mat.base_color[0], pw24_mat.base_color[1], pw24_mat.base_color[2], pw24_mat.base_color[3]);
	}
	if (!(header.flags & GMESH_FLAG_MATERIALS)) {
		new_mesh.mat_count = 1;
		auto mat = new_mesh.default_materials.add({});
		mat->name = "Default";
		mat->roughness = 0.5f;
		mat->metalness = 0.0f;
		mat->color = gen::Color(1.0f);
	}

	//new_mesh.bone_count = header.bone_count;
	//new_mesh.bone_mats.add((gen::mat4f*)bone_data, header.bone_size / sizeof(gen::mat4f));

	this->vtx_data[type_index].add(vertex_data, header.vertex_size);
	size_t vtx_count = 0;
	size_t base_vtx_index = 0;
	size_t base_vtx_index_pos = 0;
	if (type == MeshType::STATIC) {
		vtx_count = header.vertex_size / sizeof(StaticVertex);
		base_vtx_index = first_vtx_offset / sizeof(StaticVertex);
		process_gmesh_pos<StaticVertex>((StaticVertex*)this->vtx_data[type_index].ptr(first_vtx_offset), vtx_count, (gen::vec3f*)this->vtx_data[0].reserve(vtx_count * sizeof(gen::vec3f)));
	}
	else if (type == MeshType::TEXTURED) {
		vtx_count = header.vertex_size / sizeof(TexturedVertex);
		base_vtx_index = first_vtx_offset / sizeof(TexturedVertex);
		process_gmesh_pos<TexturedVertex>((TexturedVertex*)this->vtx_data[type_index].ptr(first_vtx_offset), vtx_count, (gen::vec3f*)this->vtx_data[0].reserve(vtx_count * sizeof(gen::vec3f)));
	}
	else if (type == MeshType::ANIMATED) {
		vtx_count = header.vertex_size / sizeof(AnimatedVertex);
		base_vtx_index = first_vtx_offset / sizeof(AnimatedVertex);
		process_gmesh_pos<AnimatedVertex>((AnimatedVertex*)this->vtx_data[type_index].ptr(first_vtx_offset), vtx_count, (gen::vec3f*)this->vtx_data[0].reserve(vtx_count * sizeof(gen::vec3f)));
	}
	base_vtx_index_pos = first_vtx_offset_pos / sizeof(gen::vec3f);

	new_mesh.idx_count = header.index_size / sizeof(uint32_t);
	uint32_t* idx_in = (uint32_t*)index_data;
	uint32_t* idx_out = (uint32_t*)this->idx_data[type_index].reserve(new_mesh.idx_count);
	uint32_t* idx_out_pos = (uint32_t*)this->idx_data[0].reserve(new_mesh.idx_count);
	for (size_t i = 0; i < new_mesh.idx_count; i++) {
		uint32_t index = *idx_in;
		*idx_out = (index + base_vtx_index);
		*idx_out_pos = (index + base_vtx_index_pos);
		idx_out++;
		idx_out_pos++;
		idx_in++;
	}
	
	calculate_mash_bounds<gen::vec3f>(new_mesh, (gen::vec3f*)this->vtx_data[0].ptr(first_vtx_offset_pos), vtx_count);
	return new_mesh;
}
void gen::pear3d::MeshHandler::setup_buffers(gen::gl::Device* gl_device, gen::graphics::TexturePacker& tx_packer, bool clear_data) {
	for (size_t i = 0; i < (size_t)MeshType::COUNT_; i++) {
		if (vtx_data[i].empty()) continue;
		gen::gl::BufferDesc desc;
		desc.data = vtx_data[i].ptr();
		desc.size = vtx_data[i].size_bytes();
		vtx_buffer[i] = gl_device->create_buffer(desc);
		desc.data = idx_data[i].ptr();
		desc.size = idx_data[i].size_bytes();
		idx_buffer[i] = gl_device->create_buffer(desc);
		if (clear_data) {
			vtx_data[i].clear(true);
			idx_data[i].clear(true);
		}
		if (i == (size_t)MeshType::POS) {
			vs_format[i].attrib_count = 1;
			vs_format[i].attrib[0] = { 0, gen::gl::Format::RGB32F, 0 };
			vs_format[i].binding[0] = { sizeof(gen::vec3f), false };
		} else if (i == (size_t)MeshType::STATIC) {
			vs_format[i].attrib_count = 2;
			vs_format[i].attrib[0] = { 0, gen::gl::Format::RGB32F, offsetof(StaticVertex, pos) };
			vs_format[i].attrib[1] = { 0, gen::gl::Format::RGBA8, offsetof(StaticVertex, normal) };
			vs_format[i].binding[0] = { sizeof(StaticVertex), false };
		} else if (i == (size_t)MeshType::TEXTURED) {
			vs_format[i].attrib_count = 3;
			vs_format[i].attrib[0] = { 0, gen::gl::Format::RGB32F, offsetof(TexturedVertex, pos) };
			vs_format[i].attrib[1] = { 0, gen::gl::Format::RGBA8, offsetof(TexturedVertex, normal) };
			vs_format[i].attrib[2] = { 0, gen::gl::Format::RG16, offsetof(TexturedVertex, tex_coord) };
			vs_format[i].binding[0] = { sizeof(TexturedVertex), false };
		} else if (i == (size_t)MeshType::ANIMATED) {
			vs_format[i].attrib_count = 5;
			vs_format[i].attrib[0] = { 0, gen::gl::Format::RGB32F, offsetof(AnimatedVertex, pos) };
			vs_format[i].attrib[1] = { 0, gen::gl::Format::RGBA8, offsetof(AnimatedVertex, normal) };
			vs_format[i].attrib[2] = { 0, gen::gl::Format::RG16, offsetof(AnimatedVertex, pos) };
			vs_format[i].attrib[3] = { 0, gen::gl::Format::RGBA8UI, offsetof(AnimatedVertex, bone_ids) };
			vs_format[i].attrib[4] = { 0, gen::gl::Format::RGBA16, offsetof(AnimatedVertex, bone_weights) };
			vs_format[i].binding[0] = { sizeof(AnimatedVertex), false };
		}
	}

	for (auto& m : meshes) {
		m.default_type.mesh = &m;
		m.default_type.flags = DEFAULT_FLAGS_OPAQUE;
		for (auto& mat : m.default_materials) {
			for (auto& t : tx_packer.textures) {
				if (t.name == mat.texture_id) mat.tx_coord = t.coords;
			}
		}
	}
}
void gen::pear3d::MeshHandler::clear() {
	for (size_t i = 0; i < (size_t)MeshType::COUNT_; i++) {
		vtx_data[i].clear();
		idx_data[i].clear();
		meshes.clear();
	}
}
void gen::pear3d::MeshAnimationSampler::get_time_range(float sec, size_t& min, size_t& max) {
	min = 0;
	max = 1;
	if (sec < 0.0f) return;
	for (size_t i = 0; i < times.size(); i++) {
		float time = times[i];
		if (time > sec) {
			min = i - 1;
			max = i;
			break;
		}
	}
}
gen::vec3f gen::pear3d::MeshAnimationSampler::get_data3(float sec) {
	size_t time_start = 0;
	size_t time_end = 0;
	get_time_range(sec, time_start, time_end);
	if (interpolation == gen::loaders::Mesh::Interpolation::STEP) return *((gen::vec3f*)data.ptr(sizeof(gen::vec3f) * time_end));
	float t0 = times[time_start];
	float t1 = times[time_end];
	float n = (sec - t0) / (t1 - t0);
	gen::vec3f d0 = *((gen::vec3f*)data.ptr(sizeof(gen::vec3f) * time_start));
	gen::vec3f d1 = *((gen::vec3f*)data.ptr(sizeof(gen::vec3f) * time_end));
	return gen::vec3f(gen::lerp(d0.x, d1.x, n), gen::lerp(d0.y, d1.y, n), gen::lerp(d0.z, d1.z, n));
}
gen::vec4f slerp(const gen::vec4f& v0, const gen::vec4f& v1, float t) {
	float dot = gen::dot(v0, v1);

	const double DOT_THRESHOLD = 0.9995;
	if (dot > DOT_THRESHOLD) {
		gen::vec4f out = gen::vec4f(gen::lerp(v0.x, v1.x, t), gen::lerp(v0.y, v1.y, t), gen::lerp(v0.z, v1.z, t), gen::lerp(v0.w, v1.w, t));
		return gen::normalize_safe(out);
	}
	float theta = (acosf(dot) * t);
	gen::vec4f v2 = v1 - (v0 * dot);
	v2 = gen::normalize_safe(v2);
	return (v0 * cosf(theta)) + (v2 * sinf(theta));
}
gen::vec4f gen::pear3d::MeshAnimationSampler::get_data4(float sec) {
	size_t time_start = 0;
	size_t time_end = 0;
	get_time_range(sec, time_start, time_end);
	if (interpolation == gen::loaders::Mesh::Interpolation::STEP) return *((gen::vec4f*)data.ptr(sizeof(gen::vec3f) * time_end));
	float t0 = times[time_start];
	float t1 = times[time_end];
	float n = (sec - t0) / (t1 - t0);
	gen::vec4f d0 = *((gen::vec4f*)data.ptr(sizeof(gen::vec4f) * time_start));
	gen::vec4f d1 = *((gen::vec4f*)data.ptr(sizeof(gen::vec4f) * time_end));

	return slerp(d0, d1, n);
}
gen::mat4f gen::pear3d::MeshBoneAnimation::transform_matrix(float sec) {
	gen::mat4f out;
	gen::vec3f pos = translation.get_data3(sec);
	gen::vec4f rot = rotation.get_data4(sec);
	gen::vec3f scl = scale.get_data3(sec);
	out = gen::mat4f::translate(pos) * gen::mat4f::rotate(rot) * gen::mat4f::scale(scl);
	return out;
}
