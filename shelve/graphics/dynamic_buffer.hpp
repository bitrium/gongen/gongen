#pragma once

#include <gongen/gl/gl.hpp>

namespace gen::graphics {
	template<typename TYPE>
	struct DynamicBuffer {
		gen::gl::Buffer gpu_buffer;
		size_t capacity = 0;
		size_t(*capacity_function)(size_t, size_t) = gen::capacity_func::npo2;
		gen::gl::BufferDesc desc;

		void resize(gen::gl::Device* device, size_t size) {
			if (size > capacity) {
				capacity = capacity_function(size, sizeof(TYPE));
				desc.size = capacity;
				gpu_buffer = device->create_buffer(desc);
			}
		}
	public:
		gen::Buffer<TYPE> data;

		DynamicBuffer() {
			this->capacity_function = gen::capacity_func::npo2;
			desc.data = nullptr;
			desc.usage = gen::gl::Usage::DYNAMIC;
			capacity = 0;
		}

		DynamicBuffer(gen::gl::Device* device, size_t start_capacity = 0, size_t(*capacity_function)(size_t, size_t) = gen::capacity_func::npo2, gen::gl::Usage usage = gen::gl::Usage::DYNAMIC) {
			this->capacity_function = capacity_function;
			desc.data = nullptr;
			desc.usage = usage;
			resize(device, start_capacity);
		}

		void update(gen::gl::Device* device, bool clear_data = true) {
			if (!data.empty()) {
				resize(device, data.size_bytes());
				device->update_buffer(gpu_buffer, 0, data.size_bytes(), data.ptr());
				if (clear_data) data.clear();
			}
		}
		gen::gl::Buffer& get() { return gpu_buffer; }
	};
}
