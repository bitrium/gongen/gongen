#include "texture_packer.hpp"
#include <stb_rect_pack.h>

gen::graphics::TexturePacker::~TexturePacker() {
	for (auto& t : textures) {
		if (t.own_data) gen::free((void*)t.data);
	}
}

void gen::graphics::TexturePacker::build(gen::gl::Device* device, uint32_t texture_size, gen::gl::Format format, bool free_data, bool edge_coords_fix, bool force_non_array_texture) {
	stbrp_context ctx;
	gen::Buffer<stbrp_rect> rects;
	gen::Buffer<stbrp_node> nodes;
	rects.reserve(textures.size(), true);
	nodes.reserve((size_t)texture_size);
	stbrp_init_target(&ctx, texture_size, texture_size, nodes.ptr(), texture_size);
	for (size_t i = 0; i < rects.size(); i++) {
		auto* tx = &textures[i];
		auto* rect = &rects[i];
		rect->was_packed = 0;
		rect->id = INT_MAX;
		rect->x = 0;
		rect->y = 0;
		rect->w = tx->w;
		rect->h = tx->h;
	}
	uint32_t layer = 0;
	while (true) {
		bool all_sorted = stbrp_pack_rects(&ctx, rects.ptr(), rects.size());
		for (size_t i = 0; i < rects.size(); i++) {
			if (rects[i].was_packed && (rects[i].id == INT_MAX)) rects[i].id = layer;
		}
		layer++;
		if (all_sorted) break;
	}
	uint32_t bpp = gen::gl::get_texture_size(format);

	gen::ByteBuffer texture_data(texture_size * texture_size * layer * bpp, gen::capacity_func::linear);
	texture_data.resize(texture_data.capacity());
	memset(texture_data.ptr(), 0, texture_size * texture_size * layer * bpp);
	uint8_t* texture_dst = (uint8_t*)texture_data.ptr();

	gen::gl::TextureDesc desc;
	desc.format = format;
	desc.size = gen::vec3z(texture_size, texture_size, 1);
	if (force_non_array_texture) {
		if (layer != 1) throw gen::RuntimeException("Texture Packer Layer Overflow");
		desc.layers = 0;
	}
	else desc.layers = layer;
	desc.data = texture_data.ptr();

	for (size_t i = 0; i < rects.size(); i++) {
		auto* tx = &textures[i];
		auto* rect = &rects[i];
		tx->x = rect->x;
		tx->y = rect->y;
		tx->coords.layer = rect->id;
		if (edge_coords_fix) {
			tx->coords.x = ((rect->x / (float)texture_size) * UINT16_MAX) + 1;
			tx->coords.y = ((rect->y / (float)texture_size) * UINT16_MAX) + 1;
			tx->coords.w = ((rect->w / (float)texture_size) * UINT16_MAX) - 2;
			tx->coords.h = ((rect->h / (float)texture_size) * UINT16_MAX) - 2;
		}
		else {
			tx->coords.x = ((rect->x / (float)texture_size) * UINT16_MAX);
			tx->coords.y = ((rect->y / (float)texture_size) * UINT16_MAX);
			tx->coords.w = ((rect->w / (float)texture_size) * UINT16_MAX);
			tx->coords.h = ((rect->h / (float)texture_size) * UINT16_MAX);
		}
		uint8_t* texture_src = (uint8_t*)tx->data;

		size_t layer_offset = (rect->id * texture_size * texture_size);
		for (size_t y = 0; y < rect->h; y++) {
			size_t dst_y = (rect->y + y);
			size_t dst_offset = layer_offset + (dst_y * texture_size) + rect->x;
			size_t src_offset = (y * rect->w);
			memcpy(texture_dst + (dst_offset * bpp), texture_src + (src_offset * bpp), rect->w * bpp);
		}

		if (free_data) {
			if (tx->own_data) {
				gen::free((void*)tx->data);
			}
			tx->data = nullptr;
			tx->own_data = false;
		}
	}
	//Flipping image because OpenGL is stupid.
	gen::ByteBuffer texture_data2(texture_size * texture_size * desc.layers * bpp, gen::capacity_func::linear);
	for (size_t l = 0; l < desc.layers; l++) {
		size_t layer_off = texture_size * texture_size * bpp * l;
		for (size_t i = 0; i < texture_size; i++) {
			void* dst = texture_data2.ptr(layer_off + (i * texture_size * bpp));
			size_t inv_y = (texture_size - i) - 1;
			void* src = texture_data.ptr(layer_off + (inv_y * texture_size * bpp));
			memcpy(dst, src, texture_size * bpp);
		}
	}
	texture = device->create_texture(desc);
}
