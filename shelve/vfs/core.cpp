#include "vfs.hpp"

void gen::vfs::Path::update_string() {
	if (!segments.empty()) {
		this->str.clear();
		for (PtrIterator<gen::String> it = this->segments.begin(); it != this->segments.end() - 1; it++) {
			this->str.append(*it.get());
			this->str.append("/");
		}
		this->str.append(*(this->segments.end() - 1).get());
	}
	node = vfs->get_node(this->str.c_str());
}
bool gen::vfs::Path::is_directory() const {
	return vfs->is_directory(string().c_str());
}
gen::DynamicArray<gen::String> gen::vfs::Path::enumerate_names() const {
	return vfs->enumerate_names(str);
}

gen::vfs::Vfs::Vfs() :dir_names_list(gen::hash), nodes(gen::hash) {
}
gen::vfs::Vfs::~Vfs() {
	for (auto& n : nodes) delete n;
	for (auto& p : providers) delete p;
}
gen::vfs::Node* gen::vfs::Vfs::get_node(const gen::String& path) const {
	return nodes.get(path, nullptr);
}
bool gen::vfs::Vfs::is_directory(const gen::String& path) const {
	Node* node = get_node(path);
	if (node) return node->is_directory();
	else return (dir_names_list.ptr(path) != nullptr);
}
gen::vfs::Handle* gen::vfs::Vfs::open(const gen::String& path, bool for_writing, bool no_except) const {
	Node* node = get_node(path);
	if (!node) {
		if (for_writing) {
			for (size_t i = 0; i < providers.size(); i++) {
				size_t i2 = (providers.size() - i) - 1;
				auto* provider = providers.get(i2);
				if (provider && provider->support_writing()) {
					provider->write_node(path);
					node = get_node(path);
					if (node) {
						return node->open(true, no_except);
					}
					else {
						if (no_except) return nullptr;
						else throw gen::vfs::FindException(gen::String::createf("Can't find file in Vfs: %s", path.c_str()));
					}
				};
			}
		}
		if (node) return node->open(for_writing, no_except);
		else {
			if (no_except) return nullptr;
			else throw gen::vfs::FindException(gen::String::createf("Can't find file in Vfs: %s", path.c_str()));
		}
	}
	else {
		if (node->is_directory()) {
			if (no_except) return nullptr;
			else throw gen::vfs::OpenException(gen::String::createf("Can't open directory in Vfs: %s", path.c_str()));
		}
		else return node->open(for_writing, no_except);
	}
}
gen::DynamicArray<gen::String> gen::vfs::Vfs::enumerate_names(const gen::String& path) const {
	auto* list = dir_names_list.ptr(path);
	if (list) {
		return *list;
	}
	else return DynamicArray<String>();
}
void gen::vfs::Vfs::add_dir_item(const gen::String& dir, const gen::String& item) {
	if (item.empty()) return;
	auto* list = dir_names_list.ptr(dir);
	if (!list) {
		dir_names_list.emplace(dir);
		list = dir_names_list.ptr(dir);
	}
	for (auto& i : *list) {
		if (i == item) return;
	}
	list->add(item);
}
void gen::vfs::Vfs::add_node(const gen::String& path, Node* node) {
	nodes.add(path, node);

	gen::Path parent_path = gen::Path(path).parent();
	gen::String item_name = gen::Path(path).name();
	do {
		add_dir_item(parent_path.string(), item_name);
		item_name = parent_path.name();
		parent_path = parent_path.parent();
	} while (!parent_path.is_empty());
}
