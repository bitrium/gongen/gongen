#include <miniz.h>
#define GEN_MINIZ_HEADER
#include "vfs.hpp"

gen::vfs::zip::Provider::~Provider() {
	for (auto& a : archives) {
		gen::free(a.zip);
		gen::free(a.zip_data_buffer);
		if (a.own_input) delete a.input;
	}
}

void mz_raise_exception(mz_zip_archive* zip) {
	throw gen::vfs::ZipMountException(mz_zip_get_error_string(mz_zip_get_last_error(zip)));
}

gen::vfs::zip::Handle::Handle(mz_zip_archive* zip, uint32_t index, bool no_except) {
	size_t size;
	void* file_buffer = mz_zip_reader_extract_to_heap(zip, index, &size, 0);
	if (!file_buffer) {
		if (no_except) return;
		else mz_raise_exception(zip);
	}
	source.setup(file_buffer, size, true);
}

struct zip_file_entry {
	uint32_t index;
	gen::Path path;
	bool is_directory;
	size_t compressed_size;
	size_t uncompressed_size;
};

size_t mz_read_zip(void* pUser, mz_uint64 file_offset, void* pBuf, size_t n) {
	gen::SeekableDataSource* input = (gen::SeekableDataSource*)pUser;
	input->seek(file_offset, gen::Seek::SET, true);
	return input->read(pBuf, n, true);
}

void* mz_alloc(void* opaque, size_t items, size_t size) {
	return gen::alloc(items * size);
}
void mz_free(void* opaque, void* address) {
	gen::free(address);
}
void* mz_realloc(void* opaque, void* address, size_t items, size_t size) {
	return gen::realloc(address, items * size);
}

bool gen::vfs::zip::Provider::mount(gen::SeekableDataSource* input, bool own_input, const gen::Path& vfs_path, bool no_except) {
	Archive archive;
	archive.input = input;
	archive.own_input = own_input;

	size_t data_size = input->size();
	archive.zip_data_buffer = gen::alloc(data_size);
	if (input->read(archive.zip_data_buffer, data_size, no_except) < data_size) {
		gen::free(archive.zip_data_buffer);
		if (no_except) return false;
		else throw gen::vfs::ZipMountException("Can't read data source");
	}
	archive.zip = gen::tcalloc<mz_zip_archive>();
	archive.zip->m_pIO_opaque = input;
	archive.zip->m_pRead = mz_read_zip;
	archive.zip->m_pAlloc_opaque = nullptr;
	archive.zip->m_pAlloc = mz_alloc;
	archive.zip->m_pFree = mz_free;
	archive.zip->m_pRealloc = mz_realloc;

	if (!mz_zip_reader_init(archive.zip, data_size, 0)) {
		if (no_except) return false;
		else mz_raise_exception(archive.zip);
	}
	mz_uint file_count = mz_zip_reader_get_num_files(archive.zip);
	mz_zip_archive_file_stat file_stat;

	gen::DynamicArray<zip_file_entry> file_entries;
	for (size_t i = 0; i < file_count; i++) {
		mz_zip_reader_file_stat(archive.zip, i, &file_stat);
		zip_file_entry* entry = file_entries.add(zip_file_entry());
		entry->index = file_stat.m_file_index;
		entry->is_directory = file_stat.m_is_directory;
		entry->path = gen::Path(file_stat.m_filename);
		entry->compressed_size = file_stat.m_comp_size;
		entry->uncompressed_size = file_stat.m_uncomp_size;
	}
	for (auto& e : file_entries) {
		vfs->add_node((vfs_path / e.path).string(), new gen::vfs::zip::Node(this, archive.zip, e.index, e.is_directory));
	}

	return true;
}
