#pragma once

#include <gongen/common/containers/hashmap.hpp>
#include <gongen/common/containers/dynamic_array.hpp>
#include <gongen/common/path.hpp>
#include <gongen/common/hash.hpp>
#include <gongen/common/data_source.hpp>
#include <gongen/common/platform/system.hpp>
#include <gongen/common/error.hpp>

#ifndef GEN_MINIZ_HEADER
struct mz_zip_archive;
#endif

namespace gen::vfs {
	GEN_EXCEPTION_TYPE(Exception, RuntimeException, "Vfs Error");
	GEN_EXCEPTION_TYPE(FindException, Exception, "Vfs Find Error");
	GEN_EXCEPTION_TYPE(OpenException, Exception, "Vfs Open Error");
	GEN_EXCEPTION_TYPE(MountException, Exception, "Vfs Mount Error");
	GEN_EXCEPTION_TYPE(ClearException, Exception, "Vfs Clear Error");
	GEN_EXCEPTION_TYPE(ZipMountException, MountException, "Vfs Zip Mount Error");

	class GEN_API Provider;
	class GEN_API Vfs;
	class GEN_API Handle;

	class GEN_API Node {
	public:
		virtual ~Node() {}

		virtual bool writeable() const = 0;
		virtual bool is_directory() const = 0;
		virtual Handle* open(bool for_writing = false, bool no_except = false) = 0;
	};

	class GEN_API Path : public AbstractPath {
		Vfs* vfs;
		Node* node = nullptr;

		void update_string() override;
		AbstractPath* copy() const override {
			return new Path(*this);
		}
	public:
		explicit Path(Vfs* vfs, bool root = false) : AbstractPath(root) { this->vfs = vfs; this->update_string(); }
		explicit Path(Vfs* vfs, const String& str) : AbstractPath(str) { this->vfs = vfs; this->update_string(); }
		explicit Path(Vfs* vfs, const char* c_str) : AbstractPath(c_str) { this->vfs = vfs; this->update_string(); }
		Path(const Path& copied) : AbstractPath(copied.str) {
			this->vfs = copied.vfs;
			this->node = copied.node;
			this->update_string();
		}
		~Path() = default;

		Vfs* get_vfs() const { return this->vfs; }

		bool exists() const override { return this->node; }
		bool readable() const override { return this->node; }
		bool writeable() const override { if (!node) return false; else return node->writeable(); }
		bool is_directory() const override;

		Path& to_resolved() override { return *this; };

		DynamicArray<String> enumerate_names() const override;
	};

	class GEN_API Handle :public gen::SeekableWritableDataSource {
	public:
		virtual ~Handle() {}
		virtual uint64_t time_modified(bool no_except = false) { return 0; };
	};

	class GEN_API Provider {
	public:
		virtual ~Provider() {}
		virtual bool support_writing() const = 0;
		virtual void write_node(const gen::String& path) = 0;
	};

	class GEN_API Vfs {
		gen::LinkedList<Provider*> providers;
		gen::HashMap<gen::String, gen::DynamicArray<gen::String>, 32> dir_names_list;
		gen::HashMap<gen::String, Node*, 32> nodes;

		void add_dir_item(const gen::String& dir, const gen::String& item);
	public:
		Vfs();
		~Vfs();

		template<typename T>
		T* add_provider(T* provider) {
			T* provider_ptr = (T*)providers.add(provider);
			provider_ptr->vfs = this;
			return provider_ptr;
		}
		Node* get_node(const gen::String& path) const;
		bool is_directory(const gen::String& path) const;
		//WARNING: Opening file for writing is not safe to do when other threads are accesing vfs and providers(but accesing node's and handle's is OK)
		Handle* open(const gen::String& path, bool for_writing = false, bool no_except = false) const;
		DynamicArray<String> enumerate_names(const gen::String& path) const;
		void add_node(const gen::String& path, Node* node);
	};

	namespace dir {
		class GEN_API Handle :public gen::vfs::Handle {
			gen::file::File file;
		public:
			Handle(const gen::file::Path& path, gen::file::Mode mode, bool no_except) :file(path, mode, no_except) {}

			size_t size(bool no_except = false) override { return file.size(no_except); }
			size_t cursor(bool no_except = false) override { return file.cursor(no_except); }
			size_t seek(ptrdiff_t offset, Seek type = Seek::SET, bool no_except = false) override { return file.seek(offset, type, no_except); }

			size_t read(void* data, size_t size, bool no_except = false) override { return file.read(data, size, no_except); }
			size_t write(const void* data, size_t size, bool no_except = false) override { return file.write(data, size, no_except); }
			bool flush(bool no_except = false) override { return file.flush(no_except); }

			uint64_t time_modified(bool no_except = false) override { return file.time_modified(no_except); }
		};
		class GEN_API Node : public gen::vfs::Node {
			gen::file::Path path;
			bool read_only;
		public:
			Node(const gen::file::Path& path, bool read_only) : path(path) { this->read_only = read_only; }
			bool writeable() const { return (!read_only && path.writeable()); }
			bool is_directory() const { return path.is_directory(); }
			Handle* open(bool for_writing = false, bool no_except = false);
		};
		class GEN_API Provider :public gen::vfs::Provider {
			gen::vfs::Vfs* vfs;

			bool write_dir_set = false;
			gen::file::Path write_dir;
			gen::Path vfs_write_dir;
		public:
			void mount(const gen::file::Path& path, const gen::Path& vfs_path, bool recursive = true, bool read_only = true);
			void set_write_dir(const gen::file::Path& dir, const gen::Path& vfs_dir, bool mount_dir = false, bool mount_recursive = true, bool mount_read_only = true) {
				write_dir_set = true;
				write_dir = dir;
				vfs_write_dir = vfs_dir;
				if (!dir.exists()) gen::file::make_directory(dir);
				if (mount_dir) mount(dir, vfs_dir, mount_recursive, mount_read_only);
			}
			bool support_writing() const override { return true; }
			void write_node(const gen::String& path) override;

			friend class gen::vfs::Vfs;
		};
	}

	namespace zip {
		class GEN_API Handle :public gen::vfs::Handle {
			gen::MemoryDataSource source;
		public:
			Handle(mz_zip_archive* zip, uint32_t index, bool no_except);

			size_t size(bool no_except = false) { return source.size(no_except); }
			size_t cursor(bool no_except = false) { return source.cursor(no_except); }
			size_t seek(ptrdiff_t offset, Seek type = Seek::SET, bool no_except = false) { return source.seek(offset, type, no_except); }

			size_t read(void* data, size_t size, bool no_except = false) { return source.read(data, size, no_except); }
			size_t write(const void* data, size_t size, bool no_except = false) { return 0; }
			bool flush(bool no_except = false) { return true; }
		};
		class GEN_API Node : public gen::vfs::Node {
			mz_zip_archive* zip;
			uint32_t index;
			bool directory;
		public:
			Node(gen::vfs::Provider* provider, mz_zip_archive* zip, uint32_t index, bool is_directory) {
				this->zip = zip;
				this->index = index;
				this->directory = is_directory;
			}
			bool writeable() const { return false; }
			bool is_directory() const { return directory; }
			Handle* open(bool for_writing = false, bool no_except = false) {
				if (for_writing) return nullptr;
				else {
					if (directory) return nullptr;
					else return new Handle(zip, index, no_except);
				}
			}
		};
		class GEN_API Provider : public gen::vfs::Provider {
			struct Archive {
				gen::SeekableDataSource* input = nullptr;
				bool own_input = false;

				void* zip_data_buffer = nullptr;
				mz_zip_archive* zip;
			};
			gen::Buffer<Archive> archives;

			gen::vfs::Vfs* vfs;
		public:
			~Provider();
			bool mount(gen::SeekableDataSource* input, bool own_input, const gen::Path& vfs_path, bool no_except = false);

			bool support_writing() const override { return false; }
			void write_node(const gen::String& path) override {}

			friend class gen::vfs::Vfs;
		};
	}
}
