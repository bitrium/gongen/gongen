#include "vfs.hpp"

gen::vfs::dir::Handle* gen::vfs::dir::Node::open(bool for_writing, bool no_except) {
	if (is_directory()) return nullptr;
	else {
		if (for_writing) {
			if (read_only) return nullptr;
			else {
				gen::file::make_directory(gen::file::Path(path.parent()));
				return new Handle(path, gen::file::Mode::WRITE, no_except);
			}
		}
		else {
			return new Handle(path, gen::file::Mode::READ, no_except);
		}
	}
}
void gen::vfs::dir::Provider::mount(const gen::file::Path& path, const gen::Path& vfs_path, bool recursive, bool mount_read_only) {
	auto children = path.enumerate_names();
	for (auto& child : children) {
		gen::file::Path path2 = (path / child);
		gen::Path vfs_path2 = (vfs_path / child);
		if (recursive && path2.is_directory()) {
			mount(path2, vfs_path2, recursive, mount_read_only);
		}
		vfs->add_node(vfs_path2.string(), new gen::vfs::dir::Node(path2, mount_read_only));
	}
}

void gen::vfs::dir::Provider::write_node(const gen::String& path) {
	if (write_dir_set) {
		vfs->add_node(path, new gen::vfs::dir::Node(gen::file::Path(vfs_write_dir / gen::file::Path(path)), false));
	}
}
