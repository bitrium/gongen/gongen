# Gong Engine

**GongEn** is a free and open-source modular game engine written in C++, licensed under the [3-Clause BSD License](./LICENSE). GongEn is still **under heavy development** in the alpha phase and **for now, alternatives should be strongly considered for serious projects**.

**Links**:

- [Discord server](https://discord.gg/kZg7GJkTGk) - discussion, support, announcements.

A website for the project (possibly including more explanations and a module list) does not yet exist, but can be expected to appear with the project's further development.

## Requirements

The engine **supports *Linux* and *Windows 7+***, but *Android* and *Web Browser* support is planned. Other platforms are under consideration, but ***Apple* is likely never getting support** (unless we find an active maintainer who has enough patience and uses Apple devices).

GongEn uses C++20. Currently, the only supported C++ compiler is [**CLang**](https://clang.llvm.org/), but other ones may work too. Building GongEn the official way requires [**CMake**](https://cmake.org/).

Various modules use different libraries, but all of them can easily be downloaded, prepared and/or compiled using our dependency manager, **CGen**. It requires [Python 3.10+](https://www.python.org/), and the `gongsf` package (a custom serialization format library made by us – [GSF](https://gitlab.com/bitrium/gongen/gsf)). The dependency manager can be installed using `pip install cgen` or if you provide for modifying the build system, `pip install -e ./cgen` while in the GongEn's root directory.

## Known issues

As mentioned above, GongEn is still in the alpha phase and currently has many issues. Modules `graphics`, `physics` and `vfs` are currently pending major reworks (their sources were not readded during the last restructurizaiton of the project and sit in the `shelve/` directory). For the other modules, this is a list of known issues:
- small allocations in the event system
- hash maps are very large because they use a buffer for every bucket (each taking 40 bytes)
- impl pointers require small allocations (especially problematic for Files, Threads and other utility objects)
- impl pointers could use some macros to make usage easier (both multi-impl and single-impl systems)
- networking needs benchmarking and possibly optimization
- networking api should be more consistent with other parts of the engine (impl pointers instead of subclasses?)
- mesh loaders are chaotic
- resource identifiers could use a single char array allocation instead of four strings
- windows should also use impl pointers instead of creating a subclass per implementation
- resource kinds have no indication of their handled type (make macros for their declarations?)
- compressors and ciphers should use the impl system after it's introduced

## Usage

GongEn is generally intended to be built for every project from source with the preferred configuration. This allows for every project to only use selected modules decreasing the product's size. Some customization is also available through build options.

### Obtaining the source code

Currently, the source code is best obtained manually:

```
git clone https://gitlab.com/bitrium/gongen/gongen.git
```

### Getting dependencies

It is recommended to get dependencies using our dependency manager CGen:

```
cgen pull -e "[gongen repository path]"
cgen obtain [optional list of wanted dependencies]
cgen cmake # generates the CMake dependency configuration
```

### Setting up CMake

GongEn can be added to the project's CMake in a few steps:

1. Set the variable `gongen_directory` to the GongEn's repository path
2. Include the [GongEn's CMake](cmake/gongen.cmake)
3. Configure GongEn using the CMake macro `gongen_config`
4. Link GongEn to your target

Example code:

```cmake
...
set(gongen_directory [gongen repository path])
include(${gongen_directory}/cmake/gongen.cmake)
gongen_config(MODULES common [other modules you use])
target_link_libraries([your target] gongen)
...
```

GongEn's CMake will auto-include CGen-generated CMake dependency config (`.cgen/deps.cmake`) if it exists.

Alternatively, if you don't want to use CGen, the dependencies can be added manually using the `gongen_add_dep` macro for each required dependency.

## Contributing

Contributions are welcome.

It would be the best if submitted code followed the [style guidelines](./STYLE.md). Before proposing larger changes in code, [create an issue](https://gitlab.com/bitrium/gongen/gongen/-/issues/new) to discuss the concept. Issues themselves are naturally also considered contributions and reporting bugs or submitting feature requested will be appreciated.

## Directory structure

`/cmake` - building scripts (also intended for external projects)
`/cgen` - Python package with the dependency manager
`/deps` - dependency configurations for CGen
`/include` - include directory of the engine
`/src` - sources of the engine
`/examples` - sources of example applications using the engine
`/test` - tests and testing framework of the engine
