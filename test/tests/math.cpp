#include <gtest.hpp>

#include <gongen/common/math/vector.hpp>

using namespace gen;

GTEST(gun) {
	gen::vec2d vec(0.5, 12.3);
	GTEST_ASSERT(gen::abs(vec.distance() - 12.310158) <= 0.00001);
	return 0;
}
