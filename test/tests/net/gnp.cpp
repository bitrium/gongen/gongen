#include <gtest.hpp>

#include <gongen/net/protocol.hpp>

using namespace gen;
using namespace gen::net::gnp;

const char* CUSTOM_RESPONSE = "Some app-defined information...";
const char* SERVER_NAME = "A cool server!";

#define TIMEOUT 10

static int status_seeker(uint16_t port) {
	StatusSeeker seeker;
	tcp::Socket handler_sock(ip::resolve("localhost", { String::createf("%hu", port) }).get(), false);
	uint16_t serial = seeker.request_protocol_info(handler_sock, TIMEOUT);
	ByteBuffer peer_buf;
	while (true) {
		if (!handler_sock.check(TIMEOUT * 1000)) continue;
		if (!seeker.handle(handler_sock, handler_sock, peer_buf, 2048)) continue;
		Response<ProtocolInfo> res = seeker.get_protocol_info(serial);
		if (!res.is_success()) return -4;
		if (res.get().protocol_version != VERSION_MINOR) return -5;
		Response<ByteBuffer> res_custom = seeker.get_custom(serial + 1);
		if (res_custom.state() != RESPONSE_UNKNOWN) return -6;
		handler_sock.close();
		break;
	}
	handler_sock.reopen();
	serial = seeker.request_custom(handler_sock, TIMEOUT, "test", 0);
	while (true) {
		if (!handler_sock.check(TIMEOUT * 1000)) continue;
		if (!seeker.handle(handler_sock, handler_sock, peer_buf, 2048)) continue;
		Response<ByteBuffer> res = seeker.get_custom(serial);
		if (!res.is_success()) return -8;
		if (res.get().size() != strlen(CUSTOM_RESPONSE) || memcmp((void*) res.get().ptr(), (void*) CUSTOM_RESPONSE, strlen(CUSTOM_RESPONSE)) != 0) return -9;
		handler_sock.close();
		break;
	}
	handler_sock.reopen();
	serial = seeker.request_all(handler_sock, TIMEOUT);
	while (true) {
		if (!handler_sock.check(TIMEOUT * 1000)) continue;
		if (!seeker.handle(handler_sock, handler_sock, peer_buf, 2048)) continue;
		Response<ServerInfo> res = seeker.get_server_info(serial);
		if (res.state() == RESPONSE_PENDING) continue;
		if (!res.is_success()) return -10;
		if (res.get().name.size() != strlen(SERVER_NAME) || memcmp((void*) res.get().name.c_str(), (void*) SERVER_NAME, strlen(SERVER_NAME)) != 0) return -11;
		handler_sock.close();
		break;
	}
	return 1;
}

GTEST(gnp_status) {
	tcp::Socket handler_sock(ip::Endpoint(), true);
	StatusHandler handler;
	handler.custom[{"test", 0}].add(CUSTOM_RESPONSE, strlen(CUSTOM_RESPONSE));
	handler.server_info.name = SERVER_NAME;

	Thread thread([](void* port) { return (void*) (intptr_t) status_seeker((uint16_t) (intptr_t) port); }, (void*) (intptr_t) handler_sock.endpoint().port);
	int handled = 0;
	tcp::Socket* seeker_sock = nullptr;
	ByteBuffer peer_buf;
	int attempts = 0;
	while (handled < 3) {
		if (!seeker_sock) {
			if (!handler_sock.check(TIMEOUT * 1000)) return (int) (intptr_t) thread.rethrow_exception().join();
			seeker_sock = handler_sock.accept(true);
		}
		if (attempts > 3) return -2;
		attempts++;
		if (!seeker_sock->check(TIMEOUT * 1000)) continue;
		if (!handler.handle(*seeker_sock, *seeker_sock, peer_buf, 2048)) continue;
		attempts = 0;
		seeker_sock->close();
//		delete seeker_sock;
		seeker_sock = nullptr;
		handled++;
	}
	return (int) (intptr_t) thread.rethrow_exception().join();
}

static int handshake_guest(uint16_t port) {
	HandshakeGuest handshake;
	tcp::Socket server_sock(ip::resolve("localhost", { String::createf("%hu", port) }).get(), false);
	ByteBuffer peer_buf;
	handshake.init(server_sock);
	int attempts = 0;
	while (true) {
		if (attempts > 3) return -2;
		attempts++;
		if (!server_sock.check(TIMEOUT * 1000)) continue;
		if (handshake.handle(server_sock, server_sock, peer_buf, 2048)) break;
		attempts = 0;
	}
	ApplicationHandler handler;
	handler.custom(server_sock, APP_CUSTOM_MIN, 4);
	server_sock.write("test", 4);
	gsf::Article gsf(gsf::Schema(nullptr));
	gsf.access_root().encode_i32(-12);
	handler.gsf(server_sock, gsf);
	server_sock.close();
	return 1;
}

GTEST(gnp_handshake) {
	tcp::Socket server_sock(ip::Endpoint(), true);

	Thread thread([](void* port) { return (void*) (intptr_t) handshake_guest((uint16_t) (intptr_t) port); }, (void*) (intptr_t) server_sock.endpoint().port);
	ByteBuffer peer_buf;
	HandshakeHost handshake;
	if (!server_sock.check(TIMEOUT * 1000)) return (int) (intptr_t) thread.rethrow_exception().join();
	tcp::Socket* client_sock = server_sock.accept(true);
	int attempts = 0;
	while (true) {
		if (attempts > 3) return -1;
		attempts++;
		if (!client_sock->check(TIMEOUT * 1000)) continue;
		if (handshake.handle(*client_sock, *client_sock, peer_buf, 2048)) break;
		attempts = 0;
	}
	ApplicationHandler handler;
	bool is_complete = false;
	handler.user = &is_complete;
	handler.custom_handlers[APP_CUSTOM_MIN] = [](void* user, uint8_t* data, uint32_t size, WritableDataSource& out, ByteBuffer& peer_buf) {
		if (size != 4 || memcmp(data, "test", size) != 0) return;
		*(bool*) user = true;
	};
	handler.gsf_handler_other = [](void* user, gsf::Article gsf, WritableDataSource& out, ByteBuffer& peer_buf) {
		if (gsf.access_root().decode_i32() != -12) return;
		*(bool*) user = true;
	};
	if (!client_sock->check(TIMEOUT * 1000)) return -3;
	handler.handle(*client_sock, *client_sock, peer_buf, 2048);
	if (!is_complete) return -4;
	is_complete = false;
	if (!client_sock->check(TIMEOUT * 1000)) return -5;
	handler.handle(*client_sock, *client_sock, peer_buf, 2048);
	if (!is_complete) return -6;
	client_sock->close();
	return (int) (intptr_t) thread.rethrow_exception().join();
}
