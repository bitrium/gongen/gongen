#include <gtest.hpp>

#include <gongen/net/providers.hpp>

using namespace gen;
using namespace gen::net;

GTEST(net_providers) {
	ServerProvider server(communication_tcp(), ip::Endpoint());
	event::Hopper* hopper = event::hopper(event::all_events, &server);
	server.open();
	server.start_thread();
	ClientProvider client(communication_tcp(), server.our_endpoint());
	client.connect();
	client.start_thread();
	hopper->poll_wait();
	GTEST_ASSERT(hopper->events.size() == 1);
	for (event::Event* base : hopper->events) {
		GTEST_ASSERT(base->type == event::type("net/connection"));
		auto* event = (ConnectionEvent*) base;
		GTEST_ASSERT(event->connected);
	}
	hopper->clear();
	client.send(gnp::APP_CUSTOM_MIN, "abc", 3);
	hopper->poll_wait();
	GTEST_ASSERT(hopper->events.size() == 1);
	for (event::Event* base : hopper->events) {
		GTEST_ASSERT(base->type == event::type("net/packet"));
		auto* packet = (PacketCustom*) base;
		GTEST_ASSERT(packet->type_id == gnp::APP_CUSTOM_MIN);
		GTEST_ASSERT_MSG(packet->data.size() == 3, "%zu", packet->data.size());
		GTEST_ASSERT_MSG(memcmp(packet->data.ptr(), "abc", 3) == 0, "%.3s", packet->data.ptr());
	}
	hopper->clear();
	client.disconnect();
	hopper->poll_wait();
	GTEST_ASSERT(hopper->events.size() == 1);
	for (event::Event* base : hopper->events) {
		GTEST_ASSERT(base->type == event::type("net/connection"));
		auto* event = (ConnectionEvent*) base;
		GTEST_ASSERT(!event->connected);
	}
	hopper->clear();
	server.close();
	return 0;
}
