#include <gtest.hpp>

#include <gongen/common/codecs.hpp>
#include <gongen/common/random.hpp>

GTEST(base64) {
	gen::rand::Random rng;
	rng.rng = gen::rand::rng_lcg();
	rng.seed();
	for (size_t i = 0; i < 128; i++) {
		gen::String original;
		for (char* ptr = original.reserve(rng.u16(500)); ptr - original.c_str() < original.size(); ptr++) {
			*ptr = rng.alnum();
		}
		gen::String base64 = gen::base64::encode((uint8_t*) original.c_str(), original.size() + 1);
		gen::ByteBuffer converted = gen::base64::decode(base64);
		GTEST_ASSERT_MSG(strcmp(original.c_str(), (char*) converted.ptr()) == 0, "\noriginal: %s\nbase64: %s\ndecoded: %s\n", original.c_str(), base64.c_str(), converted.ptr());
	}
	for (size_t i = 0; i < 256; i++) {
		gen::ByteBuffer original;
		for (uint8_t* ptr = original.reserve(rng.u16(10000)); ptr - original.ptr() < original.size(); ptr++) {
			*ptr = rng.u8();
		}
		gen::ByteBuffer converted = gen::base64::decode(gen::base64::encode(original));
		GTEST_ASSERT(converted == original);
	}
	return 0;
}
