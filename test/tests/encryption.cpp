#include <gtest.hpp>

#include <gongen/common/encryption.hpp>
#include <gongen/common/codecs.hpp>
#include <gongen/common/error.hpp>

GTEST(rsa) {
	gen::Cypher cypher;
	gen::cypher_rsa(&cypher, 2048);
	gen::ByteBuffer alice_skey;
	gen::ByteBuffer alice_pkey;
	cypher.key_gen(alice_skey, alice_pkey);
	gen::ByteBuffer bob_skey;
	gen::ByteBuffer bob_pkey;
	cypher.key_gen(bob_skey, bob_pkey);
	gen::ByteBuffer mallory_skey;
	gen::ByteBuffer mallory_pkey;
	cypher.key_gen(mallory_skey, mallory_pkey);

	gen::ByteBuffer message;
	message.add("Hello!", 7);
	gen::ByteBuffer telegram;
	cypher.encrypt(bob_pkey, telegram, message);
	message.clear();
	try {
		cypher.decrypt(mallory_skey, message, telegram);
		return -1; // decrypt should throw
	} catch (const gen::RuntimeException&) {}
	message.clear();
	cypher.decrypt(bob_skey, message, telegram);
	GTEST_ASSERT(strcmp((char*) message.ptr(), "Hello!") == 0);
	message.clear();
	telegram.clear();

	message.add("Alice here.", 12);
	cypher.sign(mallory_skey, telegram, message);
	try {
		cypher.verify(alice_pkey, telegram, message);
		return -2; // verify should throw
	} catch (const gen::RuntimeException&) {}
	telegram.clear();
	cypher.sign(alice_skey, telegram, message);
	cypher.verify(alice_pkey, telegram, message);

	return 0;
}
