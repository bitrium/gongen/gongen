#include <gtest.hpp>

#include <gongen/common/containers.hpp>

using namespace gen;

GTEST(string) {
	String str = "abc";
	GTEST_ASSERT(str.size() == 3);
	GTEST_ASSERT_MSG(strcmp(str.c_str(), "abc") == 0, "string is '%s'", str.c_str());
	str += "def";
	GTEST_ASSERT(str.size() == 6);
	GTEST_ASSERT_MSG(strcmp(str.c_str(), "abcdef") == 0, "string is '%s'", str.c_str());
	str.clear();
	GTEST_ASSERT(str.empty());
	for (size_t i = 0; i < 21; i++) {
		str.append('X');
	}
	str.append('a');
	GTEST_ASSERT(str.is_short());
	str.append('b');
	GTEST_ASSERT(!str.is_short());
	str.append('c');
	GTEST_ASSERT(!str.is_short());
	return 0;
}

GTEST(hashmap) {
	gen::String str1 = "ABC";
	gen::String str2 = "ABC";
	gen::String str3 = "ABD";
	gen::String str4 = "ABCD";

	GTEST_ASSERT(gen::hash(str1) == gen::hash(str2));
	GTEST_ASSERT(gen::hash(str1) != gen::hash(str3));
	GTEST_ASSERT(gen::hash(str1) != gen::hash(str4));

	gen::HashMap<gen::String, gen::String> map(gen::hash);
	map.add(str1, str3);
	map[str3] = str4;
	GTEST_ASSERT(map[str2] == str3);
	GTEST_ASSERT(map.get(str2) == str3);
	GTEST_ASSERT(map.size() == 2);
	return map.size();
}

GTEST(buffer) {
	gen::Buffer<uint32_t> buf;
	buf.add(20);
	buf.add(12);
	buf.add(10);
	GTEST_ASSERT(buf.get(0) == 20 && buf.get(1) == 12 && buf.get(2) == 10);
	buf.add(16);
	GTEST_ASSERT(buf.get(0) == 20 && buf.get(1) == 12 && buf.get(2) == 10 && buf.get(3) == 16);
	buf.add(10);
	GTEST_ASSERT(buf.get(0) == 20 && buf.get(1) == 12 && buf.get(2) == 10 && buf.get(3) == 16 && buf.get(4) == 10);
	return 1;
}

GTEST(so_buffer) {
	gen::SOBuffer<uint32_t, 3> buf;
	buf.add(20);
	buf.add(12);
	buf.add(10);
	GTEST_ASSERT(buf.is_short() && buf.capacity() == 3);
	GTEST_ASSERT(buf.get(0) == 20 && buf.get(1) == 12 && buf.get(2) == 10);
	buf.add(16);
	GTEST_ASSERT(!buf.is_short() && buf.capacity() > 3);
	GTEST_ASSERT(buf.get(0) == 20 && buf.get(1) == 12 && buf.get(2) == 10 && buf.get(3) == 16);
	buf.add(10);
	GTEST_ASSERT(!buf.is_short() && buf.capacity() > 4);
	GTEST_ASSERT(buf.get(0) == 20 && buf.get(1) == 12 && buf.get(2) == 10 && buf.get(3) == 16 && buf.get(4) == 10);
	return 1;
}

GTEST(bytebuf) {
	gen::ByteBuffer buf;
	*buf.reserve(200) = 12;
	buf.add(10);
	GTEST_ASSERT(buf.size() == 201);
	GTEST_ASSERT(buf.capacity() == 256);
	return 1;
}

GTEST(sorted_map) {
	SortedMap<int, char> map([](const char& ch) {
		return ch - 'A' + 1;
	});
	GTEST_ASSERT(map.get_key('D') == 4 && map.get_key('F') == 6);
	map.add('D');
	map.add('C');
	map.add('I');
	map.add('A');
	map.add('F');
	GTEST_ASSERT(*map.get(4) == 'D' && *map.get(6) == 'F');
	GTEST_ASSERT(map.get(5) == nullptr);
	map.add('E');
	GTEST_ASSERT(*map.get(5) == 'E');
	GTEST_ASSERT(*map.get(4) == 'D' && *map.get(6) == 'F');
	return 1;
}
