#include <gtest.hpp>

#include <gongen/common/platform/system.hpp>

GTEST(paths) {
	gen::file::File file;
	file = GEN_NO_EXCEPT(gen::file::File("data/i_exist.txt"));
	GTEST_ASSERT_MSG(file.is_open(), "i_exist should be open");
	file = GEN_NO_EXCEPT(gen::file::File("data/i_dont_exist.txt"));
	GTEST_ASSERT_MSG(!file.is_open(), "i_dont_exist should not open");
	file = GEN_NO_EXCEPT_ELSE(gen::file::File("data/i_dont_exist.txt"), GEN_NO_EXCEPT(gen::file::File("data/i_exist.txt")));
	GTEST_ASSERT_MSG(file.is_open(), "i_dont_exist should not open and fallback to i_exist");
	return 0;
}
