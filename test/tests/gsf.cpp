#include <gtest.hpp>

#include <gongen/gsf.hpp>
#include <gongen/common/platform/system.hpp>

GTEST(gsf) {
	gen::File file;
	gen::gsf::Context ctx;
	file = gen::File("data/gsf/schema1.gts");
	gen::gsf::Schema::txt_read_to_ctx(ctx, &file);
	file = gen::File("data/gsf/schema2.gts");
	gen::gsf::Schema::txt_read_to_ctx(ctx, &file);
	file = gen::File("data/gsf/article.gta");
	gen::gsf::Article article = gen::gsf::Article::txt_read_ctx(ctx, &file);
	gen::gsf::Object object = article.access_root().decode_object();
	gen::gsf::Object deeper = object.access_at_key("object")->decode_object();
	int32_t val = deeper.access_at_key("value")->decode_i32();
	GTEST_ASSERT(val == 8);
	return 0;
}
