#pragma once

#include <gongen/common/platform/memory.hpp>
#include <stdio.h>

// TODO job system
// TODO test selection through argv
// TODO test dependencies (if one fails, another is skipped)
// TODO let tests ask for human verification after all tests

struct gtest_info {
	const char* name;
	int (*test)(gtest_info&);
	const char* assert = nullptr;
	char* assert_msg = nullptr;
};

unsigned gtest_register(gtest_info info);

#define GTEST(NAME)																			\
    static int GTEST_FUNC_##NAME(gtest_info&);												\
	const static unsigned GTEST_ID_##NAME = gtest_register(gtest_info {						\
		.name = #NAME,																		\
		.test = GTEST_FUNC_##NAME,															\
	});																						\
	static int GTEST_FUNC_##NAME(gtest_info& gtest_)

#define GTEST_CODE_ASSERT 10000
#define GTEST_ASSERT(statement) do {														\
	if (statement) break;                 													\
	gtest_.assert = #statement;																\
	return -GTEST_CODE_ASSERT;																\
} while (0)
#define GTEST_ASSERT_MSG(statement, ...) do {												\
	if (statement) break;                 													\
	gtest_.assert = #statement;																\
	gtest_.assert_msg = (char*) gen::alloc(snprintf(nullptr, 0, __VA_ARGS__));				\
	sprintf(gtest_.assert_msg, __VA_ARGS__);												\
	return -GTEST_CODE_ASSERT;																\
} while (0)
