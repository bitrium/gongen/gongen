#include "gtest.hpp"

#include <time.h>
#include <exception>
#include <string.h>
#include <gongen/common/main.hpp>
#include <gongen/common/logger.hpp>

struct gtest_info tests[1024];

static unsigned next_test_id = 0;

unsigned gtest_register(gtest_info info) {
	tests[next_test_id] = info;
	return next_test_id++;
}

int main(int argc, char** argv) {
	gen::init();
	gen::log::Thread log_thread;
	log_thread.add_handler(new gen::log::ConsoleHandler(gen::log::LEVEL_DEBUG)); // TODO custom handler that adds "- logs:" to the output if there are any
	printf("running %i tests\n", next_test_id);
	int failed = 0;
	int ret = 0;
	char exception_buf[1024];
	for (unsigned i = 0; i < next_test_id; i++) {
		exception_buf[0] = '\0';
		printf("- running test '%s' (#%u)...\n", tests[i].name, i);
		clock_t start = clock();
		try {
			ret = tests[i].test(tests[i]);
		} catch (const std::exception& exception) {
			 strncpy(exception_buf, exception.what(), 1024);
			 ret = -1;
		}
		printf("  - took %f seconds\n", (double) (clock() - start) / CLOCKS_PER_SEC);
		failed++;
		if (ret >= 0) {
			printf("  - succeeded with code %i\n", ret);
			failed--;
		} else if (exception_buf[0] != '\0') {
			printf("  - failed with exception: %s\n", exception_buf);
		} else if (ret == -GTEST_CODE_ASSERT && tests[i].assert) {
			printf("  - failed on assertion: %s\n", tests[i].assert);
			if (tests[i].assert_msg) {
				printf("    - msg: %s\n", tests[i].assert_msg);
				gen::free((void*) tests[i].assert_msg);
				tests[i].assert_msg = nullptr;
			}
		} else {
			printf("  - failed with code %i\n", ret);
		}
	}
	printf("%i succeeded %i failed\n", next_test_id - failed, failed);
	return failed;
}
