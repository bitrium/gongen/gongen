#!/usr/bin/env python

from sys import argv
from pathlib import Path

lookup: list[str] = [
	'cgen/**/*.py',
	'CMakeLists.txt',
	'cmake/**/*.cmake',
	'include/**/*.hpp',
	'src/**/*.cpp',
	'src/**/*.hpp',
]

comment_prefix: dict[str, str] = {
	'py': '#',
	'txt': '#',
	'cmake': '#',
	'hpp': '//',
	'cpp': '//',
}

BOILERPLATE_LICENSE = 'SPDX-License-Identifier: BSD-3-Clause'
BOILERPLATE_COPYRIGHT = 'Copyright (c) 2024 GongEn Contributors'

def get_paths(root: Path) -> list[Path]:
	paths: list[Path] = []
	for spec in lookup:
		paths.extend(root.glob(spec))
	return paths

def update_boilerplates(paths: list[Path]) -> tuple[int, int, int]:
	added = 0
	updated = 0
	left = 0
	for path in paths:
		if not path.exists():
			continue
		if not path.suffix[1:] in comment_prefix:
			print(f'unknown extension "{path.suffix}" of file "{path}"')
			continue
		prefix = comment_prefix[path.suffix[1:]] + ' '
		license_txt = prefix + BOILERPLATE_LICENSE + '\n'
		copyright_txt = prefix + BOILERPLATE_COPYRIGHT + '\n'
		file = open(path, 'r+t')
		line = file.readline()
		if line != license_txt:
			print(f'adding boilerplate to file "{path}"')
			file.seek(0)
			content = file.read()
			file.seek(0)
			file.write(license_txt + copyright_txt + '\n' + content)
			added += 1
			continue
		pos = len(line)
		line = file.readline()
		if line != copyright_txt:
			if len(line) == len(copyright_txt):
				print(f'updating boilerplate in file "{path}"')
				file.seek(pos)
				file.write(copyright_txt)
				updated += 1
				continue
			else:
				print(f'manual action required in file "{path}"')
		left += 1
	return added, updated, left

if __name__ == '__main__':
	path = Path('.')
	if len(argv) > 1:
		path = Path(argv[1])
	if not path.exists() or not path.is_dir():
		print(f'path "{path}" does not exist or is not a directory')
		exit(1)
	print(f'updating legal boilerplates in path "{path}"')
	added, updated, left = update_boilerplates(get_paths(path))
	print(f'added {added}, updated {updated}, left {left} boilerplates')
